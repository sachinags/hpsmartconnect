package com.agstransact.HP_SC.splash.fragment;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.fragment.FragmentLoginScreen;
import com.agstransact.HP_SC.login.fragment.FragmentLoginWithMPin;
import com.agstransact.HP_SC.models.ModelRequest;
import com.agstransact.HP_SC.models.ModelResponse;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.NetworkConnectivityReceiver;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.google.gson.Gson;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Satish Patel on 25,March,2021
 */
public class FragmentLaunchScreen extends Fragment {

    int SPLASH_TIME_OUT = 1000;
    ImageView splashIV;
    String TAG = "LaunchScreen";
    Cipher cipher;
    KeyStore keyStore;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_splash, container, false);

        splashIV =view.findViewById(R.id.splashIV);

        Animation animZoomIn = AnimationUtils.loadAnimation(getContext(), R.anim.zoom_inanimation);

        splashIV.startAnimation(animZoomIn);

        new Handler().postDelayed(new Runnable() {

            /*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {

//            replaceScreen(new FragmentLoginScreen());

                if (NetworkConnectivityReceiver.isNetworkAvailable(getActivity())) {
//                if (Singleton.Instance().isNetworkAvailable) {
                    root();
//                    funCallNextScreen();
                } else {
                    NetworkConnectivityReceiver.showDialog(getActivity());
                }
            }
        }, SPLASH_TIME_OUT);

        return view;
    }

    @TargetApi(Build.VERSION_CODES.M)
    protected void generateKey() {
        try {
            keyStore = KeyStore.getInstance("AndroidKeyStore");
        } catch (Exception e) {

            Log.e("FP_Error", e.toString());
        }


        KeyGenerator keyGenerator;
        try {
            keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
        } catch (NoSuchAlgorithmException | NoSuchProviderException e) {
            throw new RuntimeException("Failed to get KeyGenerator instance", e);
        }
        try {
            keyStore.load(null);
            keyGenerator.init(new
                    KeyGenParameterSpec.Builder(ConstantDeclaration.KEY_NAME,
                    KeyProperties.PURPOSE_ENCRYPT |
                            KeyProperties.PURPOSE_DECRYPT)
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setUserAuthenticationRequired(true)
                    .setEncryptionPaddings(
                            KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build());
            keyGenerator.generateKey();
        } catch (NoSuchAlgorithmException |
                InvalidAlgorithmParameterException
                | CertificateException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    public boolean cipherInit() {
        try {
            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
            throw new RuntimeException("Failed to get Cipher", e);
        }
        try {
            keyStore.load(null);
            SecretKey key = (SecretKey) keyStore.getKey(ConstantDeclaration.KEY_NAME, null);
            cipher.init(Cipher.ENCRYPT_MODE, key);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void root() {

        versionCheck();

    }

    private void versionCheck() {

        JSONObject json = null;
        Long tsLing = null;

        try {
            int verCode = 0;
            try {
                PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
                String version = pInfo.versionName;
                verCode = pInfo.versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
                Log.e(TAG, e.toString());
            }
            json = new JSONObject();
            json.put("DeviceID", ConstantDeclaration.getDeviceID(getActivity()));
            json.put("DeviceName", Build.MODEL);
            json.put("Version", verCode);
//            json.put("Version", "18");
            json.put("SourceID", "AA6CA277-F7C1-46BF-AA29-A59CCC9CEE9F");

            tsLing = System.currentTimeMillis() / 1000;
            json.put("TIMESTAMP", tsLing.toString());
            Log.e("json",json.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

       /* String d1, d2;
        Gson gson = new Gson();
        d1 = json.toString();
        d2 = "";
        ModelRequest modelRequest =new ModelRequest(d1,d2);

        MainApplication.apiManager.getVersionDetails(json, new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.e("response",response.toString());
                String s = response.body().toString();
                if (s.contains("||")) {
                    String[] str = new String[0];
                    try {
                        str = (s.split("\\|\\|"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        if (str[0].equalsIgnoreCase("00")) {
                            //show success screen
                            try {
                                manageLoginFlow();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else if(str[0].equalsIgnoreCase("null")){
                            new UniversalDialog(getActivity(), new AlertDialogInterface() {
                                @Override
                                public void methodDone() {
                                    getActivity().finish();
                                    //                            versionCheck();
                                }
                                @Override
                                public void methodCancel() {

                                }
                            }, "",
                                    "Something went wrong.", getString(R.string.dialog_ok), "").showAlert();
                        }
                        else {
                            new UniversalDialog(getActivity(), new AlertDialogInterface() {
                                @Override
                                public void methodDone() {
                                    getActivity().finish();
                                    //                            versionCheck();
                                }
                                @Override
                                public void methodCancel() {

                                }
                            }, "",
                                    str[1], getString(R.string.dialog_ok), "").showAlert();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (NetworkConnectivityReceiver.isNetworkAvailable(getActivity())) {
                        new UniversalDialog(getActivity(), new AlertDialogInterface() {
                            @Override
                            public void methodDone() {
                                getActivity().finish();
                            }

                            @Override
                            public void methodCancel() {

                            }
                        }, "",
                                getString(R.string.server_not_resp), getString(R.string.dialog_ok), "").showAlert();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                Log.e("call",call.toString());
            }
        });*/

        new AsyncCallVersion(tsLing.toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncCallVersion extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp;

        public AsyncCallVersion(String timeStamp) {
            this.strTimeStamp = timeStamp;
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {

//            return ClsWebService.PostObject("2", false, jsonObjects[0]);
//            return null;
            return ClsWebService_hpcl.PostObjecttoken("Login/GetVersion", false, jsonObjects[0],"");
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }

            String str_time = "";
            if (s.contains("||")) {
                String[] str = new String[0];
                try {
                    str = (s.split("\\|\\|"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (str[0].equalsIgnoreCase("00")) {
                        //show success screen
                        try {
                            manageLoginFlow();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else if(str[0].equalsIgnoreCase("null")){
                        new UniversalDialog(getActivity(), new AlertDialogInterface() {
                            @Override
                            public void methodDone() {
                                getActivity().finish();
                                //                            versionCheck();
                            }
                            @Override
                            public void methodCancel() {

                            }
                        }, "",
                                "Something went wrong.", getString(R.string.dialog_ok), "").showAlert();
                    }
                    else if (str[0].equalsIgnoreCase("101") || str[0].equalsIgnoreCase("102")) {
                        String msg = null;
                        try {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            msg = jsonObject.getString("ResponseMessage");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            try {
                                msg = str[1];
                            } catch (Exception ex) {
                                ex.printStackTrace();
                            }
                        }
//                        try {
//                            msg = str[1];
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }

                        AlertDialog.Builder builder =
                                new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);
                        TextView resultMessage = new TextView(getActivity());
                        resultMessage.setTextSize(18);
                        resultMessage.setText(msg);
                        resultMessage.setGravity(Gravity.CENTER);
                        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        lp.setMargins(0, 20, 0, 0);
                        resultMessage.setPadding(0, 30, 0, 0);
                        resultMessage.setLayoutParams(lp);
                        builder.setView(resultMessage);
                        builder.setCancelable(false);
                        builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
//                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                    getActivity().finish();
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
                                    getActivity().finish();
                                }
                            }
                        });

                        if (str[0].equalsIgnoreCase("101")) {
                            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    funCallNextScreen();
                                }
                            });
                        }

                        AlertDialog dialog = builder.create();
                        dialog.show();

                    }
                    else {
                        new UniversalDialog(getActivity(), new AlertDialogInterface() {
                            @Override
                            public void methodDone() {
                                getActivity().finish();
                                //                            versionCheck();
                            }
                            @Override
                            public void methodCancel() {

                            }
                        }, "",
                                str[1], getString(R.string.dialog_ok), "").showAlert();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                if (NetworkConnectivityReceiver.isNetworkAvailable(getActivity())) {
                    new UniversalDialog(getActivity(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {
                            getActivity().finish();
                        }

                        @Override
                        public void methodCancel() {

                        }
                    }, "",
                            getString(R.string.server_not_resp), getString(R.string.dialog_ok), "").showAlert();
                }
            }

        }
    }

    private void manageLoginFlow() {
        funCallNextScreen();
    }


    private void funCallNextScreen() {
        if(UserDataPrefrence.getPreference("HPCL_Preference",
                getContext(), "MPINSET", "ISMPINSET").equalsIgnoreCase("true")) {
//            ActionChangeFrag actionChangeFrag = (ActionChangeFrag) getActivity();
//            actionChangeFrag.addFragment(new FragmentLoginWithMPin());
            replaceScreen(new FragmentLoginWithMPin());
        }else{
//            ActionChangeFrag actionChangeFrag = (ActionChangeFrag) getActivity();
//            actionChangeFrag.addFragment(new FragmentLoginScreen());
            replaceScreen(new FragmentLoginScreen());
        }
    }

    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getFragmentManager();
        Fragment currentFrag = fragmentManager.findFragmentById(R.id.frameLayout);
        com.agstransact.HP_SC.utils.Log.e("Current Fragment", "" + currentFrag);
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, frag, "Login");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("Login");
        fragmentTransaction.commitAllowingStateLoss();
        currentFrag = fragmentManager.findFragmentById(R.id.frameLayout);
        com.agstransact.HP_SC.utils.Log.e("Current Fragment", "" + currentFrag);

    }

}

package com.agstransact.HP_SC.bluetooth_print;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
//import com.agstransact.HP_SC.common.ConstantDeclaration;
//import com.agstransact.HP_SC.common.Log;
//import com.agstransact.HP_SC.prefrence.UserDataPrefrence;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;

//import static com.agstransact.HP_SC.common.ConstantDeclaration.CALL_FROM_TXNSUCCESS;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.TRAN_TYPE_MOBILETOPUP;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.TRAN_TYPE_NWALLET_TO_MOBILETOPUP;

/**
 * Created by prashant.gadekar on 25-10-2017.
 */

public class ClsBTPrintHandler {
    // android built in classes for bluetooth operations
    static public BluetoothAdapter mBluetoothAdapter;
    static public P25Connector mConnector;
    Context mContext;

    public static boolean isBluetoothConnected(){
        if(mBluetoothAdapter!=null){
            return mBluetoothAdapter.isEnabled();
        }
        return false;
    }

    public static boolean isPrinterConnected(){
//        try {
//            mConnector.connect(Singleton.Instance().mBTDevice);
//        } catch (P25ConnectionException e) {
//            e.printStackTrace();
//            return true;
//        } catch (Exception e){
//            e.printStackTrace();
//        }
        if(mConnector!=null){
            return mConnector.isConnected();
        }
        return false;
    }


    public static void initPrinterConnector(final P25Connector.P25ConnectionListener listener) {

        try {
            // more codes will be here
            // we are going to have three buttons for specific functions
            mConnector = new P25Connector(new P25Connector.P25ConnectionListener() {
                @Override
                public void onStartConnecting() {
                    listener.onStartConnecting();
                }

                @Override
                public void onConnectionSuccess() {
                    listener.onConnectionSuccess();
                }

                @Override
                public void onConnectionFailed(String error)
                {
//                    new UniversalDialog(context,null,"","Connection failed.","OK","").showAlert();
                    listener.onConnectionFailed(error);

                }

                @Override
                public void onConnectionCancelled()
                {
//                    mConnectingDlg.dismiss();
                    listener.onConnectionCancelled();
                }

                @Override
                public void onDisconnected() {
                    listener.onDisconnected();
                    mConnector = null;
                    mBluetoothAdapter = null;
                }
            });
        }catch(Exception e) {
            e.printStackTrace();
        }
//        try {
//            if(Singleton.Instance().mBTDevice!=null){
//                createBond(Singleton.Instance().mBTDevice);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    //connect printer
    static private void createBond(BluetoothDevice device) throws Exception {
        try {
            Class<?> cl 	= Class.forName("android.bluetooth.BluetoothDevice");
            Class<?>[] par 	= {};
            Method method 	= cl.getMethod("createBond", par);
            method.invoke(device);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    // this will send text data to be printed by the bluetooth printer
    /*private static void sendData(Bundle bundle, Context mContext) throws IOException {
        try {
            try {
                if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_MOBILETOPUP)){
                }
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    bundle.putString(CALL_FROM_TXNSUCCESS, bundle.getString("TranType"));
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
            //riteshb 17-11
            if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_MOBILETOPUP)
                    || bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_NWALLET_TO_MOBILETOPUP)) {

                String msg = "";
                msg += "------------------------------------------\n";
                //riteshb 20-11
                if(!bundle.getString("TELCONAME").equalsIgnoreCase("metfone")){
//                    mConnector.printCustom(strmobile,0,0);
                    String strReceipt= " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                    String strCustomer = " Telco Name.    : "+ bundle.getString("TELCONAME") + "\n";
                    String strmobile = " Mobile No.     : +855 "+ bundle.getString("MOBILENUMBER") + "\n";


//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                    String strPaymentAmountTxt= " Payment Amount : ";
                    String strCurrency = "";
                    if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                        strCurrency = "$ ";
                    }else{
                        strCurrency = "KHR ";
                    }
                    String strAmount= bundle.getString("PaymentAmntReferenceNumber", "")+"\n";
                    String strFee = " Fee            : ";
                    String strFee_value = bundle.getString("fee", "")+"\n";
                    String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";

                    mConnector.printCustom(msg,0,0);
                    mConnector.printCustom(strReceipt,0,0);
                    mConnector.printCustom(strCustomer,0,0);
                    mConnector.printCustom(strmobile,0,0);
                    mConnector.printCustom(strPaymentAmountTxt,0,0);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strAmount,0,0);
//                    mConnector.printCustom(strFee,0,0);
//                    mConnector.printCustom(strCurrency,1,0);
//                    mConnector.printCustom(strFee_value,0,0);

                    mConnector.printCustom(strDate,0,0);
                    if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                        String strpin = mContext.getString(R.string.pin_no) + "       : " + bundle.getString("PIN")+"\n";
                        String strexpdate = mContext.getString(R.string.expiry_date) + "       : " + bundle.getString("EXPIRYDATE")+"\n";
                        mConnector.printCustom(strpin,0,0);
                        mConnector.printCustom(strexpdate,0,0);
                    }
                    mConnector.printCustom(msg,0,0);
                    Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_bottom);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                    if (label_sender != null) {
                        mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(tot,0,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

//                    mConnector.printCustom(strFooter,0,1);
                }else{
                    //metfone
                    //print khmer header
                    Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_metfone_logo);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 270, 62, false);

                    if (label_fund_success != null) {
                        mConnector.printPhoto(label_fund_success);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    String strCurrency = "";
                    if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                        strCurrency = "      $ ";
                    }else{
                        strCurrency = "      KHR ";
                    }
                    String strAmount= bundle.getString("PaymentAmntReferenceNumber", "")+"\n";

                    label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_topup_refill_value);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                    if (label_fund_success != null) {
                        mConnector.printCustom(msg, 0, 0);
                        mConnector.printPhoto(label_fund_success);
                        mConnector.printCustom(strCurrency, 1, 0);
                        mConnector.printCustom(strAmount, 1, 0);
                        mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_topup_refill_code);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                    if (label_fund_success != null) {
                        mConnector.printPhoto(label_fund_success);
                        if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                            StringBuilder s;
                            s = new StringBuilder(bundle.getString("PIN"));

                            for(int i = 4; i < s.length(); i += 5){
                                s.insert(i, " ");
                            }
                            String strpin = "  " + s.toString() + "\n";
//                            String strexpdate = " " + getActivity().getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE") + "\n";

                            mConnector.printCustom(strpin, 2, 0);
//                            mConnector.printCustom(strexpdate, 0, 0);
                        }

                        mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_topup_details);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 410, 220, false);

                    if (label_fund_success != null) {
                        mConnector.printPhoto(label_fund_success);
                        mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    String strReceipt= " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                    String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";
                    mConnector.printCustom(strReceipt,1,0);
                    mConnector.printCustom(strDate,1,0);
                    mConnector.printCustom(msg, 0, 0);

                    label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_metfone_call);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 165, false);
                    if (label_fund_success != null) {
                        mConnector.printPhoto(label_fund_success);
//                    mConnector.printCustom(tot,0,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    mConnector.printCustom("\n\n", 0, 0);

                }
            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)
                    ) {
                //transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fund_trans_is_successful);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                if (label_fund_success != null) {
                    mConnector.printCustom(msg, 0, 0);
                    mConnector.printPhoto(label_fund_success);
                    mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender= "";
                try {
                    strsender = "            +855 " + bundle.getString("SenderNo") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strsender,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "            +855 "+ bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_receiver_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCustomer,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                if(bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)){
                    String strcollection = "      "+bundle.getString("collectionCode") + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_collection_code);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
                        mConnector.printPhoto(label_sender);
                        mConnector.printCustom(strcollection,3,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                }
                mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "            $ ";
                }else{
                    strCurrency = "            KHR ";
                }
                String strAmount= ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strAmount,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strFee_value,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",",""))
                            + Double.parseDouble(strFee_value.replace(",","")))+"\n";
                    mConnector.printCustom(tot,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    String agent = "            +855 "+ UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "");
                    mConnector.printCustom(agent,2,0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt= "\nReceipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate= "Date/Time : " +bundle.getString("DateTime", "")+"\n";
                mConnector.printCustom(strReceipt,1,0);
                mConnector.printCustom(strDate,1,0);
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom("\n\n", 0, 0);

            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_EWALLET)
                    ) {
                //transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_dara_to_dara);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 260, 40, false);

                if (label_fund_success != null) {
                    mConnector.printCustom(msg, 0, 0);
                    mConnector.printPhoto(label_fund_success);
                    mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender= "";
                try {
                    strsender = "            +855 " + bundle.getString("SenderNo") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strsender,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "            +855 "+ bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_dara_receiver);
                label_sender = Bitmap.createScaledBitmap(label_sender, 400, 40, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCustomer,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                if(bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)){
                    String strcollection = "      "+bundle.getString("collectionCode") + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_collection_code);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
                        mConnector.printPhoto(label_sender);
                        mConnector.printCustom(strcollection,3,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                }
                mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "            $ ";
                }else{
                    strCurrency = "            KHR ";
                }
                String strAmount= ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strAmount,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strFee_value,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",",""))
                            + Double.parseDouble(strFee_value.replace(",","")))+"\n";
                    mConnector.printCustom(tot,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    String agent = "            +855 "+ UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "");
                    mConnector.printCustom(agent,2,0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt= "\nReceipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate= "Date/Time : " +bundle.getString("DateTime", "")+"\n";
                mConnector.printCustom(strReceipt,1,0);
                mConnector.printCustom(strDate,1,0);
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom("\n\n", 0, 0);

            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_NONWALLET)
                    ) {

                //transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fund_trans_is_successful);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                if (label_fund_success != null) {
                    mConnector.printCustom(msg, 0, 0);
                    mConnector.printPhoto(label_fund_success);
                    mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender= "";
                try {
                    strsender = "            +855 " + bundle.getString("FromNumber") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strsender,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "            +855 "+ bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_receiver_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCustomer,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strcollection = "            "+ bundle.getString("collectionCode") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_collection_code);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strcollection,4,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "            $ ";
                }else{
                    strCurrency = "            KHR ";
                }
                String strAmount= ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,2,0);
                    mConnector.printCustom(strAmount,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,2,0);
                    mConnector.printCustom(strFee_value,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,2,0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",",""))
                            + Double.parseDouble(strFee_value.replace(",","")))+"\n";
                    mConnector.printCustom(tot,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    String agent = "            +855 "+ UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "");
                    mConnector.printCustom(agent,2,0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt= "\nReceipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate= "Date/Time: " +bundle.getString("DateTime", "")+"\n";
                mConnector.printCustom(strReceipt,1,0);
                mConnector.printCustom(strDate,1,0);
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom("\n\n", 0, 0);

            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA)
                    ||bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA_CP)
                    ) {
                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bank_header);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 260, 40, false);

                if (label_fund_success != null) {
                    mConnector.printCustom(msg, 0, 0);
                    mConnector.printPhoto(label_fund_success);
                    mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }


                //riteshb 21-11
                String strBankName= "";
                try {
                    strBankName= "       " + bundle.getString("BankName") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bank_name);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strBankName,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                JSONArray jsonArray = new JSONArray(bundle.getString("json"));
                JSONObject jsonObject = jsonArray.getJSONObject(0);

                String strCustAccName = "";
                if(jsonObject.getString("ACCOUNTNAME").length() <= 18){
                    strCustAccName= "       " + jsonObject.getString("ACCOUNTNAME") + "\n";
                }else{
                    strCustAccName= jsonObject.getString("ACCOUNTNAME") + "\n";
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bank_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCustAccName,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strCustAccNo= "       " + bundle.getString("AccountNumber") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_acc_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCustAccNo,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                if(!bundle.getString("CustomerMobile").isEmpty()) {
                    String strCustomerMob = "    +855 "+ bundle.getString("CustomerMobile", "") + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_cust_no_bank);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
                        mConnector.printPhoto(label_sender);
                        mConnector.printCustom(strCustomerMob,2,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }

                mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if(bundle.getString("Currency").equalsIgnoreCase("USD")) {
                    strCurrency = "            $ ";
                }else{
                    strCurrency = "            KHR ";
                }
                String strAmount= ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("Amount", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,2,0);
                    mConnector.printCustom(strAmount,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("ProcessingFee", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,2,0);
                    mConnector.printCustom(strFee_value,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,2,0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",",""))
                            + Double.parseDouble(strFee_value.replace(",","")))+"\n";
                    mConnector.printCustom(tot,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    String agent = "            +855 "+ UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "");
                    mConnector.printCustom(agent,2,0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt= "\nReceipt No.: " + jsonObject.getString("RECEIPTNO") + "\n";
                String strDate= "Date/Time: " +jsonObject.getString("TRANSACTIONDATEFORMATED")+"\n";
                mConnector.printCustom(strReceipt,1,0);
                mConnector.printCustom(strDate,1,0);
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom("\n\n", 0, 0);

//
//                String msg = "";
//                msg += "------------------------------------------\n";
//                //print khmer header
//                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_bank_header);
//                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 260, 40, false);
//
//                if (label_fund_success != null) {
//                    mConnector.printCustom(msg, 0, 0);
//                    mConnector.printPhoto(label_fund_success);
//                    mConnector.printCustom(msg, 0, 0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//
//
//                //riteshb 21-11
//                String strBankName= "";
//                try {
//                    strBankName= "     +855 " + bundle.getString("BankName") + "\n";
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_bank_name);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(strBankName,2,0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//
//                JSONArray jsonArray = new JSONArray(bundle.getString("json"));
//                JSONObject jsonObject = jsonArray.getJSONObject(0);
//
//                String strCustAccName= "            +855 " + jsonObject.getString("ACCOUNTNAME") + "\n";
//
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_bank_number);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(strCustAccName,2,0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//
//                String strCustAccNo= "       " + bundle.getString("AccountNumber") + "\n";
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_acc_number);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(strCustAccNo,2,0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//
//                String strCustomerMob = "    +855 "+ bundle.getString("CustomerMobileNo", "") + "\n";
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_cust_no_bank);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(strCustomerMob,2,0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//                mConnector.printCustom(msg, 0, 0);
//
//                String strCurrency = "";
//                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
//                    strCurrency = "            $ ";
//                }else{
//                    strCurrency = "            KHR ";
//                }
//                String strAmount= ConstantDeclaration.amountFormatter(
//                        Double.parseDouble(bundle.getString("Amount", "0").replace(",","")))
//                        +"\n";
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_amount);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(strCurrency,2,0);
//                    mConnector.printCustom(strAmount,2,0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//                String strFee_value = ConstantDeclaration.amountFormatter(
//                        Double.parseDouble(bundle.getString("ProcessingFee", "0").replace(",","")))
//                        +"\n";
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_fee);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(strCurrency,2,0);
//                    mConnector.printCustom(strFee_value,2,0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_total);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(strCurrency,2,0);
//                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",",""))
//                            + Double.parseDouble(strFee_value.replace(",","")))+"\n";
//                    mConnector.printCustom(tot,2,0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//                mConnector.printCustom(msg, 0, 0);
//
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_agent_contact);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
//                    String agent = "            +855 "+ UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
//                            ConstantDeclaration.USER_MOBILE, "");
//                    mConnector.printCustom(agent,2,0);
//
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//
//                String strReceipt= "\nReceipt No.: " + jsonObject.getString("RECEIPTNO") + "\n";
//                String strDate= "Date/Time: " +jsonObject.getString("TRANSACTIONDATEFORMATED")+"\n";
//                mConnector.printCustom(strReceipt,1,0);
//                mConnector.printCustom(strDate,1,0);
//                mConnector.printCustom(msg, 0, 0);
//
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_bottom);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 400, 170, false);
//                if (label_sender != null) {
//                    mConnector.printPhoto(label_sender);
////                    mConnector.printCustom(tot,0,0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//                mConnector.printCustom("\n\n", 0, 0);

            } else {
                //transfer

                String strTitle = "DaraPay";

                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strCustomer = " Customer No.   : +855 "+ bundle.getString("ToNumber", "") + "\n";
//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt= " Payment Amount : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }

                String strAmount= bundle.getString("PaymentAmntReferenceNumber", "")+"\n";
                String strFee = " Fee            : ";
                String strFee_value = bundle.getString("fee", "")+"\n";
                String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";
                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strCustomer,0,0);
                mConnector.printCustom(strPaymentAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount,0,0);
                mConnector.printCustom(strFee,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strFee_value,0,0);

                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strFooter,0,1);
                mConnector.printCustom("\n\n", 0, 0);

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

   /* private static void sendDataOld(Bundle bundle, Context mContext) throws IOException {
        try {
//            bundle.getString("ReceiptNoError", "");

            //riteshb 17-11
            if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_MOBILETOPUP)
                    || bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_NWALLET_TO_MOBILETOPUP)) {
                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strCustomer = " Telco Name.    : "+ bundle.getString("TELCONAME") + "\n";
                String strmobile = " Mobile No.     : +855 "+ bundle.getString("MOBILENUMBER") + "\n";
//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt= " Payment Amount : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }
                String strAmount = bundle.getString("PaymentAmntReferenceNumber", "") + "\n";
                *//*Amit Verma 22.11.17*//*
//                String strFee = " Fee            : ";
//                String strFee_value = bundle.getString("fee", "") + "\n";
                 *//*Amit Verma 22.11.17*//*
                String strDate = " Date/Time      : " + bundle.getString("DateTime", "") + "\n";

                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strCustomer,0,0);
                //riteshb 20-11
                if(!bundle.getString("TELCONAME").equalsIgnoreCase("metfone")){
                    mConnector.printCustom(strmobile,0,0);
                }
                mConnector.printCustom(strPaymentAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount, 0, 0);
                 *//*Amit Verma 22.11.17*//*
//                mConnector.printCustom(strFee, 0, 0);

//                mConnector.printCustom(strCurrency, 1, 0);
                *//*Amit Verma 22.11.17*//*
//            mConnector.printText(Ux17DB);
                 *//*Amit Verma 22.11.17*//*
//                mConnector.printCustom(strFee_value, 0, 0);
                 *//*Amit Verma 22.11.17*//*

                mConnector.printCustom(strDate,0,0);
                if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                    String strpin = " "+mContext.getString(R.string.pin_no) + "        : " + bundle.getString("PIN")+"\n";
                    String strexpdate = " "+mContext.getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE")+"\n";
                    mConnector.printCustom(strpin,0,0);
                    mConnector.printCustom(strexpdate,0,0);
//                    tvTopUpPINLabel.setText(getString(R.string.pin_no));
//                    tvTopUpPINValue.setText(": " + bundle.getString("PIN"));
//                    tvTopUpExpiryDateLabel.setText(getString(R.string.expiry_date));
//                    tvTopUpExpiryDateValue.setText(": " + bundle.getString("EXPIRYDATE"));
                }

                mConnector.printCustom(strFooter,0,1);


            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)
                    || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_EWALLET)) {
                //transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fund_trans_is_successful);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                if (label_fund_success != null) {
                    mConnector.printCustom(msg, 0, 0);
                    mConnector.printPhoto(label_fund_success);
                    mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender= "";
                try {
                    strsender = "                        +855 " + bundle.getString("SenderNo") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strsender,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "                        +855 "+ bundle.getString("ToNumber", "") + "\n\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_receiver_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCustomer,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strcollection = "                     "+bundle.getString("collectionCode") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_collection_code);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strcollection,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "                 $ ";
                }else{
                    strCurrency = "                 KHR ";
                }
                String strAmount= ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strAmount,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strFee_value,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",",""))
                            + Double.parseDouble(strFee_value.replace(",","")))+"\n";
                    mConnector.printCustom(tot,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(tot,0,0);
                    String agent = "                        +855 "+UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "");
                    mConnector.printCustom(agent,0,0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt= "\n Receipt No.  : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                strDate= "                 081 799 888\n" +
                        "                www.darapay.com.kh\n\n\n";
                mConnector.printCustom(strReceipt,0,0);

            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_NONWALLET)
                    ) {

                //transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fund_trans_is_successful);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                if (label_fund_success != null) {
                    mConnector.printCustom(msg, 0, 0);
                    mConnector.printPhoto(label_fund_success);
                    mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender= "";
                try {
                    strsender = "                         +855 " + bundle.getString("FromNumber") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strsender,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "                         +855 "+ bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_receiver_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCustomer,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strcollection = "                     "+bundle.getString("collectionCode") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_collection_code);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strcollection,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "                  $ ";
                }else{
                    strCurrency = "                  KHR ";
                }
                String strAmount= ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strAmount,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",","")))
                        +"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    mConnector.printCustom(strFee_value,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    mConnector.printCustom(strCurrency,1,0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",",""))
                            + Double.parseDouble(strFee_value.replace(",","")))+"\n";
                    mConnector.printCustom(tot,2,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
                    String agent = "                         +855 "+UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "");
                    mConnector.printCustom(agent,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt= "\n Receipt No.  : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    mConnector.printPhoto(label_sender);
//                    mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                strDate= "                 081 799 888\n" +
                        "                www.darapay.com.kh\n\n\n";
                mConnector.printCustom(strReceipt,0,0);

            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA)
                    || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA_CP)
                    ) {
                //transfer
                JSONArray jsonArray = new JSONArray(bundle.getString("json"));

                JSONObject jsonObject = jsonArray.getJSONObject(0);

                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.        : " + jsonObject.getString("RECEIPTNO") + "\n";
                String strBankName= " Bank Name          : +855 " + bundle.getString("BankName") + "\n";
                String strCustAccName= " Customer A/C Name  : +855 " + jsonObject.getString("ACCOUNTNAME") + "\n";
                String strCustAccNo= " Customer A/C No.   : " + bundle.getString("AccountNumber") + "\n";

                String strTransferAmountTxt= " Transfer Amount    : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }

                //26-3-18
                //riteshb 17-11
                //remove comma for parse
                String strAmount=  ConstantDeclaration.amountFormatter(Double.parseDouble(bundle.getString("Amount").replace(",","")))+"\n";
                String strFee = " Fee                : ";
                String strFee_value = ConstantDeclaration.amountFormatter(Double.parseDouble(bundle.getString("ProcessingFee", "").replace(",","")))+"\n";
                //26-3-18
                String strCustomerMob = " Customer Mobile No.: +855 "+ bundle.getString("CustomerMobileNo", "") + "\n";
                String strCustomerID = " Customer ID        : "+ bundle.getString("CustomerIDNo") + "\n";

                String strDate= " Date/Time       : " +jsonObject.getString("TRANSACTIONDATEFORMATED")+"\n";
                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strBankName,0,0);
                mConnector.printCustom(strCustAccName,0,0);
                mConnector.printCustom(strCustAccNo,0,0);
                mConnector.printCustom(strTransferAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount,0,0);
                mConnector.printCustom(strFee,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strFee_value,0,0);

                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strCustomerMob,0,0);
                mConnector.printCustom(strCustomerID,0,0);
                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strFooter,0,1);

            } else {
                //transfer

                String strTitle = "DaraPay";

                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strCustomer = " Customer No.   : +855 "+ bundle.getString("ToNumber", "") + "\n";
//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt= " Payment Amount : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }

                String strAmount= bundle.getString("PaymentAmntReferenceNumber", "")+"\n";
                String strFee = " Fee            : ";
                String strFee_value = bundle.getString("fee", "")+"\n";
                String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";
                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strCustomer,0,0);
                mConnector.printCustom(strPaymentAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount,0,0);
                mConnector.printCustom(strFee,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strFee_value,0,0);

                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strFooter,0,1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    /*private static void sendDataKhmer(Bundle bundle, Context mContext) throws IOException {
        try {
//            bundle.getString("ReceiptNoError", "");

            //riteshb 17-11
            if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_MOBILETOPUP)
                    || bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_NWALLET_TO_MOBILETOPUP)) {
                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strCustomer = " Telco Name.    : "+ bundle.getString("TELCONAME") + "\n";
                String strmobile = " Mobile No.     : +855 "+ bundle.getString("MOBILENUMBER") + "\n";
//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt= " Payment Amount : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }
                String strAmount= bundle.getString("PaymentAmntReferenceNumber", "")+"\n";
                String strFee = " Fee            : ";
                String strFee_value = bundle.getString("fee", "")+"\n";
                String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";

                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strCustomer,0,0);
                //riteshb 20-11
                if(!bundle.getString("TELCONAME").equalsIgnoreCase("metfone")){
                    mConnector.printCustom(strmobile,0,0);
                }
                mConnector.printCustom(strPaymentAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount,0,0);
//                mConnector.printCustom(strFee,0,0);
//                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
//                mConnector.printCustom(strFee_value,0,0);

                mConnector.printCustom(strDate,0,0);
                if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                    String strpin = " "+mContext.getString(R.string.pin_no) + "        : " + bundle.getString("PIN")+"\n";
                    String strexpdate = " "+mContext.getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE")+"\n";
                    mConnector.printCustom(strpin,0,0);
                    mConnector.printCustom(strexpdate,0,0);
//                    tvTopUpPINLabel.setText(getString(R.string.pin_no));
//                    tvTopUpPINValue.setText(": " + bundle.getString("PIN"));
//                    tvTopUpExpiryDateLabel.setText(getString(R.string.expiry_date));
//                    tvTopUpExpiryDateValue.setText(": " + bundle.getString("EXPIRYDATE"));
                }

                mConnector.printCustom(strFooter,0,1);


            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)
                    || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_EWALLET)) {
                //transfer

                String strTitle = "DaraPay";

                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.          : " + bundle.getString("ReceiptNoError", "") + "\n";
                //riteshb 21-11
                String strsender= null;
                try {
                    strsender = " Sender's Mobile No.  : +855 " + bundle.getString("SenderNo") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }

                String strCustomer = " Receiver's Mobile No.: +855 "+ bundle.getString("ToNumber", "") + "\n";
                String strcollection = " Collection Code      : "+ bundle.getString("collectionCode") + "\n";


//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt= " Payment Amount       : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }

                String strAmount= bundle.getString("PaymentAmntReferenceNumber", "")+"\n";
                String strFee = " Fee                  : ";
                String strFee_value = bundle.getString("fee", "")+"\n";
                String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";
                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strsender,0,0);
                mConnector.printCustom(strCustomer,0,0);
                mConnector.printCustom(strcollection,0,0);
                mConnector.printCustom(strPaymentAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount,0,0);
                mConnector.printCustom(strFee,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strFee_value,0,0);

                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strFooter,0,1);

            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_NONWALLET)
                    ) {
                //transfer

                String strTitle = "DaraPay";

                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.          : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strsender= " Sender's Mobile No.  : +855 " + bundle.getString("FromNumber") + "\n";

                String strCustomer = " Receiver's Mobile No.: +855 "+ bundle.getString("ToNumber", "") + "\n";
                String strcollection = " Collection Code      : "+ bundle.getString("collectionCode") + "\n";


//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt= " Payment Amount       : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }

                String strAmount= bundle.getString("PaymentAmntReferenceNumber", "")+"\n";
                String strFee = " Fee                 : ";
                String strFee_value = bundle.getString("fee", "")+"\n";
                String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";
                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
//                mConnector.printCustom(strsender,0,0);
                mConnector.printCustom(strCustomer,0,0);
                mConnector.printCustom(strcollection,0,0);
                mConnector.printCustom(strPaymentAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount,0,0);
                mConnector.printCustom(strFee,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strFee_value,0,0);

                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strFooter,0,1);

            } else  if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA)
                    || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA_CP)
                    ) {
                //transfer
                JSONArray jsonArray = new JSONArray(bundle.getString("json"));

                JSONObject jsonObject = jsonArray.getJSONObject(0);

                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.        : " + jsonObject.getString("RECEIPTNO") + "\n";
                String strBankName= " Bank Name          : +855 " + bundle.getString("BankName") + "\n";
                String strCustAccName= " Customer A/C Name  : +855 " + jsonObject.getString("ACCOUNTNAME") + "\n";
                String strCustAccNo= " Customer A/C No.   : " + bundle.getString("AccountNumber") + "\n";

                String strTransferAmountTxt= " Transfer Amount    : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }

                //26-3-18
                //riteshb 17-11
                //remove comma for parse
                String strAmount=  ConstantDeclaration.amountFormatter(Double.parseDouble(bundle.getString("Amount").replace(",","")))+"\n";
                String strFee = " Fee                : ";
                String strFee_value = ConstantDeclaration.amountFormatter(Double.parseDouble(bundle.getString("ProcessingFee", "").replace(",","")))+"\n";
                //26-3-18
                String strCustomerMob = " Customer Mobile No.: +855 "+ bundle.getString("CustomerMobileNo", "") + "\n";
                String strCustomerID = " Customer ID        : "+ bundle.getString("CustomerIDNo") + "\n";

                String strDate= " Date/Time       : " +jsonObject.getString("TRANSACTIONDATEFORMATED")+"\n";
                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strBankName,0,0);
                mConnector.printCustom(strCustAccName,0,0);
                mConnector.printCustom(strCustAccNo,0,0);
                mConnector.printCustom(strTransferAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount,0,0);
                mConnector.printCustom(strFee,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strFee_value,0,0);

                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strCustomerMob,0,0);
                mConnector.printCustom(strCustomerID,0,0);
                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strFooter,0,1);

            } else {
                //transfer

                String strTitle = "DaraPay";

                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt= " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strCustomer = " Customer No.   : +855 "+ bundle.getString("ToNumber", "") + "\n";
//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt= " Payment Amount : ";
                String strCurrency = "";
                if(bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                }else{
                    strCurrency = "KHR ";
                }

                String strAmount= bundle.getString("PaymentAmntReferenceNumber", "")+"\n";
                String strFee = " Fee            : ";
                String strFee_value = bundle.getString("fee", "")+"\n";
                String strDate= " Date/Time      : " +bundle.getString("DateTime", "")+"\n";
                String strFooter = "\n******************************************\n";
                strFooter+= "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";

                mConnector.printCustom(msg,0,0);
                mConnector.printCustom(strReceipt,0,0);
                mConnector.printCustom(strCustomer,0,0);
                mConnector.printCustom(strPaymentAmountTxt,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strAmount,0,0);
                mConnector.printCustom(strFee,0,0);
                mConnector.printCustom(strCurrency,1,0);
//            mConnector.printText(Ux17DB);
                mConnector.printCustom(strFee_value,0,0);

                mConnector.printCustom(strDate,0,0);
                mConnector.printCustom(strFooter,0,1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    public static void printReciept(Bundle bundle, Context mContext) {
            try {
                Bitmap bmp = null;
                Bitmap result = null;
                //6-3-18
                //riteshb
                //added new image to non-dara to non-dara transaction
                try {
                    if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)
                            ) {
                        bmp = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.iv_print_header_new);
//                            R.drawable.darapay_print_header_new);
                        result = Bitmap.createScaledBitmap(bmp, 261, 55, false);
                        if (bmp != null) {
                            ClsBTPrintHandler.mConnector.printPhoto(result);
                            mConnector.printCustom("\n", 0, 0);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                //6-3-18
                bmp = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_header_logo);
                result = Bitmap.createScaledBitmap(bmp, 378, 90, false);
                if (bmp != null) {
                    mConnector.printPhoto(result);
                    //riteshb 20-11
//                    sendData(bundle, mContext);
//            sendDataKhmer(bundle, mContext);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (e.toString().contains("Socket is not connected")) {
//                new UniversalDialog(mContext,null,"","Printer is not connected", "OK","").showAlert();
                } else {
                }
            }
    }

    public void disconnectPrinter() throws P25ConnectionException {
        if (mConnector == null) {
            throw new P25ConnectionException("Socket is not connected");
        }
        try {
            mConnector.disconnect();

            try {
                mConnector = null;
            } catch (Throwable e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            throw new P25ConnectionException(e.getMessage());
        }
    }

}
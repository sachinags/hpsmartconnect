package com.agstransact.HP_SC.bluetooth_print;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.agstransact.HP_SC.R;
//import com.agstransact.HP_SC.agentServices.TopUpSuccessFragment;
//import com.agstransact.HP_SC.common.ConstantDeclaration;
//import com.agstransact.HP_SC.common.Log;
//import com.agstransact.HP_SC.prefrence.UserDataPrefrence;
import com.agstransact.HP_SC.sdk.Command;
import com.agstransact.HP_SC.utils.ClsKeyboardUtil;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.Singleton;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

import static com.agstransact.HP_SC.bluetooth_print.BluetoothService.mmInStream;
import static com.agstransact.HP_SC.bluetooth_print.BluetoothService.mmOutStream;
import static com.agstransact.HP_SC.bluetooth_print.BluetoothService.mmSocket;
import static com.agstransact.HP_SC.bluetooth_print.ClsBTPrintHandler.mConnector;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.CALL_FROM_TXNSUCCESS;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_CONNECTION_LOST;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_CONN_LOST_1ST;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_CONN_LOST_2ND;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_CONN_LOST_3RD;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_DEVICE_NAME;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_READ;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_STATE_CHANGE;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_TOAST;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_UNABLE_CONNECT;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.MESSAGE_WRITE;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.Print_BMP;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.SendDataByte;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.TRAN_TYPE_MOBILETOPUP;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.TRAN_TYPE_NWALLET_TO_MOBILETOPUP;

/**
 * Created by prashant.gadekar on 25-10-2017.
 */

public class ClsBluetoothPrintFrag extends Fragment implements P25Connector.P25ConnectionListener {


    // android built in classes for bluetooth operations
    BluetoothAdapter mBluetoothAdapter;

    private static Spinner mDeviceSp;
    static ArrayList<BluetoothDevice> mDeviceList = null;
    private Button btn_connect1, sendButton;
    private LinearLayout rootLL;
    private TextView tv_printer_name;
    ClsKeyboardUtil objKeyBoardUtil;
    View view;
//    PrashantG 13-05-19 // All lines releted to this are commented
    P25Connector.P25ConnectionListener p25listener;

    Handler handlerPrint = new Handler();
    private static int count = 0;
    ArrayList<Bundle> lstPrint = new ArrayList<>();

    boolean FROM_TOP_UP_PAGER, isEnableScanPrinter, isTimerStart;
    private  Button btn_scan_printer;
    CountDownTimer countDownTimer;
    ArrayAdapter<String> adapter= null;

    @Override
    public void onStart() {
        super.onStart();
        // If Bluetooth is not on, request that it be enabled.
        // setupChat() will then be called during onActivityResult

        if (Singleton.Instance().mBTService == null)
            KeyListenerInit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0: {
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a session
                    /*Amit*/
//                    KeyListenerInit();
                } else {
                    // User did not enable Bluetooth or an error occured

                }
                break;
            }
        }
    }

    private void KeyListenerInit() {
        Singleton.Instance().mBTService = new BluetoothService(getActivity(), mHandler);
    }

    @Override
    public synchronized void onResume() {
        super.onResume();
        String strpage = "ClsBluetoothPrintFrag.java;onResume();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            if (Singleton.Instance().mBTService != null) {
                if (Singleton.Instance().mBTService.getState() == BluetoothService.STATE_NONE) {
                    // Start the Bluetooth services
                    Singleton.Instance().mBTService.start();
                } else {
                    showConnected();
                }
            }
//            BluetoothDevice device = mDeviceList.get(mDeviceSp.getSelectedItemPosition());
//            if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
//                if (ClsBTPrintHandler.isBluetoothConnected()) {
//                    if (ClsBTPrintHandler.isPrinterConnected()) {
//                        showConnected();
//                        mBluetoothAdapter = ClsBTPrintHandler.mBluetoothAdapter;
//                    }
//                }
//            } else {
//                showConnected();
//            }
        } catch (Exception e) {
            strpage = "ClsBluetoothPrintFrag.java;onResume();";
            ConstantDeclaration.writePrintLogs(e, strpage);
        }
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBluetoothReceiver,
                new IntentFilter("BluetoothConn"));
        strpage = "ClsBluetoothPrintFrag.java;onResume();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
        view = inflater.inflate(R.layout.frag_bluetooth_print, container, false);
        p25listener = this;
        viewInitialoize(view);
            mDeviceList= new ArrayList<BluetoothDevice>();
//            Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//            TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//            ImageView toolbarIV = (ImageView) toolbar.findViewById(R.id.toolbarIV);
//            toolbarTV.setVisibility(View.VISIBLE);
//            toolbarIV.setVisibility(View.GONE);
//            toolbarTV.setAllCaps(true);
//            toolbarTV.setText(getResources().getString(R.string.printer_settings));
            ClsKeyboardUtil objKeyBoardUtil;
            objKeyBoardUtil = new ClsKeyboardUtil(getActivity());
            objKeyBoardUtil.setupUI(rootLL);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }catch (Exception e) {
            e.printStackTrace();
        }
        return view;

    }

    private void viewInitialoize(View view) {
        String strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            // more codes will be here
            // we are going to have three buttons for specific functions
            btn_scan_printer = (Button) view.findViewById(R.id.btn_scan_printer);
            sendButton = (Button) view.findViewById(R.id.send);
            //21-3-18
            sendButton.setEnabled(false);
            sendButton.setClickable(false);
            sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
            sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
            //21-3-18
            rootLL = (LinearLayout) view.findViewById(R.id.rootLL);

            mDeviceSp = (Spinner) view.findViewById(R.id.sp_device);
            btn_connect1 = (Button) view.findViewById(R.id.btn_connect1);
            tv_printer_name = view.findViewById(R.id.tv_printer_name);

            if (mConnector == null) {
                ClsBTPrintHandler.initPrinterConnector(p25listener);
            }
            try {
                if (mDeviceList == null || mDeviceList.size() == 0) {
                } else {
                    mDeviceList.clear();
                }
            } catch (Exception e) {
                e.printStackTrace();
                strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();";
                ConstantDeclaration.writePrintLogs(e, strpage);
            }
            btn_connect1.setEnabled(false);
            btn_connect1.setClickable(false);
            btn_connect1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
            btn_connect1.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
//            updateDeviceList();
//            findBT();
            mDeviceSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        if(i == 0){
                            try {
                                btn_connect1.setEnabled(false);
                                btn_connect1.setClickable(false);
                                btn_connect1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                                btn_connect1.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else{
                            try {
                                btn_connect1.setEnabled(true);
                                btn_connect1.setClickable(true);
                                btn_connect1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
                                btn_connect1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
            btn_connect1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View arg0) {
                    String strpage = "ClsBluetoothPrintFrag.java;btn_connect1.onclick;--Enter--";
                    ConstantDeclaration.writePrintLogs(strpage);


                    if(isEnableScanPrinter){
                        return;
                    }

                    if (!isEnableScanPrinter && !btn_connect1.getText().toString().equalsIgnoreCase(getString(R.string.disconnect_printer))) {
                        try {

                            new CountDownTimer(10000, 1000){
                                int counter = 10;

                                public void onTick(long millisUntilFinished){
                                    try {
                                        isTimerStart = isEnableScanPrinter =  true;
                                        btn_scan_printer.setText(getString(R.string.scan_printer)+ "("
                                                +(String.valueOf(counter)+ ")"));
                                        manageScanPrinter(false);

                                        btn_connect1.setEnabled(false);
                                        btn_connect1.setClickable(false);
                                        btn_connect1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                                        btn_connect1.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                                        counter--;
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                                public  void onFinish(){
                                    try {
                                        isTimerStart = isEnableScanPrinter = false;

                                        btn_connect1.setEnabled(true);
                                        btn_connect1.setClickable(true);
                                        btn_connect1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
                                        btn_connect1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));

                                        if (btn_connect1.getText().toString().equalsIgnoreCase(getString(R.string.disconnect_printer))){
                                            manageScanPrinter(false);
                                        }else{
                                            manageScanPrinter(true);
                                        }
                                        btn_scan_printer.setText(getString(R.string.scan_printer));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }

                            }.start();
                        } catch (Exception e) {
                            try {
                                e.printStackTrace();
                                isEnableScanPrinter = false;
                                manageScanPrinter(true);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }




                    if (btn_connect1.getText().toString().equalsIgnoreCase(getString(R.string.disconnect_printer))){
                        if (Singleton.Instance().mBTService != null &&
                                Singleton.Instance().mBTService.getState() == BluetoothService.STATE_CONNECTED) {

                        } else {

                            btn_connect1.setText(getString(R.string.connect));
                            sendButton.setEnabled(false);
                            sendButton.setClickable(false);
                            sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                            return;
                        }
                    }else{

//                        PrashantG 9-05-2019

                        try {
                            if (!mBluetoothAdapter.isEnabled()) {
                                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                                startActivityForResult(enableBluetooth, 0);
                                return;
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }


                    try {
                        if (ConstantDeclaration.gifProgressDialog != null) {
                            if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                                ConstantDeclaration.showGifProgressDialog(getActivity());
                            }
                        } else {
                            ConstantDeclaration.showGifProgressDialog(getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();btn_connect1.onclick;progressdialog;";
                        ConstantDeclaration.writePrintLogs(e, strpage);
                    }
                    try {
                        if (mConnector == null) {
                            ClsBTPrintHandler.initPrinterConnector(p25listener);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();btn_connect1.onclick;ClsBTPrintHandler.initPrinterConnector();";
                        ConstantDeclaration.writePrintLogs(e, strpage);
                    }
                    try {
                        btn_connect1.setEnabled(false);
                        btn_connect1.setClickable(false);
                        btn_connect1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        btn_connect1.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                        handlerPrint.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    btn_connect1.setEnabled(true);
                                    btn_connect1.setClickable(true);
                                    btn_connect1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
                                    btn_connect1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    if (ConstantDeclaration.gifProgressDialog.isShowing())
                                        ConstantDeclaration.gifProgressDialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                try {
//                                    if (Singleton.Instance().mBTService != null &&
//                                            Singleton.Instance().mBTService.getState() == BluetoothService.STATE_CONNECTED) {
//                                        btn_connect1.setText(getString(R.string.disconnect_printer));
//                                        sendButton.setEnabled(false);
//                                        sendButton.setClickable(true);
//                                        sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
//                                        sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
//                                    } else {
//                                        btn_connect1.setText(getString(R.string.connect));
//                                        sendButton.setEnabled(false);
//                                        sendButton.setClickable(false);
//                                        sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
//                                        sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_primary));
//                                    }
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
                            }
                        }, 5000);
                        if (mDeviceList == null || mDeviceList.size() == 0) {
                            new UniversalDialog(getActivity(), null, "", "No device found,Please scan printer.", "OK", "").showAlert();
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            connectPrinter();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();btn_connect1.onclick;connectPrinter();";
                        ConstantDeclaration.writePrintLogs(e, strpage);
                        try {
                            if (ConstantDeclaration.gifProgressDialog.isShowing())
                                ConstantDeclaration.gifProgressDialog.dismiss();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();btn_connect1.onclick;--Exit--\n";
                    ConstantDeclaration.writePrintLogs(strpage);
                }
            });

            // find  bluetooth devices
            btn_scan_printer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String strpage = "ClsBluetoothPrintFrag.java;btn_scan_printer.onclick;--Enter--";
                    ConstantDeclaration.writePrintLogs(strpage);
                    try {
                        btn_scan_printer.setEnabled(false);
                        btn_scan_printer.setClickable(false);
                        handlerPrint.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    btn_scan_printer.setEnabled(true);
                                    btn_scan_printer.setClickable(true);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 5000);
                        if (mDeviceList == null || mDeviceList.size() == 0) {
                        } else {
                            mDeviceList.clear();
                        }
//                        findBT();
                    } catch (Exception e) {
                        e.printStackTrace();
                        strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();btn_scan_printer.onclick;findBT();";
                        ConstantDeclaration.writePrintLogs(e, strpage);
                    }
                    strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();btn_scan_printer.onclick;--Exit--\n";
                    ConstantDeclaration.writePrintLogs(strpage);
                }
            });

            // send data typed by the user to be printed
            sendButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    String strpage = "ClsBluetoothPrintFrag.java;sendButton.onclick;--Enter--";
                    ConstantDeclaration.writePrintLogs(strpage);
                    try {
                        sendButton.setEnabled(false);
                        sendButton.setClickable(false);
                        sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                        handlerPrint.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (btn_connect1.getText().toString().equalsIgnoreCase(getString(R.string.connect))) {
                                        manageScanPrinter(true);
                                        sendButton.setEnabled(false);
                                        sendButton.setClickable(false);
                                        sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                                        sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                                    } else if (Singleton.Instance().mBTService != null &&
                                            Singleton.Instance().mBTService.getState() == BluetoothService.STATE_CONNECTED) {
                                        sendButton.setEnabled(true);
                                        sendButton.setClickable(true);
                                        sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
                                        sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                                        manageScanPrinter(false);
                                    } else {
                                        manageScanPrinter(true);
                                        btn_connect1.setText(getString(R.string.connect));
                                        sendButton.setEnabled(false);
                                        sendButton.setClickable(false);
                                        sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                                        sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                                    }


                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 5000);
//                        printReciept();

//                        PrashantG 17-09-18

                        try {
                            FROM_TOP_UP_PAGER = getArguments().getBoolean("FROM_TOP_UP_PAGER");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        if(FROM_TOP_UP_PAGER){
                            managePrint();
                        }else{
                            try {
                                try {
                                    if (getArguments().getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_LOAN_DISBURSEMENT_AGENT)){
//                                        ConstantDeclaration.printReciept(getActivity(), getArguments());
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
//                                ConstantDeclaration.printReciept(getActivity(), getArguments());
                            } catch (Exception e) {
                                e.printStackTrace();
//                                printReciept();
                            }
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                        strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();sendButton.onclick;printReciept();";
                        ConstantDeclaration.writePrintLogs(ex, strpage);
                    }
                    strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();sendButton.onclick;--Exit--\n";
                    ConstantDeclaration.writePrintLogs(strpage);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        strpage = "ClsBluetoothPrintFrag.java;viewInitialoize();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }

    private void disconnectPrinter() {



//        PrashantG 13-05-19
        try {

            if (btn_connect1.getText().toString().equalsIgnoreCase(getString(R.string.disconnect_printer))) {
                try {
                    if(mmInStream!=null) {
                        mmInStream.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if(mmOutStream!=null) {
                        mmOutStream.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if(mmSocket!=null) {
                        mmSocket.close();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (Singleton.Instance().mBTService.getState() == BluetoothService.STATE_CONNECTED) {
                        Singleton.Instance().mBTService.stop();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            connectPrinter();
                        }
                    },2000);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        PrashantG 13-05-19


    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

//        if (Singleton.Instance().mBTService == null)
//            KeyListenerInit();

    }

    @Override
    public void onStop() {
        super.onStop();
    }

    // this will find a bluetooth printer device
    /*void findBT() {
        String strpage = "ClsBluetoothPrintFrag.java;findBT();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                new UniversalDialog(getActivity(), null, "", "", "OK", "No bluetooth adapter available");
            }
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBluetooth, 0);
            }
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();

            if (pairedDevices != null) {
                mDeviceList.addAll(pairedDevices);
                updateDeviceList();
            }

        } catch (Exception e) {
            e.printStackTrace();
            strpage = "ClsBluetoothPrintFrag.java;findBT();";
            ConstantDeclaration.writePrintLogs(e, strpage);
        }
        strpage = "ClsBluetoothPrintFrag.java;findBT();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }*/

    /*private void updateDeviceList() {
        String strpage = "ClsBluetoothPrintFrag.java;updateDeviceList();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            try {
                try {
                    adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, getArray(mDeviceList));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                mDeviceSp.setAdapter(adapter);
            } catch (Exception e) {
                e.printStackTrace();
            }
            //21-3-18
            //update UI enable connect button
            try {
              if(mDeviceList!=null
                      && !mDeviceList.isEmpty()) {
                  try {
                      mDeviceSp.setSelection(1);
                  } catch (Exception e) {
                      e.printStackTrace();
                  }
                  btn_connect1.setEnabled(true);
                  btn_connect1.setClickable(true);
                  btn_connect1.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
                  btn_connect1.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
              }else{
                  mDeviceSp.setSelection(0);
              }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //21-3-18
        } catch (Exception e) {
            e.printStackTrace();
            strpage = "ClsBluetoothPrintFrag.java;updateDeviceList();";
//            ConstantDeclaration.writePrintLogs(e, strpage);
        }
        strpage = "ClsBluetoothPrintFrag.java;updateDeviceList();--Exit--\n";
//        ConstantDeclaration.writePrintLogs(strpage);
    }*/

    private String[] getArray(ArrayList<BluetoothDevice> data) {
        String[] list = new String[0];

        try {
            list = new String[1];
            list[0] = getActivity().getString(R.string.select_printer);
            if (data == null) return list;

            int size = data.size();
            list = new String[size+1];
            list[0] = getActivity().getString(R.string.select_printer);
            for (int i = 0; i < size; i++) {
                list[i+1] = data.get(i).getName();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }

    /*private void createBond(BluetoothDevice device) throws Exception {
        String strpage = "ClsBluetoothPrintFrag.java;createBond();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            Class<?> cl = Class.forName("android.bluetooth.BluetoothDevice");
            Class<?>[] par = {};
            Method method = cl.getMethod("createBond", par);
            method.invoke(device);
        } catch (Exception e) {
            e.printStackTrace();
            strpage = "ClsBluetoothPrintFrag.java;createBond();";
            ConstantDeclaration.writePrintLogs(e, strpage);
            throw e;
        }
        ClsBTPrintHandler.mBluetoothAdapter = mBluetoothAdapter;//986799507|1010|Ags1234$
//        ClsBTPrintHandler.ClsBTPrintHandler.mConnector = mConnector;
        ClsBTPrintHandler.initPrinterConnector(p25listener);
        Singleton.Instance().mBTDevice = device;
        strpage = "ClsBluetoothPrintFrag.java;createBond();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }*/

    // this will send text data to be printed by the bluetooth printer
    /*private void sendData() throws IOException {
        String strpage = "ClsBluetoothPrintFrag.java;sendData();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            Bundle bundle = getArguments();
            try {
                if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_MOBILETOPUP)) {
                }
            } catch (Exception e) {
                e.printStackTrace();
                strpage = "ClsBluetoothPrintFrag.java;sendData();getarguments();";
                ConstantDeclaration.writePrintLogs(e, strpage);
                try {
                    bundle.putString(CALL_FROM_TXNSUCCESS, bundle.getString("TranType"));
                } catch (Exception e1) {
                    e1.printStackTrace();
                    strpage = "ClsBluetoothPrintFrag.java;sendData();getarguments();bundle.putString();";
                    ConstantDeclaration.writePrintLogs(e, strpage);
                }
            }
            //riteshb 17-11
            if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_MOBILETOPUP)
                    || bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_NWALLET_TO_MOBILETOPUP))
            {
                String msg = "";
                msg += "------------------------------------------\n";
                //riteshb 20-11
                if (!bundle.getString("TELCONAME").equalsIgnoreCase("metfone")) {
//                    ClsBTPrintHandler.mConnector.printCustom(strmobile,0,0);
                    String strReceipt = " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                    String strCustomer = " Telco Name.    : " + bundle.getString("TELCONAME") + "\n";
                    String strmobile = " Mobile No.     : +855 " + bundle.getString("MOBILENUMBER") + "\n";


//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                    String strPaymentAmountTxt = " Payment Amount : ";
                    String strCurrency = "";
                    if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                        strCurrency = "$ ";
                    } else {
                        strCurrency = "KHR ";
                    }
                    String strAmount = bundle.getString("PaymentAmntReferenceNumber", "") + "\n";
                    String strFee = " Fee            : ";
                    String strFee_value = bundle.getString("fee", "") + "\n";
                    String strDate = " Date/Time      : " + bundle.getString("DateTime", "") + "\n";

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strReceipt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strReceipt, 0, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomer.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 0, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strmobile.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strmobile, 0, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strPaymentAmountTxt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strPaymentAmountTxt, 0, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 0, 0);

//                    ClsBTPrintHandler.mConnector.printCustom(strFee,0,0);
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency,1,0);
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value,0,0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strDate.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strDate, 0, 0);


                    if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                        String strpin = getString(R.string.pin_no) + "       : " + bundle.getString("PIN") + "\n";
                        String strexpdate = getString(R.string.expiry_date) + "       : " + bundle.getString("EXPIRYDATE") + "\n";
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strpin.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strpin, 0, 0);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strexpdate.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strexpdate, 0, 0);
                    }
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    Bitmap label_sender = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_bottom);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                    if (label_sender != null) {
                        Print_BMP(label_sender, 390, 170);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

//                    ClsBTPrintHandler.mConnector.printCustom(strFooter,0,1);
                } else {
                    //metfone
                    //print khmer header
                    Bitmap label_fund_success = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_metfone_logo);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 270, 62, false);

                    if (label_fund_success != null) {
                        Print_BMP(label_fund_success, 270, 62);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    String strCurrency = "";
                    if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                        strCurrency = "      $ ";
                    } else {
                        strCurrency = "      KHR ";
                    }
                    String strAmount = bundle.getString("PaymentAmntReferenceNumber", "") + "\n";

                    label_fund_success = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_topup_refill_value);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                    if (label_fund_success != null) {
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                        Print_BMP(label_fund_success, 250, 35);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strAmount.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    label_fund_success = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_topup_refill_code);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                    if (label_fund_success != null) {
                        Print_BMP(label_fund_success, 250, 35);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                        if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                            StringBuilder s;
                            s = new StringBuilder(bundle.getString("PIN"));

                            for (int i = 4; i < s.length(); i += 5) {
                                s.insert(i, " ");
                            }
                            String strpin = "  " + s.toString() + "\n";
//                            String strexpdate = " " + getActivity().getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE") + "\n";
                            Command.ESC_Align[2] = 0x00;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x00;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strpin.getBytes("GBK"));
//                            ClsBTPrintHandler.mConnector.printCustom(strpin, 2, 0);
//                            ClsBTPrintHandler.mConnector.printCustom(strexpdate, 0, 0);
                        }

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                        mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    label_fund_success = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_topup_details);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 410, 220, false);

                    if (label_fund_success != null) {
                        Print_BMP(label_fund_success, 410, 220);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    String strReceipt = " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                    String strDate = " Date/Time      : " + bundle.getString("DateTime", "") + "\n";
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strReceipt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strDate.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    label_fund_success = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_metfone_call);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 165, false);
                    if (label_fund_success != null) {
                        Print_BMP(label_fund_success, 390, 165);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }
//                mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET))
            {
                //transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_fund_trans_is_successful);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    Print_BMP(label_fund_success, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender = "";
                try {
                    strsender = "            +855 " + bundle.getString("SenderNo") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strsender.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strsender, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "            +855 " + bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_receiver_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomer.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)) {

                    String strcollection = "      " + bundle.getString("collectionCode") + "\n";
                    label_sender = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_collection_code);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
                        Print_BMP(label_sender, 250, 35);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strcollection.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strcollection, 3, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "            $ ";
                } else {
                    strCurrency = "            KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 40);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "            +855 " + UserDataPrefrence.getPreference(getActivity().getString(R.string.prefrence_name), getActivity(),
                            ConstantDeclaration.USER_MOBILE, "");
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "\nReceipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate = "Date/Time : " + bundle.getString("DateTime", "") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 390, 170);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_EWALLET))
            {
                //transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_dara_to_dara);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 260, 40, false);

                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    Print_BMP(label_fund_success, 260, 40);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender = "";
                try {
                    strsender = "            +855 " + bundle.getString("SenderNo") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strsender.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strsender, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "            +855 " + bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_dara_receiver);
                label_sender = Bitmap.createScaledBitmap(label_sender, 400, 40, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 400, 40);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomer.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)) {

                    String strcollection = "      " + bundle.getString("collectionCode") + "\n";
                    label_sender = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_collection_code);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
                        Print_BMP(label_sender, 250, 35);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strcollection.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strcollection, 3, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "            $ ";
                } else {
                    strCurrency = "            KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 40);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "            +855 " + UserDataPrefrence.getPreference(getActivity().getString(R.string.prefrence_name), getActivity(),
                            ConstantDeclaration.USER_MOBILE, "");
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "\nReceipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate = "Date/Time : " + bundle.getString("DateTime", "") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 390, 170);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);
            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_NONWALLET))
            {

                //transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_fund_trans_is_successful);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    Print_BMP(label_fund_success, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender = "";
                try {
                    strsender = "            +855 " + bundle.getString("FromNumber") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strsender.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strsender, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "            +855 " + bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_receiver_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomer.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strcollection = "            " + bundle.getString("collectionCode") + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_collection_code);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strcollection.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strcollection, 4, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "            $ ";
                } else {
                    strCurrency = "            KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 40);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "            +855 " + UserDataPrefrence.getPreference(getActivity().getString(R.string.prefrence_name), getActivity(),
                            ConstantDeclaration.USER_MOBILE, "");
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "\nReceipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate = "Date/Time: " + bundle.getString("DateTime", "") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 390, 170);
                    mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            } else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA)
                    || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA_CP)) {
//transfer

                String msg = "";
                msg += "------------------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_bank_header);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 260, 40, false);

                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    Print_BMP(label_fund_success, 260, 40);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strBankName = "";
                try {
                    strBankName = "       " + bundle.getString("BankName") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_bank_name);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strBankName.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBankName, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                JSONArray jsonArray = new JSONArray(bundle.getString("json"));
                JSONObject jsonObject = jsonArray.getJSONObject(0);

                String strCustAccName = "";
                if (jsonObject.getString("ACCOUNTNAME").length() <= 18) {
                    strCustAccName = "       " + jsonObject.getString("ACCOUNTNAME") + "\n";
                } else {
                    strCustAccName = jsonObject.getString("ACCOUNTNAME") + "\n";
                }

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_bank_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustAccName.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccName, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strCustAccNo = "       " + bundle.getString("AccountNumber") + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_acc_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustAccNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccNo, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                if (!bundle.getString("CustomerMobile").isEmpty()) {
                    String strCustomerMob = "    +855 " + bundle.getString("CustomerMobile", "") + "\n";
                    label_sender = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_cust_no_bank);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
                        Print_BMP(label_sender, 250, 35);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustomerMob.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCustomerMob, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("Currency").equalsIgnoreCase("USD")) {
                    strCurrency = "            $ ";
                } else {
                    strCurrency = "            KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("Amount", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("ProcessingFee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 35);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 250, 40);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "            +855 " + UserDataPrefrence.getPreference(getActivity().getString(R.string.prefrence_name), getActivity(),
                            ConstantDeclaration.USER_MOBILE, "");
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "\nReceipt No.: " + jsonObject.getString("RECEIPTNO") + "\n";
                String strDate = "Date/Time: " + jsonObject.getString("TRANSACTIONDATEFORMATED") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
                    Print_BMP(label_sender, 390, 170);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            } else {
                //transfer

                String strTitle = "DaraPay";

                String msg = "";
                msg += "******************************************\n\n";
                String strReceipt = " Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strCustomer = " Customer No.   : +855 " + bundle.getString("ToNumber", "") + "\n";
//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt = " Payment Amount : ";
                String strCurrency = "";
                if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                } else {
                    strCurrency = "KHR ";
                }

                String strAmount = bundle.getString("PaymentAmntReferenceNumber", "") + "\n";
                String strFee = " Fee            : ";
                String strFee_value = bundle.getString("fee", "") + "\n";
                String strDate = " Date/Time      : " + bundle.getString("DateTime", "") + "\n";
                String strFooter = "\n******************************************\n";
                strFooter += "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";


                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strCustomer.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strCustomer, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strPaymentAmountTxt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strPaymentAmountTxt, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strCurrency.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strCurrency, 1, 0);
//            ClsBTPrintHandler.mConnector.printText(Ux17DB);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strAmount.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strAmount, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strFee.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strFee, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strCurrency.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strCurrency, 1, 0);
//            ClsBTPrintHandler.mConnector.printText(Ux17DB);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strFee_value.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strFee_value, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strFooter.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strFooter, 0, 1);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }


        } catch (Exception e) {
            e.printStackTrace();
            mConnector.printCustom("\n", 0, 0);
            strpage = "ClsBluetoothPrintFrag.java;sendData();";
            ConstantDeclaration.writePrintLogs(e, strpage);
        }
        strpage = "ClsBluetoothPrintFrag.java;sendData();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }*/


    private void printReciept() {
        String strpage = "ClsBluetoothPrintFrag.java;printReciept();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            Bitmap bmp = null;
            Bitmap result = null;
            //6-3-18
            //riteshb
            //added new image to non-dara to non-dara transaction
            try {
                Bundle bundle = getArguments();
                if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)
                        ) {
                    bmp = BitmapFactory.decodeResource(getResources(),
                            R.drawable.iv_print_header_new);
//                            R.drawable.darapay_print_header_new);
                    result = Bitmap.createScaledBitmap(bmp, 261, 55, false);
                    if (bmp != null) {
                        mConnector.printPhoto(result);
                        mConnector.printCustom("\n", 0, 0);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //6-3-18
            bmp = BitmapFactory.decodeResource(getResources(),
                    R.drawable.iv_print_header_logo);
            result = Bitmap.createScaledBitmap(bmp, 378, 90, false);
            if (bmp != null) {
                ConstantDeclaration.Print_BMP(result, 378, 90);
//                sendData();
//  ConstantDeclaration.SendDataString("Testing");

                if(ClsBTPrintHandler.mConnector==null){
                    ClsBTPrintHandler.initPrinterConnector(p25listener);
                    ClsBTPrintHandler.mConnector.printPhoto(result);
//                    sendData();
                }else {
                    ClsBTPrintHandler.mConnector.printPhoto(result);
//                    sendData();
                }
            } else {
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (e.toString().contains("Socket is not connected") || btn_connect1.getText().equals("Connect")) {
                new UniversalDialog(getActivity(), null, "", "Printer is not connected", "OK", "").showAlert();
            } else {
            }
            strpage = "ClsBluetoothPrintFrag.java;printReceipt();";
            ConstantDeclaration.writePrintLogs(e, strpage);
        }
        strpage = "ClsBluetoothPrintFrag.java;printReceipt();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }

    private void connectPrinter() {
        String strpage = "ClsBluetoothPrintFrag.java;connectPrinter();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        if (mDeviceList == null || mDeviceList.size() == 0) {
            new UniversalDialog(getActivity(), null, "", "No device found, Please scan printer."
                    , "OK", "").showAlert();
            strpage = "ClsBluetoothPrintFrag.java;connectPrinter();no device found;--Exit--\n";
            ConstantDeclaration.writePrintLogs(strpage);
            return;
        }
        if(mDeviceSp.getSelectedItemPosition()==0){
            return;
        }
        BluetoothDevice device = mDeviceList.get(mDeviceSp.getSelectedItemPosition()-1);
        // Attempt to connect to the device
        if (Singleton.Instance().mBTService != null) {
            if (Singleton.Instance().mBTService.getState() != BluetoothService.STATE_CONNECTED) {
                Singleton.Instance().mBTService.connect(device);
                count++;
                ClsBTPrintHandler.mBluetoothAdapter = mBluetoothAdapter;
                Singleton.Instance().mBTDevice = device;
            } else {
                Singleton.Instance().mBTService.stop();
                count = 0;
            }
        } else {
            KeyListenerInit();
            if (Singleton.Instance().mBTService.getState() != BluetoothService.STATE_CONNECTED) {
                Singleton.Instance().mBTService.connect(device);
                count++;
                ClsBTPrintHandler.mBluetoothAdapter = mBluetoothAdapter;
                Singleton.Instance().mBTDevice = device;
            } else {
                Singleton.Instance().mBTService.stop();
                count = 0;
            }
        }

        strpage = "ClsBluetoothPrintFrag.java;connectPrinter();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }


    private void showDisonnected() {

        manageScanPrinter(true);
        if (count != 0) {
            return;
        }
        try {
            if (Singleton.Instance().mBTService != null &&
                    Singleton.Instance().mBTService.getState() != BluetoothService.STATE_CONNECTED) {
                btn_connect1.setText(getString(R.string.connect));
                try {
                    mDeviceSp.setEnabled(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    sendButton.setEnabled(false);
                    sendButton.setClickable(false);
                    sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{

//                PrashantG 08-05-2019

                btn_connect1.setText(getString(R.string.connect));
                try {
                    mDeviceSp.setEnabled(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    sendButton.setEnabled(false);
                    sendButton.setClickable(false);
                    sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg));
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                PrashantG 08-05-2019

            }
        } catch (Exception e) {
            e.printStackTrace();
            btn_connect1.setText(getString(R.string.connect));
        }

        //21-3-18

        //21-3-18
    }

    private void showConnected() {
        try {

            if (Singleton.Instance().mBTService.getState() == BluetoothService.STATE_CONNECTED) {

                manageScanPrinter(false);
                count = 1;
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        count = 0;
                    }
                }, 3000);
                btn_connect1.setText(getString(R.string.disconnect_printer));
                try {
                    mDeviceSp.setEnabled(false);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    sendButton.setEnabled(true);
                    sendButton.setClickable(true);
                    sendButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
                    sendButton.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                manageScanPrinter(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
//            PrashantG 9-05-2019
            try {
                manageScanPrinter(true);
//                Singleton.Instance().mBTService.setStateBT(BluetoothService.STATE_NONE);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

//            PrashantG 9-05-2019

        }
    }

    private void showPrinterName(boolean isShowPrinterName) {

        try {
            if(isShowPrinterName){
                tv_printer_name.setText(BluetoothService.deviceName);
                tv_printer_name.setVisibility(View.VISIBLE);
                mDeviceSp.setVisibility(View.GONE);
            }else{
                tv_printer_name.setText(BluetoothService.deviceName);
                tv_printer_name.setVisibility(View.GONE);
                mDeviceSp.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


//    @Override
//    public void onResume() {
//        super.onResume();
//        String strpage = "ClsBluetoothPrintFrag.java;onResume();--Enter--";
//        ConstantDeclaration.writePrintLogs(strpage);
//        try {
//            BluetoothDevice device = mDeviceList.get(mDeviceSp.getSelectedItemPosition());
//            if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
//                if (ClsBTPrintHandler.isBluetoothConnected()) {
//                    if (ClsBTPrintHandler.isPrinterConnected()) {
//                        showConnected();
//                        mBluetoothAdapter = ClsBTPrintHandler.mBluetoothAdapter;
//                    }
//                }
//            } else {
//                showConnected();
//            }
//        } catch (Exception e) {
//            strpage = "ClsBluetoothPrintFrag.java;onResume();";
//            ConstantDeclaration.writePrintLogs(e, strpage);
//        }
//        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(mBluetoothReceiver,
//                new IntentFilter("BluetoothConn"));
//        strpage = "ClsBluetoothPrintFrag.java;onResume();--Exit--\n";
//        ConstantDeclaration.writePrintLogs(strpage);
//    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(mBluetoothReceiver);
    }

    @Override
    public void onStartConnecting() {
        String strpage = "ClsBluetoothPrintFrag.java;onStartConnecting();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            if (ConstantDeclaration.gifProgressDialog != null) {
                if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } else {
                ConstantDeclaration.showGifProgressDialog(getActivity());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        strpage = "ClsBluetoothPrintFrag.java;onStartConnecting();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }

    @Override
    public void onConnectionCancelled() {
        String strpage = "ClsBluetoothPrintFrag.java;onConnectionCancelled();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            if (ConstantDeclaration.gifProgressDialog.isShowing())
                ConstantDeclaration.gifProgressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        strpage = "ClsBluetoothPrintFrag.java;onConnectionCancelled();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }

    @Override
    public void onConnectionSuccess() {
        String strpage = "ClsBluetoothPrintFrag.java;onConnectionSuccess();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            if (ConstantDeclaration.gifProgressDialog.isShowing())
                ConstantDeclaration.gifProgressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        showConnected();
        strpage = "ClsBluetoothPrintFrag.java;onConnectionSuccess();--EXit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }

    @Override
    public void onConnectionFailed(String error) {
        String strpage = "ClsBluetoothPrintFrag.java;onConnectionFailed();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            if (ConstantDeclaration.gifProgressDialog.isShowing())
                ConstantDeclaration.gifProgressDialog.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            new UniversalDialog(getActivity(), null, "", "Connection failed.", "OK", "").showAlert();
        } catch (Exception e) {
            e.printStackTrace();
        }
        strpage = "ClsBluetoothPrintFrag.java;onConnectionFailed();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }

    @Override
    public void onDisconnected() {
        showDisonnected();
    }

    // Our handler for received Intents. This will be called whenever an Intent
// with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("msg");
            boolean is_connected = intent.getBooleanExtra("is_connected", false);
            Log.d("receiver", "Got message: " + message);
            try {
                count = 0;
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                if (is_connected) {
                    showConnected();
                } else {
                    showDisonnected();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            try {
                /*switch (msg.what) {
                    *//*case MESSAGE_STATE_CHANGE:
                        switch (msg.arg1) {
                            case BluetoothService.STATE_CONNECTED:
                                showConnected();
                                break;
                            case BluetoothService.STATE_CONNECTING:
//                                showDisonnected();
                                //                            mTitle.setText(R.string.title_connecting);
                                break;
                            case BluetoothService.STATE_LISTEN:
                            case BluetoothService.STATE_NONE:
                                //                            mTitle.setText(R.string.title_not_connected);
                                showDisonnected();
                                break;
                        }*//*
                        break;
                    *//*case MESSAGE_WRITE:
                        break;
                    case MESSAGE_READ:
                        break;
                    case MESSAGE_DEVICE_NAME:
                        // save the connected device's name
                        //                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                        //                    Toast.makeText(getApplicationContext(), "Connected to " + mConnectedDeviceName, Toast.LENGTH_SHORT)
                        //                            .show();
                        showConnected();
                        break;
                    case MESSAGE_TOAST:
                        //                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                        showDisonnected();
                        break;
                    case MESSAGE_CONN_LOST_1ST:
                        //                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                        showDisonnected();
                    {
                        Handler handler = new Handler();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (Singleton.Instance().mBTService.getState() != BluetoothService.STATE_CONNECTED) {
                                        Singleton.Instance().mBTService.connect_2nd(Singleton.Instance().mBTDevice);
                                        ClsBTPrintHandler.mBluetoothAdapter = mBluetoothAdapter;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    if (ConstantDeclaration.gifProgressDialog != null) {
                                        if (ConstantDeclaration.gifProgressDialog.isShowing()) {
                                            ConstantDeclaration.gifProgressDialog.dismiss();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 1000);
                    }*//*
                    break;
                    *//*case MESSAGE_CONN_LOST_2ND:
                        //                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                        showDisonnected();
                    {
                        Handler handler = new Handler();

                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    if (Singleton.Instance().mBTService.getState() != BluetoothService.STATE_CONNECTED) {
                                        Singleton.Instance().mBTService.connect_3rd(Singleton.Instance().mBTDevice);
                                        ClsBTPrintHandler.mBluetoothAdapter = mBluetoothAdapter;
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    if (ConstantDeclaration.gifProgressDialog != null) {
                                        if (ConstantDeclaration.gifProgressDialog.isShowing()) {
                                            ConstantDeclaration.gifProgressDialog.dismiss();
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }, 1000);
                    }
                    break;
                    case MESSAGE_CONN_LOST_3RD:
                        //                    Toast.makeText(getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                        showDisonnected();
                        break;
                    case MESSAGE_CONNECTION_LOST: // 蓝牙已断开连接
                        //                    Toast.makeText(getApplicationContext(), "Device connection was lost", Toast.LENGTH_SHORT).show();
                        showDisonnected();
                        count = 0;
                        break;
                    case MESSAGE_UNABLE_CONNECT: // 无法连接设备
                        //                    Toast.makeText(getApplicationContext(), "Unable to connect device", Toast.LENGTH_SHORT).show();
                        showDisonnected();
                        break;*//*
                }*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };


    void managePrint(){

        try {
            Bundle bundleMain = new Bundle();
            bundleMain = getArguments();
            JSONObject jsonObject = new JSONObject(bundleMain.getString("RESPONSE"));

            JSONArray jsonArray = new JSONArray(jsonObject.getString("RESPONSEMSG"));
            JSONObject jsonObj = jsonArray.getJSONObject(0);
            JSONArray jsonPINLIST = new JSONArray(jsonObj.getString("PINLIST"));

            lstPrint =  new ArrayList<>();
            if (jsonPINLIST.length() > 0) {

                for (int i = 0; i < jsonPINLIST.length(); i++) {

                    Bundle bundle = (Bundle) bundleMain.clone();

//                    TopUpSuccessFragment topUpSuccessFragment = new TopUpSuccessFragment();

    //                        bundle.putString(CALL_FROM_TXNSUCCESS, strCallFrom);
                    bundle.putBoolean("is_success", true);
                    bundle.putString("CURRENCY", jsonObj.getString("CURRENCYTYPE"));
                    bundle.putString("FragmentTitle", getResources().getString(R.string.services));
                    bundle.putString("ReceiptNoError", jsonPINLIST.getJSONObject(i).getString("RECEIPTNO"));
                    bundle.putString("FromNumber", jsonObj.getString("FROMID"));
                    bundle.putString("ToNumber", jsonObj.getString("MOBILENUMBER"));
                    bundle.putString("PaymentAmntReferenceNumber", jsonObj.getString("AMOUNT"));
                    bundle.putString("fee", jsonObj.getString("CUSTOMERFEE"));
                    bundle.putString("DateTime", jsonObj.getString("TRANSACTIONDATEFORMATED"));
                    bundle.putString("SuccessTextMsg", getString(R.string.tran_success));
                    bundle.putString("TELCONAME", jsonObj.getString("TELCONAME"));
                    bundle.putString("MOBILENUMBER", jsonObj.getString("MOBILENUMBER"));
    //                        bundle.putString("TELCOID", strTelcoId);

                    bundle.putBoolean("SHOW_PIN_EXP_FLAG", true);
                    bundle.putString("PIN", jsonPINLIST.getJSONObject(i).getString("DECRYPTPIN"));
                    bundle.putString("EXPIRYDATE", jsonPINLIST.getJSONObject(i).getString("EXPIRYDATE"));
                    bundle.putString("SERIAL_NO", jsonPINLIST.getJSONObject(i).getString("SERIALNO"));

                    bundle.putString("PINTRANSUBTYPE", jsonObj.getString("PINTRANSUBTYPE"));
                    bundle.putString("PINAMOUNT", jsonObj.getString("PINAMOUNT"));
                    bundle.putString("PINQUANTITY", jsonObj.getString("PINQUANTITY"));

//                    topUpSuccessFragment.setArguments(bundle);

                    lstPrint.add(bundle);

                }

                if (!lstPrint.isEmpty()) {
                    new AsyncPrintReceipt().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }



    public class AsyncPrintReceipt extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... voids) {


            for (Bundle bundle:lstPrint) {
//                ConstantDeclaration.printReciept(getActivity(), bundle);
            }


            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String bitmap) {
            super.onPostExecute(bitmap);

        }
    }

    private  void manageScanPrinter(boolean setEnable){

        try {
            if (setEnable && !isEnableScanPrinter) {
                try {
                    try {
                        mDeviceSp.setEnabled(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    btn_scan_printer.setEnabled(true);
                    btn_scan_printer.setClickable(true);
                    btn_scan_printer.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
                    btn_scan_printer.setTextColor(ContextCompat.getColor(getActivity(), R.color.white));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {

                try {
                    try {
                        mDeviceSp.setEnabled(false);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    btn_scan_printer.setEnabled(false);
                    btn_scan_printer.setClickable(false);
                    btn_scan_printer.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    btn_scan_printer.setTextColor(ContextCompat.getColor(getActivity(), R.color.grey_bg1));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
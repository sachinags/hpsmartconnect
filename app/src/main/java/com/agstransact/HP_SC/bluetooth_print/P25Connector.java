package com.agstransact.HP_SC.bluetooth_print;


import android.bluetooth.BluetoothDevice;
//import android.bluetooth.BluetoothSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;


//import com.agstransact.HP_SC.common.ConstantDeclaration;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.utils.ConstantDeclaration;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

/**
 * P25 printer connection class.
 *
 * @author Lorensius W. L. T <lorenz@londatiga.net>
 */
public class P25Connector {
    private BluetoothSocket mSocket;
    //	private BluetoothSocketWrapper mSocket;
    private OutputStream mOutputStream;

    private ConnectTask mConnectTask;
    private P25ConnectionListener mListener;

    private boolean mIsConnecting = false;

    private static final String TAG = "P25";
    private static final String SPP_UUID = "00001101-0000-1000-8000-00805F9B34FB";

    public P25Connector(P25ConnectionListener listener) {
        mListener = listener;
    }

    public boolean isConnecting() {
        return mIsConnecting;
    }

    public boolean isConnected() {
        return (mSocket == null) ? false : true;
    }

    public void connect(BluetoothDevice device) throws P25ConnectionException {
        if (mIsConnecting && mConnectTask != null) {
            throw new P25ConnectionException("Connection in progress");
        }

        if (mSocket != null) {
            throw new P25ConnectionException("Socket already connected");
//			disconnect();
        }

        try {
            (mConnectTask = new ConnectTask(device)).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() throws P25ConnectionException {
        if (mSocket == null) {
            throw new P25ConnectionException("Socket is not connected");
        }

        try {
            mSocket.close();

            mSocket = null;

            mListener.onDisconnected();
        } catch (IOException e) {
            throw new P25ConnectionException(e.getMessage());
        }
    }

    public void cancel() throws P25ConnectionException {
        if (mIsConnecting && mConnectTask != null) {
            mConnectTask.cancel(true);
        } else {
            throw new P25ConnectionException("No connection is in progress");
        }
    }

    public void sendData(byte[] msg) throws P25ConnectionException {
        if (mSocket == null) {
            throw new P25ConnectionException("Socket is not connected, try to call connect() first");
        }
        try {
            mOutputStream.write(msg);
            mOutputStream.flush();

            Log.i(TAG, StringUtil.byteToString(msg));
        } catch (Exception e) {
            throw new P25ConnectionException(e.getMessage());
        }
    }

    public interface P25ConnectionListener {
        public abstract void onStartConnecting();

        public abstract void onConnectionCancelled();

        public abstract void onConnectionSuccess();

        public abstract void onConnectionFailed(String error);

        public abstract void onDisconnected();
    }

    public interface P25ConnectionListener2 {
        public abstract void onStartConnecting();

        public abstract void onConnectionCancelled();

        public abstract void onConnectionSuccess();

        public abstract void onConnectionFailed(String error);

        public abstract void onDisconnected();
    }

    public class ConnectTask extends AsyncTask<URL, Integer, Long> {
        BluetoothDevice device;
        String error = "";

        public ConnectTask(BluetoothDevice device) {
            this.device = device;
        }

        protected void onCancelled() {
            mIsConnecting = false;

            mListener.onConnectionCancelled();
        }

        protected void onPreExecute() {
            mListener.onStartConnecting();
            mIsConnecting = true;
            //13-3-18
            Context mcontext;
            try {
                if (MainApplication.mDashBoardContext != null) {
                    mcontext = MainApplication.mDashBoardContext;
                } else {
                    mcontext = MainApplication.getContext();
                }
                try {
                    if (ConstantDeclaration.gifProgressDialog != null) {
                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                            ConstantDeclaration.showGifProgressDialog(mcontext);
                        }
                    } else {
                        ConstantDeclaration.showGifProgressDialog(mcontext);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //13-3-18
        }

        protected Long doInBackground(URL... urls) {
            long result = 0;

            try {
//            	if(mSocket==null) {
                mSocket = device.createRfcommSocketToServiceRecord(UUID.fromString(SPP_UUID));
//					mSocket = device.createInsecureRfcommSocketToServiceRecord(UUID.fromString(SPP_UUID));
//				}
                mSocket.connect();
                mOutputStream = mSocket.getOutputStream();
                result = 1;
                //20-3-18
                String strpage = "P25Connector.java;doinbackground();1st try;socket connection success";
                ConstantDeclaration.writePrintLogs(strpage);
                //20-3-18

            } catch (Exception e) {
                e.printStackTrace();
                error = e.getMessage();
                //20-3-18
                String strpage = "P25Connector.java;doinbackground();";
                ConstantDeclaration.writePrintLogs(e, strpage);
                //20-3-18
                //12-3-18
                //printer connection failed issue
                //java.io.IOException: read failed, socket might closed or timeout, read ret: -1
//				https://stackoverflow.com/a/25846024/7790252
//				https://stackoverflow.com/a/25647197/7790252
//				https://stackoverflow.com/a/25846024/7790252
//				https://stackoverflow.com/questions/25698585/bluetooth-connection-failed-java-io-ioexception-read-failed-socket-might-clos
                try {
                    mSocket = (BluetoothSocket) device.getClass().getMethod("createRfcommSocket",
                            new Class[]{int.class}).invoke(device, 1);
                    mSocket.connect();
                    result = 1;
                    //20-3-18
                    strpage = "P25Connector.java;doinbackground();2nd try;socket connection success";
                    ConstantDeclaration.writePrintLogs(strpage);
                    //20-3-18
                } catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                    //20-3-18
                    strpage = "P25Connector.java;doinbackground();2nd try;";
                    ConstantDeclaration.writePrintLogs(e, strpage);
                    //20-3-18

                } catch (InvocationTargetException e1) {
                    e1.printStackTrace();
                    //20-3-18
                    strpage = "P25Connector.java;doinbackground();2nd try;";
                    ConstantDeclaration.writePrintLogs(e, strpage);
                    //20-3-18

                } catch (NoSuchMethodException e1) {
                    e1.printStackTrace();
                    //20-3-18
                    strpage = "P25Connector.java;doinbackground();2nd try;";
                    ConstantDeclaration.writePrintLogs(e, strpage);
                    //20-3-18

                } catch (IOException e1) {
                    e1.printStackTrace();
                    //20-3-18
                    strpage = "P25Connector.java;doinbackground();2nd try;";
                    ConstantDeclaration.writePrintLogs(e, strpage);
                    //20-3-18

                } catch (Exception e1) {
                    e1.printStackTrace();
                    //20-3-18
                    strpage = "P25Connector.java;doinbackground();2nd try;";
                    ConstantDeclaration.writePrintLogs(e, strpage);
                    //20-3-18
                }
                //12-3-18
            }

            return result;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
            mIsConnecting = false;
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            try {
                if (mSocket != null && result == 1) {
                    mListener.onConnectionSuccess();
                } else {
                    mListener.onConnectionFailed("Connection failed " + error);
                    ClsBTPrintHandler.mConnector = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //print custom
    public void printCustom(String msg, int size, int align) {
        //Print config "mode"
        byte[] cc = new byte[]{0x1B, 0x21, 0x01};  // 0- small size text
        byte[] cc1 = new byte[]{0x1B, 0x21, 0x00};  // 1- normal size text
        byte[] bb = new byte[]{0x1B, 0x21, 0x08};  // 2- only bold text
        byte[] bb2 = new byte[]{0x1B, 0x21, 0x20}; // 3- bold with medium text
        byte[] bb3 = new byte[]{0x1B, 0x21, 0x10}; // 4- bold with large text
        try {
            switch (size) {
                case 0:
                    mOutputStream.write(cc);
                    break;
                case 1:
                    mOutputStream.write(cc1);
                    break;
                case 2:
                    mOutputStream.write(bb);
                    break;
                case 3:
                    mOutputStream.write(bb2);
                    break;
                case 4:
                    mOutputStream.write(bb3);
                    break;
            }

            switch (align) {
                case 0:
                    //left align
                    mOutputStream.write(PrinterCommands.ESC_ALIGN_LEFT);
                    break;
                case 1:
                    //center align
                    mOutputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                    break;
                case 2:
                    //right align
                    mOutputStream.write(PrinterCommands.ESC_ALIGN_RIGHT);
                    break;
            }
            mOutputStream.write(msg.getBytes());
            mOutputStream.flush();
//			mOutputStream.write(PrinterCommands.DLE);
            //outputStream.write(cc);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //print photo
    public void printPhoto(Bitmap img1) {
        try {
//			Bitmap bmp = BitmapFactory.decodeResource(getResources(),
//					img);
            Bitmap bmp = img1;
            if (bmp != null) {
                byte[] command = Utils.decodeBitmap(bmp);
                mOutputStream.write(PrinterCommands.ESC_ALIGN_CENTER);
                printText(command);
            } else {
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("PrintTools", "the file isn't exists");
        }
    }

    //print byte[]
    public void printText(byte[] msg) {
        try {
            // Print normal text
            mOutputStream.write(msg);
            printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //print byte[]
    public void printTextNew(byte[] msg) {
        try {
            // Print normal text
            mOutputStream.write(msg);
            //printNewLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //print new line
    private void printNewLine() {
        try {
            mOutputStream.write(PrinterCommands.FEED_LINE);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static interface BluetoothSocketWrapper {

        InputStream getInputStream() throws IOException;

        OutputStream getOutputStream() throws IOException;

        String getRemoteDeviceName();

        void connect() throws IOException;

        String getRemoteDeviceAddress();

        void close() throws IOException;

        BluetoothSocket getUnderlyingSocket();

    }


    public static class NativeBluetoothSocket implements BluetoothSocketWrapper {

        private BluetoothSocket socket;

        public NativeBluetoothSocket(BluetoothSocket tmp) {
            this.socket = tmp;
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return socket.getInputStream();
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return socket.getOutputStream();
        }

        @Override
        public String getRemoteDeviceName() {
            return socket.getRemoteDevice().getName();
        }

        @Override
        public void connect() throws IOException {
            socket.connect();
        }

        @Override
        public String getRemoteDeviceAddress() {
            return socket.getRemoteDevice().getAddress();
        }

        @Override
        public void close() throws IOException {
            socket.close();
        }

        @Override
        public BluetoothSocket getUnderlyingSocket() {
            return socket;
        }

    }

    public class FallbackBluetoothSocket extends NativeBluetoothSocket {

        private BluetoothSocket fallbackSocket;

        public FallbackBluetoothSocket(BluetoothSocket tmp) throws FallbackException {
            super(tmp);
            try {
                Class<?> clazz = tmp.getRemoteDevice().getClass();
                Class<?>[] paramTypes = new Class<?>[]{Integer.TYPE};
                Method m = clazz.getMethod("createRfcommSocket", paramTypes);
                Object[] params = new Object[]{Integer.valueOf(1)};
                fallbackSocket = (BluetoothSocket) m.invoke(tmp.getRemoteDevice(), params);
            } catch (Exception e) {
                throw new FallbackException(e);
            }
        }

        @Override
        public InputStream getInputStream() throws IOException {
            return fallbackSocket.getInputStream();
        }

        @Override
        public OutputStream getOutputStream() throws IOException {
            return fallbackSocket.getOutputStream();
        }


        @Override
        public void connect() throws IOException {
            fallbackSocket.connect();
        }


        @Override
        public void close() throws IOException {
            fallbackSocket.close();
        }

    }

    public static class FallbackException extends Exception {

        /**
         *
         */
        private static final long serialVersionUID = 1L;

        public FallbackException(Exception e) {
            super(e);
        }

    }

}
package com.agstransact.HP_SC.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.DrawerMenuModel;

import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<DrawerMenuModel> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<DrawerMenuModel, List<String>> _listDataChild;

    public ExpandableListAdapter(Context context, List<DrawerMenuModel> listDataHeader,
                                 HashMap<DrawerMenuModel, List<String>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_child_item, null);
        }

        TextView txtListChild = (TextView) convertView
                .findViewById(R.id.expandedListItem);

        txtListChild.setText(childText);
        txtListChild.setSelected(true);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        try {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    public static class GroupViewHolder {
        TextView lblListHeader;
        ImageView collapageIV;
        ImageView menuIcon;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        GroupViewHolder groupViewHolder;
        DrawerMenuModel drawerMenuModel = (DrawerMenuModel) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.drawer_group_item, null);
            groupViewHolder = new GroupViewHolder();
            groupViewHolder.lblListHeader = (TextView) convertView
                    .findViewById(R.id.listTitle);
            groupViewHolder.collapageIV = (ImageView) convertView.findViewById(R.id.collapase_icon);

            groupViewHolder.menuIcon = (ImageView) convertView.findViewById(R.id.group_icon);
            convertView.setTag(groupViewHolder);
        } else {
            groupViewHolder = (GroupViewHolder) convertView.getTag();
        }


        groupViewHolder.lblListHeader.setText(drawerMenuModel.getMenuName());
        groupViewHolder.lblListHeader.setSelected(true);
        groupViewHolder.menuIcon.setImageResource(drawerMenuModel.getMenuIcon());

        /*group indicator will show only if group has the child */
        if (getChildrenCount(groupPosition) > 0) {
            /*collapse image will be visible if have child */
            groupViewHolder.collapageIV.setVisibility(View.VISIBLE);
            if (isExpanded) {
                /*at time of expand image */
                groupViewHolder.collapageIV.setImageResource(R.drawable.down_arrow);
            } else {
                /*at time of collapse*/
                groupViewHolder.collapageIV.setImageResource(R.drawable.right_arrow);
            }

        } else {
            /*collapse image will be invisible if does not have child */
            groupViewHolder.collapageIV.setVisibility(View.INVISIBLE);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}

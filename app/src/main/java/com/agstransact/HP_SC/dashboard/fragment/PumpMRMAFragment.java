package com.agstransact.HP_SC.dashboard.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Satish Patel on 07,April,2021
 */
public class PumpMRMAFragment extends Fragment implements AlertDialogInterface {

    private Spinner spinner_select_pump,spinner_select_RO,spinner_status,spinner_manager,
            spinner_zone,spinner_region,spinner_sales_area,spinner_RO,spinner_HQ;
    private RelativeLayout fromDate,toDate,layout_spinner_select_pump;
    private TextView fromTimeMM,fromTimeSS,toTimeMM,toTimeSS,tv_from_date,tv_to_date,tv_default_ro_value,tv_selected_pump,tv_hint_Approver;
    private EditText et_requestor_remark,et_remarks,et_roomms_complaint_id,et_du_serial_number;
    private Button submitBT,cancelBtnFilter,submitBtnFilter;
    private String userRole="";
    private LinearLayout ll_filter_layout,ll_mainlayout,ll_other_user;
    private RelativeLayout ll_ro_list,ll_hq_layout;
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    private ImageView btn_filter;
    String error="", respCode="",selectedPump="",selectedSA="",selectedPumpSL="",selectedRO="",selectedStatus,selectedHQ="",selectedZone="",selectedRegion="",selectedSalesArea="",selectedROFilter;
    private ArrayList<String> pumpList = new ArrayList<>();
    private ArrayList<String> pumpIdList = new ArrayList<>();
    private ArrayList<String> pumpSLList = new ArrayList<>();
    private ArrayList<String> sAMList = new ArrayList<>();
    private ArrayList<String> sAMIdList = new ArrayList<>();
    private ArrayList<String> ROCodeList = new ArrayList<>();
    private ArrayList<String> RONameList = new ArrayList<>();
    private ArrayList<String> RMNameList = new ArrayList<>();
    private ArrayList<String> RMIdList = new ArrayList<>();
    private ArrayList<String> statusList = new ArrayList<>();
    private ArrayList<String> hqCodeList = new ArrayList<>();
    private ArrayList<String> hqNameList = new ArrayList<>();

    //filter data
    private ArrayList<String> zoneList = new ArrayList<>();
    private ArrayList<String> regionList = new ArrayList<>();
    private ArrayList<String> salesAreaList = new ArrayList<>();
    private ArrayList<String> ROCodeFilterList = new ArrayList<>();
    private ArrayList<String> RONameFilterList = new ArrayList<>();
    Toolbar toolbar;
    Bundle bundle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pump_mr_ma_fragment_new, container, false);

        bundle = getArguments();
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);

        findViewById(view);

        toolbarTV.setText(getResources().getString(R.string.manager_approval));

        alertDialogInterface = (AlertDialogInterface) this;

        setLayout();

        setListeners();
        try {
            if(bundle.getString("FilteredFragment").equalsIgnoreCase("LD_Pump_MR_MA")){
//                tv_default_ro_value.setText( UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredROName",""));
//                tv_default_ro_value.setSelected(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void findViewById(View view){
        ll_ro_list = (RelativeLayout) view.findViewById(R.id.ll_ro_list);
        ll_hq_layout = (RelativeLayout) view.findViewById(R.id.ll_hq_layout);
        ll_filter_layout = (LinearLayout) view.findViewById(R.id.ll_filter_layout);
        ll_mainlayout = (LinearLayout) view.findViewById(R.id.ll_mainlayout);
        ll_other_user = (LinearLayout) view.findViewById(R.id.ll_other_user);
        btn_filter = (ImageView) view.findViewById(R.id.btn_filter);
        submitBT = (Button) view.findViewById(R.id.submitBT);
        cancelBtnFilter = (Button) view.findViewById(R.id.cancelBtnFilter);
        submitBtnFilter = (Button) view.findViewById(R.id.submitBtnFilter);
        et_requestor_remark = (EditText)view.findViewById(R.id.et_requestor_remark);
        et_remarks = (EditText)view.findViewById(R.id.et_remarks);
        et_roomms_complaint_id = (EditText)view.findViewById(R.id.et_roomms_complaint_id);
        et_du_serial_number = (EditText)view.findViewById(R.id.et_du_serial_number);
        fromTimeMM = (TextView) view.findViewById(R.id.fromTimeMM);
        fromTimeSS = (TextView) view.findViewById(R.id.fromTimeSS);
        toTimeMM = (TextView) view.findViewById(R.id.toTimeMM);
        toTimeSS = (TextView) view.findViewById(R.id.toTimeSS);
        tv_from_date = (TextView) view.findViewById(R.id.tv_from_date);
        tv_to_date = (TextView) view.findViewById(R.id.tv_to_date);
        tv_default_ro_value = (TextView) view.findViewById(R.id.tv_default_ro_value);
        tv_selected_pump = (TextView) view.findViewById(R.id.tv_selected_pump);
        tv_hint_Approver = (TextView) view.findViewById(R.id.tv_hint_Approver);
        fromDate = (RelativeLayout) view.findViewById(R.id.fromDate);
        layout_spinner_select_pump = (RelativeLayout) view.findViewById(R.id.layout_spinner_select_pump);
        toDate = (RelativeLayout) view.findViewById(R.id.toDate);
        spinner_select_pump = (Spinner) view.findViewById(R.id.spinner_select_pump);
        spinner_select_RO = (Spinner) view.findViewById(R.id.spinner_select_RO);
        spinner_status = (Spinner) view.findViewById(R.id.spinner_status);
        spinner_HQ = (Spinner) view.findViewById(R.id.spinner_HQ);
        spinner_zone = (Spinner) view.findViewById(R.id.spinner_zone);
        spinner_region = (Spinner) view.findViewById(R.id.spinner_region);
        spinner_sales_area = (Spinner) view.findViewById(R.id.spinner_sales_area);
        spinner_RO = (Spinner) view.findViewById(R.id.spinner_RO);
    }

    private void setLayout(){

        userRole = UserDataPrefrence.getPreference("HPCL_Preference", getContext(), "UserCustomRole","");
        if(userRole.equalsIgnoreCase("HPCLZONE")){
            ll_ro_list.setVisibility(View.VISIBLE);
            ll_hq_layout.setVisibility(View.GONE);
            tv_hint_Approver.setVisibility(View.GONE);
            ll_other_user.setVisibility(View.VISIBLE);
            btn_filter.setVisibility(View.VISIBLE);
            tv_selected_pump.setVisibility(View.GONE);
            layout_spinner_select_pump.setVisibility(View.VISIBLE);
            tv_default_ro_value.setSelected(true);
            getROCodeList();
            showStatusList();
        }
//        else if(userRole.equalsIgnoreCase("HPCLREGION")){
//            ll_ro_list.setVisibility(View.GONE);
//            ll_hq_layout.setVisibility(View.VISIBLE);
//            tv_hint_Approver.setVisibility(View.VISIBLE);
//            ll_other_user.setVisibility(View.GONE);
//            tv_selected_pump.setVisibility(View.VISIBLE);
//            layout_spinner_select_pump.setVisibility(View.GONE);
//            tv_selected_pump.setSelected(true);
//        }
    }

    private void showPumpDialog(){

        // initialise the alert dialog builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);

        // set the title for the alert dialog
        builder.setTitle("Select Pump");

        final boolean[] checkedItems = new boolean[pumpList.size()];

        String[] pumpArray = new String[pumpList.size()] ;//pumpList.toArray();

        for(int i=0;i<pumpList.size();i++){
            pumpArray[i] = pumpList.get(i);
        }


        // now this is the function which sets the alert dialog for multiple item selection ready
        builder.setMultiChoiceItems(pumpArray, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                checkedItems[which] = isChecked;
            }
        });


        // alert dialog shouldn't be cancellable
        builder.setCancelable(false);

        // handle the positive button of the dialog
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    StringBuilder pumps = new StringBuilder();
                    StringBuilder pumpsId = new StringBuilder();

                    for (int i = 0; i < checkedItems.length; i++) {
                        if (checkedItems[i]) {
                            pumps.append(pumpList.get(i) + ", ");
                            pumpsId.append(pumpIdList.get(i)+",");
                        }
                    }

                    selectedPump = pumpsId.toString().substring(0,pumpsId.toString().length()-1);

                    tv_selected_pump.setText(pumps.toString().substring(0,pumps.toString().length()-2));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        // handle the negative button of the alert dialog
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        // create the builder
        builder.create();

        // create the alert dialog with the
        // alert dialog builder instance
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }


    public String getCalculatedDate(String date, String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        try {
            cal.setTime(s.parse(date));
            cal.add(Calendar.DAY_OF_YEAR, days);
            return s.format(new Date(cal.getTimeInMillis()));//(new Date(s.parse(date).getTime()));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.e("TAG", "Error in Parsing Date : " + e.getMessage());
        }
        return null;
    }

    private void setListeners(){

        btn_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ll_mainlayout.setVisibility(View.GONE);
//                ll_filter_layout.setVisibility(View.VISIBLE);
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                Bundle txnBundle = new Bundle();
                txnBundle.putString("Filter_fragment", "LD_Pump_MR_MA");
                fragment_filter_textview filter_screen = new fragment_filter_textview();
                filter_screen.setArguments(txnBundle);
                changeFrag.addFragment(filter_screen);
//                getZoneList();
            }
        });

        tv_selected_pump.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPumpDialog();
            }
        });

        fromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                fromDate();
            }
        });

        toDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //toDate();
            }
        });

        fromTimeMM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromTimeHM();
            }
        });

        fromTimeSS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fromTimeHM();
            }
        });

        toTimeMM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //toTimeHM();
            }
        });

        toTimeSS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // toTimeHM();
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    isValid();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        submitBtnFilter.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if(selectedZone.equalsIgnoreCase("Select Zone")){
                    showError("Select Zone.");
                }else if(selectedRegion.equalsIgnoreCase("Select Region")){
                    showError("Select Region.");
                }else if(selectedSalesArea.equalsIgnoreCase("Select Sales Area")){
                    showError("Select Sales Area.");
                }else if(selectedROFilter.equalsIgnoreCase("-1")){
                    showError("Select RO Code.");
                }else {
                    ll_mainlayout.setVisibility(View.VISIBLE);
                    ll_filter_layout.setVisibility(View.GONE);
                    getPumpHQList(selectedZone,selectedRegion,selectedSalesArea,selectedROFilter);
                }
            }
        });

        cancelBtnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_mainlayout.setVisibility(View.VISIBLE);
                ll_filter_layout.setVisibility(View.GONE);
                tv_default_ro_value.setText("");
            }
        });

       /* btn_pump_details.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (selectedRO.equalsIgnoreCase("-1")){
                    showError("Select RO.");
                }else if(selectedPump.equalsIgnoreCase("-1")){
                    showError("Select Pump.");
                } else {
                    getPumpRequestDetails();
                }
            }
        });*/

       spinner_zone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
           @Override
           public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               selectedZone = zoneList.get(spinner_zone.getSelectedItemPosition());
               if(!selectedZone.equalsIgnoreCase("Select Zone")){
                   getRegionList(selectedZone);
               }
           }

           @Override
           public void onNothingSelected(AdapterView<?> adapterView) {

           }
       });

        spinner_region.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedRegion = regionList.get(spinner_region.getSelectedItemPosition());
                if(!selectedRegion.equalsIgnoreCase("Select Region")){
                    getSalesAreaList(selectedZone,selectedRegion);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_sales_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedSalesArea = salesAreaList.get(spinner_sales_area.getSelectedItemPosition());
                if(!selectedSalesArea.equalsIgnoreCase("Select Sales Area")){
                    getROCodeFilterList(selectedZone,selectedRegion,selectedSalesArea);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_RO.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedROFilter = ROCodeFilterList.get(spinner_RO.getSelectedItemPosition());
                tv_default_ro_value.setText(RONameFilterList.get(spinner_RO.getSelectedItemPosition()));

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_select_pump.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                selectedPump = pumpIdList.get(spinner_select_pump.getSelectedItemPosition());

                if(!selectedPump.equalsIgnoreCase("-1") && userRole.equalsIgnoreCase("HPCLZONE")){
                    selectedPumpSL= pumpSLList.get(spinner_select_pump.getSelectedItemPosition());
                    getPendingRequest(selectedRO,selectedPumpSL);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_select_RO.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

/*                if(userRole.equalsIgnoreCase("DEALER")){
                    selectedSA = sAMIdList.get(spinner_select_RO.getSelectedItemPosition());
                }else if(userRole.equalsIgnoreCase("HPCLSALESAREA") || userRole.equalsIgnoreCase("HPCLREGION")) {
                    selectedPump = pumpIdList.get(spinner_select_RO.getSelectedItemPosition());
                }*/

                selectedRO = ROCodeList.get(spinner_select_RO.getSelectedItemPosition());

                if(!selectedRO.equalsIgnoreCase("-1")){
                    getFilterData(selectedRO);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinner_HQ.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedHQ = hqCodeList.get(spinner_HQ.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedStatus = statusList.get(spinner_status.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void showStatusList(){
        statusList.add("Select Approval Status");
        statusList.add("Approved");
        statusList.add("Rejected");

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, statusList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_status.setAdapter(adapter);
    }

    private void fromDate() {

        final Calendar newCalendar = Calendar.getInstance();

        final DatePickerDialog fromDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                String selectedDate = year+"-"+(monthOfYear+1)+"-"+dayOfMonth;
                tv_from_date.setText(selectedDate);
                tv_to_date.setText(getCalculatedDate(selectedDate,"yyyy-MM-dd",7));
                String currenthourTime = new SimpleDateFormat("HH", Locale.getDefault()).format(new Date());
                String currentminTime = new SimpleDateFormat("mm", Locale.getDefault()).format(new Date());
                fromTimeMM.setText(currenthourTime);
                fromTimeSS.setText(currentminTime);
                toTimeMM.setText(currenthourTime);
                toTimeSS.setText(currentminTime);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        fromDate.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
        fromDate.show();

    }

    private void fromTimeHM() {

        final Calendar newCalendar = Calendar.getInstance();
        int mHour = newCalendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = newCalendar.get(Calendar.MINUTE);

        final TimePickerDialog fromTimeMMSS = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hr, int mm) {
                fromTimeMM.setText(""+hr);
                fromTimeSS.setText(""+mm);
            }
        },mHour,mMinute,false);

        fromTimeMMSS.show();

    }

    private void toDate() {

        final Calendar newCalendar = Calendar.getInstance();

        final DatePickerDialog toDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tv_to_date.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDate.show();
    }

    private void toTimeHM() {

        final Calendar newCalendar = Calendar.getInstance();
        int mHour = newCalendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = newCalendar.get(Calendar.MINUTE);

        final TimePickerDialog fromTimeMMSS = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker timePicker, int hr, int mm) {
                toTimeMM.setText(""+hr);
                toTimeSS.setText(""+mm);
            }

        },mHour,mMinute,false);

        fromTimeMMSS.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void methodDone() {

        if(respCode.equalsIgnoreCase("401")){
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public void methodCancel() {

    }

    private void showError(String msg){
        universalDialog = new UniversalDialog(getActivity(),
                alertDialogInterface, "", msg, getString(R.string.dialog_ok), "");
        universalDialog.showAlert();
    }

    private void getZoneList(){
        JSONObject json = new JSONObject();
        try {
            json.put("ROCode","");
            json.put("Para1","");
            json.put("Para2","");
            json.put("Para3","");
            json.put("Para4","");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncZoneList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncZoneList extends AsyncTask<JSONObject, Void, String> {

        public AsyncZoneList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/GetZone", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayZone = new JSONArray(jsonObject.getString("Data"));

                            zoneList.clear();

                            zoneList.add(0,"Select Zone");

                            for(int i=0;i<arrayZone.length();i++){
                                JSONObject pumpObject = arrayZone.getJSONObject(i);
                                zoneList.add(pumpObject.getString("Zone"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, zoneList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_zone.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void getRegionList(String zone){
        JSONObject json = new JSONObject();
        try {
            json.put("ROCode","");
            json.put("Para1",zone);
            json.put("Para2","");
            json.put("Para3","");
            json.put("Para4","");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncRegionList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncRegionList extends AsyncTask<JSONObject, Void, String> {

        public AsyncRegionList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/GetRegion", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayZone = new JSONArray(jsonObject.getString("Data"));

                            regionList.clear();

                            regionList.add(0,"Select Region");

                            for(int i=0;i<arrayZone.length();i++){
                                JSONObject pumpObject = arrayZone.getJSONObject(i);
                                regionList.add(pumpObject.getString("Region"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, regionList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_region.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void getSalesAreaList(String zone, String region){
        JSONObject json = new JSONObject();
        try {
            json.put("ROCode","");
            json.put("Para1",zone);
            json.put("Para2",region);
            json.put("Para3","");
            json.put("Para4","");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncSalesAreaList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncSalesAreaList extends AsyncTask<JSONObject, Void, String> {

        public AsyncSalesAreaList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/GetSalesArea", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayZone = new JSONArray(jsonObject.getString("Data"));

                            salesAreaList.clear();

                            salesAreaList.add(0,"Select Sales Area");

                            for(int i=0;i<arrayZone.length();i++){
                                JSONObject pumpObject = arrayZone.getJSONObject(i);
                                salesAreaList.add(pumpObject.getString("SalesArea"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, salesAreaList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_sales_area.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void getROCodeFilterList(String zone, String region, String salesArea){

        JSONObject json = new JSONObject();

        try {
            json.put("ROCode","");
            json.put("Para1",zone);
            json.put("Para2",region);
            json.put("Para3",salesArea);
            json.put("Para4","");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncROCodeFilterList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncROCodeFilterList extends AsyncTask<JSONObject, Void, String> {

        public AsyncROCodeFilterList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/GetROCode", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayZone = new JSONArray(jsonObject.getString("Data"));

                            RONameFilterList.clear();
                            ROCodeFilterList.clear();

                            RONameFilterList.add(0,"Select RO Code");
                            ROCodeFilterList.add(0,"-1");

                            for(int i=0;i<arrayZone.length();i++){
                                JSONObject pumpObject = arrayZone.getJSONObject(i);
                                ROCodeFilterList.add(pumpObject.getString("ROCode"));
                                RONameFilterList.add(pumpObject.getString("ROName"));

                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, RONameFilterList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_RO.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void getPumpHQList(String zone, String region, String salesArea, String ROCode){

        JSONObject json = new JSONObject();
        try {
            json.put("ROCode","");
            json.put("Para1",zone);
            json.put("Para2",region);
            json.put("Para3",salesArea);
            json.put("Para4",ROCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncHQPumpList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncHQPumpList extends AsyncTask<JSONObject, Void, String> {


        public AsyncHQPumpList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/GetPumpHQ", false, jsonObjects[0],"");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject dataObj = jsonObject.getJSONObject("Data");
                            JSONArray arraypump = new JSONArray(dataObj.getString("Pump"));
                            JSONArray arrayHQ = new JSONArray(dataObj.getString("HQ"));

                            pumpIdList.clear();
                            pumpList.clear();

/*                            pumpList.add(0,"Select Pump");
                            pumpIdList.add(0,"-1");*/

                            for(int i=0;i<arraypump.length();i++){
                                JSONObject pumpObject = arraypump.getJSONObject(i);
                                pumpIdList.add(pumpObject.getString("PumpNo"));
                                pumpList.add(pumpObject.getString("PumpName"));
                            }

                            hqNameList.clear();
                            hqCodeList.clear();

                            hqNameList.add(0,"Select Approver");
                            hqCodeList.add(0,"-1");

                            for(int i=0;i<arrayHQ.length();i++){
                                JSONObject hqObject = arrayHQ.getJSONObject(i);
                                hqNameList.add(hqObject.getString("HQName"));
                                hqCodeList.add(hqObject.getString("HQCode"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, pumpList);

                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

                            ArrayAdapter adapterHQ = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, hqNameList);

                            adapterHQ.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

                            /*if(userRole.equalsIgnoreCase("DEALER")){
                                spinner_select_pump.setAdapter(adapter);
                            }else if(userRole.equalsIgnoreCase("HPCLSALESAREA") || userRole.equalsIgnoreCase("HPCLREGION")){
                                spinner_select_SAM.setAdapter(adapter);
                            }*/

                           // spinner_select_pump.setAdapter(adapter);

                            spinner_HQ.setAdapter(adapterHQ);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }


    private void getROCodeList(){

        JSONObject json = new JSONObject();

        /*try {
            json.put("ROCode","");
            json.put("Para1",zone);
            json.put("Para2",region);
            json.put("Para3",salesArea);
            json.put("Para4","");
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        new AsyncROCodeList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncROCodeList extends AsyncTask<JSONObject, Void, String> {

        public AsyncROCodeList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/GetPendingRO", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayZone = new JSONArray(jsonObject.getString("Data"));


                            RONameList.add(0,"Select RO Code");
                            ROCodeList.add(0,"-1");

                            for(int i=0;i<arrayZone.length();i++){
                                JSONObject pumpObject = arrayZone.getJSONObject(i);
                                ROCodeList.add(pumpObject.getString("ROCode"));
                                RONameList.add(pumpObject.getString("ROName"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, RONameList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_select_RO.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }


    private void getFilterData(String rocode){

        JSONObject json = new JSONObject();

        try {
            json.put("ROCode",rocode);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncGetFilterData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncGetFilterData extends AsyncTask<JSONObject, Void, String> {

        public AsyncGetFilterData() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/GetFilterData", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject dataObj = new JSONObject(jsonObject.getString("Data"));

                            JSONArray arraypump = dataObj.getJSONArray("Pump");

                            pumpIdList.clear();
                            pumpList.clear();
                            pumpSLList.clear();

                            pumpList.add(0,"Select Pump");
                            pumpIdList.add(0,"-1");
                            pumpSLList.add("-1");

                            for(int i=0;i<arraypump.length();i++){
                                JSONObject pumpObject = arraypump.getJSONObject(i);
                                pumpIdList.add(pumpObject.getString("PumpNo"));
                                pumpList.add(pumpObject.getString("PumpName"));
                                pumpSLList.add(pumpObject.getString("SL"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, pumpList);

                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

                            spinner_select_pump.setAdapter(adapter);

                        }else{

                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void getPendingRequest(String rocode, String slCode){

        JSONObject json = new JSONObject();

        try {
            json.put("ROCode",rocode);
            json.put("Para1",slCode);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncGetPendingRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncGetPendingRequest extends AsyncTask<JSONObject, Void, String> {

        public AsyncGetPendingRequest() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/GetPendingRequest", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray dataArray = new JSONArray(jsonObject.getString("Data"));

                            JSONObject dataObj = dataArray.getJSONObject(0);


                            String dealerRemarks = dataObj.getString("Remarks");
                            String fromDate = dataObj.getString("FromDate");
                            String toDate = dataObj.getString("ToDate");
                            String rommsComplaintID = dataObj.getString("ROMMSComplaintID");
                            String duSerialNumber = dataObj.getString("DUSerialNumber");

                            et_requestor_remark.setText(dealerRemarks);

                            String[] dateTimeFrom = fromDate.split(" ");
                            tv_from_date.setText(dateTimeFrom[0]);
                            String[] fromTimes = dateTimeFrom[1].split(":");
                            fromTimeMM.setText(fromTimes[0]);
                            fromTimeSS.setText(fromTimes[1]);

                            String[] dateTimeTo = toDate.split(" ");
                            tv_to_date.setText(dateTimeTo[0]);

                            String[] toTimes = dateTimeTo[1].split(":");
                            toTimeMM.setText(toTimes[0]);
                            toTimeSS.setText(toTimes[1]);

                            et_roomms_complaint_id.setText(rommsComplaintID);
                            et_du_serial_number.setText(duSerialNumber);


                        }else{

                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }


    private void isValid(){

//        if(userRole.equalsIgnoreCase("HPCLREGION")) {
//
//            String fromDate = tv_from_date.getText().toString();
//            String fromTimeHr = fromTimeMM.getText().toString();
//            String fromTimeMm = fromTimeSS.getText().toString();
//
//            String toDate = tv_to_date.getText().toString();
//            String toTimeHr = toTimeMM.getText().toString();
//            String toTimeMm = toTimeSS.getText().toString();
//
//            String remarks = et_remarks.getText().toString().trim();
//
//            String roommss = et_roomms_complaint_id.getText().toString().trim();
//            String duSerialNumber = et_du_serial_number.getText().toString().trim();
//
//
//            if (selectedPump.equalsIgnoreCase("")) {
//                showError("Select Pump.");
//            } else if (fromDate.equalsIgnoreCase("") || fromTimeHr.equalsIgnoreCase("") || fromTimeMm.equalsIgnoreCase("")) {
//                showError("Set From Datetime.");
//
//            } else if (toDate.equalsIgnoreCase("") || toTimeHr.equalsIgnoreCase("") || toTimeMm.equalsIgnoreCase("")) {
//                showError("Set To Datetime.");
//
//            } else if (roommss.equalsIgnoreCase("")) {
//                showError("Enter ROMMS Complaint Id.");
//            } else if (duSerialNumber.equalsIgnoreCase("")) {
//                showError("Enter DU Serial Number.");
//            } else if (remarks.equalsIgnoreCase("")) {
//                showError("Enter remarks.");
//
//            } else if (selectedHQ.equalsIgnoreCase("-1")) {
//                showError("Select Approver");
//            } else {
//                saveRequest();
//            }
//
//        }
         if(userRole.equalsIgnoreCase("HPCLZONE")){

            String fromDate = tv_from_date.getText().toString();
            String fromTimeHr = fromTimeMM.getText().toString();
            String fromTimeMm = fromTimeSS.getText().toString();

            String toDate = tv_to_date.getText().toString();
            String toTimeHr = toTimeMM.getText().toString();
            String toTimeMm = toTimeSS.getText().toString();

            String remarks = et_remarks.getText().toString().trim();

            String roommss = et_roomms_complaint_id.getText().toString().trim();
            String duSerialNumber = et_du_serial_number.getText().toString().trim();

            if (selectedRO.equalsIgnoreCase("-1")) {
                showError("Select RO Code.");
            } else if (selectedPump.equalsIgnoreCase("-1")) {
                showError("Select Pump.");
            }else if (fromDate.equalsIgnoreCase("") || fromTimeHr.equalsIgnoreCase("") || fromTimeMm.equalsIgnoreCase("")) {
                showError("Set From Datetime.");

            } else if (toDate.equalsIgnoreCase("") || toTimeHr.equalsIgnoreCase("") || toTimeMm.equalsIgnoreCase("")) {
                showError("Set To Datetime.");

            } else if (roommss.equalsIgnoreCase("")) {
                showError("Enter Roomms Complaint Id.");
            } else if (duSerialNumber.equalsIgnoreCase("")) {
                showError("Enter DU Serial Number.");
            }else if(selectedStatus.equalsIgnoreCase("Select Approval Status")){
                showError("Select Approval Status.");
            } else if (remarks.equalsIgnoreCase("")) {
                showError("Enter remarks.");
            }else{
                approveRejectRequest();
            }
        }
    }

    private void saveRequest(){

        String fromDate = tv_from_date.getText().toString();
        String fromTimeHr = fromTimeMM.getText().toString();
        String fromTimeMm = fromTimeSS.getText().toString();

        String toDate = tv_to_date.getText().toString();
        String toTimeHr = toTimeMM.getText().toString();
        String toTimeMm = toTimeSS.getText().toString();

        String remarks = et_remarks.getText().toString().trim();
        String roommss = et_roomms_complaint_id.getText().toString().trim();
        String duSerialNumber = et_du_serial_number.getText().toString().trim();

        JSONObject json = new JSONObject();

        try {
            json.put("ROCode",selectedROFilter);
            json.put("PumpDetails",selectedPump);
            json.put("PumpDetails1","");
            json.put("FromDate",fromDate+" "+fromTimeHr+":"+fromTimeMm+":00");
            json.put("ToDate",toDate+" "+toTimeHr+":"+toTimeMm+":00");
            json.put("ROMMSComplaintID",roommss);
            json.put("DUSerialNumber",duSerialNumber);
            json.put("Remarks1",remarks);
            json.put("RegionalManager",selectedHQ);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncSaveRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncSaveRequest extends AsyncTask<JSONObject, Void, String> {

        public AsyncSaveRequest() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/SaveRequest", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);

                            String msg = jsonObject.getString("ResponseMessage");

                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {

                                        @Override
                                        public void methodDone() {
                                            try {
                                                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                                            ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                                                    , new HomeFragment_old(), new HomeFragment_old()
                                                    , new HomeFragment_old(), new HomeFragment_old());
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", msg, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void approveRejectRequest(){

        String remarks = et_remarks.getText().toString().trim();

        JSONObject json = new JSONObject();

        try {
            json.put("SL",selectedPumpSL);
            json.put("Remarks2",remarks);
            json.put("AprovalStatus1",selectedStatus);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncApproveRejectRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncApproveRejectRequest extends AsyncTask<JSONObject, Void, String> {

        public AsyncApproveRejectRequest() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpMR/ApproveRejectRequest", false, jsonObjects[0],"");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);

                            String msg = jsonObject.getString("ResponseMessage");

                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {

                                        @Override
                                        public void methodDone() {
                                            try {
                                                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                                            ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                                                    , new HomeFragment_old(), new HomeFragment_old()
                                                    , new HomeFragment_old(), new HomeFragment_old());
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", msg, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

}

package com.agstransact.HP_SC.dashboard.fragment;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.splash.ActivityLaunchScreen;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsKeyboardUtil;
import com.agstransact.HP_SC.utils.ClsWeakDataEncrption;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONObject;

public class Change_New_Confirm_Mpin_new extends Fragment implements  View.OnFocusChangeListener,
        View.OnKeyListener, TextWatcher,View.OnClickListener, AlertDialogInterface {
    View rootView;
    EditText edtPinForTxn1, edtPinForTxn2, edtPinForTxn3, edtPinForTxn4, mPinHiddenEditText;
    TextView btnResendOtp, txtResendOtpholder, tvTitle, tvAttempt, tv2,tvEnterPIN,tv5;
    String strMPIN="", device_no,strSMSType,regId;
    int verCode;
    ImageView back, ivHome;
    CheckedTextView chkdViewMPIN;
//    PinEntryEditText mPinEntry;
    AlertDialogInterface dialogInterface;
    UniversalDialog universalDialog;
    Bundle bundle;
    JSONObject jsonObject;
    Button btn_send;
    LinearLayout rootLL;
    TextView tv_enter_mpin;
    int[] intMpin;
//    int i = 1;

//    Bundle bundle;
    private AppCompatCheckBox Cb_Password;


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_change_pin, container, false);
        bundle = getArguments();
        seyUpWidgets();

        ClsKeyboardUtil objKeyBoardUtil;
        objKeyBoardUtil = new ClsKeyboardUtil(getActivity());
        objKeyBoardUtil.setupUI(rootView);
        btn_send.setOnClickListener(this);
        tv_enter_mpin.setText("Confirm your new MPIN");
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void seyUpWidgets() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            verCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        dialogInterface = this;
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
        toolbarTV.setText(getResources().getString(R.string.change_mpin));

//        tvTitle = (TextView) rootView.findViewById(R.id.tvTitle);
//        mPinEntry = (PinEntryEditText) rootView.findViewById(R.id.pinEntryET);
        btn_send = (Button) rootView.findViewById(R.id.btn_send);
        tv_enter_mpin = (TextView) rootView.findViewById(R.id.tv_enter_mpin);
        rootLL = (LinearLayout) rootView.findViewById(R.id.rootLL);

        edtPinForTxn1 = rootView.findViewById(R.id.et1);
        edtPinForTxn2 = rootView.findViewById(R.id.et2);
        edtPinForTxn3 = rootView.findViewById(R.id.et3);
        edtPinForTxn4 = rootView.findViewById(R.id.et4);

        edtPinForTxn1.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn2.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn3.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn4.setTransformationMethod(new MyPasswordTransformationMethod());
        mPinHiddenEditText = (EditText) rootView.findViewById(R.id.mPinHiddenEditText);

        setPINListeners();

        chkdViewMPIN =  rootView.findViewById(R.id.chkdViewMPIN);
        Cb_Password =  rootView.findViewById(R.id.Cb_Password);
        Cb_Password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    // show password
                    edtPinForTxn1.setTransformationMethod(null);
                    edtPinForTxn2.setTransformationMethod(null);
                    edtPinForTxn3.setTransformationMethod(null);
                    edtPinForTxn4.setTransformationMethod(null);
                    Cb_Password.setBackground(getResources().getDrawable(R.drawable.eye_close));
                } else {
                    // hide password
                    edtPinForTxn1.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn2.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn3.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn4.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    Cb_Password.setBackground(getResources().getDrawable(R.drawable.eye));
                }
            }
        });
        chkdViewMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkdViewMPIN.isChecked()) {


                }else{

                }
            }
        });
        strSMSType ="";
        strSMSType = "DEVICECHANGEOTP";


        String android_id = Settings.Secure.getString(getActivity().getBaseContext().
                getContentResolver(), Settings.Secure.ANDROID_ID); //returns the Android ID as an unique 64-bit hex string.
        TelephonyManager tm = (TelephonyManager) getActivity().
                getSystemService(Context.TELEPHONY_SERVICE); // IMEI
        device_no = android_id;
        Log.e("device_no", "" + device_no);

    }
    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);
        edtPinForTxn1.setOnFocusChangeListener(this);
        edtPinForTxn2.setOnFocusChangeListener(this);
        edtPinForTxn3.setOnFocusChangeListener(this);
        edtPinForTxn4.setOnFocusChangeListener(this);

        edtPinForTxn1.setOnKeyListener(this);
        edtPinForTxn2.setOnKeyListener(this);
        edtPinForTxn3.setOnKeyListener(this);
        edtPinForTxn4.setOnKeyListener(this);
        mPinHiddenEditText.setOnKeyListener(this);
    }
    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            final int id = v.getId();
            switch (id) {
                case R.id.mPinHiddenEditText:
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (mPinHiddenEditText.getText().length() == 4)
                            edtPinForTxn4.setText("");
                        else if (mPinHiddenEditText.getText().length() == 3)
                            edtPinForTxn3.setText("");
                        else if (mPinHiddenEditText.getText().length() == 2)
                            edtPinForTxn2.setText("");
                        else if (mPinHiddenEditText.getText().length() == 1)
                            edtPinForTxn1.setText("");

                        if (mPinHiddenEditText.length() > 0)
                            mPinHiddenEditText.setText(mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1));

                        return true;
                    }

                    break;

                default:
                    return false;
            }
        }

        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


        if (s.length() == 0) {
            edtPinForTxn1.setText("");
        } else if (s.length() == 1) {
            edtPinForTxn1.setText(s.charAt(0) + "");
            edtPinForTxn2.setText("");
            edtPinForTxn3.setText("");
            edtPinForTxn4.setText("");


        } else if (s.length() == 2) {
            edtPinForTxn2.setText(s.charAt(1) + "");
            edtPinForTxn3.setText("");
            edtPinForTxn4.setText("");


        } else if (s.length() == 3) {
//            setFocusedPinBackground(mPinForthDigitEditText);
            edtPinForTxn3.setText(s.charAt(2) + "");
            edtPinForTxn4.setText("");

        } else if (s.length() == 4) {
//            setFocusedPinBackground(mPinFifthDigitEditText);
            edtPinForTxn4.setText(s.charAt(3) + "");

            hideSoftKeyboard(edtPinForTxn4);


        }

    }

    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    public void afterTextChanged(Editable s) {

        strMPIN = s.toString();
        if (strMPIN.length() == 4) {
//                mPinHiddenEditText.setText("");
//                edtPinForTxn1.setText("");
//                edtPinForTxn2.setText("");
//                edtPinForTxn3.setText("");
//                edtPinForTxn4.setText("");
            if (strMPIN.length() == 4) {
                try{
                    if (ConstantDeclaration.gifProgressDialog != null) {
                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                            ConstantDeclaration.showGifProgressDialog(getActivity());
                        }
                    } else {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                    try {
//                            intMpin = universalDialog.getIntFromString(str.toString());

                        int[] intArray = new int[4];
                        for (int i = 0; i < 4; i++) {
                            intArray[i] = Integer.parseInt(String.valueOf(strMPIN.charAt(i)));
                        }
                        intMpin = intArray;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                            intMpin = universalDialog.getIntFromString(mPinEntry.getText().toString().trim());
                    if( checkPinSequence()){
                        try {
                            if (ConstantDeclaration.gifProgressDialog.isShowing())
                                ConstantDeclaration.gifProgressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        checkValidPIN(strMPIN.toString());
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else{
                universalDialog = new UniversalDialog(getActivity(),
                        dialogInterface, "", "Please enter Valid MPIN", getString(R.string.got_it), "");
                universalDialog.showAlert();
            }
        }
        if (strMPIN.length() == 4) {

            btn_send.setEnabled(true);
            btn_send.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_corner_button));
        } else {
            btn_send.setEnabled(false);
            btn_send.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final int id = v.getId();
        switch (id) {
            case R.id.et1:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et2:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et3:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et4:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;


            default:
                break;

        }
    }

    @Override
    public void onClick(View view) {
        if (view == btn_send) {
//            intMpin = universalDialog.getIntFromString(mPinEntry.getText().toString().trim());
            checkValidPIN(strMPIN);

        }
    }

    @Override
    public void methodDone() {

    }

    @Override
    public void methodCancel() {

    }

    public class MyPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }
    private void checkValidPIN(String strPin) {
//        hideSoftKeyboard();
        Long tsLing = null;

        JSONObject json = null;
        try {
            json = new JSONObject();
            json.put("OldMPIN", ClsWeakDataEncrption.encryptData(getActivity(), bundle.getString("OLDMPIN")));
            json.put("MPIN", ClsWeakDataEncrption.encryptData(getActivity(), strMPIN));

        } catch (Exception e) {
            e.printStackTrace();
        }
        new AsyncChangePin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }
    public class AsyncChangePin extends AsyncTask<JSONObject, Void, String> {

        String strTimeStamp;

        public AsyncChangePin(String strTime) {
            strTimeStamp = strTime;
        }
        public AsyncChangePin() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Login/ChangeMPIN", false, jsonObjects[0],"");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            String strWallet = "";
            String str_response_msg = "", str_time = "";
            if (s.contains("||")) {
                String[] str = (s.split("\\|\\|"));
//                For token refresh 22-08-2019
//                int respCode = Integer.parseInt(str[0]);
//                try {
//                    if(respCode == 401){
//                        ActionRefreshToken refreshToken = (ActionRefreshToken) getActivity();
//                        refreshToken.refreshToken();
//                        return;
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                if (str[0].equalsIgnoreCase("00")
                        ||str[0].equalsIgnoreCase("0")) {
                    universalDialog = new UniversalDialog(getActivity(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {
                            try {
                                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                            ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                                    , new HomeFragment_old(), new HomeFragment_old()
                                    , new HomeFragment_old(), new HomeFragment_old());
                        }

                        @Override
                        public void methodCancel() {

                        }
                    }, ""
                            , "MPIN Changed Successfully", getString(R.string.dialog_ok), "");
                    universalDialog.setCancelable(false);
                    universalDialog.showAlert();

                }
                else if (str[0].equalsIgnoreCase("907")) {
                    clearPIN();
                    universalDialog = new UniversalDialog(getActivity(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {
                            Intent intentLogOut = new Intent(getActivity(), ActivityLaunchScreen.class);
                            intentLogOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentLogOut);
                            getActivity().finish();
                        }

                        @Override
                        public void methodCancel() {

                        }
                    }, ""
                            , str[1], getString(R.string.dialog_ok), "");
                    universalDialog.setCancelable(false);
                    universalDialog.showAlert();
                    return;
                }
                else if(str[0].equalsIgnoreCase("401")){
                    try {
                        if (ConstantDeclaration.gifProgressDialog.isShowing())
                            ConstantDeclaration.gifProgressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    universalDialog = new UniversalDialog(getActivity(),
                            new AlertDialogInterface() {
                                @Override
                                public void methodDone() {
                                    Intent intent = new Intent(getActivity(), LoginActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }

                                @Override
                                public void methodCancel() {

                                }
                            }, "", str[1], "OK", "");
                    universalDialog.showAlert();
                }
                else {
                    //18-1-19
                    UniversalDialog universalDialog = new UniversalDialog(getActivity(), null, ""
                            , str[1], getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
                     clearPIN();
                }
            }
        }
    }

    boolean checkPinSequence() {
        if (checkBackwardPinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else if (checkForwardPinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else if (checkSamePinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else {
            return true;
        }

    }
    public void clearPIN() {
        edtPinForTxn1.setText("");
        edtPinForTxn2.setText("");
        edtPinForTxn3.setText("");
        edtPinForTxn4.setText("");
        strMPIN = "";
    }
    public boolean checkForwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] + 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("ForwardPin", "false");
                return false;
            }
        }
        Log.e("ForwardPin", "true");
        return isSequence;
    }
    public boolean checkBackwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] - 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("BackwardPin", "false");
                return false;
            }
        }
        Log.e("BackwardPin", "true");
        return isSequence;
    }
    public boolean checkSamePinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length; i++) {
            for (int j = i + 1; j < mPin.length; j++) {
                if (mPin[i] == mPin[j]) {
                    isSequence = true;
                } else {
                    Log.e("SamePin", "false");
                    return false;
                }
            }
        }
        Log.e("SamePin", "true");
        return isSequence;
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

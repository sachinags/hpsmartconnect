package com.agstransact.HP_SC.dashboard;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.fragment.FragmentCriticalStockList;
import com.agstransact.HP_SC.dashboard.fragment.FragmentNanoStatus;
import com.agstransact.HP_SC.dashboard.fragment.FragmentPriceExceptionList;
import com.agstransact.HP_SC.dashboard.fragment.FragmentROConnectivity;
import com.agstransact.HP_SC.dashboard.fragment.HomeFragment_old;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.DrawerLocker;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.google.android.material.navigation.NavigationView;

public class HOSDashboardActivity_new extends AppCompatActivity implements AlertDialogInterface,NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, DrawerLocker, ActionChangeFrag {

    public DrawerLayout drawerLayout;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    private Toolbar toolbar;
    private ImageView iv_drawer_menu;
    AlertDialogInterface dialogInterface;
    UniversalDialog universalDialog;
    Menu option_menu;
    /*toggle menu action */
    private ActionBarDrawerToggle mDrawerToggle;
    /*drawer layout of left*/
    private DrawerLayout mDrawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hos_dashboard);
        configureToolbar();
        drawerLayout = findViewById(R.id. drawer_layout ) ;
//        iv_drawer_menu = (ImageView) findViewById(R.id.iv_drawer_menu);
//        iv_drawer_menu.setImageResource(R.drawable.drawer_icon);
//        iv_drawer_menu.setOnClickListener(this);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawerLayout , toolbar , R.string.nav_open ,
//                R.string.nav_close ) ;
//        drawerLayout.addDrawerListener(toggle) ;
//        dialogInterface = this;
//        toggle.syncState() ;
//        NavigationView navigationView = findViewById(R.id.nav_view) ;
//        navigationView.setNavigationItemSelectedListener(this) ;
        replaceScreen(new HomeFragment_old());
    }
    private void setupDrawer() {

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        getSupportFragmentManager().addOnBackStackChangedListener(mOnBackStackChangedListener);
    }
    private FragmentManager.OnBackStackChangedListener
            mOnBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            int countFrag = getSupportFragmentManager().getBackStackEntryCount();
            Log.e("Count", "" + countFrag);

            mDrawerToggle.setDrawerIndicatorEnabled(countFrag == 1);

            Fragment frag = getSupportFragmentManager().findFragmentById(R.id.dashboard_container);
            String fragname = null;
            if (frag != null) {
                fragname = frag.getClass().getSimpleName();
            }

            try {
                if (fragname != null && (fragname.equalsIgnoreCase("HomeFragment_old"))) {
                    TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
                    ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
                    toolbarIv.setVisibility(View.VISIBLE);
                    toolbarTV.setVisibility(View.GONE);
                    //show drawer icon and menu options
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                option_menu.findItem(R.id.action_notification).setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);
                    try {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                        getSupportActionBar().setDisplayShowHomeEnabled(true);
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
                    ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
                    toolbarIv.setVisibility(View.GONE);
                    toolbarTV.setVisibility(View.VISIBLE);
                    //hide drawer icon and menu options
//                    if (fragname != null && (fragname.equalsIgnoreCase("SuccessfulFragment"))) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                option_menu.findItem(R.id.action_notification).setVisible(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);

                    try {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };



    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.dashboard_container, frag, "LAUNCH");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("LAUNCH");
        fragmentTransaction.commit();

    }

    private void configureToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().hide();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.drawer_icon);
        }
        /*remove toolbar title*/
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        ActionBar actionbar = getSupportActionBar();
//        actionbar.setHomeAsUpIndicator(R.drawable.drawer_icon);
//        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        int itemID = item.getItemId();
        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout ) ;
        drawerLayout.closeDrawer(GravityCompat.START ) ;

        int itemID = item.getItemId();

        if (itemID == R.id.nav_dashboard){

            replaceScreen(new HomeFragment_old());
            return true;

        } else if (itemID == R.id.nav_price_exception_list){

            replaceScreen(new FragmentPriceExceptionList());
            return true;

        }else if (itemID == R.id.nav_ro_connectivity){

            replaceScreen(new FragmentROConnectivity());
            return true;

        }else if (itemID == R.id.nav_critical_stock){

            replaceScreen(new FragmentCriticalStockList());
            return true;

        }else if (itemID == R.id.nav_nano_status){

            replaceScreen(new FragmentNanoStatus());
            return true;

        }else if (itemID == R.id.nav_pump_mr){

        }else if (itemID == R.id.nav_pump_manual_mode){

        }else if (itemID == R.id.nav_change_mPIN){

        }else if (itemID == R.id.nav_logout){

        }
        return true;



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
//            case R.id.iv_drawer_menu:
//
//
//                break;

        }
    }

    @Override
    public void setDrawerLocked(boolean enabled) {
        if(enabled){
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }else{
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void addFragment(Fragment fragment) {
        ConstantDeclaration.replaceFragment(fragment, getSupportFragmentManager());
        ConstantDeclaration.hideKeyboard(HOSDashboardActivity_new.this);
    }

    @Override
    public void ReplaceFrag(Fragment fragment) {

    }

    @Override
    public void updateDrawer(boolean isShown) {
        try {
            mDrawerToggle.setDrawerIndicatorEnabled(isShown);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(isShown);
            getSupportActionBar().setDisplayHomeAsUpEnabled(isShown);
            option_menu.getItem(0).setVisible(isShown);
            option_menu.getItem(1).setVisible(isShown);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        }else{
            Fragment frag = getSupportFragmentManager().findFragmentById(R.id.dashboard_container);
            String fragname = null;

            if (frag != null) {
                fragname = frag.getClass().getSimpleName();

                if (fragname.equalsIgnoreCase("HomeFragment_old")) {
                    universalDialog = new UniversalDialog(HOSDashboardActivity_new.this, dialogInterface, "", getString(R.string.app_close_msg), getString(R.string.yes), getString(R.string.no));
                    universalDialog.showAlert();
//                    toolbar.setNavigationIcon(navIcon);
                }
            }
        }
    }

    @Override
    public void methodDone() {
        finish();
    }

    @Override
    public void methodCancel() {

    }
}
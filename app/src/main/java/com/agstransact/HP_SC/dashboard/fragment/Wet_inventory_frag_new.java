package com.agstransact.HP_SC.dashboard.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;

import com.agstransact.HP_SC.dashboard.adapter.NewDesign.Pump_model_new;

import com.agstransact.HP_SC.dashboard.adapter.NewDesign.pump_status_adapter;
import com.agstransact.HP_SC.dashboard.adapter.NewDesign.tank_model_new;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.GridSpacingItemDecoration;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.agstransact.HP_SC.dashboard.adapter.NewDesign.tank_status_adapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Wet_inventory_frag_new extends Fragment implements AlertDialogInterface {
    RecyclerView recycler_grid_tank_status,recycler_grid_pump_nozzle_status;
    private static ArrayList<tank_model_new> tank_model_new;
    private static ArrayList<Pump_model_new> pump_model_new;
    tank_status_adapter adapter;
    pump_status_adapter adapter1;
    TextView tv_tank_status,tv_pump_nozzle_status;
    Bundle bundle;
    Integer[] drawableArray;
    JSONArray arraydata;
    JSONArray arraytankdata;
    ArrayList<String> RoList;
    ArrayList<String> RoCode;
    UniversalDialog universalDialog;
    AlertDialogInterface alertDialogInterface;
    String SelectedRO ="",error ="";
    Spinner Spinner_Select_RO;
    String respCode="";
    TextView tv_No_data_tank,tv_No_data_pump;
    LinearLayout ll_RO_spinner;
    TextView tv_select_filter;
    ImageView iv_filter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.wet_inventory_frag, container, false);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.iv_tlb_additional);
        toolbarTV.setText(getResources().getString(R.string.wet_inventory));
        bundle = getArguments();
        intializeView(view);
        setHasOptionsMenu(true);

//        CallPumpStatus();
        alertDialogInterface = (AlertDialogInterface) this;
//        CallTankStatus();
        try {
            if(bundle.getString("fragment").equalsIgnoreCase("tankstatus")){
                recycler_grid_tank_status.setVisibility(View.VISIBLE);
                recycler_grid_pump_nozzle_status.setVisibility(View.GONE);
                tv_tank_status.setVisibility(View.VISIBLE);
                tv_pump_nozzle_status.setVisibility(View.GONE);
                tv_No_data_pump.setVisibility(View.GONE);

            }else if(bundle.getString("fragment").equalsIgnoreCase("pumpstatus")){
                recycler_grid_tank_status.setVisibility(View.GONE);
                recycler_grid_pump_nozzle_status.setVisibility(View.VISIBLE);
                tv_tank_status.setVisibility(View.GONE);
                tv_pump_nozzle_status.setVisibility(View.VISIBLE);
                tv_No_data_tank.setVisibility(View.GONE);

            }else{
                recycler_grid_tank_status.setVisibility(View.VISIBLE);
                recycler_grid_pump_nozzle_status.setVisibility(View.VISIBLE);
                tv_tank_status.setVisibility(View.VISIBLE);
                tv_pump_nozzle_status.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        adapter1 = new pumpnozzle_adapter(pump_nozzle_models, getActivity());
//        recycler_grid_pump_nozzle_status.setAdapter(adapter1);
        populateROList();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, RoList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        Spinner_Select_RO.setAdapter(adapter);
        Spinner_Select_RO.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (RoList.get(Spinner_Select_RO.getSelectedItemPosition()) != null) {
//                    SelectedRO = RoList.get(Spinner_Select_RO.getSelectedItemPosition());
                    SelectedRO = RoCode.get(Spinner_Select_RO.getSelectedItemPosition());
                 UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                "SelectdRO", SelectedRO);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });
        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                "UserCustomRole","").equalsIgnoreCase("DEALER")){
            iv_filter.setVisibility(View.GONE);
        }
        else{
            ll_RO_spinner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    txnBundle.putString("Filter_fragment", "WetInventory");
                    try {
                        txnBundle.putString("fragment",bundle.getString("fragment"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    Filter_Screen_Fragment filter_screen = new Filter_Screen_Fragment();
//                    Filter_Fragment_TextView filter_screen = new Filter_Fragment_TextView();
                    fragment_filter_textview filter_screen = new fragment_filter_textview();
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);
                }
            });
        }
        try {
            try {
                if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")){
                    try {
                        tv_select_filter.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "ROValue",""));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    CallPumpStatus();
                    CallTankStatus();
                }
                else{
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd_Name","").equalsIgnoreCase("")){
                        tv_select_filter.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName",""));
                        try {
                            if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
                                if(bundle.getString("fragment").equalsIgnoreCase("tankstatus")){
                                    CallTankStatus();
                                }else if(bundle.getString("fragment").equalsIgnoreCase("pumpstatus")){
                                    CallPumpStatus();
                                }else{
                                    CallPumpStatus();
                                    CallTankStatus();
                                }
                            }else{
                                CallPumpStatus();
                                CallTankStatus();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            CallPumpStatus();
                            CallTankStatus();
                        }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if (!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO", "").equalsIgnoreCase("")) {
                            tv_select_filter.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredROName",""));
                            try {
                                if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
                                    if(bundle.getString("fragment").equalsIgnoreCase("tankstatus")){
                                        CallTankStatus();
                                    }else if(bundle.getString("fragment").equalsIgnoreCase("pumpstatus")){
                                        CallPumpStatus();
                                    }else{
                                        CallPumpStatus();
                                        CallTankStatus();
                                    }
                                }else{
                                    CallPumpStatus();
                                    CallTankStatus();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                CallPumpStatus();
                                CallTankStatus();
                            }
                        } else {

                        }
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        tv_select_filter.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName",""));
                        try {
                            if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
                                if(bundle.getString("fragment").equalsIgnoreCase("tankstatus")){
                                    CallTankStatus();
                                }else if(bundle.getString("fragment").equalsIgnoreCase("pumpstatus")){
                                    CallPumpStatus();
                                }else{
                                    CallPumpStatus();
                                    CallTankStatus();
                                }
                            }else{
                                CallPumpStatus();
                                CallTankStatus();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            CallPumpStatus();
                            CallTankStatus();
                        }
                    }
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")||UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("Select RO")){
//
//                    }else{
//                        tv_select_filter.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredROName",""));
//                        try {
//                            if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
//        //                        try {
//        //                            if (ConstantDeclaration.gifProgressDialog.isShowing())
//        //                                ConstantDeclaration.gifProgressDialog.dismiss();
//        //                        } catch (Exception e) {
//        //                            e.printStackTrace();
//        //                            try {
//        //                                ConstantDeclaration.gifProgressDialog.dismiss();
//        //                            } catch (Exception ex) {
//        //                                ex.printStackTrace();
//        //                            }
//        //                        }
////                                if (ConstantDeclaration.gifProgressDialog != null) {
////                                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
////                                        ConstantDeclaration.showGifProgressDialog(getActivity());
////                                    }else{
////                                      ConstantDeclaration.gifProgressDialog.dismiss();
////                                    }
////                                } else {
////                                    ConstantDeclaration.showGifProgressDialog(getActivity());
////                                }
//                                CallTankStatus();
////                                CallPumpStatus();
//
//                            }else{
//                                CallPumpStatus();
//                                CallTankStatus();
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                            CallPumpStatus();
//                            CallTankStatus();
//                        }
//
//                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }
    @Override
    public void onResume() {
        super.onResume();
//        if (DashBoardActivity.foreground) {
//            for (int i = 0; i < RoList.size(); i++) {
//                if (!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserRole", "").equalsIgnoreCase("Dealer")) {
//                    Spinner_Select_RO.setSelection(0);
////                SelectedRO = RoList.get(Spinner_Select_RO.getSelectedItemPosition());
//                    try {
//                        SelectedRO = RoCode.get(Spinner_Select_RO.getSelectedItemPosition());
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "SelectdRO_NEW", "ALL");
//                } else {
//                    if (RoList.get(i).contains(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "SelectdRO", ""))) {
//                        Spinner_Select_RO.setSelection(i);
//                    }
//                }
//
//            }

//        }
//        else {
//            // You just came from the background
//            for (int i = 0; i < RoList.size(); i++) {
//
//                if (RoList.get(i).contains(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "SelectdRO", ""))) {
//                    Spinner_Select_RO.setSelection(i);
//                }
//
//            }
//
//
//        }
//        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                "FilteredRO","").equalsIgnoreCase("")){
//            tv_select_filter.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "FilteredROName",""));
//            CallPumpStatus();
//            CallTankStatus();
//        }else{
//
//        }
//        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                "UserCustomRole","").equalsIgnoreCase("DEALER")){
//            tv_select_filter.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "ROValue",""));
//            CallPumpStatus();
//            CallTankStatus();
//        }
//        else{
//            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "FilteredRO","").equalsIgnoreCase("")||UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "FilteredRO","").equalsIgnoreCase("Select RO")){
//
//            }else{
//                tv_select_filter.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredROName",""));
//                try {
//                    if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
////                        try {
////                            if (ConstantDeclaration.gifProgressDialog.isShowing())
////                                ConstantDeclaration.gifProgressDialog.dismiss();
////                        } catch (Exception e) {
////                            e.printStackTrace();
////                            try {
////                                ConstantDeclaration.gifProgressDialog.dismiss();
////                            } catch (Exception ex) {
////                                ex.printStackTrace();
////                            }
////                        }
//                        CallPumpStatus();
//                        CallTankStatus();
//
//                    }else{
//                        CallPumpStatus();
//                        CallTankStatus();
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                    CallPumpStatus();
//                    CallTankStatus();
//                }
//
//            }
//
//        }
    }
    private void intializeView(View view) {
        recycler_grid_tank_status = (RecyclerView) view.findViewById(R.id.recycler_grid_tank_status);
        recycler_grid_pump_nozzle_status = (RecyclerView) view.findViewById(R.id.recycler_grid_pump_nozzle_status);
        tv_No_data_tank = (TextView) view.findViewById(R.id.tv_no_data_tank);
        tv_No_data_pump = (TextView) view.findViewById(R.id.tv_No_data_pump);
        Spinner_Select_RO = view.findViewById(R.id.Spinner_Select_RO);
        tv_tank_status = view.findViewById(R.id.tv_tank_status);
        ll_RO_spinner = view.findViewById(R.id.ll_RO_spinner);
        iv_filter = view.findViewById(R.id.iv_filter);
        tv_select_filter = view.findViewById(R.id.tv_select_filter);
        tv_select_filter.setSelected(true);
        tv_pump_nozzle_status = view.findViewById(R.id.tv_pump_nozzle_status);
        RecyclerView.LayoutManager mLayoutManager = new GridLayoutManager(getActivity(), 2);
        RecyclerView.LayoutManager mLayoutManager1 = new GridLayoutManager(getActivity(), 2);
//        RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
        recycler_grid_tank_status.setLayoutManager(mLayoutManager);
        recycler_grid_pump_nozzle_status.setLayoutManager(mLayoutManager1);
        recycler_grid_tank_status.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(getResources().getInteger(R.integer.dashboard_grid_transfer)), true));
        recycler_grid_tank_status.setItemAnimator(new DefaultItemAnimator());
        recycler_grid_pump_nozzle_status.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(getResources().getInteger(R.integer.dashboard_grid_transfer)), true));
        recycler_grid_pump_nozzle_status.setItemAnimator(new DefaultItemAnimator());
        String[] tank_name_array = {"HSD", "MS", "POWER", "TURBOJET"};
        String[] tank_size_array= {"30000L", "45000L", "20000L", "60000L"};
        String[] tank_percentage_array= {"30%", "45%", "20%", "60%"};
        String[] n1= {"N1", "N1", "N1", "N1"};
        String[] n2= {"N2", "N2", "N2", "N2"};
        String[] n3= {"N3", "N3", "N3", "N3"};
        String[] n4= {"N4", "N4", "N4", "N4"};



        tank_model_new = new ArrayList<tank_model_new>();
        pump_model_new = new ArrayList<Pump_model_new>();
//        CallTankStatus();

        for (int i = 0; i < 4; i++) {
//            pump_nozzle_models.add(new Pump_Nozzle_model(
//                    tank_name_array[i],
//                    n1[i],
//                    n2[i],
//                    n3[i],
//                    n4[i],
//                    pump_images[i],
//                    n1_images[i],
//                    n2_images[i],
//                    n3_images[i],
//                    n4_images[i]
//            ));
        }



    }
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }
    private void CallPumpStatus(){
        try {
            JSONObject json = new JSONObject();
//            try {
//                if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Zone")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Region")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "Selectdzone", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdRegion", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//                }
//                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Region")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdRegion", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//
//                }
//                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//
//                }
//                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//                }
//                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("DEALER")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "ROKey","") );
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            try {
                if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Zone")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                            }

                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Region")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray2 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("SalesArea", jsonObject2);
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdSalesArea",""));
                                String[] myArray3 = "ALL".split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("ROCode", jsonObject3);
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray2 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "Selectdzone", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone","").split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("Zone", jsonObject2 );
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "Selectdzone", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject3 = new JSONArray(myArray3);
                                    json.put("Region", jsonObject3 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray4 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject4 = new JSONArray(myArray4);
                                    json.put("SalesArea", jsonObject4 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject5 = new JSONArray(myArray5);
                            json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject5 = new JSONArray(myArray5);
                            json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }



                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }

                }
                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Region")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray = "ALL".split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                            }
                            else{
                                String[] myArray = "ALL".split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){

                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion",""));
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea",""));
                                json.put("ROCode", "ALL");
                            }else{
                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("Region", jsonObject2 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject3 = new JSONArray(myArray3);
                                    json.put("SalesArea", jsonObject3);
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }

                }
                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLREGION"))
                {
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){

                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray1);
                                json.put("SalesArea", jsonObject1);
                                json.put("ROCode", jsonObject2);
                            }
                            else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject2);
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                }
                                else {
                                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray);
                                    json.put("SalesArea", jsonObject1 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }
                        else{
//                            String[] myArray1 = "ALL".split(",");
//                            JSONArray jsonObject2 = new JSONArray(myArray1);
//                            json.put("SalesArea", jsonObject2 );
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }

                        //                    try {
                        //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                        //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                        //
                        //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "SelectdSalesArea",""));
                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "FilteredRO",""));
                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                    "FirstFilteredRO", "");
                        //                        }else{
                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "FilteredRO",""));
                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                    "FirstFilteredRO", "");
                        //                        }
                        //                    } catch (Exception e) {
                        //                        e.printStackTrace();
                        //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                "FilteredRO",""));
                        //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                "FirstFilteredRO", "");
                        //                    }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FilteredRO",""));
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){

                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "ROKey","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            new AsyncPumpStatus().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void methodDone() {
        if(respCode.equalsIgnoreCase("401")){
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        }

    }

    @Override
    public void methodCancel() {

    }

    private class AsyncPumpStatus extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncPumpStatus() {
        }

        public AsyncPumpStatus(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
//                if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
//                    if (ConstantDeclaration.gifProgressDialog != null) {
//                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
//                            ConstantDeclaration.showGifProgressDialog(getActivity());
//                        }
//                    } else {
//                        ConstantDeclaration.showGifProgressDialog(getActivity());
//                    }
//                    if (ConstantDeclaration.gifProgressDialog != null) {
//                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
//                            ConstantDeclaration.showGifProgressDialog(getActivity());
//                        }
//                    } else {
//                        ConstantDeclaration.showGifProgressDialog(getActivity());
//                    }
//                    ConstantDeclaration.showGifProgressDialog(getActivity());
//
//                }else{
                    if (ConstantDeclaration.gifProgressDialog != null) {
                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                            ConstantDeclaration.showGifProgressDialog(getActivity());
                        }
                    } else {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
//                }

            } catch (Exception e) {
                e.printStackTrace();
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
//            return ClsWebService_hpcl.PostObjecttoken("PumpTank/PumpStatus", false, jsonObjects[0],"");
            return ClsWebService_hpcl.PostObjecttoken("DashboardV2/GetPumpStatus", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            try {
//                if (ConstantDeclaration.gifProgressDialog.isShowing())
//                    ConstantDeclaration.gifProgressDialog.dismiss();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
//                            try {
//                                if (ConstantDeclaration.gifProgressDialog.isShowing())
//                                    ConstantDeclaration.gifProgressDialog.dismiss();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
                            JSONObject jsonObject = new JSONObject(str[2]);
                            arraydata = new JSONArray(jsonObject.getString("Data"));
                            if(arraydata.length() != 0){
                                pump_model_new.clear();
                                tv_No_data_pump.setVisibility(View.GONE);
                                for (int i = 0; i < arraydata.length(); i++) {
                                    pump_model_new.add(new Pump_model_new(
                                            arraydata.getJSONObject(i).getString("ROCode"),
                                            arraydata.getJSONObject(i).getString("ROName"),
                                            arraydata.getJSONObject(i).getString("PumpNumber"),
                                            arraydata.getJSONObject(i).getString("NozzleNumber"),
                                            arraydata.getJSONObject(i).getString("Nozzles"),
                                            arraydata.getJSONObject(i).getString("DUType"),
                                            arraydata.getJSONObject(i).getString("DUMake"),
                                            arraydata.getJSONObject(i).getString("DUNo"),
                                            arraydata.getJSONObject(i).getString("TransactionDateTime"),
                                            arraydata.getJSONObject(i).getString("Product"),
                                            arraydata.getJSONObject(i).getString("Price"),
                                            arraydata.getJSONObject(i).getString("Quantity"),
                                            arraydata.getJSONObject(i).getString("Amount"),
                                            arraydata.getJSONObject(i).getString("Online")
                                    ));
                                    adapter1 = new pump_status_adapter(pump_model_new, getActivity());
                                    adapter1.notifyDataSetChanged();
//                                    try {
//                                        if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
//                                            try {
//                                                if (ConstantDeclaration.gifProgressDialog.isShowing())
//                                                    ConstantDeclaration.gifProgressDialog.dismiss();
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                                try {
//                                                    ConstantDeclaration.gifProgressDialog.dismiss();
//                                                } catch (Exception ex) {
//                                                    ex.printStackTrace();
//                                                }
//                                            }
//
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
                                    try {
                                        if (ConstantDeclaration.gifProgressDialog.isShowing())
                                            ConstantDeclaration.gifProgressDialog.dismiss();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    recycler_grid_pump_nozzle_status.setAdapter(adapter1);
                                    recycler_grid_pump_nozzle_status.invalidate();

                                }
                            }else{
                                pump_model_new.clear();
                                adapter1.notifyDataSetChanged();
                                recycler_grid_pump_nozzle_status.invalidate();
                                tv_No_data_pump.setVisibility(View.VISIBLE);


                            }


//                            ArrayList<pump_list> pump_nozzle_array = new ArrayList<>();
//                            ArrayList<pump_list> nozzle_array = new ArrayList<>();
//                            List<Nozzle_status_list> nozzleArray = new ArrayList<>();
//                            adapter2 = new pump_adapter(arraydata, getActivity(), nozzle_array);
//                            recycler_grid_pump_nozzle_status.setAdapter(adapter2);
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            pump_model_new.clear();
                            recycler_grid_pump_nozzle_status.invalidate();
                           tv_No_data_pump.setVisibility(View.VISIBLE);
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        if (ConstantDeclaration.gifProgressDialog.isShowing())
                            ConstantDeclaration.gifProgressDialog.dismiss();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }

    }

    private void CallTankStatus(){
        try {
            JSONObject json = new JSONObject();
//            try {
//                if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Zone")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Region")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
////                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                "FilteredRO","").equalsIgnoreCase("ALL")) {
////                            try {
////                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
////                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                                "Selectdzone", "").equalsIgnoreCase("")) {
////                                } else {
////                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                            "Selectdzone", ""));
////                                }
////                            } catch (JSONException e) {
////                                e.printStackTrace();
////                            }
////                            try {
////                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
////                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                                "SelectdRegion", "").equalsIgnoreCase("")) {
////                                } else {
////                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                            "SelectdRegion", ""));
////                                }
////                            } catch (JSONException e) {
////                                e.printStackTrace();
////                            }
////                            try {
////                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
////                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
////                                } else {
////                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                            "SelectdSalesArea", ""));
////                                }
////                            } catch (JSONException e) {
////                                e.printStackTrace();
////                            }
////                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                    "FilteredRO",""));
////                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
////                                    "FirstFilteredRO", "");
////                        }else{
////                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
////                                    "FilteredRO",""));
////                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
////                                    "FirstFilteredRO", "");
////                        }
//
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "Selectdzone", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdRegion", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//
//                }
//                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Region")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdRegion", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//
//    //                    try {
//    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//    //                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//    //                                    "SelectdRegion",""));
//    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//    //                                    "SelectdSalesArea",""));
//    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//    //                                    "FilteredRO",""));
//    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//    //                                    "FirstFilteredRO", "");
//    //                        }else{
//    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//    //                                    "FilteredRO",""));
//    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//    //                                    "FirstFilteredRO", "");
//    //                        }
//    //                    } catch (Exception e) {
//    //                        e.printStackTrace();
//    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//    //                                "FilteredRO",""));
//    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//    //                                "FirstFilteredRO", "");
//    //                    }
//
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//
//                }
//                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//
//                }
//                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//                }
//                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("DEALER")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "ROKey","") );
//                }
//            }
//            catch (JSONException e) {
//                e.printStackTrace();
//            }
            try {
                if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Zone")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                            }

                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Region")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray2 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("SalesArea", jsonObject2);
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdSalesArea",""));
                                String[] myArray3 = "ALL".split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("ROCode", jsonObject3);
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray2 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "Selectdzone", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone","").split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("Zone", jsonObject2 );
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "Selectdzone", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject3 = new JSONArray(myArray3);
                                    json.put("Region", jsonObject3 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray4 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject4 = new JSONArray(myArray4);
                                    json.put("SalesArea", jsonObject4 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject5 = new JSONArray(myArray5);
                            json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject5 = new JSONArray(myArray5);
                            json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }



                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }

                }
                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Region")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray = "ALL".split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                            }
                            else{
                                String[] myArray = "ALL".split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){

                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion",""));
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea",""));
                                json.put("ROCode", "ALL");
                            }else{
                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("Region", jsonObject2 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject3 = new JSONArray(myArray3);
                                    json.put("SalesArea", jsonObject3);
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }

                }
                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLREGION"))
                {
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){

                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray1);
                                json.put("SalesArea", jsonObject1);
                                json.put("ROCode", jsonObject2);
                            }
                            else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject2);
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                }
                                else {
                                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray);
                                    json.put("SalesArea", jsonObject1 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }
                        else{
//                            String[] myArray1 = "ALL".split(",");
//                            JSONArray jsonObject2 = new JSONArray(myArray1);
//                            json.put("SalesArea", jsonObject2 );
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }

                        //                    try {
                        //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                        //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                        //
                        //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "SelectdSalesArea",""));
                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "FilteredRO",""));
                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                    "FirstFilteredRO", "");
                        //                        }else{
                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "FilteredRO",""));
                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                    "FirstFilteredRO", "");
                        //                        }
                        //                    } catch (Exception e) {
                        //                        e.printStackTrace();
                        //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                "FilteredRO",""));
                        //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                "FirstFilteredRO", "");
                        //                    }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FilteredRO",""));
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){

                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "ROKey","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            new AsyncTankStatus().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncTankStatus extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncTankStatus() {
        }

        public AsyncTankStatus(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
//                if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
//                    if (ConstantDeclaration.gifProgressDialog != null) {
//                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
//                            ConstantDeclaration.showGifProgressDialog(getActivity());
//                        }else{
//                                ConstantDeclaration.showGifProgressDialog(getActivity());
//                        }
//                    } else {
//                        ConstantDeclaration.showGifProgressDialog(getActivity());
//                    }
////                    ConstantDeclaration.showGifProgressDialog(getActivity());
//
//                }else{
                    if (ConstantDeclaration.gifProgressDialog != null) {
                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                            ConstantDeclaration.showGifProgressDialog(getActivity());
                        }
                    } else {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
//                }

            } catch (Exception e) {
                e.printStackTrace();
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            try {
//                if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
//                    ConstantDeclaration.showGifProgressDialog(getActivity());
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            return ClsWebService_hpcl.PostObjecttoken("PumpTank/TankStatus", false, jsonObjects[0],"");
            return ClsWebService_hpcl.PostObjecttoken("DashboardV2/GetTankStatus", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
//                if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
////                        ConstantDeclaration.gifProgressDialog.dismiss();
//                }else {
                  try {
                    if (ConstantDeclaration.gifProgressDialog.isShowing())
                        ConstantDeclaration.gifProgressDialog.dismiss();
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            try {
//                    if (ConstantDeclaration.gifProgressDialog.isShowing())
//                        ConstantDeclaration.gifProgressDialog.dismiss();
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }

            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
//            dialog.dismiss();
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                         respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
//                            arraytankdata.remove(arraytankdata.length());
                            try {
                                for (int i = arraytankdata.length(); i >= 0; i--) {
                                    arraytankdata.remove(i);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            arraytankdata = new JSONArray(jsonObject.getString("Data"));
                            tank_model_new.clear();
                            if(arraytankdata.length() != 0){
                                tv_No_data_tank.setVisibility(View.GONE);
                                for (int i = 0; i < arraytankdata.length(); i++) {
                                    tank_model_new.add(new tank_model_new(
                                            arraytankdata.getJSONObject(i).getString("ROCode"),
                                            arraytankdata.getJSONObject(i).getString("ROName"),
                                            arraytankdata.getJSONObject(i).getString("TankNo"),
                                            arraytankdata.getJSONObject(i).getString("ProductCode"),
                                            arraytankdata.getJSONObject(i).getString("Product"),
                                            arraytankdata.getJSONObject(i).getString("TankName"),
                                            arraytankdata.getJSONObject(i).getString("Capacity"),
                                            arraytankdata.getJSONObject(i).getString("NetVolume"),
                                            arraytankdata.getJSONObject(i).getString("ProductHeight"),
                                            arraytankdata.getJSONObject(i).getString("Ullage"),
                                            arraytankdata.getJSONObject(i).getString("WaterHeight"),
                                            arraytankdata.getJSONObject(i).getString("Temparature"),
                                            arraytankdata.getJSONObject(i).getString("Density"),
                                            arraytankdata.getJSONObject(i).getString("Density15C"),
                                            arraytankdata.getJSONObject(i).getString("STKDateTime"),
                                            arraytankdata.getJSONObject(i).getString("Online"),
                                            arraytankdata.getJSONObject(i).getString("Current")
                                    ));
                                    adapter = new tank_status_adapter(tank_model_new, getActivity());
                                    adapter.notifyDataSetChanged();
//                                    try {
//                                        if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
//                                            try {
//                                                if (ConstantDeclaration.gifProgressDialog.isShowing())
//                                                    ConstantDeclaration.gifProgressDialog.dismiss();
//                                            } catch (Exception e) {
//                                                e.printStackTrace();
//                                                try {
//                                                    ConstantDeclaration.gifProgressDialog.dismiss();
//                                                } catch (Exception ex) {
//                                                    ex.printStackTrace();
//                                                }
//                                            }
//
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                    try {
//                                        if (ConstantDeclaration.gifProgressDialog.isShowing())
//                                            ConstantDeclaration.gifProgressDialog.dismiss();
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
                                    recycler_grid_tank_status.setAdapter(adapter);
                                    recycler_grid_tank_status.invalidate();
//                                    try {
//                                        if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
//                                            CallPumpStatus();
//                                        }
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
                                }
                                }else{
                                tank_model_new.clear();
                                adapter.notifyDataSetChanged();
                                recycler_grid_tank_status.invalidate();
                                tv_No_data_tank.setVisibility(View.VISIBLE);
                            }


                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }

                        else{
//                            try {
//                                if (ConstantDeclaration.gifProgressDialog.isShowing())
//                                    ConstantDeclaration.gifProgressDialog.dismiss();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
                            try {
                                if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
                                    CallPumpStatus();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            tank_model_new.clear();
                            recycler_grid_tank_status.invalidate();
                            tv_No_data_tank.setVisibility(View.VISIBLE);
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }else {
//                        try {
//                            if (ConstantDeclaration.gifProgressDialog.isShowing())
//                                ConstantDeclaration.gifProgressDialog.dismiss();
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
                        try {
                            if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
                                CallPumpStatus();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        universalDialog = new UniversalDialog(getActivity(), alertDialogInterface, "", getString(R.string.server_not_resp), getString(R.string.dialog_ok), "");
                        universalDialog.showAlert();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                    try {
//                        if (ConstantDeclaration.gifProgressDialog.isShowing())
//                            ConstantDeclaration.gifProgressDialog.dismiss();
//                    } catch (Exception e1) {
//                        e1.printStackTrace();
//                    }
                    try {
                        if(bundle.getString("FilteredFragment").equalsIgnoreCase("WETInventory")){
                            CallPumpStatus();
                        }
                    } catch (Exception e2) {
                        e2.printStackTrace();
                    }
                }
            }
        }

    }
    private void populateROList() {
        RoList = new ArrayList<>();
        RoCode = new ArrayList<>();
        if(UserDataPrefrence.getArrayList( "ROLIst",getContext()).size() != 1){
            Spinner_Select_RO.setEnabled(true);
//            RoList.add("Select RO");
            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserRole","").equalsIgnoreCase("Dealer")){
                RoList.add("ALL");
                RoCode.add("ALL");
            }else{
                RoList.add("Select RO");
                RoCode.add("Select RO");
            }
            RoList.addAll(UserDataPrefrence.getArrayList( "ROLIst",getContext()));
            RoCode.addAll(UserDataPrefrence.getArrayList( "ROCode",getContext()));
        } else if(UserDataPrefrence.getArrayList( "ROLIst",getContext()).size() == 0){
            Spinner_Select_RO.setEnabled(false);
        } else{
            Spinner_Select_RO.setEnabled(false);
            RoList.addAll(UserDataPrefrence.getArrayList( "ROLIst",getContext()));
            RoCode.addAll(UserDataPrefrence.getArrayList( "ROCode",getContext()));
        }
    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

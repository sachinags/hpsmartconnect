package com.agstransact.HP_SC.dashboard;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.adapter.ExpandableListAdapter;
import com.agstransact.HP_SC.dashboard.fragment.Change_Mpin_new;
import com.agstransact.HP_SC.dashboard.fragment.FragmentCriticalStockList;
import com.agstransact.HP_SC.dashboard.fragment.FragmentNanoStatus;
import com.agstransact.HP_SC.dashboard.fragment.FragmentPriceExceptionList;
import com.agstransact.HP_SC.dashboard.fragment.FragmentROConnectivity;
import com.agstransact.HP_SC.dashboard.fragment.HomeFragment_old;
import com.agstransact.HP_SC.dashboard.fragment.HomeFragment_new;
import com.agstransact.HP_SC.dashboard.fragment.PumpMRFragment;
import com.agstransact.HP_SC.dashboard.fragment.PumpMRMAFragment;
import com.agstransact.HP_SC.dashboard.fragment.PumpManualModeDealerFragment;
import com.agstransact.HP_SC.dashboard.fragment.PumpManualModeFragment;
import com.agstransact.HP_SC.dashboard.fragment.PumpManualModeRegionFragment;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.model.DrawerMenuModel;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.DrawerLocker;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HOSDashboardActivity extends AppCompatActivity implements AlertDialogInterface,NavigationView.OnNavigationItemSelectedListener,
        View.OnClickListener, DrawerLocker, ActionChangeFrag {
    private ImageView iv_bnv_fuel_sale,iv_bnv_wet_inventory,iv_bnv_dashbord,iv_bnv_ro_snapshot,
            iv_bnv_equipment;
    public DrawerLayout drawerLayout;
    public ActionBarDrawerToggle actionBarDrawerToggle;
    private Toolbar toolbar;
    private ImageView iv_drawer_menu;
    AlertDialogInterface dialogInterface;
    UniversalDialog universalDialog;
    Menu option_menu;
    /*toggle menu action */
    private ActionBarDrawerToggle mDrawerToggle;
    /*drawer layout of left*/
    private DrawerLayout mDrawerLayout;
    /*for closing the another expand group*/
    private int lastExpandedPosition = -1;
    /*expanded list inside drawer*/
    private ExpandableListView mExpandableListView;
    /*drawer item model list for group*/
    List<DrawerMenuModel> listDataHeader;
    /*drawer item model list for child*/
    HashMap<DrawerMenuModel, List<String>> listDataChild;
    /*drawer expanded list adapter*/
    ExpandableListAdapter listAdapter;
    Drawable navIcon = null;

    LinearLayout ll_navigation_views;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        show full screen
        try {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_hos_dashboard);
        /*intializing ID of View */
        intializeID();
        /*populating list data for expanded list of drawer */
        initItems();
        /*drawer header layout*/
        LayoutInflater inflater = getLayoutInflater();
        View listHeaderView = inflater.inflate(R.layout.drawer_header, mExpandableListView, false);
        mExpandableListView.addHeaderView(listHeaderView);
        try {
            TextView userNameTV = listHeaderView.findViewById(R.id.nav_header_tv_ro_name);
            userNameTV.setText(UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                    "FullName","")+", "+UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                    "Area",""));
            userNameTV.setSelected(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        /*Addiing drawer item */
        addDrawerItems();
        /*setup drawer for toggle the menu icon*/
        setupDrawer();
        if (savedInstanceState == null) {
            Bundle bundle = new Bundle();
            /*default fragmnet when open the activity*/
            FragmentManager fragmentManager = getSupportFragmentManager();
            //22-5
            ConstantDeclaration.shouldCallAPI = true;
//            Bundle bundle1 = new Bundle();
//            bundle1.putString(ConstantDeclaration.TXN_LIST,extras.getString(ConstantDeclaration.TXN_LIST));
//                HomeFragment homeFragment = new HomeFragment();
//                homeFragment.setArguments(bundle1);
//                addFragment(homeFragment);
            replaceScreen(new HomeFragment_old());
//            replaceScreen(new HomeFragment_new());
            int countFrag = fragmentManager.getBackStackEntryCount();
            Log.e("Count", "" + countFrag);

            actionBarDrawerToggle.setDrawerIndicatorEnabled(countFrag == 0);

        }
        navIcon = toolbar.getNavigationIcon();
        configureToolbar();
        drawerLayout = findViewById(R.id. drawer_layout ) ;
        iv_bnv_fuel_sale = (ImageView) findViewById(R.id.iv_bnv_fuel_sale);
        iv_bnv_wet_inventory = (ImageView) findViewById(R.id.iv_bnv_wet_inventory);
        iv_bnv_dashbord = (ImageView) findViewById(R.id.iv_bnv_dashbord);
        iv_bnv_ro_snapshot = (ImageView) findViewById(R.id.iv_bnv_ro_snapshot);
        iv_bnv_equipment = (ImageView) findViewById(R.id.iv_bnv_equipment);
        ll_navigation_views = (LinearLayout) findViewById(R.id.ll_navigation_views);
//        iv_bnv_fuel_sale.setOnClickListener(this);
//        iv_bnv_wet_inventory.setOnClickListener(this);
//        iv_bnv_ro_snapshot.setOnClickListener(this);
//        iv_bnv_equipment.setOnClickListener(this);
//        iv_bnv_dashbord.setOnClickListener(this);
//        iv_drawer_menu = (ImageView) findViewById(R.id.iv_drawer_menu);
//        iv_drawer_menu.setImageResource(R.drawable.drawer_icon);
//        iv_drawer_menu.setOnClickListener(this);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawerLayout , toolbar , R.string.nav_open ,
//                R.string.nav_close ) ;
//        drawerLayout.addDrawerListener(toggle) ;
//        dialogInterface = this;
//        toggle.syncState() ;
//        NavigationView navigationView = findViewById(R.id.nav_view) ;
//        navigationView.setNavigationItemSelectedListener(this) ;

    }

    private void intializeID() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        try {
            setSupportActionBar(toolbar);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.drawer_icon);
        }
        /*remove toolbar title*/
        getSupportActionBar().setDisplayShowTitleEnabled(false);
//        actionBar.setDisplayHomeAsUpEnabled(true);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mExpandableListView = (ExpandableListView) findViewById(R.id.navList);
    }
    private void initItems() {
        // preparing list data
        listDataHeader = new ArrayList<>();
        listDataChild = new HashMap<>();

        // Adding group data
        if(!UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                "UserCustomRole","").equalsIgnoreCase("DEALER")) {
            DrawerMenuModel drawerMenuModel = new DrawerMenuModel();
            drawerMenuModel.setMenuIcon(R.drawable.dashboard_icon);
            drawerMenuModel.setMenuName(getString(R.string.Dashbord));

            listDataHeader.add(drawerMenuModel);


            DrawerMenuModel drawerMenuModel1 = new DrawerMenuModel();
            drawerMenuModel1.setMenuIcon(R.drawable.price_exception_list);
            drawerMenuModel1.setMenuName(getString(R.string.price_exception_list));

            listDataHeader.add(drawerMenuModel1);

            DrawerMenuModel drawerMenuModel2 = new DrawerMenuModel();
            drawerMenuModel2.setMenuIcon(R.drawable.ro_connectivity_list);
            drawerMenuModel2.setMenuName(getString(R.string.ro_connectivity_list));

            listDataHeader.add(drawerMenuModel2);

            DrawerMenuModel drawerMenuModel3 = new DrawerMenuModel();
            drawerMenuModel3.setMenuIcon(R.drawable.crictical_stock);
            drawerMenuModel3.setMenuName(getString(R.string.crictical_stock));

            listDataHeader.add(drawerMenuModel3);

            DrawerMenuModel drawerMenuModel4 = new DrawerMenuModel();
            drawerMenuModel4.setMenuIcon(R.drawable.nano_status_ro_list);
            drawerMenuModel4.setMenuName(getString(R.string.nano_status_ro_list));

            listDataHeader.add(drawerMenuModel4);

            DrawerMenuModel drawerMenuModel5 = new DrawerMenuModel();
            drawerMenuModel5.setMenuIcon(R.drawable.pump_m_r);
            drawerMenuModel5.setMenuName(getString(R.string.pump_m_r));

            listDataHeader.add(drawerMenuModel5);

            DrawerMenuModel drawerMenuModel6 = new DrawerMenuModel();
            drawerMenuModel6.setMenuIcon(R.drawable.pump_manual_mode);
            drawerMenuModel6.setMenuName(getString(R.string.pump_manual_mode));

            listDataHeader.add(drawerMenuModel6);

            DrawerMenuModel drawerMenuModel7 = new DrawerMenuModel();
            drawerMenuModel7.setMenuIcon(R.drawable.change_mpin);
            drawerMenuModel7.setMenuName(getString(R.string.change_mpin));

            listDataHeader.add(drawerMenuModel7);
            DrawerMenuModel drawerMenuModel8 = new DrawerMenuModel();
            drawerMenuModel8.setMenuIcon(R.drawable.log_out);
            drawerMenuModel8.setMenuName(getString(R.string.logout));

            listDataHeader.add(drawerMenuModel8);

            // Adding child data
            List<String> pump_mnr_child = new ArrayList<>();


            pump_mnr_child.add(getString(R.string.pump_mnr_request));
            pump_mnr_child.add(getString(R.string.manager_approval));

            listDataChild.put(listDataHeader.get(5), pump_mnr_child); // Header, Child data

            // Adding child data
            List<String> pump_manual_mode_child = new ArrayList<>();

            pump_manual_mode_child.add(getString(R.string.Sales_area_manager_approval));
            pump_manual_mode_child.add(getString(R.string.Regional_manager_approval));


            listDataChild.put(listDataHeader.get(6), pump_manual_mode_child); // Header, Child data


        }
        else{

            DrawerMenuModel drawerMenuModel = new DrawerMenuModel();
            drawerMenuModel.setMenuIcon(R.drawable.dashboard_icon);
            drawerMenuModel.setMenuName(getString(R.string.Dashbord));

            listDataHeader.add(drawerMenuModel);


            DrawerMenuModel drawerMenuModel1 = new DrawerMenuModel();
            drawerMenuModel1.setMenuIcon(R.drawable.price_exception_list);
            drawerMenuModel1.setMenuName(getString(R.string.price_exception_list));

            listDataHeader.add(drawerMenuModel1);

            DrawerMenuModel drawerMenuModel2 = new DrawerMenuModel();
            drawerMenuModel2.setMenuIcon(R.drawable.ro_connectivity_list);
            drawerMenuModel2.setMenuName(getString(R.string.ro_connectivity_list));

            listDataHeader.add(drawerMenuModel2);

            DrawerMenuModel drawerMenuModel3 = new DrawerMenuModel();
            drawerMenuModel3.setMenuIcon(R.drawable.crictical_stock);
            drawerMenuModel3.setMenuName(getString(R.string.crictical_stock));

            listDataHeader.add(drawerMenuModel3);

            DrawerMenuModel drawerMenuModel4 = new DrawerMenuModel();
            drawerMenuModel4.setMenuIcon(R.drawable.nano_status_ro_list);
            drawerMenuModel4.setMenuName(getString(R.string.nano_status_ro_list));

            listDataHeader.add(drawerMenuModel4);

//            DrawerMenuModel drawerMenuModel5 = new DrawerMenuModel();
//            drawerMenuModel5.setMenuIcon(R.drawable.pump_m_r);
//            drawerMenuModel5.setMenuName(getString(R.string.pump_m_r));
//
//            listDataHeader.add(drawerMenuModel5);

            DrawerMenuModel drawerMenuModel5 = new DrawerMenuModel();
            drawerMenuModel5.setMenuIcon(R.drawable.pump_manual_mode);
            drawerMenuModel5.setMenuName(getString(R.string.pump_manual_mode));

            listDataHeader.add(drawerMenuModel5);

            DrawerMenuModel drawerMenuModel6 = new DrawerMenuModel();
            drawerMenuModel6.setMenuIcon(R.drawable.change_mpin);
            drawerMenuModel6.setMenuName(getString(R.string.change_mpin));

            listDataHeader.add(drawerMenuModel6);
            DrawerMenuModel drawerMenuModel7 = new DrawerMenuModel();
            drawerMenuModel7.setMenuIcon(R.drawable.log_out);
            drawerMenuModel7.setMenuName(getString(R.string.logout));

            listDataHeader.add(drawerMenuModel7);

            // Adding child data
            List<String> pump_manual_mode_child = new ArrayList<>();
            pump_manual_mode_child.add(getString(R.string.manual_mode_request));
            listDataChild.put(listDataHeader.get(5), pump_manual_mode_child); // Header, Child data


        }

    }
    private void addDrawerItems() {
        listAdapter = new ExpandableListAdapter(this, listDataHeader, listDataChild);
        mExpandableListView.setAdapter(listAdapter);

        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long l) {

                /*if (groupPosition == 0) {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    ConstantDeclaration.replaceFragment(new HomeFragment(), fragmentManager);

                    int countFrag = fragmentManager.getBackStackEntryCount();
                    Log.e("Count", "" + countFrag);

//                    mDrawerToggle.setDrawerIndicatorEnabled(countFrag == 0);
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                }*/
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                    if (groupPosition == 0) {
                        replaceScreen(new HomeFragment_old());
                        mDrawerLayout.closeDrawer(GravityCompat.START);

                    }
                    else if (groupPosition == 1) {
                        replaceScreen(new FragmentPriceExceptionList());
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 2) {
                        replaceScreen(new FragmentROConnectivity());
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 3) {
                        replaceScreen(new FragmentCriticalStockList());
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 4) {
                        replaceScreen(new FragmentNanoStatus());
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 7) {
//                    replaceScreen(new FragmentCriticalStockList());
//                    Change_Mpin aboout_us_Fragment = new Change_Mpin();
//                    Change_Mpin_new aboout_us_Fragment = new Change_Mpin_new();
//                    Bundle bundle = new Bundle();
//                    aboout_us_Fragment.setArguments(bundle);
//                    addFragment(aboout_us_Fragment);
                        replaceScreen(new Change_Mpin_new());
//                    universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Coming Soon...", getString(R.string.dialog_ok), "");
//                    universalDialog.showAlert();
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 8) {
                        logOut();
                    }
                }
                else{
                    if (groupPosition == 0) {
                        replaceScreen(new HomeFragment_old());
                        mDrawerLayout.closeDrawer(GravityCompat.START);

                    }
                    else if (groupPosition == 1) {
                        replaceScreen(new FragmentPriceExceptionList());
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 2) {
                        replaceScreen(new FragmentROConnectivity());
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 3) {
                        replaceScreen(new FragmentCriticalStockList());
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 4) {
                        replaceScreen(new FragmentNanoStatus());
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 6) {
//                    replaceScreen(new FragmentCriticalStockList());
//                    Change_Mpin aboout_us_Fragment = new Change_Mpin();
//                    Change_Mpin_new aboout_us_Fragment = new Change_Mpin_new();
//                    Bundle bundle = new Bundle();
//                    aboout_us_Fragment.setArguments(bundle);
//                    addFragment(aboout_us_Fragment);
                        replaceScreen(new Change_Mpin_new());
//                    universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Coming Soon...", getString(R.string.dialog_ok), "");
//                    universalDialog.showAlert();
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                    }
                    else if (groupPosition == 7) {
                        logOut();
                    }
                }


                return false;
            }
        });
        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                /*for closing the all group has expanded*/
                if (lastExpandedPosition != -1
                        && groupPosition != lastExpandedPosition) {
                    mExpandableListView.collapseGroup(lastExpandedPosition);
                }
                lastExpandedPosition = groupPosition;
            }
        });

        mExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

                /*At time of clollapse */
            }
        });

        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v,
                                        int groupPosition, int childPosition, long id) {
                int index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition));
                parent.setItemChecked(index, true);
                String childText = (String) listAdapter.getChild(groupPosition, childPosition);
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                    if (groupPosition == 5) {
                        if (childText.equalsIgnoreCase(getString(R.string.pump_mnr_request))) {
//                            universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Coming Soon...", getString(R.string.dialog_ok), "");
//                            universalDialog.showAlert();
//                            mDrawerLayout.closeDrawer(GravityCompat.START);
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                                    "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                                        "FilteredRO", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                                        "FilteredRO", "").equalsIgnoreCase("Select RO") || UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                                        "FirstFilteredRO", "").equalsIgnoreCase("ALL") || UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
                                        "FilteredRO", "").equalsIgnoreCase("ALL")) {
                            universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Please Select RO", getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                            mDrawerLayout.closeDrawer(GravityCompat.START);
                                }
                                else {
                                    replaceScreen(new PumpMRFragment());
                                    mDrawerLayout.closeDrawer(GravityCompat.START);
                                }
                            }
                            else{
                                replaceScreen(new PumpMRFragment());
                                mDrawerLayout.closeDrawer(GravityCompat.START);
                            }

                        }
                        else if (childText.equalsIgnoreCase(getString(R.string.manager_approval))) {
//                        addFragment(new ChangePasswordFragment());
//                            universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Coming Soon...", getString(R.string.dialog_ok), "");
//                            universalDialog.showAlert();
//                            mDrawerLayout.closeDrawer(GravityCompat.START);
                            replaceScreen(new PumpMRMAFragment());
                            mDrawerLayout.closeDrawer(GravityCompat.START);
                        }
                    }
                    else if (groupPosition == 6) {
                        if (childText.equalsIgnoreCase(getString(R.string.Sales_area_manager_approval))) {
//                        addFragment(new AcBalanceInquiryFragment());
//                            universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Coming Soon...", getString(R.string.dialog_ok), "");
//                            universalDialog.showAlert();
//                            mDrawerLayout.closeDrawer(GravityCompat.START);
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
//                                    "UserCustomRole","").equalsIgnoreCase("DEALER")) {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
//                                        "FilteredRO", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
//                                        "FilteredRO", "").equalsIgnoreCase("Select RO") || UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
//                                        "FirstFilteredRO", "").equalsIgnoreCase("ALL") || UserDataPrefrence.getPreference("HPCL_Preference", getApplicationContext(),
//                                        "FilteredRO", "").equalsIgnoreCase("ALL")) {
//                                    universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Please Select RO", getString(R.string.dialog_ok), "");
//                                    universalDialog.showAlert();
//                                    mDrawerLayout.closeDrawer(GravityCompat.START);
//                                }
//                                else {
//                                    replaceScreen(new PumpManualModeFragment());
//                                    mDrawerLayout.closeDrawer(GravityCompat.START);
//                                }
//                            }
//                            else{
                                replaceScreen(new PumpManualModeFragment());
                                mDrawerLayout.closeDrawer(GravityCompat.START);
//                            }
                        }
                        else if (childText.equalsIgnoreCase(getString(R.string.Regional_manager_approval))) {
//                        addFragment(new AcLimitSettingFragment());
//                            universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Coming Soon...", getString(R.string.dialog_ok), "");
//                            universalDialog.showAlert();
//                            mDrawerLayout.closeDrawer(GravityCompat.START);
                                replaceScreen(new PumpManualModeRegionFragment());
                                mDrawerLayout.closeDrawer(GravityCompat.START);

                        }

                    }
                }
                else{
                    if (groupPosition == 5) {
                        if (childText.equalsIgnoreCase(getString(R.string.manual_mode_request))) {
//                        addFragment(new AcBalanceInquiryFragment());
//                            universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Coming Soon...", getString(R.string.dialog_ok), "");
//                            universalDialog.showAlert();
//                            mDrawerLayout.closeDrawer(GravityCompat.START);
                                replaceScreen(new PumpManualModeDealerFragment());
                                mDrawerLayout.closeDrawer(GravityCompat.START);

                        }
                    }

                }

                mDrawerLayout.closeDrawer(GravityCompat.START);
                return false;
            }
        });
    }
    private void setupDrawer() {

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);

        getSupportFragmentManager().addOnBackStackChangedListener(mOnBackStackChangedListener);
    }

    private FragmentManager.OnBackStackChangedListener
            mOnBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            int countFrag = getSupportFragmentManager().getBackStackEntryCount();

            Log.e("Count", "" + countFrag);
            Fragment frag = getSupportFragmentManager().findFragmentById(R.id.dashboard_container);
            String fragname = null;
            if (frag != null) {
                fragname = frag.getClass().getSimpleName();
            }
//            actionBarDrawerToggle.setDrawerIndicatorEnabled(countFrag == 1);
            try {
                if (fragname != null && (fragname.equalsIgnoreCase("HomeFragment_old")||
                        fragname.equalsIgnoreCase("HomeFragment_new") ||
                        fragname.equalsIgnoreCase("FragmentFuelSales")||
                        fragname.equalsIgnoreCase("FragmentWetInventory")||
                        fragname.equalsIgnoreCase("FragmentROSnapsahot")||
                        fragname.equalsIgnoreCase("Fragmnet_Equipment"))) {
                    actionBarDrawerToggle.setDrawerIndicatorEnabled(countFrag == 0);
                    actionBarDrawerToggle.setDrawerIndicatorEnabled(true);
                    TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
                    ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
//                    ll_navigation_views.setVisibility(View.VISIBLE);
                    toolbarIv.setVisibility(View.VISIBLE);
                    toolbarTV.setVisibility(View.GONE);
                    //show drawer icon and menu options
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                option_menu.findItem(R.id.action_notification).setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);
                    try {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
                        getSupportActionBar().setDisplayShowHomeEnabled(true);
                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else {
                    actionBarDrawerToggle.setDrawerIndicatorEnabled(countFrag == 1);
                    ll_navigation_views.setVisibility(View.GONE);
                    TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
                    ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
                    toolbarIv.setVisibility(View.GONE);
                    toolbarTV.setVisibility(View.VISIBLE);
//                    ActionBar actionBar = getSupportActionBar();
//                    if (actionBar != null) {
//                        actionBar.setHomeAsUpIndicator(R.drawable.back_button);
//                    }
//                    setSupportActionBar(toolbar);
//                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                    getSupportActionBar().setDisplayShowHomeEnabled(true);
//                    getSupportActionBar().setDisplayShowTitleEnabled(false);
//                    actionBar.setC(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View v) {
//                            //What to do on back clicked
//                        }
//                    });
//                    if (actionBar != null) {
//                        actionBar.setHomeAsUpIndicator(R.drawable.drawer_icon);
//                    }
                    /*remove toolbar title*/
//                    getSupportActionBar().setDisplayShowTitleEnabled(false);
//                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
//                    ActionBar actionBar = getSupportActionBar();
//                    getSupportActionBar().setDisplayShowTitleEnabled(false);

//                    getSupportActionBar().setDisplayShowHomeEnabled(false);
//                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);

                    //hide drawer icon and menu options
//                    if (fragname != null && (fragname.equalsIgnoreCase("SuccessfulFragment"))) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                option_menu.findItem(R.id.action_notification).setVisible(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);

                    try {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        actionBarDrawerToggle.syncState();
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        actionBarDrawerToggle.onConfigurationChanged(newConfig);
    }
    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.dashboard_container, frag, "LAUNCH");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("LAUNCH");
        fragmentTransaction.commit();

    }

    private void configureToolbar() {
//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().hide();
//        ActionBar actionbar = getSupportActionBar();
//        actionbar.setHomeAsUpIndicator(R.drawable.drawer_icon);
//        actionbar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

//        switch (item.getItemId()) {
//            case android.R.id.home:
//                if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
//                    getSupportFragmentManager().popBackStack();
//                }
//                else{
//                    Fragment frag = getSupportFragmentManager().findFragmentById(R.id.dashboard_container);
//                    String fragname = null;
//
//                    if (frag != null) {
//                        fragname = frag.getClass().getSimpleName();
//
//                        if (fragname.equalsIgnoreCase("HomeFragment"))
//                        {
//                            TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//                            ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
//                            toolbarIv.setVisibility(View.VISIBLE);
//                            toolbarTV.setVisibility(View.GONE);
//                            //show drawer icon and menu options
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    try {
//                                        option_menu.findItem(R.id.action_notification).setVisible(true);
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }, 500);
//                            try {
//                                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                                getSupportActionBar().setDisplayShowHomeEnabled(true);
//                                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        else if (fragname.equalsIgnoreCase("FragmentFuelSales")||
//                                fragname.equalsIgnoreCase("FragmentWetInventory")||
//                                fragname.equalsIgnoreCase("FragmentROSnapsahot")||
//                                fragname.equalsIgnoreCase("Fragmnet_Equipment"))
//                        {
//                            TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//                            ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
//                            toolbarIv.setVisibility(View.VISIBLE);
//                            toolbarTV.setVisibility(View.GONE);
//                            //show drawer icon and menu options
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    try {
//                                        option_menu.findItem(R.id.action_notification).setVisible(true);
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }, 500);
//                            try {
//                                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                                getSupportActionBar().setDisplayShowHomeEnabled(true);
//                                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//
//                        }
//                        else{
//                            TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//                            ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
//                            toolbarIv.setVisibility(View.GONE);
//                            toolbarTV.setVisibility(View.VISIBLE);
//                            ActionBar actionBar = getSupportActionBar();
////                            if (actionBar != null) {
////                                actionBar.setHomeAsUpIndicator(R.drawable.back_press);
////                            }
//                            //hide drawer icon and menu options
////                    if (fragname != null && (fragname.equalsIgnoreCase("SuccessfulFragment"))) {
//                            Handler handler = new Handler();
//                            handler.postDelayed(new Runnable() {
//                                @Override
//                                public void run() {
//                                    try {
//                                        option_menu.findItem(R.id.action_notification).setVisible(false);
//                                    } catch (Exception e) {
//                                        e.printStackTrace();
//                                    }
//                                }
//                            }, 500);
//
////                    }
////                    updateDrawer(false);
//                            try {
//                                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                }
//                break;
//
//        }

//        int itemID = item.getItemId();
//        if (actionBarDrawerToggle.onOptionsItemSelected(item)) {
//            return true;
//        }
        return actionBarDrawerToggle.isDrawerIndicatorEnabled() && actionBarDrawerToggle.onOptionsItemSelected(item)
                || item.getItemId() == android.R.id.home && getSupportFragmentManager().popBackStackImmediate()
                || super.onOptionsItemSelected(item);
//        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
//        DrawerLayout drawer = findViewById(R.id.drawer_layout ) ;
        drawerLayout.closeDrawer(GravityCompat.START ) ;

        int itemID = item.getItemId();


        if (itemID == R.id.nav_dashboard){

            replaceScreen(new HomeFragment_old());
            return true;

        } else if (itemID == R.id.nav_price_exception_list){

            replaceScreen(new FragmentPriceExceptionList());
            return true;

        }else if (itemID == R.id.nav_ro_connectivity){

            replaceScreen(new FragmentROConnectivity());
            return true;

        }else if (itemID == R.id.nav_critical_stock){

            replaceScreen(new FragmentCriticalStockList());
            return true;

        }else if (itemID == R.id.nav_nano_status){

            replaceScreen(new FragmentNanoStatus());
            return true;

        }else if (itemID == R.id.nav_pump_mr){

        }else if (itemID == R.id.nav_pump_manual_mode){

        }else if (itemID == R.id.nav_change_mPIN){

        }else if (itemID == R.id.nav_logout){

        }
        return true;



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }

    @Override
    public void setDrawerLocked(boolean enabled) {
        if(enabled){
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }else{
            drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
    }

    @Override
    public void addFragment(Fragment fragment) {
        ConstantDeclaration.replaceFragment(fragment, getSupportFragmentManager());
        ConstantDeclaration.hideKeyboard(HOSDashboardActivity.this);
    }

    @Override
    public void ReplaceFrag(Fragment fragment) {

    }

    @Override
    public void updateDrawer(boolean isShown) {
        try {
            actionBarDrawerToggle.setDrawerIndicatorEnabled(isShown);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            getSupportActionBar().setDisplayShowHomeEnabled(isShown);
            getSupportActionBar().setDisplayHomeAsUpEnabled(isShown);
            option_menu.getItem(0).setVisible(isShown);
            option_menu.getItem(1).setVisible(isShown);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        Fragment frag = getSupportFragmentManager().findFragmentById(R.id.dashboard_container);
        String fragname = null;
        try {
            fragname = frag.getClass().getSimpleName();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            if (frag != null) {

                if (fragname.equalsIgnoreCase("HomeFragment_old")||
                        fragname.equalsIgnoreCase("HomeFragment_new")) {
                    universalDialog = new UniversalDialog(HOSDashboardActivity.this, new AlertDialogInterface() {
                        @Override
                        public void methodDone() {
                            finish();
                        }

                        @Override
                        public void methodCancel() {

                        }
                    }, "", getString(R.string.app_close_msg), getString(R.string.yes), getString(R.string.no));
                    universalDialog.showAlert();

                    TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
                    ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
                    toolbarIv.setVisibility(View.VISIBLE);
                    toolbarTV.setVisibility(View.GONE);
                    //show drawer icon and menu options
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                option_menu.findItem(R.id.action_notification).setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);
                    try {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                        getSupportActionBar().setDisplayShowHomeEnabled(true);
//                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    getSupportFragmentManager().popBackStack();
                }
            }else{
                getSupportFragmentManager().popBackStack();

            }
        }
        else{

            if (frag != null) {
                if (fragname.equalsIgnoreCase("HomeFragment_old")||
                        fragname.equalsIgnoreCase("HomeFragment_new")) {
                    universalDialog = new UniversalDialog(HOSDashboardActivity.this, new AlertDialogInterface() {
                        @Override
                        public void methodDone() {
                         finish();
                        }

                        @Override
                        public void methodCancel() {

                        }
                    }, "", getString(R.string.app_close_msg), getString(R.string.yes), getString(R.string.no));
                    universalDialog.showAlert();

                    TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
                    ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
                    toolbarIv.setVisibility(View.VISIBLE);
                    toolbarTV.setVisibility(View.GONE);
                    //show drawer icon and menu options
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                option_menu.findItem(R.id.action_notification).setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);
                    try {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                        getSupportActionBar().setDisplayShowHomeEnabled(true);
//                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else if (fragname.equalsIgnoreCase("FragmentFuelSales")||
                        fragname.equalsIgnoreCase("FragmentWetInventory")||
                        fragname.equalsIgnoreCase("FragmentROSnapsahot")||
                        fragname.equalsIgnoreCase("Fragmnet_Equipment")) {
                    TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
                    ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
                    toolbarIv.setVisibility(View.VISIBLE);
                    toolbarTV.setVisibility(View.GONE);
                    //show drawer icon and menu options
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                option_menu.findItem(R.id.action_notification).setVisible(true);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);
                    try {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
//                        getSupportActionBar().setDisplayShowHomeEnabled(true);
//                        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                  else{
                    TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
                    ImageView toolbarIv = (ImageView) toolbar.findViewById(R.id.toolbarIV);
                    toolbarIv.setVisibility(View.GONE);
                    toolbarTV.setVisibility(View.VISIBLE);
                    ActionBar actionBar = getSupportActionBar();
                    if (actionBar != null) {
                        actionBar.setHomeAsUpIndicator(R.drawable.back_button);
                    }
//                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//                    getSupportActionBar().setDisplayShowHomeEnabled(true);
//                    getSupportActionBar().setDisplayShowTitleEnabled(false);
//                    getSupportActionBar().setHomeAsUpIndicator(R.drawable.back_button);
                    //hide drawer icon and menu options
//                    if (fragname != null && (fragname.equalsIgnoreCase("SuccessfulFragment"))) {
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                option_menu.findItem(R.id.action_notification).setVisible(false);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 500);

//                    }
//                    updateDrawer(false);
                    try {
                        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void logOut() {
        try {
            JSONObject json = new JSONObject();
            new AsyncLogOut().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private class AsyncLogOut extends AsyncTask<JSONObject, Void, String> {
        public AsyncLogOut() {

        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(HOSDashboardActivity.this);
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(HOSDashboardActivity.this);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Login/Logout", false, jsonObjects[0],"");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try {
                UserDataPrefrence.savePreference(getString(R.string.prefrence_name),
                        HOSDashboardActivity.this, ConstantDeclaration.TOKEN,
                        "");
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing()) {
                    ConstantDeclaration.gifProgressDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            String str_time = "";
            if (s.contains("||")) {
                String[] str = (s.split("\\|\\|"));
                /*Saving data in pref*/
                if (str[0].equalsIgnoreCase("00")) {
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
                else {
                    universalDialog = new UniversalDialog(HOSDashboardActivity.this, null, "",
                            str[1], getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
                }
            } else {
                universalDialog = new UniversalDialog(HOSDashboardActivity.this, null, "", getString(R.string.server_not_resp), getString(R.string.dialog_ok), "");
                universalDialog.showAlert();
            }

        }
    }

    @Override
    public void methodDone() {
        finish();
    }

    @Override
    public void methodCancel() {

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.actionbar_menu, menu);
        option_menu = menu;
        final View notificaitons = menu.findItem(R.id.action_notification).getActionView();
        TextView txtCount = (TextView) notificaitons.findViewById(R.id.txtCount);
        ImageView img_notification = (ImageView) notificaitons.findViewById(R.id.img_notification);
//        badge = new BadgeView(this, txtCount);
//        badge.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);
//        badge.setBackground(ContextCompat.getDrawable(this, R.drawable.badge_circle));
//        badge.setBadgeBackgroundColor(ContextCompat.getColor(this, R.color.orange_color));
        img_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                addFragment(new NotificationFragment());
                    universalDialog = new UniversalDialog(HOSDashboardActivity.this, dialogInterface, "", "Coming Soon...", getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
            }
        });




        return true;
    }
}
package com.agstransact.HP_SC.dashboard.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Satish Patel on 05,April,2021
 */
public class PumpManualModeDealerFragment extends Fragment implements AlertDialogInterface {

    private Spinner spinner_RO,spinner_select_SAM,spinner_status,spinner_manager;
    private RelativeLayout fromDate,toDate,layout_spinner_select_pump,layout_spinner_RO;
    private TextView fromTimeMM,fromTimeSS,toTimeMM,toTimeSS,tv_dealer_sa_remarks,tv_from_date,tv_to_date,tv_selected_pump,tv_default_ro_value,tv_hint_SAM,tv_hint_RM,et_dealer_remarks;
    private EditText et_remarks;
    private Button submitBT,btn_pump_details;
    private LinearLayout sm_rm_layout;
    private String userRole="";
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    String error="", respCode="",selectedPump="",selectedSA="",selectedRO="",selectedStatus,selectedRM="";
    private ArrayList<String> pumpList = new ArrayList<>();
    private ArrayList<String> pumpIdList = new ArrayList<>();
    private ArrayList<String> sAMList = new ArrayList<>();
    private ArrayList<String> sAMIdList = new ArrayList<>();
    private ArrayList<String> ROCodeList = new ArrayList<>();
    private ArrayList<String> RONameList = new ArrayList<>();
    private ArrayList<String> RMNameList = new ArrayList<>();
    private ArrayList<String> RMIdList = new ArrayList<>();
    private ArrayList<String> statusList = new ArrayList<>();
    ImageView iv_filter;
    Toolbar toolbar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.pump_manual_mode_fragment, container, false);

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);

        findViewById(view);

        toolbarTV.setText(getResources().getString(R.string.pump_manual_mode));

        alertDialogInterface = (AlertDialogInterface) this;

        setLayout();

        setListeners();

        return view;
    }

    private void setLayout(){

        userRole = UserDataPrefrence.getPreference("HPCL_Preference", getContext(), "UserCustomRole","");


        if(userRole.equalsIgnoreCase("DEALER")){
          sm_rm_layout.setVisibility(View.GONE);
          tv_default_ro_value.setSelected(true);
          tv_selected_pump.setSelected(true);
          tv_default_ro_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(), "ROValue", ""));
          getPumpList("");
          getSalesAreaManagerList();
        }
        else if(userRole.equalsIgnoreCase("HPCLSALESAREA")){
            sm_rm_layout.setVisibility(View.VISIBLE);
            btn_pump_details.setVisibility(View.VISIBLE);
            layout_spinner_RO.setVisibility(View.VISIBLE);
            layout_spinner_select_pump.setVisibility(View.GONE);
            tv_hint_SAM.setText("Selected Pump :");
            spinner_select_SAM.setBackgroundColor(Color.TRANSPARENT);
            showStatusList();
            getROList();
//            getPumpList();
            getRMList();
            tv_dealer_sa_remarks.setText("Dealer Remarks :");
        }
        else if(userRole.equalsIgnoreCase("HPCLREGION")){
            sm_rm_layout.setVisibility(View.VISIBLE);
            btn_pump_details.setVisibility(View.VISIBLE);
            layout_spinner_RO.setVisibility(View.VISIBLE);
            layout_spinner_select_pump.setVisibility(View.GONE);
            tv_dealer_sa_remarks.setText("SA Remarks :");
            tv_hint_SAM.setText("Selected Pump :");
            tv_hint_RM.setText("Select RE Office :");
            spinner_select_SAM.setBackgroundColor(Color.TRANSPARENT);
            showStatusList();
            getROList();
//            getPumpList();
            getREOfficerList();
        }

    }


    private void showStatusList(){
        statusList.add("Select Approval Status");
        statusList.add("Approved");
        statusList.add("Rejected");

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, statusList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_status.setAdapter(adapter);
    }

    private void fromDate() {

        final Calendar newCalendar = Calendar.getInstance();

        final DatePickerDialog fromDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tv_from_date.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
                String currenthourTime = new SimpleDateFormat("HH", Locale.getDefault()).format(new Date());
                String currentminTime = new SimpleDateFormat("mm", Locale.getDefault()).format(new Date());
                fromTimeMM.setText(currenthourTime);
                fromTimeSS.setText(currentminTime);

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        fromDate.getDatePicker().setMinDate(System.currentTimeMillis()-100);

        fromDate.show();

    }

    private void fromTimeHM() {

       final Calendar newCalendar = Calendar.getInstance();
       int mHour = newCalendar.get(Calendar.HOUR_OF_DAY);
       int mMinute = newCalendar.get(Calendar.MINUTE);

        final TimePickerDialog fromTimeMMSS = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hr, int mm) {
                    fromTimeMM.setText(""+hr);
                    fromTimeSS.setText(""+mm);
            }
        },mHour,mMinute,false);

        fromTimeMMSS.show();

    }

    private void toDate() {

        final Calendar newCalendar = Calendar.getInstance();

        final DatePickerDialog toDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                tv_to_date.setText(year+"-"+(monthOfYear+1)+"-"+dayOfMonth);
                String currenthourTime = new SimpleDateFormat("HH", Locale.getDefault()).format(new Date());
                String currentminTime = new SimpleDateFormat("mm", Locale.getDefault()).format(new Date());
                toTimeMM.setText(currenthourTime);
                toTimeSS.setText(currentminTime);
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDate.getDatePicker().setMinDate(System.currentTimeMillis()-100);

        toDate.show();
    }

    private void toTimeHM() {

        final Calendar newCalendar = Calendar.getInstance();
        int mHour = newCalendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = newCalendar.get(Calendar.MINUTE);

        final TimePickerDialog fromTimeMMSS = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {

            @Override
            public void onTimeSet(TimePicker timePicker, int hr, int mm) {
                toTimeMM.setText(""+hr);
                toTimeSS.setText(""+mm);
            }

        },mHour,mMinute,false);

        fromTimeMMSS.show();
    }

    private void findViewById(View view){
        tv_hint_RM = (TextView) view.findViewById(R.id.tv_hint_RM);
        tv_hint_SAM = (TextView) view.findViewById(R.id.tv_hint_SAM);
        tv_default_ro_value = (TextView) view.findViewById(R.id.tv_default_ro_value);
        tv_selected_pump = (TextView) view.findViewById(R.id.tv_selected_pump);
        sm_rm_layout = (LinearLayout) view.findViewById(R.id.sm_rm_layout);
        submitBT = (Button) view.findViewById(R.id.submitBT);
        btn_pump_details = (Button) view.findViewById(R.id.btn_pump_details);
        et_dealer_remarks = (TextView) view.findViewById(R.id.et_dealer_remarks);
        et_remarks = (EditText)view.findViewById(R.id.et_remarks);
        iv_filter = (ImageView) view.findViewById(R.id.iv_filter);
        iv_filter.setVisibility(View.GONE);
        fromTimeMM = (TextView) view.findViewById(R.id.fromTimeMM);
        fromTimeSS = (TextView) view.findViewById(R.id.fromTimeSS);
        toTimeMM = (TextView) view.findViewById(R.id.toTimeMM);
        toTimeSS = (TextView) view.findViewById(R.id.toTimeSS);
        tv_dealer_sa_remarks = (TextView) view.findViewById(R.id.tv_dealer_sa_remarks);
        tv_from_date = (TextView) view.findViewById(R.id.tv_from_date);
        tv_to_date = (TextView) view.findViewById(R.id.tv_to_date);
        fromDate = (RelativeLayout) view.findViewById(R.id.fromDate);
        toDate = (RelativeLayout) view.findViewById(R.id.toDate);
        spinner_RO = (Spinner) view.findViewById(R.id.spinner_RO);
        spinner_select_SAM = (Spinner) view.findViewById(R.id.spinner_select_SAM);
        spinner_status = (Spinner) view.findViewById(R.id.spinner_status);
        spinner_manager = (Spinner) view.findViewById(R.id.spinner_manager);
        layout_spinner_select_pump = (RelativeLayout) view.findViewById(R.id.layout_spinner_select_pump);
        layout_spinner_RO = (RelativeLayout) view.findViewById(R.id.layout_spinner_RO);
    }

    private void setListeners(){

        fromDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(!userRole.equalsIgnoreCase("HPCLREGION")) {
                    fromDate();
                }
            }
        });

        toDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(!userRole.equalsIgnoreCase("HPCLREGION"))
                toDate();
            }
        });

        fromTimeMM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!userRole.equalsIgnoreCase("HPCLREGION"))
                fromTimeHM();
            }
        });

        fromTimeSS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!userRole.equalsIgnoreCase("HPCLREGION"))
                fromTimeHM();
            }
        });

        toTimeMM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!userRole.equalsIgnoreCase("HPCLREGION"))
                toTimeHM();
            }
        });

        toTimeSS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!userRole.equalsIgnoreCase("HPCLREGION"))
                toTimeHM();
            }
        });

        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    isValid();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btn_pump_details.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                if (selectedRO.equalsIgnoreCase("-1")){
                    showError("Select RO.");
                }else if(selectedPump.equalsIgnoreCase("-1")){
                    showError("Select Pump.");
                } else {
                    getPumpRequestDetails();
                }
            }
        });
        spinner_RO.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                 if(userRole.equalsIgnoreCase("HPCLSALESAREA") || userRole.equalsIgnoreCase("HPCLREGION")){
                    selectedRO = ROCodeList.get(spinner_RO.getSelectedItemPosition());
                    if(!selectedRO.equalsIgnoreCase("-1"))
                     getPumpList(selectedRO);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        layout_spinner_select_pump.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                showPumpDialog();
            }
        });

        spinner_select_SAM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if(userRole.equalsIgnoreCase("DEALER")){
                    selectedSA = sAMIdList.get(spinner_select_SAM.getSelectedItemPosition());
                }else if(userRole.equalsIgnoreCase("HPCLSALESAREA") || userRole.equalsIgnoreCase("HPCLREGION")) {
                    selectedPump = pumpIdList.get(spinner_select_SAM.getSelectedItemPosition());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        spinner_manager.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedRM = RMIdList.get(spinner_manager.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedStatus = statusList.get(spinner_status.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }


    @Override
    public void methodDone() {

        if(respCode.equalsIgnoreCase("401")){
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void methodCancel() {

    }

    private void getPumpList(String ro){

        JSONObject json = new JSONObject();
        try {
            json.put("RoCode", ro);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new AsyncGetPumpList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncGetPumpList extends AsyncTask<JSONObject, Void, String> {


        public AsyncGetPumpList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpManualMode/GetPumpList", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arraypump = new JSONArray(jsonObject.getString("Data"));

                            /*if(userRole.equalsIgnoreCase("HPCLSALESAREA") || userRole.equalsIgnoreCase("HPCLREGION")) {
                                pumpList.add(0, "Select All");
                                pumpIdList.add(0, "-1");
                            }*/

                            if(userRole.equalsIgnoreCase("DEALER")) {

                                for (int i = 0; i < arraypump.length(); i++) {
                                    JSONObject pumpObject = arraypump.getJSONObject(i);
                                    pumpIdList.add(pumpObject.getString("Key"));
                                    pumpList.add(pumpObject.getString("Value"));
                                }
                            }else if(userRole.equalsIgnoreCase("HPCLSALESAREA") || userRole.equalsIgnoreCase("HPCLREGION")){

                                pumpIdList.clear();
                                pumpList.clear();

                                for (int i = 0; i < arraypump.length(); i++) {
                                    JSONObject pumpObject = arraypump.getJSONObject(i);
                                    pumpIdList.add(pumpObject.getString("Key"));
                                    pumpList.add(pumpObject.getString("Value"));
                                }

                                ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, pumpList);
                                adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

                               spinner_select_SAM.setAdapter(adapter);

                            }

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void showPumpDialog(){

        // initialise the alert dialog builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.MyDialogTheme);

        // set the title for the alert dialog
        builder.setTitle("Select Pump");

       final boolean[] checkedItems = new boolean[pumpList.size()];

        String[] pumpArray = new String[pumpList.size()] ;//pumpList.toArray();

        for(int i=0;i<pumpList.size();i++){
            pumpArray[i] = pumpList.get(i);
        }


        // now this is the function which sets the alert dialog for multiple item selection ready
        builder.setMultiChoiceItems(pumpArray, checkedItems, new DialogInterface.OnMultiChoiceClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which, boolean isChecked) {
                checkedItems[which] = isChecked;
            }
        });


        // alert dialog shouldn't be cancellable
        builder.setCancelable(false);

        // handle the positive button of the dialog
        builder.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    StringBuilder pumps = new StringBuilder();
                    StringBuilder pumpsId = new StringBuilder();

                    for (int i = 0; i < checkedItems.length; i++) {
                        if (checkedItems[i]) {
                            pumps.append(pumpList.get(i) + ", ");
                            pumpsId.append(pumpIdList.get(i)+",");
                        }
                    }

                    selectedPump = pumpsId.toString().substring(0,pumpsId.toString().length()-1);

                    tv_selected_pump.setText(pumps.toString().substring(0,pumps.toString().length()-2));
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        // handle the negative button of the alert dialog
        builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });


        // create the builder
        builder.create();

        // create the alert dialog with the
        // alert dialog builder instance
        AlertDialog alertDialog = builder.create();
        alertDialog.show();

    }

    private void getSalesAreaManagerList(){

        JSONObject json = new JSONObject();
       /* try {
            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRO", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        new AsyncGetSalesAMList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncGetSalesAMList extends AsyncTask<JSONObject, Void, String> {


        public AsyncGetSalesAMList() {


        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpManualMode/GetSaleAreaManagerList", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arraySA = new JSONArray(jsonObject.getString("Data"));

                            sAMList.add(0,"Select SA Manager");
                            sAMIdList.add(0,"-1");

                            for(int i=0;i<arraySA.length();i++){
                                JSONObject pumpObject = arraySA.getJSONObject(i);
                                sAMIdList.add(pumpObject.getString("Key"));
                                sAMList.add(pumpObject.getString("Value"));
                            }


                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, sAMList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_select_SAM.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }


    private void getROList(){

        JSONObject json = new JSONObject();
       /* try {
            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRO", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        new AsyncGetROList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncGetROList extends AsyncTask<JSONObject, Void, String> {


        public AsyncGetROList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpManualMode/GetROList", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayRO = new JSONArray(jsonObject.getString("Data"));

                            RONameList.add(0,"Select RO");
                            ROCodeList.add(0,"-1");

                            for(int i=0;i<arrayRO.length();i++){
                                JSONObject pumpObject = arrayRO.getJSONObject(i);
                                RONameList.add(pumpObject.getString("ROCodeName"));
                                ROCodeList.add(pumpObject.getString("ROCode"));
                            }


                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, RONameList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_RO.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void getRMList(){

        JSONObject json = new JSONObject();
       /* try {
            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRO", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        new AsyncGetRMList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncGetRMList extends AsyncTask<JSONObject, Void, String> {

        public AsyncGetRMList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {

                if (ConstantDeclaration.gifProgressDialog != null) {

                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpManualMode/GetRegionManagerList", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {

                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayRO = new JSONArray(jsonObject.getString("Data"));

                            RMNameList.add(0,"Select Regional Manager");
                            RMIdList.add(0,"-1");

                            for(int i=0;i<arrayRO.length();i++) {
                                JSONObject pumpObject = arrayRO.getJSONObject(i);
                                RMIdList.add(pumpObject.getString("Key"));
                                RMNameList.add(pumpObject.getString("Value"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, RMNameList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_manager.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void getREOfficerList(){

        JSONObject json = new JSONObject();
       /* try {
            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRO", ""));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        new AsyncGetREOfficersList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class AsyncGetREOfficersList extends AsyncTask<JSONObject, Void, String> {

        public AsyncGetREOfficersList() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {

                if (ConstantDeclaration.gifProgressDialog != null) {

                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpManualMode/REOfficerList", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {

                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayRO = new JSONArray(jsonObject.getString("Data"));

                            RMNameList.add(0,"Select RE Office");
                            RMIdList.add(0,"-1");

                            for(int i=0;i<arrayRO.length();i++) {
                                JSONObject pumpObject = arrayRO.getJSONObject(i);
                                RMIdList.add(pumpObject.getString("Key"));
                                RMNameList.add(pumpObject.getString("Value"));
                            }

                            ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, RMNameList);
                            adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                            spinner_manager.setAdapter(adapter);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void showError(String msg){
        universalDialog = new UniversalDialog(getActivity(),
                alertDialogInterface, "", msg, getString(R.string.dialog_ok), "");
        universalDialog.showAlert();
    }

    private void isValid(){

        if(userRole.equalsIgnoreCase("DEALER")){

            String fromDate = tv_from_date.getText().toString();
            String fromTimeHr = fromTimeMM.getText().toString();
            String fromTimeMm = fromTimeSS.getText().toString();

            String toDate = tv_to_date.getText().toString();
            String toTimeHr = toTimeMM.getText().toString();
            String toTimeMm = toTimeSS.getText().toString();

            String remarks = et_remarks.getText().toString().trim();

            if(selectedPump.equalsIgnoreCase("")){
               showError("Select Pump.");
            }else if(selectedSA.equalsIgnoreCase("-1")){
                showError("Select SA Manager.");

            }else if(fromDate.equalsIgnoreCase("") || fromTimeHr.equalsIgnoreCase("")||fromTimeMm.equalsIgnoreCase("")){
                showError("Set From Datetime.");

            }else if(toDate.equalsIgnoreCase("") || toTimeHr.equalsIgnoreCase("")||toTimeMm.equalsIgnoreCase("")){
                showError("Set To Datetime.");

            }else if(remarks.equalsIgnoreCase("")){
                showError("Enter remarks.");

            }else{
                updatePumpManualMode();

            }

        }else if(userRole.equalsIgnoreCase("HPCLSALESAREA") || userRole.equalsIgnoreCase("HPCLREGION")){

            String fromDate = tv_from_date.getText().toString();
            String fromTimeHr = fromTimeMM.getText().toString();
            String fromTimeMm = fromTimeSS.getText().toString();

            String toDate = tv_to_date.getText().toString();
            String toTimeHr = toTimeMM.getText().toString();
            String toTimeMm = toTimeSS.getText().toString();

            String remarks = et_remarks.getText().toString().trim();

            if(selectedPump.equalsIgnoreCase("-1")){
                showError("Select RO.");

            }else if(selectedSA.equalsIgnoreCase("-1")){
                showError("Select Pump.");

            }else if(fromDate.equalsIgnoreCase("") || fromTimeHr.equalsIgnoreCase("")||fromTimeMm.equalsIgnoreCase("")){
                showError("Set From Datetime.");

            }else if(toDate.equalsIgnoreCase("") || toTimeHr.equalsIgnoreCase("")||toTimeMm.equalsIgnoreCase("")){
                showError("Set To Datetime.");

            }else if(selectedStatus.equalsIgnoreCase("Select Approval Status")){
                showError("Select Approval Status.");

            }else if(selectedRM.equalsIgnoreCase("-1")){
                if(userRole.equalsIgnoreCase("HPCLREGION")){
                    showError("Select RE Office");
                }else{
                    showError("Select Regional Manager");
                }

            } else if(remarks.equalsIgnoreCase("")){
                showError("Enter remarks.");
            }else{
                approveRejectRequest();
            }

        }

    }

    private void updatePumpManualMode(){

        String fromDate = tv_from_date.getText().toString();
        String fromTimeHr = fromTimeMM.getText().toString();
        String fromTimeMm = fromTimeSS.getText().toString();

        String toDate = tv_to_date.getText().toString();
        String toTimeHr = toTimeMM.getText().toString();
        String toTimeMm = toTimeSS.getText().toString();

        String remarks = et_remarks.getText().toString().trim();

        String userId= UserDataPrefrence.getPreference(getActivity().getString(R.string.prefrence_name),
                getActivity(), ConstantDeclaration.USERIDPREFKEY,
                "");

        JSONObject json = new JSONObject();

        try {
            json.put("PumpDetails",selectedPump);
            json.put("FromDate",fromDate+" "+fromTimeHr+":"+fromTimeMm+":00");
            json.put("ToDate",toDate+" "+toTimeHr+":"+toTimeMm+":00");
            json.put("Remarks",remarks);
            json.put("SaleAreaManager",selectedSA);
            json.put("UserId",userId);
            json.put("Rocode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(), "SelectdRO", ""));

            new AsyncUpdateManualMode().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncUpdateManualMode extends AsyncTask<JSONObject, Void, String> {

        public AsyncUpdateManualMode() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpManualMode/UpdatePumpManualMode", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);

                            String msg = jsonObject.getString("ResponseMessage");

                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {

                                        @Override
                                        public void methodDone() {
                                            try {
                                                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                                            ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                                                    , new HomeFragment_old(), new HomeFragment_old()
                                                    , new HomeFragment_old(), new HomeFragment_old());
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", msg, getString(R.string.dialog_ok), "");

                            universalDialog.showAlert();

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void getPumpRequestDetails(){

        JSONObject json = new JSONObject();

        try {
            json.put("Rocode",selectedRO);
            json.put("RequestID",selectedPump);

            new AsyncGetPumpDetails().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncGetPumpDetails extends AsyncTask<JSONObject, Void, String> {

        public AsyncGetPumpDetails() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {

                if (ConstantDeclaration.gifProgressDialog != null) {

                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpManualMode/GetPumpManualModeRequest", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {

                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray arrayDetails = new JSONArray(jsonObject.getString("Data"));

                            JSONObject data = arrayDetails.getJSONObject(0);
                            String dealerRemarks = data.getString("DealerRemarks");
                            String fromDate = data.getString("FromDate");
                            String toDate = data.getString("ToDate");
                            String approvalStatus = data.getString("AprovalStatus");
                            String salesArea = data.getString("SalesArea");
                            String regionalOffice = data.getString("RegionalOffice");
                            String zonalOffice = data.getString("ZonalOffice");


                            if(userRole.equalsIgnoreCase("HPCLSALESAREA")){
                                et_dealer_remarks.setText(dealerRemarks);
                            }else{
                                String saRemarks = data.getString("SalesManagerRemarks");
                                et_dealer_remarks.setText(saRemarks);
                            }



                            String[] dateTimeFrom = fromDate.split(" ");
                            tv_from_date.setText(dateTimeFrom[0]);
                            String[] fromTimes = dateTimeFrom[1].split(":");
                            fromTimeMM.setText(fromTimes[0]);
                            fromTimeSS.setText(fromTimes[1]);

                            String[] dateTimeTo = toDate.split(" ");
                            tv_to_date.setText(dateTimeTo[0]);

                            String[] toTimes = dateTimeTo[1].split(":");
                            toTimeMM.setText(toTimes[0]);
                            toTimeSS.setText(toTimes[1]);

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void approveRejectRequest(){


        String fromDate = tv_from_date.getText().toString();
        String fromTimeHr = fromTimeMM.getText().toString();
        String fromTimeMm = fromTimeSS.getText().toString();

        String toDate = tv_to_date.getText().toString();
        String toTimeHr = toTimeMM.getText().toString();
        String toTimeMm = toTimeSS.getText().toString();


        String remarks = et_remarks.getText().toString().trim();

        String userId= UserDataPrefrence.getPreference(getActivity().getString(R.string.prefrence_name),
                getActivity(), ConstantDeclaration.USERIDPREFKEY,
                "");

        JSONObject json = new JSONObject();

        try {
            json.put("FromDate",fromDate+" "+fromTimeHr+":"+fromTimeMm+":00");
            json.put("ToDate",toDate+" "+toTimeHr+":"+toTimeMm+":00");
            json.put("AprovalStatus",selectedStatus);
            json.put("Remarks",remarks);
            json.put("UserId",userId);
            json.put("Checker",selectedRM);
            json.put("RequestID",selectedPump);

            new AsyncApproveRejectRequest().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private class AsyncApproveRejectRequest extends AsyncTask<JSONObject, Void, String> {

        public AsyncApproveRejectRequest() {

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("PumpManualMode/ApproveRejectRequest", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];

                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);

                            String msg = jsonObject.getString("ResponseMessage");

                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {

                                        @Override
                                        public void methodDone() {
                                            try {
                                                getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                                            ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                                                    , new HomeFragment_old(), new HomeFragment_old()
                                                    , new HomeFragment_old(), new HomeFragment_old());
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", msg, getString(R.string.dialog_ok), "");

                            universalDialog.showAlert();

                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }
}

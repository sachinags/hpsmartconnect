package com.agstransact.HP_SC.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.Nozzle_status_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Equipment_Details_Adapter extends RecyclerView.Adapter<Equipment_Details_Adapter.MyInterlockModel> {
    private Context context;
    private ArrayList<Nozzle_status_Model> models = new ArrayList<>();
    private JSONArray zonedetails_array;

    public Equipment_Details_Adapter(Activity activity, ArrayList<Nozzle_status_Model> models) {
        this.context = activity;
        this.models = models;
    }
    public Equipment_Details_Adapter(JSONArray zonedetails_array) {
        this.zonedetails_array = zonedetails_array;
    }


    @NonNull
    @Override
    public Equipment_Details_Adapter.MyInterlockModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.equipment_details_adapter,parent,false);
        return new MyInterlockModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Equipment_Details_Adapter.MyInterlockModel holder, int position) {

//        Nozzle_status_Model model = models.get(position);

//        try {
//            if(zonedetails_array.getJSONObject(position).getString("Product").equalsIgnoreCase("Total")){
                try {
                    holder.tv_product_value.setText(zonedetails_array.getJSONObject(position).getString("Product"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    holder.tv_nozzle_conf_value.setText(zonedetails_array.getJSONObject(position).getString("Tank"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    holder.tv_nozzle_in_use_value.setText(zonedetails_array.getJSONObject(position).getString("Du"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                try {
                    holder.tv_nozzle_not_in_use_value.setText(zonedetails_array.getJSONObject(position).getString("Nozzle"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                holder.tv_product_value.setTypeface(holder.tv_product_value.getTypeface(), Typeface.BOLD);
                holder.tv_nozzle_conf_value.setTypeface(holder.tv_product_value.getTypeface(), Typeface.BOLD);
                holder.tv_nozzle_in_use_value.setTypeface(holder.tv_product_value.getTypeface(), Typeface.BOLD);
                holder.tv_nozzle_not_in_use_value.setTypeface(holder.tv_product_value.getTypeface(), Typeface.BOLD);
                holder.tv_product_value.setVisibility(View.VISIBLE);
                holder.tv_nozzle_conf_value.setVisibility(View.VISIBLE);
                holder.tv_nozzle_in_use_value.setVisibility(View.VISIBLE);
                holder.tv_nozzle_not_in_use_value.setVisibility(View.VISIBLE);
//            }else{
//                holder.tv_product_value.setVisibility(View.GONE);
//                holder.tv_nozzle_conf_value.setVisibility(View.GONE);
//                holder.tv_nozzle_in_use_value.setVisibility(View.GONE);
//                holder.tv_nozzle_not_in_use_value.setVisibility(View.GONE);
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


    }

    @Override
    public int getItemCount() {
        return zonedetails_array.length();
    }

    public class MyInterlockModel extends RecyclerView.ViewHolder {

        private TextView tv_product_value,tv_nozzle_conf_value,tv_nozzle_in_use_value,tv_nozzle_not_in_use_value;

        public MyInterlockModel(@NonNull View itemView) {
            super(itemView);

            tv_product_value = itemView.findViewById(R.id.tv_product_value);
            tv_nozzle_conf_value = itemView.findViewById(R.id.tv_nozzle_conf_value);
            tv_nozzle_in_use_value = itemView.findViewById(R.id.tv_nozzle_in_use_value);
            tv_nozzle_not_in_use_value = itemView.findViewById(R.id.tv_nozzle_not_in_use_value);
        }
    }
}

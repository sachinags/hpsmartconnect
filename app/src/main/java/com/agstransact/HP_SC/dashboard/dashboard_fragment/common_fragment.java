package com.agstransact.HP_SC.dashboard.dashboard_fragment;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.adapter.dashboard_adapter_new;
import com.agstransact.HP_SC.dashboard.fragment.MyMarkerView;
import com.agstransact.HP_SC.model.CombinedModel;
import com.agstransact.HP_SC.model.CriticalStock;
import com.agstransact.HP_SC.model.NanoStatus;
import com.agstransact.HP_SC.model.ROMOdel;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class common_fragment extends Fragment {
        View rootView;
    public ImageView imageView;
    public TextView textView;
    public ConstraintLayout item_layout;
    LinearLayout ll_todaysale_graph,item_layout_main,item_layout_graph,item_layout_ys,ll_cumulative_graph,ll_roconnectivity_graph;
    LinearLayout ll_yesterday_graph,ll_nano_graph,ll_price_exception_graph,ll_critical_stock_graph;
    ViewPager viewPager;
    FrameLayout container,container_ys,container_cs,container_rcs,container_ns,container_ps,container_crs;
    private String error="", respCode="",fuel_type="",graph_open="";
    public static JSONArray arraymonth1,arrayHSD1,arrayMS1,arrayPower1,arrayTurbojet1;
    public static JSONArray arraymonth2,arrayHSD2,arrayMS2,arrayPower2,arrayTurbojet2;
    private TextView tv_uptolbl,tv_untilllbl,tv_y_axis;
    private JSONArray catArray,valArray;
    private List<String> todaycategorydata,todayvalueData;
    private BarChart barChart;
    private AlertDialogInterface alertDialogInterface;
    private UniversalDialog universalDialog;
    private int [] colorcode = {R.color.line_chart_hsd_color,R.color.line_chart_ms_color,R.color.line_chart_power_color,R.color.line_chart_turbojet_color};
    private static final float BAR_WIDTH = 0.8f;
    private static final float BAR_SPACE = 0.05f;
    private static final int GROUPS = 2;
    private BarChart yesterdaySaleChart,critical_barChart;
    private ArrayList<CriticalStock> stockArrayList = new ArrayList<>();
    private TextView tv_cr_date;
    private TextView tv_yes_untilllbl,tv_yes_uptolbl;
    private ArrayList<String> xVal = new ArrayList<>();
    private Spinner sp_sales_trends_filter;
    int biggestmonthmain1,biggest_HSDmain1,biggestMSmain1,biggestPowermain1,biggestTurbojetmain1;
    int smallestmonthmain1,smallest_HSDmain1,smallestMSmain1,smallestPowermain1,smallestTurbojetmain1;
    int biggestmonthmain2,biggest_HSDmain2,biggestMSmain2,biggestPowermain2,biggestTurbojetmain2;
    int smallestmonthmain2,smallest_HSDmain2,smallestMSmain2,smallestPowermain2,smallestTurbojetmain2;
    String CSMonth1,CSMonth2;
    private LineChart mChart;
    private ArrayList<String> fuelList;
    private ArrayList pieEntries;
    private PieDataSet pieDataSet;
    private PieData pieData;
    private ArrayList<ROMOdel> roModels = new ArrayList<>();
    List<String> listKeys;
    List<Integer> listValues;
    private PieChart ro_piechart;
    private List<String> listData,roData,nanodata;
    private PieChart nano_piechart;
    private TextView nano_status_date;
    private ArrayList<NanoStatus> nanoStatusArrayList = new ArrayList<>();
    private ArrayList<CombinedModel> combinedModels = new ArrayList<>();
    private PieChart pieChart;
    private TextView tv_dt_price;
    Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.common_fragment, container, false);
        bundle =getArguments();
        ll_cumulative_graph = (LinearLayout)rootView.findViewById(R.id.ll_cumulative_graph);
        ll_todaysale_graph = (LinearLayout) rootView.findViewById(R.id.ll_todaysale_graph);
        ll_yesterday_graph = (LinearLayout) rootView.findViewById(R.id.ll_yesterday_graph);
        ll_nano_graph = (LinearLayout) rootView.findViewById(R.id.ll_nano_graph);
        ll_price_exception_graph = (LinearLayout) rootView.findViewById(R.id.ll_price_exception_graph);
        ll_critical_stock_graph = (LinearLayout) rootView.findViewById(R.id.ll_critical_stock_graph);
        ll_roconnectivity_graph = (LinearLayout) rootView.findViewById(R.id.ll_roconnectivity_graph);
        barChart = (BarChart) rootView.findViewById(R.id.barchart1);
        yesterdaySaleChart = (BarChart) rootView.findViewById(R.id.barchart2);
        tv_uptolbl = (TextView) rootView.findViewById(R.id.tv_uptolbl);
        tv_untilllbl = (TextView) rootView.findViewById(R.id.tv_untilllbl);
        tv_y_axis = (TextView) rootView.findViewById(R.id.tv_y_axis);
        tv_yes_uptolbl = (TextView) rootView.findViewById(R.id.tv_yes_uptolbl);
        tv_yes_untilllbl = (TextView) rootView.findViewById(R.id.tv_yes_untilllbl);
        sp_sales_trends_filter=(Spinner) rootView.findViewById(R.id.sp_sales_trends_filter);
        mChart = (LineChart) rootView.findViewById(R.id.chart2);
        ro_piechart = (PieChart) rootView.findViewById(R.id.ro_piechart);
        nano_piechart = (PieChart) rootView.findViewById(R.id.piechart_nano_status);
        nano_status_date = (TextView) rootView.findViewById(R.id.nano_status_date);
        tv_dt_price = (TextView) rootView.findViewById(R.id.tv_dt_price);
        pieChart = (PieChart) rootView.findViewById(R.id.piechart_price_exception);
        critical_barChart = (BarChart) rootView.findViewById(R.id.critical_barChart);
        tv_cr_date = (TextView) rootView.findViewById(R.id.tv_cr_date);
       if( bundle.getString("Fragment").equalsIgnoreCase("TodaySales")){
           ll_todaysale_graph.setVisibility(View.VISIBLE);
           callTodaysSale();
       }else if( bundle.getString("Fragment").equalsIgnoreCase("YesterdaySales")){
           ll_yesterday_graph.setVisibility(View.VISIBLE);
           callYesterdaySale();
           ll_todaysale_graph.setVisibility(View.GONE);

       }

        return  rootView;
    }
    //Todays Sales Start
    private void callTodaysSale() {

        JSONObject json = new JSONObject();
        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }



                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){

                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new TodaysSale().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }
    private class TodaysSale extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public TodaysSale() {
        }

        public TodaysSale(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getContext());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {

            return ClsWebService_hpcl.PostObjecttoken("DashBoard/GetTodaySales", false, jsonObjects[0], "");
        }


        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);


            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }

            if (s != null) {

                try {
                    ((AppCompatActivity) getContext()).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ll_todaysale_graph.setVisibility(View.VISIBLE);
                            ll_yesterday_graph.setVisibility(View.GONE);
                            ll_cumulative_graph.setVisibility(View.GONE);
                            ll_roconnectivity_graph.setVisibility(View.GONE);
                            ll_nano_graph.setVisibility(View.GONE);
                            ll_price_exception_graph.setVisibility(View.GONE);
                            ll_critical_stock_graph.setVisibility(View.GONE);
                        }
                    });

                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Data");
                            catArray = new JSONArray();
                            valArray = new JSONArray();
                            catArray = jsonObject1.getJSONArray("Product");
                            valArray = jsonObject1.getJSONArray("Value");
                            JSONArray timeSlot = jsonObject.getJSONArray("TimeSlot");
                            for (int ts =0;ts<timeSlot.length();ts++){
                                JSONObject timeObj = timeSlot.getJSONObject(ts);
                                tv_untilllbl.setText(timeObj.getString("EndTime"));
                                SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
                                Date dateObj = null;
                                String fromDate = timeObj.getString("FromDate");
                                dateObj = curFormater.parse(fromDate);
                                SimpleDateFormat postFormater = new SimpleDateFormat("DD |MM | YYYY");

                                String nFDate = postFormater.format(dateObj);
                                try {
                                    tv_uptolbl.setText(fromDate.replace("-"," |"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                            todaycategorydata = new ArrayList<>();
                            for (int i1 = 0; i1 < catArray.length(); i1++) {
                                todaycategorydata.add(catArray.getString(i1));
                            }
                            todayvalueData = new ArrayList<>();
                            for (int i1 = 0; i1 < valArray.length(); i1++) {
                                todayvalueData.add(valArray.getString(i1));
                            }
                            BarData data = createChartData();
                            configureChartAppearance();
                            prepareChartData(data);
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
//                                            Intent intent = new Intent(getContext(), LoginActivity.class);
//                                            startActivity(intent);
//                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    alertDialogInterface, "", error, getContext().getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }

    }
    private BarData createChartData() {

        ArrayList<BarEntry> values1 = new ArrayList<>();
        ArrayList<BarEntry> values2 = new ArrayList<>();
        BarDataSet set1,set2;
        String color="";
        for (int i = 0; i < todayvalueData.size() ; i++) {
//            values1.add(new BarEntry(i, Float.valueOf(todaycategorydata.get(i))));
            values2.add(new BarEntry(i, Float.parseFloat(todayvalueData.get(i))));

//            if(i== 4){
//              co
//
//              lor = "disabled";
//            }
        }
        set1  = new BarDataSet(values1, "Product");
        set2 = new BarDataSet(values2, "Value");
//        set1.getEntryForXValue(0,0)
        ArrayList<Integer> colorList = new ArrayList<>();
        for(int i =0; i<colorcode.length; i++){
            colorList.add(colorcode.length);
        }
//        set1.setValueTextColors(colorList);
//        set1.contains(values1.get(4));
//        set1.setColor(getResources().getColor(R.color.hsdcolor));
//        set2.setColor(getResources().getColor(R.color.orange_logo));
        for(int i = 0; i < todayvalueData.size() ; i++){


        }
//        set1.setValueTextColor(getResources().getColor(R.color.white));
        set2.setValueTextColor(getContext().getResources().getColor(R.color.black));
        set1.setColors(new int[]{Color.rgb(31, 64, 120),
                Color.rgb(0, 0, 254),
                Color.rgb(236, 28, 35),
                Color.rgb(5, 1, 3),
                Color.rgb(201, 108, 56)});

        set2.setColors(new int[]{Color.rgb(0, 0, 254),
                Color.rgb(0, 166, 81),
                Color.rgb(236, 28, 35),
                Color.rgb(5, 1, 3),
                Color.rgb(201, 108, 56)});
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        dataSets.add(set2);
        BarData data = new BarData(dataSets);
        return data;
    }
    private void configureChartAppearance() {

        barChart.setPinchZoom(false);
        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getAxisLeft().setDrawGridLines(false);
        barChart.getAxisRight().setDrawGridLines(false);
        barChart.getDescription().setEnabled(false);
        barChart.getLegend().setEnabled(false);
        barChart.setFitBars(true);

        ArrayList<String> labels = new ArrayList<>();
        for (int val =0 ; val< todaycategorydata.size(); val ++){
            labels.add(todaycategorydata.get(val));
            Log.e("labels",labels.toString());
        }

//        BarData data = new BarData(labels,getDataSet());
        XAxis xAxis = barChart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(false);
        xAxis.setTextColor(getContext().getResources().getColor(R.color.black));
//        xVal.add(String.valueOf(catArray));
//        xAxis.setValueFormatter(todaycategorydata.get(i));
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        final YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f);

        // Add color code arraylist
//        leftAxis.setTextColor(getResources().getColor(R.color.mscolor));


        barChart.getAxisRight().setEnabled(false);

//        barChart.getXAxis().setAxisMinimum(0);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        barChart.getLegend().setTextColor(getContext().getResources().getColor(R.color.black));
//        xAxis.setLabelCount(5);
        int monthsize = catArray.length();
        float f2 = (float) monthsize / 1;
        xAxis.setAxisMaximum(f2);

        List<String> catData = new ArrayList<>();
        for (int j =0;j<catArray.length();j++){
            try {
                catData.add(catArray.getString(j));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        catData.add(" ");
        try {
//            barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(todayvalueData));
            barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(catData));
            if(barChart.getXAxis().getLabelCount() == 7||
                    barChart.getXAxis().getLabelCount() == 8||
                    barChart.getXAxis().getLabelCount() == 9){
                barChart.getXAxis().setTextSize(1+ barChart.getXAxis().getLabelCount());
            }
//            barChart.getXAxis().setTextSize(1+barChart.getXAxis().getLabelCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* XAxis xl = new XAxis();
        xl.setValueFormatter(new AxisValueFormatter() {
            @Override
            public int formatValueForManualAxis(char[] formattedValue, AxisValue axisValue) {
                return 0;
            }

            @Override
            public int formatValueForAutoGeneratedAxis(char[] formattedValue, float value, int autoDecimalDigits) {
                return 0;
            }

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.valueOf((int) value);
            }
        });*/

//        todayvalueData.add(" ");

    }
    private void prepareChartData(BarData data) {

        barChart.setData(data);
        barChart.getBarData().setBarWidth(BAR_WIDTH);
        barChart.setFitBars(true);
        barChart.invalidate();
    }
    //Todays Sales End

    //Yesterdays Sales Start
    private void callYesterdaySale(){
        JSONObject json = new JSONObject();
//        try {
//            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "SelectdRO", "ALL"));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }



                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){

                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new YesterdaySale().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }
    private class YesterdaySale extends AsyncTask<JSONObject, Void, String> {

        String strTimeStamp = "";

        public YesterdaySale() {
        }

        public YesterdaySale(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getContext());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("DashBoard/GetYesterdaysSales", false, jsonObjects[0], "");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
                if (!isAdded()) {
                    return;
                }
                if (isDetached()) {
                    return;
                }

            if (s != null) {

                try {

                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Data");
                            catArray = new JSONArray();
                            valArray = new JSONArray();
                            catArray = jsonObject1.getJSONArray("Product");
                            valArray = jsonObject1.getJSONArray("Value");
                            JSONArray timeSlot = jsonObject.getJSONArray("TimeSlot");
                            for (int ts =0;ts<timeSlot.length();ts++){
                                JSONObject timeObj = timeSlot.getJSONObject(ts);
                                tv_yes_untilllbl.setText(timeObj.getString("EndTime"));
                                SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
                                Date dateObj = null;
                                String fromDate = timeObj.getString("FromDate");
                                dateObj = curFormater.parse(fromDate);
                                SimpleDateFormat postFormater = new SimpleDateFormat("DD |MM | YYYY");

                                String nFDate = postFormater.format(dateObj);
//                                tv_yes_uptolbl.setText(nFDate);
                                try {
                                    tv_yes_uptolbl.setText(fromDate.replace("-"," |"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                            }
//                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("Data"));
//                            catArray =new JSONArray(jsonObject1.getJSONObject("Product"));
//                            valArray = new JSONArray(jsonObject1.getJSONObject("Value"));
                            todaycategorydata = new ArrayList<>();
                            for (int i1 = 0; i1 < catArray.length(); i1++) {
                                todaycategorydata.add(catArray.getString(i1));
                            }
                            todayvalueData = new ArrayList<>();
                            for (int i1 = 0; i1 < valArray.length(); i1++) {
                                todayvalueData.add(valArray.getString(i1));
                            }
                            BarData data = createChartData();
                            configureYesterdayChartAppearance();
                            prepareYesterdayChartData(data);
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
//                                                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                                                startActivity(intent);
//                                                getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    alertDialogInterface, "", error, getContext().getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }

    }
    private BarData createChartData1() {

        ArrayList<BarEntry> values1 = new ArrayList<>();
        ArrayList<BarEntry> values2 = new ArrayList<>();
        BarDataSet set1,set2;
        String color="";
        for (int i = 0; i < todayvalueData.size() ; i++) {
//            values1.add(new BarEntry(i, Float.valueOf(todaycategorydata.get(i))));
            values2.add(new BarEntry(i, Float.parseFloat(todayvalueData.get(i))));

//            if(i== 4){
//              co
//
//              lor = "disabled";
//            }
        }
        set1  = new BarDataSet(values1, "Product");
        set2 = new BarDataSet(values2, "Value");
//        set1.getEntryForXValue(0,0)
        ArrayList<Integer> colorList = new ArrayList<>();
        for(int i =0; i<colorcode.length; i++){
            colorList.add(colorcode.length);
        }
//        set1.setValueTextColors(colorList);
//        set1.contains(values1.get(4));
//        set1.setColor(getResources().getColor(R.color.hsdcolor));
//        set2.setColor(getResources().getColor(R.color.orange_logo));
        for(int i = 0; i < todayvalueData.size() ; i++){


        }
//        set1.setValueTextColor(getResources().getColor(R.color.white));
        set2.setValueTextColor(getContext().getResources().getColor(R.color.black));
        set1.setColors(new int[]{Color.rgb(31, 64, 120),
                Color.rgb(0, 0, 254),
                Color.rgb(236, 28, 35),
                Color.rgb(5, 1, 3),
                Color.rgb(201, 108, 56)});

        set2.setColors(new int[]{Color.rgb(0, 0, 254),
                Color.rgb(0, 166, 81),
                Color.rgb(236, 28, 35),
                Color.rgb(5, 1, 3),
                Color.rgb(201, 108, 56)});
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        dataSets.add(set2);
        BarData data = new BarData(dataSets);
        return data;
    }
    private void prepareYesterdayChartData(BarData data) {
        yesterdaySaleChart.setData(data);
        yesterdaySaleChart.getBarData().setBarWidth(BAR_WIDTH);
        yesterdaySaleChart.setFitBars(true);

//        float groupSpace = 1f - ((BAR_SPACE + BAR_WIDTH) * GROUPS);
//        yesterdaySaleChart.groupBars(0, groupSpace, BAR_SPACE);

//        tv_Xaxis.setText("X-Axis = "+ "Fuel Type");
//        tv_Yaxis.setText("Y-Axis = "+ "Quantity (Ltrs)");


        yesterdaySaleChart.invalidate();
    }
    private void configureYesterdayChartAppearance() {

        yesterdaySaleChart.setPinchZoom(false);
        yesterdaySaleChart.setDrawBarShadow(false);
        yesterdaySaleChart.setDrawGridBackground(false);
        yesterdaySaleChart.getXAxis().setDrawGridLines(false);
        yesterdaySaleChart.getAxisLeft().setDrawGridLines(false);
        yesterdaySaleChart.getAxisRight().setDrawGridLines(false);
        yesterdaySaleChart.getDescription().setEnabled(false);
        yesterdaySaleChart.getLegend().setEnabled(false);
        yesterdaySaleChart.setFitBars(true);

        XAxis xAxis = yesterdaySaleChart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(false);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM_INSIDE);
        xAxis.setTextColor(getContext().getResources().getColor(R.color.black));
        xVal.add(String.valueOf(catArray));
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVal));

        final YAxis leftAxis = yesterdaySaleChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f);
//        leftAxis.setTextColor(getResources().getColor(R.color.button_light_blue));


        yesterdaySaleChart.getAxisRight().setEnabled(false);

//        yesterdaySaleChart.getXAxis().setAxisMinimum(0);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        yesterdaySaleChart.getLegend().setTextColor(getContext().getResources().getColor(R.color.black));
//        xAxis.setLabelCount(5);
        int monthsize = catArray.length();
        float f2 = (float) monthsize / 1;
        xAxis.setAxisMaximum(f2);
        List<String> catData = new ArrayList<>();
        for (int j =0;j<catArray.length();j++){
            try {
                catData.add(catArray.getString(j));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        catData.add(" ");
//        todayvalueData.add(" ");
        try {
            yesterdaySaleChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(catData));
            if(yesterdaySaleChart.getXAxis().getLabelCount() == 7||
                    yesterdaySaleChart.getXAxis().getLabelCount() == 8||
                    yesterdaySaleChart.getXAxis().getLabelCount() == 9){
                yesterdaySaleChart.getXAxis().setTextSize(1+ yesterdaySaleChart.getXAxis().getLabelCount());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    //Yesterdays Sales End

    //Cummulative Sales Start
    private void populateFuelList() {
        fuelList = new ArrayList<>();
//        fuelList.add("ALL");
        fuelList.add("HSD");
        fuelList.add("MS");
        fuelList.add("POWER");
        fuelList.add("TURBOJET");
        fuelList.add("POWER 99");
    }
    private void CallcumulativeSales(){
        JSONObject json = new JSONObject();
        try {
            String dateStr = "10/12/2020";
            String lmDatestr = "11/01/2021";
            SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
            Date dateObj = null;
            Date lmDateObj = null;
            try {
                dateObj = curFormater.parse(dateStr);
                lmDateObj = curFormater.parse(lmDatestr);
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            SimpleDateFormat postFormater = new SimpleDateFormat("MMM YYYY");
//            String newDateStr = postFormater.format(dateObj);
//            String lastDateStr = postFormater.format(lmDateObj);
            String PreviousDate, CurrentDate;
            SimpleDateFormat curFormater1 = new SimpleDateFormat("dd/MM/yyyy");
            Date d = new Date();
            CurrentDate = curFormater1.format(d);
//            Date Dateobj1 = curFormater1.format()
            PreviousDate = curFormater1.format(getLastMonthLastDate());

            Date dateObj1 = null;
            Date lmDateObj1 = null;
            try {
                dateObj1 = curFormater.parse(CurrentDate);
                lmDateObj1 = curFormater.parse(PreviousDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            SimpleDateFormat postFormater1 = new SimpleDateFormat("MMM YYYY");
            String Currendate1 = postFormater1.format(dateObj1);
            String PreviousDate1 = postFormater1.format(lmDateObj1);
            CSMonth1 =Currendate1;
            CSMonth2= PreviousDate1;

            json.put("fromDate",PreviousDate1);
            json.put("toDate",Currendate1);
            json.put("ProductType",fuel_type);
//            json.put("fromDate","DEC 2020");
//            json.put("toDate","JAN 2021");

            try {
                if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Zone")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").equalsIgnoreCase("ALL")){
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone",""));
                                json.put("ROCode", "ALL");
                            }else{
                                json.put("ROCode", "ALL");
                            }

                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Region")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone",""));
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion",""));
                                json.put("ROCode", "ALL");
                            }else{
                                json.put("ROCode", "ALL");
                            }
                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone",""));
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion",""));
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea",""));
                                json.put("ROCode", "ALL");
                            }else{
                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "Selectdzone", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{

                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "Selectdzone", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO",""));
                    }

                }
                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Region")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion",""));
                                json.put("ROCode", "ALL");
                            }else{
                                json.put("ROCode", "ALL");
                            }
                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion",""));
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea",""));
                                json.put("ROCode", "ALL");
                            }
                            else{
                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO",""));
                    }

                }
                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){

                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea",""));
                                json.put("ROCode", "ALL");
                            }else{
                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }

                        //                    try {
                        //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                        //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                        //
                        //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        //                                    "SelectdSalesArea",""));
                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        //                                    "FilteredRO",""));
                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                        //                                    "FirstFilteredRO", "");
                        //                        }else{
                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        //                                    "FilteredRO",""));
                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                        //                                    "FirstFilteredRO", "");
                        //                        }
                        //                    } catch (Exception e) {
                        //                        e.printStackTrace();
                        //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        //                                "FilteredRO",""));
                        //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                        //                                "FirstFilteredRO", "");
                        //                    }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO",""));
                    }

                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO",""));
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "ROKey","") );
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
//            try {
//                json.put("ROCode", " 31957060");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
        new AsyncCummulativeSales().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);

    }
    private class AsyncCummulativeSales extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public AsyncCummulativeSales() {
        }

        public AsyncCummulativeSales(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getContext());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Dashboard/CumulativeSales", false, jsonObjects[0], "");
        }


        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
                if (!isAdded()) {
                    return;
                }
                if (isDetached()) {
                    return;
                }

            if (s != null) {

                try {

                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("Data"));
                            JSONObject jsonObject_first_month = new JSONObject(jsonObject1.getString("firstmonth"));
                            JSONObject jsonObject_second_month = new JSONObject(jsonObject1.getString("secondMonth"));

                            arraymonth1 = new JSONArray(jsonObject_first_month.getString("month"));
                            arrayHSD1 = new JSONArray(jsonObject_first_month.getString("HSD"));
                            arrayMS1 = new JSONArray(jsonObject_first_month.getString("MS"));
                            arrayPower1 = new JSONArray(jsonObject_first_month.getString("Power"));
                            arrayTurbojet1 = new JSONArray(jsonObject_first_month.getString("Turbojet"));

                            arraymonth2 = new JSONArray(jsonObject_second_month.getString("month"));
                            arrayHSD2 = new JSONArray(jsonObject_second_month.getString("HSD"));
                            arrayMS2 = new JSONArray(jsonObject_second_month.getString("MS"));
                            arrayPower2 = new JSONArray(jsonObject_second_month.getString("Power"));
                            arrayTurbojet2 = new JSONArray(jsonObject_second_month.getString("Turbojet"));

                            List<Integer> list_month1 = new ArrayList<Integer>();
                            List<Integer> list_HSD1 = new ArrayList<Integer>();
                            List<Integer> listMS1 = new ArrayList<Integer>();
                            List<Integer> listPower1 = new ArrayList<Integer>();
                            List<Integer> listTurbojet1 = new ArrayList<Integer>();

                            List<Integer> list_month2 = new ArrayList<Integer>();
                            List<Integer> list_HSD2 = new ArrayList<Integer>();
                            List<Integer> listMS2 = new ArrayList<Integer>();
                            List<Integer> listPower2 = new ArrayList<Integer>();
                            List<Integer> listTurbojet2 = new ArrayList<Integer>();
                            for (int i1 = 0; i1 < arraymonth1.length(); i1++) {
                                list_month1.add(arraymonth1.getInt(i1));
                            }
                            for (int i = 0; i < arrayHSD1.length(); i++) {
                                list_HSD1.add(arrayHSD1.getInt(i));
                            }
                            for (int ms = 0; ms < arrayMS1.length(); ms++) {
                                listMS1.add(arrayMS1.getInt(ms));
                            }
                            for (int power = 0; power < arrayPower1.length(); power++) {
                                listPower1.add(arrayPower1.getInt(power));
                            }
                            for (int turbojet = 0; turbojet < arrayTurbojet1.length(); turbojet++) {
                                listTurbojet1.add(arrayTurbojet1.getInt(turbojet));
                            }
                            for (int i1 = 0; i1 < arraymonth2.length(); i1++) {
                                list_month2.add(arraymonth2.getInt(i1));
                            }
                            for (int i = 0; i < arrayHSD2.length(); i++) {
                                list_HSD2.add(arrayHSD2.getInt(i));
                            }
                            for (int ms1 = 0; ms1 < arrayMS2.length(); ms1++) {
                                listMS2.add(arrayMS2.getInt(ms1));
                            }
                            for (int power = 0; power < arrayPower2.length(); power++) {
                                listPower2.add(arrayPower2.getInt(power));
                            }
                            for (int turbojet = 0; turbojet < arrayTurbojet2.length(); turbojet++) {
                                listTurbojet2.add(arrayTurbojet2.getInt(turbojet));
                            }
                            int smallestmonth1 = list_month1.get(0);
                            int biggestmonth1 = list_month1.get(0);
                            int smallest_HSD1 = list_HSD1.get(0);
                            int biggest_HSD1 = list_HSD1.get(0);
                            int smallestMS1 = listMS1.get(0);
                            int biggestMS1 = listMS1.get(0);
                            int smallestPower1 = listPower1.get(0);
                            int biggestPower1 = listPower1.get(0);
                            int smallestTurbojet1 = listTurbojet1.get(0);
                            int biggestTurbojet1 = listTurbojet1.get(0);
                            int smallestmonth2 = list_month2.get(0);
                            int biggestmonth2 = list_month2.get(0);
                            int smallest_HSD2 = list_HSD2.get(0);
                            int biggest_HSD2 = list_HSD2.get(0);
                            int smallestMS2 = listMS2.get(0);
                            int biggestMS2 = listMS2.get(0);
                            int smallestPower2 = listPower2.get(0);
                            int biggestPower2 = listPower2.get(0);
                            int smallestTurbojet2 = listTurbojet2.get(0);
                            int biggestTurbojet2 = listTurbojet2.get(0);

                            for (int i = 1; i < list_month1.size(); i++) {
                                if (list_month1.get(i) > biggestmonth1)
                                    biggestmonth1 = list_month1.get(i);
                                else if (list_month1.get(i) < smallestmonth1)
                                    smallestmonth1 = list_month1.get(i);
                            }
                            for (int i2 = 1; i2 < list_HSD1.size(); i2++) {
                                if (list_HSD1.get(i2) > biggest_HSD1)
                                    biggest_HSD1 = list_HSD1.get(i2);
                                else if (list_HSD1.get(i2) < smallest_HSD1)
                                    smallest_HSD1 = list_HSD1.get(i2);

                            }
                            for (int ms = 1; ms < listMS1.size(); ms++) {
                                if (listMS1.get(ms) > biggestMS1)
                                    biggestMS1 = listMS1.get(ms);
                                else if (listMS1.get(ms) < smallestMS1)
                                    smallestMS1 = listMS1.get(ms);

                            }
                            for (int power = 1; power < listPower1.size(); power++) {
                                if (listPower1.get(power) > biggestPower1)
                                    biggestPower1 = listPower1.get(power);
                                else if (listPower1.get(power) < smallestPower1)
                                    smallestPower1 = listPower1.get(power);
                            }
                            for (int turbojet = 1; turbojet < listTurbojet1.size(); turbojet++) {
                                if (listTurbojet1.get(turbojet) > biggestTurbojet1)
                                    biggestTurbojet1 = listTurbojet1.get(turbojet);
                                else if (listTurbojet1.get(turbojet) < smallestTurbojet1)
                                    smallestTurbojet1 = listTurbojet1.get(turbojet);

                            }

                            for (int i = 1; i < list_month2.size(); i++) {
                                if (list_month2.get(i) > biggestmonth2)
                                    biggestmonth2 = list_month2.get(i);
                                else if (list_month2.get(i) < smallestmonth2)
                                    smallestmonth2 = list_month2.get(i);
                            }
                            for (int i2 = 1; i2 < list_HSD2.size(); i2++) {
                                if (list_HSD2.get(i2) > biggest_HSD2)
                                    biggest_HSD2 = list_HSD2.get(i2);
                                else if (list_HSD2.get(i2) < smallest_HSD2)
                                    smallest_HSD2 = list_HSD2.get(i2);

                            }
                            for (int ms1 = 1; ms1 < listMS2.size(); ms1++) {
                                if (listMS2.get(ms1) > biggestMS2)
                                    biggestMS2 = listMS2.get(ms1);
                                else if (listMS2.get(ms1) < smallestMS2)
                                    smallestMS2 = listMS2.get(ms1);

                            }
                            for (int power = 1; power < listPower2.size(); power++) {
                                if (listPower2.get(power) > biggestPower2)
                                    biggestPower2 = listPower2.get(power);
                                else if (listPower2.get(power) < smallestPower2)
                                    smallestPower2 = listPower2.get(power);
                            }
                            for (int turbojet = 1; turbojet < listTurbojet2.size(); turbojet++) {
                                if (listTurbojet2.get(turbojet) > biggestTurbojet2)
                                    biggestTurbojet2 = listTurbojet2.get(turbojet);
                                else if (listTurbojet2.get(turbojet) < smallestTurbojet2)
                                    smallestTurbojet2 = listTurbojet2.get(turbojet);

                            }
                            biggestmonthmain1 = biggestmonth1;
                            smallestmonthmain1 = smallestmonth1;
                            biggest_HSDmain1 = biggest_HSD1;
                            smallest_HSDmain1 = smallest_HSD1;
                            biggestMSmain1 = biggestMS1;
                            smallestMSmain1 = smallestMS1;
                            biggestPowermain1 = biggestPower1;
                            smallestPowermain1 = smallestPower1;
                            biggestTurbojetmain1 = biggestTurbojet1;
                            smallestTurbojetmain1 = smallestTurbojet1;
                            biggestmonthmain2 = biggestmonth2;
                            smallestmonthmain2 = smallestmonth2;
                            biggest_HSDmain2 = biggest_HSD2;
                            smallest_HSDmain2 = smallest_HSD2;
                            biggestMSmain2 = biggestMS2;
                            smallestMSmain2 = smallestMS2;
                            biggestPowermain2 = biggestPower2;
                            smallestPowermain2 = smallestPower2;
                            biggestTurbojetmain2 = biggestTurbojet2;
                            smallestTurbojetmain2 = smallestTurbojet2;////                        revArrayList=  new ArrayList<Integer>();

                            renderData();

                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    alertDialogInterface, "", error, getContext().getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                            mChart.setNoDataText("NIL");
                            mChart.invalidate();
                            mChart.clear();
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
//                                                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                                                startActivity(intent);
//                                                getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    alertDialogInterface, "", error, getContext().getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                            mChart.setNoDataText("NIL");
                            mChart.invalidate();
                            mChart.clear();
                        }
                    }else{
                        mChart.setNoDataText("NIL");
                        mChart.invalidate();
                        mChart.clear();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    mChart.setNoDataText("NIL");
                    mChart.invalidate();
                    mChart.clear();
                }
            }
        }
    }
    private void renderData() {

        LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setLineWidth(4f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setGranularity(1f);

        xAxis.setTextColor(getContext().getResources().getColor(R.color.white));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.enableGridDashedLine(00f, 00f, 0f);
        int[] biggestallmonth = {biggestmonthmain1, biggestmonthmain2};
        int[] smallestallmonth = {smallestmonthmain1, smallestmonthmain2};
        int smallestallmonth_main = smallestallmonth[0];
        int biggestallmonth_main = biggestallmonth[0];
        List<Integer> listsmallestAllmonth = new ArrayList<Integer>();
        List<Integer> listbiggestAllmonth = new ArrayList<Integer>();
        for (int i1=0; i1<biggestallmonth.length; i1++) {
            listbiggestAllmonth.add(biggestallmonth[i1]);
        }
        for (int i1=0; i1<smallestallmonth.length; i1++) {
            listsmallestAllmonth.add(smallestallmonth[i1]);
        }

        for(int i=1; i< listbiggestAllmonth.size(); i++)
        {
            if(listbiggestAllmonth.get(i) > biggestallmonth_main)
                biggestallmonth_main = listbiggestAllmonth.get(i);
        }
        for(int i=1; i< listbiggestAllmonth.size(); i++)
        {
            if (listsmallestAllmonth.get(i) < smallestallmonth_main)
                smallestallmonth_main = listsmallestAllmonth.get(i);
        }
        float f2 = (float) biggestallmonth_main / 1;
        float f3 = (float) smallestallmonth_main / 1;
//        xAxis.setAxisMaximum(10f);
//        xAxis.setAxisMinimum(0f);
        xAxis.setAxisMaximum(f2);
        xAxis.setAxisMinimum(f3);

        xAxis.setTextColor(getContext().getResources().getColor(R.color.black));
        xAxis.setDrawLimitLinesBehindData(true);

        LimitLine ll1 = new LimitLine(215f, "Maximum Limit");
        ll1.setLineWidth(4f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll1.setTextSize(10f);

        LimitLine ll2 = new LimitLine(70f, "Minimum Limit");
        ll2.setLineWidth(4f);
        ll2.enableDashedLine(00f, 00f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setGranularity(1f);
        leftAxis.removeAllLimitLines();
        leftAxis.setTextColor(getContext().getResources().getColor(R.color.black));

//        leftAxis.addLimitLine(ll1);
//        leftAxis.addLimitLine(ll2);
        int[] biggestall = {biggest_HSDmain1, biggest_HSDmain2, biggestMSmain1,
                biggestMSmain2,biggestPowermain1,biggestPowermain2,biggestTurbojetmain1,biggestTurbojetmain2};
        int[] smallestall = {smallest_HSDmain1, smallest_HSDmain2, smallestMSmain1,
                smallestMSmain2,smallestPowermain1,smallestPowermain2,smallestTurbojetmain1,smallestTurbojetmain1};
        int smallestall1 = smallestall[0];
        int biggestall1 = biggestall[0];
        List<Integer> listsmallestAll = new ArrayList<Integer>();
        List<Integer> listbiggestAll = new ArrayList<Integer>();
        for (int i1=0; i1<biggestall.length; i1++) {
            listbiggestAll.add(biggestall[i1]);
        }
        for (int i1=0; i1<smallestall.length; i1++) {
            listsmallestAll.add(smallestall[i1]);
        }

        for(int i=1; i< listbiggestAll.size(); i++)
        {
            if(listbiggestAll.get(i) > biggestall1)
                biggestall1 = listbiggestAll.get(i);
        }
        for(int i=1; i< listbiggestAll.size(); i++)
        {
            if (listsmallestAll.get(i) < smallestall1)
                smallestall1 = listsmallestAll.get(i);
        }
        float f=0.0f,f1=0.0f;
        if(fuel_type.equalsIgnoreCase("HSD")){
            int[] biggestallHSD = {biggest_HSDmain1, biggest_HSDmain2};
            int[] smallestallHSD = {smallest_HSDmain1, smallest_HSDmain2};
            int smallestallHSD_main = biggestallHSD[0];
            int biggestallHSD_main = smallestallHSD[0];
            List<Integer> listsmallestAllHSD = new ArrayList<Integer>();
            List<Integer> listbiggestAllHSD = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallHSD.length; i1++) {
                listbiggestAllHSD.add(biggestallHSD[i1]);
            }
            for (int i1=0; i1<smallestallHSD.length; i1++) {
                listsmallestAllHSD.add(smallestallHSD[i1]);
            }

            for(int i=0; i< listbiggestAllHSD.size(); i++)
            {
                if(listbiggestAllHSD.get(i) > biggestallHSD_main)
                    biggestallHSD_main = listbiggestAllHSD.get(i);
            }
            for(int i=0; i< listbiggestAllHSD.size(); i++)
            {
                if (listsmallestAllHSD.get(i) < smallestallHSD_main)
                    smallestallHSD_main = listsmallestAllHSD.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallHSD_main / 1;
            f1 = (float) smallestallHSD_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("POWER")){
            int[] biggestallPOWER = {biggestPowermain1, biggestPowermain2};
            int[] smallestallPOWER = {smallestPowermain1, smallestPowermain2};
            int smallestallPOWER_main = biggestallPOWER[0];
            int biggestallPOWER_main = biggestallPOWER[0];
            List<Integer> listsmallestAllPOWER = new ArrayList<Integer>();
            List<Integer> listbiggestAllPOWER = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallPOWER.length; i1++) {
                listbiggestAllPOWER.add(biggestallPOWER[i1]);
            }
            for (int i1=0; i1<smallestallPOWER.length; i1++) {
                listsmallestAllPOWER.add(smallestallPOWER[i1]);
            }

            for(int i=0; i< listbiggestAllPOWER.size(); i++)
            {
                if(listbiggestAllPOWER.get(i) > biggestallPOWER_main)
                    biggestallPOWER_main = listbiggestAllPOWER.get(i);
            }
            for(int i=0; i< listbiggestAllPOWER.size(); i++)
            {
                if (listsmallestAllPOWER.get(i) < smallestallPOWER_main)
                    smallestallPOWER_main = listsmallestAllPOWER.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallPOWER_main / 1;
            f1 = (float) smallestallPOWER_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("MS")){
            int[] biggestallMS = {biggestMSmain1, biggestMSmain2};
            int[] smallestallMS = {smallestMSmain1, smallestMSmain2};
            int smallestallMS_main = biggestallMS[0];
            int biggestallMS_main = smallestallMS[0];
            List<Integer> listsmallestAllMS = new ArrayList<Integer>();
            List<Integer> listbiggestAllMS = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallMS.length; i1++) {
                listbiggestAllMS.add(biggestallMS[i1]);
            }
            for (int i1=0; i1<smallestallMS.length; i1++) {
                listsmallestAllMS.add(smallestallMS[i1]);
            }

            for(int i=0; i< listbiggestAllMS.size(); i++)
            {
                if(listbiggestAllMS.get(i) > biggestallMS_main)
                    biggestallMS_main = listbiggestAllMS.get(i);
            }
            for(int i=0; i< listbiggestAllMS.size(); i++)
            {
                if (listsmallestAllMS.get(i) < smallestallMS_main)
                    smallestallMS_main = listsmallestAllMS.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallMS_main / 1;
            f1 = (float) smallestallMS_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("TURBOJET")){
            int[] biggestallTURBOJET = {biggestTurbojetmain1, biggestTurbojetmain2};
            int[] smallestallTURBOJET= {smallestTurbojetmain1, smallestTurbojetmain2};
            int smallestallTURBOJET_main = biggestallTURBOJET[0];
            int biggestallTURBOJET_main = smallestallTURBOJET[0];
            List<Integer> listsmallestAllTURBOJET = new ArrayList<Integer>();
            List<Integer> listbiggestAllTURBOJET = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallTURBOJET.length; i1++) {
                listbiggestAllTURBOJET.add(biggestallTURBOJET[i1]);
            }
            for (int i1=0; i1<smallestallTURBOJET.length; i1++) {
                listsmallestAllTURBOJET.add(smallestallTURBOJET[i1]);
            }

            for(int i=0; i< listbiggestAllTURBOJET.size(); i++)
            {
                if(listbiggestAllTURBOJET.get(i) > biggestallTURBOJET_main)
                    biggestallTURBOJET_main = listbiggestAllTURBOJET.get(i);
            }
            for(int i=0; i< listbiggestAllTURBOJET.size(); i++)
            {
                if (listsmallestAllTURBOJET.get(i) < smallestallTURBOJET_main)
                    smallestallTURBOJET_main = listsmallestAllTURBOJET.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallTURBOJET_main / 1;
            f1 = (float) smallestallTURBOJET_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("ALL")){
            f = (float) biggestall1 / 1;
            f1 = (float) smallestall1 / 1;
        }

//        leftAxis.setAxisMaximum(350f);
//        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(f);
        leftAxis.setAxisMinimum(f1);
//        leftAxis.setAxisMaximum(f2);
//        leftAxis.setAxisMinimum(f3);
        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(false);
        mChart.getLegend().setTextColor(getContext().getResources().getColor(R.color.black));
        mChart.getDescription().setEnabled(false);
        mChart.getAxisRight().setEnabled(false);
        mChart.setTouchEnabled(true);
        mChart.setPinchZoom(true);
        MyMarkerView mv = new MyMarkerView(getContext(), R.layout.custom_marker_view);
        mv.setChartView(mChart);
        mChart.setMarker(mv);
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
        mChart.animateXY(3000,2000);
        setData();
    }
    private void setData() {

//        ArrayList<Entry> values = new ArrayList<>();
//        ArrayList<Entry> values2 = new ArrayList<>();
        ArrayList<Entry> valuesHSD1 = new ArrayList<>();
        ArrayList<Entry> valuesMS1 = new ArrayList<>();
        ArrayList<Entry> valuesPower1 = new ArrayList<>();
        ArrayList<Entry> valuesTurbojet1 = new ArrayList<>();
        ArrayList<Entry> valuesHSD2 = new ArrayList<>();
        ArrayList<Entry> valuesMS2 = new ArrayList<>();
        ArrayList<Entry> valuesPower2 = new ArrayList<>();
        ArrayList<Entry> valuesTurbojet2 = new ArrayList<>();
        if(fuel_type.equalsIgnoreCase("HSD")){
            for(int i = 0; i < arrayHSD1.length(); i++){
                try {
                    valuesHSD1.add(new Entry(arraymonth1.getInt(i), arrayHSD1.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayHSD2.length(); i++){
                try {
                    valuesHSD2.add(new Entry(arraymonth2.getInt(i), arrayHSD2.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else if(fuel_type.equalsIgnoreCase("POWER")){
            for(int i = 0; i < arrayPower1.length(); i++){
                try {
                    valuesPower1.add(new Entry(arraymonth1.getInt(i), arrayPower1.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayPower2.length(); i++){
                try {
                    valuesPower2.add(new Entry(arraymonth2.getInt(i), arrayPower2.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else if(fuel_type.equalsIgnoreCase("MS")){
            for(int i = 0; i < arrayMS1.length(); i++){
                try {
                    valuesMS1.add(new Entry(arraymonth1.getInt(i), arrayMS1.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayMS2.length(); i++){
                try {
                    valuesMS2.add(new Entry(arraymonth2.getInt(i), arrayMS2.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else if(fuel_type.equalsIgnoreCase("TURBOJET")){
            for(int i = 0; i < arrayTurbojet1.length(); i++){
                try {
                    valuesTurbojet1.add(new Entry(arraymonth1.getInt(i), arrayTurbojet1.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayTurbojet2.length(); i++){
                try {
                    valuesTurbojet2.add(new Entry(arraymonth2.getInt(i), arrayTurbojet2.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }else if(fuel_type.equalsIgnoreCase("ALL")){
//            for(int i = 0; i < arrayHSD.length(); i++){
//                try {
//                    valuesHSD.add(new Entry(arraymonth.getInt(i), arrayHSD.getInt(i)));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            for(int i = 0; i < arrayMS.length(); i++){
//                try {
//                    valuesMS.add(new Entry(arraymonth.getInt(i), arrayMS.getInt(i)));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            for(int i = 0; i < arrayPower.length(); i++){
//                try {
//                    valuesPower.add(new Entry(arraymonth.getInt(i), arrayPower.getInt(i)));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            for(int i = 0; i < arrayTurbojet.length(); i++){
//                try {
//                    valuesTurbojet.add(new Entry(arraymonth.getInt(i), arrayTurbojet.getInt(i)));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
        }

        LineDataSet setHSD1,setHSD2;
        LineDataSet setPower1,setPower2;
        LineDataSet setMS1,setMS2;
        LineDataSet setTurbojet1,setTurbojet2;
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        LineData data;
       /* if(!(fuel_type.equals("Select") || from_month.equals("Select Month")||
                to_month.equals("Select Month"))){
            tv_Xaxis.setText("X-Axis = "+ "Days");
            tv_Yaxis.setText("Y-Axis = "+ "Quantity(Ltrs)");
        }else{
            tv_Xaxis.setVisibility(View.GONE);
            tv_Yaxis.setVisibility(View.GONE);
        }*/
        if(fuel_type.equalsIgnoreCase("HSD")){
//            setHSD1 = new LineDataSet(valuesHSD1, "Month 1");
//            setHSD2 = new LineDataSet(valuesHSD2, "Month 2");
            setHSD1 = new LineDataSet(valuesHSD1, CSMonth2);
            setHSD2 = new LineDataSet(valuesHSD2, CSMonth1);
            setHSD1.setValueTextColor(getContext().getResources().getColor(R.color.white));
            setHSD1.setDrawIcons(false);
//            setHSD1.enableDashedLine(10f, 0f, 0f);
//            setHSD1.enableDashedHighlightLine(10f, 0f, 0f);
            setHSD1.setColor(getContext().getResources().getColor(R.color.hsdcolor));
            setHSD1.setCircleColor(getContext().getResources().getColor(R.color.dot_light_screen2));
            setHSD1.setLineWidth(1f);
            setHSD1.setCircleRadius(3f);
            setHSD1.setDrawCircleHole(false);
            setHSD1.setValueTextSize(9f);
            setHSD1.setDrawFilled(false);
            setHSD1.setFormLineWidth(1f);

//            setHSD1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setHSD1.setFormSize(15.f);
            setHSD2.setValueTextColor(getContext().getResources().getColor(R.color.white));
            setHSD2.setDrawIcons(false);
//            setHSD2.enableDashedLine(10f, 0f, 0f);
//            setHSD2.enableDashedHighlightLine(10f, 0f, 0f);
            setHSD2.setColor(getContext().getResources().getColor(R.color.light_blue_button3));
            setHSD2.setCircleColor(getContext().getResources().getColor(R.color.dot_light_screen2));
            setHSD2.setLineWidth(1f);
            setHSD2.setCircleRadius(2f);
            setHSD2.setDrawCircleHole(false);
            setHSD2.setValueTextSize(9f);
            setHSD2.setDrawFilled(false);
            setHSD2.setFormLineWidth(1f);
//            setHSD2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setHSD2.setFormSize(15.f);
           /* if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_hsd);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setHSD1.setFillDrawable(drawable);
                setHSD2.setFillDrawable(drawable1);
            } else {
                setHSD1.setFillColor(Color.DKGRAY);
                setHSD2.setFillColor(Color.DKGRAY);
            }*/
            dataSets.add(setHSD1);
            dataSets.add(setHSD2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();


        }
        else if(fuel_type.equalsIgnoreCase("MS")){
//            setMS1 = new LineDataSet(valuesMS1, "Month 1");
//            setMS2 = new LineDataSet(valuesMS2, "Month 2");
            setMS1 = new LineDataSet(valuesMS1,   CSMonth2);
            setMS2 = new LineDataSet(valuesMS2,   CSMonth1);


            setMS1.setValueTextColor(getContext().getResources().getColor(R.color.white));
            setMS1.setDrawIcons(false);
//            setMS1.enableDashedLine(10f, 0f, 0f);
//            setMS1.enableDashedHighlightLine(10f, 0f, 0f);
            setMS1.setColor(getContext().getResources().getColor(R.color.mscolor));
            setMS1.setCircleColor(getContext().getResources().getColor(R.color.dot_light_screen2));
            setMS1.setLineWidth(1f);
            setMS1.setCircleRadius(2f);
            setMS1.setDrawCircleHole(false);
            setMS1.setValueTextSize(9f);
            setMS1.setDrawFilled(false);
            setMS1.setFormLineWidth(1f);
//            setMS1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setMS1.setFormSize(15.f);
            setMS2.setValueTextColor(getContext().getResources().getColor(R.color.white));
            setMS2.setDrawIcons(false);
//            setMS2.enableDashedLine(10f, 0f, 0f);
//            setMS2.enableDashedHighlightLine(10f, 0f, 0f);
            setMS2.setColor(getContext().getResources().getColor(R.color.light_blue_button3));
            setMS2.setCircleColor(getContext().getResources().getColor(R.color.dot_light_screen2));
            setMS2.setLineWidth(1f);
            setMS2.setCircleRadius(3f);
            setMS2.setDrawCircleHole(false);
            setMS2.setValueTextSize(9f);
            setMS2.setDrawFilled(false);
            setMS2.setFormLineWidth(1f);
//            setMS2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setMS2.setFormSize(15.f);
            /*if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_ms);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setMS1.setFillDrawable(drawable);
                setMS2.setFillDrawable(drawable1);
            } else {
                setMS1.setFillColor(Color.DKGRAY);
                setMS2.setFillColor(Color.DKGRAY);
            }*/
            dataSets.add(setMS1);
            dataSets.add(setMS2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();


        }
        else if(fuel_type.equalsIgnoreCase("POWER")){
//            setPower1 = new LineDataSet(valuesPower1, "Month 1");
//            setPower2 = new LineDataSet(valuesPower2, "Month 2");
            setPower1 = new LineDataSet(valuesPower1,  CSMonth2);
            setPower2 = new LineDataSet(valuesPower2,  CSMonth1);


            setPower1.setValueTextColor(getContext().getResources().getColor(R.color.white));
            setPower1.setDrawIcons(false);
//            setPower1.enableDashedLine(10f, 0f, 0f);
//            setPower1.enableDashedHighlightLine(10f, 0f, 0f);
            setPower1.setColor(getContext().getResources().getColor(R.color.powercolor));
            setPower1.setCircleColor(getContext().getResources().getColor(R.color.dot_light_screen2));
            setPower1.setLineWidth(1f);
            setPower1.setCircleRadius(3f);
            setPower1.setDrawCircleHole(false);
            setPower1.setValueTextSize(9f);
            setPower1.setDrawFilled(false);
            setPower1.setFormLineWidth(1f);
//            setPower1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setPower1.setFormSize(15.f);
            setPower2.setValueTextColor(getContext().getResources().getColor(R.color.white));
            setPower2.setDrawIcons(false);
//            setPower2.enableDashedLine(10f, 0f, 0f);
//            setPower2.enableDashedHighlightLine(10f, 0f, 0f);
            setPower2.setColor(getContext().getResources().getColor(R.color.light_blue_button3));
            setPower2.setCircleColor(getContext().getResources().getColor(R.color.dot_light_screen2));
            setPower2.setLineWidth(1f);
            setPower2.setCircleRadius(3f);
            setPower2.setDrawCircleHole(false);
            setPower2.setValueTextSize(9f);
            setPower2.setDrawFilled(false);
            setPower2.setFormLineWidth(1f);
//            setPower2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setPower2.setFormSize(15.f);
            /*if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_power);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setPower1.setFillDrawable(drawable);
                setPower2.setFillDrawable(drawable1);
            } else {
                setPower1.setFillColor(Color.DKGRAY);
                setPower2.setFillColor(Color.DKGRAY);
            }*/
            dataSets.add(setPower1);
            dataSets.add(setPower2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();

        }
        else if(fuel_type.equalsIgnoreCase("TURBOJET")){
//            setTurbojet1 = new LineDataSet(valuesTurbojet1, "Month 1");
//            setTurbojet2 = new LineDataSet(valuesTurbojet2, "Month 2");
            setTurbojet1 = new LineDataSet(valuesTurbojet1, CSMonth2);
            setTurbojet2 = new LineDataSet(valuesTurbojet2, CSMonth1);
            setTurbojet1.setValueTextColor(getContext().getResources().getColor(R.color.white));
            setTurbojet1.setDrawIcons(false);
//            setTurbojet1.enableDashedLine(10f, 0f, 0f);
//            setTurbojet1.enableDashedHighlightLine(10f, 0f, 0f);
            setTurbojet1.setColor(getContext().getResources().getColor(R.color.turbojetcolor));
            setTurbojet1.setCircleColor(getContext().getResources().getColor(R.color.dot_light_screen2));
            setTurbojet1.setLineWidth(1f);
            setTurbojet1.setCircleRadius(2f);
            setTurbojet1.setDrawCircleHole(false);
            setTurbojet1.setValueTextSize(9f);
            setTurbojet1.setDrawFilled(false);
            setTurbojet1.setFormLineWidth(1f);
//            setTurbojet1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setTurbojet1.setFormSize(15.f);
            setTurbojet2.setValueTextColor(getContext().getResources().getColor(R.color.white));
            setTurbojet2.setDrawIcons(false);
//            setTurbojet2.enableDashedLine(10f, 0f, 0f);
//            setTurbojet2.enableDashedHighlightLine(10f, 0f, 0f);
            setTurbojet2.setColor(getContext().getResources().getColor(R.color.light_blue_button3));
            setTurbojet2.setCircleColor(getContext().getResources().getColor(R.color.dot_light_screen2));
            setTurbojet2.setLineWidth(1f);
            setTurbojet2.setCircleRadius(3f);
            setTurbojet2.setDrawCircleHole(false);
            setTurbojet2.setValueTextSize(9f);
            setTurbojet2.setDrawFilled(false);
            setTurbojet2.setFormLineWidth(1f);
//            setTurbojet2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setTurbojet2.setFormSize(15.f);
           /* if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_turbojet);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setTurbojet1.setFillDrawable(drawable);
                setTurbojet2.setFillDrawable(drawable1);
            } else {
                setTurbojet1.setFillColor(Color.DKGRAY);
                setTurbojet2.setFillColor(Color.DKGRAY);
            }*/
            dataSets.add(setTurbojet1);
            dataSets.add(setTurbojet2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();

        }
    }
    public static Date getLastMonthLastDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH, -1);

        int max = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        calendar.set(Calendar.DAY_OF_MONTH, max);

        return calendar.getTime();
    }
    //Cummulative Sales End

    //RO Connectivity Status Start
    private void getROChartData() {

        JSONObject json = new JSONObject();

        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{

                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }
                        else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new Asyncro_exception().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }
    private class Asyncro_exception extends AsyncTask<JSONObject, Void, String>{

        String strTimeStamp = "";

        public Asyncro_exception() {
        }

        public Asyncro_exception(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getContext());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Status/ConnectivityStatus", false, jsonObjects[0], "");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if (respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject data = jsonObject.getJSONObject("Data");
                            JSONArray valuesArray = data.getJSONArray("Value");
                            JSONArray jsonArray = data.getJSONArray("Category");
                            roModels = new ArrayList<>();
                            roData = new ArrayList<String>();
                            for (int i1 = 0; i1 < valuesArray.length(); i1++) {
                                ROMOdel romOdel = new ROMOdel();
                                romOdel.setCategory(jsonArray.getString(i1));
                                romOdel.setRovalue(valuesArray.getString(i1));
                                roModels.add(romOdel);
//                                    roData.add(valuesArray.getString(i1));
                            }
                            listKeys = new ArrayList<String>();
                            listValues = new ArrayList<Integer>();
                            for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                                listKeys.add(jsonArray.getString(i1));
                            }
                            for (int value = 0; value < valuesArray.length(); value++) {
                                listValues.add(Integer.valueOf(valuesArray.getString(value)));
                            }
                            getroEntries();
                            pieDataSet = new PieDataSet(pieEntries, "");
                            pieData = new PieData(pieDataSet);
                            ro_piechart.setData(pieData);
                            ro_piechart.getData().setDrawValues(false);
                            ro_piechart.setDrawEntryLabels(false);
                            ro_piechart.setDrawSliceText(false);
//                                final int[] MY_COLORS = {R.color.line_chart_ms_color,
//                                        R.color.piechart_yellow,
//                                        R.color.light_blue_button,
//                                        R.color.darapay_blue,
//                                        R.color.darapay_orange,
//                                        R.color.piechart_pink};
                            final int[] MY_COLORS = {Color.rgb(90, 186, 152),
                                    Color.rgb(16, 156, 144),
                                    Color.rgb(215, 176, 72),
                                    Color.rgb(31, 64, 120),
                                    Color.rgb(183, 108, 41),
                                    Color.rgb(236, 28, 35)};
                            ArrayList<Integer> colors = new ArrayList<Integer>();

                            for (int c : MY_COLORS) colors.add(c);
                            ro_piechart.getLegend().setEnabled(true);

                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLACK);
                            pieDataSet.setValueTextSize(10f);
                            ro_piechart.getDescription().setEnabled(false);
                            LegendEntry[] legendEntries=new LegendEntry[pieEntries.size()];
                            for(int i=0;i<legendEntries.length;i++)
                            {
                                LegendEntry entry=new LegendEntry();
                                entry.formColor=MY_COLORS[i];
                                try {
                                    entry.label=listKeys.get(i)+ " = "+listValues.get(i);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                legendEntries[i]=(entry);

                            }

                            ro_piechart.getLegend().setCustom(legendEntries);
                            ro_piechart.getLegend().setTextColor(Color.BLACK);
                            ro_piechart.setExtraBottomOffset(40f);
//                                ro_piechart.setExtraTopOffset(20f);
                            ro_piechart.getLegend().setWordWrapEnabled(true);
                            ro_piechart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
                            ro_piechart.getLegend().setEnabled(true);
                            ro_piechart.getLegend().setTextColor(getContext().getResources().getColor(R.color.black));
                            ro_piechart.getLegend().setTextSize(10f);
                            ro_piechart.getLegend().setYOffset(10f);
                            ro_piechart.getLegend().setXOffset(10f);
                            ro_piechart.getLegend().getHorizontalAlignment();
                            ro_piechart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
                            ro_piechart.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                            ro_piechart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                            ro_piechart.getLegend().setWordWrapEnabled(true);
                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLACK);
                            pieDataSet.setValueTextSize(10f);
                            ro_piechart.getDescription().setEnabled(false);
                            ro_piechart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                                @Override
                                public void onValueSelected(Entry e, Highlight h) {
                                    PieEntry pe = (PieEntry) e;
                                    String str= pe.getLabel();
                                    str = str.replaceAll("[^a-zA-Z0-9]", " ");
                                    ro_piechart.setCenterText( str+ "\n" + ConstantDeclaration.amountFormatter_pie(Double.valueOf(e.getY())));
                                    ro_piechart.setCenterTextColor(getContext().getResources().getColor(R.color.black));


                                }

                                @Override
                                public void onNothingSelected() {
                                    ro_piechart.setCenterText("");

                                }
                            });
                            ro_piechart.setCenterText(jsonArray.getString(0)+"\n"+valuesArray.getInt(0));
                            ro_piechart.highlightValue(0, 0, false);
                            ro_piechart.invalidate();
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
//                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
//                                            startActivity(intent);
//                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    alertDialogInterface, "", error, getContext().getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }

    }
    private void getroEntries(){
        pieEntries = new ArrayList<>();
        for(int i =0;i <roModels.size();i++){
            pieEntries.add(new PieEntry(Float.parseFloat(roModels.get(i).getRovalue()), roModels.get(i).getCategory()));
//            Log.e("roData",roData.get(i));

        }
    }
    //RO Connectivity Status End

    //Nano Status Start
    public void getNanoChart() {
        JSONObject json = new JSONObject();

        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{

                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }
                        else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new Asyncnano_exception().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }
    private  class Asyncnano_exception extends AsyncTask<JSONObject, Void, String> {

        String strTimeStamp = "";

        public Asyncnano_exception() {
        }

        public Asyncnano_exception(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getContext());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Status/NanoStatus", false, jsonObjects[0], "");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
                if (!isAdded()) {
                    return;
                }
                if (isDetached()) {
                    return;
                }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if (respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject data = jsonObject.getJSONObject("Data");
                            JSONArray valuesArray = data.getJSONArray("PTCount");
                            JSONArray ptArray = data.getJSONArray("PTStatus");
                            JSONArray statusArray = data.getJSONArray("NanoStatus");

                            for (int ts =0;ts<statusArray.length();ts++){
//                                JSONObject timeObj = statusArray.getJSONObject(ts);
                                String nanoStatus = statusArray.getString(ts);
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
//                                String fromDate = timeObj.getString("FromDate");
                                dateObj = curFormater.parse(nanoStatus);
                                Log.e("dta",nanoStatus);
                                SimpleDateFormat postFormater = new SimpleDateFormat("DD |MM | YYYY");

                                String nFDate = postFormater.format(dateObj);
//                                nano_status_date.setText(nFDate);
                                nano_status_date.setText(nanoStatus.replace("-"," |"));
                            }
                            listKeys = new ArrayList<String>();
                            List<Double> listValues1 = new ArrayList<Double>();
                            for (int i1 = 0; i1 < ptArray.length(); i1++) {
                                listKeys.add(ptArray.getString(i1));
                            }
                            for (int value = 0; value < valuesArray.length(); value++) {
                                listValues1.add(Double.valueOf((valuesArray.getString(value))));
                            }

                            nanoStatusArrayList = new ArrayList<>();
                            nanodata = new ArrayList<String>();
                            for (int i1 = 0; i1 < valuesArray.length(); i1++) {
                                NanoStatus nanoStatus = new NanoStatus();
                                nanoStatus.setPtValue(Float.parseFloat(valuesArray.getString(i1)));
                                nanoStatus.setPtStatus(ptArray.getString(i1));
                                nanoStatusArrayList.add(nanoStatus);
//                                nanodata.add(valuesArray.getString(i1));
                                Log.e("nanoData",valuesArray.getString(i1));
                            }
                            getnanoEntries();
                            pieDataSet = new PieDataSet(pieEntries, "");
                            pieData = new PieData(pieDataSet);
                            nano_piechart.setData(pieData);
                            nano_piechart.getData().setDrawValues(false);
                            nano_piechart.setDrawEntryLabels(false);
                            nano_piechart.setDrawSliceText(false);
                            final int[] MY_COLORS = {Color.rgb(0, 0, 254), Color.rgb(0, 128, 1), Color.rgb(249, 40, 39),
                                    Color.rgb(5, 1, 3)};
                            ArrayList<Integer> colors = new ArrayList<Integer>();

                            for (int c : MY_COLORS) colors.add(c);
                            nano_piechart.getLegend().setEnabled(false);
                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLUE);
                            pieDataSet.setValueTextSize(10f);
                            pieDataSet.setValueFormatter(new PercentFormatter());
                            nano_piechart.getDescription().setEnabled(false);
                            LegendEntry[] legendEntries=new LegendEntry[pieEntries.size()];
                            for(int i=0;i<legendEntries.length;i++)
                            {
                                LegendEntry entry=new LegendEntry();
                                entry.formColor=MY_COLORS[i];
                                try {
                                    entry.label=listKeys.get(i)+ " = "+listValues1.get(i);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                legendEntries[i]=(entry);

                            }
                            nano_piechart.getLegend().setCustom(legendEntries);
                            nano_piechart.getLegend().setTextColor(Color.BLACK);
                            nano_piechart.setExtraBottomOffset(10f);
                            nano_piechart.getLegend().setWordWrapEnabled(true);
                            nano_piechart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
                            nano_piechart.getLegend().setEnabled(true);
                            nano_piechart.getLegend().setTextColor(getContext().getResources().getColor(R.color.black));
                            nano_piechart.getLegend().setTextSize(8f);
                            nano_piechart.getLegend().setYOffset(8f);
                            nano_piechart.getLegend().setXOffset(8f);
                            nano_piechart.getLegend().getHorizontalAlignment();
                            nano_piechart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
                            nano_piechart.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                            nano_piechart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                            nano_piechart.getLegend().setWordWrapEnabled(true);
                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLACK);
                            pieDataSet.setValueTextSize(8f);
                            nano_piechart.getDescription().setEnabled(false);

                            nano_piechart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                                @Override
                                public void onValueSelected(Entry e, Highlight h) {
                                    PieEntry pe = (PieEntry) e;
                                    String str= pe.getLabel();
                                    str = str.replaceAll("[^a-zA-Z0-9]", " ");
                                    nano_piechart.setCenterText(str + "\n" + e.getY());
                                    nano_piechart.setCenterTextColor(getContext().getResources().getColor(R.color.black));


                                }

                                @Override
                                public void onNothingSelected() {
                                    nano_piechart.setCenterText("");

                                }
                            });
                            nano_piechart.setCenterText(ptArray.getString(0)+"\n"+valuesArray.getDouble(0));
                            nano_piechart.invalidate();
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
//                                                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                                                startActivity(intent);
//                                                getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    alertDialogInterface, "", error, getContext().getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }

    }
    private void getnanoEntries(){
        pieEntries = new ArrayList<>();
        for(int i =0;i <nanoStatusArrayList.size();i++){
            pieEntries.add(new PieEntry(nanoStatusArrayList.get(i).getPtValue(),nanoStatusArrayList.get(i).getPtStatus()));
//            Log.e("roData",nanodata.get(i));

        }
    }
    //Nano Sales End

    private void getPieChartData() {

        JSONObject json = new JSONObject();
        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{

                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }
                        else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new Asyncprice_exception().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class Asyncprice_exception extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public Asyncprice_exception() {
        }

        public Asyncprice_exception(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getContext());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Status/PriceException", false, jsonObjects[0], "");
        }


        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
                if (!isAdded()) {
                    return;
                }
                if (isDetached()) {
                    return;
                }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject data = jsonObject.getJSONObject("Data");
                            JSONArray valuesArray = data.getJSONArray("Value");
                            JSONArray jsonArray = data.getJSONArray("Category");
                            JSONArray array = data.getJSONArray("HPEffectiveDate");

                            for (int ts =0;ts<array.length();ts++){
//                                JSONObject timeObj = statusArray.getJSONObject(ts);
                                String nanoStatus = array.getString(ts);
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
//                                String fromDate = timeObj.getString("FromDate");
                                dateObj = curFormater.parse(nanoStatus);
                                Log.e("dta",nanoStatus);
                                SimpleDateFormat postFormater = new SimpleDateFormat("DD |MM | YYYY");

                                String nFDate = postFormater.format(dateObj);
                                tv_dt_price.setText(nanoStatus.replace("-"," |"));
                            }
//                            JSONArray arrayPrice_Exception = new JSONArray(jsonObject.getString("Data"));
//                            listData = new ArrayList<String>();
                            combinedModels = new ArrayList<>();
                            for (int i1 = 0; i1 < valuesArray.length(); i1++) {
                                CombinedModel model = new CombinedModel();
                                model.setCategoryName(jsonArray.getString(i1));
                                model.setValue(valuesArray.getString(i1));
                                combinedModels.add(model);
//                                listData.add(valuesArray.getString(i1));
                            }
                            getEntries();
                            pieDataSet = new PieDataSet(pieEntries, "");
                            pieData = new PieData(pieDataSet);
                            pieChart.setData(pieData);
                            pieChart.getData().setDrawValues(false);
                            pieChart.setDrawEntryLabels(false);
                            pieChart.setDrawSliceText(false);

                            listKeys = new ArrayList<String>();
                            listValues = new ArrayList<Integer>();
                            for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                                listKeys.add(jsonArray.getString(i1));
                            }
                            for (int value = 0; value < valuesArray.length(); value++) {
                                listValues.add((int) Float.parseFloat(valuesArray.getString(value)));
                            }

                            final int[] MY_COLORS = {Color.rgb(255, 124, 16), Color.rgb(52, 91, 149), Color.rgb(43, 51, 50),
                                    Color.rgb(249, 82, 147)};
                            ArrayList<Integer> colors = new ArrayList<Integer>();

                            for (int c : MY_COLORS) colors.add(c);
                            pieChart.getLegend().setEnabled(false);
                            pieDataSet.setColors(ColorTemplate.createColors(colorcode));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLACK);
                            pieDataSet.setValueTextSize(10f);
                            pieChart.getDescription().setEnabled(false);

                            LegendEntry[] legendEntries=new LegendEntry[pieEntries.size()];
                            for(int i=0;i<legendEntries.length;i++)
                            {
                                LegendEntry entry=new LegendEntry();
                                entry.formColor=MY_COLORS[i];
                                try {
                                    entry.label=listKeys.get(i)+ " = "+listValues.get(i);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                legendEntries[i]=(entry);

                            }
                            pieChart.getLegend().setCustom(legendEntries);
                            pieChart.getLegend().setTextColor(Color.BLACK);
                            pieChart.setExtraBottomOffset(30f);
                            pieChart.getLegend().setWordWrapEnabled(true);
                            pieChart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
                            pieChart.getLegend().setEnabled(true);
                            pieChart.getLegend().setTextColor(getContext().getResources().getColor(R.color.black));
                            pieChart.getLegend().setTextSize(8f);
                            pieChart.getLegend().setYOffset(-8f);
                            pieChart.getLegend().setXOffset(8f);
                            pieChart.getLegend().getHorizontalAlignment();
                            pieChart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
                            pieChart.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                            pieChart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                            pieChart.getLegend().setWordWrapEnabled(true);
                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLACK);
                            pieDataSet.setValueTextSize(8f);
                            pieChart.getDescription().setEnabled(false);


                            pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                                @Override
                                public void onValueSelected(Entry e, Highlight h) {
                                    PieEntry pe = (PieEntry) e;
                                    String str= pe.getLabel();
                                    str = str.replaceAll("[^a-zA-Z0-9]", " ");
                                    pieChart.setCenterText(str+"\n"+ConstantDeclaration.amountFormatter_pie(Double.valueOf(e.getY())));
                                    pieChart.setCenterTextColor(getContext().getResources().getColor(R.color.black));


                                }

                                @Override
                                public void onNothingSelected() {
                                    pieChart.setCenterText("");

                                }
                            });
                            pieChart.setCenterText(jsonArray.getString(0)+"\n"+valuesArray.getInt(0));

                            pieChart.invalidate();



                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
//                                                Intent intent = new Intent(getContext(), LoginActivity.class);
//                                                startActivity(intent);
//                                                getContext().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    alertDialogInterface, "", error, getContext().getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
    private void getEntries() {
        pieEntries = new ArrayList<>();
        for(int i =0;i <combinedModels.size();i++){
            pieEntries.add(new PieEntry(Float.parseFloat(combinedModels.get(i).getValue()),combinedModels.get(i).getCategoryName()));

        }
    }
    private void callCriticalStockSales() {

        JSONObject json = new JSONObject();
        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{

                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }
                        else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        new CriticalStockData().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class CriticalStockData extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public CriticalStockData() {
        }


        public CriticalStockData(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getContext());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("DashBoard/CriticalStock", false, jsonObjects[0], "");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
                if (!isAdded()) {
                    return;
                }
                if (isDetached()) {
                    return;
                }

            if (s != null) {

                try {

                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray dataArray = jsonObject.getJSONArray("Data");
                            stockArrayList = new ArrayList<>();
                            JSONObject dataObject = null;
                            for (int i =0; i<dataArray.length(); i++){
                                dataObject = dataArray.getJSONObject(i);
                                CriticalStock stock = new CriticalStock();
                                stock.setProduct(dataObject.getString("Product"));
                                stock.setStock(dataObject.getInt("stock"));
                                stockArrayList.add(stock);
                            }
                            tv_cr_date.setText("Date : "+dataObject.getString("STKDateTime"));
                            BarData data = createStockChart();
                            configureCriticalChartAppearance();
                            prepareCriticalChartData(data);
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
//                                                Intent intent = new Intent(getActivity(), LoginActivity.class);
//                                                startActivity(intent);
//                                                getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getContext(),
                                    alertDialogInterface, "", error, getContext().getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }



        }


    }

    private BarData createStockChart() {
        ArrayList<BarEntry> values1 = new ArrayList<>();
        ArrayList<BarEntry> values2 = new ArrayList<>();
        BarDataSet set1,set2;
        String color="";
        for (int i = 0; i < stockArrayList.size() ; i++) {
//            values1.add(new BarEntry(i, Float.valueOf(todaycategorydata.get(i))));
            values2.add(new BarEntry(i,stockArrayList.get(i).getStock()));

//            if(i== 4){
//              co
//
//              lor = "disabled";
//            }
        }
        set1  = new BarDataSet(values1, "Product");
        set2 = new BarDataSet(values2, "Value");
//        set1.getEntryForXValue(0,0)
        ArrayList<Integer> colorList = new ArrayList<>();
        for(int i =0; i<colorcode.length; i++){
            colorList.add(colorcode.length);
        }
//        set1.setValueTextColors(colorList);
//        set1.contains(values1.get(4));
        set1.setColor(getContext().getResources().getColor(R.color.hsdcolor));
//        set2.setColor(getResources().getColor(R.color.orange_logo));
        /*for(int i = 0; i < todayvalueData.size() ; i++){


        }*/
//        set1.setValueTextColor(getResources().getColor(R.color.white));
        set2.setValueTextColor(getContext().getResources().getColor(R.color.black));
        set1.setColors(new int[]{Color.rgb(0, 0, 254),
                Color.rgb(0, 166, 81),
                Color.rgb(236, 28, 35),
                Color.rgb(5, 1, 3),
                Color.rgb(201, 108, 56)});

        set2.setColors(new int[]{Color.rgb(0, 0, 254),
                Color.rgb(0, 166, 81),
                Color.rgb(236, 28, 35),
                Color.rgb(5, 1, 3),
                Color.rgb(201, 108, 56)});
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        dataSets.add(set2);
        BarData data = new BarData(dataSets);
        return data;
    }

    private void prepareCriticalChartData(BarData data) {

        critical_barChart.setData(data);
        critical_barChart.getBarData().setBarWidth(BAR_WIDTH);
//        critical_barChart.getBarData().setBarWidth(0.8f);

        float groupSpace = 1f - ((BAR_SPACE + BAR_WIDTH) * GROUPS);
//        critical_barChart.groupBars(-1, groupSpace, BAR_SPACE);

//        tv_Xaxis.setText("X-Axis = "+ "Fuel Type");
//        tv_Yaxis.setText("Y-Axis = "+ "Quantity (Ltrs)");


        critical_barChart.invalidate();
    }

    private void configureCriticalChartAppearance() {
        critical_barChart.setPinchZoom(false);
        critical_barChart.setDrawBarShadow(false);
        critical_barChart.setDrawGridBackground(false);
        critical_barChart.getDescription().setEnabled(false);
        critical_barChart.getLegend().setEnabled(false);
        critical_barChart.getXAxis().setDrawGridLines(false);
        critical_barChart.getAxisLeft().setDrawGridLines(false);
        critical_barChart.getAxisRight().setDrawGridLines(false);
//        critical_barChart.getXAxis().setAxisMinValue(10f);
        critical_barChart.setFitBars(true);


        List<String> xarray = new ArrayList<>();
        for (int i =0; i< stockArrayList.size();i++){
            xarray.add(stockArrayList.get(i).getProduct());
        }
        XAxis xAxis = critical_barChart.getXAxis();
        xAxis.setGranularity(1f);
//        xAxis.setCenterAxisLabels(true);
//        xAxis.setLabelRotationAngle(-90);
//        xAxis.setLabelRotationAngle(20f);
        xAxis.setAvoidFirstLastClipping(false);
        xAxis.setCenterAxisLabels(false);
        xAxis.setTextColor(getContext().getResources().getColor(R.color.black));

        xVal.add(String.valueOf(xarray));
//        xAxis.setTextSize(8f);
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(xVal));

        final YAxis leftAxis;
        try {
            leftAxis = yesterdaySaleChart.getAxisLeft();
            leftAxis.setDrawGridLines(false);
            leftAxis.setSpaceTop(35f);
            leftAxis.setAxisMinimum(0f);
        } catch (Exception e) {
            e.printStackTrace();
        }

//        leftAxis.setTextColor(getResources().getColor(R.color.button_light_blue));


        critical_barChart.getAxisRight().setEnabled(false);

//        critical_barChart.getXAxis().setAxisMinimum(0f);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        critical_barChart.getLegend().setTextColor(getContext().getResources().getColor(R.color.black));
//        xAxis.setLabelCount(6);
//        mChart.getXAxis().setCenterAxisLabels(false);
        int monthsize = xarray.size();
        float f2 = (float) monthsize / 1;
        xAxis.setAxisMaximum(f2);


        List<String> catData = new ArrayList<>();
        for (int j =0;j<stockArrayList.size();j++){
            try {
                catData.add(stockArrayList.get(j).getProduct());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        try {
//            barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(todayvalueData));
            critical_barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(catData));
            if(critical_barChart.getXAxis().getLabelCount() == 6||
                    critical_barChart.getXAxis().getLabelCount() == 7||
                    critical_barChart.getXAxis().getLabelCount() == 8){
                critical_barChart.getXAxis().setTextSize(1+ critical_barChart.getXAxis().getLabelCount());
            }
//            xAxis.setTextSize(1 + xAxis.getLabelCount());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package com.agstransact.HP_SC.dashboard.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;

import org.json.JSONArray;

import java.util.ArrayList;

public class Dialog_search_adapter extends RecyclerView.Adapter<Dialog_search_adapter.ViewHolder> {
    private Context context;

    JSONArray branchlists  ;
    JSONArray mfilteredList;
    JSONArray filteredList;
    private ItemClickListener mClickListener;
    ItemClickListener itemClickListener;
    ArrayList<String> ZoneList;
    public Dialog_search_adapter(JSONArray branchlists, Context context, ItemClickListener itemClickListener){
        super();
        this.branchlists = branchlists;
        this.mfilteredList = branchlists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }
    public Dialog_search_adapter(ArrayList<String> ZoneList, Context context, ItemClickListener itemClickListener){
        super();
        this.ZoneList = ZoneList;
        this.mfilteredList = branchlists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.branch_search_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        try {
            holder.tv_branch_name.setText(ZoneList.get(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return ZoneList.size();
    }


//    @Override
//    public  Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//                try {
//                    if (charString.isEmpty()) {
//                        try {
//                            mfilteredList = branchlists;
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//
//                           mfilteredList = branchlists;
//                          filteredList= new JSONArray();
//                        for(int i = 0; i < mfilteredList.length(); i++){
//                            // name match condition. this might differ depending on your requirement
//                            // here we are looking for name or phone number match
//                            if (mfilteredList.getJSONObject(i).getString("BranchName")
//                                    .toLowerCase().contains(charString.toLowerCase())) {
//                                filteredList.put(mfilteredList.getJSONObject(i));
//                            }
//                        }
//                        mfilteredList =  filteredList;
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = mfilteredList;
//                return filterResults;
////        return null;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//
//                mfilteredList = (JSONArray) filterResults.values;
//                        notifyDataSetChanged();
//
//            }
//        };
//    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_branch_name;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_branch_name = (TextView) itemView.findViewById(R.id.tv_branch_name);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View view) {
            try {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(ZoneList.get(getAdapterPosition()));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public interface ItemClickListener {
        void onItemClick(String selectedBranch);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}


package com.agstransact.HP_SC.dashboard.fragment;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentFilter extends Fragment {

    private View rootView;
    private Spinner spinner_select_zone,spinner_select_region,spinner_select_sales_area,spinner_select_ro;
    private TextView submitButton;
    String error="", respCode="";
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    Bundle bundle;
    ArrayList<String> ZoneList,ROCODE_List;
    JSONArray Regionarray,ZoneArray,ROArray,SalesAreaArray;
    ArrayList<String> RegionKey,RegionValue,SalesAreaKey,SalesAreaValue,ROKey,ROValue;
    String SelectedZone= "", SelectedRegion= "", SelectedSalesArea= "", SelectedRO= "";
    Boolean ZoneClicked= false,Regionclicked =false,SalesAreaClicked =false,ROClicked = false;
    String filteredRO;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_filter,container,false);

        setupViews();
        return rootView;
    }

    private void setupViews() {
        spinner_select_zone = rootView.findViewById(R.id.spinner_select_zone);
        spinner_select_region = rootView.findViewById(R.id.spinner_select_region);
        spinner_select_sales_area = rootView.findViewById(R.id.spinner_select_sales_area);
        spinner_select_ro = rootView.findViewById(R.id.spinner_select_ro);
        submitButton = rootView.findViewById(R.id.submit);

        populateZoneList();

        if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLHQ")){
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "Selectdzone","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "Selectdzone","").equalsIgnoreCase("Select Zone"))){
                SelectedZone = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Selectdzone",""));
                CallRegion();

            }
            else{
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("Select Region"))){
                SelectedRegion = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdRegion",""));
                CallSales_Area();
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("Select Sales Area"))){
                SelectedSalesArea = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea",""));
                CallRO();
            }

            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    SelectedRO = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
//                submitBT.setEnabled(true);
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    SelectedRO = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
//                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
//                submitBT.setEnabled(true);

            }

        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLZONE")) {
            spinner_select_region.setEnabled(true);
            spinner_select_zone.setVisibility(View.GONE);
            if (!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion", "").equalsIgnoreCase("Select Region"))) {
                SelectedRegion = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdRegion", ""));
                CallSales_Area();

            } else {
            }
            if (!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea", "").equalsIgnoreCase("Select Sales Area"))) {
                SelectedSalesArea = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea", ""));
                CallRO();
            }
            if (!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO", "").equalsIgnoreCase("Select RO"))) {
                try {
                    SelectedRO = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName", ""));
                    try {
                        if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName", "").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        } else {
                            filteredRO = SelectedRO.substring(0, 8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO", "").equalsIgnoreCase("ALL")) {
                try {
                    SelectedRO = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName", ""));
                    try {
                        if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName", "").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        } else {
                            filteredRO = SelectedRO.substring(0, 8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLREGION")){
            spinner_select_region.setEnabled(true);
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("Select Sales Area"))){
                SelectedSalesArea = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea",""));
                SelectedSalesArea = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea","");
                CallRO();
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    SelectedRO = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
//                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    SelectedRO = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
//                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
//                submitBT.setEnabled(true);

            }

        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLSALESAREA")){
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    SelectedRO = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
//                submitBT.setEnabled(true);
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    SelectedRO = (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
//                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitButton.setEnabled(true);

            }
        }

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                try {
//                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                Bundle txnBundle = new Bundle();
                if(!SelectedZone.equalsIgnoreCase("Select Zone")){

                }else if(!SelectedRegion.equalsIgnoreCase("Select Region")){

                }else if(!SelectedSalesArea.equalsIgnoreCase("Select Sales Area")){

                }else if(!SelectedRO.equalsIgnoreCase("Select RO")){

                }
                HomeFragment_old filter_screen = new HomeFragment_old();
                filter_screen.setArguments(txnBundle);
                changeFrag.addFragment(filter_screen);


            }
        });

    }

    private void populateZoneList() {

        ZoneList = new ArrayList<>();
        ROCODE_List = new ArrayList<>();
        if(UserDataPrefrence.getArrayList( "ROLIst",getContext()).size() != 1){
            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLHQ")){
                ZoneList.add("Select Zone");
            }else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLZONE")){
                ZoneList.add("Select REGION");
            } else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLREGION")){
                ZoneList.add("Select Sales Area");
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLSALESAREA")){
                ZoneList.add("Select RO");
            }
            ZoneList.add(("ALL"));
            ZoneList.addAll(UserDataPrefrence.getArrayList( "ROCode",getContext()));
            ROCODE_List.addAll(UserDataPrefrence.getArrayList( "ROLIst",getContext()));

        } else if(UserDataPrefrence.getArrayList( "ROLIst",getContext()).size() == 0){
        } else{
            ZoneList.addAll(UserDataPrefrence.getArrayList( "ROLIst",getContext()));
        }

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, ZoneList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_select_zone.setAdapter(adapter);
        spinner_select_zone.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (ZoneList.get(spinner_select_zone.getSelectedItemPosition()) != null) {
                    SelectedZone = ZoneList.get(spinner_select_zone.getSelectedItemPosition());
                    if (!(SelectedZone.equalsIgnoreCase("All"))){
                        CallRegion();
                    }
                    /*if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Selectdzone","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Selectdzone","").equalsIgnoreCase("Select Zone"))){
                        SelectedZone = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","");
                        CallRegion();
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
//                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitButton.setEnabled(true);
                        }else{
//                            submitButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitButton.setEnabled(false);
                        }
                    }
                    else{
//                        submitButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitButton.setEnabled(false);
                    }*/
                    Log.e("zonelist",String.valueOf(ZoneList));

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }

        });
    }

    private void CallRegion() {

        try {
            JSONObject json = new JSONObject();
            if(!SelectedZone.equalsIgnoreCase("Select Zone")){
                json.put("Para1",SelectedZone);
            }
            new AsyncRegion().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncRegion extends AsyncTask<JSONObject, Void, String>{
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncRegion() {
        }

        public AsyncRegion(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetRegionDetail", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            Regionarray = new JSONArray(jsonObject.getString("Data"));
                            RegionKey = new ArrayList<String>();
                            RegionValue = new ArrayList<String>();
                            RegionKey.add("ALL");
                            for (int i1=0; i1<Regionarray.length(); i1++) {
                                RegionKey.add(Regionarray.getJSONObject(i1).getString("Key"));
                            }
                            RegionValue.add("ALL");
                            for (int i1=0; i1<Regionarray.length(); i1++) {
                                RegionValue.add(Regionarray.getJSONObject(i1).getString("Value"));

                            }
                            poulateRegionDetails();
                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void poulateRegionDetails() {


        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, RegionValue);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_select_region.setAdapter(adapter);

        spinner_select_region.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SelectedRegion = RegionValue.get(spinner_select_zone.getSelectedItemPosition());
                if ((!SelectedRegion.equalsIgnoreCase("ALL"))){
                    CallSales_Area();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*spinner_select_region.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SelectedRegion = RegionValue.get(spinner_select_zone.getSelectedItemPosition());
                if ((!SelectedRegion.equalsIgnoreCase("ALL"))){
                    CallSales_Area();
                }
                *//*if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdRegion","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdRegion","").equalsIgnoreCase("Select Region"))){
                    SelectedRegion = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "SelectdRegion","");
                    CallSales_Area();
                    if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
//                        submitButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                        submitButton.setEnabled(true);
                    }else{
//                        submitButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitButton.setEnabled(false);
                    }
                }else{
//                    submitButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitButton.setEnabled(false);
                }*//*

            }
        });*/

    }

    private void CallSales_Area(){
        try {
            JSONObject json = new JSONObject();
            if(!SelectedRegion.equalsIgnoreCase("Select Region")){
                json.put("Para1",SelectedRegion );
            }
            new AsyncsalesArea().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncsalesArea extends AsyncTask<JSONObject, Void, String>{
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncsalesArea() {
        }

        public AsyncsalesArea(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetSalesArea", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            SalesAreaArray = new JSONArray(jsonObject.getString("Data"));
                            SalesAreaKey = new ArrayList<String>();
                            SalesAreaValue = new ArrayList<String>();
                            SalesAreaKey.add("ALL");
                            for (int i1=0; i1<SalesAreaArray.length(); i1++) {
                                SalesAreaKey.add(SalesAreaArray.getJSONObject(i1).getString("Key"));
                            }
                            SalesAreaValue.add("ALL");
                            for (int i1=0; i1<SalesAreaArray.length(); i1++) {
                                SalesAreaValue.add(SalesAreaArray.getJSONObject(i1).getString("Value"));
                            }
                            populateSalesAreaList();
//populate dialog with list
                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void populateSalesAreaList() {

        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, SalesAreaValue);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_select_sales_area.setAdapter(adapter);

        spinner_select_sales_area.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SelectedSalesArea = SalesAreaValue.get(spinner_select_sales_area.getSelectedItemPosition());
                if (!SelectedSalesArea.equalsIgnoreCase("ALL")){
                    CallRO();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        /*spinner_select_sales_area.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SelectedSalesArea = RegionValue.get(spinner_select_sales_area.getSelectedItemPosition());
                if (!SelectedSalesArea.equalsIgnoreCase("ALL")){
                    CallRO();
                }
                *//*if (SalesAreaValue.get(spinner_select_sales_area.getSelectedItemPosition()) != null) {
                    SelectedSalesArea = SalesAreaValue.get(spinner_select_sales_area.getSelectedItemPosition());
                    if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "SelectdRegion","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "SelectdRegion","").equalsIgnoreCase("Select Region"))){
                        SelectedZone = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","");
                        CallRO();
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
//                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitButton.setEnabled(true);
                        }else{
//                            submitButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitButton.setEnabled(false);
                        }
                    }
                    else{
//                        submitButton.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitButton.setEnabled(false);
                    }
                    Log.e("zonelist",String.valueOf(ZoneList));

                }*//*
            }
        });*/

    }

    private void CallRO() {

        try {
            JSONObject json = new JSONObject();
            if(!SelectedSalesArea.equalsIgnoreCase("Select Sales Area")){
                json.put("Para1",SelectedSalesArea );
                json.put("Para2","10" );
            }

            new AsyncRO().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncRO extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncRO() {
        }

        public AsyncRO(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetROList", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            ROArray = new JSONArray(jsonObject.getString("Data"));
                            ROKey = new ArrayList<String>();
                            ROValue = new ArrayList<String>();
                            ROKey.add("ALL");
                            for (int i1=0; i1<ROArray.length(); i1++) {
                                ROKey.add(ROArray.getJSONObject(i1).getString("Key"));
                            }
                            ROValue.add("ALL");
                            for (int i1=0; i1<ROArray.length(); i1++) {
                                ROValue.add(ROArray.getJSONObject(i1).getString("Value"));
                            }
//                 populate with list
                            populateROList();
                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void populateROList() {


        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, ROValue);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_select_ro.setAdapter(adapter);

        spinner_select_ro.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SelectedRO = ROValue.get(spinner_select_ro.getSelectedItemPosition());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
       /* spinner_select_ro.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SelectedRO = ROValue.get(spinner_select_ro.getSelectedItemPosition());
                if (SelectedRO.equalsIgnoreCase("ALL")){

                }
            }
        });*/

    }
}

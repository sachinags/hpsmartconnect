package com.agstransact.HP_SC.dashboard;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.adapter.Current_Price_Adapter;
import com.agstransact.HP_SC.dashboard.adapter.MyListData;
import com.agstransact.HP_SC.model.Current_Price_Model;
import com.agstransact.HP_SC.model.legends_model;
import com.agstransact.HP_SC.utils.RecyclerItemClickListner;

import java.util.ArrayList;

public class legend_adapter extends RecyclerView.Adapter<legend_adapter.legend_model>{
    private Context context;
    private ArrayList<Integer> models = new ArrayList<>();
    private ArrayList<String> model_keys = new ArrayList<>();
    private ArrayList<Double> ns_values = new ArrayList<>();
    String pie_name;
//    private ArrayList<legends_model> models = new ArrayList<>();
//    private ArrayList<legends_model> models = new ArrayList<>();
    public legend_adapter(Activity activity, ArrayList<Integer> models,ArrayList<String> model_keys,
                          String pie_name,ArrayList<Double> ns_values) {
        this.context = activity;
        this.models = models;
        this.model_keys = model_keys;
        this.pie_name =pie_name;
        this.ns_values =ns_values;
    }
    @NonNull
    @Override
    public legend_adapter.legend_model onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.legend_adapter,parent,false);
        return new legend_adapter.legend_model(view);
    }
    @Override
    public void onBindViewHolder(@NonNull legend_adapter.legend_model holder, int position) {

//        legends_model model = models.get(position);
//        holder.tv_legend_value.setText(model.getLegendName());
        int[] MY_COLORS = new int[0];
        if(pie_name.equalsIgnoreCase("RC_Status")){
             MY_COLORS = new int[]{Color.rgb(90, 186, 152),
                     Color.rgb(16, 156, 144),
                     Color.rgb(215, 176, 72),
                     Color.rgb(31, 64, 120),
                     Color.rgb(183, 108, 41),
                     Color.rgb(236, 28, 35)};
            holder.tv_legend_value.setText(model_keys.get(position).toString().replace("_"," ")+" = "+models.get(position));
        }else if(pie_name.equalsIgnoreCase("Nano_Status")){
            MY_COLORS = new int[]{Color.rgb(0, 0, 254), Color.rgb(0, 128, 1), Color.rgb(249, 40, 39),
                    Color.rgb(5, 1, 3)};
            holder.tv_legend_value.setText(model_keys.get(position).toString().replace("_"," ")+" = "+ns_values.get(position)+" %");
//            holder.tv_legend_value.setText(model_keys.get(position).toString().replace("_"," "));
        }else{
            MY_COLORS = new int[]{Color.rgb(255, 124, 16), Color.rgb(52, 91, 149), Color.rgb(43, 51, 50),
                    Color.rgb(249, 82, 147)};
            holder.tv_legend_value.setText(model_keys.get(position).toString().replace("_"," ")+" = "+models.get(position));
        }

        ArrayList<Integer> colors = new ArrayList<Integer>();
        for (int c : MY_COLORS) colors.add(c);
        holder.legend_color.setBackgroundColor(colors.get(position));

    }
    @Override
    public int getItemCount() {
        return model_keys.size();
    }

    public class legend_model extends RecyclerView.ViewHolder {

        private TextView tv_legend_value;
        ImageView legend_color;

        public legend_model(@NonNull View itemView) {
            super(itemView);

            tv_legend_value = itemView.findViewById(R.id.tv_legend_value);
            legend_color = itemView.findViewById(R.id.legend_color);
            tv_legend_value.setSelected(true);
        }
    }
}

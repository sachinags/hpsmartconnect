package com.agstransact.HP_SC.dashboard.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.fragment.fragment_filter_textview_multiple_new;
import com.agstransact.HP_SC.model.Model;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import static android.graphics.Color.*;

public class Dialog_search_adapter_multiple_new extends RecyclerView.Adapter<Dialog_search_adapter_multiple_new.ViewHolder> {
    private Context context;
    JSONArray branchlists  ;
    JSONArray mfilteredList;
    JSONArray filteredList;
    private ItemClickListener mClickListener;
    ItemClickListener itemClickListener;
    ArrayList<String> ZoneList;
   public static ArrayList<String> SelectedAllList = new ArrayList<String>();
   public static ArrayList<String> SelectedsingleList = new ArrayList<String>();
//    ArrayList<Model> ZoneList;
    private SparseBooleanArray storeChecked = new SparseBooleanArray();
    private boolean isMultiselect;
    private int itemSelected;
    private boolean [] checked;
    private String [] text;
    private boolean isChecked;
    private int lastSelectedPosition = -1;  // declare this variable
    boolean selected = false;
    boolean selected_all ;
    String user;


    public Dialog_search_adapter_multiple_new(JSONArray branchlists, Context context, ItemClickListener itemClickListener){
        super();
        this.branchlists = branchlists;
        this.mfilteredList = branchlists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }
    public Dialog_search_adapter_multiple_new(ArrayList<String> ZoneList, Context context, ItemClickListener itemClickListener){
        super();
        this.ZoneList = ZoneList;
        this.mfilteredList = branchlists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }
    public Dialog_search_adapter_multiple_new(ArrayList<String> ZoneList, Context context, ItemClickListener itemClickListener,String user){
        super();
        this.ZoneList = ZoneList;
        this.mfilteredList = branchlists;
        this.context = context;
        this.itemClickListener = itemClickListener;
        this.user = user;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.branch_search_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }
    public void toggleSelection(boolean isChecked) {
        if (ZoneList != null && ZoneList.size() > 0) {
            for (int i = 0; i < ZoneList.size(); i++) {
//                ZoneList.get(i).setSelected(isChecked);
                selected = isChecked;
                selected_all = isChecked;
            }
        }
        //don't forget to notify
//        SelectedList = new ArrayList<String>();
        if(isChecked) {
            for (int i = 0; i < SelectedAllList.size(); i++) {
                SelectedAllList.remove(SelectedAllList.get(i));
            }
//            for (int i = 0; i < ZoneList.size(); i++) {
//                SelectedAllList.add("ALL");
//            }
            SelectedAllList.add("ALL");
            fragment_filter_textview_multiple_new.stringselecteditem = "ALL";
//            fragment_filter_textview_multiple_new.SelecteditemsAll = SelectedAllList;
//            fragment_filter_textview_multiple_new.SelecteditemsAll = SelectedAllList;
        }else{
            for(int i = 0;i < SelectedAllList.size(); i++){
                SelectedAllList.remove(SelectedAllList.get(i));
            }
            fragment_filter_textview_multiple_new.SelecteditemsAll = SelectedAllList;
//            fragment_filter_textview_multiple_new.stringselecteditem = "ALL";
        }
        notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
//        holder.setIsRecyclable(false);
//        final Model model = ZoneList.get(position);
//        try {
//            holder.tv_branch_name.setText(ZoneList.get(position).getText());
////            holder.itemView.setBackgroundColor(storeChecked.get(position) ? context.getResources().getColor(R.color.white) :context.getResources().getColor(R.color.light_blue_button));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                // check whether you selected an item
////                if (holder.categoryIcon.isSelected) {
////                    selectedPos = position;
////                }
////
////                if (selectedPos == position) {
////                    notifyItemChanged(selectedPos);
////                    selectedPos = RecyclerView.NO_POSITION
////                } else {
////                    selectedPos = position
////                    notifyItemChanged(selectedPos)
////                }
////                if(lastSelectedPosition > 0) {
////                    mModelList.get(lastSelectedPosition).setSelected(false);
////                }
//                holder.itemView.setBackgroundColor(context.getResources().getColor(R.color.light_blue_button));
//                // store last selected item position
//
//                lastSelectedPosition = holder.getAdapterPosition();
//            }
//        });


        if(user.equalsIgnoreCase("Region")){
            holder.tv_branch_name.setText(ZoneList.get(position));
        }
        else{
            if (ZoneList != null) {
                //in some cases, it will prevent unwanted situations
                holder.recycler_checkbox.setOnCheckedChangeListener(null);
                holder.tv_branch_name.setText(ZoneList.get(position));
                holder.recycler_checkbox.setChecked(selected);
                holder.recycler_checkbox.setTag(holder.getAdapterPosition());
                Log.d("selected list3",fragment_filter_textview_multiple_new.SelecteditemsAll.toString());
//                if(selected_all){
//                    holder.recycler_checkbox.setChecked(selected);
//                }else{
//                    holder.recycler_checkbox.setChecked(selected);
//                }
//
                if(selected_all){
//                    holder.recycler_checkbox.setChecked(selected);
                    if(ZoneList.get(position).equalsIgnoreCase("ALL")){
                        holder.recycler_checkbox.setEnabled(true);
                        holder.recycler_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                            @Override
                            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                if(ZoneList.get(position).equalsIgnoreCase("ALL")){
                                    toggleSelection(isChecked);
                                }else{
                                    if (isChecked) {
                                        selected = true;
                                        try {
                                            if (!SelectedsingleList.contains(ZoneList.get(position))){
                                                SelectedsingleList.add(ZoneList.get(position));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            SelectedsingleList.add(ZoneList.get(position));
                                        }
//                            }


                                    }
                                    else {
//                        studentList.get(position).setSelected(false);
//                            if (fragment_filter_textview_multiple_new.Selecteditems.contains(ZoneList.get(position))){
//                                fragment_filter_textview_multiple_new.Selecteditems.remove(ZoneList.get(position));
//                            }
                                        try {
                                            if (SelectedsingleList.contains(ZoneList.get(position))){
                                                SelectedsingleList.remove(ZoneList.get(position));
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();
                                            SelectedsingleList.remove(ZoneList.get(position));
                                        }
                                        selected = false;
//                                        Toast.makeText(context,
//                                                "Unchecked : " + ZoneList.get(position)
//                                                , Toast.LENGTH_LONG).show();

                                    }
                                }

                            }
                        });
                    }else{
                        holder.recycler_checkbox.setEnabled(false);
                    }
                }
                else{
//                    holder.recycler_checkbox.setChecked(selected);
//                    if(fragment_filter_textview_multiple_new.checkscroll){
//
//                    }else{
//                        if(selected_all){
//
//                        }else{
////                        holder.setIsRecyclable(false);
//                            holder.recycler_checkbox.setChecked(false);
//
//                        }
//                    }

                    holder.recycler_checkbox.setEnabled(true);
                    holder.recycler_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            if(ZoneList.get(position).equalsIgnoreCase("ALL")){
                                toggleSelection(isChecked);
                            }else{
//                                holder.recycler_checkbox.setOnCheckedChangeListener(null);
//                                holder.recycler_checkbox.setChecked(isChecked);
//                                notifyItemChanged(position);
                                if (isChecked) {
                                    selected = true;
                                    try {
                                        if (!SelectedsingleList.contains(ZoneList.get(position))){
                                            SelectedsingleList.add(ZoneList.get(position));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        SelectedsingleList.add(ZoneList.get(position));
                                    }
                                    fragment_filter_textview_multiple_new.stringselecteditem = "Single";
                                }
                                else {
                                    try {
                                        if (SelectedsingleList.contains(ZoneList.get(position))){
                                            SelectedsingleList.remove(ZoneList.get(position));
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        SelectedsingleList.remove(ZoneList.get(position));
                                    }
                                    selected = false;
                                    fragment_filter_textview_multiple_new.stringselecteditem = "Single";
                                }
                            }
                        }
                    });
                }
            }
        }
//        super.onViewRecycled(holder);

    }

    @Override
    public int getItemCount() {
        return ZoneList.size();
    }


//    @Override
//    public  Filter getFilter() {
//        return new Filter() {
//            @Override
//            protected FilterResults performFiltering(CharSequence charSequence) {
//                String charString = charSequence.toString();
//                try {
//                    if (charString.isEmpty()) {
//                        try {
//                            mfilteredList = branchlists;
//
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    } else {
//
//                           mfilteredList = branchlists;
//                          filteredList= new JSONArray();
//                        for(int i = 0; i < mfilteredList.length(); i++){
//                            // name match condition. this might differ depending on your requirement
//                            // here we are looking for name or phone number match
//                            if (mfilteredList.getJSONObject(i).getString("BranchName")
//                                    .toLowerCase().contains(charString.toLowerCase())) {
//                                filteredList.put(mfilteredList.getJSONObject(i));
//                            }
//                        }
//                        mfilteredList =  filteredList;
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                FilterResults filterResults = new FilterResults();
//                filterResults.values = mfilteredList;
//                return filterResults;
////        return null;
//            }
//
//            @Override
//            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
//
//                mfilteredList = (JSONArray) filterResults.values;
//                        notifyDataSetChanged();
//
//            }
//        };
//    }
//clear on actionmode close
public void exitMultiselectMode() {
    isMultiselect = false;
    itemSelected = 0;
    storeChecked.clear();
    notifyDataSetChanged();
}

    // get all selected position
    public List<Integer> getSelectedItems() {
        List<Integer> items = new ArrayList<>(storeChecked.size());
        for (int i = 0; i < storeChecked.size(); ++i) {
            items.add(storeChecked.keyAt(i));
        }
        return items;
    }

    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_branch_name;
        public CheckBox recycler_checkbox;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_branch_name = (TextView) itemView.findViewById(R.id.tv_branch_name);
            recycler_checkbox = (CheckBox) itemView.findViewById(R.id.recycler_checkbox);
            fragment_filter_textview_multiple_new.SelecteditemsAll = new ArrayList<>();
            SelectedsingleList = new ArrayList<>();
            if(user.equalsIgnoreCase("Region")){
                recycler_checkbox.setVisibility(View.GONE);
                itemView.setOnClickListener(this);
            }else{
                recycler_checkbox.setVisibility(View.VISIBLE);

            }
//            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View view) {
            try {

//                if(view.getId() == recycler_checkbox.getId()){
//                    if(getAdapterPosition() == 0){
//                        recycler_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                            @Override
//                            public void onCheckedChanged(CompoundButton compoundButton, boolean ischecked) {
//                              if(ischecked){
//                                  recycler_checkbox.setChecked(ischecked);
//                              }else{
//                                  recycler_checkbox.setChecked(ischecked);
//                              }
//                            }
//                        });
//                    }else{
//                        recycler_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                            @Override
//                            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//
//                            }
//                        });
//
//                    }
//                }
                try {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClick(ZoneList.get(getAdapterPosition()));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public interface ItemClickListener {
        void onItemClick(String selectedBranch);
    }
        @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}


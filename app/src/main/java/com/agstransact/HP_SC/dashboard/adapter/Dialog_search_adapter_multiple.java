package com.agstransact.HP_SC.dashboard.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.Multiple_Selection;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

public class Dialog_search_adapter_multiple extends RecyclerView.Adapter<Dialog_search_adapter_multiple.ViewHolder> {
    private Context context;

    JSONArray branchlists  ;
    JSONArray mfilteredList;
    JSONArray filteredList;
    private ItemClickListener mClickListener;
    ItemClickListener itemClickListener;
    ArrayList<String> ZoneList;
    public Dialog_search_adapter_multiple(JSONArray branchlists, Context context, ItemClickListener itemClickListener){
        super();
        this.branchlists = branchlists;
        this.mfilteredList = branchlists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }
    public Dialog_search_adapter_multiple(ArrayList<String> ZoneList, Context context, ItemClickListener itemClickListener){
        super();
        this.ZoneList = ZoneList;
        this.mfilteredList = branchlists;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.branch_search_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
//        final Multiple_Selection model = ZoneList.get(position);
        try {
            holder.tv_branch_name.setText(ZoneList.get(position));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        holder.view.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
//        holder.tv_branch_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                model.setSelected(!model.isSelected());
//                holder.view.setBackgroundColor(model.isSelected() ? Color.CYAN : Color.WHITE);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return ZoneList.size();
    }


    public  class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tv_branch_name;
        View view;
        public CheckBox recycler_checkbox;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            view =itemView;
            tv_branch_name = (TextView) itemView.findViewById(R.id.tv_branch_name);
            recycler_checkbox = (CheckBox) itemView.findViewById(R.id.recycler_checkbox);
            recycler_checkbox.setVisibility(View.GONE);
            itemView.setOnClickListener(this);

        }
        @Override
        public void onClick(View view) {
            try {
                if (itemClickListener != null) {
                    itemClickListener.onItemClick(ZoneList.get(getAdapterPosition()));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public interface ItemClickListener {
        void onItemClick(String selectedBranch);
    }
}


package com.agstransact.HP_SC.dashboard.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.DateSelectedInterface;
import com.agstransact.HP_SC.utils.UniversalDatePickerDailog;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

public class Fragment_FuelSalesTrends extends Fragment implements View.OnClickListener {

    private View rootView;
    private LineChart mChart;
    private ArrayList<String> fuelList;
    private Spinner spinner_fuel_type;
    private String fuel_type="",time="";
    String error="", respCode="";
    public static JSONArray arraymonth,arrayHSD,arrayMS,arrayPower,arrayTurbojet,arrayPower99,arrayTimeSlot;
    int biggest1,biggestmonth1,biggestMS1,biggestPower1,biggestTurbojet1,biggestPower991;
    int smallest1,smallestmonth1,smallestMS1,smallestPower1,smallestTurbojet1,smallestPower991;
    private TextView tv_hour_sales_trends,tv_day_sales_trends,tv_week_sales_trends,tv_month_sales_trends;
    private TextView ms_sale_value,hsd_sale_value,power_sale_value,turbojet_value,power_nine_value;
    private AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    private TextView tv_calendar,tvsp_to_month,tv_date,tv_to_date,tv_hsd,
            tv_ms,tv_power,tv_turbojet;
    private DateSelectedInterface dateSelectedInterface;
    private int currentYear;
    private int yearSelected;
    private int monthSelected;
    private DatePickerDialog picker;
    private Calendar calendar,calendar1,calendar2,calendarweek1,calendarweek2,calendarmonth,calendarmonth2;
    private ImageView iv_back_date,iv_front_date;
    private String formattedDate,formattedDatemonth,previousweeklimit,formattedweeklimit,currentweeklimit,previousmonthlimit,currentmonthlimit;
    private String CurrentDate,last7daysends;
    private SimpleDateFormat df;
    private TextView tv_submit,tv_sales_date;
    private ImageView iv_filter;
    RelativeLayout rl_calender_view;
    Bundle bundle;
    TextView tv_x_axis,tv_y_axis;
    String Unit;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_fuel_sales_trends,container,false);
        /*Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.tlb_header);
        toolbarTV.setVisibility(View.VISIBLE);
        toolbarTV.setText(getResources().getString(R.string.sales_trends));
        ImageView ivBack = (ImageView) toolbar.findViewById(R.id.iv_drawer_menu);
        ivBack.setVisibility(View.VISIBLE);
        ivBack.setImageResource(R.drawable.back_press);
        ImageView tlb_notification = (ImageView) toolbar.findViewById(R.id.tlb_notification);
        tlb_notification.setImageResource(R.drawable.hp_smart_connect_logo);*/
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
        bundle = getArguments();
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.iv_tlb_additional);
        toolbarTV.setText(getResources().getString(R.string.sales_trends));
        setUpViews();
//        if(tv_hour_sales_trends.getText().toString().equalsIgnoreCase("Hour")){
//            rl_calender_view.setVisibility(View.GONE);
//        }else{
//            rl_calender_view.setVisibility(View.VISIBLE);
//        }
//        showChart();

        try {
            if(bundle.getString("FilteredFragment").equalsIgnoreCase("SalesTrends")){
                tv_calendar.setText(bundle.getString("Date"));
                tv_hour_sales_trends.setBackground(null);
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));

                if(bundle.getString("Date_type").equalsIgnoreCase("Hour")){
                    tv_x_axis.setText("HOUR");
                    tv_sales_date.setText(bundle.getString("Date_type"));
                    tv_hour_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }else if(bundle.getString("Date_type").equalsIgnoreCase("Day")){
                    tv_x_axis.setText("DAY");
                    tv_sales_date.setText(bundle.getString("Date_type"));
                    tv_day_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_day_sales_trends.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }else if(bundle.getString("Date_type").equalsIgnoreCase("Week")){
                    tv_x_axis.setText("WEEK");
                    tv_sales_date.setText(bundle.getString("Date_type"));
                    tv_week_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_week_sales_trends.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }else if(bundle.getString("Date_type").equalsIgnoreCase("Month")){
                    tv_x_axis.setText("MONTH");
                    tv_sales_date.setText(bundle.getString("Date_type"));
                    tv_month_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_month_sales_trends.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }
                try {
//                    if(bundle.getString("FilteredFragment").equalsIgnoreCase("SalesTrends")) {
                        for (int i = 0; i < fuelList.size(); i++) {
                            if (bundle.getString("Fuel_Type").contains(fuelList.get(i))) {
                                spinner_fuel_type.setSelection(i);
                            }
                        }
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            time = "HOUR";
        }

        return rootView;
    }

    private void setUpViews() {
        mChart = rootView.findViewById(R.id.chart1);
        spinner_fuel_type = rootView.findViewById(R.id.sp_sales_trends_filter);
        tv_hour_sales_trends = rootView.findViewById(R.id.tv_hour_sales_trends);
        tv_day_sales_trends = rootView.findViewById(R.id.tv_day_sales_trends);
        tv_week_sales_trends = rootView.findViewById(R.id.tv_week_sales_trends);
        tv_month_sales_trends = rootView.findViewById(R.id.tv_month_sales_trends);
        tv_calendar = rootView.findViewById(R.id.tv_calendar);
        iv_back_date = rootView.findViewById(R.id.iv_back_date);
        iv_front_date = rootView.findViewById(R.id.iv_front_date);
        ms_sale_value = rootView.findViewById(R.id.ms_sale_value);
        rl_calender_view = rootView.findViewById(R.id.rl_calender_view);
        hsd_sale_value = rootView.findViewById(R.id.hsd_sale_value);
        power_sale_value = rootView.findViewById(R.id.power_sale_value);
        power_nine_value = rootView.findViewById(R.id.power_nine_value);
        turbojet_value = rootView.findViewById(R.id.turbojet_value);
        tv_submit = rootView.findViewById(R.id.tv_submit);
        iv_filter = rootView.findViewById(R.id.iv_filter);
        tv_x_axis = rootView.findViewById(R.id.tv_x_axis);
        tv_y_axis = rootView.findViewById(R.id.tv_y_axis);
        tv_sales_date = rootView.findViewById(R.id.tv_sales_date);

        iv_front_date.setOnClickListener(this);
        iv_back_date.setOnClickListener(this);
        tv_calendar.setOnClickListener(this);
        tv_hour_sales_trends.setOnClickListener(this);
        tv_day_sales_trends.setOnClickListener(this);
        tv_week_sales_trends.setOnClickListener(this);
        tv_month_sales_trends.setOnClickListener(this);
        tv_submit.setOnClickListener(this);
//        alertDialogInterface = (AlertDialogInterface) this;
        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                "UserCustomRole","").equalsIgnoreCase("DEALER")) {
            tv_y_axis.setText("QTY (KL)");
        }else{
            tv_y_axis.setText("QTY (L)");
        }
        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                "UserCustomRole","").equalsIgnoreCase("DEALER")) {
            Unit =(" KL");
        }else{
            Unit = (" L");
        }
        calendar = Calendar.getInstance();
        currentYear = calendar.get(Calendar.YEAR);
        yearSelected = currentYear;
        monthSelected = calendar.get(Calendar.MONTH);
        calendar = Calendar.getInstance(TimeZone.getDefault());
//        calendar.get(Calendar.MONTH);
        System.out.println("Current time => " + calendar.getTime());
        df = new SimpleDateFormat("dd|MM|yyyy");
//        df = new SimpleDateFormat("MMM YYYY");
        formattedDate = df.format(calendar.getTime());

        calendar1 = Calendar.getInstance();
        calendar1 = Calendar.getInstance(TimeZone.getDefault());
        calendar1.add(Calendar.DATE, -7);
        CurrentDate = df.format(calendar.getTime());
        last7daysends = df.format(calendar1.getTime());
        tv_calendar.setText(formattedDate);
//        if(time.equalsIgnoreCase("MONTH")){
//            df = new SimpleDateFormat("MMM YYYY");
//            tv_calendar.setText(formattedDate);
//        }else if(time.equalsIgnoreCase("WEEK")){
//            df = new SimpleDateFormat("dd|MM|yyyy");
//            tv_calendar.setText(formattedDate);
//        }else{
//            df = new SimpleDateFormat("dd|MM|yyyy");
//            tv_calendar.setText(formattedDate);
//        }



//        Log.e("month",String.valueOf(calendar.get(Calendar.MONTH)));
//        tv_calendar.setText("DATE: " + calendar.get(Calendar.DATE) +"|" + (calendar.get(Calendar.MONTH)+1) +"|" + calendar.get(Calendar.YEAR));
//        String date_n = new SimpleDateFormat("dd | mmm| yyyy", Locale.getDefault()).format(new Date());
//        tv_calendar.setText("DATE:" +date_n);
        populateFuelList();
        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, fuelList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_fuel_type.setAdapter(adapter);
        spinner_fuel_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (fuelList.get(spinner_fuel_type.getSelectedItemPosition()) != null) {
                    fuel_type = fuelList.get(spinner_fuel_type.getSelectedItemPosition());

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }

        });
        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                replaceScreen(new FragmentFilter());
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    txnBundle.putString("Filter_fragment", "SalesTrends");
                    txnBundle.putString("Fuel_Type", fuel_type);
                    txnBundle.putString("Date", tv_calendar.getText().toString());
                    txnBundle.putString("Date_type", time);
//                    fragment_filter_textview filter_screen = new fragment_filter_textview();
                    fragment_filter_textview_multiple_new filter_screen = new fragment_filter_textview_multiple_new();
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else{
                    UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {

                        }

                        @Override
                        public void methodCancel() {

                        }
                    },
                            "",
                            "No filter....."
                            , getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
                }
            }
        });
    }

    private void populateFuelList() {
        fuelList = new ArrayList<>();
        fuelList.add("Select");
        fuelList.add("ALL");
        fuelList.add("HSD");
        fuelList.add("MS");
        fuelList.add("POWER");
        fuelList.add("TURBOJET");
        fuelList.add("POWER 99");
    }

    private void showChart() {
        mChart.clear();
        mChart.setTouchEnabled(true);
        mChart.setPinchZoom(true);
        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
        mv.setChartView(mChart);
        mChart.setMarker(mv);
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
        mChart.animateXY(3000,2000);
        CallSalesTrends();
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//    }
    private void CallSalesTrends() {
        JSONObject json = new JSONObject();
        try {
//            json.put("productType",fuel_type );
            json.put("Product",fuel_type );
            json.put("timeFrame",time);
//            json.put("ROCode","11468030");
//            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "SelectdRO", ""));

//            try {
//                if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Zone")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Region")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone",""));
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "Selectdzone", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdRegion", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//
//
//
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//
//                }
//                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("Region")){
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion",""));
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdRegion", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//
//                        //                    try {
//                        //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//                        //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//                        //                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                    "SelectdRegion",""));
//                        //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                    "SelectdSalesArea",""));
//                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                    "FilteredRO",""));
//                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                        //                                    "FirstFilteredRO", "");
//                        //                        }else{
//                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                    "FilteredRO",""));
//                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                        //                                    "FirstFilteredRO", "");
//                        //                        }
//                        //                    } catch (Exception e) {
//                        //                        e.printStackTrace();
//                        //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                "FilteredRO",""));
//                        //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                        //                                "FirstFilteredRO", "");
//                        //                    }
//
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//
//                }
//                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Selectd","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea",""));
//                                json.put("ROCode", "ALL");
//                            }else{
//                                json.put("ROCode", "ALL");
//                            }
//                        }
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("ALL")) {
//                            try {
//                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                } else {
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", ""));
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }else{
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//
//                        //                    try {
//                        //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//                        //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//                        //
//                        //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                    "SelectdSalesArea",""));
//                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                    "FilteredRO",""));
//                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                        //                                    "FirstFilteredRO", "");
//                        //                        }else{
//                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                    "FilteredRO",""));
//                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                        //                                    "FirstFilteredRO", "");
//                        //                        }
//                        //                    } catch (Exception e) {
//                        //                        e.printStackTrace();
//                        //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        //                                "FilteredRO",""));
//                        //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                        //                                "FirstFilteredRO", "");
//                        //                    }
//
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//
//                }
//                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
//                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO",""));
//                    }
//                }
//                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "UserCustomRole","").equalsIgnoreCase("DEALER")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "ROKey","") );
//                }
//            }
//            catch (JSONException e) {
//                e.printStackTrace();
//            }
            try {
                if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Zone")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                            }

                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Region")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray2 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("SalesArea", jsonObject2);
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdSalesArea",""));
                                String[] myArray3 = "ALL".split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("ROCode", jsonObject3);
//                                json.put("ROCode", "ALL");
                            }else{
                                String[] myArray2 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "Selectdzone", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone","").split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("Zone", jsonObject2 );
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "Selectdzone", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject3 = new JSONArray(myArray3);
                                    json.put("Region", jsonObject3 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray4 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject4 = new JSONArray(myArray4);
                                    json.put("SalesArea", jsonObject4 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject5 = new JSONArray(myArray5);
                            json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject5 = new JSONArray(myArray5);
                            json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }



                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }

                }
                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("Region")){
                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").equalsIgnoreCase("ALL")){
                                String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray1);
                                json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                String[] myArray = "ALL".split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                            }
                            else{
                                String[] myArray = "ALL".split(",");
                                JSONArray jsonObject = new JSONArray(myArray);
                                json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                            }
                        }
                        else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){

                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion",""));
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea",""));
                                json.put("ROCode", "ALL");
                            }else{
                                json.put("ROCode", "ALL");
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("Region", jsonObject2 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                } else {
                                    String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject3 = new JSONArray(myArray3);
                                    json.put("SalesArea", jsonObject3);
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }

                }
                else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLREGION"))
                {
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Selectd","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Field","").equalsIgnoreCase("SalesArea")){

                            if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray1);
                                json.put("SalesArea", jsonObject1);
                                json.put("ROCode", jsonObject2);
                            }
                            else{
                                String[] myArray1 = "ALL".split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray1);
                                json.put("ROCode", jsonObject2);
                            }
                        }
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("ALL")) {
                            try {
                                if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                }
                                else {
                                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray);
                                    json.put("SalesArea", jsonObject1 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }
                        else{
//                            String[] myArray1 = "ALL".split(",");
//                            JSONArray jsonObject2 = new JSONArray(myArray1);
//                            json.put("SalesArea", jsonObject2 );
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }

                        //                    try {
                        //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                        //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                        //
                        //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "SelectdSalesArea",""));
                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "FilteredRO",""));
                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                    "FirstFilteredRO", "");
                        //                        }else{
                        //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                    "FilteredRO",""));
                        //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                    "FirstFilteredRO", "");
                        //                        }
                        //                    } catch (Exception e) {
                        //                        e.printStackTrace();
                        //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                        //                                "FilteredRO",""));
                        //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                        //                                "FirstFilteredRO", "");
                        //                    }

                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                    if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FilteredRO",""));
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").equalsIgnoreCase("")){

                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "ROKey","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }
            String sdf3 = null;
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
               String date = tv_calendar.getText().toString().replace("|","-");
                sdf3 = sdf2.format(sdf.parse(date));
            } catch (ParseException e) {
                e.printStackTrace();
            }
//            if(time.equalsIgnoreCase("HOUR")){
//              json.put("FromDate","");
//
//            }else{
            if(time.equalsIgnoreCase("MONTH")){
                json.put("FromDate",tv_calendar.getText().toString());
            }else{
                json.put("FromDate",sdf3);
            }
            if(time.equalsIgnoreCase("HOUR")){
                tv_x_axis.setText("Hour");
            }else if(time.equalsIgnoreCase("DAY")){
                tv_x_axis.setText("Hours(Cumulative)");
            }else if(time.equalsIgnoreCase("WEEK")){
                tv_x_axis.setText("Day");
            }else if(time.equalsIgnoreCase("MONTH")){
                tv_x_axis.setText("Day");

            }

//            }
            Log.e("json",json.toString());
            new AsyncSalesTrends().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tv_submit:
                showChart();
                break;
            case R.id.tv_calendar:

//                final Calendar cldr = Calendar.getInstance();
//                int day = cldr.get(Calendar.DAY_OF_MONTH);
//                int month = cldr.get(Calendar.MONTH);
//                int year = cldr.get(Calendar.YEAR);
//                // date picker dialog
//
//                picker = new DatePickerDialog(getActivity(),
//                        new DatePickerDialog.OnDateSetListener() {
//                            @Override
//                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                                tv_calendar.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
//                            }
//                        }, year, month, day);
//                picker.show();
                if(time.equalsIgnoreCase("WEEK")){
                    UniversalDatePickerDailog datePickerDailog = new UniversalDatePickerDailog(getActivity(), tv_calendar, dateSelectedInterface, "Select WEEk");
                    datePickerDailog.show(getActivity().getSupportFragmentManager(), "datePicker");
                }


                break;

            case R.id.tv_hour_sales_trends:

                tv_hour_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.black));
                time = "HOUR";
                tv_calendar.setText(formattedDate);
                tv_sales_date.setText(time);
//                rl_calender_view.setVisibility(View.GONE);
                tv_day_sales_trends.setBackground(null);
                tv_week_sales_trends.setBackground(null);
                tv_month_sales_trends.setBackground(null);
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.tv_day_sales_trends:

                tv_day_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.black));
                time = "DAY";
                tv_calendar.setText(formattedDate);
                rl_calender_view.setVisibility(View.VISIBLE);
                tv_sales_date.setText(time);
                tv_hour_sales_trends.setBackground(null);
                tv_week_sales_trends.setBackground(null);
                tv_month_sales_trends.setBackground(null);
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.tv_week_sales_trends:

                tv_week_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.black));
                time = "WEEK";
                calendarweek1 = Calendar.getInstance();
                calendarweek1 = Calendar.getInstance(TimeZone.getDefault());
                calendarweek1.add(Calendar.DATE, -7);
                df = new SimpleDateFormat("dd|MM|yyyy");
                currentweeklimit = df.format(calendarweek1.getTime());
                formattedweeklimit = df.format(calendarweek1.getTime());
                tv_calendar.setText(formattedweeklimit);

                calendarweek2 = Calendar.getInstance();
                calendarweek2 = Calendar.getInstance(TimeZone.getDefault());
                calendarweek2.add(Calendar.MONTH, -3);
                previousweeklimit = df.format(calendarweek2.getTime());
                tv_sales_date.setText(time);
                rl_calender_view.setVisibility(View.VISIBLE);
                tv_day_sales_trends.setBackground(null);
                tv_hour_sales_trends.setBackground(null);
                tv_month_sales_trends.setBackground(null);

                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.tv_month_sales_trends:

                tv_month_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.black));
                time = "MONTH";
                calendarmonth = Calendar.getInstance();
                calendarmonth = Calendar.getInstance(TimeZone.getDefault());
//                calendar1.add(Calendar.MONTH, -7);
                df = new SimpleDateFormat("MMM YYYY");
                formattedDatemonth = df.format(calendarmonth.getTime());
                tv_calendar.setText(formattedDatemonth);

                calendarmonth2 = Calendar.getInstance();
                calendarmonth2 = Calendar.getInstance(TimeZone.getDefault());
                calendarmonth2.add(Calendar.MONTH, -3);
                currentmonthlimit= df.format(calendarmonth.getTime());
                previousmonthlimit= df.format(calendarmonth2.getTime());
                tv_sales_date.setText(time);
                rl_calender_view.setVisibility(View.VISIBLE);
                tv_day_sales_trends.setBackground(null);
                tv_week_sales_trends.setBackground(null);
                tv_hour_sales_trends.setBackground(null);
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.iv_back_date:
                if(time.equalsIgnoreCase("MONTH")){
                    if(previousmonthlimit.equalsIgnoreCase(tv_calendar.getText().toString()))
                    {

                    }else {
                        calendarmonth.add(Calendar.MONTH, -1);
                        formattedDatemonth = df.format(calendarmonth.getTime());
                        android.util.Log.v("PREVIOUS DATE : ", formattedDatemonth);
                        tv_calendar.setText(formattedDatemonth);
                    }

                 }
                else if(time.equalsIgnoreCase("WEEK")){
                    if(previousweeklimit.equalsIgnoreCase(tv_calendar.getText().toString())){

                    }else{
                        calendarweek1.add(Calendar.DATE, -1);
                        formattedweeklimit = df.format(calendarweek1.getTime());
                        Log.v("PREVIOUS DATE : ", formattedweeklimit);
                        tv_calendar.setText(formattedweeklimit);
                    }
                }else{
                    if(last7daysends.equalsIgnoreCase(tv_calendar.getText().toString())){

                    }else{
                        calendar.add(Calendar.DATE, -1);
                        formattedDate = df.format(calendar.getTime());
                        Log.v("PREVIOUS DATE : ", formattedDate);
                        tv_calendar.setText(formattedDate);
                    }
                }


//                Date referenceDate = new Date();
//                Calendar c = Calendar.getInstance();
//                c.setTime(referenceDate);
//                c.add(Calendar.MONTH, -1);
//                SimpleDateFormat postFormater = new SimpleDateFormat("MMM YYYY");
//                formattedDate = postFormater.format(c.getTime());
//                tv_calendar.setText(formattedDate);
                break;


            case R.id.iv_front_date:
//                Date referenceDate1 = new Date();
//                Calendar c1 = Calendar.getInstance();
//                c1.setTime(referenceDate1);
//                c1.add(Calendar.MONTH, 1);
//                SimpleDateFormat postFormater1 = new SimpleDateFormat("MMM YYYY");
//                formattedDate = postFormater1.format(c1.getTime());
//                tv_calendar.setText(formattedDate);
//                Date referenceDate1 = new Date();
//                calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) +1);

//                referenceDate1.setMonth(3);
//                calendar.setTime(referenceDate1);
                if(time.equalsIgnoreCase("MONTH")){
                    if(time.equalsIgnoreCase("MONTH")){
                        if(currentmonthlimit.equalsIgnoreCase(tv_calendar.getText().toString()))
                        {

                        }else {
                            calendarmonth.add(Calendar.MONTH, 1);
                            formattedDatemonth = df.format(calendarmonth.getTime());
                            android.util.Log.v("PREVIOUS DATE : ", formattedDatemonth);
                            tv_calendar.setText(formattedDatemonth);
                        }

                    }

                }else if(time.equalsIgnoreCase("WEEK")){
                    if(currentweeklimit.equalsIgnoreCase(tv_calendar.getText().toString()))
                    {

                    }else {
                        calendarweek1.add(Calendar.DATE, 1);
                        formattedweeklimit = df.format(calendarweek1.getTime());
                        android.util.Log.v("PREVIOUS DATE : ", previousweeklimit);
                        tv_calendar.setText(formattedweeklimit);
                    }


                }else{
                    if(CurrentDate.equalsIgnoreCase(tv_calendar.getText().toString())){

                    }else{
                        calendar.add(Calendar.DATE, 1);
                        formattedDate = df.format(calendar.getTime());
                        Log.v("NEXT DATE : ", formattedDate);
                        tv_calendar.setText(formattedDate);
                    }
                }
//                calendar.add(Calendar.DATE, 1);
//                formattedDate = df.format(calendar.getTime());
//                Log.v("NEXT DATE : ", formattedDate);
//                tv_calendar.setText(formattedDate);

                break;
//            for date
//                case R.id.iv_back_date:
//                calendar.add(Calendar.DATE, -1);
//                formattedDate = df.format(calendar.getTime());
//                Log.v("PREVIOUS DATE : ", formattedDate);
//                tv_calendar.setText(formattedDate);
//
//                break;
//
//
//                case R.id.iv_front_date:
//
//                calendar.add(Calendar.DATE, 1);
//                formattedDate = df.format(calendar.getTime());
//                Log.v("NEXT DATE : ", formattedDate);
//                tv_calendar.setText(formattedDate);
//
//                break;

        }
    }

    private class AsyncSalesTrends extends AsyncTask<JSONObject, Void, String>{

        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncSalesTrends() {
        }

        public AsyncSalesTrends(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
//            try {
//                dialog = ProgressDialog.show(getActivity(), "",
//                        "Processing", true);
//                dialog.setCancelable(false);
//                dialog.show();
//            } catch (Exception e) {
//            }
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
//            return ClsWebService_hpcl.PostObjecttoken("Dashboard/SalesTrend", false, jsonObjects[0],"");
            return ClsWebService_hpcl.PostObjecttoken("DashboardV2/GetSalesTrend", false, jsonObjects[0],"");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }

            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("Data"));
                            arraymonth = new JSONArray(jsonObject1.getString("month"));
                            arrayHSD = new JSONArray(jsonObject1.getString("HSD"));
                            arrayMS = new JSONArray(jsonObject1.getString("MS"));
                            arrayPower = new JSONArray(jsonObject1.getString("Power"));
                            arrayTurbojet = new JSONArray(jsonObject1.getString("Turbojet"));
                            arrayPower99 = new JSONArray(jsonObject1.getString("Power99"));

                            JSONArray tsArray= jsonObject.getJSONArray("TimeSlot");
                            for (int tl=0; tl< tsArray.length(); tl++){
                                JSONObject tsObject = tsArray.getJSONObject(tl);
                                ms_sale_value.setText(tsObject.getString("TotalMS")+ Unit);
                                hsd_sale_value.setText(tsObject.getString("TotalHSD")+Unit);
                                power_sale_value.setText(tsObject.getString("TotalPOWER")+Unit);
                                turbojet_value.setText(tsObject.getString("TotalTURBOJECT")+Unit);
                                power_nine_value.setText(tsObject.getString("TotalPOWER99")+Unit);
                            }
//                            JSONObject tsObject = new JSONObject(str[3]);
                            Log.e("tsObject",tsArray.toString());

                            /*if (jsonObject.has("TimeSlot")){
                                JSONArray Jsonarray1 = new JSONArray(jsonObject1.getString("TimeSlot"));
                                for (int tl=0; tl<= Jsonarray1.length(); tl++){
                                    JSONObject tsObject = arrayTimeSlot.getJSONObject(tl);
                                    ms_sale_value.setText(tsObject.getString("TotalMS"));
                                    hsd_sale_value.setText(tsObject.getString("TotalHSD"));
                                    power_sale_value.setText(tsObject.getString("TotalPOWER"));
                                    turbojet_value.setText(tsObject.getString("TotalTURBOJECT"));
                                    power_nine_value.setText(tsObject.getString("TotalPOWER99"));
                                }
                            }*/

//                            JSONObject jsonObject2 = new JSONObject(str[3]);

//                            JSONObject jsonObject3 = new JSONObject(jsonObject.getString("TimeSlot"));
//                            arrayTimeSlot = new JSONArray(jsonObject1.getString("TimeSlot"));


                            List<Integer> list = new ArrayList<Integer>();
                            List<Integer> list2 = new ArrayList<Integer>();
                            List<Integer> listMS = new ArrayList<Integer>();
                            List<Integer> listPower = new ArrayList<Integer>();
                            List<Integer> listTurbojet = new ArrayList<Integer>();
                            List<Integer> listPower99 = new ArrayList<Integer>();
                            List<Integer> listTimeSlot = new ArrayList<Integer>();
                            for (int i1 = 0; i1 < arraymonth.length(); i1++) {
                                list2.add(arraymonth.getInt(i1));
                            }
                            for (int i = 0; i < arrayHSD.length(); i++) {
                                list.add(arrayHSD.getInt(i));
                            }
                            for (int ms = 0; ms < arrayMS.length(); ms++) {
                                listMS.add(arrayMS.getInt(ms));
                            }
                            for (int power = 0; power < arrayPower.length(); power++) {
                                listPower.add(arrayPower.getInt(power));
                            }
                            for (int turbojet = 0; turbojet < arrayTurbojet.length(); turbojet++) {
                                listTurbojet.add(arrayTurbojet.getInt(turbojet));
                            }
                            for (int power99 = 0; power99 < arrayPower99.length(); power99++) {
                                listPower99.add(arrayPower99.getInt(power99));
                            }
                            int smallest = list.get(0);
                            int biggest = list.get(0);
                            int smallestmonth = list2.get(0);
                            int biggestmonth = list2.get(0);
                            int smallestMS = listMS.get(0);
                            int biggestMS = listMS.get(0);
                            int smallestPower = listPower.get(0);
                            int biggestPower = listPower.get(0);
                            int smallestTurbojet = listTurbojet.get(0);
                            int biggestTurbojet = listTurbojet.get(0);
                            int smallestPower99 = listPower99.get(0);
                            int biggestPower99 = listPower99.get(0);

                            for (int i = 1; i < list.size(); i++) {
                                if (list.get(i) > biggest)
                                    biggest = list.get(i);
                                else if (list.get(i) < smallest)
                                    smallest = list.get(i);
                            }
                            for (int i2 = 1; i2 < list2.size(); i2++) {
                                if (list2.get(i2) > biggestmonth)
                                    biggestmonth = list2.get(i2);
                                else if (list2.get(i2) < smallestmonth)
                                    smallestmonth = list2.get(i2);

                            }
                            for (int ms = 1; ms < listMS.size(); ms++) {
                                if (listMS.get(ms) > biggestMS)
                                    biggestMS = listMS.get(ms);
                                else if (listMS.get(ms) < smallestMS)
                                    smallestMS = listMS.get(ms);

                            }
                            for (int power = 1; power < list2.size(); power++) {
                                if (listPower.get(power) > biggestPower)
                                    biggestPower = listPower.get(power);
                                else if (listPower.get(power) < smallestPower)
                                    smallestPower = listPower.get(power);
                            }
                            for (int turbojet = 1; turbojet < listTurbojet.size(); turbojet++) {
                                if (listTurbojet.get(turbojet) > biggestTurbojet)
                                    biggestTurbojet = listTurbojet.get(turbojet);
                                else if (listTurbojet.get(turbojet) < smallestTurbojet)
                                    smallestTurbojet = listTurbojet.get(turbojet);
                            }
                            for (int Power99 = 1; Power99 < listPower99.size(); Power99++) {
                                if (listPower99.get(Power99) > biggestPower99)
                                    biggestPower99 = listPower99.get(Power99);
                                else if (listPower99.get(Power99) < smallestPower99)
                                    smallestPower99 = listPower99.get(Power99);
                            }

//                        revArrayList=  new ArrayList<Integer>();
//                        for (int i = list2.size() - 1; i >= 0; i--) {
//                            // Append the elements in reverse order
//                              revArrayList.add(list2.get(i));
//                        }
                            biggest1 = biggest;
                            smallest1 = smallest;
                            biggestmonth1 = biggestmonth;
                            smallestmonth1 = smallestmonth;
                            biggestMS1 = biggestMS;
                            smallestMS1 = smallestMS;
                            biggestPower1 = biggestPower;
                            smallestPower1 = smallestPower;
                            biggestTurbojet1 = biggestTurbojet;
                            smallestTurbojet1 = smallestTurbojet;
                            biggestPower991 = biggestPower99;
                            smallestPower991 = smallestPower99;
//                        arraymonth = new JSONArray(revArrayList);
//                            tv_Xaxis.setVisibility(View.VISIBLE);
//                            tv_Yaxis.setVisibility(View.VISIBLE);
                           /* if(time.equalsIgnoreCase("MONTH")){
                                tv_date.setText("From Date :"+Jsonarray1.getJSONObject(0).getString("FromDate"));
                                tv_to_date.setText("To Date :"+Jsonarray1.getJSONObject(0).getString("ToDate"));
                                tv_date.setVisibility(View.VISIBLE);
                                tv_to_date.setVisibility(View.VISIBLE);
                            }else if(time.equalsIgnoreCase("WEEK")){
                                tv_date.setText("From Date :"+Jsonarray1.getJSONObject(0).getString("FromDate"));
                                tv_to_date.setText("To Date :"+Jsonarray1.getJSONObject(0).getString("ToDate"));
                                tv_date.setVisibility(View.VISIBLE);
                                tv_to_date.setVisibility(View.VISIBLE);
                            }else if(time.equalsIgnoreCase("DAY")){
                                tv_date.setVisibility(View.VISIBLE);
                                tv_to_date.setVisibility(View.GONE);
                                tv_date.setText("Date :"+Jsonarray1.getJSONObject(0).getString("FromDate"));
//                                tv_to_date.setText("To Date :"+Jsonarray1.getJSONObject(0).getString("ToDate"));

                            }else if(time.equalsIgnoreCase("HOUR")){
                                tv_date.setVisibility(View.VISIBLE);
                                tv_to_date.setVisibility(View.GONE);
                                tv_date.setText("Date :"+Jsonarray1.getJSONObject(0).getString("FromDate"));
//                                tv_to_date.setText("To Date :"+Jsonarray1.getJSONObject(0).getString("ToDate"));

                            }else{
//                                tv_date.setVisibility(View.GONE);
//                                tv_to_date.setVisibility(View.GONE);
                            }*/
                            /*if(fuel_type.equalsIgnoreCase("HSD")){
                                tv_hsd.setText("Total HSD :"+ Jsonarray1.getJSONObject(0).getString("TotalHSD"));
                                tv_power.setVisibility(View.GONE);
                                tv_ms.setVisibility(View.GONE);
                                tv_turbojet.setVisibility(View.GONE);
                            }else if(fuel_type.equalsIgnoreCase("POWER")){
                                tv_hsd.setText("Total Power :"+ Jsonarray1.getJSONObject(0).getString("TotalPOWER"));
                                tv_power.setVisibility(View.GONE);
                                tv_ms.setVisibility(View.GONE);
                                tv_turbojet.setVisibility(View.GONE);
                            }else if(fuel_type.equalsIgnoreCase("MS")){
                                tv_hsd.setText("Total MS :"+ Jsonarray1.getJSONObject(0).getString("TotalMS"));
                                tv_power.setVisibility(View.GONE);
                                tv_ms.setVisibility(View.GONE);
                                tv_turbojet.setVisibility(View.GONE);
                            }else if(fuel_type.equalsIgnoreCase("TURBOJET")){
                                tv_hsd.setText("Total Turbojet :"+ Jsonarray1.getJSONObject(0).getString("TotalTURBOJECT"));
                                tv_power.setVisibility(View.GONE);
                                tv_ms.setVisibility(View.GONE);
                                tv_turbojet.setVisibility(View.GONE);
                            }else if(fuel_type.equalsIgnoreCase("ALL")){
                                tv_power.setVisibility(View.VISIBLE);
                                tv_ms.setVisibility(View.VISIBLE);
                                tv_turbojet.setVisibility(View.VISIBLE);
                                tv_hsd.setVisibility(View.VISIBLE);
                                tv_hsd.setText("Total HSD :"+ Jsonarray1.getJSONObject(0).getString("TotalHSD"));
                                tv_power.setText("Total Power :"+ Jsonarray1.getJSONObject(0).getString("TotalPOWER"));
                                tv_ms.setText("Total MS :"+ Jsonarray1.getJSONObject(0).getString("TotalMS"));
                                tv_turbojet.setText("Total Turbojet :"+ Jsonarray1.getJSONObject(0).getString("TotalTURBOJECT"));
                            }*/
                            renderData();
                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                            mChart.setNoDataText("NIL");
                            mChart.invalidate();
                            mChart.clear();
//                            tv_Xaxis.setVisibility(View.GONE);
//                            tv_Yaxis.setVisibility(View.GONE);
                            tv_date.setVisibility(View.GONE);
                            tv_to_date.setVisibility(View.GONE);
                            tv_hsd.setVisibility(View.GONE);
                            tv_ms.setVisibility(View.GONE);
                            tv_power.setVisibility(View.GONE);
                            tv_turbojet.setVisibility(View.GONE);
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                            mChart.setNoDataText("NIL");
                            mChart.invalidate();
                            mChart.clear();
//                            tv_Xaxis.setVisibility(View.GONE);
//                            tv_Yaxis.setVisibility(View.GONE);
                            tv_date.setVisibility(View.GONE);
                            tv_to_date.setVisibility(View.GONE);
                            tv_hsd.setVisibility(View.GONE);
                            tv_ms.setVisibility(View.GONE);
                            tv_power.setVisibility(View.GONE);
                            tv_turbojet.setVisibility(View.GONE);
                        }
                    }else{
                        mChart.setNoDataText("NIL");
                        mChart.invalidate();
                        mChart.clear();
//                        tv_Xaxis.setVisibility(View.GONE);
//                        tv_Yaxis.setVisibility(View.GONE);
                        tv_date.setVisibility(View.GONE);
                        tv_to_date.setVisibility(View.GONE);
                        tv_hsd.setVisibility(View.GONE);
                        tv_ms.setVisibility(View.GONE);
                        tv_power.setVisibility(View.GONE);
                        tv_turbojet.setVisibility(View.GONE);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    mChart.setNoDataText("NIL");
                    mChart.invalidate();
                    mChart.clear();
//                    tv_Xaxis.setVisibility(View.GONE);
//                    tv_Yaxis.setVisibility(View.GONE);

                }
            }
        }
    }

    private void renderData() {

        LimitLine llXAxis = new LimitLine(10f, "Index 10");
//        llXAxis.setLineWidth(4f);
//        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);


        XAxis xAxis = mChart.getXAxis();
        xAxis.setTextColor(getResources().getColor(R.color.black));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
//        xAxis.enableGridDashedLine(00f, 00f, 0f);
        xAxis.setAxisMaximum(10f);
        xAxis.setAxisMinimum(0f);
        float f2 =0.0f;
        float f3 =0.0f;
        if(time.equalsIgnoreCase("DAY")||
                time.equalsIgnoreCase("HOUR")){
            f3 = (float) smallestmonth1+1 / 1;
            f2 = (float) biggestmonth1+1 / 1;
        }else{
            f2 = (float) biggestmonth1 / 1;
            f3 = (float) smallestmonth1/ 1;
        }


        xAxis.setAxisMaximum(f2);
        xAxis.setAxisMinimum(f3);
//        xAxis.setValueFormatter(new AxisValueFormatter());
//        xAxis.setValueFormatter(new DateAxisValueFormatter(arraymonth.getString()){
//
//        });
//
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                List<String> listmonth = new ArrayList<String>();
//                for (int i1 = 0; i1 < arraymonth.length(); i1++) {
//                    try {
//                        listmonth.add(arraymonth.getString(i1));
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                return listmonth.get((int) value); // xVal is a string array
//            }
//
//        });
//        xAxis.setValueFormatter(new IAxisValueFormatter() {
//            @Override
//            public String getFormattedValue(float value, AxisBase axis) {
//                for (int i = 0 ; i < arraymonth.length(); ++i) {
//                    try {
//                        if (arraymonth.get(i).equals(value)) {
//                            return String.valueOf(arraymonth.get(i));
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//                return null;
//            }
//        });
        int count = biggestmonth1 - smallestmonth1;
        if(count <= 5){
            xAxis.setLabelCount(4);
        }
        xAxis.setDrawLimitLinesBehindData(true);
        xAxis.setGranularity(1f);

        LimitLine ll1 = new LimitLine(215f, "Maximum Limit");
        ll1.setLineWidth(4f);
//        ll1.enableDashedLine(10f, 10f, 0f);
//        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll1.setTextSize(10f);

        LimitLine ll2 = new LimitLine(70f, "Minimum Limit");
        ll2.setLineWidth(4f);
//        ll2.enableDashedLine(00f, 00f, 0f);
//        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);
        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.setGranularity(1f);
//        leftAxis.addLimitLine(ll1);
//        leftAxis.addLimitLine(ll2);
        leftAxis.setTextColor(getResources().getColor(R.color.black));
        int[] biggestall = {biggest1, biggestMS1, biggestPower1, biggestTurbojet1,biggestPower991};
        int[] smallestall = {smallest1, smallestMS1, smallestPower1, smallestTurbojet1,smallestPower991};
        int smallestall1 = smallestall[0];
        int biggestall1 = biggestall[0];
        List<Integer> listsmallestAll = new ArrayList<Integer>();
        List<Integer> listbiggestAll = new ArrayList<Integer>();
        for (int i1=0; i1<biggestall.length; i1++) {
            listbiggestAll.add(biggestall[i1]);
        }
        for (int i1=0; i1<smallestall.length; i1++) {
            listsmallestAll.add(smallestall[i1]);
        }

        for(int i=1; i< listbiggestAll.size(); i++)
        {
            if(listbiggestAll.get(i) > biggestall1)
                biggestall1 = listbiggestAll.get(i);
        }
        for(int i=1; i< listbiggestAll.size(); i++)
        {
            if (listsmallestAll.get(i) < smallestall1)
                smallestall1 = listsmallestAll.get(i);
        }
        float f=0.0f,f1=0.0f;
        if(fuel_type.equalsIgnoreCase("HSD")){
            f = (float) biggest1 / 1;
            f1 = (float) smallest1 / 1;
        }else if(fuel_type.equalsIgnoreCase("POWER")){
            f = (float) biggestPower1 / 1;
            f1 = (float) smallestPower1 / 1;
        }else if(fuel_type.equalsIgnoreCase("MS")){
            f = (float) biggestMS1 / 1;
            f1 = (float) smallestMS1 / 1;
        }else if(fuel_type.equalsIgnoreCase("TURBOJET")){
            f = (float) biggestTurbojet1 / 1;
            f1 = (float) smallestTurbojet1 / 1;
        }else if(fuel_type.equalsIgnoreCase("POWER 99")){
            f = (float) biggestPower991 / 1;
            f1 = (float) smallestPower991 / 1;
        } else if(fuel_type.equalsIgnoreCase("ALL")){
            f = (float) biggestall1 / 1;
            f1 = (float) smallestall1 / 1;
        }


        leftAxis.setAxisMaximum(f);
        leftAxis.setAxisMinimum(0f);
//        leftAxis.setAxisMaximum(350f);
//        leftAxis.setAxisMinimum(0f);
//        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(false);
        mChart.getLegend().setTextColor(getResources().getColor(R.color.white));
        mChart.getAxisRight().setEnabled(false);
        mChart.getLegend().setEnabled(false);
        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
        mv.setChartView(mChart);
        mChart.setMarker(mv);
        mChart.animateXY(3000,2000);
        setData();
    }

    private void setData() {

        ArrayList<Entry> values = new ArrayList<>();
        ArrayList<Entry> valuesHSD = new ArrayList<>();
        ArrayList<Entry> valuesMS = new ArrayList<>();
        ArrayList<Entry> valuesPower = new ArrayList<>();
        ArrayList<Entry> valuesTurbojet = new ArrayList<>();
        ArrayList<Entry> valuesPower99 = new ArrayList<>();
        if(fuel_type.equalsIgnoreCase("HSD")){
            for(int i = 0; i < arrayHSD.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesHSD.add(new Entry(arraymonth.getInt(i)+1, arrayHSD.getInt(i)));
                    }else{
                        valuesHSD.add(new Entry(arraymonth.getInt(i), arrayHSD.getInt(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("POWER")){
            for(int i = 0; i < arrayPower.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesPower.add(new Entry(arraymonth.getInt(i)+1, arrayPower.getInt(i)));
                    }else{
                        valuesPower.add(new Entry(arraymonth.getInt(i), arrayPower.getInt(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("MS")){
            for(int i = 0; i < arrayMS.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesMS.add(new Entry(arraymonth.getInt(i)+1, arrayMS.getInt(i)));
                    }else{
                        valuesMS.add(new Entry(arraymonth.getInt(i), arrayMS.getInt(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("TURBOJET")){
            for(int i = 0; i < arrayTurbojet.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesTurbojet.add(new Entry(arraymonth.getInt(i)+1, arrayTurbojet.getInt(i)));
                    }else{
                        valuesTurbojet.add(new Entry(arraymonth.getInt(i), arrayTurbojet.getInt(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("POWER 99")){
            for(int i = 0; i < arrayPower99.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesPower99.add(new Entry(arraymonth.getInt(i)+1, arrayPower99.getInt(i)));
                    }else{
                        valuesPower99.add(new Entry(arraymonth.getInt(i), arrayPower99.getInt(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("ALL")){
            for(int i = 0; i < arrayHSD.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesHSD.add(new Entry(arraymonth.getInt(i)+1, arrayHSD.getInt(i)));
                    }else{
                        valuesHSD.add(new Entry(arraymonth.getInt(i), arrayHSD.getInt(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayMS.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesMS.add(new Entry(arraymonth.getInt(i)+1, arrayMS.getInt(i)));
                    }else{
                        valuesMS.add(new Entry(arraymonth.getInt(i), arrayMS.getInt(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayPower.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesPower.add(new Entry(arraymonth.getInt(i)+1, arrayPower.getInt(i)));
                    }else{
                        valuesPower.add(new Entry(arraymonth.getInt(i), arrayPower.getInt(i)));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayTurbojet.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesTurbojet.add(new Entry(arraymonth.getInt(i)+1, arrayTurbojet.getInt(i)));
                    }else{
                        valuesTurbojet.add(new Entry(arraymonth.getInt(i), arrayTurbojet.getInt(i)));
                    }                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayPower99.length(); i++){
                try {
                    if(time.equalsIgnoreCase("DAY")||
                            time.equalsIgnoreCase("HOUR")){
                        valuesPower99.add(new Entry(arraymonth.getInt(i)+1, arrayPower99.getInt(i)));
                    }else{
                        valuesPower99.add(new Entry(arraymonth.getInt(i), arrayPower99.getInt(i)));
                    }                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
//        values.add(new Entry(0, 0));
//        values.add(new Entry(1, 50));
//        values.add(new Entry(2, 100));
//        values.add(new Entry(3, 80));
//        values.add(new Entry(4, 120));
//        values.add(new Entry(5, 110));
//        values.add(new Entry(7, 150));
//        values.add(new Entry(8, 250));
//        values.add(new Entry(9, 190));
//        values2.add(new Entry(0, 50));
//        values2.add(new Entry(1, 100));
//        values2.add(new Entry(2, 150));
//        values2.add(new Entry(3, 100));
//        values2.add(new Entry(4, 140));
//        values2.add(new Entry(5, 120));
//        values2.add(new Entry(7, 170));
//        values2.add(new Entry(8, 280));
//        values2.add(new Entry(9, 140));

        LineDataSet set1,set2,set3,set4,set5;
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        LineData data ;
//        if (mChart.getData() != null &&
//                mChart.getData().getDataSetCount() > 0) {
//            set1 = (LineDataSet) mChart.getData().getDataSetByIndex(0);
//            set1.setValues(values);
//            mChart.getData().notifyDataChanged();
//            mChart.notifyDataSetChanged();
//        } else {
        /*if(!(fuel_type.equals("Select") || time.equals("Select"))){
            if(time.equalsIgnoreCase("WEEK")||
                    time.equalsIgnoreCase("MONTH")){
                tv_Xaxis.setText("X-Axis = "+ "DAY");
            }else if(time.equalsIgnoreCase("DAY")||
                    time.equalsIgnoreCase("HOUR")){
                tv_Xaxis.setText("X-Axis = "+ "HOUR");
            }
            tv_Yaxis.setText("Y-Axis = "+ "Quantity(Ltrs)");
        }else{
            tv_Xaxis.setVisibility(View.GONE);
            tv_Yaxis.setVisibility(View.GONE);
        }*/
        if(fuel_type.equalsIgnoreCase("HSD")){
            set1 = new LineDataSet(valuesHSD, "HSD");
            set1.setValueTextColor(getResources().getColor(R.color.white));
            set1.setDrawIcons(false);
//            set1.enableDashedLine(10f, 0f, 0f);
//            set1.enableDashedHighlightLine(10f, 0f, 0f);
//                set1.setColor(getResources().getColor(R.color.dot_light_screen2));
            set1.setColor(getResources().getColor(R.color.line_chart_hsd_color));
//            set1.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set1.setLineWidth(1f);
//            set1.setCircleRadius(3f);
//            set1.setDrawCircleHole(false);
            set1.setValueTextSize(0f);
            set1.setDrawFilled(true);
//            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f,5f}, 0f));
            set1.setFormSize(15.f);
//                Drawable drawable2 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
//                set1.setFillDrawable(drawable2);
//                                set1.setFillColor(Color.DKGRAY);
           if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_hsd);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.DKGRAY);
            }
            dataSets.add(set1);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.getDescription().setEnabled(false);
            mChart.invalidate();

        }
        else if(fuel_type.equalsIgnoreCase("MS")){
            set2 = new LineDataSet(valuesMS, "MS");
            set2.setValueTextColor(getResources().getColor(R.color.white));
            set2.setDrawIcons(false);
//            set2.enableDashedLine(10f, 0f, 0f);
//            set2.enableDashedHighlightLine(10f, 0f, 0f);
            set2.setColor(getResources().getColor(R.color.line_chart_ms_color));
//            set2.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set2.setLineWidth(1f);
//            set2.setCircleRadius(3f);
//            set2.setDrawCircleHole(false);
            set2.setValueTextSize(0f);
            set2.setDrawFilled(true);
            set2.setFormLineWidth(1f);
//            set2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set2.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_ms);
                set2.setFillDrawable(drawable);
            } else {
                set2.setFillColor(Color.DKGRAY);
            }
            dataSets.add(set2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.getDescription().setEnabled(false);
            mChart.invalidate();
        }
        else if(fuel_type.equalsIgnoreCase("POWER")){
            set3 = new LineDataSet(valuesPower, "POWER");
            set3.setValueTextColor(getResources().getColor(R.color.white));
            set3.setDrawIcons(false);
//            set3.enableDashedLine(10f, 0f, 0f);
//            set3.enableDashedHighlightLine(10f, 0f, 0f);
            set3.setColor(getResources().getColor(R.color.line_chart_power_color));
//            set3.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set3.setLineWidth(1f);
//            set3.setCircleRadius(3f);
//            set3.setDrawCircleHole(false);
            set3.setValueTextSize(0f);
            set3.setDrawFilled(true);
            set3.setFormLineWidth(1f);
//            set3.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set3.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_power);
                set3.setFillDrawable(drawable);
            } else {
                set3.setFillColor(Color.DKGRAY);
            }
            dataSets.add(set3);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.getDescription().setEnabled(false);
            mChart.invalidate();
        }
        else if(fuel_type.equalsIgnoreCase("TURBOJET")){
            set4 = new LineDataSet(valuesTurbojet, "TURBOJET");
            set4.setValueTextColor(getResources().getColor(R.color.white));
            set4.setDrawIcons(false);
//            set4.enableDashedLine(10f, 0f, 0f);
//            set4.enableDashedHighlightLine(10f, 0f, 0f);
            set4.setColor(getResources().getColor(R.color.line_chart_turbojet_color));
//            set4.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set4.setLineWidth(1f);
//            set4.setCircleRadius(3f);
//            set4.setDrawCircleHole(false);
            set4.setValueTextSize(0f);
            set4.setDrawFilled(true);
            set4.setFormLineWidth(1f);
//            set4.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set4.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_turbojet);
                set4.setFillDrawable(drawable);
            } else {
                set4.setFillColor(Color.DKGRAY);
            }
            dataSets.add(set4);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.getDescription().setEnabled(false);
            mChart.invalidate();
        }
        else if(fuel_type.equalsIgnoreCase("POWER 99")){
            set5 = new LineDataSet(valuesPower99, "TURBOJET");
            set5.setValueTextColor(getResources().getColor(R.color.white));
            set5.setDrawIcons(false);
//            set4.enableDashedLine(10f, 0f, 0f);
//            set4.enableDashedHighlightLine(10f, 0f, 0f);
            set5.setColor(getResources().getColor(R.color.line_chart_turbojet_color));
//            set4.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set5.setLineWidth(1f);
//            set4.setCircleRadius(3f);
//            set4.setDrawCircleHole(false);
            set5.setValueTextSize(0f);
            set5.setDrawFilled(true);
            set5.setFormLineWidth(1f);
//            set4.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set5.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_turbojet);
                set5.setFillDrawable(drawable);
            } else {
                set5.setFillColor(Color.DKGRAY);
            }
            dataSets.add(set5);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.getDescription().setEnabled(false);
            mChart.invalidate();
        }
        else if(fuel_type.equalsIgnoreCase("ALL")){
            set1 = new LineDataSet(valuesHSD, "HSD");
            set1.setValueTextColor(getResources().getColor(R.color.white));
            set1.setDrawIcons(false);
//            set1.enableDashedLine(10f, 0f, 0f);
//            set1.enableDashedHighlightLine(10f, 0f, 0f);
            set1.setColor(getResources().getColor(R.color.line_chart_hsd_color));
//            set1.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set1.setLineWidth(1f);
//            set1.setCircleRadius(3f);
//            set1.setDrawCircleHole(false);
            set1.setValueTextSize(0f);
            set1.setDrawFilled(true);
            set1.setFormLineWidth(1f);
//            set1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set1.setFormSize(15.f);

            set2 = new LineDataSet(valuesMS, "MS");
            set2.setValueTextColor(getResources().getColor(R.color.white));
            set2.setDrawIcons(false);
//            set2.enableDashedLine(10f, 0f, 0f);
//            set2.enableDashedHighlightLine(10f, 0f, 0f);
            set2.setColor(getResources().getColor(R.color.line_chart_ms_color));
//            set2.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set2.setLineWidth(1f);
//            set2.setCircleRadius(3f);
//            set2.setDrawCircleHole(false);
            set2.setValueTextSize(0f);
            set2.setDrawFilled(true);
            set2.setFormLineWidth(1f);
//            set2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set2.setFormSize(15.f);

            set3 = new LineDataSet(valuesPower, "POWER");
            set3.setValueTextColor(getResources().getColor(R.color.white));
            set3.setDrawIcons(false);
//            set3.enableDashedLine(10f, 0f, 0f);
//            set3.enableDashedHighlightLine(10f, 0f, 0f);
            set3.setColor(getResources().getColor(R.color.line_chart_power_color));
//            set3.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set3.setLineWidth(1f);
//            set3.setCircleRadius(3f);
//            set3.setDrawCircleHole(false);
            set3.setValueTextSize(0f);
            set3.setDrawFilled(true);
            set3.setFormLineWidth(1f);
//            set3.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set3.setFormSize(15.f);

            set4 = new LineDataSet(valuesTurbojet, "TURBOJET");
//            Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
//            set1.setFillDrawable(drawable1);
            set4.setValueTextColor(getResources().getColor(R.color.white));
            set4.setDrawIcons(false);
//            set4.enableDashedLine(10f, 0f, 0f);
//            set4.enableDashedHighlightLine(10f, 0f, 0f);
            set4.setColor(getResources().getColor(R.color.line_chart_turbojet_color));
//            set4.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set4.setLineWidth(1f);
//            set4.setCircleRadius(3f);
//            set4.setDrawCircleHole(false);
            set4.setValueTextSize(0f);
            set4.setDrawFilled(true);
            set4.setFormLineWidth(1f);
//            set4.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set4.setFormSize(15.f);

             set5 = new LineDataSet(valuesTurbojet, "POWER 99");
//            Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
//            set1.setFillDrawable(drawable1);
            set5.setValueTextColor(getResources().getColor(R.color.white));
            set5.setDrawIcons(false);
//            set4.enableDashedLine(10f, 0f, 0f);
//            set4.enableDashedHighlightLine(10f, 0f, 0f);
            set5.setColor(getResources().getColor(R.color.line_chart_turbojet_color));
//            set4.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            set5.setLineWidth(1f);
//            set4.setCircleRadius(3f);
//            set4.setDrawCircleHole(false);
            set5.setValueTextSize(0f);
            set5.setDrawFilled(true);
            set5.setFormLineWidth(1f);
//            set4.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            set5.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
//                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_hsd);
                set1.setFillDrawable(drawable);
            } else {
                set1.setFillColor(Color.DKGRAY);
            }
            if (Utils.getSDKInt() >= 18) {
//                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg4);
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_ms);
                set2.setFillDrawable(drawable);
            } else {
                set2.setFillColor(Color.DKGRAY);
            }
            if (Utils.getSDKInt() >= 18) {
//                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_power);
                set3.setFillDrawable(drawable);
            } else {
                set3.setFillColor(Color.DKGRAY);
            }
            if (Utils.getSDKInt() >= 18) {
//                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg4);
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_turbojet);
                set4.setFillDrawable(drawable);
            } else {
                set4.setFillColor(Color.DKGRAY);
            }
            if (Utils.getSDKInt() >= 18) {
//                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg4);
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_turbojet);
                set5.setFillDrawable(drawable);
            } else {
                set5.setFillColor(Color.DKGRAY);
            }
            dataSets.add(set1);
            dataSets.add(set2);
            dataSets.add(set3);
            dataSets.add(set4);
            dataSets.add(set5);
            data = new LineData(dataSets);
            mChart.getDescription().setEnabled(false);
            mChart.setData(data);
            mChart.invalidate();
        }

    }
    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.dashboard_container, frag, "LOGIN_USERID");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("LOGIN_USERID");
        fragmentTransaction.commit();

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }



}

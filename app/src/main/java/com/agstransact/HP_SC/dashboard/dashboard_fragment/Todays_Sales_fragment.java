package com.agstransact.HP_SC.dashboard.dashboard_fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Todays_Sales_fragment extends Fragment {
    View rootview;
    private String error="", respCode="",fuel_type="";
    private TextView tv_uptolbl,tv_untilllbl,tv_y_axis;
    private JSONArray catArray,valArray;
    private List<String> todaycategorydata,todayvalueData;
    private BarChart barChart;
    private AlertDialogInterface alertDialogInterface;
    private UniversalDialog universalDialog;
    private int [] colorcode = {R.color.line_chart_hsd_color,R.color.line_chart_ms_color,R.color.line_chart_power_color,R.color.line_chart_turbojet_color};
    private static final float BAR_WIDTH = 0.8f;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.fargment_todays_sales, container, false);
        tv_uptolbl = (TextView) rootview.findViewById(R.id.tv_uptolbl);
        tv_untilllbl = (TextView) rootview.findViewById(R.id.tv_untilllbl);
        barChart = (BarChart) rootview.findViewById(R.id.barchart1);
        tv_y_axis = (TextView) rootview.findViewById(R.id.tv_y_axis);
        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                "UserCustomRole","").equalsIgnoreCase("DEALER")) {
            tv_y_axis.setText("QTY (KL)");
        }else{
            tv_y_axis.setText("QTY (L)");
        }
        callTodaysSale();
        return  rootview;
    }

    private void callTodaysSale() {

        JSONObject json = new JSONObject();
        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }



                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){

                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new TodaysSale().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }
    private class TodaysSale extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public TodaysSale() {
        }

        public TodaysSale(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("DashBoard/GetTodaySales", false, jsonObjects[0], "");
        }


        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }

            if (s != null) {

                try {

                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject jsonObject1 = jsonObject.getJSONObject("Data");
                            catArray = new JSONArray();
                            valArray = new JSONArray();
                            catArray = jsonObject1.getJSONArray("Product");
                            valArray = jsonObject1.getJSONArray("Value");
                            JSONArray timeSlot = jsonObject.getJSONArray("TimeSlot");
                            for (int ts =0;ts<timeSlot.length();ts++){
                                JSONObject timeObj = timeSlot.getJSONObject(ts);
                                tv_untilllbl.setText(timeObj.getString("EndTime"));
                                SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
                                Date dateObj = null;
                                String fromDate = timeObj.getString("FromDate");
                                dateObj = curFormater.parse(fromDate);
                                SimpleDateFormat postFormater = new SimpleDateFormat("DD |MM | YYYY");

                                String nFDate = postFormater.format(dateObj);
                                try {
                                    tv_uptolbl.setText(fromDate.replace("-"," |"));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
//                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("Data"));
//                            catArray =new JSONArray(jsonObject1.getJSONObject("Product"));
//                            valArray = new JSONArray(jsonObject1.getJSONObject("Value"));
                            todaycategorydata = new ArrayList<>();
                            for (int i1 = 0; i1 < catArray.length(); i1++) {
                                todaycategorydata.add(catArray.getString(i1));
                            }
                            todayvalueData = new ArrayList<>();
                            for (int i1 = 0; i1 < valArray.length(); i1++) {
                                todayvalueData.add(valArray.getString(i1));
                            }
                            BarData data = createChartData();
                            configureChartAppearance();
                            prepareChartData(data);
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }

    }
    private BarData createChartData() {

        ArrayList<BarEntry> values1 = new ArrayList<>();
        ArrayList<BarEntry> values2 = new ArrayList<>();
        BarDataSet set1,set2;
        String color="";
        for (int i = 0; i < todayvalueData.size() ; i++) {
//            values1.add(new BarEntry(i, Float.valueOf(todaycategorydata.get(i))));
            values2.add(new BarEntry(i, Float.parseFloat(todayvalueData.get(i))));

//            if(i== 4){
//              co
//
//              lor = "disabled";
//            }
        }
        set1  = new BarDataSet(values1, "Product");
        set2 = new BarDataSet(values2, "Value");
//        set1.getEntryForXValue(0,0)
        ArrayList<Integer> colorList = new ArrayList<>();
        for(int i =0; i<colorcode.length; i++){
            colorList.add(colorcode.length);
        }
//        set1.setValueTextColors(colorList);
//        set1.contains(values1.get(4));
//        set1.setColor(getResources().getColor(R.color.hsdcolor));
//        set2.setColor(getResources().getColor(R.color.orange_logo));
        for(int i = 0; i < todayvalueData.size() ; i++){


        }
//        set1.setValueTextColor(getResources().getColor(R.color.white));
        set2.setValueTextColor(getResources().getColor(R.color.black));
        set1.setColors(new int[]{Color.rgb(31, 64, 120),
                Color.rgb(0, 0, 254),
                Color.rgb(236, 28, 35),
                Color.rgb(5, 1, 3),
                Color.rgb(201, 108, 56)});

        set2.setColors(new int[]{Color.rgb(0, 0, 254),
                Color.rgb(0, 166, 81),
                Color.rgb(236, 28, 35),
                Color.rgb(5, 1, 3),
                Color.rgb(201, 108, 56)});
        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(set1);
        dataSets.add(set2);
        BarData data = new BarData(dataSets);
        return data;
    }
    private void configureChartAppearance() {

        barChart.setPinchZoom(false);
        barChart.setDrawBarShadow(false);
        barChart.setDrawGridBackground(false);
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getAxisLeft().setDrawGridLines(false);
        barChart.getAxisRight().setDrawGridLines(false);
        barChart.getDescription().setEnabled(false);
        barChart.getLegend().setEnabled(false);
        barChart.setFitBars(true);

        ArrayList<String> labels = new ArrayList<>();
        for (int val =0 ; val< todaycategorydata.size(); val ++){
            labels.add(todaycategorydata.get(val));
            Log.e("labels",labels.toString());
        }

//        BarData data = new BarData(labels,getDataSet());
        XAxis xAxis = barChart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(false);
        xAxis.setTextColor(getResources().getColor(R.color.black));
//        xVal.add(String.valueOf(catArray));
//        xAxis.setValueFormatter(todaycategorydata.get(i));
//        xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));
//        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        final YAxis leftAxis = barChart.getAxisLeft();
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f);

        // Add color code arraylist
//        leftAxis.setTextColor(getResources().getColor(R.color.mscolor));


        barChart.getAxisRight().setEnabled(false);

//        barChart.getXAxis().setAxisMinimum(0);
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        barChart.getLegend().setTextColor(getResources().getColor(R.color.black));
//        xAxis.setLabelCount(5);
        int monthsize = catArray.length();
        float f2 = (float) monthsize / 1;
        xAxis.setAxisMaximum(f2);

        List<String> catData = new ArrayList<>();
        for (int j =0;j<catArray.length();j++){
            try {
                catData.add(catArray.getString(j));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        catData.add(" ");
        try {
//            barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(todayvalueData));
            barChart.getXAxis().setValueFormatter(new IndexAxisValueFormatter(catData));
            if(barChart.getXAxis().getLabelCount() == 7||
                    barChart.getXAxis().getLabelCount() == 8||
                    barChart.getXAxis().getLabelCount() == 9){
                barChart.getXAxis().setTextSize(1+ barChart.getXAxis().getLabelCount());
            }
//            barChart.getXAxis().setTextSize(1+barChart.getXAxis().getLabelCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
       /* XAxis xl = new XAxis();
        xl.setValueFormatter(new AxisValueFormatter() {
            @Override
            public int formatValueForManualAxis(char[] formattedValue, AxisValue axisValue) {
                return 0;
            }

            @Override
            public int formatValueForAutoGeneratedAxis(char[] formattedValue, float value, int autoDecimalDigits) {
                return 0;
            }

            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return String.valueOf((int) value);
            }
        });*/

//        todayvalueData.add(" ");

    }
    private void prepareChartData(BarData data) {

        barChart.setData(data);
        barChart.getBarData().setBarWidth(BAR_WIDTH);
        barChart.setFitBars(true);
        barChart.invalidate();
    }


}

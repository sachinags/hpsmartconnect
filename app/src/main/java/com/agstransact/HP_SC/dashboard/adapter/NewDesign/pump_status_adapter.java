package com.agstransact.HP_SC.dashboard.adapter.NewDesign;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;

import java.util.ArrayList;

public class pump_status_adapter extends RecyclerView.Adapter<pump_status_adapter.MyViewHolder> {

    private ArrayList<Pump_model_new> pumpmodel;
    Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_nozzles_lbl,tv_gilbarco_lbl,
                tv_product_lbl,tv_RS_lbl,tv_vol_label,
                tv_Amt_label,tv_LTDate_label;
        ImageView iv_logo1;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.tv_nozzles_lbl = (TextView) itemView.findViewById(R.id.tv_nozzles_lbl);
            this.tv_gilbarco_lbl = (TextView) itemView.findViewById(R.id.tv_gilbarco_lbl);
            this.tv_product_lbl = (TextView) itemView.findViewById(R.id.tv_product_lbl);
            this.tv_RS_lbl = (TextView) itemView.findViewById(R.id.tv_RS_lbl);
            this.tv_vol_label = (TextView) itemView.findViewById(R.id.tv_vol_label);
            this.tv_Amt_label = (TextView) itemView.findViewById(R.id.tv_Amt_label);
            this.tv_LTDate_label = (TextView) itemView.findViewById(R.id.tv_LTDate_label);
            this.iv_logo1 = (ImageView) itemView.findViewById(R.id.iv_logo1);
        }
    }

    public pump_status_adapter(ArrayList<Pump_model_new> pumpmodel, Context context) {
        this.pumpmodel = pumpmodel;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pump_adapter_new, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        TextView tv_nozzles_lbl = holder.tv_nozzles_lbl;
        TextView tv_gilbarco_lbl = holder.tv_gilbarco_lbl;
        TextView tv_product_lbl = holder.tv_product_lbl;
        TextView tv_RS_lbl = holder.tv_RS_lbl;
        TextView tv_vol_label = holder.tv_vol_label;
        TextView tv_Amt_label = holder.tv_Amt_label;
        TextView tv_LTDate_label = holder.tv_LTDate_label;
        ImageView iv_logo1 = holder.iv_logo1;

        try {
            tv_nozzles_lbl.setText("Nozzles: "+pumpmodel.get(listPosition).getNozzles());
            tv_gilbarco_lbl.setText(pumpmodel.get(listPosition).getDUMake()+" "+
                    pumpmodel.get(listPosition).getDUNo()+"|"+
                    pumpmodel.get(listPosition).getPumpNumber()+"|"+
                    pumpmodel.get(listPosition).getNozzleNumber()
                    );
            tv_product_lbl.setText("Product ["+pumpmodel.get(listPosition).getProduct()+"]");
            tv_RS_lbl.setText("Rs."+pumpmodel.get(listPosition).getPrice()+"/Ltr");
            tv_vol_label.setText("Vol.:"+pumpmodel.get(listPosition).getQuantity()+" Ltr");
            tv_Amt_label.setText("Amt.:"+pumpmodel.get(listPosition).getAmount()+" Rs.");
            tv_LTDate_label.setText("LTDate:"+pumpmodel.get(listPosition).getTransactionDateTime());
            tv_nozzles_lbl.setSelected(true);
            tv_gilbarco_lbl.setSelected(true);
            tv_product_lbl.setSelected(true);
            tv_RS_lbl.setSelected(true);
            tv_vol_label.setSelected(true);
            tv_Amt_label.setSelected(true);
            tv_LTDate_label.setSelected(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if(pumpmodel.get(listPosition).getOnline().equalsIgnoreCase("1")){
                iv_logo1.setImageResource(R.drawable.online);
            }else if(pumpmodel.get(listPosition).getOnline().equalsIgnoreCase("2")){
                iv_logo1.setImageResource(R.drawable.notcomm);
            }else if(pumpmodel.get(listPosition).getOnline().equalsIgnoreCase("3")){
                iv_logo1.setImageResource(R.drawable.notinuse);
            }else{
                iv_logo1.setImageResource(R.drawable.offline_new);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return pumpmodel.size();
    }
}

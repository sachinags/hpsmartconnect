package com.agstransact.HP_SC.dashboard.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.adapter.Dialog_search_adapter_multiple;
import com.agstransact.HP_SC.dashboard.adapter.Dialog_search_adapter_multiple_new;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.model.Model;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class fragment_filter_textview_multiple_new extends Fragment implements  AlertDialogInterface,Dialog_search_adapter_multiple_new.ItemClickListener,View.OnClickListener  {
    private View view;
    View loginFormView;
    TextView tv_select_zone_value,tv_select_region_value,tv_select_sales_area_value,tv_select_RO_value;
//    LinearLayout ll_select_zone_value,ll_select_region_value,ll_select_sales_area_value,ll_select_RO_value;
    LinearLayout tv_select_zone,tv_select_region,tv_select_sales_area,tv_select_RO;
    Button submitBT;
    String error="", respCode="",FilteredCount="";
    String FilteredCountHQ="",FilteredCountZone="",FilteredCountRegion="",FilteredCountSalesArea="";
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    Bundle bundle;
    ArrayList<String> ZoneList;
    ArrayList<String> Selected_zonelist,Selected_region_list,Selected_sales_area_list,Selected_ro_list;
    ArrayList<Model> ZoneList1 = null;
    ArrayList<String> ROCODE_List;
    JSONArray Regionarray,ZoneArray,ROArray,SalesAreaArray;
    ArrayList<String> RegionKey,RegionValue,SalesAreaKey,SalesAreaValue,ROKey,ROValue;
    ArrayList<Model> RegionValue1,SalesAreaValue1,ROValue1;
    public static ArrayList<String> SelecteditemsAll;
    public static ArrayList<String> Selecteditemssingle;
    Dialog_search_adapter_multiple_new search_adapter;
    Dialog_search_adapter_multiple search_adapterRo;
    AlertDialog dialogSelectBranch;
    String SelectedZone= "", SelectedRegion= "", SelectedSalesArea= "", SelectedRO= "";
    String SelectedZoneList= "",SelectedRegionList= "",SelectedSalesAreaList= "",SelectedROList= "";
    Boolean ZoneClicked= false,Regionclicked =false,SalesAreaClicked =false,ROClicked = false;
    String filteredRO;
    public static String stringselecteditem ="";
    Boolean isScrolling = false;
    int currentItems, totalItems, scrollOutItems;
    Boolean isScrollingsa = false;
    int currentItemssa, totalItemssa, scrollOutItemssa;
    private int currentPage = 0;
    int first=1,last=2;
    int firstsa=1,lastsa=2;
    RecyclerView recyclerViewro;
    LinearLayoutManager mLayoutManager;
    ArrayList<String> RO_temp1;
    int ROListsize = 0;
    int ROTempListsize = 0;
    boolean scrolledend = false;
    ArrayList<String> RO_temp1sa;
    int ROListsizesa = 0;
    int ROTempListsizesa = 0;
    boolean scrolledendsa = false;
    public static boolean checkscroll = true;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_filter_textview, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
        toolbarTV.setText(getResources().getString(R.string.filter));
        setHasOptionsMenu(true);
        bundle = getArguments();

        tv_select_zone_value = view.findViewById(R.id.tv_select_zone_value);
        tv_select_region_value = view.findViewById(R.id.tv_select_region_value);
        tv_select_sales_area_value = view.findViewById(R.id.tv_select_sales_area_value);
        tv_select_RO_value = view.findViewById(R.id.tv_select_RO_value);
        tv_select_zone = view.findViewById(R.id.ll_select_zone_value);
        tv_select_region = view.findViewById(R.id.ll_select_region_value);
        tv_select_sales_area = view.findViewById(R.id.ll_select_sales_area_value);
        tv_select_RO = view.findViewById(R.id.ll_select_RO_value);
        tv_select_RO_value.setSelected(true);
        submitBT = view.findViewById(R.id.submitBT);

        if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLHQ")){
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "Selectdzone","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "Selectdzone","").equalsIgnoreCase("Select Zone"))){
                tv_select_zone_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Selectdzone",""));
                CallRegion();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }
            else{
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                submitBT.setEnabled(false);
            }

            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("Select Region"))){
                tv_select_region_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdRegion",""));
                CallSales_Area();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }

            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("Select Sales Area"))){
                tv_select_sales_area_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea",""));
                CallRO();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                } else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }

            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);
            }

            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);

            }

            tv_select_zone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    populateROList();
                    ZoneClicked=true;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = false;
                    selectBranch();
                }

            });
            tv_select_region_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = true;
                    SalesAreaClicked = false;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_sales_area_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = true;
                    ROClicked = false;
//                    selectBranch();
                    scrolledendsa = false;
                    try {
                        firstsa = 1;
                        lastsa = 2;
                        ROTempListsizesa = 0;
                        RO_temp1sa.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectBranch();
                }
            });
            tv_select_RO_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = true;
//                    selectBranch();
                    scrolledend = false;
                    try {
                        first = 1;
                        last = 2;
                        ROTempListsize = 0;
                        RO_temp1.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectBranchro();
                    try {
                        if (ConstantDeclaration.gifProgressDialog.isShowing())
                            ConstantDeclaration.gifProgressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });

            tv_select_zone_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Zone")){
                        tv_select_sales_area_value.setEnabled(false);
                        tv_select_RO_value.setEnabled(false);
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitBT.setEnabled(false);
                    }else{
                        tv_select_region_value.setEnabled(true);
                        tv_select_sales_area_value.setEnabled(true);
                        tv_select_RO_value.setEnabled(true);
                        CallRegion();
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }
                    tv_select_zone_value.setSelected(true);


                }
            });
            tv_select_region_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Region")){
                        tv_select_sales_area_value.setEnabled(false);
                        tv_select_RO_value.setEnabled(false);
                    }else{
                        tv_select_sales_area_value.setEnabled(true);
                        tv_select_RO_value.setEnabled(true);
                        CallSales_Area();
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }
                    tv_select_region_value.setSelected(true);

                }
            });
            tv_select_sales_area_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Sales Area")){
                        tv_select_RO_value.setEnabled(false);
                    }else{
                        tv_select_RO_value.setEnabled(true);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                        CallRO();
                    }
                    tv_select_sales_area_value.setSelected(true);

                }
            });
            tv_select_RO_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select RO")){
//                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
//                        submitBT.setEnabled(false);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }else{
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                        submitBT.setEnabled(true);
                    }
                    tv_select_RO_value.setSelected(true);

                }
            });

            if(tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                    tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")){
                tv_select_region_value.setEnabled(false);
            }
            if(tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                    tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
                tv_select_sales_area_value.setEnabled(false);
            }
            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")&&
                    tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")){
                tv_select_RO_value.setEnabled(false);
            }

        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLZONE")){
            tv_select_region_value.setEnabled(true);
            tv_select_zone.setVisibility(View.GONE);
            tv_select_zone_value.setVisibility(View.GONE);
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("Select Region"))){
                tv_select_region_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdRegion",""));
                CallSales_Area();

                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }
            else{
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                submitBT.setEnabled(false);
            }

            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("Select Sales Area"))){
                tv_select_sales_area_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea",""));
                CallRO();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }

            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);
            }

            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);

            }

            tv_select_region_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    populateROList();
                    ZoneClicked=false;
                    Regionclicked = true;
                    SalesAreaClicked = false;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_sales_area_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = true;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_RO_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = true;
                    scrolledend = false;
                    try {
                        first = 1;
                        last = 2;
                        ROTempListsize = 0;
                        RO_temp1.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectBranchro();
//                    selectBranch();
                }
            });
            tv_select_region_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Region")){
                        tv_select_sales_area_value.setEnabled(false);
                        tv_select_RO_value.setEnabled(false);
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitBT.setEnabled(false);
                    }else{
                        tv_select_sales_area_value.setEnabled(true);
                        tv_select_RO_value.setEnabled(true);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            tv_select_sales_area_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Sales Area")){
                        tv_select_RO_value.setEnabled(false);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }else{
                        tv_select_RO_value.setEnabled(true);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            tv_select_RO_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select RO")){
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }else{
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                        submitBT.setEnabled(true);
                    }

                }
            });


            if(tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                    tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
                tv_select_sales_area_value.setEnabled(false);
            }
            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")&&
                    tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                    tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
                tv_select_RO_value.setEnabled(false);
            }
        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLREGION")){
            tv_select_region_value.setEnabled(true);
            tv_select_zone.setVisibility(View.GONE);
            tv_select_zone_value.setVisibility(View.GONE);
            tv_select_region.setVisibility(View.GONE);
            tv_select_region_value.setVisibility(View.GONE);
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("Select Sales Area"))){
                tv_select_sales_area_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea",""));
                SelectedSalesArea = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea","");
                CallRO();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);
            }else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);

            }
            tv_select_sales_area_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    populateROList();
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = true;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_RO_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = true;
                    scrolledend = false;
                    try {
                        first = 1;
                        last = 2;
                        ROTempListsize = 0;
                        RO_temp1.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    selectBranchro();
//                    selectBranch();
                }
            });
            tv_select_sales_area_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Sales Area")){
                        tv_select_RO_value.setEnabled(false);
                    }else{
                        tv_select_RO_value.setEnabled(true);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            tv_select_RO_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select RO")){
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }else{
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")&&
                    tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")){
                tv_select_RO_value.setEnabled(false);
            }
        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLSALESAREA")){
            tv_select_RO_value.setEnabled(true);
            tv_select_zone.setVisibility(View.GONE);
            tv_select_zone_value.setVisibility(View.GONE);
            tv_select_region.setVisibility(View.GONE);
            tv_select_region_value.setVisibility(View.GONE);
            tv_select_sales_area.setVisibility(View.GONE);
            tv_select_sales_area_value.setVisibility(View.GONE);
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);

            }
            tv_select_RO_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    populateROList();
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = true;
                    selectBranch();
                }
            });
            tv_select_RO_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select RO")){
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitBT.setEnabled(false);
                    }else{
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                        submitBT.setEnabled(true);
                    }

                }
            });
        }


        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                        "SalectedROCount", FilteredCount);
                if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                        "").equalsIgnoreCase("HPCLHQ")){
                    try {
                        if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")&&
                                tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Selectdzone", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "Zone");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "SalectedROCount", FilteredCountZone);
                        }
                        else if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone") &&
                                !tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")) {
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Selectdzone", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_zone_value.getText().toString()+"-"+
                                            tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "Region");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SalectedROCount", FilteredCountRegion);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SalectedROCount", FilteredCountRegion);

                        }
                        else if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone") &&
                                !tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                !tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name",tv_select_zone_value.getText().toString()+"-"+
                                            tv_select_region_value.getText().toString()+"-"+tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Selectdzone", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "SalesArea");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SalectedROCount", FilteredCountSalesArea);
                        }
                        else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                        }

                        if(!tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Selectdzone", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", filteredRO);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", SelectedRO);
                            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("ALL")){
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "SalectedROCount", FilteredCountSalesArea);
                            }else{
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "SalectedROCount", "1");
                            }

                        }
                        else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                        "").equalsIgnoreCase("HPCLZONE")){
                    try {
                        if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")) {
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "Region");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SalectedROCount", FilteredCountRegion);
                        }
                        else if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                !tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO"))
                        {
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_region_value.getText().toString()+"-"
                                            +tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "SalesArea");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SalectedROCount", FilteredCountSalesArea);
                        }
                        else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                        }
                        if(!tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", filteredRO);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", SelectedRO);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("ALL")){
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "SalectedROCount", FilteredCountSalesArea);
                            }else{
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "SalectedROCount", "1");
                            }
                        }else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    try {
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "SelectdRegion", SelectedRegion);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea", SelectedSalesArea);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FilteredRO", filteredRO);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FilteredROName", SelectedRO);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                        "").equalsIgnoreCase("HPCLREGION")){
                    try {
                        if(!tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO"))
                        {
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "SalesArea");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "SalectedROCount", FilteredCountSalesArea);
                        }
                        else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                        }

                        if(!tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", filteredRO);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", SelectedRO);
                            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("ALL")){
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "SalectedROCount", FilteredCountSalesArea);
                            }else{
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "SalectedROCount", "1");
                            }
                        }else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    try {
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea", SelectedSalesArea);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FilteredRO", filteredRO);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FilteredROName", SelectedRO);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                        "").equalsIgnoreCase("HPCLSALESAREA")){
                    try {
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FilteredRO", filteredRO);
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FilteredROName", SelectedRO);
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "SalectedROCount", "1");
                        if(tv_select_RO_value.getText().toString().equalsIgnoreCase("ALL")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SalectedROCount", UserDataPrefrence.getPreference("HPCL_Preference",getContext(),
                                            "SalectedROSalesArea",""));
                        }else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SalectedROCount", "1");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(bundle.getString("Filter_fragment").equalsIgnoreCase("HomeFragment_old")){
                    //get values from HomeFragment which selected and pass to again HomeFragment to default select.
                    try {
                        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    HomeFragment_old filter_screen = new HomeFragment_old();
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("SalesTrends")){
//                    HomeFragment.dashboardapi = false;
                    try {
                        int backStackId = 0;
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                        getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "dashboardapi", "false");

                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    Fragment_FuelSalesTrends_Float filter_screen = new Fragment_FuelSalesTrends_Float();
//                    Fragment_FuelSalesTrends filter_screen = new Fragment_FuelSalesTrends();
                    txnBundle.putString("FilteredFragment","SalesTrends");
                    txnBundle.putString("Fuel_Type",bundle.getString("Fuel_Type"));
                    txnBundle.putString("Date",bundle.getString("Date"));
                    txnBundle.putString("Date_type",bundle.getString("Date_type"));
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);


                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("Comparative")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    Fragment_ComparativeSales filter_screen = new Fragment_ComparativeSales();
                    txnBundle.putString("FilteredFragment","ComparativeFragment");
                    txnBundle.putString("Date_type",bundle.getString("Date_type"));
                    txnBundle.putString("Month1",bundle.getString("Month1"));
                    txnBundle.putString("Month2",bundle.getString("Month2"));
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentWetInventory filter_screen = new FragmentWetInventory();
                    try {
                        txnBundle.putString("fragment",bundle.getString("fragment"));
                        txnBundle.putString("FilteredFragment","WETInventory");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);
                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("Equipment")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    Fragmnet_Equipment filter_screen = new Fragmnet_Equipment();
                    try {
                        txnBundle.putString("fragment",bundle.getString("fragment"));
                        txnBundle.putString("FilteredFragment","Equipment");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);
                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("RO_SnapShot")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentROSnapsahot filter_screen = new FragmentROSnapsahot();
                    txnBundle.putString("FilteredFragment","RO_SnapShotFragment");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_Price_Status")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentPriceExceptionList filter_screen = new FragmentPriceExceptionList();
                    txnBundle.putString("FilteredFragment","LD_Price_Status");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_ROConnectivity_Status")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentROConnectivity filter_screen = new FragmentROConnectivity();
                    txnBundle.putString("FilteredFragment","LD_ROConnectivity_Status");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_Critical_Stcock")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentCriticalStockList filter_screen = new FragmentCriticalStockList();
                    txnBundle.putString("FilteredFragment","LD_Critical_Stcock");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_Nano_status")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentNanoStatus filter_screen = new FragmentNanoStatus();
                    txnBundle.putString("FilteredFragment","LD_Nano_status");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_Pump_MR")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    PumpMRFragment filter_screen = new PumpMRFragment();
                    txnBundle.putString("FilteredFragment","LD_Pump_MR");
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_Pump_MR_MA")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    PumpMRMAFragment filter_screen = new PumpMRMAFragment();
                    txnBundle.putString("FilteredFragment","LD_Pump_MR_MA");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
            }
        });
        tv_select_zone_value.setSelected(true);
        tv_select_region_value.setSelected(true);
        tv_select_sales_area_value.setSelected(true);
        tv_select_RO_value.setSelected(true);
//        try {
//            recyclerViewro.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                @Override
//                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                    super.onScrollStateChanged(recyclerView, newState);
//                    if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
//                    {
//                        isScrolling = true;
//                        Log.e("scrolled",isScrolling.toString());
//
//                    }
//                }
//
//                @Override
//                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                    super.onScrolled(recyclerView, dx, dy);
//                    currentItems = mLayoutManager.getChildCount();
//                    totalItems = mLayoutManager.getItemCount();
//                    scrollOutItems = mLayoutManager.findFirstVisibleItemPosition();
//
//                    if(isScrolling && (currentItems + scrollOutItems == totalItems))
//                    {
//                        isScrolling = false;
//                        Log.e("scrolled",isScrolling.toString());
//                        getData(first,last);
//
//                    }
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return view;
    }

    @Override
    public void onItemClick(String selectedBranch) {
//        if(ZoneClicked){
//            tv_select_zone_value.setText(selectedBranch);
//            SelectedZone = selectedBranch;
//            tv_select_region_value.setText("Select Region");
//            tv_select_sales_area_value.setText("Select Sales Area");
//            tv_select_RO_value.setText("Select RO");
//            tv_select_sales_area_value.setEnabled(false);
//            tv_select_RO_value.setEnabled(false);
//            CallRegion();
//        }
//        else if(Regionclicked){
//            tv_select_region_value.setText(selectedBranch);
//            SelectedRegion = selectedBranch;
//            tv_select_sales_area_value.setText("Select Sales Area");
//            tv_select_RO_value.setText("Select RO");
//            tv_select_sales_area_value.setEnabled(true);
//            tv_select_RO_value.setEnabled(false);
////            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
////                    "SelectdRegion", SelectedRegion);
//            CallSales_Area();
//        }
//        else if(SalesAreaClicked){
//            tv_select_sales_area_value.setText(selectedBranch);
//            SelectedSalesArea = selectedBranch;
//            tv_select_RO_value.setText("Select RO");
//            tv_select_RO_value.setEnabled(true);
////            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
////                    "SelectdSalesArea", SelectedSalesArea);
//            CallRO();
//        }
//        else
            if(ROClicked){
            tv_select_RO_value.setText(selectedBranch);
            SelectedRO = selectedBranch;
            try {
                if(SelectedRO.equalsIgnoreCase("ALL")) {
                    filteredRO = SelectedRO;
                }else{
                    filteredRO = SelectedRO.substring(0,8);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    "FilteredRO", filteredRO);
//            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    "FilteredROName", SelectedRO);
        }
        try {
            if (dialogSelectBranch.isShowing()) {
                dialogSelectBranch.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void methodDone() {

    }

    @Override
    public void methodCancel() {

    }

    @Override
    public void onClick(View v) {

    }

    private void selectBranch() {


        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        loginFormView = getLayoutInflater().inflate(R.layout.dialog_recyclerview_multiple, null);
        loginFormView = getLayoutInflater().inflate(R.layout.dialog_recyclerview_multiple_new, null);
        // Set above view in alert dialog.
        builder.setView(loginFormView);

        RecyclerView recyclerView;
        NestedScrollView scroll_summary;
        TextView title;
        Button btn_send;
        recyclerView = (RecyclerView) loginFormView.findViewById(R.id.rv_filter_list);
        scroll_summary = (NestedScrollView) loginFormView.findViewById(R.id.scroll_summary);
        title = (TextView) loginFormView.findViewById(R.id.title);
        btn_send = (Button) loginFormView.findViewById(R.id.btn_send);
        Model tcnHistoryObj = null;
        try {

            final LinearLayoutManager mLayoutManager;
            mLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLHQ")){
                if(ZoneClicked){
//                    try {
//                        if (ConstantDeclaration.gifProgressDialog != null) {
//                            if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
//                                ConstantDeclaration.showGifProgressDialog(getActivity());
//                            }
//                        } else {
//                            ConstantDeclaration.showGifProgressDialog(getActivity());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    title.setText("Select Zone");
                    search_adapter = new Dialog_search_adapter_multiple_new(ZoneList, getContext(), fragment_filter_textview_multiple_new.this,"ZONE");
                    recyclerView.setAdapter(search_adapter);
                    tv_select_region_value.setText("Select Region");
                    tv_select_sales_area_value.setText("Select Sales Area");
                    tv_select_RO_value.setText("Select RO");
                    tv_select_sales_area_value.setEnabled(false);
                    tv_select_sales_area_value.setClickable(false);
                    tv_select_sales_area_value.setFocusable(false);
                    tv_select_RO_value.setClickable(false);
                    tv_select_RO_value.setFocusable(false);
                    tv_select_RO_value.setEnabled(false);
//                    try {
//                        if (ConstantDeclaration.gifProgressDialog.isShowing())
//                            ConstantDeclaration.gifProgressDialog.dismiss();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    }
                else if(Regionclicked){
                    title.setText("Select Region");
                    search_adapter = new Dialog_search_adapter_multiple_new(RegionValue, getContext(), fragment_filter_textview_multiple_new.this,"ZONE");
                    recyclerView.setAdapter(search_adapter);
                    tv_select_sales_area_value.setText("Select Sales Area");
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_sales_area_value.setEnabled(true);
//                    tv_select_RO_value.setEnabled(false);
                    tv_select_sales_area_value.setEnabled(true);
                    tv_select_sales_area_value.setClickable(true);
                    tv_select_sales_area_value.setFocusable(true);
                    tv_select_RO_value.setClickable(false);
                    tv_select_RO_value.setFocusable(false);
                    tv_select_RO_value.setEnabled(false);
                }
                else if(SalesAreaClicked){
                    title.setText("Select Sales Area");
                    search_adapter = new Dialog_search_adapter_multiple_new(SalesAreaValue, getContext(), fragment_filter_textview_multiple_new.this,"SALESAREA");
                    recyclerView.setAdapter(search_adapter);
//                    RO_temp1sa  = new ArrayList<>();
//                    ROListsizesa =SalesAreaValue.size();
//                    Log.d("SalesAreaSize", String.valueOf(SalesAreaValue.size()));
//                    if(ROListsizesa <= 100){
//                        for (int i = 0 ; i<SalesAreaValue.size();i++){
//                            RO_temp1sa.add(SalesAreaValue.get(i)) ;
//                            search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1sa, getContext(), fragment_filter_textview_multiple_new.this,"SALESAREA");
//                            recyclerView.setAdapter(search_adapter);
//                        }
//                    }
//                    else{
//                        for (int i = 0 ; i<100;i++){
//                            RO_temp1sa.add(SalesAreaValue.get(i)) ;
//                        }
//                        search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1sa, getContext(), fragment_filter_textview_multiple_new.this,"SALESAREA");
//                        recyclerView.setAdapter(search_adapter);
//                        recyclerView.setOnScrollListener(new RecyclerView.OnScrollListener() {
//                            @Override
//                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
//                                {
//                                    isScrollingsa = true;
//                                    checkscroll = true;
//                                    Log.e("scrolled",isScrollingsa.toString());
//
//                                }
//
//                            }
//
//                            @Override
//                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                                super.onScrolled(recyclerView, dx, dy);
//                                currentItemssa = mLayoutManager.getChildCount();
//                                totalItemssa = mLayoutManager.getItemCount();
//                                scrollOutItemssa = mLayoutManager.findFirstVisibleItemPosition();
//                                if(isScrollingsa && (currentItemssa + scrollOutItemssa == totalItemssa))
//                                {
//                                    if(scrolledendsa){
//
//                                    }else{
//                                        isScrollingsa = false;
//                                        checkscroll = false;
//                                        Log.e("scrolled",isScrollingsa.toString());
//                                        getDataSalesArea(first,last);
//                                        search_adapter.notifyDataSetChanged();
//                                    }
//
////                                      new Handler().postDelayed(new Runnable() {
////                                                @Override
////                                                public void run() {
////                                                    search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
////                                                    recyclerViewro.setAdapter(search_adapter);
////                                                }
////                                            }, 5000);
//                                }
//                            }
//                        });
//                        search_adapter.notifyDataSetChanged();
//                    }
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_RO_value.setEnabled(true);
                    tv_select_RO_value.setClickable(true);
                    tv_select_RO_value.setFocusable(true);
                    tv_select_RO_value.setEnabled(true);
                }
                else if(ROClicked){
                    title.setText("Select RO");
//                    search_adapterRo = new Dialog_search_adapter_multiple(ROValue, getContext(), (Dialog_search_adapter_multiple.ItemClickListener) fragment_filter_textview_multiple_new.this,"Region");
//                    recyclerView.setAdapter(search_adapterRo);
                    try {
                        if (ConstantDeclaration.gifProgressDialog != null) {
                            if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                                ConstantDeclaration.showGifProgressDialog(getActivity());
                            }
                        } else {
                            ConstantDeclaration.showGifProgressDialog(getActivity());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    search_adapter = new Dialog_search_adapter_multiple_new(ROValue, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                    recyclerView.setAdapter(search_adapter);
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_RO_value.setEnabled(true);
                    tv_select_RO_value.setClickable(true);
                    tv_select_RO_value.setFocusable(true);
                    tv_select_RO_value.setEnabled(true);
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLZONE")){
                if(Regionclicked){
                    title.setText("Select Region");
                    search_adapter = new Dialog_search_adapter_multiple_new(ZoneList, getContext(), fragment_filter_textview_multiple_new.this,"Zone");
                    recyclerView.setAdapter(search_adapter);
                    tv_select_sales_area_value.setText("Select Sales Area");
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_sales_area_value.setEnabled(true);
//                    tv_select_RO_value.setEnabled(false);
                    tv_select_sales_area_value.setEnabled(true);
                    tv_select_sales_area_value.setClickable(true);
                    tv_select_sales_area_value.setFocusable(true);
                    tv_select_RO_value.setClickable(false);
                    tv_select_RO_value.setFocusable(false);
                    tv_select_RO_value.setEnabled(false);
                }
                else if(SalesAreaClicked){
                    title.setText("Select Sales Area");
                    search_adapter = new Dialog_search_adapter_multiple_new(SalesAreaValue, getContext(), fragment_filter_textview_multiple_new.this,"SalesArea");
                    recyclerView.setAdapter(search_adapter);
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_RO_value.setEnabled(true);
                    tv_select_RO_value.setClickable(true);
                    tv_select_RO_value.setFocusable(true);
                    tv_select_RO_value.setEnabled(true);
                }
                else if(ROClicked){
                    title.setText("Select RO");
//                    try {
//                        if (ConstantDeclaration.gifProgressDialog != null) {
//                            if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
//                                ConstantDeclaration.showGifProgressDialog(getActivity());
//                            }
//                        } else {
//                            ConstantDeclaration.showGifProgressDialog(getActivity());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    search_adapter = new Dialog_search_adapter_multiple_new(ROValue, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                    recyclerView.setAdapter(search_adapter);
//                    try {
//                        if (ConstantDeclaration.gifProgressDialog.isShowing())
//                            ConstantDeclaration.gifProgressDialog.dismiss();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLREGION")){
                if(SalesAreaClicked){
                    title.setText("Select Sales Area");
                    search_adapter = new Dialog_search_adapter_multiple_new(ZoneList, getContext(), fragment_filter_textview_multiple_new.this,"SalesArea");
                    recyclerView.setAdapter(search_adapter);
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_RO_value.setEnabled(true);
                    tv_select_RO_value.setClickable(true);
                    tv_select_RO_value.setFocusable(true);
                    tv_select_RO_value.setEnabled(true);
                }
                else if(ROClicked){
                    title.setText("Select RO");
                    search_adapter = new Dialog_search_adapter_multiple_new(ROValue, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                    recyclerView.setAdapter(search_adapter);
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLSALESAREA")){
                if(ROClicked){
                    title.setText("Select RO");
                    search_adapterRo = new Dialog_search_adapter_multiple(ROValue, getContext(), (Dialog_search_adapter_multiple.ItemClickListener) fragment_filter_textview_multiple_new.this);
                    recyclerView.setAdapter(search_adapterRo);

                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if(!ROClicked){
            builder.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                                    "").equalsIgnoreCase("HPCLHQ")){
                                if(ZoneClicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_zonelist = new ArrayList<>();
                                        Selected_zonelist = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        UserDataPrefrence.saveArrayList(Selected_zonelist,getContext(),"Filtered_Zone");
                                        dialogSelectBranch.dismiss();
                                        SelectedZoneList = Selected_zonelist.toString().replace("[","")
                                                .replace("]","").replace(", ",",");;
                                        tv_select_zone_value.setText(SelectedZoneList);
                                        CallRegion();
                                    }
                                    else{
                                        Selected_zonelist = new ArrayList<>();
                                        Selected_zonelist = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        UserDataPrefrence.saveArrayList(Selected_zonelist,getContext(),"Filtered_Zone");
                                        dialogSelectBranch.dismiss();
                                        SelectedZoneList = Selected_zonelist.toString().replace("[","").
                                                replace("]","").replace(", ",",");
                                        tv_select_zone_value.setText(SelectedZoneList);
                                        CallRegion();
                                    }                                    }
                                else if(Regionclicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_region_list = new ArrayList<>();
                                        Selected_region_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        UserDataPrefrence.saveArrayList(Selected_region_list,getContext(),"Filtered_Region");
                                        dialogSelectBranch.dismiss();
                                        SelectedRegionList = Selected_region_list.toString().replace("[","")
                                                .replace("]","").replace(", ",",");;
                                        tv_select_region_value.setText(SelectedRegionList);
                                        CallSales_Area();

                                    }else{
                                        Selected_region_list = new ArrayList<>();
                                        Selected_region_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        UserDataPrefrence.saveArrayList(Selected_region_list,getContext(),"Filtered_Region");
                                        dialogSelectBranch.dismiss();
                                        SelectedRegionList = Selected_region_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_region_value.setText(SelectedRegionList);
                                        CallSales_Area();
                                    }
                                }
                                else if(SalesAreaClicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        UserDataPrefrence.saveArrayList(Selected_region_list,getContext(),"Filtered_SalesArea");
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().
                                                replace("[","").replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }else{
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        UserDataPrefrence.saveArrayList(Selected_region_list,getContext(),"Filtered_SalesArea");
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().
                                                replace("[","").replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }
                                }
//                                    else if(ROClicked){
//                                        if(stringselecteditem.equalsIgnoreCase("ALL")){
//                                            Selected_ro_list = new ArrayList<>();
//                                            Selected_ro_list = Dialog_search_adapter_multiple_new.SelectedAllList;
//                                            dialogSelectBranch.dismiss();
//                                            SelectedROList = Selected_ro_list.toString().replace("[","");
//                                            tv_select_RO_value.setText(SelectedROList);
//                                        }else{
//                                            Selected_ro_list = new ArrayList<>();
//                                            Selected_ro_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
//                                            dialogSelectBranch.dismiss();
//                                            SelectedROList = Selected_ro_list.toString().replace("[","");
//                                            tv_select_RO_value.setText(SelectedROList);
//                                        }
//
//                                    }
                            }
                            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                                    "").equalsIgnoreCase("HPCLZONE")){
                                if(Regionclicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_region_list = new ArrayList<>();
                                        Selected_region_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        dialogSelectBranch.dismiss();
                                        SelectedRegionList = Selected_region_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_region_value.setText(SelectedRegionList);
                                        CallSales_Area();

                                    }else{
                                        Selected_region_list = new ArrayList<>();
                                        Selected_region_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        dialogSelectBranch.dismiss();
                                        SelectedRegionList = Selected_region_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_region_value.setText(SelectedRegionList);
                                        CallSales_Area();
                                    }
                                }
                                else if(SalesAreaClicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }
                                    else{
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }
                                }
//                                    else if(ROClicked){
//                                        if(stringselecteditem.equalsIgnoreCase("ALL")){
//                                            Selected_ro_list = new ArrayList<>();
//                                            Selected_ro_list = Dialog_search_adapter_multiple_new.SelectedAllList;
//                                            dialogSelectBranch.dismiss();
//                                            SelectedROList = Selected_ro_list.toString().replace("[","");
//                                            tv_select_RO_value.setText(SelectedROList);
//                                        }else{
//                                            Selected_ro_list = new ArrayList<>();
//                                            Selected_ro_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
//                                            dialogSelectBranch.dismiss();
//                                            SelectedROList = Selected_ro_list.toString().replace("[","");
//                                            tv_select_RO_value.setText(SelectedROList);
//                                        }
//
//                                    }
                            }
                            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                                    "").equalsIgnoreCase("HPCLREGION")){
                                if(SalesAreaClicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }else{
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().
                                                replace("[","").replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }
                                }
//                                    else if(ROClicked){
//                                        if(stringselecteditem.equalsIgnoreCase("ALL")){
//                                            Selected_ro_list = new ArrayList<>();
//                                            Selected_ro_list = Dialog_search_adapter_multiple_new.SelectedAllList;
//                                            dialogSelectBranch.dismiss();
//                                            SelectedROList = Selected_ro_list.toString().replace("[","");
//                                            tv_select_RO_value.setText(SelectedROList);
//                                        }else{
//                                            Selected_ro_list = new ArrayList<>();
//                                            Selected_ro_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
//                                            dialogSelectBranch.dismiss();
//                                            SelectedROList = Selected_ro_list.toString().replace("[","");
//                                            tv_select_RO_value.setText(SelectedROList);
//                                        }
//
//                                    }
                            }
                            tv_select_zone_value.setSelected(true);
                            tv_select_region_value.setSelected(true);
                            tv_select_sales_area_value.setSelected(true);
                            tv_select_RO_value.setSelected(true);

                        }
                    });

            builder.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialogSelectBranch.dismiss();

                        }
                    });
        }
        else{

        }
        builder.setView(loginFormView);
        dialogSelectBranch = builder.create();// show it
        dialogSelectBranch.show();
    }

    private void selectBranchro() {


        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
//        loginFormView = getLayoutInflater().inflate(R.layout.dialog_recyclerview_multiple, null);
        loginFormView = getLayoutInflater().inflate(R.layout.ro_multiple_pagination_new, null);
        // Set above view in alert dialog.
        builder.setView(loginFormView);

//        RecyclerView recyclerView;
        TextView title;
        Button btn_send;
        ProgressBar progressBar;
        recyclerViewro = (RecyclerView) loginFormView.findViewById(R.id.rv_filter_list);
        title = (TextView) loginFormView.findViewById(R.id.title);
        btn_send = (Button) loginFormView.findViewById(R.id.btn_send);
        progressBar = (ProgressBar) loginFormView.findViewById(R.id.progressbar);
        Model tcnHistoryObj = null;
        try {

//            final LinearLayoutManager mLayoutManager;
            mLayoutManager = new LinearLayoutManager(getContext());
            recyclerViewro.setLayoutManager(mLayoutManager);
            recyclerViewro.setItemAnimator(new DefaultItemAnimator());
            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLHQ")){
                if(ZoneClicked){
//                    try {
//                        if (ConstantDeclaration.gifProgressDialog != null) {
//                            if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
//                                ConstantDeclaration.showGifProgressDialog(getActivity());
//                            }
//                        } else {
//                            ConstantDeclaration.showGifProgressDialog(getActivity());
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    title.setText("Select Zone");
                    search_adapter = new Dialog_search_adapter_multiple_new(ZoneList, getContext(), fragment_filter_textview_multiple_new.this,"ZONE");
                    recyclerViewro.setAdapter(search_adapter);
                    tv_select_region_value.setText("Select Region");
                    tv_select_sales_area_value.setText("Select Sales Area");
                    tv_select_RO_value.setText("Select RO");
                    tv_select_sales_area_value.setEnabled(false);
                    tv_select_sales_area_value.setClickable(false);
                    tv_select_sales_area_value.setFocusable(false);
                    tv_select_RO_value.setClickable(false);
                    tv_select_RO_value.setFocusable(false);
                    tv_select_RO_value.setEnabled(false);
//                    try {
//                        if (ConstantDeclaration.gifProgressDialog.isShowing())
//                            ConstantDeclaration.gifProgressDialog.dismiss();
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
                else if(Regionclicked){
                    title.setText("Select Region");
                    search_adapter = new Dialog_search_adapter_multiple_new(RegionValue, getContext(), fragment_filter_textview_multiple_new.this,"ZONE");
                    recyclerViewro.setAdapter(search_adapter);
                    tv_select_sales_area_value.setText("Select Sales Area");
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_sales_area_value.setEnabled(true);
//                    tv_select_RO_value.setEnabled(false);
                    tv_select_sales_area_value.setEnabled(true);
                    tv_select_sales_area_value.setClickable(true);
                    tv_select_sales_area_value.setFocusable(true);
                    tv_select_RO_value.setClickable(false);
                    tv_select_RO_value.setFocusable(false);
                    tv_select_RO_value.setEnabled(false);
                }
                else if(SalesAreaClicked){
                    title.setText("Select Sales Area");
//                    search_adapter = new Dialog_search_adapter_multiple_new(SalesAreaValue, getContext(), fragment_filter_textview_multiple_new.this,"SALESAREA");
//                    recyclerViewro.setAdapter(search_adapter);
                    RO_temp1sa  = new ArrayList<>();
                    ROListsizesa =SalesAreaValue.size();
                    Log.d("ROValueSize", String.valueOf(SalesAreaValue.size()));
                    if(ROListsizesa <= 100){
                        for (int i = 0 ; i<SalesAreaValue.size();i++){
                            RO_temp1sa.add(SalesAreaValue.get(i)) ;
                            search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1sa, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                            recyclerViewro.setAdapter(search_adapter);
                        }
                    }else{
                        for (int i = 0 ; i<100;i++){
                            RO_temp1sa.add(SalesAreaValue.get(i)) ;
                        }
                        search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1sa, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                        recyclerViewro.setAdapter(search_adapter);
                        recyclerViewro.setOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                                {
                                    isScrollingsa = true;
                                    Log.e("scrolled",isScrolling.toString());

                                }

                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                currentItemssa = mLayoutManager.getChildCount();
                                totalItemssa = mLayoutManager.getItemCount();
                                scrollOutItemssa = mLayoutManager.findFirstVisibleItemPosition();

                                if(isScrollingsa && (currentItemssa + scrollOutItemssa == totalItemssa))
                                {
                                    if(scrolledendsa){

                                    }else{
                                        isScrollingsa = false;
                                        Log.e("scrolled",isScrollingsa.toString());
                                        getDataSalesArea(first,last);
                                        search_adapter.notifyDataSetChanged();
                                    }

//                                      new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
//                                                    recyclerViewro.setAdapter(search_adapter);
//                                                }
//                                            }, 5000);
                                }
                            }
                        });
                        search_adapter.notifyDataSetChanged();
                    }

                    tv_select_RO_value.setText("Select RO");
//                    tv_select_RO_value.setEnabled(true);
                    tv_select_RO_value.setClickable(true);
                    tv_select_RO_value.setFocusable(true);
                    tv_select_RO_value.setEnabled(true);
                }
                else if(ROClicked){
                    title.setText("Select RO");
//                    final ArrayList<String> RO_temp = new ArrayList<>();
                    RO_temp1  = new ArrayList<>();
                    ROListsize =ROValue.size();
                    Log.d("ROValueSize", String.valueOf(ROValue.size()));
                    if(ROListsize <= 100){
                        for (int i = 0 ; i<ROValue.size();i++){
                            RO_temp1.add(ROValue.get(i)) ;
                            search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                            recyclerViewro.setAdapter(search_adapter);
                        }
                    }else{
                        for (int i = 0 ; i<100;i++){
                            RO_temp1.add(ROValue.get(i)) ;
                        }
                        search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                        recyclerViewro.setAdapter(search_adapter);
                        recyclerViewro.setOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                                {
                                    isScrolling = true;
                                    Log.e("scrolled",isScrolling.toString());

                                }

                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                currentItems = mLayoutManager.getChildCount();
                                totalItems = mLayoutManager.getItemCount();
                                scrollOutItems = mLayoutManager.findFirstVisibleItemPosition();

                                if(isScrolling && (currentItems + scrollOutItems == totalItems))
                                {
                                    if(scrolledend){

                                    }else{
                                        isScrolling = false;
                                        Log.e("scrolled",isScrolling.toString());
                                        getData(first,last);
                                        search_adapter.notifyDataSetChanged();
                                    }

//                                      new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
//                                                    recyclerViewro.setAdapter(search_adapter);
//                                                }
//                                            }, 5000);
                                }
                            }
                        });
                        search_adapter.notifyDataSetChanged();
                    }

//                    search_adapter = new Dialog_search_adapter_multiple_new(ROValue, getContext(), fragment_filter_textview_multiple_new.this,"Region");


//                    try {
//                        recyclerViewro.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
//                            @Override
//                            public boolean onInterceptTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
//                                return false;
//                            }
//
//                            @Override
//                            public void onTouchEvent(@NonNull RecyclerView rv, @NonNull MotionEvent e) {
//                                recyclerViewro.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                                    @Override
//                                    public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                                        super.onScrollStateChanged(recyclerView, newState);
//                                        if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
//                                        {
//                                            isScrolling = true;
//                                            Log.e("scrolled",isScrolling.toString());
//
//                                        }
//                                    }
//
//                                    @Override
//                                    public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                                        super.onScrolled(recyclerView, dx, dy);
//                                        currentItems = mLayoutManager.getChildCount();
//                                        totalItems = mLayoutManager.getItemCount();
//                                        scrollOutItems = mLayoutManager.findFirstVisibleItemPosition();
//
//                                        if(isScrolling && (currentItems + scrollOutItems == totalItems))
//                                        {
//                                            isScrolling = false;
//                                            Log.e("scrolled",isScrolling.toString());
//                                            getData(first,last);
//                                            new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
////                                                    getData(first,last);
//                                                    search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
//                                                    recyclerViewro.setAdapter(search_adapter);
//                                                }
//                                            }, 5000);
//
//                                        }
//                                    }
//                                });
//
//                            }
//
//                            @Override
//                            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
//
//                            }
//                        });
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

                    tv_select_RO_value.setText("Select RO");
//                    tv_select_RO_value.setEnabled(true);
                    tv_select_RO_value.setClickable(true);
                    tv_select_RO_value.setFocusable(true);
                    tv_select_RO_value.setEnabled(true);
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLZONE")){
                if(Regionclicked){
                    title.setText("Select Region");
                    search_adapter = new Dialog_search_adapter_multiple_new(ZoneList, getContext(), fragment_filter_textview_multiple_new.this,"Zone");
                    recyclerViewro.setAdapter(search_adapter);
                    tv_select_sales_area_value.setText("Select Sales Area");
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_sales_area_value.setEnabled(true);
//                    tv_select_RO_value.setEnabled(false);
                    tv_select_sales_area_value.setEnabled(true);
                    tv_select_sales_area_value.setClickable(true);
                    tv_select_sales_area_value.setFocusable(true);
                    tv_select_RO_value.setClickable(false);
                    tv_select_RO_value.setFocusable(false);
                    tv_select_RO_value.setEnabled(false);
                }
                else if(SalesAreaClicked){
                    title.setText("Select Sales Area");
                    search_adapter = new Dialog_search_adapter_multiple_new(SalesAreaValue, getContext(), fragment_filter_textview_multiple_new.this,"SalesArea");
                    recyclerViewro.setAdapter(search_adapter);
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_RO_value.setEnabled(true);
                    tv_select_RO_value.setClickable(true);
                    tv_select_RO_value.setFocusable(true);
                    tv_select_RO_value.setEnabled(true);
                }
                else if(ROClicked){
                    title.setText("Select RO");
//                    search_adapter = new Dialog_search_adapter_multiple_new(ROValue, getContext(), fragment_filter_textview_multiple_new.this,"Region");
//                    recyclerViewro.setAdapter(search_adapter);
                    RO_temp1  = new ArrayList<>();
                    ROListsize =ROValue.size();
                    Log.d("ROValueSize", String.valueOf(ROValue.size()));
                    if(ROListsize <= 100){
                        for (int i = 0 ; i<ROValue.size();i++){
                            RO_temp1.add(ROValue.get(i)) ;
                            search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                            recyclerViewro.setAdapter(search_adapter);
                        }
                    }else{
                        for (int i = 0 ; i<100;i++){
                            RO_temp1.add(ROValue.get(i)) ;
                        }
                        search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                        recyclerViewro.setAdapter(search_adapter);
                        recyclerViewro.setOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                                {
                                    isScrolling = true;
                                    Log.e("scrolled",isScrolling.toString());

                                }

                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                currentItems = mLayoutManager.getChildCount();
                                totalItems = mLayoutManager.getItemCount();
                                scrollOutItems = mLayoutManager.findFirstVisibleItemPosition();

                                if(isScrolling && (currentItems + scrollOutItems == totalItems))
                                {
                                    if(scrolledend){

                                    }else{
                                        isScrolling = false;
                                        Log.e("scrolled",isScrolling.toString());
                                        getData(first,last);
                                        search_adapter.notifyDataSetChanged();
                                    }

//                                      new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
//                                                    recyclerViewro.setAdapter(search_adapter);
//                                                }
//                                            }, 5000);
                                }
                            }
                        });
                        search_adapter.notifyDataSetChanged();
                    }

                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLREGION")){
                if(SalesAreaClicked){
                    title.setText("Select Sales Area");
                    search_adapter = new Dialog_search_adapter_multiple_new(ZoneList, getContext(), fragment_filter_textview_multiple_new.this,"SalesArea");
                    recyclerViewro.setAdapter(search_adapter);
                    tv_select_RO_value.setText("Select RO");
//                    tv_select_RO_value.setEnabled(true);
                    tv_select_RO_value.setClickable(true);
                    tv_select_RO_value.setFocusable(true);
                    tv_select_RO_value.setEnabled(true);
                }
                else if(ROClicked){
                    title.setText("Select RO");
//                    search_adapter = new Dialog_search_adapter_multiple_new(ROValue, getContext(), fragment_filter_textview_multiple_new.this,"Region");
//                    recyclerViewro.setAdapter(search_adapter);
                    RO_temp1  = new ArrayList<>();
                    ROListsize =ROValue.size();
                    Log.d("ROValueSize", String.valueOf(ROValue.size()));
                    if(ROListsize <= 100){
                        for (int i = 0 ; i<ROValue.size();i++){
                            RO_temp1.add(ROValue.get(i)) ;
                            search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                            recyclerViewro.setAdapter(search_adapter);
                        }
                    }else{
                        for (int i = 0 ; i<100;i++){
                            RO_temp1.add(ROValue.get(i)) ;
                        }
                        search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                        recyclerViewro.setAdapter(search_adapter);
                        recyclerViewro.setOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                                {
                                    isScrolling = true;
                                    Log.e("scrolled",isScrolling.toString());

                                }

                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                currentItems = mLayoutManager.getChildCount();
                                totalItems = mLayoutManager.getItemCount();
                                scrollOutItems = mLayoutManager.findFirstVisibleItemPosition();

                                if(isScrolling && (currentItems + scrollOutItems == totalItems))
                                {
                                    if(scrolledend){

                                    }else{
                                        isScrolling = false;
                                        Log.e("scrolled",isScrolling.toString());
                                        getData(first,last);
                                        search_adapter.notifyDataSetChanged();
                                    }

//                                      new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
//                                                    recyclerViewro.setAdapter(search_adapter);
//                                                }
//                                            }, 5000);
                                }
                            }
                        });
                        search_adapter.notifyDataSetChanged();
                    }
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLSALESAREA")){
                if(ROClicked){
                    title.setText("Select RO");
//                    search_adapterRo = new Dialog_search_adapter_multiple(ROValue, getContext(), (Dialog_search_adapter_multiple.ItemClickListener) fragment_filter_textview_multiple_new.this);
//                    recyclerViewro.setAdapter(search_adapterRo);
                    RO_temp1  = new ArrayList<>();
                    ROListsize =ROValue.size();
                    Log.d("ROValueSize", String.valueOf(ROValue.size()));
                    if(ROListsize <= 100){
                        for (int i = 0 ; i<ROValue.size();i++){
                            RO_temp1.add(ROValue.get(i)) ;
                            search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                            recyclerViewro.setAdapter(search_adapter);
                        }
                    }else{
                        for (int i = 0 ; i<100;i++){
                            RO_temp1.add(ROValue.get(i)) ;
                        }
                        search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
                        recyclerViewro.setAdapter(search_adapter);
                        recyclerViewro.setOnScrollListener(new RecyclerView.OnScrollListener() {
                            @Override
                            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                                {
                                    isScrolling = true;
                                    Log.e("scrolled",isScrolling.toString());

                                }

                            }

                            @Override
                            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                                super.onScrolled(recyclerView, dx, dy);
                                currentItems = mLayoutManager.getChildCount();
                                totalItems = mLayoutManager.getItemCount();
                                scrollOutItems = mLayoutManager.findFirstVisibleItemPosition();

                                if(isScrolling && (currentItems + scrollOutItems == totalItems))
                                {
                                    if(scrolledend){

                                    }else{
                                        isScrolling = false;
                                        Log.e("scrolled",isScrolling.toString());
                                        getData(first,last);
                                        search_adapter.notifyDataSetChanged();
                                    }

//                                      new Handler().postDelayed(new Runnable() {
//                                                @Override
//                                                public void run() {
//                                                    search_adapter = new Dialog_search_adapter_multiple_new(RO_temp1, getContext(), fragment_filter_textview_multiple_new.this,"Region");
//                                                    recyclerViewro.setAdapter(search_adapter);
//                                                }
//                                            }, 5000);
                                }
                            }
                        });
                        search_adapter.notifyDataSetChanged();
                    }

                }
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        if(!ROClicked){
            builder.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                                    "").equalsIgnoreCase("HPCLHQ")){
                                if(ZoneClicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_zonelist = new ArrayList<>();
                                        Selected_zonelist = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        UserDataPrefrence.saveArrayList(Selected_zonelist,getContext(),"Filtered_Zone");
                                        dialogSelectBranch.dismiss();
                                        SelectedZoneList = Selected_zonelist.toString().replace("[","")
                                                .replace("]","").replace(", ",",");;
                                        tv_select_zone_value.setText(SelectedZoneList);
                                        CallRegion();
                                    }
                                    else{
                                        Selected_zonelist = new ArrayList<>();
                                        Selected_zonelist = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        UserDataPrefrence.saveArrayList(Selected_zonelist,getContext(),"Filtered_Zone");
                                        dialogSelectBranch.dismiss();
                                        SelectedZoneList = Selected_zonelist.toString().replace("[","").
                                                replace("]","").replace(", ",",");
                                        tv_select_zone_value.setText(SelectedZoneList);
                                        CallRegion();
                                    }                                    }
                                else if(Regionclicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_region_list = new ArrayList<>();
                                        Selected_region_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        UserDataPrefrence.saveArrayList(Selected_region_list,getContext(),"Filtered_Region");
                                        dialogSelectBranch.dismiss();
                                        SelectedRegionList = Selected_region_list.toString().replace("[","")
                                                .replace("]","").replace(", ",",");;
                                        tv_select_region_value.setText(SelectedRegionList);
                                        CallSales_Area();

                                    }else{
                                        Selected_region_list = new ArrayList<>();
                                        Selected_region_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        UserDataPrefrence.saveArrayList(Selected_region_list,getContext(),"Filtered_Region");
                                        dialogSelectBranch.dismiss();
                                        SelectedRegionList = Selected_region_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_region_value.setText(SelectedRegionList);
                                        CallSales_Area();
                                    }
                                }
                                else if(SalesAreaClicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        UserDataPrefrence.saveArrayList(Selected_region_list,getContext(),"Filtered_SalesArea");
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().
                                                replace("[","").replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }else{
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        UserDataPrefrence.saveArrayList(Selected_region_list,getContext(),"Filtered_SalesArea");
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().
                                                replace("[","").replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }
                                }
                            }
                            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                                    "").equalsIgnoreCase("HPCLZONE")){
                                if(Regionclicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_region_list = new ArrayList<>();
                                        Selected_region_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        dialogSelectBranch.dismiss();
                                        SelectedRegionList = Selected_region_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_region_value.setText(SelectedRegionList);
                                        CallSales_Area();

                                    }else{
                                        Selected_region_list = new ArrayList<>();
                                        Selected_region_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        dialogSelectBranch.dismiss();
                                        SelectedRegionList = Selected_region_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_region_value.setText(SelectedRegionList);
                                        CallSales_Area();
                                    }
                                }
                                else if(SalesAreaClicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }
                                    else{
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }
                                }
                            }
                            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                                    "").equalsIgnoreCase("HPCLREGION")){
                                if(SalesAreaClicked){
                                    if(stringselecteditem.equalsIgnoreCase("ALL")){
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedAllList;
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().replace("[","").
                                                replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }else{
                                        Selected_sales_area_list = new ArrayList<>();
                                        Selected_sales_area_list = Dialog_search_adapter_multiple_new.SelectedsingleList;
                                        dialogSelectBranch.dismiss();
                                        SelectedSalesAreaList = Selected_sales_area_list.toString().
                                                replace("[","").replace("]","").replace(", ",",");;
                                        tv_select_sales_area_value.setText(SelectedSalesAreaList);
                                        CallRO();
                                    }
                                }
                            }
                            tv_select_zone_value.setSelected(true);
                            tv_select_region_value.setSelected(true);
                            tv_select_sales_area_value.setSelected(true);
                            tv_select_RO_value.setSelected(true);

                        }
                    });

            builder.setNegativeButton(
                    "Cancel",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which)
                        {
                            dialogSelectBranch.dismiss();

                        }
                    });
        }

        else{

        }
        builder.setView(loginFormView);
        dialogSelectBranch = builder.create();// show it
        dialogSelectBranch.show();
    }


    private void CallRegion(){
        try {
            JSONObject json = new JSONObject();
//                String[] mStringArray = new String[Selected_zonelist.size()];
//                mStringArray = Selected_zonelist.toArray(mStringArray);
//                JSONArray jsonObject1 = new JSONArray(mStringArray);
                String[] myArray1 =tv_select_zone_value.getText().toString().split(",");
                JSONArray jsonObject1 = new JSONArray(myArray1);
                json.put("Zone",jsonObject1);
            new AsyncRegion().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncRegion extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncRegion() {
        }

        public AsyncRegion(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("MasterData/GetRegions", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
//                            FilteredCount = str[4];
//                            UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
//                                    "SalectedROCount",FilteredCount);
                            JSONObject jsonObject = new JSONObject(str[2]);
//                            FilteredCount = jsonObject.getString("ROCount");
                            FilteredCountZone = jsonObject.getString("ROCount");
                            Regionarray = new JSONArray(jsonObject.getString("Data"));
                            RegionKey = new ArrayList<String>();
                            RegionValue = new ArrayList<String>();
                            RegionKey.add("ALL");
//                            for (int i1=0; i1<Regionarray.length(); i1++) {
//                                RegionKey.add(Regionarray.getJSONObject(i1).getString("Key"));
//                            }
                            RegionValue.add("ALL");
                            for (int i1=0; i1<Regionarray.length(); i1++) {
                                RegionValue.add(Regionarray.getJSONObject(i1).getString("Region"));
                            }
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void CallSales_Area(){
        try {
            JSONObject json = new JSONObject();
            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLHQ")){
              if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
                if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")){
//                    json.put("Para1",tv_select_zone_value.getText().toString() );
//                    json.put("Para2",tv_select_region_value.getText().toString() );
                    String[] myArray =tv_select_zone_value.getText().toString().split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
//                    Selected_zonelist.add(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"Zone",
//                            ""));
//                    String[] mStringArray = new String[Selected_zonelist.size()];
//                    mStringArray = Selected_zonelist.toArray(mStringArray);
//                String[] mStringArray1 = new String[Selected_region_list.size()];
//                mStringArray1 = Selected_region_list.toArray(mStringArray1);
//                    JSONArray jsonObject1 = new JSONArray(mStringArray);
                    String[] myArray1 =tv_select_region_value.getText().toString().split(",");
                    JSONArray jsonObject2 = new JSONArray(myArray1);
//                JSONArray jsonObject2 = new JSONArray(mStringArray1);
                    json.put("Zone",jsonObject1);
                    json.put("Region", jsonObject2);
                }
            }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLZONE")){
//                json.put("Para1",UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"Zone",
//                        ""));
//                if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
//                    json.put("Para2",tv_select_region_value.getText().toString() );
//                }
                Selected_zonelist = new ArrayList<>();
                Selected_zonelist.add(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"Zone",
                        ""));
                String[] mStringArray = new String[Selected_zonelist.size()];
                mStringArray = Selected_zonelist.toArray(mStringArray);
//                String[] mStringArray1 = new String[Selected_region_list.size()];
//                mStringArray1 = Selected_region_list.toArray(mStringArray1);
                JSONArray jsonObject1 = new JSONArray(mStringArray);

                String[] myArray1 =tv_select_region_value.getText().toString().split(",");
                JSONArray jsonObject2 = new JSONArray(myArray1);
//                JSONArray jsonObject2 = new JSONArray(mStringArray1);
                json.put("Zone",jsonObject1);
                json.put("Region", jsonObject2);


            }
//            if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
//                if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")){
//                    json.put("Para1",tv_select_zone_value.getText().toString() );
//                    json.put("Para2",tv_select_region_value.getText().toString() );
//                }else{
//                    json.put("Para1",tv_select_region_value.getText().toString() );
//                }
//            }
            new AsyncsalesArea().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncsalesArea extends AsyncTask<JSONObject, Void, String>{
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncsalesArea() {
        }

        public AsyncsalesArea(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
//            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetSalesArea", false, jsonObjects[0],"");
            return ClsWebService_hpcl.PostObjecttoken("MasterData/GetSalesArea", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
//                            FilteredCount = str[4];
//                            UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
//                                    "SalectedROCount",FilteredCount);
                            JSONObject jsonObject = new JSONObject(str[2]);
//                            FilteredCount = jsonObject.getString("ROCount");
                            FilteredCountRegion = jsonObject.getString("ROCount");
                            SalesAreaArray = new JSONArray(jsonObject.getString("Data"));
                            SalesAreaKey = new ArrayList<String>();
                            SalesAreaValue = new ArrayList<String>();
//                            SalesAreaKey.add("ALL");
//                            for (int i1=0; i1<SalesAreaArray.length(); i1++) {
//                                SalesAreaKey.add(SalesAreaArray.getJSONObject(i1).getString("Key"));
//                            }
                            SalesAreaValue.add("ALL");
                            for (int i1=0; i1<SalesAreaArray.length(); i1++) {
                                SalesAreaValue.add(SalesAreaArray.getJSONObject(i1).getString("SalesArea"));
                            }
//populate dialog with list
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void CallRO(){
        try {
            JSONObject json = new JSONObject();
            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLHQ")){
//                if(!tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")) {
//                    if (!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")) {
//                        json.put("Para1", tv_select_zone_value.getText().toString());
//                        if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
//                            json.put("Para2", tv_select_region_value.getText().toString());
//                        }
//                        json.put("Para3", tv_select_sales_area_value.getText().toString());
//                    }
//                }
                String[] myArray =tv_select_zone_value.getText().toString().split(",");
                JSONArray jsonObject1 = new JSONArray(myArray);
//                String[] mStringArray = new String[Selected_zonelist.size()];
//                mStringArray = Selected_zonelist.toArray(mStringArray);
                String[] myArray1 =tv_select_region_value.getText().toString().split(",");
                JSONArray jsonObject2 = new JSONArray(myArray1);
                String[] myArray2 =tv_select_sales_area_value.getText().toString().split(",");
                JSONArray jsonObject3 = new JSONArray(myArray2);
                json.put("Zone",jsonObject1);
                json.put("Region", jsonObject2);
                json.put("SalesArea", jsonObject3);

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLZONE")){

                Selected_zonelist = new ArrayList<>();
                Selected_zonelist.add(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"Zone",
                        ""));
                String[] mStringArray = new String[Selected_zonelist.size()];
                mStringArray = Selected_zonelist.toArray(mStringArray);
                String[] myArray1 =tv_select_region_value.getText().toString().split(",");
                JSONArray jsonObject2 = new JSONArray(myArray1);
                String[] myArray2 =tv_select_sales_area_value.getText().toString().split(",");
                JSONArray jsonObject3 = new JSONArray(myArray2);
//                String[] mStringArray1 = new String[Selected_region_list.size()];
//                mStringArray1 = Selected_region_list.toArray(mStringArray1);

//                String[] mStringArray2 = new String[Selected_sales_area_list.size()];
//                mStringArray2 = Selected_sales_area_list.toArray(mStringArray2);
                JSONArray jsonObject1 = new JSONArray(mStringArray);
//                JSONArray jsonObject2 = new JSONArray(mStringArray1);
//                JSONArray jsonObject3 = new JSONArray(mStringArray2);
                json.put("Zone",jsonObject1);
                json.put("Region", jsonObject2);
                json.put("SalesArea", jsonObject3);
            } else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLREGION")){
                Selected_zonelist = new ArrayList<>();
                Selected_zonelist.add(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"Zone",
                        ""));
                Selected_region_list = new ArrayList<>();
                Selected_region_list.add(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"Region",
                        ""));
                String[] mStringArray = new String[Selected_zonelist.size()];
                mStringArray = Selected_zonelist.toArray(mStringArray);
                String[] mStringArray1 = new String[Selected_region_list.size()];
                mStringArray1 = Selected_region_list.toArray(mStringArray1);
                String[] myArray2 =tv_select_sales_area_value.getText().toString().split(",");
                JSONArray jsonObject3 = new JSONArray(myArray2);
//                String[] mStringArray1 = new String[Selected_region_list.size()];
//                mStringArray1 = Selected_region_list.toArray(mStringArray1);
//                String[] mStringArray2 = new String[Selected_sales_area_list.size()];
//                mStringArray2 = Selected_sales_area_list.toArray(mStringArray2);
                JSONArray jsonObject1 = new JSONArray(mStringArray);
                JSONArray jsonObject2 = new JSONArray(mStringArray1);
//                JSONArray jsonObject3 = new JSONArray(mStringArray2);
                json.put("Zone",jsonObject1);
                json.put("Region", jsonObject2);
                json.put("SalesArea", jsonObject3);
//                if(!tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")) {
//                    json.put("Para1", UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"Zone",
//                            ""));
//                    json.put("Para2", UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"Region",
//                            ""));
//                    json.put("Para3", tv_select_sales_area_value.getText().toString());
//                }
            }
//            if(!tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")){
//                if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")){
//                    json.put("Para1",tv_select_zone_value.getText().toString() );
//                    json.put("Para2",tv_select_region_value.getText().toString() );
//                    json.put("Para3",tv_select_sales_area_value.getText().toString() );
//                } else  if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
//                    json.put("Para1",tv_select_region_value.getText().toString() );
//                    json.put("Para2",tv_select_sales_area_value.getText().toString() );
//                }else{
//                    json.put("Para1",tv_select_sales_area_value.getText().toString() );
//                }
//            }

            new AsyncRO().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncRO extends AsyncTask<JSONObject, Void, String>{
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncRO() {
        }

        public AsyncRO(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("MasterData/GetROs", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            FilteredCount = jsonObject.getString("ROCount");
                            FilteredCountSalesArea = jsonObject.getString("ROCount");
                            ROArray = new JSONArray(jsonObject.getString("Data"));
                            ROKey = new ArrayList<String>();
                            ROValue = new ArrayList<String>();
                            ROKey.add("ALL");
                            for (int i1=0; i1<ROArray.length(); i1++) {
                                ROKey.add(ROArray.getJSONObject(i1).getString("ROCode"));
                            }
                            ROValue.add("ALL");
                            for (int i1=0; i1<ROArray.length(); i1++) {
                                ROValue.add(ROArray.getJSONObject(i1).getString("ROName"));
                            }
//                 populate with list
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void populateROList() {
        ZoneList = new ArrayList<>();
        ROCODE_List = new ArrayList<>();
        if(UserDataPrefrence.getArrayList( "ROLIst",getContext()).size() != 1){
            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLHQ")){
//                ZoneList.add("Select Zone");
            }else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLZONE")){
//                ZoneList.add("Select REGION");
            } else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLREGION")){
//                ZoneList.add("Select Sales Area");
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLSALESAREA")){
//                ZoneList.add("Select RO");
            }
            ZoneList.add(("ALL"));
            ZoneList.addAll(UserDataPrefrence.getArrayList( "ROCode",getContext()));
            ROCODE_List.addAll(UserDataPrefrence.getArrayList( "ROLIst",getContext()));

        } else if(UserDataPrefrence.getArrayList( "ROLIst",getContext()).size() == 0){
        } else{
            ZoneList.addAll(UserDataPrefrence.getArrayList( "ROLIst",getContext()));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void CallFilterRequest(JSONObject json, Context context){
        if(UserDataPrefrence.getPreference("HPCL_Preference", context,
                "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                try {
                    json.put("Zone",Selected_zonelist);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    json.put("Region",Selected_region_list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    json.put("SalesArea",Selected_sales_area_list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    json.put("RO",Selected_ro_list);
                } catch (Exception e) {
                    e.printStackTrace();
                }

        }
        else  if(UserDataPrefrence.getPreference("HPCL_Preference", context,
                "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
            try {
                json.put("Region",Selected_region_list);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                json.put("SalesArea",Selected_sales_area_list);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                json.put("RO",Selected_ro_list);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else  if(UserDataPrefrence.getPreference("HPCL_Preference", context,
                "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
            try {
                json.put("SalesArea",Selected_sales_area_list);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                json.put("RO",Selected_ro_list);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",context,
                "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
            try {
                json.put("RO",Selected_ro_list);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
//        try {
//            recyclerViewro.addOnScrollListener(new RecyclerView.OnScrollListener() {
//                @Override
//                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
//                    super.onScrollStateChanged(recyclerView, newState);
//                    if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
//                    {
//                        isScrolling = true;
//                        Log.e("scrolled",isScrolling.toString());
//
//                    }
//                }
//
//                @Override
//                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
//                    super.onScrolled(recyclerView, dx, dy);
//                    currentItems = mLayoutManager.getChildCount();
//                    totalItems = mLayoutManager.getItemCount();
//                    scrollOutItems = mLayoutManager.findFirstVisibleItemPosition();
//
//                    if(isScrolling && (currentItems + scrollOutItems == totalItems))
//                    {
//                        isScrolling = false;
//                        Log.e("scrolled",isScrolling.toString());
//                        getData(first,last);
//
//                    }
//                }
//            });
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onPause() {
        super.onPause();

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void getData(int first1, int last1){
//       RO_temp1  = new ArrayList<>();
       first = first1 +1;
       last = last1 +1;

       int lastfirstvalue = 0 ;
       int lastlastvalue =0;
       int firstvalue =0;
       int lastvalue =0;
       if(RO_temp1.size() == 100){
           ROTempListsize = RO_temp1.size() /100;
       }else{
           ROTempListsize = (RO_temp1.size()+1) /100;
       }
       ROTempListsize = (RO_temp1.size()-1) /99;
       lastfirstvalue =ROValue.size()/100;
       lastlastvalue =ROValue.size();
       if(lastfirstvalue == ROTempListsize){
           lastvalue = lastlastvalue-lastfirstvalue*100;
           firstvalue = lastfirstvalue*100 +lastvalue;
           if(lastvalue > 0){
               for (int i = lastfirstvalue*100 ; i<lastlastvalue;i++){
                   RO_temp1.add(ROValue.get(i)) ;
               }
               scrolledend = true;
           }else{
           }

       }else{
           first1 =first1*100 +1;
           last1 =last1*100;
           for (int i = first1 ; i<last1;i++){
               RO_temp1.add(ROValue.get(i)) ;
           }
           scrolledend = false;
           Log.e("scrolled",isScrolling.toString());
       }

    }
    public void getDataSalesArea(int first1, int last1){
//       RO_temp1  = new ArrayList<>();
        firstsa = first1 +1;
        lastsa = last1 +1;
        int lastfirstvalue = 0 ;
        int lastlastvalue =0;
        int firstvalue =0;
        int lastvalue =0;
        if(RO_temp1sa.size() == 100){
            ROTempListsizesa = RO_temp1sa.size() /100;
        }else{
            ROTempListsizesa = (RO_temp1sa.size()+1) /100;
        }
        ROTempListsizesa = (RO_temp1sa.size()-1) /99;
        lastfirstvalue =SalesAreaValue.size()/100;
        lastlastvalue =SalesAreaValue.size();
        if(lastfirstvalue == ROTempListsizesa){
            lastvalue = lastlastvalue-lastfirstvalue*100;
            firstvalue = lastfirstvalue*100 +lastvalue;
            if(lastvalue > 0){
                for (int i = lastfirstvalue*100 ; i<lastlastvalue;i++){
                    RO_temp1sa.add(SalesAreaValue.get(i)) ;
                }
                scrolledendsa = true;
            }else{
            }

        }else{
            first1 =first1*100 +1;
            last1 =last1*100;
            for (int i = first1 ; i<last1;i++){
                RO_temp1sa.add(SalesAreaValue.get(i)) ;
            }
            scrolledendsa = false;
            Log.e("scrolled",isScrollingsa.toString());
        }

    }

}

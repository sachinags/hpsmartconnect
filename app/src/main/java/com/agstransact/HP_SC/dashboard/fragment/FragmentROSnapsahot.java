package com.agstransact.HP_SC.dashboard.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.RippleImageView;
import com.agstransact.HP_SC.utils.RippleLinearLayout;
import com.agstransact.HP_SC.utils.RippleTextView;
import com.agstransact.HP_SC.utils.UniversalDialog;

public class FragmentROSnapsahot  extends Fragment implements View.OnClickListener {

    private View rootView;
    private ImageView iv_filter,iv_interlock_status,iv_current_price,iv_nozzle_status,iv_du_pump_details;
    private ImageView iv_fuel_sales_trends,iv_fuel_comparative_trends,iv_bnv_fuel_sale,
            iv_bnv_wet_inventory,iv_bnv_dashbord,iv_bnv_ro_snapshot,iv_bnv_equipment;
    RippleLinearLayout fuel_salesLL, wet_inventoryLL, dashboard_LL,ro_snapshotLL,equipmentLL;
    RippleImageView fuel_salesIV, wet_inventoryIV, dashboardIV,ro_snapshotIV,equipmentIV;
    RippleTextView fuels_salesTV,wet_inventoryTV,dashboardTV,ro_snapshot_TV,equipmentTV;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_ro_snapshot,container,false);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
////        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.iv_tlb_additional);
//        toolbarTV.setText(getResources().getString(R.string.ro_snapshot));
        setUpViews();
        fuel_salesLL = (RippleLinearLayout)   rootView.findViewById(R.id.fuel_salesLL);
        wet_inventoryLL = (RippleLinearLayout)rootView.findViewById(R.id.wet_inventoryLL);
        dashboard_LL = (RippleLinearLayout)   rootView.findViewById(R.id.dashboard_LL);
        ro_snapshotLL = (RippleLinearLayout)  rootView.findViewById(R.id.ro_snapshotLL);
        equipmentLL = (RippleLinearLayout)    rootView.findViewById(R.id.equipmentLL);
        fuel_salesIV = (RippleImageView)    rootView.findViewById(R.id.fuel_salesIV);
        wet_inventoryIV = (RippleImageView) rootView.findViewById(R.id.wet_inventoryIV);
        dashboardIV = (RippleImageView)     rootView.findViewById(R.id.dashboardIV);
        ro_snapshotIV = (RippleImageView)   rootView.findViewById(R.id.ro_snapshotIV);
        equipmentIV = (RippleImageView)     rootView.findViewById(R.id.equipmentIV);
        fuels_salesTV = (RippleTextView)    rootView.findViewById(R.id.fuels_salesTV);
        wet_inventoryTV = (RippleTextView)  rootView.findViewById(R.id.wet_inventoryTV);
        dashboardTV = (RippleTextView)      rootView.findViewById(R.id.dashboardTV);
        ro_snapshot_TV = (RippleTextView)   rootView.findViewById(R.id.ro_snapshot_TV);
        equipmentTV = (RippleTextView)      rootView.findViewById(R.id.equipmentTV);
        ro_snapshotIV.setImageResource(R.drawable.ro_snapshot_enabled_new_wt);

        fuel_salesLL.setOnClickListener(this);
        wet_inventoryLL.setOnClickListener(this);
        dashboard_LL.setOnClickListener(this);
        ro_snapshotLL.setOnClickListener(this);
        equipmentLL.setOnClickListener(this);
        fuel_salesIV.setOnClickListener(this);
        wet_inventoryIV.setOnClickListener(this);
        dashboardIV.setOnClickListener(this);
        ro_snapshotIV.setOnClickListener(this);
        equipmentIV.setOnClickListener(this);
        fuels_salesTV.setOnClickListener(this);
        wet_inventoryTV.setOnClickListener(this);
        dashboardTV.setOnClickListener(this);
        ro_snapshot_TV.setOnClickListener(this);
        equipmentTV.setOnClickListener(this);
        return rootView;
    }

    private void setUpViews() {
        iv_bnv_fuel_sale = rootView.findViewById(R.id.iv_bnv_fuel_sale);
        iv_bnv_wet_inventory = rootView.findViewById(R.id.iv_bnv_wet_inventory);
        iv_bnv_dashbord = rootView.findViewById(R.id.iv_bnv_dashbord);
        iv_bnv_ro_snapshot = rootView.findViewById(R.id.iv_bnv_ro_snapshot);
        iv_bnv_equipment = rootView.findViewById(R.id.iv_bnv_equipment);
        iv_filter = (ImageView) rootView.findViewById(R.id.iv_filter);
        iv_interlock_status = (ImageView) rootView.findViewById(R.id.iv_interlock_status);
        iv_current_price = (ImageView) rootView.findViewById(R.id.iv_current_price);
        iv_nozzle_status = (ImageView) rootView.findViewById(R.id.iv_nozzle_status);
        iv_du_pump_details = (ImageView) rootView.findViewById(R.id.iv_du_pump_details);

        iv_filter.setOnClickListener(this);
        iv_interlock_status.setOnClickListener(this);
        iv_current_price.setOnClickListener(this);
        iv_nozzle_status.setOnClickListener(this);
        iv_du_pump_details.setOnClickListener(this);
//        iv_bnv_fuel_sale.setOnClickListener(this);
//        iv_bnv_wet_inventory.setOnClickListener(this);
//        iv_bnv_dashbord.setOnClickListener(this);
//        iv_bnv_ro_snapshot.setOnClickListener(this);
//        iv_bnv_equipment.setOnClickListener(this);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_filter:
//                replaceScreen(new FragmentFilter());
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    txnBundle.putString("Filter_fragment", "RO_SnapShot");
//                    fragment_filter_textview filter_screen = new fragment_filter_textview();
                    fragment_filter_textview_multiple_new filter_screen = new fragment_filter_textview_multiple_new();
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else{
                    UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {

                        }

                        @Override
                        public void methodCancel() {

                        }
                    },
                            "",
                            "No filter....."
                            , getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
                }
                break;

            case R.id.iv_interlock_status:
                replaceScreen(new Fragment_InterlockStatus());
                break;

            case R.id.iv_current_price:
                replaceScreen(new Fragmnet_CurrentPrice());
                break;

            case R.id.iv_nozzle_status:
                replaceScreen(new Fragment_NozzleStatus());
                break;

            case R.id.iv_du_pump_details:
                replaceScreen(new Fragemnt_DUPumpStatus());
                break;
            case R.id.fuel_salesLL:
            case R.id.fuel_salesIV:
            case R.id.fuels_salesTV:
                ActionChangeFrag changeFrag6 = (ActionChangeFrag) getActivity();
                FragmentFuelSales fuelSales = new FragmentFuelSales();
                changeFrag6.addFragment(fuelSales);
                break;

            case R.id.wet_inventoryLL:
            case R.id.wet_inventoryTV:
            case R.id.wet_inventoryIV:
                ActionChangeFrag changeFrag1 = (ActionChangeFrag) getActivity();
                FragmentWetInventory wet_inventory = new FragmentWetInventory();
                changeFrag1.addFragment(wet_inventory);
                break;

            case R.id.ro_snapshotLL:
            case R.id.ro_snapshotIV:
            case R.id.ro_snapshot_TV:
                ActionChangeFrag changeFrag2 = (ActionChangeFrag) getActivity();
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {

                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("Select RO") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "").equalsIgnoreCase("ALL") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("ALL")) {
                        UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                            @Override
                            public void methodDone() {

                            }

                            @Override
                            public void methodCancel() {

                            }
                        },
                                "",
                                "Please Select RO"
                                , getString(R.string.dialog_ok), "");
                        universalDialog.showAlert();
                    } else {
                        ActionChangeFrag changeFrag3 = (ActionChangeFrag) getActivity();
                        FragmentROSnapsahot ro_snapshot = new FragmentROSnapsahot();
                        changeFrag3.addFragment(ro_snapshot);
                    }
                }else{
                    ActionChangeFrag changeFrag3 = (ActionChangeFrag) getActivity();
                    FragmentROSnapsahot ro_snapshot = new FragmentROSnapsahot();
                    changeFrag3.addFragment(ro_snapshot);
                }
                break;
            case R.id.equipmentLL:
            case R.id.equipmentTV:
            case R.id.equipmentIV:
                ActionChangeFrag changeFrag4 = (ActionChangeFrag) getActivity();
                Fragmnet_Equipment equipment = new Fragmnet_Equipment();
                changeFrag4.addFragment(equipment);
                break;
            case R.id.dashboard_LL:
            case R.id.dashboardIV:
            case R.id.dashboardTV:
                ActionChangeFrag changeFrag7 = (ActionChangeFrag) getActivity();
                HomeFragment_old home_Frag = new HomeFragment_old();
                changeFrag7.addFragment(home_Frag);
                break;
        }
    }

    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.dashboard_container, frag, "LOGIN_USERID");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("LOGIN_USERID");
        fragmentTransaction.commit();

    }
}

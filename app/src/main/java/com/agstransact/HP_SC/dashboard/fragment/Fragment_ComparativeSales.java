package com.agstransact.HP_SC.dashboard.fragment;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.Viewport;
import lecho.lib.hellocharts.view.LineChartView;

public class Fragment_ComparativeSales extends Fragment implements View.OnClickListener {

    private View rootView;
    private Spinner spinner_fuel_type;
    private ArrayList<String> fuelList;
    private TextView tv_hour_sales_trends,tv_day_sales_trends,tv_week_sales_trends,tv_month_sales_trends,
            tv_power_comp_sales,tv_submit;
    private TextView ms_sale_value,hsd_sale_value,power_sale_value,turbojet_value,power_nine_value;
    private String fuel_type="",time="",currentDate="",lastmDate= "";
    String error="", respCode="";
    private TextView sp_from_date,sp_to_date;
    private int day,month,year;
    private AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    private DatePickerDialog picker;
    private Calendar calendar,calendar1,calendar2,calendar3;
    private String formattedDate;
    private SimpleDateFormat df;
    private LineChartView lineChartView;
    private ImageView iv_filter;
    private LineChart mChart;
    private ArrayList<String> fmListData = new ArrayList<>();
    private ArrayList<String> smListData = new ArrayList<>();
    private JSONArray fmArray, smArray;
    public static JSONArray arraymonth1,arrayHSD1,arrayMS1,arrayPower1,arrayTurbojet1,arrayPOWER991;
    public static JSONArray arraymonth2,arrayHSD2,arrayMS2,arrayPower2,arrayTurbojet2,arrayPOWER992;
    int biggestmonthmain1,biggest_HSDmain1,biggestMSmain1,biggestPowermain1,biggestTurbojetmain1,biggestPOWER99main1;
    int smallestmonthmain1,smallest_HSDmain1,smallestMSmain1,smallestPowermain1,smallestTurbojetmain1,smallestPOWER99main1;
    int biggestmonthmain2,biggest_HSDmain2,biggestMSmain2,biggestPowermain2,biggestTurbojetmain2,biggestPOWER99main2;
    int smallestmonthmain2,smallest_HSDmain2,smallestMSmain2,smallestPowermain2,smallestTurbojetmain2,smallestPOWER99main2;



    String fromDate = "";
    String toDate = "";
    private TextView tv_calendar_from,tv_calendar_to;
    private ImageView iv_back_date_from,iv_front_date_from,iv_back_date_to,iv_front_date_to;
    private String CurrentformattedDate_from,CurrentformattedDate_to,previousformattedDate_from,previousformattedDate_to;
    String currentdate_from,currentdate_to,previousdate_from,previousdate_to;
    Bundle bundle;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_comparative_sales,container,false);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
        toolbarTV.setText(getResources().getString(R.string.comaprative_trends));
        bundle = getArguments();
        setUpViews();
        try {
            if(bundle.getString("FilteredFragment").equalsIgnoreCase("ComparativeFragment")){
                tv_calendar_from.setText(bundle.getString("Month1"));
                tv_calendar_to.setText(bundle.getString("Month2"));
                tv_hour_sales_trends.setBackground(null);
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));
                if(bundle.getString("Date_type").equalsIgnoreCase("HSD")){
                    tv_hour_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }else if(bundle.getString("Date_type").equalsIgnoreCase("MS")){
                    tv_day_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_day_sales_trends.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }else if(bundle.getString("Date_type").equalsIgnoreCase("Power")){
                    tv_week_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_week_sales_trends.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }else if(bundle.getString("Date_type").equalsIgnoreCase("turbojet")){
                    tv_month_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_month_sales_trends.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }else if(bundle.getString("Date_type").equalsIgnoreCase("Power99")){
                    tv_power_comp_sales.setBackgroundResource(R.drawable.rounded_corner_white_background);
                    tv_power_comp_sales.setTextColor(getResources().getColor(R.color.black));
                    time = bundle.getString("Date_type");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            time = "HSD";
        }
        return rootView;
    }

    private void setUpViews() {
        mChart = rootView.findViewById(R.id.comp_sale_chart);
        mChart.setNoDataText("");
        spinner_fuel_type = rootView.findViewById(R.id.sp_comp_sales_filter);
        tv_hour_sales_trends = rootView.findViewById(R.id.tv_hour_comp_sales);
        tv_day_sales_trends = rootView.findViewById(R.id.tv_day_comp_sales);
        tv_week_sales_trends = rootView.findViewById(R.id.tv_week_comp_sales);
        tv_month_sales_trends = rootView.findViewById(R.id.tv_month_comp_sales);
        ms_sale_value = rootView.findViewById(R.id.ms_sale_value);
        hsd_sale_value = rootView.findViewById(R.id.hsd_sale_value);
        power_sale_value = rootView.findViewById(R.id.power_sale_value);
        power_nine_value = rootView.findViewById(R.id.power_nine_value);
        turbojet_value = rootView.findViewById(R.id.turbojet_value);
        lineChartView = rootView.findViewById(R.id.linechart);
        iv_filter = rootView.findViewById(R.id.iv_filter);
        sp_from_date = rootView.findViewById(R.id.sp_from_date);
        sp_to_date = rootView.findViewById(R.id.sp_to_date);
        tv_power_comp_sales = rootView.findViewById(R.id.tv_power_comp_sales);
        tv_submit = rootView.findViewById(R.id.tv_submit);

        tv_calendar_from = rootView.findViewById(R.id.tv_calendar_from);
        tv_calendar_to = rootView.findViewById(R.id.tv_calendar_to);
        iv_back_date_from = rootView.findViewById(R.id.iv_back_date_from);
        iv_front_date_from = rootView.findViewById(R.id.iv_front_date_from);
        iv_back_date_to = rootView.findViewById(R.id.iv_back_date_to);
        iv_front_date_to = rootView.findViewById(R.id.iv_front_date_to);



        tv_hour_sales_trends.setOnClickListener(this);
        tv_day_sales_trends.setOnClickListener(this);
        tv_week_sales_trends.setOnClickListener(this);
        tv_month_sales_trends.setOnClickListener(this);
        tv_power_comp_sales.setOnClickListener(this);
        sp_to_date.setOnClickListener(this);
        sp_from_date.setOnClickListener(this);
        tv_submit.setOnClickListener(this);
        iv_back_date_from.setOnClickListener(this);
        iv_front_date_from.setOnClickListener(this);
        iv_back_date_to.setOnClickListener(this);
        iv_front_date_to.setOnClickListener(this);


        populateFuelList();
        ShowCurrentDate();
        ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.simple_spinner_item, fuelList);
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner_fuel_type.setAdapter(adapter);
        spinner_fuel_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (fuelList.get(spinner_fuel_type.getSelectedItemPosition()) != null) {
                    fuel_type = fuelList.get(spinner_fuel_type.getSelectedItemPosition());
//                    showChart();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }

        });
        calendar = Calendar.getInstance();
        calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.get(Calendar.MONTH);
        calendar3 = Calendar.getInstance();
        calendar3 = Calendar.getInstance(TimeZone.getDefault());
        calendar3.get(Calendar.MONTH);
        System.out.println("Current time => " + calendar.getTime());
        System.out.println("Current time => " + calendar3.getTime());

//        df = new SimpleDateFormat("dd|MM|yyyy");
        df = new SimpleDateFormat("MMM YYYY");
        CurrentformattedDate_from = df.format(calendar.getTime());
        CurrentformattedDate_to = df.format(calendar3.getTime());
        tv_calendar_from.setText(CurrentformattedDate_from);
        tv_calendar_to.setText(CurrentformattedDate_to);

        currentdate_from = df.format(calendar.getTime());
        currentdate_to = df.format(calendar3.getTime());
//        calendar.get(Calendar.MONTH 3);
        calendar1 = Calendar.getInstance();
        calendar1 = Calendar.getInstance(TimeZone.getDefault());
        calendar1.add(Calendar.MONTH, -3);

        calendar2 = Calendar.getInstance();
        calendar2 = Calendar.getInstance(TimeZone.getDefault());
        calendar2.add(Calendar.MONTH, -3);

        previousdate_from= df.format(calendar1.getTime());
        previousdate_to= df.format(calendar2.getTime());



        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    txnBundle.putString("Filter_fragment", "Comparative");
//                txnBundle.putString("Fuel_Type", fuel_type);
                    txnBundle.putString("Month1", tv_calendar_from.getText().toString());
                    txnBundle.putString("Month2", tv_calendar_to.getText().toString());
                    txnBundle.putString("Date_type", time);
//                    fragment_filter_textview filter_screen = new fragment_filter_textview();
                    fragment_filter_textview_multiple_new filter_screen = new fragment_filter_textview_multiple_new();
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else{
                    UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {

                        }

                        @Override
                        public void methodCancel() {

                        }
                    },
                            "",
                            "No filter....."
                            , getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
                }
            }
        });
    }

    private void ShowCurrentDate() {
        final Calendar cldr2 = Calendar.getInstance();
        day = cldr2.get(Calendar.DAY_OF_MONTH);
        month = cldr2.get(Calendar.MONTH);
        year = cldr2.get(Calendar.YEAR);
        // date picker dialog

        String strCurrentDate = day + "/" + (month+1) + "/" + year;
        String lmDate = day + "/" + (month) + "/" + year;
        SimpleDateFormat currFormater = new SimpleDateFormat("dd/MM/yyyy");
        Date dateObj = null;
        Date lmDateObj = null;
        try {
            lmDateObj = currFormater.parse(lmDate);
            dateObj = currFormater.parse(strCurrentDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SimpleDateFormat postlmFormater = new SimpleDateFormat("MMM YYYY");

        currentDate = postlmFormater.format(dateObj);
        lastmDate = postlmFormater.format(lmDateObj);
        sp_to_date.setText(currentDate);
        sp_from_date.setText(lastmDate);
    }

    private void populateFuelList() {
        fuelList = new ArrayList<>();
//        fuelList.add("ALL");
        fuelList.add("HSD");
        fuelList.add("MS");
        fuelList.add("POWER");
        fuelList.add("TURBOJET");
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.sp_from_date:

                picker = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                                SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
                                Date dateObj = null;
                                Date lmDateObj = null;
                                try {
                                    dateObj = curFormater.parse(date);
//                                    lmDateObj = curFormater.parse(lmDatestr);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat postFormater = new SimpleDateFormat("MMM YYYY");

                                String newDateStr = postFormater.format(dateObj);
                                sp_from_date.setText(newDateStr);
                                fromDate = sp_from_date.getText().toString();

                            }
                        }, year, month, day);
                picker.show();

                break;

            case R.id.sp_to_date:

                picker = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                String date = dayOfMonth + "/" + (monthOfYear + 1) + "/" + year;

                                SimpleDateFormat curFormater = new SimpleDateFormat("dd/MM/yyyy");
                                Date dateObj = null;
                                Date lmDateObj = null;
                                try {
                                    dateObj = curFormater.parse(date);
//                                    lmDateObj = curFormater.parse(lmDatestr);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                SimpleDateFormat postFormater = new SimpleDateFormat("MMM YYYY");

                                String newDateStr = postFormater.format(dateObj);
                                sp_to_date.setText(newDateStr);


                               /* if (!toDate.equalsIgnoreCase("")){

                                }*/

//                                sp_to_date.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();


                break;

            case R.id.iv_back_date_from:
                String tv_date = tv_calendar_from.getText().toString();
                if(previousdate_from.equalsIgnoreCase(tv_calendar_from.getText().toString())){

                }else{
                    calendar.add(Calendar.MONTH, -1);
//                calendar.set(2021, 2, 1);
                    previousformattedDate_from = df.format(calendar.getTime());
                    android.util.Log.v("PREVIOUS DATE : ", previousformattedDate_from);
                    tv_calendar_from.setText(previousformattedDate_from);
                }
                break;


            case R.id.iv_front_date_from:
                if(currentdate_from.equalsIgnoreCase(tv_calendar_from.getText().toString())){

                }else{
                    calendar.add(Calendar.MONTH, 1);
                    CurrentformattedDate_to = df.format(calendar.getTime());
                    android.util.Log.v("NEXT DATE : ", CurrentformattedDate_to);
                    tv_calendar_from.setText(CurrentformattedDate_to);
                }

                break;

            case R.id.iv_back_date_to:
                if(previousdate_to.equalsIgnoreCase(tv_calendar_to.getText().toString())){

                }else {
                    calendar3.add(Calendar.MONTH, -1);
                    previousformattedDate_to = df.format(calendar3.getTime());
                    android.util.Log.v("PREVIOUS DATE : ", previousformattedDate_to);
                    tv_calendar_to.setText(previousformattedDate_to);
                }

                break;


            case R.id.iv_front_date_to:
                if(currentdate_to.equalsIgnoreCase(tv_calendar_to.getText().toString())){

                }else {
                    calendar3.add(Calendar.MONTH, 1);
//                    currentdate_to = df.format(calendar3.getTime());
                    CurrentformattedDate_to = df.format(calendar3.getTime());
                    android.util.Log.v("NEXT DATE : ", CurrentformattedDate_to);
                    tv_calendar_to.setText(CurrentformattedDate_to);
                }
                break;

            case R.id.tv_hour_comp_sales:

                tv_hour_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.black));
//                time = "MS";
                time = "HSD";
                tv_day_sales_trends.setBackground(null);
                tv_week_sales_trends.setBackground(null);
                tv_month_sales_trends.setBackground(null);
                tv_power_comp_sales.setBackground(null);
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_power_comp_sales.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.tv_day_comp_sales:

                tv_day_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.black));
                time = "MS";
                tv_hour_sales_trends.setBackground(null);
                tv_week_sales_trends.setBackground(null);
                tv_month_sales_trends.setBackground(null);
                tv_power_comp_sales.setBackground(null);
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_power_comp_sales.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.tv_week_comp_sales:

                tv_week_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.black));
                time = "POWER";
                tv_hour_sales_trends.setBackground(null);
                tv_day_sales_trends.setBackground(null);
                tv_month_sales_trends.setBackground(null);
                tv_power_comp_sales.setBackground(null);
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_power_comp_sales.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.tv_month_comp_sales:

                tv_month_sales_trends.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.black));
                time = "TURBOJET";
                tv_hour_sales_trends.setBackground(null);
                tv_day_sales_trends.setBackground(null);
                tv_week_sales_trends.setBackground(null);
                tv_power_comp_sales.setBackground(null);
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_power_comp_sales.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.tv_power_comp_sales:

                tv_power_comp_sales.setBackgroundResource(R.drawable.rounded_corner_white_background);
                tv_power_comp_sales.setTextColor(getResources().getColor(R.color.black));
                time = "POWER 99";
                tv_day_sales_trends.setBackground(null);
                tv_week_sales_trends.setBackground(null);
                tv_hour_sales_trends.setBackground(null);
                tv_month_sales_trends.setBackground(null);
                tv_hour_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_week_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_day_sales_trends.setTextColor(getResources().getColor(R.color.white));
                tv_month_sales_trends.setTextColor(getResources().getColor(R.color.white));
                break;

            case R.id.tv_submit:
                toDate = sp_to_date.getText().toString();
                fromDate = sp_from_date.getText().toString();
//                if (sp_to_date.getText().toString().equalsIgnoreCase(currentDate)){
//                    Toast.makeText(getActivity(),"Choose Last month Date",Toast.LENGTH_SHORT).show();
//                }else
                    if (tv_calendar_from.getText().toString().equalsIgnoreCase(tv_calendar_to.getText().toString())){
                    Toast.makeText(getActivity(),"Month can not be same",Toast.LENGTH_SHORT).show();
                }else {
//                    callComparativeSale(time,fromDate.toUpperCase(),toDate.toUpperCase());
                    callComparativeSale(time,tv_calendar_from.getText().toString(),tv_calendar_to.getText().toString());
                }
                break;
        }
    }

    private void callComparativeSale(String fuel_type_str, String fromDate_str, String toDate_str) {

            try {
                JSONObject json = new JSONObject();
//                json.put("productType", fuel_type_str);
                json.put("Product", fuel_type_str);
                fuel_type = fuel_type_str;
//                json.put("fromDate",toDate_str );
                json.put("fromDate",tv_calendar_from.getText().toString());
//                json.put("fromDate",fromDate_str );
//                json.put("toDate",fromDate_str);
                json.put("toDate",tv_calendar_to.getText().toString());
//                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "SelectdRO", ""));
//                try {
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Selectd","").equalsIgnoreCase("")){
//                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Request_Field","").equalsIgnoreCase("Zone")){
//                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone","").equalsIgnoreCase("ALL")){
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone",""));
//                                    json.put("ROCode", "ALL");
//                                }else{
//                                    json.put("ROCode", "ALL");
//                                }
//
//                            }
//                            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Request_Field","").equalsIgnoreCase("Region")){
//                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion","").equalsIgnoreCase("ALL")){
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone",""));
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion",""));
//                                    json.put("ROCode", "ALL");
//                                }else{
//                                    json.put("ROCode", "ALL");
//                                }
//                            }
//                            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Request_Field","").equalsIgnoreCase("SalesArea")){
//                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone",""));
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion",""));
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea",""));
//                                    json.put("ROCode", "ALL");
//                                }else{
//                                    json.put("ROCode", "ALL");
//                                }
//                            }
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("")){
//                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO","").equalsIgnoreCase("ALL")) {
//                                try {
//                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                    "Selectdzone", "").equalsIgnoreCase("")) {
//                                    } else {
//                                        json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "Selectdzone", ""));
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                try {
//                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                    "SelectdRegion", "").equalsIgnoreCase("")) {
//                                    } else {
//                                        json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdRegion", ""));
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                try {
//                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                    "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                    } else {
//                                        json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", ""));
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "FilteredRO",""));
//                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                        "FirstFilteredRO", "");
//                            }else{
//                                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "FilteredRO",""));
//                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                        "FirstFilteredRO", "");
//                            }
//
//
//
//                        }
//                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO","").equalsIgnoreCase("")){
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO",""));
//                        }
//
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Selectd","").equalsIgnoreCase("")){
//
//                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Request_Field","").equalsIgnoreCase("Region")){
//                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion","").equalsIgnoreCase("ALL")){
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion",""));
//                                    json.put("ROCode", "ALL");
//                                }else{
//                                    json.put("ROCode", "ALL");
//                                }
//                            }
//                            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion",""));
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea",""));
//                                    json.put("ROCode", "ALL");
//                                }else{
//                                    json.put("ROCode", "ALL");
//                                }
//                            }
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("")){
//                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO","").equalsIgnoreCase("ALL")) {
//                                try {
//                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                    "SelectdRegion", "").equalsIgnoreCase("")) {
//                                    } else {
//                                        json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdRegion", ""));
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                try {
//                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                    "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                    } else {
//                                        json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", ""));
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "FilteredRO",""));
//                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                        "FirstFilteredRO", "");
//                            }else{
//                                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "FilteredRO",""));
//                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                        "FirstFilteredRO", "");
//                            }
//
//                            //                    try {
//                            //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//                            //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//                            //                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                    "SelectdRegion",""));
//                            //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                    "SelectdSalesArea",""));
//                            //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                    "FilteredRO",""));
//                            //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            //                                    "FirstFilteredRO", "");
//                            //                        }else{
//                            //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                    "FilteredRO",""));
//                            //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            //                                    "FirstFilteredRO", "");
//                            //                        }
//                            //                    } catch (Exception e) {
//                            //                        e.printStackTrace();
//                            //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                "FilteredRO",""));
//                            //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            //                                "FirstFilteredRO", "");
//                            //                    }
//
//                        }
//                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO","").equalsIgnoreCase("")){
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO",""));
//                        }
//
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Request_Selectd","").equalsIgnoreCase("")){
//                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea",""));
//                                    json.put("ROCode", "ALL");
//                                }else{
//                                    json.put("ROCode", "ALL");
//                                }
//                            }
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("")){
//                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO","").equalsIgnoreCase("ALL")) {
//                                try {
//                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                    "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                                    } else {
//                                        json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                                "SelectdSalesArea", ""));
//                                    }
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "FilteredRO",""));
//                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                        "FirstFilteredRO", "");
//                            }else{
//                                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "FilteredRO",""));
//                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                        "FirstFilteredRO", "");
//                            }
//
//                            //                    try {
//                            //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//                            //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//                            //
//                            //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                    "SelectdSalesArea",""));
//                            //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                    "FilteredRO",""));
//                            //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            //                                    "FirstFilteredRO", "");
//                            //                        }else{
//                            //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                    "FilteredRO",""));
//                            //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            //                                    "FirstFilteredRO", "");
//                            //                        }
//                            //                    } catch (Exception e) {
//                            //                        e.printStackTrace();
//                            //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            //                                "FilteredRO",""));
//                            //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            //                                "FirstFilteredRO", "");
//                            //                    }
//
//                        }
//                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO","").equalsIgnoreCase("")){
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO",""));
//                        }
//
//                    }
//                    else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO","").equalsIgnoreCase("")){
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FilteredRO",""));
//                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO", "");
//                        }
//                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO","").equalsIgnoreCase("")){
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "FirstFilteredRO",""));
//                        }
//                    }
//                    else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "UserCustomRole","").equalsIgnoreCase("DEALER")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "ROKey","") );
//                    }
//                }
//                catch (JSONException e) {
//                    e.printStackTrace();
//                }
                try {
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Selectd","").equalsIgnoreCase("")){
                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Request_Field","").equalsIgnoreCase("Zone")){
                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").equalsIgnoreCase("ALL")){
                                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone","").split(",");
                                    JSONArray jsonObject = new JSONArray(myArray);
                                    json.put("Zone", jsonObject);
                                    String[] myArray1 = "ALL".split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray1);
                                    json.put("ROCode", jsonObject1);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
                                }else{
                                    String[] myArray1 = "ALL".split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray1);
                                    json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                                }

                            }
                            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Request_Field","").equalsIgnoreCase("Region")){
                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").equalsIgnoreCase("ALL")){
                                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone","").split(",");
                                    JSONArray jsonObject = new JSONArray(myArray);
                                    json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                                    String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray1);
                                    json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                    String[] myArray2 = "ALL".split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                                }else{
                                    String[] myArray1 = "ALL".split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray1);
                                    json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                                }
                            }
                            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Request_Field","").equalsIgnoreCase("SalesArea")){
                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone","").split(",");
                                    JSONArray jsonObject = new JSONArray(myArray);
                                    json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                                    String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray1);
                                    json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                    String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("SalesArea", jsonObject2);
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdSalesArea",""));
                                    String[] myArray3 = "ALL".split(",");
                                    JSONArray jsonObject3 = new JSONArray(myArray3);
                                    json.put("ROCode", jsonObject3);
//                                json.put("ROCode", "ALL");
                                }else{
                                    String[] myArray2 = "ALL".split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray2);
                                    json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                                }
                            }
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }
                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("")){
                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").equalsIgnoreCase("ALL")) {
                                try {
                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                    "Selectdzone", "").equalsIgnoreCase("")) {
                                    } else {
                                        String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "Selectdzone","").split(",");
                                        JSONArray jsonObject2 = new JSONArray(myArray2);
                                        json.put("Zone", jsonObject2 );
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "Selectdzone", ""));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                    "SelectdRegion", "").equalsIgnoreCase("")) {
                                    } else {
                                        String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion","").split(",");
                                        JSONArray jsonObject3 = new JSONArray(myArray3);
                                        json.put("Region", jsonObject3 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                    "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                    } else {
                                        String[] myArray4 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea","").split(",");
                                        JSONArray jsonObject4 = new JSONArray(myArray4);
                                        json.put("SalesArea", jsonObject4 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "FilteredRO","").split(",");
                                JSONArray jsonObject5 = new JSONArray(myArray5);
                                json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "FirstFilteredRO", "");
                            }else{
                                String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "FilteredRO","").split(",");
                                JSONArray jsonObject5 = new JSONArray(myArray5);
                                json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "FirstFilteredRO", "");
                            }



                        }
                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").equalsIgnoreCase("")){
                            String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO","").split(",");
                            JSONArray jsonObject5 = new JSONArray(myArray5);
                            json.put("ROCode", jsonObject5 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Selectd","").equalsIgnoreCase("")){
                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Request_Field","").equalsIgnoreCase("Region")){
                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").equalsIgnoreCase("ALL")){
                                    String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","").split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray1);
                                    json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                                    String[] myArray = "ALL".split(",");
                                    JSONArray jsonObject = new JSONArray(myArray);
                                    json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                                }
                                else{
                                    String[] myArray = "ALL".split(",");
                                    JSONArray jsonObject = new JSONArray(myArray);
                                    json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                                }
                            }
                            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Request_Field","").equalsIgnoreCase("SalesArea")){

                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion",""));
                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea",""));
                                    json.put("ROCode", "ALL");
                                }else{
                                    json.put("ROCode", "ALL");
                                }
                            }
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }
                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("")){
                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").equalsIgnoreCase("ALL")) {
                                try {
                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                    "SelectdRegion", "").equalsIgnoreCase("")) {
                                    } else {
                                        String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdRegion","").split(",");
                                        JSONArray jsonObject2 = new JSONArray(myArray2);
                                        json.put("Region", jsonObject2 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                try {
                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                    "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                    } else {
                                        String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea","").split(",");
                                        JSONArray jsonObject3 = new JSONArray(myArray3);
                                        json.put("SalesArea", jsonObject3);
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "FilteredRO","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "FirstFilteredRO", "");
                            }else{
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "FilteredRO","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "FirstFilteredRO", "");
                            }

                        }
                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").equalsIgnoreCase("")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "UserCustomRole","").equalsIgnoreCase("HPCLREGION"))
                    {
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Request_Selectd","").equalsIgnoreCase("")){
                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Request_Field","").equalsIgnoreCase("SalesArea")){

                                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","").split(",");
                                    JSONArray jsonObject1 = new JSONArray(myArray);
                                    String[] myArray1 = "ALL".split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray1);
                                    json.put("SalesArea", jsonObject1);
                                    json.put("ROCode", jsonObject2);
                                }
                                else{
                                    String[] myArray1 = "ALL".split(",");
                                    JSONArray jsonObject2 = new JSONArray(myArray1);
                                    json.put("ROCode", jsonObject2);
                                }
                            }
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }
                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("")){
                            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").equalsIgnoreCase("ALL")) {
                                try {
                                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                            UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                    "SelectdSalesArea", "").equalsIgnoreCase("")) {
                                    }
                                    else {
                                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                                "SelectdSalesArea","").split(",");
                                        JSONArray jsonObject1 = new JSONArray(myArray);
                                        json.put("SalesArea", jsonObject1 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "FilteredRO","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "FirstFilteredRO", "");
                            }
                            else{
//                            String[] myArray1 = "ALL".split(",");
//                            JSONArray jsonObject2 = new JSONArray(myArray1);
//                            json.put("SalesArea", jsonObject2 );
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "FilteredRO","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "FirstFilteredRO", "");
                            }

                            //                    try {
                            //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                            //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                            //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                            //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                            //
                            //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                            //                                    "SelectdSalesArea",""));
                            //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                            //                                    "FilteredRO",""));
                            //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                            //                                    "FirstFilteredRO", "");
                            //                        }else{
                            //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                            //                                    "FilteredRO",""));
                            //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                            //                                    "FirstFilteredRO", "");
                            //                        }
                            //                    } catch (Exception e) {
                            //                        e.printStackTrace();
                            //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                            //                                "FilteredRO",""));
                            //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                            //                                "FirstFilteredRO", "");
                            //                    }

                        }
                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").equalsIgnoreCase("")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                        }
                    }
                    else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FilteredRO",""));
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }
                        else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO","").equalsIgnoreCase("")){

                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                        }
                    }
                    else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "UserCustomRole","").equalsIgnoreCase("DEALER")){
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "ROKey","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
                    }
                }
                catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.e("json",json.toString());
                new AsyncComparativeSales().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
            } catch (JSONException e) {
                e.printStackTrace();
            }
    }

    private class AsyncComparativeSales extends AsyncTask<JSONObject, Void, String>{
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncComparativeSales() {
        }

        public AsyncComparativeSales(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
//            return ClsWebService_hpcl.PostObjecttoken("Dashboard/ComparativeSale", false, jsonObjects[0],"");
            return ClsWebService_hpcl.PostObjecttoken("DashboardV2/GetComparativeSale", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
//            dialog.dismiss();
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("Data"));
                            JSONObject jsonObject_first_month = new JSONObject(jsonObject1.getString("firstmonth"));
                            JSONObject jsonObject_second_month = new JSONObject(jsonObject1.getString("secondMonth"));

                            arraymonth1 = new JSONArray(jsonObject_first_month.getString("month"));
                            arrayHSD1 = new JSONArray(jsonObject_first_month.getString("HSD"));
                            arrayMS1 = new JSONArray(jsonObject_first_month.getString("MS"));
                            arrayPower1 = new JSONArray(jsonObject_first_month.getString("Power"));
                            arrayTurbojet1 = new JSONArray(jsonObject_first_month.getString("Turbojet"));
                            arrayPOWER991 = new JSONArray(jsonObject_first_month.getString("Power99"));

                            arraymonth2 = new JSONArray(jsonObject_second_month.getString("month"));
                            arrayHSD2 = new JSONArray(jsonObject_second_month.getString("HSD"));
                            arrayMS2 = new JSONArray(jsonObject_second_month.getString("MS"));
                            arrayPower2 = new JSONArray(jsonObject_second_month.getString("Power"));
                            arrayTurbojet2 = new JSONArray(jsonObject_second_month.getString("Turbojet"));
                            arrayPOWER992 = new JSONArray(jsonObject_second_month.getString("Power99"));

                            List<Integer> list_month1 = new ArrayList<Integer>();
                            List<Integer> list_HSD1 = new ArrayList<Integer>();
                            List<Integer> listMS1 = new ArrayList<Integer>();
                            List<Integer> listPower1 = new ArrayList<Integer>();
                            List<Integer> listTurbojet1 = new ArrayList<Integer>();
                            List<Integer> listPOWER991 = new ArrayList<Integer>();

                            List<Integer> list_month2 = new ArrayList<Integer>();
                            List<Integer> list_HSD2 = new ArrayList<Integer>();
                            List<Integer> listMS2 = new ArrayList<Integer>();
                            List<Integer> listPower2 = new ArrayList<Integer>();
                            List<Integer> listTurbojet2 = new ArrayList<Integer>();
                            List<Integer> listPOWER992 = new ArrayList<Integer>();
                            for (int i1 = 0; i1 < arraymonth1.length(); i1++) {
                                list_month1.add(arraymonth1.getInt(i1));
                            }
                            for (int i = 0; i < arrayHSD1.length(); i++) {
                                list_HSD1.add(arrayHSD1.getInt(i));
                            }
                            for (int ms = 0; ms < arrayMS1.length(); ms++) {
                                listMS1.add(arrayMS1.getInt(ms));
                            }
                            for (int power = 0; power < arrayPower1.length(); power++) {
                                listPower1.add(arrayPower1.getInt(power));
                            }
                            for (int turbojet = 0; turbojet < arrayTurbojet1.length(); turbojet++) {
                                listTurbojet1.add(arrayTurbojet1.getInt(turbojet));
                            }
                            for (int POWER99 = 0; POWER99 < arrayPOWER991.length(); POWER99++) {
                                listPOWER991.add(arrayPOWER991.getInt(POWER99));
                            }

                            for (int i1 = 0; i1 < arraymonth2.length(); i1++) {
                                list_month2.add(arraymonth2.getInt(i1));
                            }
                            for (int i = 0; i < arrayHSD2.length(); i++) {
                                list_HSD2.add(arrayHSD2.getInt(i));
                            }
                            for (int ms1 = 0; ms1 < arrayMS2.length(); ms1++) {
                                listMS2.add(arrayMS2.getInt(ms1));
                            }
                            for (int power = 0; power < arrayPower2.length(); power++) {
                                listPower2.add(arrayPower2.getInt(power));
                            }
                            for (int turbojet = 0; turbojet < arrayTurbojet2.length(); turbojet++) {
                                listTurbojet2.add(arrayTurbojet2.getInt(turbojet));
                            }
                            for (int POWER99 = 0; POWER99 < arrayPOWER992.length(); POWER99++) {
                                listPOWER992.add(arrayPOWER992.getInt(POWER99));
                            }

                            int smallestmonth1 = list_month1.get(0);
                            int biggestmonth1 = list_month1.get(0);
                            int smallest_HSD1 = list_HSD1.get(0);
                            int biggest_HSD1 = list_HSD1.get(0);
                            int smallestMS1 = listMS1.get(0);
                            int biggestMS1 = listMS1.get(0);
                            int smallestPower1 = listPower1.get(0);
                            int biggestPower1 = listPower1.get(0);
                            int smallestTurbojet1 = listTurbojet1.get(0);
                            int biggestTurbojet1 = listTurbojet1.get(0);
                            int smallestPOWER991 = listPOWER991.get(0);
                            int biggestPOWER991 = listPOWER991.get(0);

                            int smallestmonth2 = list_month2.get(0);
                            int biggestmonth2 = list_month2.get(0);
                            int smallest_HSD2 = list_HSD2.get(0);
                            int biggest_HSD2 = list_HSD2.get(0);
                            int smallestMS2 = listMS2.get(0);
                            int biggestMS2 = listMS2.get(0);
                            int smallestPower2 = listPower2.get(0);
                            int biggestPower2 = listPower2.get(0);
                            int smallestTurbojet2 = listTurbojet2.get(0);
                            int biggestTurbojet2 = listTurbojet2.get(0);
                            int smallestPOWER992 = listPOWER992.get(0);
                            int biggestPOWER992 = listPOWER992.get(0);

                            for (int i = 1; i < list_month1.size(); i++) {
                                if (list_month1.get(i) > biggestmonth1)
                                    biggestmonth1 = list_month1.get(i);
                                else if (list_month1.get(i) < smallestmonth1)
                                    smallestmonth1 = list_month1.get(i);
                            }
                            for (int i2 = 1; i2 < list_HSD1.size(); i2++) {
                                if (list_HSD1.get(i2) > biggest_HSD1)
                                    biggest_HSD1 = list_HSD1.get(i2);
                                else if (list_HSD1.get(i2) < smallest_HSD1)
                                    smallest_HSD1 = list_HSD1.get(i2);

                            }
                            for (int ms = 1; ms < listMS1.size(); ms++) {
                                if (listMS1.get(ms) > biggestMS1)
                                    biggestMS1 = listMS1.get(ms);
                                else if (listMS1.get(ms) < smallestMS1)
                                    smallestMS1 = listMS1.get(ms);

                            }
                            for (int power = 1; power < listPower1.size(); power++) {
                                if (listPower1.get(power) > biggestPower1)
                                    biggestPower1 = listPower1.get(power);
                                else if (listPower1.get(power) < smallestPower1)
                                    smallestPower1 = listPower1.get(power);
                            }
                            for (int turbojet = 1; turbojet < listTurbojet1.size(); turbojet++) {
                                if (listTurbojet1.get(turbojet) > biggestTurbojet1)
                                    biggestTurbojet1 = listTurbojet1.get(turbojet);
                                else if (listTurbojet1.get(turbojet) < smallestTurbojet1)
                                    smallestTurbojet1 = listTurbojet1.get(turbojet);

                            }
                            for (int POWER99 = 1; POWER99 < listPOWER991.size(); POWER99++) {
                                if (listPOWER991.get(POWER99) > biggestPOWER991)
                                    biggestPOWER991 = listPOWER991.get(POWER99);
                                else if (listPOWER991.get(POWER99) < smallestPOWER991)
                                    smallestPOWER991 = listPOWER991.get(POWER99);

                            }

                            for (int i = 1; i < list_month2.size(); i++) {
                                if (list_month2.get(i) > biggestmonth2)
                                    biggestmonth2 = list_month2.get(i);
                                else if (list_month2.get(i) < smallestmonth2)
                                    smallestmonth2 = list_month2.get(i);
                            }
                            for (int i2 = 1; i2 < list_HSD2.size(); i2++) {
                                if (list_HSD2.get(i2) > biggest_HSD2)
                                    biggest_HSD2 = list_HSD2.get(i2);
                                else if (list_HSD2.get(i2) < smallest_HSD2)
                                    smallest_HSD2 = list_HSD2.get(i2);

                            }
                            for (int ms1 = 1; ms1 < listMS2.size(); ms1++) {
                                if (listMS2.get(ms1) > biggestMS2)
                                    biggestMS2 = listMS2.get(ms1);
                                else if (listMS2.get(ms1) < smallestMS2)
                                    smallestMS2 = listMS2.get(ms1);

                            }
                            for (int power = 1; power < listPower2.size(); power++) {
                                if (listPower2.get(power) > biggestPower2)
                                    biggestPower2 = listPower2.get(power);
                                else if (listPower2.get(power) < smallestPower2)
                                    smallestPower2 = listPower2.get(power);
                            }
                            for (int turbojet = 1; turbojet < listTurbojet2.size(); turbojet++) {
                                if (listTurbojet2.get(turbojet) > biggestTurbojet2)
                                    biggestTurbojet2 = listTurbojet2.get(turbojet);
                                else if (listTurbojet2.get(turbojet) < smallestTurbojet2)
                                    smallestTurbojet2 = listTurbojet2.get(turbojet);

                            }
                            for (int POWER99 = 1; POWER99 < listPOWER992.size(); POWER99++) {
                                if (listPOWER992.get(POWER99) > biggestPOWER992)
                                    biggestPOWER992 = listPOWER992.get(POWER99);
                                else if (listPOWER992.get(POWER99) < smallestPOWER992)
                                    smallestPOWER992 = listPOWER992.get(POWER99);

                            }
                            biggestmonthmain1 = biggestmonth1;
                            smallestmonthmain1 = smallestmonth1;
                            biggest_HSDmain1 = biggest_HSD1;
                            smallest_HSDmain1 = smallest_HSD1;
                            biggestMSmain1 = biggestMS1;
                            smallestMSmain1 = smallestMS1;
                            biggestPowermain1 = biggestPower1;
                            smallestPowermain1 = smallestPower1;
                            biggestTurbojetmain1 = biggestTurbojet1;
                            smallestTurbojetmain1 = smallestTurbojet1;
                            biggestPOWER99main1 = biggestPOWER991;
                            smallestPOWER99main1 = biggestPOWER991;
                            biggestmonthmain2 = biggestmonth2;
                            smallestmonthmain2 = smallestmonth2;
                            biggest_HSDmain2 = biggest_HSD2;
                            smallest_HSDmain2 = smallest_HSD2;
                            biggestMSmain2 = biggestMS2;
                            smallestMSmain2 = smallestMS2;
                            biggestPowermain2 = biggestPower2;
                            smallestPowermain2 = smallestPower2;
                            biggestTurbojetmain2 = biggestTurbojet2;
                            smallestTurbojetmain2 = smallestTurbojet2;
                            biggestPOWER99main2 = biggestPOWER992;
                            smallestPOWER99main2 = smallestPOWER992;
//                          revArrayList=  new ArrayList<Integer>();
////                        for (int i = list2.size() - 1; i >= 0; i--) {
////                            // Append the elements in reverse order
////                              revArrayList.add(list2.get(i));
////                        }
//                            tv_Xaxis.setVisibility(View.VISIBLE);
//                            tv_Yaxis.setVisibility(View.VISIBLE);
                            renderData();
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();
                            mChart.setNoDataText("NIL");
                            mChart.invalidate();
                            mChart.clear();
//                            tv_Xaxis.setVisibility(View.GONE);
//                            tv_Yaxis.setVisibility(View.GONE);
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                    mChart.setNoDataText("NIL");
                    mChart.invalidate();
                    mChart.clear();
//                    tv_Xaxis.setVisibility(View.GONE);
//                    tv_Yaxis.setVisibility(View.GONE);
                }
            }
        }


    }

    private void renderData() {

        LimitLine llXAxis = new LimitLine(10f, "Index 10");
        llXAxis.setLineWidth(4f);
        llXAxis.enableDashedLine(10f, 10f, 0f);
        llXAxis.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        llXAxis.setTextSize(10f);

        XAxis xAxis = mChart.getXAxis();
        xAxis.setGranularity(1f);


        xAxis.setTextColor(getResources().getColor(R.color.black));
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.enableGridDashedLine(00f, 00f, 0f);
        int[] biggestallmonth = {biggestmonthmain1, biggestmonthmain2};
        int[] smallestallmonth = {smallestmonthmain1, smallestmonthmain2};
        int smallestallmonth_main = smallestallmonth[0];
        int biggestallmonth_main = biggestallmonth[0];
        List<Integer> listsmallestAllmonth = new ArrayList<Integer>();
        List<Integer> listbiggestAllmonth = new ArrayList<Integer>();
        for (int i1=0; i1<biggestallmonth.length; i1++) {
            listbiggestAllmonth.add(biggestallmonth[i1]);
        }
        for (int i1=0; i1<smallestallmonth.length; i1++) {
            listsmallestAllmonth.add(smallestallmonth[i1]);
        }

        for(int i=1; i< listbiggestAllmonth.size(); i++)
        {
            if(listbiggestAllmonth.get(i) > biggestallmonth_main)
                biggestallmonth_main = listbiggestAllmonth.get(i);
        }
        for(int i=1; i< listbiggestAllmonth.size(); i++)
        {
            if (listsmallestAllmonth.get(i) < smallestallmonth_main)
                smallestallmonth_main = listsmallestAllmonth.get(i);
        }
        float f2 = (float) biggestallmonth_main / 1;
        float f3 = (float) smallestallmonth_main / 1;
//        xAxis.setAxisMaximum(10f);
//        xAxis.setAxisMinimum(0f);
        xAxis.setAxisMaximum(f2);
        xAxis.setAxisMinimum(f3);

        xAxis.setTextColor(getResources().getColor(R.color.black));
        xAxis.setDrawLimitLinesBehindData(true);

        LimitLine ll1 = new LimitLine(215f, "Maximum Limit");
        ll1.setLineWidth(4f);
        ll1.enableDashedLine(10f, 10f, 0f);
        ll1.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll1.setTextSize(10f);

        LimitLine ll2 = new LimitLine(70f, "Minimum Limit");
        ll2.setLineWidth(4f);
        ll2.enableDashedLine(00f, 00f, 0f);
        ll2.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_BOTTOM);
        ll2.setTextSize(10f);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setGranularity(1f);
        leftAxis.removeAllLimitLines();
        leftAxis.setTextColor(getResources().getColor(R.color.black));

//        leftAxis.addLimitLine(ll1);
//        leftAxis.addLimitLine(ll2);
        int[] biggestall = {biggest_HSDmain1, biggest_HSDmain2, biggestMSmain1,
                biggestMSmain2,biggestPowermain1,biggestPowermain2,
                biggestTurbojetmain1,biggestTurbojetmain2,biggestPOWER99main1,biggestPOWER99main2};
        int[] smallestall = {smallest_HSDmain1, smallest_HSDmain2, smallestMSmain1,
                smallestMSmain2,smallestPowermain1,smallestPowermain2,smallestTurbojetmain1,smallestTurbojetmain1,
                smallestPOWER99main1,smallestPOWER99main1};

        int smallestall1 = smallestall[0];
        int biggestall1 = biggestall[0];
        List<Integer> listsmallestAll = new ArrayList<Integer>();
        List<Integer> listbiggestAll = new ArrayList<Integer>();
        for (int i1=0; i1<biggestall.length; i1++) {
            listbiggestAll.add(biggestall[i1]);
        }
        for (int i1=0; i1<smallestall.length; i1++) {
            listsmallestAll.add(smallestall[i1]);
        }

        for(int i=1; i< listbiggestAll.size(); i++)
        {
            if(listbiggestAll.get(i) > biggestall1)
                biggestall1 = listbiggestAll.get(i);
        }
        for(int i=1; i< listbiggestAll.size(); i++)
        {
            if (listsmallestAll.get(i) < smallestall1)
                smallestall1 = listsmallestAll.get(i);
        }
        float f=0.0f,f1=0.0f;
        if(fuel_type.equalsIgnoreCase("HSD")){
            int[] biggestallHSD = {biggest_HSDmain1, biggest_HSDmain2};
            int[] smallestallHSD = {smallest_HSDmain1, smallest_HSDmain2};
            int smallestallHSD_main = biggestallHSD[0];
            int biggestallHSD_main = smallestallHSD[0];
            List<Integer> listsmallestAllHSD = new ArrayList<Integer>();
            List<Integer> listbiggestAllHSD = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallHSD.length; i1++) {
                listbiggestAllHSD.add(biggestallHSD[i1]);
            }
            for (int i1=0; i1<smallestallHSD.length; i1++) {
                listsmallestAllHSD.add(smallestallHSD[i1]);
            }

            for(int i=0; i< listbiggestAllHSD.size(); i++)
            {
                if(listbiggestAllHSD.get(i) > biggestallHSD_main)
                    biggestallHSD_main = listbiggestAllHSD.get(i);
            }
            for(int i=0; i< listbiggestAllHSD.size(); i++)
            {
                if (listsmallestAllHSD.get(i) < smallestallHSD_main)
                    smallestallHSD_main = listsmallestAllHSD.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallHSD_main / 1;
            f1 = (float) smallestallHSD_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("POWER")){
            int[] biggestallPOWER = {biggestPowermain1, biggestPowermain2};
            int[] smallestallPOWER = {smallestPowermain1, smallestPowermain2};
            int smallestallPOWER_main = biggestallPOWER[0];
            int biggestallPOWER_main = biggestallPOWER[0];
            List<Integer> listsmallestAllPOWER = new ArrayList<Integer>();
            List<Integer> listbiggestAllPOWER = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallPOWER.length; i1++) {
                listbiggestAllPOWER.add(biggestallPOWER[i1]);
            }
            for (int i1=0; i1<smallestallPOWER.length; i1++) {
                listsmallestAllPOWER.add(smallestallPOWER[i1]);
            }

            for(int i=0; i< listbiggestAllPOWER.size(); i++)
            {
                if(listbiggestAllPOWER.get(i) > biggestallPOWER_main)
                    biggestallPOWER_main = listbiggestAllPOWER.get(i);
            }
            for(int i=0; i< listbiggestAllPOWER.size(); i++)
            {
                if (listsmallestAllPOWER.get(i) < smallestallPOWER_main)
                    smallestallPOWER_main = listsmallestAllPOWER.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallPOWER_main / 1;
            f1 = (float) smallestallPOWER_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("MS")){
            int[] biggestallMS = {biggestMSmain1, biggestMSmain2};
            int[] smallestallMS = {smallestMSmain1, smallestMSmain2};
            int smallestallMS_main = biggestallMS[0];
            int biggestallMS_main = smallestallMS[0];
            List<Integer> listsmallestAllMS = new ArrayList<Integer>();
            List<Integer> listbiggestAllMS = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallMS.length; i1++) {
                listbiggestAllMS.add(biggestallMS[i1]);
            }
            for (int i1=0; i1<smallestallMS.length; i1++) {
                listsmallestAllMS.add(smallestallMS[i1]);
            }

            for(int i=0; i< listbiggestAllMS.size(); i++)
            {
                if(listbiggestAllMS.get(i) > biggestallMS_main)
                    biggestallMS_main = listbiggestAllMS.get(i);
            }
            for(int i=0; i< listbiggestAllMS.size(); i++)
            {
                if (listsmallestAllMS.get(i) < smallestallMS_main)
                    smallestallMS_main = listsmallestAllMS.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallMS_main / 1;
            f1 = (float) smallestallMS_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("TURBOJET")){
            int[] biggestallTURBOJET = {biggestTurbojetmain1, biggestTurbojetmain2};
            int[] smallestallTURBOJET= {smallestTurbojetmain1, smallestTurbojetmain2};
            int smallestallTURBOJET_main = biggestallTURBOJET[0];
            int biggestallTURBOJET_main = smallestallTURBOJET[0];
            List<Integer> listsmallestAllTURBOJET = new ArrayList<Integer>();
            List<Integer> listbiggestAllTURBOJET = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallTURBOJET.length; i1++) {
                listbiggestAllTURBOJET.add(biggestallTURBOJET[i1]);
            }
            for (int i1=0; i1<smallestallTURBOJET.length; i1++) {
                listsmallestAllTURBOJET.add(smallestallTURBOJET[i1]);
            }

            for(int i=0; i< listbiggestAllTURBOJET.size(); i++)
            {
                if(listbiggestAllTURBOJET.get(i) > biggestallTURBOJET_main)
                    biggestallTURBOJET_main = listbiggestAllTURBOJET.get(i);
            }
            for(int i=0; i< listbiggestAllTURBOJET.size(); i++)
            {
                if (listsmallestAllTURBOJET.get(i) < smallestallTURBOJET_main)
                    smallestallTURBOJET_main = listsmallestAllTURBOJET.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallTURBOJET_main / 1;
            f1 = (float) smallestallTURBOJET_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("POWER 99")){
            int[] biggestallPOWER99 = {biggestPOWER99main1, biggestPOWER99main2};
            int[] smallestallPOWER99= {smallestPOWER99main1, smallestPOWER99main2};
            int smallestallPOWER99_main = biggestallPOWER99[0];
            int biggestallPOWER99_main = smallestallPOWER99[0];
            List<Integer> listsmallestAllPOWER99 = new ArrayList<Integer>();
            List<Integer> listbiggestAllPOWER99 = new ArrayList<Integer>();
            for (int i1=0; i1<biggestallPOWER99.length; i1++) {
                listbiggestAllPOWER99.add(biggestallPOWER99[i1]);
            }
            for (int i1=0; i1<smallestallPOWER99.length; i1++) {
                listsmallestAllPOWER99.add(smallestallPOWER99[i1]);
            }

            for(int i=0; i< listbiggestAllPOWER99.size(); i++)
            {
                if(listbiggestAllPOWER99.get(i) > biggestallPOWER99_main)
                    biggestallPOWER99_main = listbiggestAllPOWER99.get(i);
            }
            for(int i=0; i< listbiggestAllPOWER99.size(); i++)
            {
                if (listsmallestAllPOWER99.get(i) < smallestallPOWER99_main)
                    smallestallPOWER99_main = listsmallestAllPOWER99.get(i);
            }
//          f2 = (float) biggestallmonth_main / 1;
//           f3 = (float) smallestallmonth_main / 1;
            f = (float) biggestallPOWER99_main / 1;
            f1 = (float) smallestallPOWER99_main / 1;
        }
        else if(fuel_type.equalsIgnoreCase("ALL")){
            f = (float) biggestall1 / 1;
            f1 = (float) smallestall1 / 1;
        }

//        leftAxis.setAxisMaximum(350f);
//        leftAxis.setAxisMinimum(0f);
        leftAxis.setAxisMaximum(f);
        leftAxis.setAxisMinimum(f1);
//        leftAxis.setAxisMaximum(f2);
//        leftAxis.setAxisMinimum(f3);
//        leftAxis.enableGridDashedLine(10f, 10f, 0f);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawLimitLinesBehindData(false);
        mChart.getLegend().setTextColor(getResources().getColor(R.color.black));
        mChart.getDescription().setEnabled(false);
        mChart.getAxisRight().setEnabled(false);
        MyMarkerView mv = new MyMarkerView(getActivity(), R.layout.custom_marker_view);
        mv.setChartView(mChart);
        mChart.getAxisLeft().setDrawGridLines(false);
        mChart.getXAxis().setDrawGridLines(false);
        mChart.setMarker(mv);
        mChart.animateXY(3000,2000);
        setData();
    }

    private void setData() {

//        ArrayList<Entry> values = new ArrayList<>();
//        ArrayList<Entry> values2 = new ArrayList<>();
        ArrayList<Entry> valuesHSD1 = new ArrayList<>();
        ArrayList<Entry> valuesMS1 = new ArrayList<>();
        ArrayList<Entry> valuesPower1 = new ArrayList<>();
        ArrayList<Entry> valuesTurbojet1 = new ArrayList<>();
        ArrayList<Entry> valuesHSD2 = new ArrayList<>();
        ArrayList<Entry> valuesMS2 = new ArrayList<>();
        ArrayList<Entry> valuesPower2 = new ArrayList<>();
        ArrayList<Entry> valuesTurbojet2 = new ArrayList<>();
        ArrayList<Entry> valuesPower991 = new ArrayList<>();
        ArrayList<Entry> valuesPower992 = new ArrayList<>();
        if(fuel_type.equalsIgnoreCase("HSD")){
            for(int i = 0; i < arrayHSD1.length(); i++){
                try {
                    valuesHSD1.add(new Entry(arraymonth1.getInt(i), arrayHSD1.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayHSD2.length(); i++){
                try {
                    valuesHSD2.add(new Entry(arraymonth2.getInt(i), arrayHSD2.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("POWER")){
            for(int i = 0; i < arrayPower1.length(); i++){
                try {
                    valuesPower1.add(new Entry(arraymonth1.getInt(i), arrayPower1.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayPower2.length(); i++){
                try {
                    valuesPower2.add(new Entry(arraymonth2.getInt(i), arrayPower2.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("MS")){
            for(int i = 0; i < arrayMS1.length(); i++){
                try {
                    valuesMS1.add(new Entry(arraymonth1.getInt(i), arrayMS1.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayMS2.length(); i++){
                try {
                    valuesMS2.add(new Entry(arraymonth2.getInt(i), arrayMS2.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("TURBOJET")){
            for(int i = 0; i < arrayTurbojet1.length(); i++){
                try {
                    valuesTurbojet1.add(new Entry(arraymonth1.getInt(i), arrayTurbojet1.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayTurbojet2.length(); i++){
                try {
                    valuesTurbojet2.add(new Entry(arraymonth2.getInt(i), arrayTurbojet2.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("POWER 99")){
            for(int i = 0; i < arrayPOWER991.length(); i++){
                try {
                    valuesPower991.add(new Entry(arraymonth1.getInt(i), arrayPOWER991.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for(int i = 0; i < arrayPOWER992.length(); i++){
                try {
                    valuesPower992.add(new Entry(arraymonth2.getInt(i), arrayPOWER992.getInt(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        else if(fuel_type.equalsIgnoreCase("ALL")){
//            for(int i = 0; i < arrayHSD.length(); i++){
//                try {
//                    valuesHSD.add(new Entry(arraymonth.getInt(i), arrayHSD.getInt(i)));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            for(int i = 0; i < arrayMS.length(); i++){
//                try {
//                    valuesMS.add(new Entry(arraymonth.getInt(i), arrayMS.getInt(i)));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            for(int i = 0; i < arrayPower.length(); i++){
//                try {
//                    valuesPower.add(new Entry(arraymonth.getInt(i), arrayPower.getInt(i)));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//            for(int i = 0; i < arrayTurbojet.length(); i++){
//                try {
//                    valuesTurbojet.add(new Entry(arraymonth.getInt(i), arrayTurbojet.getInt(i)));
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
        }

        LineDataSet setHSD1,setHSD2;
        LineDataSet setPower1,setPower2;
        LineDataSet setMS1,setMS2;
        LineDataSet setTurbojet1,setTurbojet2;
        LineDataSet setPower991,setPower992;
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        LineData data;
       /* if(!(fuel_type.equals("Select") || from_month.equals("Select Month")||
                to_month.equals("Select Month"))){
            tv_Xaxis.setText("X-Axis = "+ "Days");
            tv_Yaxis.setText("Y-Axis = "+ "Quantity(Ltrs)");
        }else{
            tv_Xaxis.setVisibility(View.GONE);
            tv_Yaxis.setVisibility(View.GONE);
        }*/
        if(fuel_type.equalsIgnoreCase("HSD")){
//            setHSD1 = new LineDataSet(valuesHSD1, "Month 2");
//            setHSD2 = new LineDataSet(valuesHSD2, "Month 1");
            setHSD1 = new LineDataSet(valuesHSD1, tv_calendar_from.getText().toString());
            setHSD2 = new LineDataSet(valuesHSD2, tv_calendar_to.getText().toString());
//            setHSD1.setValueTextColor(getResources().getColor(R.color.white));
            setHSD1.setDrawIcons(false);
//            setHSD1.enableDashedLine(10f, 0f, 0f);
//            setHSD1.enableDashedHighlightLine(10f, 0f, 0f);
            setHSD1.setColor(getResources().getColor(R.color.line_chart_hsd_color));
            setHSD1.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setHSD1.setLineWidth(1f);
            setHSD1.setCircleRadius(3f);
            setHSD1.setDrawCircleHole(false);
            setHSD1.setValueTextSize(0f);
            setHSD1.setDrawFilled(true);
            setHSD1.setFormLineWidth(1f);

//            setHSD1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setHSD1.setFormSize(15.f);
//            setHSD2.setValueTextColor(getResources().getColor(R.color.white));
            setHSD2.setDrawIcons(false);
//            setHSD2.enableDashedLine(10f, 0f, 0f);
//            setHSD2.enableDashedHighlightLine(10f, 0f, 0f);
            setHSD2.setColor(getResources().getColor(R.color.line_chart_all_color));
            setHSD2.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setHSD2.setLineWidth(1f);
            setHSD2.setCircleRadius(2f);
            setHSD2.setDrawCircleHole(false);
            setHSD2.setValueTextSize(0f);
            setHSD2.setDrawFilled(true);
            setHSD2.setFormLineWidth(1f);
//            setHSD2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setHSD2.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_hsd);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setHSD1.setFillDrawable(drawable);
                setHSD2.setFillDrawable(drawable1);
            } else {
                setHSD1.setFillColor(Color.DKGRAY);
                setHSD2.setFillColor(Color.DKGRAY);
            }
            dataSets.add(setHSD1);
            dataSets.add(setHSD2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();


        }
        else if(fuel_type.equalsIgnoreCase("MS")){
            setMS1 = new LineDataSet(valuesMS1, tv_calendar_from.getText().toString());
            setMS2 = new LineDataSet(valuesMS2, tv_calendar_to.getText().toString());
//            setMS1.setValueTextColor(getResources().getColor(R.color.white));
            setMS1.setDrawIcons(false);
//            setMS1.enableDashedLine(10f, 0f, 0f);
//            setMS1.enableDashedHighlightLine(10f, 0f, 0f);
            setMS1.setColor(getResources().getColor(R.color.line_chart_ms_color));
            setMS1.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setMS1.setLineWidth(1f);
            setMS1.setCircleRadius(2f);
            setMS1.setDrawCircleHole(false);
            setMS1.setValueTextSize(0f);
            setMS1.setDrawFilled(true);
            setMS1.setFormLineWidth(1f);
//            setMS1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setMS1.setFormSize(15.f);
//            setMS2.setValueTextColor(getResources().getColor(R.color.white));
            setMS2.setDrawIcons(false);
//            setMS2.enableDashedLine(10f, 0f, 0f);
//            setMS2.enableDashedHighlightLine(10f, 0f, 0f);
            setMS2.setColor(getResources().getColor(R.color.line_chart_all_color));
            setMS2.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setMS2.setLineWidth(1f);
            setMS2.setCircleRadius(3f);
            setMS2.setDrawCircleHole(false);
            setMS2.setValueTextSize(0f);
            setMS2.setDrawFilled(true);
            setMS2.setFormLineWidth(1f);
//            setMS2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setMS2.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_ms);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setMS1.setFillDrawable(drawable);
                setMS2.setFillDrawable(drawable1);
            } else {
                setMS1.setFillColor(Color.DKGRAY);
                setMS2.setFillColor(Color.DKGRAY);
            }
            dataSets.add(setMS1);
            dataSets.add(setMS2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();


        }
        else if(fuel_type.equalsIgnoreCase("POWER")){
            setPower1 = new LineDataSet(valuesPower1, tv_calendar_from.getText().toString());
            setPower2 = new LineDataSet(valuesPower2, tv_calendar_to.getText().toString());
//            setPower1.setValueTextColor(getResources().getColor(R.color.white));
            setPower1.setDrawIcons(false);
//            setPower1.enableDashedLine(10f, 0f, 0f);
//            setPower1.enableDashedHighlightLine(10f, 0f, 0f);
            setPower1.setColor(getResources().getColor(R.color.line_chart_power_color));
            setPower1.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setPower1.setLineWidth(1f);
            setPower1.setCircleRadius(3f);
            setPower1.setDrawCircleHole(false);
            setPower1.setValueTextSize(0f);
            setPower1.setDrawFilled(true);
            setPower1.setFormLineWidth(1f);
//            setPower1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setPower1.setFormSize(15.f);
//            setPower2.setValueTextColor(getResources().getColor(R.color.white));
            setPower2.setDrawIcons(false);
//            setPower2.enableDashedLine(10f, 0f, 0f);
//            setPower2.enableDashedHighlightLine(10f, 0f, 0f);
            setPower2.setColor(getResources().getColor(R.color.line_chart_all_color));
            setPower2.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setPower2.setLineWidth(1f);
            setPower2.setCircleRadius(3f);
            setPower2.setDrawCircleHole(false);
            setPower2.setValueTextSize(0f);
            setPower2.setDrawFilled(true);
            setPower2.setFormLineWidth(1f);
//            setPower2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setPower2.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_power);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setPower1.setFillDrawable(drawable);
                setPower2.setFillDrawable(drawable1);
            } else {
                setPower1.setFillColor(Color.DKGRAY);
                setPower2.setFillColor(Color.DKGRAY);
            }
            dataSets.add(setPower1);
            dataSets.add(setPower2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();

        }
        else if(fuel_type.equalsIgnoreCase("TURBOJET")){
            setTurbojet1 = new LineDataSet(valuesTurbojet1, tv_calendar_from.getText().toString());
            setTurbojet2 = new LineDataSet(valuesTurbojet2, tv_calendar_to.getText().toString());
//            setTurbojet1.setValueTextColor(getResources().getColor(R.color.white));
            setTurbojet1.setDrawIcons(false);
//            setTurbojet1.enableDashedLine(10f, 0f, 0f);
//            setTurbojet1.enableDashedHighlightLine(10f, 0f, 0f);
            setTurbojet1.setColor(getResources().getColor(R.color.line_chart_turbojet_color));
            setTurbojet1.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setTurbojet1.setLineWidth(1f);
            setTurbojet1.setCircleRadius(2f);
            setTurbojet1.setDrawCircleHole(false);
            setTurbojet1.setValueTextSize(0f);
            setTurbojet1.setDrawFilled(true);
            setTurbojet1.setFormLineWidth(1f);
//            setTurbojet1.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setTurbojet1.setFormSize(15.f);
//            setTurbojet2.setValueTextColor(getResources().getColor(R.color.white));
            setTurbojet2.setDrawIcons(false);
//            setTurbojet2.enableDashedLine(10f, 0f, 0f);
//            setTurbojet2.enableDashedHighlightLine(10f, 0f, 0f);
            setTurbojet2.setColor(getResources().getColor(R.color.line_chart_all_color));
            setTurbojet2.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setTurbojet2.setLineWidth(1f);
            setTurbojet2.setCircleRadius(3f);
            setTurbojet2.setDrawCircleHole(false);
            setTurbojet2.setValueTextSize(0f);
            setTurbojet2.setDrawFilled(true);
            setTurbojet2.setFormLineWidth(1f);
//            setTurbojet2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setTurbojet2.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_turbojet);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setTurbojet1.setFillDrawable(drawable);
                setTurbojet2.setFillDrawable(drawable1);
            } else {
                setTurbojet1.setFillColor(Color.DKGRAY);
                setTurbojet2.setFillColor(Color.DKGRAY);
            }
            dataSets.add(setTurbojet1);
            dataSets.add(setTurbojet2);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();

        }
        else if(fuel_type.equalsIgnoreCase("POWER 99")){
            setPower991 = new LineDataSet(valuesPower991, tv_calendar_from.getText().toString());
            setPower992 = new LineDataSet(valuesPower992, tv_calendar_to.getText().toString());
            setPower991.setDrawIcons(false);
            setPower991.setColor(getResources().getColor(R.color.line_chart_turbojet_color));
            setPower991.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setPower991.setLineWidth(1f);
            setPower991.setCircleRadius(2f);
            setPower991.setDrawCircleHole(false);
            setPower991.setValueTextSize(0f);
            setPower991.setDrawFilled(true);
            setPower991.setFormLineWidth(1f);
            setPower991.setFormSize(15.f);
            setPower992.setDrawIcons(false);
            setPower992.setColor(getResources().getColor(R.color.line_chart_all_color));
            setPower992.setCircleColor(getResources().getColor(R.color.dot_light_screen2));
            setPower992.setLineWidth(1f);
            setPower992.setCircleRadius(3f);
            setPower992.setDrawCircleHole(false);
            setPower992.setValueTextSize(0f);
            setPower992.setDrawFilled(true);
            setPower992.setFormLineWidth(1f);
//            setTurbojet2.setFormLineDashEffect(new DashPathEffect(new float[]{10f, 5f}, 0f));
            setPower992.setFormSize(15.f);
            if (Utils.getSDKInt() >= 18) {
                Drawable drawable = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg_turbojet);
                Drawable drawable1 = ContextCompat.getDrawable(getContext(), R.drawable.gradient_bg2);
                setPower991.setFillDrawable(drawable);
                setPower992.setFillDrawable(drawable1);
            } else {
                setPower991.setFillColor(Color.DKGRAY);
                setPower992.setFillColor(Color.DKGRAY);
            }
            dataSets.add(setPower991);
            dataSets.add(setPower992);
            data = new LineData(dataSets);
            mChart.setData(data);
            mChart.invalidate();

        }
    }
}

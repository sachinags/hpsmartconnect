package com.agstransact.HP_SC.dashboard.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.adapter.Current_Price_Adapter;
import com.agstransact.HP_SC.dashboard.adapter.Nozzle_status_Adapter;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.model.Current_Price_Model;
import com.agstransact.HP_SC.model.Nozzle_status_Model;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

public class Fragmnet_CurrentPrice extends Fragment {
    private View rootView;
    private TextView tv_interlock_lbl,tv_date_lbl;
    private RecyclerView rv_interlock;
    private Current_Price_Adapter adapter;
    private ArrayList<Current_Price_Model> models = new ArrayList<>();
    String error="", respCode="";
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_current_price,container,false);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.iv_tlb_additional);
        toolbarTV.setText(getResources().getString(R.string.current_price));
        setUpViews();
        getCurrent_Price();
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rv_interlock.setLayoutManager(manager);
        adapter = new Current_Price_Adapter(getActivity(),models);
        rv_interlock.setAdapter(adapter);
        return rootView;
    }
    private void getCurrent_Price() {

        JSONObject json = new JSONObject();

//        try {
//            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Zone")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Selectdzone","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Region")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdRegion","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//
//
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Region")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdRegion","").equalsIgnoreCase("ALL")){
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//
//                    //                    try {
//                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//                    //
//                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "SelectdSalesArea",""));
//                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "FilteredRO",""));
//                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                    "FirstFilteredRO", "");
//                    //                        }else{
//                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "FilteredRO",""));
//                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                    "FirstFilteredRO", "");
//                    //                        }
//                    //                    } catch (Exception e) {
//                    //                        e.printStackTrace();
//                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                "FilteredRO",""));
//                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                "FirstFilteredRO", "");
//                    //                    }
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO",""));
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//            }
//            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
//                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "ROKey","") );
//            }
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray2 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("SalesArea", jsonObject2);
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdSalesArea",""));
                            String[] myArray3 = "ALL".split(",");
                            JSONArray jsonObject3 = new JSONArray(myArray3);
                            json.put("ROCode", jsonObject3);
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray2 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("Zone", jsonObject2 );
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("Region", jsonObject3 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray4 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject4 = new JSONArray(myArray4);
                                json.put("SalesArea", jsonObject4 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }



                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject5 = new JSONArray(myArray5);
                    json.put("ROCode", jsonObject5 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray = "ALL".split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                        }
                        else{
                            String[] myArray = "ALL".split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("Region", jsonObject2 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("SalesArea", jsonObject3);
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION"))
            {
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray1);
                            json.put("SalesArea", jsonObject1);
                            json.put("ROCode", jsonObject2);
                        }
                        else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject2);
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            }
                            else {
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                json.put("SalesArea", jsonObject1 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else{
//                            String[] myArray1 = "ALL".split(",");
//                            JSONArray jsonObject2 = new JSONArray(myArray1);
//                            json.put("SalesArea", jsonObject2 );
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FilteredRO",""));
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){

                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","").split(",");
                JSONArray jsonObject1 = new JSONArray(myArray);
                json.put("ROCode", jsonObject1 );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new Async_Current_Price().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class Async_Current_Price extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public Async_Current_Price() {
        }


        public Async_Current_Price(String strTime) {
            strTimeStamp = strTime;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
//            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetProductPrice", false, jsonObjects[0],"");
            return ClsWebService_hpcl.PostObjecttoken("DashboardV2/GetProductPrice", false, jsonObjects[0],"");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }

            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONArray jsonArray1 = null;
                            try {
                                jsonArray1 = new JSONArray(jsonObject.getString("data"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            JSONArray arraydata = null;
                            try {
                                arraydata = new JSONArray(jsonObject.getString("Data"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Iterator<String> keys = arraydata.getJSONObject(0).keys();
                            JSONArray images = new JSONArray();
                            while (keys.hasNext()) {
                                String key = keys.next();
                                String value = arraydata.getJSONObject(0).optString(key);
                                //add value to JSONArray from JSONObject
                                images.put(value);
                            }
                            JSONArray keys1 = arraydata.getJSONObject(0).names();
                            JSONArray values = arraydata.getJSONObject(0).toJSONArray(keys1);
                            JSONArray interlockarray = arraydata.getJSONObject(0).toJSONArray(arraydata.getJSONObject(0).names());

//                            List<InterlockModel> equipment_model = new ArrayList<>();
                            for (int i = 0; i < arraydata.length(); i++) {
                                models.add(new Current_Price_Model(
                                        arraydata.getJSONObject(i).getString("ProductName"),
                                        arraydata.getJSONObject(i).getString("Price"),
                                        arraydata.getJSONObject(i).getString("EffectiveDate")
                                ));
                            }

                            setData(models);

                           /* RecyclerView.LayoutManager mLayoutManager1 = new LinearLayoutManager(getActivity());
                            recycler_interlock.setLayoutManager(mLayoutManager1);
                            recycler_interlock.setItemAnimator(new DefaultItemAnimator());
                            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler_interlock.getContext(), LinearLayout.VERTICAL);
                            recycler_interlock.addItemDecoration(new SimpleDividerItemDecoration(
                                    getContext()
                            ));
                            recycler_interlock.addItemDecoration(dividerItemDecoration);
                            adapter = new Interlock_adapter(equipment_model, getActivity());
                            recycler_interlock.setAdapter(adapter);*/
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void setData(ArrayList<Current_Price_Model> models) {

        rv_interlock.setAdapter(new Current_Price_Adapter(getActivity(),models));
        adapter.notifyDataSetChanged();
    }

    private void setUpViews() {
        tv_interlock_lbl = rootView.findViewById(R.id.tv_interlock_lbl);
        rv_interlock = rootView.findViewById(R.id.rv_interlock);
        tv_date_lbl = rootView.findViewById(R.id.tv_date_lbl);
        tv_date_lbl.setSelected(true);

    }

}

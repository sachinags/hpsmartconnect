package com.agstransact.HP_SC.dashboard.fragment;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.adapter.Current_Price_Adapter;
import com.agstransact.HP_SC.dashboard.adapter.Equipment_Adapter;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.model.Current_Price_Model;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.RippleImageView;
import com.agstransact.HP_SC.utils.RippleLinearLayout;
import com.agstransact.HP_SC.utils.RippleTextView;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Fragmnet_Equipment extends Fragment implements View.OnClickListener{
    private View rootView;
    private TextView tv_interlock_lbl,tv_date_lbl,tv_duno;
    private RecyclerView rv_interlock;
    private Equipment_Adapter adapter;
    private ArrayList<Current_Price_Model> models = new ArrayList<>();
    String error="", respCode="";
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    ImageView iv_filter;
    private ImageView iv_fuel_sales_trends,iv_fuel_comparative_trends,iv_bnv_fuel_sale,
            iv_bnv_wet_inventory,iv_bnv_dashbord,iv_bnv_ro_snapshot,iv_bnv_equipment;
    RippleLinearLayout fuel_salesLL, wet_inventoryLL, dashboard_LL,ro_snapshotLL,equipmentLL;
    RippleImageView fuel_salesIV, wet_inventoryIV, dashboardIV,ro_snapshotIV,equipmentIV;
    RippleTextView fuels_salesTV,wet_inventoryTV,dashboardTV,ro_snapshot_TV,equipmentTV;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_equipment_new,container,false);
        setUpViews();
        getEquipment();
        LinearLayoutManager manager = new LinearLayoutManager(getActivity());
        rv_interlock.setLayoutManager(manager);
//        adapter = new Equipment_Adapter(getActivity(),models);
//        rv_interlock.setAdapter(adapter);
//        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
////        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.iv_tlb_additional);
//        toolbarTV.setText(getResources().getString(R.string.equipment));
        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    txnBundle.putString("Filter_fragment", "Equipment");
//                    fragment_filter_textview filter_screen = new fragment_filter_textview();
                    fragment_filter_textview_multiple_new filter_screen = new fragment_filter_textview_multiple_new();
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else{
                    UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {

                        }

                        @Override
                        public void methodCancel() {

                        }
                    },
                            "",
                            "No filter....."
                            , getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
                }
            }
        });
//        iv_bnv_fuel_sale.setOnClickListener(this);
//        iv_bnv_wet_inventory.setOnClickListener(this);
//        iv_bnv_dashbord.setOnClickListener(this);
//        iv_bnv_ro_snapshot.setOnClickListener(this);
//        iv_bnv_equipment.setOnClickListener(this);

        fuel_salesLL = (RippleLinearLayout)   rootView.findViewById(R.id.fuel_salesLL);
        wet_inventoryLL = (RippleLinearLayout)rootView.findViewById(R.id.wet_inventoryLL);
        dashboard_LL = (RippleLinearLayout)   rootView.findViewById(R.id.dashboard_LL);
        ro_snapshotLL = (RippleLinearLayout)  rootView.findViewById(R.id.ro_snapshotLL);
        equipmentLL = (RippleLinearLayout)    rootView.findViewById(R.id.equipmentLL);
        fuel_salesIV = (RippleImageView)    rootView.findViewById(R.id.fuel_salesIV);
        wet_inventoryIV = (RippleImageView) rootView.findViewById(R.id.wet_inventoryIV);
        dashboardIV = (RippleImageView)     rootView.findViewById(R.id.dashboardIV);
        ro_snapshotIV = (RippleImageView)   rootView.findViewById(R.id.ro_snapshotIV);
        equipmentIV = (RippleImageView)     rootView.findViewById(R.id.equipmentIV);
        fuels_salesTV = (RippleTextView)    rootView.findViewById(R.id.fuels_salesTV);
        wet_inventoryTV = (RippleTextView)  rootView.findViewById(R.id.wet_inventoryTV);
        dashboardTV = (RippleTextView)      rootView.findViewById(R.id.dashboardTV);
        ro_snapshot_TV = (RippleTextView)   rootView.findViewById(R.id.ro_snapshot_TV);
        equipmentTV = (RippleTextView)      rootView.findViewById(R.id.equipmentTV);
        equipmentIV.setImageResource(R.drawable.equipment_enabled_new_wt);

        fuel_salesLL.setOnClickListener(this);
        wet_inventoryLL.setOnClickListener(this);
        dashboard_LL.setOnClickListener(this);
        ro_snapshotLL.setOnClickListener(this);
        equipmentLL.setOnClickListener(this);
        fuel_salesIV.setOnClickListener(this);
        wet_inventoryIV.setOnClickListener(this);
        dashboardIV.setOnClickListener(this);
        ro_snapshotIV.setOnClickListener(this);
        equipmentIV.setOnClickListener(this);
        fuels_salesTV.setOnClickListener(this);
        wet_inventoryTV.setOnClickListener(this);
        dashboardTV.setOnClickListener(this);
        ro_snapshot_TV.setOnClickListener(this);
        equipmentTV.setOnClickListener(this);
        return rootView;
    }
    private void getEquipment() {

        JSONObject json = new JSONObject();
//        try {
//            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Zone")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Selectdzone","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Region")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdRegion","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Region")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdRegion","").equalsIgnoreCase("ALL")){
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }
//                        else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//
//                    //                    try {
//                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//                    //
//                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "SelectdSalesArea",""));
//                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "FilteredRO",""));
//                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                    "FirstFilteredRO", "");
//                    //                        }else{
//                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "FilteredRO",""));
//                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                    "FirstFilteredRO", "");
//                    //                        }
//                    //                    } catch (Exception e) {
//                    //                        e.printStackTrace();
//                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                "FilteredRO",""));
//                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                "FirstFilteredRO", "");
//                    //                    }
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO",""));
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//            }
//            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
//                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "ROKey","") );
//            }
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray2 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("SalesArea", jsonObject2);
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdSalesArea",""));
                            String[] myArray3 = "ALL".split(",");
                            JSONArray jsonObject3 = new JSONArray(myArray3);
                            json.put("ROCode", jsonObject3);
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray2 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("Zone", jsonObject2 );
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("Region", jsonObject3 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray4 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject4 = new JSONArray(myArray4);
                                json.put("SalesArea", jsonObject4 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }



                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject5 = new JSONArray(myArray5);
                    json.put("ROCode", jsonObject5 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray = "ALL".split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                        }
                        else{
                            String[] myArray = "ALL".split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("Region", jsonObject2 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("SalesArea", jsonObject3);
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION"))
            {
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray1);
                            json.put("SalesArea", jsonObject1);
                            json.put("ROCode", jsonObject2);
                        }
                        else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject2);
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            }
                            else {
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                json.put("SalesArea", jsonObject1 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else{
//                            String[] myArray1 = "ALL".split(",");
//                            JSONArray jsonObject2 = new JSONArray(myArray1);
//                            json.put("SalesArea", jsonObject2 );
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FilteredRO",""));
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){

                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","").split(",");
                JSONArray jsonObject1 = new JSONArray(myArray);
                json.put("ROCode", jsonObject1 );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new Async_Equipment().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.fuel_salesLL:
            case R.id.fuel_salesIV:
            case R.id.fuels_salesTV:
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                FragmentFuelSales fuelSales = new FragmentFuelSales();
                changeFrag.addFragment(fuelSales);
                break;

            case R.id.wet_inventoryLL:
            case R.id.wet_inventoryTV:
            case R.id.wet_inventoryIV:
                ActionChangeFrag changeFrag1 = (ActionChangeFrag) getActivity();
                FragmentWetInventory wet_inventory = new FragmentWetInventory();
                changeFrag1.addFragment(wet_inventory);
                break;

            case R.id.ro_snapshotLL:
            case R.id.ro_snapshotIV:
            case R.id.ro_snapshot_TV:
                ActionChangeFrag changeFrag2 = (ActionChangeFrag) getActivity();
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {

                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("Select RO") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "").equalsIgnoreCase("ALL") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("ALL")) {
                        UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                            @Override
                            public void methodDone() {

                            }

                            @Override
                            public void methodCancel() {

                            }
                        },
                                "",
                                "Please Select RO"
                                , getString(R.string.dialog_ok), "");
                        universalDialog.showAlert();
                    } else {
                        ActionChangeFrag changeFrag3 = (ActionChangeFrag) getActivity();
                        FragmentROSnapsahot ro_snapshot = new FragmentROSnapsahot();
                        changeFrag3.addFragment(ro_snapshot);
                    }
                }else{
                    ActionChangeFrag changeFrag3 = (ActionChangeFrag) getActivity();
                    FragmentROSnapsahot ro_snapshot = new FragmentROSnapsahot();
                    changeFrag3.addFragment(ro_snapshot);
                }
                break;
            case R.id.equipmentLL:
            case R.id.equipmentTV:
            case R.id.equipmentIV:
                ActionChangeFrag changeFrag4 = (ActionChangeFrag) getActivity();
                Fragmnet_Equipment equipment = new Fragmnet_Equipment();
                changeFrag4.addFragment(equipment);
                break;
            case R.id.dashboard_LL:
            case R.id.dashboardIV:
            case R.id.dashboardTV:
                ActionChangeFrag changeFrag7 = (ActionChangeFrag) getActivity();
                HomeFragment_old home_Frag = new HomeFragment_old();
                changeFrag7.addFragment(home_Frag);
                break;
        }

    }

    private class Async_Equipment extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public Async_Equipment() {
        }


        public Async_Equipment(String strTime) {
            strTimeStamp = strTime;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
//            return ClsWebService_hpcl.PostObjecttoken("PumpTank/ZoneWiseEquipmentListV2", false, jsonObjects[0],"");
            return ClsWebService_hpcl.PostObjecttoken("DashboardV2/GetZoneWiseEquipment", false, jsonObjects[0],"");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }

            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
//                            JSONObject jsonObjectdata = new JSONObject(jsonObject.getString("Data"));
//                            Iterator<String> keys = arraydata.getJSONObject(0).keys();

//                            Iterator<String> keys = jsonObjectdata.keys();
//                            JSONArray images = new JSONArray();
//                            while (keys.hasNext()) {
//                                String key = keys.next();
//
//                            }
//                            JSONArray interlockarray = jsonObjectdata.names();
                            JSONArray arrayPrice_Exception = new JSONArray(jsonObject.getString("Data"));

//                            JSONArray interlockarray = jsonObject.getString("Data");
//                            for(int i = 0;i<interlockarray.length();i++){
//
////                                for(int j = 0; j< interlockarray.getJSONObject(i).length();j++){
////
////                                }
////
//                            }
                            adapter = new Equipment_Adapter(getActivity(), arrayPrice_Exception);
                            rv_interlock.setAdapter(adapter);
//                            setData(models);

                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    private void setData(ArrayList<Current_Price_Model> models) {

        rv_interlock.setAdapter(new Current_Price_Adapter(getActivity(),models));
        adapter.notifyDataSetChanged();
    }

    private void setUpViews() {
        iv_bnv_fuel_sale = rootView.findViewById(R.id.iv_bnv_fuel_sale);
        iv_bnv_wet_inventory = rootView.findViewById(R.id.iv_bnv_wet_inventory);
        iv_bnv_dashbord = rootView.findViewById(R.id.iv_bnv_dashbord);
        iv_bnv_ro_snapshot = rootView.findViewById(R.id.iv_bnv_ro_snapshot);
        iv_bnv_equipment = rootView.findViewById(R.id.iv_bnv_equipment);
        tv_interlock_lbl = rootView.findViewById(R.id.tv_interlock_lbl);
        rv_interlock = rootView.findViewById(R.id.rv_interlock);
        iv_filter = rootView.findViewById(R.id.iv_filter);
        tv_duno = rootView.findViewById(R.id.tv_duno);
        tv_duno.setSelected(true);
//        tv_date_lbl = rootView.findViewById(R.id.tv_date_lbl);
//        tv_date_lbl.setSelected(true);

    }

}

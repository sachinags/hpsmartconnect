package com.agstransact.HP_SC.dashboard.table_sample;

import java.math.BigDecimal;
import java.util.Date;

public class Invoices {
    public InvoiceDate[] getInvoices() {
        InvoiceDate[] data = new InvoiceDate[5];
        for (int i = 0; i < 5; i++) {
            InvoiceDate row = new InvoiceDate();
            row.id = (i + 1);
            row.invoiceNumber = row.id;
            row.amountDue = BigDecimal.valueOf(20.00 * i);
            row.invoiceAmount = BigDecimal.valueOf(120.00 * (i + 1));
            row.invoiceDate = new Date();
            row.customerName = "Yuvan Shankar Raja";
            row.customerAddress = "1112, Chennai, India";
            data[i] = row;
        }
        return data;
    }
}
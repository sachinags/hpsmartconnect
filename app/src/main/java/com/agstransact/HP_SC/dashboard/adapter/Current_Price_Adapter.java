package com.agstransact.HP_SC.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.Current_Price_Model;
import com.agstransact.HP_SC.model.Dupump_Details_Model;

import java.util.ArrayList;

public class Current_Price_Adapter extends RecyclerView.Adapter<Current_Price_Adapter.MyInterlockModel> {
    private Context context;
    private ArrayList<Current_Price_Model> models = new ArrayList<>();

    public Current_Price_Adapter(Activity activity, ArrayList<Current_Price_Model> models) {
        this.context = activity;
        this.models = models;
    }


    @NonNull
    @Override
    public Current_Price_Adapter.MyInterlockModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.current_price_adapter,parent,false);
        return new MyInterlockModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Current_Price_Adapter.MyInterlockModel holder, int position) {

        Current_Price_Model model = models.get(position);

        holder.tv_product_value.setText(model.getProductName());
        holder.tv_price_value.setText(model.getStatus());
        holder.tv_date_value.setText(model.getEffectiveDate());

    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class MyInterlockModel extends RecyclerView.ViewHolder {

        private TextView tv_product_value,tv_price_value,tv_date_value;

        public MyInterlockModel(@NonNull View itemView) {
            super(itemView);

            tv_product_value = itemView.findViewById(R.id.tv_product_value);
            tv_price_value = itemView.findViewById(R.id.tv_price_value);
            tv_date_value = itemView.findViewById(R.id.tv_date_value);
        }
    }
}

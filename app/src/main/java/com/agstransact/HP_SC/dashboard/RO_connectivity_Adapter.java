package com.agstransact.HP_SC.dashboard;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.CombinedModel;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.RecyclerItemClickListner;

import java.util.ArrayList;

public class RO_connectivity_Adapter extends RecyclerView.Adapter<RO_connectivity_Adapter.MyViewModel> {
    private Context context;
    private ArrayList<CombinedModel> priceExceptionModels = new ArrayList<>();
    private static RecyclerItemClickListner itemClickListener;

    public RO_connectivity_Adapter(Context mContext, ArrayList<CombinedModel> models,RecyclerItemClickListner itemClickListener) {
        this.context = mContext;
        this.priceExceptionModels = models;
        this.itemClickListener = itemClickListener;

    }


    @NonNull
    @Override
    public RO_connectivity_Adapter.MyViewModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.list_ro_connectivity,parent,false);
        return new MyViewModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RO_connectivity_Adapter.MyViewModel holder, int position) {

        CombinedModel model = priceExceptionModels.get(position);
        String str= model.getCategoryName();
        str = str.replaceAll("[^a-zA-Z0-9]", " ");
        holder.tv_price_lbl.setSelected(true);
        holder.tv_price_lbl.setText(str);
        holder.tv_total_exception_value.setText(" "+model.getValue());
//        ArrayList<CategoryModel> categoryModel = priceExceptionModels.get(position).getCategoryData();
//        ArrayList<ValueModel> valueData = model.getValueData();
        //for (int i =0; i<=priceExceptionModels.size(); i++){
//            ArrayList<CombinedModel> categoryModel = model.;
//            CategoryModel model1 = categoryModel.get(i);

//            Log.e("lbl",model1.getCategoryName());
       // }
        /*for (int j =0; j<=priceExceptionModels.size(); j++){
            ArrayList<ValueModel> valueData = model.getValueData();
            ValueModel valueModel = valueData.get(j);
            holder.tv_total_exception_value.setText(" "+valueModel.getValue());
        }*/

    }

    @Override
    public int getItemCount() {
        Log.e("getSize",String.valueOf(priceExceptionModels.size()));
        return priceExceptionModels.size();

    }


    public class MyViewModel extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView tv_price_lbl,tv_total_exception_value;
        ImageView iv_download;

        public MyViewModel(@NonNull View itemView) {
            super(itemView);
            tv_price_lbl = itemView.findViewById(R.id.tv_price_lbl);
            tv_total_exception_value = itemView.findViewById(R.id.tv_total_exception_value);
            iv_download = itemView.findViewById(R.id.iv_download);
            if(UserDataPrefrence.getPreference("HPCL_Preference", context,
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                iv_download.setVisibility(View.VISIBLE);
            }else{
                iv_download.setVisibility(View.GONE);
            }
            iv_download.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick(tv_price_lbl.getText().toString(),
                    tv_total_exception_value.getText().toString(), getAdapterPosition());
        }
    }
}

package com.agstransact.HP_SC.dashboard.adapter.NewDesign;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;

import java.util.ArrayList;

public class tank_status_adapter extends RecyclerView.Adapter<tank_status_adapter.MyViewHolder> {

    private ArrayList<tank_model_new> tankmodel;
    Context context;

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tv_tank_capacityl_tr_label,tv_Net_volume_ltr_label,
                tv_product_label,tv_water_height_label,tv_temperature_label,
                tv_ullage_label,tv_stockdate_label;
        TextView tv_tank_lbl,tv_tank_val1,tv_critical_stock_label;
        ImageView iv_logo1,img_status,img_circle_status;

        public MyViewHolder(View itemView) {
            super(itemView);
            this.tv_tank_lbl = (TextView) itemView.findViewById(R.id.tv_tank_lbl);
            this.tv_tank_val1 = (TextView) itemView.findViewById(R.id.tv_tank_val1);
            this.tv_tank_capacityl_tr_label = (TextView) itemView.findViewById(R.id.tv_tank_capacityl_tr_label);
            this.tv_Net_volume_ltr_label = (TextView) itemView.findViewById(R.id.tv_Net_volume_ltr_label);
            this.tv_product_label = (TextView) itemView.findViewById(R.id.tv_product_label);
            this.tv_water_height_label = (TextView) itemView.findViewById(R.id.tv_water_height_label);
            this.tv_temperature_label = (TextView) itemView.findViewById(R.id.tv_temperature_label);
            this.tv_ullage_label = (TextView) itemView.findViewById(R.id.tv_ullage_label);
            this.tv_stockdate_label = (TextView) itemView.findViewById(R.id.tv_stockdate_label);
            this.iv_logo1 = (ImageView) itemView.findViewById(R.id.iv_logo1);
            this.img_status = (ImageView) itemView.findViewById(R.id.img_status);
            this.tv_critical_stock_label = (TextView) itemView.findViewById(R.id.tv_critical_stock_label);
            this.img_circle_status = (ImageView) itemView.findViewById(R.id.img_circle_status);
        }
    }

    public tank_status_adapter(ArrayList<tank_model_new> tankmdel, Context context) {
        this.tankmodel = tankmdel;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tank_adapter_new, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView tv_tank_lbl = holder.tv_tank_lbl;
        TextView tv_tank_val1 = holder.tv_tank_val1;
        TextView tv_tank_capacityl_tr_label = holder.tv_tank_capacityl_tr_label;
        TextView tv_Net_volume_ltr_label = holder.tv_Net_volume_ltr_label;
        TextView tv_product_label = holder.tv_product_label;
        TextView tv_water_height_label = holder.tv_water_height_label;
        TextView tv_temperature_label = holder.tv_temperature_label;
        TextView tv_ullage_label = holder.tv_ullage_label;
        TextView tv_stockdate_label = holder.tv_stockdate_label;
        ImageView iv_logo1 = holder.iv_logo1;
        ImageView img_status = holder.img_status;
        TextView tv_critical_stock_label = holder.tv_critical_stock_label;
        final ImageView img_circle_status = holder.img_circle_status;

        try {
            tv_tank_lbl.setText("Tank: "+tankmodel.get(listPosition).getTankNo()+"/"+tankmodel.get(listPosition).getProduct());
            tv_tank_val1.setText(tankmodel.get(listPosition).getCurrent()+ "%");
            tv_tank_capacityl_tr_label.setText("Capacity(Ltr) :"+tankmodel.get(listPosition).getCapacity());
            tv_Net_volume_ltr_label.setText("Net Volume(Ltr) :"+tankmodel.get(listPosition).getNetVolume());
            tv_product_label.setText("Product_height(mm) :"+tankmodel.get(listPosition).getProductHeight());
            tv_water_height_label.setText("Water Height(mm) :"+tankmodel.get(listPosition).getWaterHeight());
            tv_temperature_label.setText("Temperature(C) :"+tankmodel.get(listPosition).getTemparature());
            tv_ullage_label.setText("Ullage(Ltr) :"+tankmodel.get(listPosition).getUllage());
            tv_stockdate_label.setText("StockDate :"+tankmodel.get(listPosition).getSTKDateTime());
            tv_tank_val1.setSelected(true);
            tv_tank_capacityl_tr_label.setSelected(true);
            tv_Net_volume_ltr_label.setSelected(true);
            tv_product_label.setSelected(true);
            tv_water_height_label.setSelected(true);
            tv_temperature_label.setSelected(true);
            tv_ullage_label.setSelected(true);
            tv_stockdate_label.setSelected(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            if(tankmodel.get(listPosition).getProduct().equalsIgnoreCase("HSD")){
                iv_logo1.setImageResource(R.drawable.hsd_tank);
            }else if(tankmodel.get(listPosition).getProduct().equalsIgnoreCase("MS")){
                iv_logo1.setImageResource(R.drawable.ms_tank);
            }else if(tankmodel.get(listPosition).getProduct().equalsIgnoreCase("POWER")){
                iv_logo1.setImageResource(R.drawable.power_color);
            }else if(tankmodel.get(listPosition).getProduct().equalsIgnoreCase("TURBOJET")){
                iv_logo1.setImageResource(R.drawable.turboject_tank);
            }else if(tankmodel.get(listPosition).getProduct().equalsIgnoreCase("POWER 99")){
                iv_logo1.setImageResource(R.drawable.power_99_tank_new);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            int percentage = Integer.parseInt(tankmodel.get(listPosition).getCurrent());
            if(percentage > 15){
    //            iv_logo1.setImageResource(tankmodel.get(listPosition).getTank_image());
//                iv_logo1.setImageResource(R.drawable.green_tank_big);
//                img_circle_status.setImageResource(R.drawable.green_dot1);
                img_circle_status.setVisibility(View.GONE);
                tv_critical_stock_label.setVisibility(View.GONE);
//                img_circle_status.setImageResource(R.drawable.alert_sign);
//                Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
//                animation.setDuration(1000); //1 second duration for each animation cycle
//                animation.setInterpolator(new LinearInterpolator());
//                animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
//                animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
//                img_circle_status.startAnimation(animation); //to start animation


            }else{
//                iv_logo1.setImageResource(R.drawable.red_tank_big);
//                img_circle_status.setImageResource(R.drawable.red_dot);
                img_circle_status.setVisibility(View.GONE);
                img_circle_status.setImageResource(R.drawable.alert_sign);
                tv_critical_stock_label.setVisibility(View.VISIBLE);
//                img_circle_status.setVisibility(View.VISIBLE);
                Animation animation = new AlphaAnimation(1, 0); //to change visibility from visible to invisible
                animation.setDuration(1000); //1 second duration for each animation cycle
                animation.setInterpolator(new LinearInterpolator());
                animation.setRepeatCount(Animation.INFINITE); //repeating indefinitely
                animation.setRepeatMode(Animation.REVERSE); //animation will start from end point once ended.
                img_circle_status.startAnimation(animation); //to start animation

//                new Thread( new Runnable() {
//                    @Override public void run() {
//                        int count = 0;
//                        if(count == 0){
//                            img_circle_status.setImageResource(R.drawable.alert_sign);
//                            count++;
//                            if(count > 0){
//                                count--;
//                                img_circle_status.setImageResource(R.drawable.alert_sign);
//                            }
//
//                        }
//                }
//                }
//                ).start();

            }
            if(tankmodel.get(listPosition).getOnline().equalsIgnoreCase("1")){
                img_status.setImageResource(R.drawable.right_image);
            }else{
                img_status.setImageResource(R.drawable.wrong_image);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return tankmodel.size();
    }
}

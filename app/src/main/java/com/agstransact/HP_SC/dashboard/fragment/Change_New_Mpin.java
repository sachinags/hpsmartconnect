package com.agstransact.HP_SC.dashboard.fragment;

import android.app.Activity;
import android.app.Service;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.agstransact.HP_SC.R;

import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsKeyboardUtil;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.PinEntryEditText;
import com.agstransact.HP_SC.utils.UniversalDialog;

public class Change_New_Mpin extends Fragment implements AlertDialogInterface{
    PinEntryEditText mPinEntry;
    String strMPIN;
    AlertDialogInterface dialogInterface;
    UniversalDialog universalDialog;
    Bundle bundle;
    Button btn_send;
    LinearLayout rootLL;
    TextView tv_enter_mpin;
    int[] intMpin;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_change_pin_new, container, false);
        //16-1-19
        setHasOptionsMenu(true);
        bundle = getArguments();

        intializeID(view);
        dialogInterface = this;
//        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//        toolbarTV.setText(getResources().getString(R.string.ChangePin));
        ClsKeyboardUtil objKeyBoardUtil;
        objKeyBoardUtil = new ClsKeyboardUtil(getActivity());
        objKeyBoardUtil.setupUI(rootLL);

        final Bundle bundle = getArguments();


        if (mPinEntry != null) {
            mPinEntry.setOnPinEnteredListener(new PinEntryEditText.OnPinEnteredListener() {
                @Override
                public void onPinEntered(CharSequence str) {
                    strMPIN = str.toString();
                    if (str.length() == 4) {
                        try{
                            if (ConstantDeclaration.gifProgressDialog != null) {
                                if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                                    ConstantDeclaration.showGifProgressDialog(getActivity());
                                }
                            } else {
                                ConstantDeclaration.showGifProgressDialog(getActivity());
                            }
                            try {
                                int[] intArray = new int[4];
                                for (int i = 0; i < 4; i++) {
                                    intArray[i] = Integer.parseInt(String.valueOf(str.charAt(i)));
                                }
                                intMpin = intArray;

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
//                            intMpin = universalDialog.getIntFromString(mPinEntry.getText().toString().trim());
                            if( checkPinSequence()){
                                try {
                                    if (ConstantDeclaration.gifProgressDialog.isShowing())
                                        ConstantDeclaration.gifProgressDialog.dismiss();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                Bundle b = new Bundle();
                                Fragment frag = null;
                                frag = new Change_New_Confirm_Mpin();
                                b.putString("OLDMPIN",bundle.getString("OLDMPIN"));
                                b.putString("NEWMPIN",strMPIN);
                                frag.setArguments(b);
                                clearPIN();
                                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                                changeFrag.addFragment(frag);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        universalDialog = new UniversalDialog(getActivity(),
                                dialogInterface, "", "Please enter Valid MPIN", getString(R.string.got_it), "");
                        universalDialog.showAlert();
                    }
                }
            });
        }

        mPinEntry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() == 4) {
//                    btn_send.setEnabled(true);
//                    btn_send.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.button_bg));
                    //riteshb 22-11
//                    checkValidPIN(mPinEntry.getText().toString());
                } else {
                    btn_send.setEnabled(false);
                    btn_send.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                }
            }
        });
        tv_enter_mpin.setText("Enter your new MPIN");

        return view;
    }

    private void intializeID(View view) {
        mPinEntry = (PinEntryEditText) view.findViewById(R.id.pinEntryET);
        btn_send = (Button) view.findViewById(R.id.btn_send);
        tv_enter_mpin = (TextView) view.findViewById(R.id.tv_enter_mpin);
        rootLL = (LinearLayout) view.findViewById(R.id.rootLL);
    }

    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }
    @Override
    public void methodDone() {

    }

    @Override
    public void methodCancel() {

    }

    //16-1-19
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void clearPIN() {
        mPinEntry.setText("");
    }
    boolean checkPinSequence() {
        if (checkBackwardPinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else if (checkForwardPinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else if (checkSamePinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else {
            return true;
        }

    }

    public boolean checkForwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] + 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("ForwardPin", "false");
                return false;
            }
        }
        Log.e("ForwardPin", "true");
        return isSequence;
    }
    public boolean checkBackwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] - 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("BackwardPin", "false");
                return false;
            }
        }
        Log.e("BackwardPin", "true");
        return isSequence;
    }
    public boolean checkSamePinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length; i++) {
            for (int j = i + 1; j < mPin.length; j++) {
                if (mPin[i] == mPin[j]) {
                    isSequence = true;
                } else {
                    Log.e("SamePin", "false");
                    return false;
                }
            }
        }
        Log.e("SamePin", "true");
        return isSequence;
    }
//    @Override
//    public void onBackPressed(){
//        List<Fragment> fragments = getFragmentManager().getFragments();
//        if (fragments != null) {
//            for (Fragment fragment : fragments) {
//                if (!fragment.isVisible()) continue;
//
//                if (fragment instanceof BaseFragment && ((BaseFragment) fragment).onBackPressed()) {
//                    return;
//                }
//            }
//        }
//        super.onBackPressed();

//    }
    @Override
    public void onResume() {
        super.onResume();
        /*for handdling the backpressed of the device*/

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    Bundle bundle = new Bundle();
//                    getActivity().onBackPressed();

                    // handle back button
                    FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                    for (int i = 0; i < fragmentManager.getBackStackEntryCount(); ++i) {
                        fragmentManager.popBackStack();
//                    }
//                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
//                    changeFrag.addFragment(new Change_Mpin());
//                    Change_Mpin fragment =new Change_Mpin();
//                    ConstantDeclaration.replaceFragment(fragment, fragmentManager);


                    return true;

                }

                return false;
            }
        });
    }
}
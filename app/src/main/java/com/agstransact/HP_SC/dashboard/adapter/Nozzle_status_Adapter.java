package com.agstransact.HP_SC.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.InterlockModel;
import com.agstransact.HP_SC.model.Nozzle_status_Model;

import java.util.ArrayList;

public class Nozzle_status_Adapter extends RecyclerView.Adapter<Nozzle_status_Adapter.MyInterlockModel> {
    private Context context;
    private ArrayList<Nozzle_status_Model> models = new ArrayList<>();

    public Nozzle_status_Adapter(Activity activity, ArrayList<Nozzle_status_Model> models) {
        this.context = activity;
        this.models = models;
    }


    @NonNull
    @Override
    public Nozzle_status_Adapter.MyInterlockModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.nozzle_status_ro_adapter,parent,false);
        return new MyInterlockModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Nozzle_status_Adapter.MyInterlockModel holder, int position) {

        Nozzle_status_Model model = models.get(position);

        holder.tv_product_value.setText(model.getProduct());
        holder.tv_nozzle_conf_value.setText(model.getNozzlesConfigured());
        holder.tv_nozzle_in_use_value.setText(model.getNozzlesinUse());
        holder.tv_nozzle_not_in_use_value.setText(model.getNozzlesDisabled());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class MyInterlockModel extends RecyclerView.ViewHolder {

        private TextView tv_product_value,tv_nozzle_conf_value,tv_nozzle_in_use_value,tv_nozzle_not_in_use_value;

        public MyInterlockModel(@NonNull View itemView) {
            super(itemView);

            tv_product_value = itemView.findViewById(R.id.tv_product_value);
            tv_nozzle_conf_value = itemView.findViewById(R.id.tv_nozzle_conf_value);
            tv_nozzle_in_use_value = itemView.findViewById(R.id.tv_nozzle_in_use_value);
            tv_nozzle_not_in_use_value = itemView.findViewById(R.id.tv_nozzle_not_in_use_value);
        }
    }
}

package com.agstransact.HP_SC.dashboard.table_sample;

import java.math.BigDecimal;
import java.util.Date;

public class InvoiceDate {
    public int id;
    public int invoiceNumber;
    public Date invoiceDate;
    public String customerName;
    public String customerAddress;
    public BigDecimal invoiceAmount;
    public BigDecimal amountDue;
}
package com.agstransact.HP_SC.dashboard.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.model.CombinedModel;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FragmentAverageNozzle extends Fragment implements  AlertDialogInterface {

    private View rootView;
    private PieChart pieChart;
    PieData pieData;
    PieDataSet pieDataSet;
    ArrayList pieEntries;
    ArrayList PieEntryLabels;
    private String error="", respCode="",fuel_type="";
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    public static JSONArray arraymonth,arrayunitsSold,arrayunitsBought;
    List<String> listmonth ,listpiemonth;
    List<Integer> listunitsSold,listunitsBought;
    List<Integer>lispietunitsSold;
    String Unit;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_average_nozzle,container,false);
        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.iv_tlb_additional);
        toolbarTV.setText(getResources().getString(R.string.avg_nozzle_tp));
        setUpViews();
        Unit =(" Ltrs");

//        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                "UserCustomRole","").equalsIgnoreCase("DEALER")) {
//            Unit =(" Ltrs");
//        }else{
//            Unit = (" L");
//        }
        getAvg_Nozzle();
//        showPieCharData();
        return rootView;
    }
    private void getEntries() {
        pieEntries = new ArrayList<>();
        for(int i =0;i <lispietunitsSold.size();i++){
            float f = (float) lispietunitsSold.get(i) / 1;
            pieEntries.add(new PieEntry(f, listpiemonth.get(i)));

        }

    }
    private void getAvg_Nozzle() {

        JSONObject json = new JSONObject();
//        try {
//            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Zone")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "Selectdzone","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Region")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdRegion","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone",""));
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "Selectdzone", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "Selectdzone", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//
//
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("Region")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdRegion","").equalsIgnoreCase("ALL")){
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion",""));
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FirstFilteredRO", "");
//                    }
//
//                    //                    try {
//                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//                    //
//                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "SelectdSalesArea",""));
//                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "FilteredRO",""));
//                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                    "FirstFilteredRO", "");
//                    //                        }else{
//                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                    "FilteredRO",""));
//                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                    "FirstFilteredRO", "");
//                    //                        }
//                    //                    } catch (Exception e) {
//                    //                        e.printStackTrace();
//                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    //                                "FilteredRO",""));
//                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    //                                "FirstFilteredRO", "");
//                    //                    }
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FilteredRO",""));
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                            "FirstFilteredRO",""));
//                }
//            }
//            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
//                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
//                        "ROKey","") );
//            }
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray2 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject1);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone","").split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("Zone", jsonObject);
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "Selectdzone",""));
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("SalesArea", jsonObject2);
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdSalesArea",""));
                            String[] myArray3 = "ALL".split(",");
                            JSONArray jsonObject3 = new JSONArray(myArray3);
                            json.put("ROCode", jsonObject3);
//                                json.put("ROCode", "ALL");
                        }else{
                            String[] myArray2 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray2);
                            json.put("ROCode", jsonObject2);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("Zone", jsonObject2 );
//                                    json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("Region", jsonObject3 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray4 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject4 = new JSONArray(myArray4);
                                json.put("SalesArea", jsonObject4 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject5 = new JSONArray(myArray5);
                        json.put("ROCode", jsonObject5 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }



                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray5 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject5 = new JSONArray(myArray5);
                    json.put("ROCode", jsonObject5 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            String[] myArray1 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray1);
                            json.put("Region", jsonObject1);
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                        "SelectdRegion",""));
                            String[] myArray = "ALL".split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                        }
                        else{
                            String[] myArray = "ALL".split(",");
                            JSONArray jsonObject = new JSONArray(myArray);
                            json.put("ROCode", jsonObject);
//                                json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray2 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion","").split(",");
                                JSONArray jsonObject2 = new JSONArray(myArray2);
                                json.put("Region", jsonObject2 );
//                                    json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                String[] myArray3 = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject3 = new JSONArray(myArray3);
                                json.put("SalesArea", jsonObject3);
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION"))
            {
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","").split(",");
                            JSONArray jsonObject1 = new JSONArray(myArray);
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray1);
                            json.put("SalesArea", jsonObject1);
                            json.put("ROCode", jsonObject2);
                        }
                        else{
                            String[] myArray1 = "ALL".split(",");
                            JSONArray jsonObject2 = new JSONArray(myArray1);
                            json.put("ROCode", jsonObject2);
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            }
                            else {
                                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea","").split(",");
                                JSONArray jsonObject1 = new JSONArray(myArray);
                                json.put("SalesArea", jsonObject1 );
//                                    json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                            "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                    else{
//                            String[] myArray1 = "ALL".split(",");
//                            JSONArray jsonObject2 = new JSONArray(myArray1);
//                            json.put("SalesArea", jsonObject2 );
                        String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO","").split(",");
                        JSONArray jsonObject1 = new JSONArray(myArray);
                        json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(,
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                    "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FilteredRO",""));
                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){

                    String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO","").split(",");
                    JSONArray jsonObject1 = new JSONArray(myArray);
                    json.put("ROCode", jsonObject1 );
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(,
//                                "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                String[] myArray = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","").split(",");
                JSONArray jsonObject1 = new JSONArray(myArray);
                json.put("ROCode", jsonObject1 );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new AsyncAvg_Nozzle().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    @Override
    public void methodDone() {
        if(respCode.equalsIgnoreCase("401")){
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            startActivity(intent);
            getActivity().finish();
        }
    }

    @Override
    public void methodCancel() {

    }

    private class AsyncAvg_Nozzle extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public AsyncAvg_Nozzle() {
        }

        public AsyncAvg_Nozzle(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
//            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetNozzleThroughput", false, jsonObjects[0], "");
            return ClsWebService_hpcl.PostObjecttoken("DashboardV2/GetNozzleThrougput", false, jsonObjects[0], "");
        }


        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject jsonObject1 = new JSONObject(jsonObject.getString("Data"));
                            arraymonth = new JSONArray(jsonObject1.getString("months"));
                            arrayunitsSold = new JSONArray(jsonObject1.getString("unitsSold"));
                            listpiemonth = new ArrayList<String>();
                            lispietunitsSold = new ArrayList<Integer>();
                            for (int i1 = 0; i1 < arraymonth.length(); i1++) {
                                listpiemonth.add(arraymonth.getString(i1));
                            }
                            for (int unitsold = 0; unitsold < arrayunitsSold.length(); unitsold++) {
                                lispietunitsSold.add(arrayunitsSold.getInt(unitsold));
                            }
                            getEntries();
                            pieDataSet = new PieDataSet(pieEntries, "");
                            pieData = new PieData(pieDataSet);
                            pieChart.setData(pieData);
                            pieChart.getData().setDrawValues(false);
                            pieChart.setDrawEntryLabels(false);
//                            pieChart.setDrawSliceText(false);﻿
                            final int[] MY_COLORS = {Color.rgb(0, 0, 254), Color.rgb(0, 128, 1), Color.rgb(249, 40, 39),
                                    Color.rgb(201, 108, 56),Color.rgb(5, 1, 3)};
//                                    Color.rgb(5, 1, 3),Color.rgb(5, 1, 3),Color.rgb(5, 1, 3)};
                            ArrayList<Integer> colors = new ArrayList<Integer>();

                            for (int c : MY_COLORS) colors.add(c);
                            pieChart.getLegend().setEnabled(false);
//                        pieChart.getLegend().setTextColor(Color.WHITE);

//                        pieDataSet.setColors(colors);
                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
//                        pieDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.WHITE);
                            pieDataSet.setValueTextSize(10f);
                            pieChart.getDescription().setEnabled(false);
                            LegendEntry[] legendEntries=new LegendEntry[pieEntries.size()];
                            for(int i=0;i<legendEntries.length;i++)
                            {
                                LegendEntry entry=new LegendEntry();
                                entry.formColor=MY_COLORS[i];
                                try {
                                    entry.label=listpiemonth.get(i)+ " = "+lispietunitsSold.get(i)+ Unit;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                legendEntries[i]=(entry);

                            }
                            pieChart.getLegend().setCustom(legendEntries);
                            pieChart.getLegend().setWordWrapEnabled(true);
                            pieChart.setExtraBottomOffset(60f);
                            pieChart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_LEFT);
                            pieChart.getLegend().setEnabled(true);
                            pieChart.getLegend().setTextColor(getResources().getColor(R.color.black));
                            pieChart.getLegend().setTextSize(10f);
                            pieChart.getLegend().setYOffset(-10f);
                            pieChart.getLegend().setXOffset(10f);
                            pieChart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
                            pieChart.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                            pieChart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                            pieChart.getLegend().setWordWrapEnabled(true);

//                            pieChart.setCenterText("");
//                            pieChart.setCenterTextColor();
//                        pieDataSet.setSliceSpace(5f);
                            pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                                @Override
                                public void onValueSelected(Entry e, Highlight h) {
                                    PieEntry pe = (PieEntry) e;
                                    pieChart.setCenterText(pe.getLabel()+"\n"+ConstantDeclaration.amountFormatter(Double.valueOf(e.getY())) + Unit);
//                                    pieChart.setCenterText(pe.getLabel()+"\n"+e.getY());
                                    pieChart.setCenterTextColor(getContext().getResources().getColor(R.color.black));


                                }

                                @Override
                                public void onNothingSelected() {
                                    pieChart.setCenterText("");

                                }
                            });
                            pieChart.highlightValue(0, 0, false);
                            pieChart.setCenterText(arraymonth.getString(0)+"\n"+arrayunitsSold.getInt(0)+Unit);
                            pieChart.setCenterTextColor(getResources().getColor(R.color.black));


                            pieChart.invalidate();

                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }


//    private void showPieCharData() {
//
//        getEntries();
//        pieDataSet = new PieDataSet(pieEntries, "");
//        pieData = new PieData(pieDataSet);
//        pieChart.setData(pieData);
//        pieDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
//        pieDataSet.setSliceSpace(2f);
//        pieDataSet.setValueTextColor(Color.WHITE);
//        pieDataSet.setValueTextSize(10f);
//        pieDataSet.setSliceSpace(5f);
//
//    }
//
//    private void getEntries() {
//        pieEntries = new ArrayList<>();
//        pieEntries.add(new PieEntry(2f, 0));
//        pieEntries.add(new PieEntry(4f, 1));
//        pieEntries.add(new PieEntry(6f, 2));
//        pieEntries.add(new PieEntry(8f, 3));
//        pieEntries.add(new PieEntry(7f, 4));
//        pieEntries.add(new PieEntry(3f, 5));
//    }

    private void setUpViews() {
        pieChart = (PieChart) rootView.findViewById(R.id.piechart_average_nozzle);
    }
}

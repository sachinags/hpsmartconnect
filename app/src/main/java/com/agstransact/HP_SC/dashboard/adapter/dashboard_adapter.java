package com.agstransact.HP_SC.dashboard.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.dashboard_fragment.Critical_Stock_fragment;
import com.agstransact.HP_SC.dashboard.dashboard_fragment.Cumulative_Sales_fragment;
import com.agstransact.HP_SC.dashboard.dashboard_fragment.Nano_Status_fragment;
import com.agstransact.HP_SC.dashboard.dashboard_fragment.Price_Exception_fragment;
import com.agstransact.HP_SC.dashboard.dashboard_fragment.RO_Connectivity_fragment;
import com.agstransact.HP_SC.dashboard.dashboard_fragment.Todays_Sales_fragment;
import com.agstransact.HP_SC.dashboard.dashboard_fragment.Yesterdays_Sales_fragment;
import com.agstransact.HP_SC.dashboard.fragment.HomeFragment_new;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.RecyclerItemClickListner;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.github.mikephil.charting.data.BarData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class dashboard_adapter extends RecyclerView.Adapter<dashboard_adapter.ViewHolder>{
    private MyListData[] listdata;
    private static RecyclerItemClickListner itemClickListener;
    Context mContext;
    private String error="", respCode="",fuel_type="",graph_open="";
    public static JSONArray arraymonth1,arrayHSD1,arrayMS1,arrayPower1,arrayTurbojet1;
    public static JSONArray arraymonth2,arrayHSD2,arrayMS2,arrayPower2,arrayTurbojet2;
    int biggestmonthmain1,biggest_HSDmain1,biggestMSmain1,biggestPowermain1,biggestTurbojetmain1;
    int smallestmonthmain1,smallest_HSDmain1,smallestMSmain1,smallestPowermain1,smallestTurbojetmain1;
    int biggestmonthmain2,biggest_HSDmain2,biggestMSmain2,biggestPowermain2,biggestTurbojetmain2;
    int smallestmonthmain2,smallest_HSDmain2,smallestMSmain2,smallestPowermain2,smallestTurbojetmain2;
    // RecyclerView recyclerView;
    int mExpandedPosition = -1;
    int lastid;
    public dashboard_adapter(MyListData[] listdata,RecyclerItemClickListner itemClickListener,Context mContext) {
        this.listdata = listdata;
        this.itemClickListener = itemClickListener;
        this.mContext =mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.dashboard_item_new, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final MyListData myListData = listdata[position];
        holder.textView.setText(listdata[position].getDescription());
//        holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;;
//        holder.item_layout_main.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT));
        holder.item_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if(listdata[position].getDescription().equalsIgnoreCase("Today's Sale")){
//                    holder.ll_todaysale_graph.setVisibility(View.VISIBLE);
//                    try {
//                        holder.ll_yesterday_graph.setVisibility(View.GONE);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    notifyItemChanged(position);
//                }
//                else{
//                    try {
//                        holder.ll_todaysale_graph.setVisibility(View.GONE);
//                        holder.ll_yesterday_graph.setVisibility(View.VISIBLE);
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    notifyItemChanged(position);
//                }
//                holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
////                holder.item_layout_main.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.MATCH_PARENT));
////                holder.item_layout_main.setLayoutParams(new ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.MATCH_PARENT));
//                Toast.makeText(view.getContext(),"click on item: "+myListData.getDescription(),Toast.LENGTH_LONG).show();
//                itemClickListener.onItemClick(myListData.getDescription(),
//                        "", position);
//                if(graph_open.equalsIgnoreCase("")){
                    FragmentManager fragmentManager = ((AppCompatActivity)mContext).getSupportFragmentManager();
                    int currentContainerId = holder.container.getId();
                    Fragment fragment1;
                    String name = ((AppCompatActivity)mContext).getSupportFragmentManager().getClass().getName();
                Fragment currentFragment = null;
                try {
                    currentFragment = fragmentManager.findFragmentById(lastid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                Todays_Sales_fragment myFragment = (Todays_Sales_fragment)fragmentManager.findFragmentByTag("Todays_Sales_fragment");
//                if (myFragment != null && myFragment.isVisible()) {
//                    ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction().remove(currentFragment).commit();
//                }
                if (currentFragment != null) {
                    ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction().remove(currentFragment).commit();
                     holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                    notifyDataSetChanged();
//                    holder.container.setVisibility(View.GONE);
//                    holder.item_layout_graph.setVisibility(View.GONE);
                }
//                    try {
//                        for (Fragment fragment : ((AppCompatActivity) mContext).getSupportFragmentManager().getFragments()) {
//                            ((AppCompatActivity) mContext).getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

//                    if(Fragment.class.getName().equalsIgnoreCase("Todays_Sales_fragment")){
//
//                    }
//                    Fragment oldFragment = fragmentManager.findFragmentById(currentContainerId);
//                    Fragment Todays_Sales_fragment ;
//                    if(oldFragment != null) {
//                        // Delete fragmet from ui, do not forget commit() otherwise no action
//                        // is going to be observed
//                        fragmentManager.beginTransaction().remove(oldFragment).commit();
//                    }
                    int newContainerId = getUniqueId();
                    holder.container.setId(newContainerId);
//                    holder.container.removeAllViews();
                    if(position == 0){
                        if(graph_open.equalsIgnoreCase("TS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack("Todays_Sales_fragment").commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="TS";
                        }
                        else if(graph_open.equalsIgnoreCase("YS")){
                            graph_open ="";
//                            ((ViewGroup)holder.container.getParent()).removeView(holder.container);
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                            ConstantDeclaration.replaceFragment1(new Todays_Sales_fragment(), fragmentManager);
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            lastid = holder.container.getId();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="TS";
//                            notifyDataSetChanged();
//                            if(holder.container.getVisibility() == View.VISIBLE){
//
//                            }else{
//                                holder.item_layout_graph.setVisibility(View.VISIBLE);
//                                holder.container.setVisibility(View.VISIBLE);
//                                fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).commit();
//                                holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
//                                // Once all fragment replacement is done we can show the hidden container
//
//                                itemClickListener.onItemClick(myListData.getDescription(),
//                                        "", position);
//                                graph_open ="TS";
//                            }


                        }
                        else if(graph_open.equalsIgnoreCase("CS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack("Todays_Sales_fragment").commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="TS";

                        }
                        else if(graph_open.equalsIgnoreCase("RCS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack("Todays_Sales_fragment").commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="TS";
                        }
                        else if(graph_open.equalsIgnoreCase("NS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack("Todays_Sales_fragment").commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="TS";
                        }
                        else if(graph_open.equalsIgnoreCase("PS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack("Todays_Sales_fragment").commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="TS";
                        }
                        else if(graph_open.equalsIgnoreCase("CRS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack("Todays_Sales_fragment").commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="TS";
                        }
                        else{
//                            ConstantDeclaration.replaceFragment1(new Todays_Sales_fragment(), fragmentManager);
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Todays_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            holder.container.setId(holder.container.getId());
                            lastid = holder.container.getId();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="TS";
                        }

                    }
                    else if(position == 1){
                        if(graph_open.equalsIgnoreCase("TS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(holder.container.getId(), new Yesterdays_Sales_fragment()).commit();
//                            fragmentManager.beginTransaction().hide(new Todays_Sales_fragment());
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            lastid =holder.container.getId();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="YS";
                        }
                        else if(graph_open.equalsIgnoreCase("YS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                        }
                        else if(graph_open.equalsIgnoreCase("CS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Yesterdays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
//                            fragmentManager.beginTransaction().hide(new Todays_Sales_fragment());
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="YS";
                        }
                        else if(graph_open.equalsIgnoreCase("RCS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Yesterdays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
//                            fragmentManager.beginTransaction().hide(new Todays_Sales_fragment());
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="YS";
                        }
                        else if(graph_open.equalsIgnoreCase("NS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Yesterdays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
//                            fragmentManager.beginTransaction().hide(new Todays_Sales_fragment());
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="YS";
                        }
                        else if(graph_open.equalsIgnoreCase("PS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Yesterdays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
//                            fragmentManager.beginTransaction().hide(new Todays_Sales_fragment());
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="YS";
                        }
                        else if(graph_open.equalsIgnoreCase("CRS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Yesterdays_Sales_fragment()).
                                    setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack(null).commit();
//                            fragmentManager.beginTransaction().hide(new Todays_Sales_fragment());
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="YS";
                        }
                        else{
//                            ConstantDeclaration.replaceFragment1(new Yesterdays_Sales_fragment(), fragmentManager);
                            fragmentManager.beginTransaction().replace(newContainerId, new Yesterdays_Sales_fragment()).commit();
//                            fragmentManager.beginTransaction().hide(new Todays_Sales_fragment());
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="YS";

                        }

//                    notifyDataSetChanged();
                    }
                    else if(position == 2){
                        if(graph_open.equalsIgnoreCase("TS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Cumulative_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            //                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CS";
                        }
                        else if(graph_open.equalsIgnoreCase("YS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Cumulative_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            //                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CS";
                        }
                        else if(graph_open.equalsIgnoreCase("CS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                        }
                        else if(graph_open.equalsIgnoreCase("RCS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Cumulative_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            //                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CS";
                        }
                        else if(graph_open.equalsIgnoreCase("NS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Cumulative_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            //                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CS";
                        }
                        else if(graph_open.equalsIgnoreCase("PS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Cumulative_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            //                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CS";
                        }
                        else if(graph_open.equalsIgnoreCase("CRS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Cumulative_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            //                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CS";
                        }
                        else{
                            fragmentManager.beginTransaction().replace(newContainerId, new Cumulative_Sales_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            //                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CS";
                        }


                    }
                    else if(position == 3){
                        if(graph_open.equalsIgnoreCase("TS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new RO_Connectivity_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="RCS";
                        }
                        else if(graph_open.equalsIgnoreCase("YS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new RO_Connectivity_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="RCS";
                        }
                        else if(graph_open.equalsIgnoreCase("CS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new RO_Connectivity_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="RCS";
                        }
                        else if(graph_open.equalsIgnoreCase("RCS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                        }
                        else if(graph_open.equalsIgnoreCase("NS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new RO_Connectivity_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="RCS";
                        }
                        else if(graph_open.equalsIgnoreCase("PS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new RO_Connectivity_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="RCS";
                        }
                        else if(graph_open.equalsIgnoreCase("CRS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new RO_Connectivity_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="RCS";
                        }
                        else{
                            fragmentManager.beginTransaction().replace(newContainerId, new RO_Connectivity_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="RCS";
                        }

                    }
                    else if(position == 4){
                        if(graph_open.equalsIgnoreCase("TS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Nano_Status_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
//                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="NS";
                        }
                        else if(graph_open.equalsIgnoreCase("YS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Nano_Status_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
//                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="NS";
                        }
                        else if(graph_open.equalsIgnoreCase("CS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Nano_Status_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
//                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="NS";
                        }
                        else if(graph_open.equalsIgnoreCase("RCS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Nano_Status_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
//                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="NS";
                        }
                        else if(graph_open.equalsIgnoreCase("NS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                        }
                        else if(graph_open.equalsIgnoreCase("PS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Nano_Status_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
//                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="NS";
                        }
                        else if(graph_open.equalsIgnoreCase("CRS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Nano_Status_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
//                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="NS";
                        }
                        else{
                            fragmentManager.beginTransaction().replace(newContainerId, new Nano_Status_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            // Once all fragment replacement is done we can show the hidden container
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
//                    notifyDataSetChanged();
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="NS";
                        }

                    }
                    else if(position == 5){
                        if(graph_open.equalsIgnoreCase("TS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            fragmentManager.beginTransaction().replace(newContainerId, new Price_Exception_fragment()).commit();
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container

                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="PS";
                        }
                        else if(graph_open.equalsIgnoreCase("YS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            fragmentManager.beginTransaction().replace(newContainerId, new Price_Exception_fragment()).commit();
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container

                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="PS";
                        }
                        else if(graph_open.equalsIgnoreCase("CS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            fragmentManager.beginTransaction().replace(newContainerId, new Price_Exception_fragment()).commit();
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container

                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="PS";
                        }
                        else if(graph_open.equalsIgnoreCase("RCS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            fragmentManager.beginTransaction().replace(newContainerId, new Price_Exception_fragment()).commit();
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container

                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="PS";
                        }
                        else if(graph_open.equalsIgnoreCase("NS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            fragmentManager.beginTransaction().replace(newContainerId, new Price_Exception_fragment()).commit();
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container

                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="PS";
                        }
                        else if(graph_open.equalsIgnoreCase("PS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                        }
                        else if(graph_open.equalsIgnoreCase("CRS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            fragmentManager.beginTransaction().replace(newContainerId, new Price_Exception_fragment()).commit();
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container

                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="PS";
                        }
                        else{
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;

                            fragmentManager.beginTransaction().replace(newContainerId, new Price_Exception_fragment()).commit();
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container

                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="PS";
                        }

                    }
                    else if(position == 6){
                        if(graph_open.equalsIgnoreCase("TS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Critical_Stock_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CRS";
                        }
                        else if(graph_open.equalsIgnoreCase("YS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Critical_Stock_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CRS";
                        }
                        else if(graph_open.equalsIgnoreCase("CS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Critical_Stock_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CRS";
                        }
                        else if(graph_open.equalsIgnoreCase("RCS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Critical_Stock_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CRS";
                        }
                        else if(graph_open.equalsIgnoreCase("NS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Critical_Stock_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CRS";
                        }
                        else if(graph_open.equalsIgnoreCase("PS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                            fragmentManager.beginTransaction().replace(newContainerId, new Critical_Stock_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CRS";
                        }
                        else if(graph_open.equalsIgnoreCase("CRS")){
                            graph_open ="";
                            holder.container.setVisibility(View.GONE);
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
                            notifyDataSetChanged();
                        }
                        else{

                            fragmentManager.beginTransaction().replace(newContainerId, new Critical_Stock_fragment()).commit();
                            holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                            holder.item_layout_graph.setVisibility(View.VISIBLE);
                            holder.container.setVisibility(View.VISIBLE);
                            // Once all fragment replacement is done we can show the hidden container
                            itemClickListener.onItemClick(myListData.getDescription(),
                                    "", position);
                            graph_open ="CRS";
                        }

                    }
//                }
//                else{
//                    graph_open ="";
//                    holder.container.setVisibility(View.GONE);
//                    holder.item_layout_main.getLayoutParams().height = ViewGroup.LayoutParams.WRAP_CONTENT;
//                    notifyDataSetChanged();
//                }


            }
        });



//        // Delete previous added fragment
//        int currentContainerId = holder.iDetailsContainer.getId();
//        // Get the current fragment
//        Fragment oldFragment = fragmentManager.findFragmentById(currentContainerId);
//        if(oldFragment != null) {
//            // Delete fragmet from ui, do not forget commit() otherwise no action
//            // is going to be observed
//            fragmentManager.beginTransaction().remove(oldFragment).commit();
//        }
//
//        // In order to be able of replacing a fragment on a recycler view
//        // the target container should always have a different id ALWAYS
//        int newContainerId = getUniqueId();
//        // Set the new Id to our know fragment container
//        holder.iDetailsContainer.setId(newContainerId);
//
//        // Just for Testing we are going to create a new fragment according
//        // if the view position is pair one fragment type is created, if not
//        // a different one is used
//        Fragment f;
//        if(position%2 == 0) {
//            f = new FragmentCard();
//        }else{
//            f=new FragmentChat();
//        }
//
//        // Then just replace the recycler view fragment as usually
//        fragmentManager.beginTransaction().replace(newContainerId, f).commit();

    }
    public int getUniqueId(){
        return (int) SystemClock.currentThreadTimeMillis();
    }

    @Override
    public int getItemCount() {
        return listdata.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public TextView textView;
        public ConstraintLayout item_layout;
        LinearLayout ll_todaysale_graph,item_layout_main,item_layout_graph,item_layout_ys;
        LinearLayout ll_yesterday_graph;
        ViewPager viewPager;
        FrameLayout container,container_ys,container_cs,container_rcs,container_ns,container_ps,container_crs;
        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.tv_status_name);
            item_layout = (ConstraintLayout)itemView.findViewById(R.id.item_layout);
            item_layout_main = (LinearLayout)itemView.findViewById(R.id.item_layout_main);
            item_layout_graph = (LinearLayout)itemView.findViewById(R.id.item_layout_graph);
            item_layout_ys = (LinearLayout)itemView.findViewById(R.id.item_layout_ys);
            ll_todaysale_graph = (LinearLayout) itemView.findViewById(R.id.ll_todaysale_graph);
            ll_yesterday_graph = (LinearLayout) itemView.findViewById(R.id.ll_yesterday_graph);
            viewPager = (ViewPager) itemView.findViewById(R.id.viewpager);
            container = (FrameLayout) itemView.findViewById(R.id.container);
            container_ys = (FrameLayout) itemView.findViewById(R.id.container_ys);
            container_cs = (FrameLayout) itemView.findViewById(R.id.container_cs);
            container_rcs = (FrameLayout) itemView.findViewById(R.id.container_rcs);
            container_ns = (FrameLayout) itemView.findViewById(R.id.container_ns);
            container_ps = (FrameLayout) itemView.findViewById(R.id.container_ps);
            container_crs = (FrameLayout) itemView.findViewById(R.id.container_crs);
//            container.setVisibility(View.GONE);

        }

    }
//    private void callTodaysSale() {
//
//        JSONObject json = new JSONObject();
//        try {
//            if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "Request_Field","").equalsIgnoreCase("Zone")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "Selectdzone","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "Selectdzone",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "Request_Field","").equalsIgnoreCase("Region")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "SelectdRegion","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "Selectdzone",""));
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdRegion",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "Selectdzone",""));
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdRegion",""));
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                            "Selectdzone", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                        "Selectdzone", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                                "FirstFilteredRO", "");
//                    }
//
//
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else  if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "Request_Selectd","").equalsIgnoreCase("")){
//
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "Request_Field","").equalsIgnoreCase("Region")){
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "SelectdRegion","").equalsIgnoreCase("ALL")){
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdRegion",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdRegion",""));
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                            "SelectdRegion", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                        "SelectdRegion", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                                "FirstFilteredRO", "");
//                    }
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else  if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "Request_Selectd","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "Request_Field","").equalsIgnoreCase("SalesArea")){
//
//                        if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
//                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdSalesArea",""));
//                            json.put("ROCode", "ALL");
//                        }else{
//                            json.put("ROCode", "ALL");
//                        }
//                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "FilteredRO","").equalsIgnoreCase("ALL")) {
//                        try {
//                            if (UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
//                                    UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
//                            } else {
//                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                        "SelectdSalesArea", ""));
//                            }
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                                "FirstFilteredRO", "");
//                    }else{
//                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                                "FilteredRO",""));
//                        UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                                "FirstFilteredRO", "");
//                    }
//
//                    //                    try {
//                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
//                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
//                    //
//                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    //                                    "SelectdSalesArea",""));
//                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    //                                    "FilteredRO",""));
//                    //                            UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                    //                                    "FirstFilteredRO", "");
//                    //                        }else{
//                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    //                                    "FilteredRO",""));
//                    //                            UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                    //                                    "FirstFilteredRO", "");
//                    //                        }
//                    //                    } catch (Exception e) {
//                    //                        e.printStackTrace();
//                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    //                                "FilteredRO",""));
//                    //                        UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                    //                                "FirstFilteredRO", "");
//                    //                    }
//
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "FirstFilteredRO",""));
//                }
//
//            }
//            else if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
//                if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "FilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "FilteredRO",""));
//                    UserDataPrefrence.savePreference("HPCL_Preference", mContext,
//                            "FirstFilteredRO", "");
//                }
//                else if(!UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "FirstFilteredRO","").equalsIgnoreCase("")){
//                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                            "FirstFilteredRO",""));
//                }
//            }
//            else if(UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
//                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", mContext,
//                        "ROKey","") );
//            }
//        }
//        catch (JSONException e) {
//            e.printStackTrace();
//        }
//        new TodaysSale().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
//    }
//    private class TodaysSale extends AsyncTask<JSONObject, Void, String> {
//        String strTimeStamp = "";
//
//        public TodaysSale() {
//        }
//
//        public TodaysSale(String strTime) {
//            strTimeStamp = strTime;
//        }
//
//        @Override
//        protected void onPreExecute() {
//            super.onPreExecute();
//
//            try {
//                if (ConstantDeclaration.gifProgressDialog != null) {
//                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
//                        ConstantDeclaration.showGifProgressDialog(mContext);
//                    }
//                } else {
//                    ConstantDeclaration.showGifProgressDialog(mContext);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//        @Override
//        protected String doInBackground(JSONObject... jsonObjects) {
//            return ClsWebService_hpcl.PostObjecttoken("DashBoard/GetTodaySales", false, jsonObjects[0], "");
//        }
//
//
//        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
//        @Override
//        protected void onPostExecute(String s) {
//            super.onPostExecute(s);
//            try {
//                if (ConstantDeclaration.gifProgressDialog.isShowing())
//                    ConstantDeclaration.gifProgressDialog.dismiss();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
////            if (!isAdded()) {
////                return;
////            }
////            if (isDetached()) {
////                return;
////            }
//
//            if (s != null) {
//
//                try {
//
//                    if (s.contains("||")) {
//                        String[] str = (s.split("\\|\\|"));
//                        respCode = str[0];
//                        if(respCode.equalsIgnoreCase("00")) {
//                            JSONObject jsonObject = new JSONObject(str[2]);
//                            JSONObject jsonObject1 = jsonObject.getJSONObject("Data");
//                            catArray = new JSONArray();
//                            valArray = new JSONArray();
//                            catArray = jsonObject1.getJSONArray("Product");
//                            valArray = jsonObject1.getJSONArray("Value");
//                            JSONArray timeSlot = jsonObject.getJSONArray("TimeSlot");
//                            for (int ts =0;ts<timeSlot.length();ts++){
//                                JSONObject timeObj = timeSlot.getJSONObject(ts);
//                                tv_untilllbl.setText(timeObj.getString("EndTime"));
//                                SimpleDateFormat curFormater = new SimpleDateFormat("yyyy-MM-dd");
//                                Date dateObj = null;
//                                String fromDate = timeObj.getString("FromDate");
//                                dateObj = curFormater.parse(fromDate);
//                                SimpleDateFormat postFormater = new SimpleDateFormat("DD |MM | YYYY");
//
//                                String nFDate = postFormater.format(dateObj);
//                                try {
//                                    tv_uptolbl.setText(fromDate.replace("-"," |"));
//                                } catch (Exception e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            todaycategorydata = new ArrayList<>();
//                            for (int i1 = 0; i1 < catArray.length(); i1++) {
//                                todaycategorydata.add(catArray.getString(i1));
//                            }
//                            todayvalueData = new ArrayList<>();
//                            for (int i1 = 0; i1 < valArray.length(); i1++) {
//                                todayvalueData.add(valArray.getString(i1));
//                            }
//                            BarData data = createChartData();
//                            configureChartAppearance();
//                            prepareChartData(data);
//                        }
//                        else if(respCode.equalsIgnoreCase("401")){
//                            try {
//                                if (ConstantDeclaration.gifProgressDialog.isShowing())
//                                    ConstantDeclaration.gifProgressDialog.dismiss();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            error = str[1];
//                            universalDialog = new UniversalDialog(mContext,
//                                    new AlertDialogInterface() {
//                                        @Override
//                                        public void methodDone() {
//                                            Intent intent = new Intent(mContext, LoginActivity.class);
//                                            startActivity(intent);
//                                            getActivity().finish();
//                                        }
//
//                                        @Override
//                                        public void methodCancel() {
//
//                                        }
//                                    }, "", error, "OK", "");
//                            universalDialog.showAlert();
//                        }
//                        else{
//                            try {
//                                if (ConstantDeclaration.gifProgressDialog.isShowing())
//                                    ConstantDeclaration.gifProgressDialog.dismiss();
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                            error = str[1];
//                            universalDialog = new UniversalDialog(getActivity(),
//                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
//                            universalDialog.showAlert();
//                        }
//
//                    }
//                }catch (Exception e){
//                    e.printStackTrace();
//                }
//            }
//
//        }
//
//    }
public void refreshView(int position) {
    notifyItemChanged(position);
}
    // Method in ViewHolder class

}


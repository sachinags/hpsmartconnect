package com.agstransact.HP_SC.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.Dupump_Details_Model;
import com.agstransact.HP_SC.model.Nozzle_status_Model;

import java.util.ArrayList;

public class Dupump_Details_Adapter extends RecyclerView.Adapter<Dupump_Details_Adapter.MyInterlockModel> {
    private Context context;
    private ArrayList<Dupump_Details_Model> models = new ArrayList<>();

    public Dupump_Details_Adapter(Activity activity, ArrayList<Dupump_Details_Model> models) {
        this.context = activity;
        this.models = models;
    }


    @NonNull
    @Override
    public Dupump_Details_Adapter.MyInterlockModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.dupump_details_adapter,parent,false);
        return new MyInterlockModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Dupump_Details_Adapter.MyInterlockModel holder, int position) {

        Dupump_Details_Model model = models.get(position);

        holder.tv_dumake_value.setText(model.getDUMake());
        holder.tv_duno_value.setText(model.getDuNo());
        holder.tv_pumpno_value.setText(model.getPumpNo());
        holder.tv_nozzleno_value.setText(model.getNozzleno());
        holder.tv_tankno_value.setText(model.getTankNo());
        holder.tv_productname_value.setText(model.getProductName());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class MyInterlockModel extends RecyclerView.ViewHolder {

        private TextView tv_dumake_value,tv_duno_value,tv_pumpno_value,
                tv_nozzleno_value,tv_tankno_value,tv_productname_value;

        public MyInterlockModel(@NonNull View itemView) {
            super(itemView);

            tv_dumake_value = itemView.findViewById(R.id.tv_dumake_value);
            tv_duno_value = itemView.findViewById(R.id.tv_duno_value);
            tv_pumpno_value = itemView.findViewById(R.id.tv_pumpno_value);
            tv_nozzleno_value = itemView.findViewById(R.id.tv_nozzleno_value);
            tv_tankno_value = itemView.findViewById(R.id.tv_tankno_value);
            tv_productname_value = itemView.findViewById(R.id.tv_productname_value);
        }
    }
}

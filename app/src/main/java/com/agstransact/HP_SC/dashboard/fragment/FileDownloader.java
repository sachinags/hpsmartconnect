package com.agstransact.HP_SC.dashboard.fragment;

import android.os.Environment;
import android.util.Log;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ClsWebService;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public class FileDownloader {

    private static final String TAG = "FileDownloader";

    private static final int MEGABYTE = 1024 * 1024;

    public static void downloadFile(String fileUrl, File directory,String Filename) {
        Log.v(TAG, "downloadFile() invoked ");
        Log.v(TAG, "downloadFile() fileUrl " + fileUrl);
        Log.v(TAG, "downloadFile() directory " + directory);

//            URL url = new URL(fileUrl);
//            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
//            urlConnection.connect();

        HttpsURLConnection connection = null;
        try {

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open("chos.hpcl.co.in.crt"));
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }

// Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

// Tell the URLConnection to use a SocketFactory from our SSLContext
            /*Amit 30.11.17 ssl pining */
            URL url = new URL("https://chosm.hpcl.co.in/" +fileUrl);

            connection = (HttpsURLConnection) url.openConnection();
            /*Comment below line when url is development 06.12.17*/
//            connection.setSSLSocketFactory(context.getSocketFactory());
            connection.setRequestProperty("Content-type", "application/json");
//            String token;
//            token = UserDataPrefrence.getPreference("HPCL_Preference", MainApplication.getContext(), "HPCLTOKEN", "");
//            connection.setRequestProperty("authorization", "Bearer " + token);
            /*06.12.17*/


            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setConnectTimeout(45000);
            connection.setReadTimeout(45000);

            connection.connect();

            int responsecode = connection.getResponseCode();
            if (responsecode == HttpsURLConnection.HTTP_OK) {
                int contentLength = connection.getContentLength();
                InputStream inputStream = connection.getInputStream();
                FileOutputStream fileOutputStream = new FileOutputStream(directory);
                DataInputStream stream = new DataInputStream(connection.getInputStream());

                byte[] buffer = new byte[contentLength];
                int bufferLength = 0;
                while ((bufferLength = inputStream.read(buffer)) > 0) {
//                    fileOutputStream.write(buffer, 0, bufferLength);
//                    stream.readFully(buffer);
//                    stream.close();
                    String fullName = "";
                    File file;
//                    fileOutputStream.close();
                    File imagePath = new File(Environment.getExternalStorageDirectory(), "App HOS");

                    String path1 = Environment.getExternalStorageDirectory() + "/" + "App HOS/";
                    File directory2 = new File(path1);
                    directory2.mkdir();

//                    File newFile = new File(imagePath, "PriceException.pdf");
                    File newFile = new File(imagePath, Filename+".pdf");
                    fullName = newFile.getPath();
                    file = new File(fullName);
                    if (!file.exists()) {
                        file.createNewFile();
                    }
                    DataOutputStream fos = new DataOutputStream(new FileOutputStream(file));
                    fos.write(buffer);
                    fos.flush();
                    fos.close();
                }
            }

//        int totalSize = connection.getContentLength();

//        byte[] buffer = new byte[MEGABYTE];

        Log.v(TAG, "downloadFile() completed ");
        }
        catch (Exception e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            com.agstransact.HP_SC.utils.Log.e("API Exception", e.toString());
        } finally {

            try {
                connection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}

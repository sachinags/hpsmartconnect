package com.agstransact.HP_SC.dashboard.adapter.NewDesign;

public class tank_model_new {
    String ROCode;
    String ROName;
    String TankNo;
    String ProductCode;
    String Product;
    String TankName;
    String Capacity;
    String NetVolume;
    String ProductHeight;
    String Ullage;
    String WaterHeight;
    String Temparature;
    String Density;
    String Density15C;
    String STKDateTime;
    String Online;
    String Current;
    public tank_model_new(String ROCode, String ROName,
                          String tankNo, String productCode, String product,
                          String tankName, String capacity, String netVolume,
                          String productHeight, String ullage, String waterHeight,
                          String temparature, String density, String density15C,
                          String STKDateTime, String online, String current) {
        this.ROCode = ROCode;
        this.ROName = ROName;
        TankNo = tankNo;
        ProductCode = productCode;
        Product = product;
        TankName = tankName;
        Capacity = capacity;
        NetVolume = netVolume;
        ProductHeight = productHeight;
        Ullage = ullage;
        WaterHeight = waterHeight;
        Temparature = temparature;
        Density = density;
        Density15C = density15C;
        this.STKDateTime = STKDateTime;
        Online = online;
        Current = current;
    }

    public String getROCode() {
        return ROCode;
    }

    public void setROCode(String ROCode) {
        this.ROCode = ROCode;
    }

    public String getROName() {
        return ROName;
    }

    public void setROName(String ROName) {
        this.ROName = ROName;
    }

    public String getTankNo() {
        return TankNo;
    }

    public void setTankNo(String tankNo) {
        TankNo = tankNo;
    }

    public String getProductCode() {
        return ProductCode;
    }

    public void setProductCode(String productCode) {
        ProductCode = productCode;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getTankName() {
        return TankName;
    }

    public void setTankName(String tankName) {
        TankName = tankName;
    }

    public String getCapacity() {
        return Capacity;
    }

    public void setCapacity(String capacity) {
        Capacity = capacity;
    }

    public String getNetVolume() {
        return NetVolume;
    }

    public void setNetVolume(String netVolume) {
        NetVolume = netVolume;
    }

    public String getProductHeight() {
        return ProductHeight;
    }

    public void setProductHeight(String productHeight) {
        ProductHeight = productHeight;
    }

    public String getUllage() {
        return Ullage;
    }

    public void setUllage(String ullage) {
        Ullage = ullage;
    }

    public String getWaterHeight() {
        return WaterHeight;
    }

    public void setWaterHeight(String waterHeight) {
        WaterHeight = waterHeight;
    }

    public String getTemparature() {
        return Temparature;
    }

    public void setTemparature(String temparature) {
        Temparature = temparature;
    }

    public String getDensity() {
        return Density;
    }

    public void setDensity(String density) {
        Density = density;
    }

    public String getDensity15C() {
        return Density15C;
    }

    public void setDensity15C(String density15C) {
        Density15C = density15C;
    }

    public String getSTKDateTime() {
        return STKDateTime;
    }

    public void setSTKDateTime(String STKDateTime) {
        this.STKDateTime = STKDateTime;
    }

    public String getOnline() {
        return Online;
    }

    public void setOnline(String online) {
        Online = online;
    }

    public String getCurrent() {
        return Current;
    }

    public void setCurrent(String current) {
        Current = current;
    }



}

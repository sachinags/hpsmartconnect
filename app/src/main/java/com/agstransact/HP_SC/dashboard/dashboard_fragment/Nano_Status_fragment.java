package com.agstransact.HP_SC.dashboard.dashboard_fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.model.NanoStatus;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Nano_Status_fragment extends Fragment {
    View rootView;
    private String error="", respCode="",fuel_type="";
    private PieChart nano_piechart;
    private TextView nano_status_date;
    private ArrayList<NanoStatus> nanoStatusArrayList = new ArrayList<>();
    List<String> listKeys;
    List<Integer> listValues;
    private List<String> listData,roData,nanodata;
    private ArrayList pieEntries;
    private PieDataSet pieDataSet;
    private PieData pieData;
    private AlertDialogInterface alertDialogInterface;
    private UniversalDialog universalDialog;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fargment_nano_status_dashboard, container, false);
        nano_piechart = (PieChart) rootView.findViewById(R.id.piechart_nano_status);
        nano_status_date = (TextView) rootView.findViewById(R.id.nano_status_date);
        getNanoChart();
        return  rootView;
    }
    private void getNanoChart() {
        JSONObject json = new JSONObject();

        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{

                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }
                        else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new Asyncnano_exception().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }
    private  class Asyncnano_exception extends AsyncTask<JSONObject, Void, String> {

        String strTimeStamp = "";

        public Asyncnano_exception() {
        }

        public Asyncnano_exception(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Status/NanoStatus", false, jsonObjects[0], "");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if (respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject data = jsonObject.getJSONObject("Data");
                            JSONArray valuesArray = data.getJSONArray("PTCount");
                            JSONArray ptArray = data.getJSONArray("PTStatus");
                            JSONArray statusArray = data.getJSONArray("NanoStatus");

                            for (int ts =0;ts<statusArray.length();ts++){
//                                JSONObject timeObj = statusArray.getJSONObject(ts);
                                String nanoStatus = statusArray.getString(ts);
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
//                                String fromDate = timeObj.getString("FromDate");
                                dateObj = curFormater.parse(nanoStatus);
                                Log.e("dta",nanoStatus);
                                SimpleDateFormat postFormater = new SimpleDateFormat("DD |MM | YYYY");

                                String nFDate = postFormater.format(dateObj);
//                                nano_status_date.setText(nFDate);
                                nano_status_date.setText(nanoStatus.replace("-"," |"));
                            }
                            listKeys = new ArrayList<String>();
                            List<Double> listValues1 = new ArrayList<Double>();
                            for (int i1 = 0; i1 < ptArray.length(); i1++) {
                                listKeys.add(ptArray.getString(i1));
                            }
                            for (int value = 0; value < valuesArray.length(); value++) {
                                listValues1.add(Double.valueOf((valuesArray.getString(value))));
                            }

                            nanoStatusArrayList = new ArrayList<>();
                            nanodata = new ArrayList<String>();
                            for (int i1 = 0; i1 < valuesArray.length(); i1++) {
                                NanoStatus nanoStatus = new NanoStatus();
                                nanoStatus.setPtValue(Float.parseFloat(valuesArray.getString(i1)));
                                nanoStatus.setPtStatus(ptArray.getString(i1));
                                nanoStatusArrayList.add(nanoStatus);
//                                nanodata.add(valuesArray.getString(i1));
                                Log.e("nanoData",valuesArray.getString(i1));
                            }
                            getnanoEntries();
                            pieDataSet = new PieDataSet(pieEntries, "");
                            pieData = new PieData(pieDataSet);
                            nano_piechart.setData(pieData);
                            nano_piechart.getData().setDrawValues(false);
                            nano_piechart.setDrawEntryLabels(false);
                            nano_piechart.setDrawSliceText(false);
                            final int[] MY_COLORS = {Color.rgb(0, 0, 254), Color.rgb(0, 128, 1), Color.rgb(249, 40, 39),
                                    Color.rgb(5, 1, 3)};
                            ArrayList<Integer> colors = new ArrayList<Integer>();

                            for (int c : MY_COLORS) colors.add(c);
                            nano_piechart.getLegend().setEnabled(false);
                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLUE);
                            pieDataSet.setValueTextSize(10f);
                            pieDataSet.setValueFormatter(new PercentFormatter());
                            nano_piechart.getDescription().setEnabled(false);
                            LegendEntry[] legendEntries=new LegendEntry[pieEntries.size()];
                            for(int i=0;i<legendEntries.length;i++)
                            {
                                LegendEntry entry=new LegendEntry();
                                entry.formColor=MY_COLORS[i];
                                try {
                                    entry.label=listKeys.get(i)+ " = "+listValues1.get(i);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                legendEntries[i]=(entry);

                            }
                            nano_piechart.getLegend().setCustom(legendEntries);
                            nano_piechart.getLegend().setTextColor(Color.BLACK);
                            nano_piechart.setExtraBottomOffset(10f);
                            nano_piechart.getLegend().setWordWrapEnabled(true);
                            nano_piechart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
                            nano_piechart.getLegend().setEnabled(true);
                            nano_piechart.getLegend().setTextColor(getResources().getColor(R.color.black));
                            nano_piechart.getLegend().setTextSize(8f);
                            nano_piechart.getLegend().setYOffset(8f);
                            nano_piechart.getLegend().setXOffset(8f);
                            nano_piechart.getLegend().getHorizontalAlignment();
                            nano_piechart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
                            nano_piechart.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                            nano_piechart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                            nano_piechart.getLegend().setWordWrapEnabled(true);
                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLACK);
                            pieDataSet.setValueTextSize(8f);
                            nano_piechart.getDescription().setEnabled(false);

                            nano_piechart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                                @Override
                                public void onValueSelected(Entry e, Highlight h) {
                                    PieEntry pe = (PieEntry) e;
                                    String str= pe.getLabel();
                                    str = str.replaceAll("[^a-zA-Z0-9]", " ");
                                    nano_piechart.setCenterText(str + "\n" + e.getY());
                                    nano_piechart.setCenterTextColor(getResources().getColor(R.color.black));


                                }

                                @Override
                                public void onNothingSelected() {
                                    nano_piechart.setCenterText("");

                                }
                            });
                            nano_piechart.setCenterText(ptArray.getString(0)+"\n"+valuesArray.getDouble(0));
                            nano_piechart.invalidate();
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }

                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }

        }

    }
    private void getnanoEntries(){
        pieEntries = new ArrayList<>();
        for(int i =0;i <nanoStatusArrayList.size();i++){
            pieEntries.add(new PieEntry(nanoStatusArrayList.get(i).getPtValue(),nanoStatusArrayList.get(i).getPtStatus()));
//            Log.e("roData",nanodata.get(i));

        }
    }

}

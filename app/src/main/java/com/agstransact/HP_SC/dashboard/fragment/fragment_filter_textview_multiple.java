package com.agstransact.HP_SC.dashboard.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.adapter.Dialog_search_adapter_multiple;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class fragment_filter_textview_multiple extends Fragment implements  AlertDialogInterface,Dialog_search_adapter_multiple.ItemClickListener  {
    private View view;
    TextView tv_select_zone_value,tv_select_region_value,tv_select_sales_area_value,tv_select_RO_value;
//    LinearLayout ll_select_zone_value,ll_select_region_value,ll_select_sales_area_value,ll_select_RO_value;
    LinearLayout tv_select_zone,tv_select_region,tv_select_sales_area,tv_select_RO;
    Button submitBT;
    String error="", respCode="";
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    Bundle bundle;
    ArrayList<String> ZoneList,ROCODE_List;
    JSONArray Regionarray,ZoneArray,ROArray,SalesAreaArray;
    ArrayList<String> RegionKey,RegionValue,SalesAreaKey,SalesAreaValue,ROKey,ROValue;
    Dialog_search_adapter_multiple search_adapter;
    AlertDialog dialogSelectBranch;
    String SelectedZone= "", SelectedRegion= "", SelectedSalesArea= "", SelectedRO= "";
    Boolean ZoneClicked= false,Regionclicked =false,SalesAreaClicked =false,ROClicked = false;
    String filteredRO;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_filter_textview, container, false);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
        toolbarTV.setText(getResources().getString(R.string.filter));
        setHasOptionsMenu(true);
        bundle = getArguments();

        tv_select_zone_value = view.findViewById(R.id.tv_select_zone_value);
        tv_select_region_value = view.findViewById(R.id.tv_select_region_value);
        tv_select_sales_area_value = view.findViewById(R.id.tv_select_sales_area_value);
        tv_select_RO_value = view.findViewById(R.id.tv_select_RO_value);

        tv_select_zone = view.findViewById(R.id.ll_select_zone_value);
        tv_select_region = view.findViewById(R.id.ll_select_region_value);
        tv_select_sales_area = view.findViewById(R.id.ll_select_sales_area_value);
        tv_select_RO = view.findViewById(R.id.ll_select_RO_value);
        tv_select_RO_value.setSelected(true);
        submitBT = view.findViewById(R.id.submitBT);

        if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLHQ")){
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "Selectdzone","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "Selectdzone","").equalsIgnoreCase("Select Zone"))){
                tv_select_zone_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Selectdzone",""));
                CallRegion();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }
            else{
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                submitBT.setEnabled(false);
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("Select Region"))){
                tv_select_region_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdRegion",""));
                CallSales_Area();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("Select Sales Area"))){
                tv_select_sales_area_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea",""));
                CallRO();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                } else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }

            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);

            }

            tv_select_zone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    populateROList();
                    ZoneClicked=true;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = false;
                    selectBranch();
                }

            });
            tv_select_region_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = true;
                    SalesAreaClicked = false;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_sales_area_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = true;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_RO_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = true;
                    selectBranch();
                }
            });
            tv_select_zone_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Zone")){
                        tv_select_sales_area_value.setEnabled(false);
                        tv_select_RO_value.setEnabled(false);
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitBT.setEnabled(false);
                    }else{
                        tv_select_region_value.setEnabled(true);
                        tv_select_sales_area_value.setEnabled(true);
                        tv_select_RO_value.setEnabled(true);
                        CallRegion();
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            tv_select_region_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Region")){
                        tv_select_sales_area_value.setEnabled(false);
                        tv_select_RO_value.setEnabled(false);
                    }else{
                        tv_select_sales_area_value.setEnabled(true);
                        tv_select_RO_value.setEnabled(true);
                        CallSales_Area();
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            tv_select_sales_area_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Sales Area")){
                        tv_select_RO_value.setEnabled(false);
                    }else{
                        tv_select_RO_value.setEnabled(true);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                        CallRO();
                    }

                }
            });
            tv_select_RO_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select RO")){
//                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
//                        submitBT.setEnabled(false);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }else{
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                        submitBT.setEnabled(true);
                    }

                }
            });
            if(tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                    tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")){
                tv_select_region_value.setEnabled(false);
            }
            if(tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                    tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
                tv_select_sales_area_value.setEnabled(false);
            }
            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")&&
                    tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")){
                tv_select_RO_value.setEnabled(false);
            }

        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLZONE")){
            tv_select_region_value.setEnabled(true);
            tv_select_zone.setVisibility(View.GONE);
            tv_select_zone_value.setVisibility(View.GONE);
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdRegion","").equalsIgnoreCase("Select Region"))){
                tv_select_region_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdRegion",""));
                CallSales_Area();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }else{
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                submitBT.setEnabled(false);
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("Select Sales Area"))){
                tv_select_sales_area_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea",""));
                CallRO();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);
            }else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);

            }
            tv_select_region_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    populateROList();
                    ZoneClicked=false;
                    Regionclicked = true;
                    SalesAreaClicked = false;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_sales_area_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = true;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_RO_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = true;
                    selectBranch();
                }
            });
            tv_select_region_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Region")){
                        tv_select_sales_area_value.setEnabled(false);
                        tv_select_RO_value.setEnabled(false);
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitBT.setEnabled(false);
                    }else{
                        tv_select_sales_area_value.setEnabled(true);
                        tv_select_RO_value.setEnabled(true);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            tv_select_sales_area_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Sales Area")){
                        tv_select_RO_value.setEnabled(false);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }else{
                        tv_select_RO_value.setEnabled(true);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            tv_select_RO_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select RO")){
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }else{
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                        submitBT.setEnabled(true);
                    }

                }
            });


            if(tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                    tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
                tv_select_sales_area_value.setEnabled(false);
            }
            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")&&
                    tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                    tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
                tv_select_RO_value.setEnabled(false);
            }
        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLREGION")){
            tv_select_region_value.setEnabled(true);
            tv_select_zone.setVisibility(View.GONE);
            tv_select_zone_value.setVisibility(View.GONE);
            tv_select_region.setVisibility(View.GONE);
            tv_select_region_value.setVisibility(View.GONE);
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "SelectdSalesArea","").equalsIgnoreCase("Select Sales Area"))){
                tv_select_sales_area_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea",""));
                SelectedSalesArea = UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "SelectdSalesArea","");
                CallRO();
                if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                    submitBT.setEnabled(true);
                }else{
                    submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                    submitBT.setEnabled(false);
                }
            }
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);
            }else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);

            }
            tv_select_sales_area_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    populateROList();
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = true;
                    ROClicked = false;
                    selectBranch();
                }
            });
            tv_select_RO_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = true;
                    selectBranch();
                }
            });
            tv_select_sales_area_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select Sales Area")){
                        tv_select_RO_value.setEnabled(false);
                    }else{
                        tv_select_RO_value.setEnabled(true);
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            tv_select_RO_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select RO")){
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }else{
                        if(!bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                            submitBT.setEnabled(true);
                        }else{
                            submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                            submitBT.setEnabled(false);
                        }
                    }

                }
            });
            if(tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")&&
                    tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")){
                tv_select_RO_value.setEnabled(false);
            }
        }
        else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                "").equalsIgnoreCase("HPCLSALESAREA")){
            tv_select_RO_value.setEnabled(true);
            tv_select_zone.setVisibility(View.GONE);
            tv_select_zone_value.setVisibility(View.GONE);
            tv_select_region.setVisibility(View.GONE);
            tv_select_region_value.setVisibility(View.GONE);
            tv_select_sales_area.setVisibility(View.GONE);
            tv_select_sales_area_value.setVisibility(View.GONE);
            if(!(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("")|| UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("Select RO"))){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);
            }else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "FilteredRO","").equalsIgnoreCase("ALL")){
                try {
                    tv_select_RO_value.setText(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredROName",""));
                    try {
                        SelectedRO = tv_select_RO_value.getText().toString();
                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredROName","").equalsIgnoreCase("ALL")) {
                            filteredRO = SelectedRO;
                        }else{
                            filteredRO = SelectedRO.substring(0,8);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                submitBT.setEnabled(true);

            }
            tv_select_RO_value.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    populateROList();
                    ZoneClicked=false;
                    Regionclicked = false;
                    SalesAreaClicked = false;
                    ROClicked = true;
                    selectBranch();
                }
            });
            tv_select_RO_value.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    String editable1 =editable.toString();
                    if(editable1.equalsIgnoreCase("Select RO")){
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
                        submitBT.setEnabled(false);
                    }else{
                        submitBT.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.selector_btn_bg));
                        submitBT.setEnabled(true);
                    }

                }
            });
        }


        submitBT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                        "").equalsIgnoreCase("HPCLHQ")){
                    try {
                        if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")&&
                                tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Selectdzone", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "Zone");
                        }
                        else if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone") &&
                                !tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")) {
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Selectdzone", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_zone_value.getText().toString()+"-"+
                                            tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "Region");

                        }
                        else if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone") &&
                                !tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                !tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name",tv_select_zone_value.getText().toString()+"-"+
                                            tv_select_region_value.getText().toString()+"-"+tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Selectdzone", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "SalesArea");
                        }
                        else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                        }

                        if(!tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Selectdzone", tv_select_zone_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", filteredRO);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", SelectedRO);
                        }
                        else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                        "").equalsIgnoreCase("HPCLZONE")){
                    try {
                        if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")) {
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "Region");
                        }
                        else if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")&&
                                !tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_region_value.getText().toString()+"-"
                                            +tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "SalesArea");
                        }else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                        }
                        if(!tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", filteredRO);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", SelectedRO);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                        }else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    try {
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "SelectdRegion", SelectedRegion);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea", SelectedSalesArea);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FilteredRO", filteredRO);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                    "FilteredROName", SelectedRO);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                        "").equalsIgnoreCase("HPCLREGION")){
                    try {
                        if(!tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")&&
                                tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "SalesArea");
                        }
                        else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                        }

                        if(!tv_select_RO_value.getText().toString().equalsIgnoreCase("Select RO")){
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Selectd_Name","");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "Request_Field", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", tv_select_region_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",tv_select_sales_area_value.getText().toString());
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", filteredRO);
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FirstFilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", SelectedRO);
                        }
                        else{
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredRO", "");
                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                    "FilteredROName", "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    try {
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "SelectdSalesArea", SelectedSalesArea);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FilteredRO", filteredRO);
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "FilteredROName", SelectedRO);
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                }
                else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                        "").equalsIgnoreCase("HPCLSALESAREA")){
                    try {
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FilteredRO", filteredRO);
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FilteredROName", SelectedRO);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                if(bundle.getString("Filter_fragment").equalsIgnoreCase("HomeFragment_old")){
                    //get values from HomeFragment which selected and pass to again HomeFragment to default select.
                    try {
                        getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    HomeFragment_old filter_screen = new HomeFragment_old();
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("SalesTrends")){
//                    HomeFragment.dashboardapi = false;
                    try {
                        int backStackId = 0;
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                        } catch (Exception e) {
                            e.printStackTrace();
                            try {
                                backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                        getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                            "dashboardapi", "false");

                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
//                    Fragment_FuelSalesTrends filter_screen = new Fragment_FuelSalesTrends();
                    Fragment_FuelSalesTrends_Float filter_screen = new Fragment_FuelSalesTrends_Float();
                    txnBundle.putString("FilteredFragment","SalesTrends");
                    txnBundle.putString("Fuel_Type",bundle.getString("Fuel_Type"));
                    txnBundle.putString("Date",bundle.getString("Date"));
                    txnBundle.putString("Date_type",bundle.getString("Date_type"));
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);


                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("Comparative")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    Fragment_ComparativeSales filter_screen = new Fragment_ComparativeSales();
                    txnBundle.putString("FilteredFragment","ComparativeFragment");
                    txnBundle.putString("Date_type",bundle.getString("Date_type"));
                    txnBundle.putString("Month1",bundle.getString("Month1"));
                    txnBundle.putString("Month2",bundle.getString("Month2"));
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("WetInventory")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentWetInventory filter_screen = new FragmentWetInventory();
                    try {
                        txnBundle.putString("fragment",bundle.getString("fragment"));
                        txnBundle.putString("FilteredFragment","WETInventory");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);
                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("Equipment")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    Fragmnet_Equipment filter_screen = new Fragmnet_Equipment();
                    try {
                        txnBundle.putString("fragment",bundle.getString("fragment"));
                        txnBundle.putString("FilteredFragment","Equipment");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);
                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("RO_SnapShot")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentROSnapsahot filter_screen = new FragmentROSnapsahot();
                    txnBundle.putString("FilteredFragment","RO_SnapShotFragment");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_Price_Status")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentPriceExceptionList filter_screen = new FragmentPriceExceptionList();
                    txnBundle.putString("FilteredFragment","LD_Price_Status");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_ROConnectivity_Status")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentROConnectivity filter_screen = new FragmentROConnectivity();
                    txnBundle.putString("FilteredFragment","LD_ROConnectivity_Status");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_Critical_Stcock")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentCriticalStockList filter_screen = new FragmentCriticalStockList();
                    txnBundle.putString("FilteredFragment","LD_Critical_Stcock");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else if(bundle.getString("Filter_fragment").equalsIgnoreCase("LD_Nano_status")){
//                    HomeFragment.dashboardapi = false;
                    int backStackId = 0;
                    try {
                        backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(1).getId();
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            backStackId = getActivity().getSupportFragmentManager().getBackStackEntryAt(0).getId();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                    getActivity().getSupportFragmentManager().popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    FragmentNanoStatus filter_screen = new FragmentNanoStatus();
                    txnBundle.putString("FilteredFragment","LD_Nano_status");

                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
            }
        });

        return view;
    }

    @Override
    public void onItemClick(String selectedBranch) {
        if(ZoneClicked){
            tv_select_zone_value.setText(selectedBranch);
            SelectedZone = selectedBranch;
            tv_select_region_value.setText("Select Region");
            tv_select_sales_area_value.setText("Select Sales Area");
            tv_select_RO_value.setText("Select RO");
            tv_select_sales_area_value.setEnabled(false);
            tv_select_RO_value.setEnabled(false);
            CallRegion();
        }
        else if(Regionclicked){
            tv_select_region_value.setText(selectedBranch);
            SelectedRegion = selectedBranch;
            tv_select_sales_area_value.setText("Select Sales Area");
            tv_select_RO_value.setText("Select RO");
            tv_select_sales_area_value.setEnabled(true);
            tv_select_RO_value.setEnabled(false);
//            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    "SelectdRegion", SelectedRegion);
            CallSales_Area();
        }
        else if(SalesAreaClicked){
            tv_select_sales_area_value.setText(selectedBranch);
            SelectedSalesArea = selectedBranch;
            tv_select_RO_value.setText("Select RO");
            tv_select_RO_value.setEnabled(true);
//            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    "SelectdSalesArea", SelectedSalesArea);
            CallRO();
        }
        else if(ROClicked){
            tv_select_RO_value.setText(selectedBranch);
            SelectedRO = selectedBranch;
            try {
                if(SelectedRO.equalsIgnoreCase("ALL")) {
                    filteredRO = SelectedRO;
                }else{
                    filteredRO = SelectedRO.substring(0,8);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
//            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    "FilteredRO", filteredRO);
//            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                    "FilteredROName", SelectedRO);
        }
        try {
            if (dialogSelectBranch.isShowing()) {
                dialogSelectBranch.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void methodDone() {

    }

    @Override
    public void methodCancel() {

    }

    private void selectBranch() {


        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        final View loginFormView = getLayoutInflater().inflate(R.layout.dialog_recyclerview_new, null);
        // Set above view in alert dialog.
        builder.setView(loginFormView);

        RecyclerView recyclerView;
        TextView title;
        recyclerView = (RecyclerView) loginFormView.findViewById(R.id.rv_filter_list);
        title = (TextView) loginFormView.findViewById(R.id.title);
        try {

            final LinearLayoutManager mLayoutManager;
            mLayoutManager = new LinearLayoutManager(getContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());

            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLHQ")){
                if(ZoneClicked){
                    title.setText("Select Zone");
                    search_adapter = new Dialog_search_adapter_multiple(ZoneList, getContext(), fragment_filter_textview_multiple.this);
                }else if(Regionclicked){
                    title.setText("Select Region");
                    search_adapter = new Dialog_search_adapter_multiple(RegionValue, getContext(), fragment_filter_textview_multiple.this);
                }else if(SalesAreaClicked){
                    title.setText("Select Sales Area");
                    search_adapter = new Dialog_search_adapter_multiple(SalesAreaValue, getContext(), fragment_filter_textview_multiple.this);
                }else if(ROClicked){
                    title.setText("Select RO");
                    search_adapter = new Dialog_search_adapter_multiple(ROValue, getContext(), fragment_filter_textview_multiple.this);
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLZONE")){
                if(Regionclicked){
                    title.setText("Select Region");
                    search_adapter = new Dialog_search_adapter_multiple(ZoneList, getContext(), fragment_filter_textview_multiple.this);
                }else if(SalesAreaClicked){
                    title.setText("Select Sales Area");
                    search_adapter = new Dialog_search_adapter_multiple(SalesAreaValue, getContext(), fragment_filter_textview_multiple.this);
                }else if(ROClicked){
                    title.setText("Select RO");
                    search_adapter = new Dialog_search_adapter_multiple(ROValue, getContext(), fragment_filter_textview_multiple.this);
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLREGION")){
                if(SalesAreaClicked){
                    title.setText("Select Sales Area");
                    search_adapter = new Dialog_search_adapter_multiple(ZoneList, getContext(), fragment_filter_textview_multiple.this);
                }else if(ROClicked){
                    title.setText("Select RO");
                    search_adapter = new Dialog_search_adapter_multiple(ROValue, getContext(), fragment_filter_textview_multiple.this);
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLSALESAREA")){
                if(ROClicked){
                    title.setText("Select RO");
                    search_adapter = new Dialog_search_adapter_multiple(ZoneList, getContext(), fragment_filter_textview_multiple.this);
                }
            }

            recyclerView.setAdapter(search_adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
        builder.setView(loginFormView);
        dialogSelectBranch = builder.create();// show it
        dialogSelectBranch.show();
    }

    private void CallRegion(){
        try {
            JSONObject json = new JSONObject();
            if(!tv_select_zone_value.getText().toString().equalsIgnoreCase("Select Zone")){
                json.put("Para1",tv_select_zone_value.getText().toString());
            }
            new AsyncRegion().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncRegion extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncRegion() {
        }

        public AsyncRegion(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetRegionDetail", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            Regionarray = new JSONArray(jsonObject.getString("Data"));
                            RegionKey = new ArrayList<String>();
                            RegionValue = new ArrayList<String>();
                            RegionKey.add("ALL");
                            for (int i1=0; i1<Regionarray.length(); i1++) {
                                RegionKey.add(Regionarray.getJSONObject(i1).getString("Key"));
                            }
                            RegionValue.add("ALL");
                            for (int i1=0; i1<Regionarray.length(); i1++) {
                                RegionValue.add(Regionarray.getJSONObject(i1).getString("Value"));
                            }
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void CallSales_Area(){
        try {
            JSONObject json = new JSONObject();
            if(!tv_select_region_value.getText().toString().equalsIgnoreCase("Select Region")){
                json.put("Para1",tv_select_zone_value.getText().toString() );
                json.put("Para2",tv_select_region_value.getText().toString() );
            }
            new AsyncsalesArea().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncsalesArea extends AsyncTask<JSONObject, Void, String>{
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncsalesArea() {
        }

        public AsyncsalesArea(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetSalesArea", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            SalesAreaArray = new JSONArray(jsonObject.getString("Data"));
                            SalesAreaKey = new ArrayList<String>();
                            SalesAreaValue = new ArrayList<String>();
                            SalesAreaKey.add("ALL");
                            for (int i1=0; i1<SalesAreaArray.length(); i1++) {
                                SalesAreaKey.add(SalesAreaArray.getJSONObject(i1).getString("Key"));
                            }
                            SalesAreaValue.add("ALL");
                            for (int i1=0; i1<SalesAreaArray.length(); i1++) {
                                SalesAreaValue.add(SalesAreaArray.getJSONObject(i1).getString("Value"));
                            }
//populate dialog with list
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void CallRO(){
        try {
            JSONObject json = new JSONObject();
            if(!tv_select_sales_area_value.getText().toString().equalsIgnoreCase("Select Sales Area")){
                json.put("Para1",tv_select_zone_value.getText().toString() );
                json.put("Para2",tv_select_region_value.getText().toString() );
                json.put("Para3",tv_select_sales_area_value.getText().toString() );
//                json.put("Para2","10" );
            }

            new AsyncRO().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private class AsyncRO extends AsyncTask<JSONObject, Void, String>{
        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncRO() {
        }

        public AsyncRO(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Dashboard/GetROList", false, jsonObjects[0],"");
        }
        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                JSONObject jsonObj = null;
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {

                            JSONObject jsonObject = new JSONObject(str[2]);
                            ROArray = new JSONArray(jsonObject.getString("Data"));
                            ROKey = new ArrayList<String>();
                            ROValue = new ArrayList<String>();
                            ROKey.add("ALL");
                            for (int i1=0; i1<ROArray.length(); i1++) {
                                ROKey.add(ROArray.getJSONObject(i1).getString("Key"));
                            }
                            ROValue.add("ALL");
                            for (int i1=0; i1<ROArray.length(); i1++) {
                                ROValue.add(ROArray.getJSONObject(i1).getString("Value"));
                            }
//                 populate with list
                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else if(respCode.equalsIgnoreCase("999")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.dialog_ok), "");
                            universalDialog.showAlert();

                        }
                    }else{

                    }
                }catch (Exception e){
                    e.printStackTrace();


                }
            }
        }

    }

    private void populateROList() {
        ZoneList = new ArrayList<>();
        ROCODE_List = new ArrayList<>();
        if(UserDataPrefrence.getArrayList( "ROLIst",getContext()).size() != 1){
            if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLHQ")){
//                ZoneList.add("Select Zone");
            }else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLZONE")){
//                ZoneList.add("Select REGION");
            } else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLREGION")){
//                ZoneList.add("Select Sales Area");
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference",getContext(),"UserCustomRole",
                    "").equalsIgnoreCase("HPCLSALESAREA")){
//                ZoneList.add("Select RO");
            }
            ZoneList.add(("ALL"));
            ZoneList.addAll(UserDataPrefrence.getArrayList( "ROCode",getContext()));
            ROCODE_List.addAll(UserDataPrefrence.getArrayList( "ROLIst",getContext()));

        } else if(UserDataPrefrence.getArrayList( "ROLIst",getContext()).size() == 0){
        } else{
            ZoneList.addAll(UserDataPrefrence.getArrayList( "ROLIst",getContext()));
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.agstransact.HP_SC.dashboard.adapter.NewDesign;

public class Pump_model_new {
    String ROCode;
    String AUTOMOBILE;
    String PumpNumber;
    String NozzleNumber;
    String Nozzles;
    String DUType;
    String DUMake;
    String TransactionDateTime;
    String Product;
    String Price;
    String Quantity;
    String Amount;
    String Online;

    public String getDUNo() {
        return DUNo;
    }

    public void setDUNo(String DUNo) {
        this.DUNo = DUNo;
    }

    String DUNo;

    public Pump_model_new(String ROCode, String AUTOMOBILE, String pumpNumber, String nozzleNumber, String nozzles, String DUType, String DUMake, String DUNo, String transactionDateTime, String product, String price, String quantity, String amount, String online) {
        this.ROCode = ROCode;
        this.AUTOMOBILE = AUTOMOBILE;
        PumpNumber = pumpNumber;
        NozzleNumber = nozzleNumber;
        Nozzles = nozzles;
        this.DUType = DUType;
        this.DUMake = DUMake;
        this.DUNo = DUNo;
        TransactionDateTime = transactionDateTime;
        Product = product;
        Price = price;
        Quantity = quantity;
        Amount = amount;
        Online = online;
    }
    public String getROCode() {
        return ROCode;
    }

    public void setROCode(String ROCode) {
        this.ROCode = ROCode;
    }

    public String getAUTOMOBILE() {
        return AUTOMOBILE;
    }

    public void setAUTOMOBILE(String AUTOMOBILE) {
        this.AUTOMOBILE = AUTOMOBILE;
    }

    public String getPumpNumber() {
        return PumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {
        PumpNumber = pumpNumber;
    }

    public String getNozzleNumber() {
        return NozzleNumber;
    }

    public void setNozzleNumber(String nozzleNumber) {
        NozzleNumber = nozzleNumber;
    }

    public String getNozzles() {
        return Nozzles;
    }

    public void setNozzles(String nozzles) {
        Nozzles = nozzles;
    }

    public String getDUType() {
        return DUType;
    }

    public void setDUType(String DUType) {
        this.DUType = DUType;
    }

    public String getDUMake() {
        return DUMake;
    }

    public void setDUMake(String DUMake) {
        this.DUMake = DUMake;
    }

    public String getTransactionDateTime() {
        return TransactionDateTime;
    }

    public void setTransactionDateTime(String transactionDateTime) {
        TransactionDateTime = transactionDateTime;
    }

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getPrice() {
        return Price;
    }

    public void setPrice(String price) {
        Price = price;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getOnline() {
        return Online;
    }

    public void setOnline(String online) {
        Online = online;
    }


}

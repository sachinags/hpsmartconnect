package com.agstransact.HP_SC.dashboard.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.RippleImageView;
import com.agstransact.HP_SC.utils.RippleLinearLayout;
import com.agstransact.HP_SC.utils.RippleTextView;
import com.agstransact.HP_SC.utils.UniversalDialog;

public class FragmentWetInventory extends Fragment implements View.OnClickListener {

    private View rootView;
    private ImageView iv_avg_nozzle_tp,iv_tank_pump_status,iv_filter;
    private ImageView iv_fuel_sales_trends,iv_fuel_comparative_trends,iv_bnv_fuel_sale,
            iv_bnv_wet_inventory,iv_bnv_dashbord,iv_bnv_ro_snapshot,iv_bnv_equipment;
    RippleLinearLayout fuel_salesLL, wet_inventoryLL, dashboard_LL,ro_snapshotLL,equipmentLL;
    RippleImageView fuel_salesIV, wet_inventoryIV, dashboardIV,ro_snapshotIV,equipmentIV;
    RippleTextView fuels_salesTV,wet_inventoryTV,dashboardTV,ro_snapshot_TV,equipmentTV;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_wet_inventory,container,false);
//        Toolbar toolbar = getActivity().findViewById(R.id.toolbar);
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//        toolbarTV.setText(getResources().getString(R.string.wet_inventory));
        setUpViews();
        fuel_salesLL = (RippleLinearLayout)   rootView.findViewById(R.id.fuel_salesLL);
        wet_inventoryLL = (RippleLinearLayout)rootView.findViewById(R.id.wet_inventoryLL);
        dashboard_LL = (RippleLinearLayout)   rootView.findViewById(R.id.dashboard_LL);
        ro_snapshotLL = (RippleLinearLayout)  rootView.findViewById(R.id.ro_snapshotLL);
        equipmentLL = (RippleLinearLayout)    rootView.findViewById(R.id.equipmentLL);
        fuel_salesIV = (RippleImageView)    rootView.findViewById(R.id.fuel_salesIV);
        wet_inventoryIV = (RippleImageView) rootView.findViewById(R.id.wet_inventoryIV);
        dashboardIV = (RippleImageView)     rootView.findViewById(R.id.dashboardIV);
        ro_snapshotIV = (RippleImageView)   rootView.findViewById(R.id.ro_snapshotIV);
        equipmentIV = (RippleImageView)     rootView.findViewById(R.id.equipmentIV);
        fuels_salesTV = (RippleTextView)    rootView.findViewById(R.id.fuels_salesTV);
        wet_inventoryTV = (RippleTextView)  rootView.findViewById(R.id.wet_inventoryTV);
        dashboardTV = (RippleTextView)      rootView.findViewById(R.id.dashboardTV);
        ro_snapshot_TV = (RippleTextView)   rootView.findViewById(R.id.ro_snapshot_TV);
        equipmentTV = (RippleTextView)      rootView.findViewById(R.id.equipmentTV);
        wet_inventoryIV.setImageResource(R.drawable.wet_inventory_enabled_new_wt);
        fuel_salesLL.setOnClickListener(this);
        wet_inventoryLL.setOnClickListener(this);
        dashboard_LL.setOnClickListener(this);
        ro_snapshotLL.setOnClickListener(this);
        equipmentLL.setOnClickListener(this);
        fuel_salesIV.setOnClickListener(this);
        wet_inventoryIV.setOnClickListener(this);
        dashboardIV.setOnClickListener(this);
        ro_snapshotIV.setOnClickListener(this);
        equipmentIV.setOnClickListener(this);
        fuels_salesTV.setOnClickListener(this);
        wet_inventoryTV.setOnClickListener(this);
        dashboardTV.setOnClickListener(this);
        ro_snapshot_TV.setOnClickListener(this);
        equipmentTV.setOnClickListener(this);

//        iv_bnv_fuel_sale.setOnClickListener(this);
//        iv_bnv_wet_inventory.setOnClickListener(this);
//        iv_bnv_dashbord.setOnClickListener(this);
//        iv_bnv_ro_snapshot.setOnClickListener(this);
//        iv_bnv_equipment.setOnClickListener(this);
        return rootView;
    }

    private void setUpViews() {
        iv_bnv_fuel_sale = rootView.findViewById(R.id.iv_bnv_fuel_sale);
        iv_bnv_wet_inventory = rootView.findViewById(R.id.iv_bnv_wet_inventory);
        iv_bnv_dashbord = rootView.findViewById(R.id.iv_bnv_dashbord);
        iv_bnv_ro_snapshot = rootView.findViewById(R.id.iv_bnv_ro_snapshot);
        iv_bnv_equipment = rootView.findViewById(R.id.iv_bnv_equipment);

        iv_avg_nozzle_tp = (ImageView) rootView.findViewById(R.id.iv_avg_nozzle_tp);
        iv_tank_pump_status = (ImageView) rootView.findViewById(R.id.iv_tank_pump_status);
        iv_filter = (ImageView) rootView.findViewById(R.id.iv_filter);

        iv_filter.setOnClickListener(this);
        iv_avg_nozzle_tp.setOnClickListener(this);
        iv_tank_pump_status.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_filter:
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                    Bundle txnBundle = new Bundle();
                    txnBundle.putString("Filter_fragment", "WetInventory");
//                    fragment_filter_textview filter_screen = new fragment_filter_textview();
                    fragment_filter_textview_multiple_new filter_screen = new fragment_filter_textview_multiple_new();
                    filter_screen.setArguments(txnBundle);
                    changeFrag.addFragment(filter_screen);

                }
                else{
                    UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {

                        }

                        @Override
                        public void methodCancel() {

                        }
                    },
                            "",
                            "No filter....."
                            , getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
                }
                break;

            case R.id.iv_avg_nozzle_tp:
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {
                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("Select RO") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "").equalsIgnoreCase("ALL") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("ALL")) {
                        UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                            @Override
                            public void methodDone() {

                            }

                            @Override
                            public void methodCancel() {

                            }
                        },
                                "",
                                "Please Select RO"
                                , getString(R.string.dialog_ok), "");
                        universalDialog.showAlert();
                    }
                    else {
                        replaceScreen(new FragmentAverageNozzle());
                    }
                }else{
                    replaceScreen(new FragmentAverageNozzle());
        }

                break;

            case R.id.iv_tank_pump_status:
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {

                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("Select RO") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "").equalsIgnoreCase("ALL") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("ALL")) {
                        UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                            @Override
                            public void methodDone() {

                            }

                            @Override
                            public void methodCancel() {

                            }
                        },
                                "",
                                "Please Select RO"
                                , getString(R.string.dialog_ok), "");
                        universalDialog.showAlert();
                    } else {
                        replaceScreen(new Wet_inventory_frag_new());
                    }
                }
                else{
                    replaceScreen(new Wet_inventory_frag_new());
                }
                break;

            case R.id.fuel_salesLL:
            case R.id.fuel_salesIV:
            case R.id.fuels_salesTV:
                ActionChangeFrag changeFrag6 = (ActionChangeFrag) getActivity();
                FragmentFuelSales fuelSales = new FragmentFuelSales();
                changeFrag6.addFragment(fuelSales);
                break;

            case R.id.wet_inventoryLL:
            case R.id.wet_inventoryTV:
            case R.id.wet_inventoryIV:
                ActionChangeFrag changeFrag1 = (ActionChangeFrag) getActivity();
                FragmentWetInventory wet_inventory = new FragmentWetInventory();
                changeFrag1.addFragment(wet_inventory);
                break;

            case R.id.ro_snapshotLL:
            case R.id.ro_snapshotIV:
            case R.id.ro_snapshot_TV:
                ActionChangeFrag changeFrag2 = (ActionChangeFrag) getActivity();
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "UserCustomRole","").equalsIgnoreCase("DEALER")) {

                    if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("Select RO") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "").equalsIgnoreCase("ALL") || UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO", "").equalsIgnoreCase("ALL")) {
                        UniversalDialog universalDialog = new UniversalDialog(getContext(), new AlertDialogInterface() {
                            @Override
                            public void methodDone() {

                            }

                            @Override
                            public void methodCancel() {

                            }
                        },
                                "",
                                "Please Select RO"
                                , getString(R.string.dialog_ok), "");
                        universalDialog.showAlert();
                    } else {
                        ActionChangeFrag changeFrag3 = (ActionChangeFrag) getActivity();
                        FragmentROSnapsahot ro_snapshot = new FragmentROSnapsahot();
                        changeFrag3.addFragment(ro_snapshot);
                    }
                }else{
                    ActionChangeFrag changeFrag3 = (ActionChangeFrag) getActivity();
                    FragmentROSnapsahot ro_snapshot = new FragmentROSnapsahot();
                    changeFrag3.addFragment(ro_snapshot);
                }
                break;
            case R.id.equipmentLL:
            case R.id.equipmentTV:
            case R.id.equipmentIV:
                ActionChangeFrag changeFrag4 = (ActionChangeFrag) getActivity();
                Fragmnet_Equipment equipment = new Fragmnet_Equipment();
                changeFrag4.addFragment(equipment);
                break;
            case R.id.dashboard_LL:
            case R.id.dashboardIV:
            case R.id.dashboardTV:
                ActionChangeFrag changeFrag7 = (ActionChangeFrag) getActivity();
                HomeFragment_old home_Frag = new HomeFragment_old();
                changeFrag7.addFragment(home_Frag);
                break;
        }

    }

    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.dashboard_container, frag, "LOGIN_USERID");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("LOGIN_USERID");
        fragmentTransaction.commit();

    }
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.actionbar_menu_home, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                try {
                    getActivity().getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                ConstantDeclaration.fun_changeNav_withRole(getActivity(), changeFrag
                        , new HomeFragment_old(), new HomeFragment_old()
                        , new HomeFragment_old(), new HomeFragment_old());

                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

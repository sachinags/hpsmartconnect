package com.agstransact.HP_SC.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.InterlockModel;
import com.agstransact.HP_SC.model.Nozzle_status_Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Equipment_Adapter extends RecyclerView.Adapter<Equipment_Adapter.MyInterlockModel> {
    private Context context;
    private ArrayList<Nozzle_status_Model> models = new ArrayList<>();
    private JSONObject jsonobject_equipment;
    private JSONArray zone_array;
    private RecyclerView.RecycledViewPool viewPool = new RecyclerView.RecycledViewPool();

    public Equipment_Adapter(Activity activity, ArrayList<Nozzle_status_Model> models) {
        this.context = activity;
        this.models = models;
    }
    public Equipment_Adapter(Activity activity, JSONObject jsonobject_equipment,JSONArray zone_array) {
        this.context = activity;
        this.jsonobject_equipment = jsonobject_equipment;
        this.zone_array = zone_array;
    }
    public Equipment_Adapter(Activity activity,JSONArray zone_array) {
        this.context = activity;
        this.zone_array = zone_array;
    }


    @NonNull
    @Override
    public Equipment_Adapter.MyInterlockModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.equipment_adapter,parent,false);
        return new MyInterlockModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Equipment_Adapter.MyInterlockModel holder, int position) {

//        Nozzle_status_Model model = models.get(position);

        try {
//            holder.tv_product_value.setText(zone_array.get(position).toString());
            holder.tv_dumake_lbl.setText(zone_array.getJSONObject(position).getString("Zones"));
            holder.tv_duno.setText(zone_array.getJSONObject(position).getString("Pump"));
            holder.tv_pump_no.setText(zone_array.getJSONObject(position).getString("Tank"));
            holder.tv_nozzle_no.setText(zone_array.getJSONObject(position).getString("DU"));
            holder.tv_produt.setText(zone_array.getJSONObject(position).getString("Nozzle"));
//            holder.tv_nozzle_conf_value.setText(jsonobject_equipment.getJSONArray(zone_array.get(position).toString()).getJSONObject(position).getString("Product"));
//            holder.tv_nozzle_in_use_value.setText(jsonobject_equipment.getJSONArray(zone_array.get(position).toString()).getJSONObject(position).getString("Tank"));
//            holder.tv_nozzle_not_in_use_value.setText(jsonobject_equipment.getJSONArray(zone_array.get(position).toString()).getJSONObject(position).getString("Du"));
//            holder.tv_nozzle_in_use_value.setText(model.getNozzlesinUse());
//            holder.tv_nozzle_not_in_use_value.setText(model.getNozzlesDisabled());


        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Create layout manager with initial prefetch item count
//        LinearLayoutManager layoutManager = new LinearLayoutManager(
//                holder.rv_zone_details.getContext(),
//                LinearLayoutManager.VERTICAL,
//                false
//        );
//        try {
//            layoutManager.setInitialPrefetchItemCount(jsonobject_equipment.getJSONArray(zone_array.get(position).toString()).length());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        // Create sub item view adapter
//        Equipment_Details_Adapter subItemAdapter = null;
////        try {
////            if(json_array_pump_nozzle.getJSONObject(listPosition).getJSONArray("LstNozzleStatus").length() >= 2){
//////                subItemAdapter = new nozzle_adapter(json_array_pump_nozzle.getJSONObject(listPosition).getJSONArray("LstNozzleStatus"));
////                subItemAdapter = new Equipment_Details_Adapter(json_array_pump_nozzle.getJSONObject(listPosition).getJSONArray("LstNozzleStatus"));
////            }else{
////                subItemAdapter = new Equipment_Details_Adapter(json_array_pump_nozzle.getJSONObject(listPosition).getJSONArray("LstNozzleStatus"));
////            }
////        } catch (JSONException e) {
////            e.printStackTrace();
////        }
//        try {
//            subItemAdapter = new Equipment_Details_Adapter(jsonobject_equipment.getJSONArray(zone_array.get(position).toString()));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        holder.rv_zone_details.addItemDecoration(new DividerItemDecoration(context, 0));
//        holder.rv_zone_details.setLayoutManager(layoutManager);
//        holder.rv_zone_details.setAdapter(subItemAdapter);
//        holder.rv_zone_details.setRecycledViewPool(viewPool);

    }

    @Override
    public int getItemCount() {
        return zone_array.length();
    }

    public class MyInterlockModel extends RecyclerView.ViewHolder {

        private TextView tv_product_value,tv_nozzle_conf_value,tv_nozzle_in_use_value,tv_nozzle_not_in_use_value;
        private TextView tv_produt,tv_dumake_lbl,tv_duno,tv_pump_no,tv_nozzle_no;
        RecyclerView rv_zone_details;

        public MyInterlockModel(@NonNull View itemView) {
            super(itemView);

            tv_product_value = itemView.findViewById(R.id.tv_product_value);
            tv_nozzle_conf_value = itemView.findViewById(R.id.tv_nozzle_conf_value);
            tv_nozzle_in_use_value = itemView.findViewById(R.id.tv_nozzle_in_use_value);
            tv_nozzle_not_in_use_value = itemView.findViewById(R.id.tv_nozzle_not_in_use_value);

            tv_produt = itemView.findViewById(R.id.tv_tank_no);
            tv_dumake_lbl = itemView.findViewById(R.id.tv_dumake_lbl);
            tv_dumake_lbl.setSelected(true);
            tv_duno = itemView.findViewById(R.id.tv_duno);
            tv_pump_no = itemView.findViewById(R.id.tv_pump_no);
            tv_nozzle_no = itemView.findViewById(R.id.tv_nozzle_no);
            rv_zone_details = itemView.findViewById(R.id.rv_zone_details);
        }
    }
}

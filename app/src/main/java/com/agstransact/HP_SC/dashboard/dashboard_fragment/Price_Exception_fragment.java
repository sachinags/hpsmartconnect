package com.agstransact.HP_SC.dashboard.dashboard_fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.model.CombinedModel;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.Log;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Price_Exception_fragment extends Fragment {
    View rootView;
    private String error="", respCode="",fuel_type="";
    private AlertDialogInterface alertDialogInterface;
    private UniversalDialog universalDialog;
    private TextView tv_dt_price;
    List<String> listKeys;
    List<Integer> listValues;
    private ArrayList pieEntries;
    private PieDataSet pieDataSet;
    private PieData pieData;
    private PieChart pieChart;
    private ArrayList<CombinedModel> combinedModels = new ArrayList<>();
    private int [] colorcode = {R.color.line_chart_hsd_color,R.color.line_chart_ms_color,R.color.line_chart_power_color,R.color.line_chart_turbojet_color};


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fargment_price_exception, container, false);
        tv_dt_price = (TextView) rootView.findViewById(R.id.tv_dt_price);
        pieChart = (PieChart) rootView.findViewById(R.id.piechart_price_exception);
        getPieChartData();

        return  rootView;
    }
    private void getPieChartData() {

        JSONObject json = new JSONObject();
        try {
            if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLHQ")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Zone")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "Selectdzone","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }

                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone",""));
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{

                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "Selectdzone", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "Selectdzone", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Zone", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "Selectdzone", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLZONE")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("Region")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRegion","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){
                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion",""));
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }
                        else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdRegion", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdRegion", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("Region", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdRegion", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else  if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLREGION")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "Request_Selectd","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "Request_Field","").equalsIgnoreCase("SalesArea")){

                        if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdSalesArea","").equalsIgnoreCase("ALL")){
                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea",""));
                            json.put("ROCode", "ALL");
                        }else{
                            json.put("ROCode", "ALL");
                        }
                    }
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO","").equalsIgnoreCase("ALL")) {
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }else{
                        try {
                            if (UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                    "SelectdSalesArea", "").equalsIgnoreCase("ALL") ||
                                    UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea", "").equalsIgnoreCase("")) {
                            } else {
                                json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                        "SelectdSalesArea", ""));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "FilteredRO",""));
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "FirstFilteredRO", "");
                    }

                    //                    try {
                    //                        if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO","").equalsIgnoreCase("ALL")&&
                    //                                !UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                        "SelectdSalesArea","").equalsIgnoreCase("")){
                    //
                    //                            json.put("SalesArea", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "SelectdSalesArea",""));
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }else{
                    //                            json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                    "FilteredRO",""));
                    //                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                    "FirstFilteredRO", "");
                    //                        }
                    //                    } catch (Exception e) {
                    //                        e.printStackTrace();
                    //                        json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    //                                "FilteredRO",""));
                    //                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                    //                                "FirstFilteredRO", "");
                    //                    }

                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }

            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("HPCLSALESAREA")){
                if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FilteredRO",""));
                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO", "");
                }
                else if(!UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "FirstFilteredRO","").equalsIgnoreCase("")){
                    json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                            "FirstFilteredRO",""));
                }
            }
            else if(UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserCustomRole","").equalsIgnoreCase("DEALER")){
                json.put("ROCode", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                        "ROKey","") );
            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        new Asyncprice_exception().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    private class Asyncprice_exception extends AsyncTask<JSONObject, Void, String> {
        String strTimeStamp = "";

        public Asyncprice_exception() {
        }

        public Asyncprice_exception(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Status/PriceException", false, jsonObjects[0], "");
        }


        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }
            if (s != null) {
                try {
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        respCode = str[0];
                        if(respCode.equalsIgnoreCase("00")) {
                            JSONObject jsonObject = new JSONObject(str[2]);
                            JSONObject data = jsonObject.getJSONObject("Data");
                            JSONArray valuesArray = data.getJSONArray("Value");
                            JSONArray jsonArray = data.getJSONArray("Category");
                            JSONArray array = data.getJSONArray("HPEffectiveDate");

                            for (int ts =0;ts<array.length();ts++){
//                                JSONObject timeObj = statusArray.getJSONObject(ts);
                                String nanoStatus = array.getString(ts);
                                SimpleDateFormat curFormater = new SimpleDateFormat("dd-MM-yyyy");
                                Date dateObj = null;
//                                String fromDate = timeObj.getString("FromDate");
                                dateObj = curFormater.parse(nanoStatus);
                                Log.e("dta",nanoStatus);
                                SimpleDateFormat postFormater = new SimpleDateFormat("DD |MM | YYYY");

                                String nFDate = postFormater.format(dateObj);
                                tv_dt_price.setText(nanoStatus.replace("-"," |"));
                            }
//                            JSONArray arrayPrice_Exception = new JSONArray(jsonObject.getString("Data"));
//                            listData = new ArrayList<String>();
                            combinedModels = new ArrayList<>();
                            for (int i1 = 0; i1 < valuesArray.length(); i1++) {
                                CombinedModel model = new CombinedModel();
                                model.setCategoryName(jsonArray.getString(i1));
                                model.setValue(valuesArray.getString(i1));
                                combinedModels.add(model);
//                                listData.add(valuesArray.getString(i1));
                            }
                            getEntries();
                            pieDataSet = new PieDataSet(pieEntries, "");
                            pieData = new PieData(pieDataSet);
                            pieChart.setData(pieData);
                            pieChart.getData().setDrawValues(false);
                            pieChart.setDrawEntryLabels(false);
                            pieChart.setDrawSliceText(false);

                            listKeys = new ArrayList<String>();
                            listValues = new ArrayList<Integer>();
                            for (int i1 = 0; i1 < jsonArray.length(); i1++) {
                                listKeys.add(jsonArray.getString(i1));
                            }
                            for (int value = 0; value < valuesArray.length(); value++) {
                                listValues.add((int) Float.parseFloat(valuesArray.getString(value)));
                            }

                            final int[] MY_COLORS = {Color.rgb(255, 124, 16), Color.rgb(52, 91, 149), Color.rgb(43, 51, 50),
                                    Color.rgb(249, 82, 147)};
                            ArrayList<Integer> colors = new ArrayList<Integer>();

                            for (int c : MY_COLORS) colors.add(c);
                            pieChart.getLegend().setEnabled(false);
                            pieDataSet.setColors(ColorTemplate.createColors(colorcode));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLACK);
                            pieDataSet.setValueTextSize(10f);
                            pieChart.getDescription().setEnabled(false);

                            LegendEntry[] legendEntries=new LegendEntry[pieEntries.size()];
                            for(int i=0;i<legendEntries.length;i++)
                            {
                                LegendEntry entry=new LegendEntry();
                                entry.formColor=MY_COLORS[i];
                                try {
                                    entry.label=listKeys.get(i)+ " = "+listValues.get(i);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                legendEntries[i]=(entry);

                            }
                            pieChart.getLegend().setCustom(legendEntries);
                            pieChart.getLegend().setTextColor(Color.BLACK);
                            pieChart.setExtraBottomOffset(30f);
                            pieChart.getLegend().setWordWrapEnabled(true);
                            pieChart.getLegend().setPosition(Legend.LegendPosition.BELOW_CHART_CENTER);
                            pieChart.getLegend().setEnabled(true);
                            pieChart.getLegend().setTextColor(getResources().getColor(R.color.black));
                            pieChart.getLegend().setTextSize(8f);
                            pieChart.getLegend().setYOffset(-8f);
                            pieChart.getLegend().setXOffset(8f);
                            pieChart.getLegend().getHorizontalAlignment();
                            pieChart.getLegend().setOrientation(Legend.LegendOrientation.VERTICAL);
                            pieChart.getLegend().setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
                            pieChart.getLegend().setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
                            pieChart.getLegend().setWordWrapEnabled(true);
                            pieDataSet.setColors(ColorTemplate.createColors(MY_COLORS));
                            pieDataSet.setSliceSpace(2f);
                            pieDataSet.setValueTextColor(Color.BLACK);
                            pieDataSet.setValueTextSize(8f);
                            pieChart.getDescription().setEnabled(false);


                            pieChart.setOnChartValueSelectedListener(new OnChartValueSelectedListener() {
                                @Override
                                public void onValueSelected(Entry e, Highlight h) {
                                    PieEntry pe = (PieEntry) e;
                                    String str= pe.getLabel();
                                    str = str.replaceAll("[^a-zA-Z0-9]", " ");
                                    pieChart.setCenterText(str+"\n"+ConstantDeclaration.amountFormatter_pie(Double.valueOf(e.getY())));
                                    pieChart.setCenterTextColor(getResources().getColor(R.color.black));


                                }

                                @Override
                                public void onNothingSelected() {
                                    pieChart.setCenterText("");

                                }
                            });
                            pieChart.setCenterText(jsonArray.getString(0)+"\n"+valuesArray.getInt(0));

                            pieChart.invalidate();



                        }
                        else if(respCode.equalsIgnoreCase("401")){
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    new AlertDialogInterface() {
                                        @Override
                                        public void methodDone() {
                                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                                            startActivity(intent);
                                            getActivity().finish();
                                        }

                                        @Override
                                        public void methodCancel() {

                                        }
                                    }, "", error, "OK", "");
                            universalDialog.showAlert();
                        }
                        else{
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }

                    }

                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
    private void getEntries() {
        pieEntries = new ArrayList<>();
        for(int i =0;i <combinedModels.size();i++){
            pieEntries.add(new PieEntry(Float.parseFloat(combinedModels.get(i).getValue()),combinedModels.get(i).getCategoryName()));

        }
    }

}

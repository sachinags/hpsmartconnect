package com.agstransact.HP_SC.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.model.InterlockModel;

import java.util.ArrayList;

public class InterlockAdapter extends RecyclerView.Adapter<InterlockAdapter.MyInterlockModel> {
    private Context context;
    private ArrayList<InterlockModel> models = new ArrayList<>();

    public InterlockAdapter(Activity activity, ArrayList<InterlockModel> models) {
        this.context = activity;
        this.models = models;
    }


    @NonNull
    @Override
    public InterlockAdapter.MyInterlockModel onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_interlock_exception,parent,false);
        return new MyInterlockModel(view);
    }

    @Override
    public void onBindViewHolder(@NonNull InterlockAdapter.MyInterlockModel holder, int position) {

        InterlockModel model = models.get(position);

        holder.tv_il_value.setText(model.getInterlockLbl());
        holder.tv_status_value.setText(model.getInterlockValue());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    public class MyInterlockModel extends RecyclerView.ViewHolder {

        private TextView tv_il_value,tv_status_value;

        public MyInterlockModel(@NonNull View itemView) {
            super(itemView);

            tv_status_value = itemView.findViewById(R.id.tv_status_value);
            tv_il_value = itemView.findViewById(R.id.tv_il_value);
        }
    }
}

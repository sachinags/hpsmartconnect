package com.agstransact.HP_SC.login.fragment;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.HOSDashboardActivity;
import com.agstransact.HP_SC.dashboard.fragment.Change_New_Confirm_Mpin_new;
import com.agstransact.HP_SC.dashboard.fragment.Change_New_Mpin_new;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsKeyboardUtil;
import com.agstransact.HP_SC.utils.ClsWeakDataEncrption;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONException;
import org.json.JSONObject;

public class FragmentSetMPin extends Fragment implements  View.OnFocusChangeListener,
        View.OnKeyListener, TextWatcher,View.OnClickListener, AlertDialogInterface {
    View rootView;
    EditText edtPinForTxn1, edtPinForTxn2, edtPinForTxn3, edtPinForTxn4, mPinHiddenEditText;
    String strMPIN="",ISMPINSET ="";
    TextView tv_continue,tv_mpin_header;
    CheckedTextView chkdViewMPIN;
    //    PinEntryEditText mPinEntry;
    AlertDialogInterface dialogInterface;
    UniversalDialog universalDialog;
    int[] intMpin;
//    int i = 1;

    //    Bundle bundle;
    private AppCompatCheckBox Cb_Password;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fargment_set_mpin_new,container,false);
        seyUpWidgets();

        ClsKeyboardUtil objKeyBoardUtil;
        objKeyBoardUtil = new ClsKeyboardUtil(getActivity());
        objKeyBoardUtil.setupUI(rootView);
        tv_continue.setOnClickListener(this);

        return rootView;
    }
    private void seyUpWidgets() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            Window window = getActivity().getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }

        dialogInterface = this;
//        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        TextView toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
//        toolbarTV.setText(getResources().getString(R.string.set_mpin));
        tv_continue = (TextView) rootView.findViewById(R.id.tv_continue);
        tv_mpin_header = (TextView) rootView.findViewById(R.id.tv_mpin_header);
        tv_mpin_header.setText("SET Your MPIN");
        edtPinForTxn1 = rootView.findViewById(R.id.et1);
        edtPinForTxn2 = rootView.findViewById(R.id.et2);
        edtPinForTxn3 = rootView.findViewById(R.id.et3);
        edtPinForTxn4 = rootView.findViewById(R.id.et4);

        edtPinForTxn1.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn2.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn3.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn4.setTransformationMethod(new MyPasswordTransformationMethod());
        mPinHiddenEditText = (EditText) rootView.findViewById(R.id.mPinHiddenEditText);

        setPINListeners();

        chkdViewMPIN =  rootView.findViewById(R.id.chkdViewMPIN);
        Cb_Password =  rootView.findViewById(R.id.Cb_Password);
        Cb_Password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    // show password
                    edtPinForTxn1.setTransformationMethod(null);
                    edtPinForTxn2.setTransformationMethod(null);
                    edtPinForTxn3.setTransformationMethod(null);
                    edtPinForTxn4.setTransformationMethod(null);
                    Cb_Password.setBackground(getResources().getDrawable(R.drawable.eye_close));
                } else {
                    // hide password
                    edtPinForTxn1.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn2.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn3.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn4.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    Cb_Password.setBackground(getResources().getDrawable(R.drawable.eye));
                }
            }
        });
        chkdViewMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkdViewMPIN.isChecked()) {


                }else{

                }
            }
        });


    }


    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);
        edtPinForTxn1.setOnFocusChangeListener(this);
        edtPinForTxn2.setOnFocusChangeListener(this);
        edtPinForTxn3.setOnFocusChangeListener(this);
        edtPinForTxn4.setOnFocusChangeListener(this);

        edtPinForTxn1.setOnKeyListener(this);
        edtPinForTxn2.setOnKeyListener(this);
        edtPinForTxn3.setOnKeyListener(this);
        edtPinForTxn4.setOnKeyListener(this);
        mPinHiddenEditText.setOnKeyListener(this);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


        if (s.length() == 0) {
            edtPinForTxn1.setText("");
        } else if (s.length() == 1) {
            edtPinForTxn1.setText(s.charAt(0) + "");
            edtPinForTxn2.setText("");
            edtPinForTxn3.setText("");
            edtPinForTxn4.setText("");


        } else if (s.length() == 2) {
            edtPinForTxn2.setText(s.charAt(1) + "");
            edtPinForTxn3.setText("");
            edtPinForTxn4.setText("");


        } else if (s.length() == 3) {
//            setFocusedPinBackground(mPinForthDigitEditText);
            edtPinForTxn3.setText(s.charAt(2) + "");
            edtPinForTxn4.setText("");

        } else if (s.length() == 4) {
//            setFocusedPinBackground(mPinFifthDigitEditText);
            edtPinForTxn4.setText(s.charAt(3) + "");

            hideSoftKeyboard(edtPinForTxn4);


        }

    }

    @Override
    public void onClick(View view) {
        if (view == tv_continue) {
            if (strMPIN.length() == 4) {
                try{
                    if (ConstantDeclaration.gifProgressDialog != null) {
                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                            ConstantDeclaration.showGifProgressDialog(getActivity());
                        }
                    } else {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                    try {
                        int[] intArray = new int[4];
                        for (int i = 0; i < 4; i++) {
                            intArray[i] = Integer.parseInt(String.valueOf(strMPIN.charAt(i)));
                        }
                        intMpin = intArray;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                            intMpin = universalDialog.getIntFromString(mPinEntry.getText().toString().trim());
                    if( checkPinSequence()){
                        try {
                            if (ConstantDeclaration.gifProgressDialog.isShowing())
                                ConstantDeclaration.gifProgressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Bundle b = new Bundle();
                        Fragment frag = null;
                        frag = new FragmentConfirmMPin();
                        b.putString("OLDMPIN",strMPIN);
                        frag.setArguments(b);
                        clearPIN();
                        ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
                        changeFrag.addFragment(frag);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else{
                universalDialog = new UniversalDialog(getActivity(),
                        dialogInterface, "", "Please enter Valid MPIN", getString(R.string.got_it), "");
                universalDialog.showAlert();
            }
        }
    }



    @Override
    public void afterTextChanged(Editable s) {

        strMPIN = s.toString();
        if (strMPIN.length() == 4) {

//            checkValidPIN(strMPIN);
            if (strMPIN.length() == 4) {
                try{
                    if (ConstantDeclaration.gifProgressDialog != null) {
                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                            ConstantDeclaration.showGifProgressDialog(getActivity());
                        }
                    } else {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                    try {
//                            intMpin = universalDialog.getIntFromString(str.toString());

                        int[] intArray = new int[4];
                        for (int i = 0; i < 4; i++) {
                            intArray[i] = Integer.parseInt(String.valueOf(strMPIN.charAt(i)));
                        }
                        intMpin = intArray;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                            intMpin = universalDialog.getIntFromString(mPinEntry.getText().toString().trim());
                    if( checkPinSequence()){
                        try {
                            if (ConstantDeclaration.gifProgressDialog.isShowing())
                                ConstantDeclaration.gifProgressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Bundle b = new Bundle();
                        FragmentConfirmMPin frag = null;
                        frag = new FragmentConfirmMPin();
                        b.putString("OLDMPIN",strMPIN);
                        frag.setArguments(b);
                        clearPIN();
                        replaceScreen(frag);
//                        ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
//                        changeFrag.addFragment(frag);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else{
                universalDialog = new UniversalDialog(getActivity(),
                        dialogInterface, "", "Please enter Valid MPIN", getString(R.string.got_it), "");
                universalDialog.showAlert();
            }
        }


    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final int id = v.getId();
        switch (id) {
            case R.id.et1:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et2:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et3:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et4:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;


            default:
                break;

        }
    }


    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            final int id = v.getId();
            switch (id) {
                case R.id.mPinHiddenEditText:
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (mPinHiddenEditText.getText().length() == 4)
                            edtPinForTxn4.setText("");
                        else if (mPinHiddenEditText.getText().length() == 3)
                            edtPinForTxn3.setText("");
                        else if (mPinHiddenEditText.getText().length() == 2)
                            edtPinForTxn2.setText("");
                        else if (mPinHiddenEditText.getText().length() == 1)
                            edtPinForTxn1.setText("");

                        if (mPinHiddenEditText.length() > 0)
                            mPinHiddenEditText.setText(mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1));

                        return true;
                    }

                    break;

                default:
                    return false;
            }
        }

        return false;
    }

    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    public void methodDone() {

    }

    @Override
    public void methodCancel() {

    }
    public class MyPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    boolean checkPinSequence() {
        if (checkBackwardPinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else if (checkForwardPinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else if (checkSamePinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else {
            return true;
        }

    }
    public void clearPIN() {
        edtPinForTxn1.setText("");
        edtPinForTxn2.setText("");
        edtPinForTxn3.setText("");
        edtPinForTxn4.setText("");
        strMPIN = "";
    }
    public boolean checkForwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] + 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("ForwardPin", "false");
                return false;
            }
        }
        Log.e("ForwardPin", "true");
        return isSequence;
    }
    public boolean checkBackwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] - 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("BackwardPin", "false");
                return false;
            }
        }
        Log.e("BackwardPin", "true");
        return isSequence;
    }
    public boolean checkSamePinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length; i++) {
            for (int j = i + 1; j < mPin.length; j++) {
                if (mPin[i] == mPin[j]) {
                    isSequence = true;
                } else {
                    Log.e("SamePin", "false");
                    return false;
                }
            }
        }
        Log.e("SamePin", "true");
        return isSequence;
    }
    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, frag, "LAUNCH");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("LAUNCH");
        fragmentTransaction.commit();

    }

}

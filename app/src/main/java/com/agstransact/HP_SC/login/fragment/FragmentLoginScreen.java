package com.agstransact.HP_SC.login.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.HOSDashboardActivity;
import com.agstransact.HP_SC.dashboard.HOSDashboardActivity_new;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWeakDataEncrption;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.UniversalDialog;
import com.google.android.material.textfield.TextInputLayout;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Satish Patel on 25,March,2021
 */
public class FragmentLoginScreen extends Fragment implements View.OnClickListener {

    private EditText mEditUserName, mEditPassword;
    private TextView mTxtSubmit,tv_version_name,tv_help;
    TextInputLayout mMobileNoTIL, mPassTIL;
    private View rootView;
    String IMEI = "";
    String SimSerialNo = "";
    AlertDialogInterface alertDialogInterface;
    UniversalDialog universalDialog;
    String strRole = "",error = "";
    String ResponseCode,ResponseMessage,UserID,FullName,UserName,UserRole,Designation,
            Location,Address,State,Gender,EmailId,Vendor,UserType,Active,Token,ISMPINSET,Area,Zone,
            Region,UserCustomRole;
    ImageView imageViewTop;
    Bundle bundle;
//    private Boolean isLoggedIn = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_login_screen, container, false);

        setUpViews();
        bundle =getArguments();
        return rootView;
    }


    @Override
    public void onResume() {
        super.onResume();
        try {
            ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setUpViews() {

        mEditUserName = (EditText) rootView.findViewById(R.id.edt_user_id);
        mEditPassword = (EditText) rootView.findViewById(R.id.edt_password);
        mTxtSubmit = (TextView) rootView.findViewById(R.id.tv_submit);
        tv_help = (TextView) rootView.findViewById(R.id.tv_help);
        tv_version_name = (TextView) rootView.findViewById(R.id.tv_version_name);
        imageViewTop = (ImageView) rootView.findViewById(R.id.imageViewTop);
        mMobileNoTIL = (TextInputLayout) rootView.findViewById(R.id.textfieldUserID);
        mPassTIL = (TextInputLayout) rootView.findViewById(R.id.textfieldPassword);
        String version = "";
        int verCode = 0;
        PackageInfo pInfo = null;
        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        version = pInfo.versionName;
        verCode = pInfo.versionCode;

        tv_version_name.setText("Ver: " +version);

        mTxtSubmit.setOnClickListener(this);
        tv_help.setOnClickListener(this);
        final String finalVersion = version;
        imageViewTop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                Toast.makeText(getContext(),"Ver: "+ Version,Toast.LENGTH_LONG);
            }
        });
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    private boolean validatePass() {
        if (mEditPassword.getText().toString().trim().isEmpty()) {
            mPassTIL.setError(getString(R.string.error_pass));
            requestFocus(mEditPassword);
            return false;
        } else {
            mPassTIL.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateMobileNo() {
        if (mEditUserName.getText().toString().trim().isEmpty()) {
            mMobileNoTIL.setError(getString(R.string.error_mobile_no));
            requestFocus(mEditUserName);
            return false;
        } else {
            mMobileNoTIL.setErrorEnabled(false);
        }

        return true;
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.tv_submit:

                if (!validateMobileNo()) {
                    return;
                } else if (!validatePass()) {
                    return;
                } else if (IMEI == null || SimSerialNo == null) {


                    if (ConstantDeclaration.IS_WITHOUT_SIM_ACCESS) {
                        UserDataPrefrence.savePreference(getActivity().getString(R.string.prefrence_name),
                                getActivity(), ConstantDeclaration.SIMSERIALNOPREFKEY, IMEI);
                        SimSerialNo = IMEI;
                    }else{
                        universalDialog = new UniversalDialog(getActivity(),
                                alertDialogInterface, "", getString(R.string.need_sim_card), getString(R.string.got_it), "");
                        universalDialog.showAlert();
                        return;
                    }
                }

                login();
//                isLoggedIn = true;
               /* Intent intent = new Intent(getActivity(), HOSDashboardActivity.class);
                intent.putExtra("loggged in",true);
                startActivity(intent);*/
                break;

            case R.id.tv_help:
                replaceScreen(new Fragment_help());
                break;
        }
    }

    private void login() {
        try {
            JSONObject json = new JSONObject();
            json.put("UserName", mEditUserName.getText().toString().trim());
//            json.put("password",mEditPassword.getText().toString().trim());
            json.put("password", ClsWeakDataEncrption.encryptData(getActivity(), mEditPassword.getText().toString().trim()));
//            byte[] password = ClsWeakDataEncrption.encryptData(getActivity(), mPassET.getText().toString().trim());
//            String decryptedpassword = ClsWeakDataEncrption.DecryptData(getActivity(), password);
            //            json.put("Password", mPassET.getText().toString().trim());

//            JSONObject jsondata = ConstantDeclaration.getJsonRequesthpcl(mPassET.getText().toString().trim());
//            String password1 = String.valueOf(ConstantDeclaration.getJsonRequesthpcl(mPassET.getText().toString().trim()));
//            String password1 = String.valueOf(ConstantDeclaration.RSAEncrypt(mPassET.getText().toString().trim()));
//            json.put("password", password1);

            new AsyncLogin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class AsyncLogin extends AsyncTask<JSONObject,Void,String>{

        String strTimeStamp = "";
        private ProgressDialog dialog;
        private String MESSAGE;

        public AsyncLogin() {
        }

        public AsyncLogin(String strTime) {
            strTimeStamp = strTime;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Login/Login", false, jsonObjects[0],"1");
        }

        @RequiresApi(api = Build.VERSION_CODES.KITKAT)
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }

            if (s != null) {

                if (s != null) {
                    JSONObject jsonObj = null;
                    if (s.contains("||")) {
                        String[] str = (s.split("\\|\\|"));
                        String respCode = str[0];
                        if (respCode.equalsIgnoreCase("00")) {
                            SharedPreferences settings = getContext().getSharedPreferences("HPCL_Preference", Context.MODE_PRIVATE);
                            settings.edit().clear().commit();

                            try {
                                Bundle b = new Bundle();
                                JSONObject jsonObject = new JSONObject(str[2]);
                                JSONObject jsonObject1  = new JSONObject(jsonObject.getString("Data"));
                                String ResponseCode =  jsonObject1.getString("ResponseCode");
                                String ResponseMessage =  jsonObject1.getString("ResponseMessage");
                                JSONArray arrayLstROCode = new JSONArray(jsonObject1.getString("LstRoCode"));
//                            JSONArray arrayLstROCode = jsonObject1.getJSONArray("LstROCode");
                                List<String> ROlist1 = new ArrayList<String>();
                                List<String> ROCode1 = new ArrayList<String>();
                                String roCode = jsonObject1.getString("ROCount");
                                String FullName = jsonObject1.getString("FullName");
                                UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
                                        "ROCount",roCode);
                                UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
                                        "SalectedROCount",roCode);
                                UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
                                        "SalectedROSalesArea",roCode);
//                            ROlist1.add("ALL");
//                            for (int i1=0; i1< jsonObject1.getJSONArray("LstROCode").length(); i1++) {
//                                ROlist1.add( jsonObject1.getJSONArray("LstROCode").getString(i1));
//                            }

                                for (int i1=0; i1<arrayLstROCode.length(); i1++) {
//                                ROlist1.add(arrayLstROCode.getString(i1));
                                    if (arrayLstROCode.getJSONObject(i1).has("Key")){
                                        ROlist1.add(arrayLstROCode.getJSONObject(i1).getString("Key"));
                                    }

                                }
                                for (int i1=0; i1<arrayLstROCode.length(); i1++) {
                                    if (arrayLstROCode.getJSONObject(i1).has("Value")){
                                        ROCode1.add(arrayLstROCode.getJSONObject(i1).getString("Value"));
                                    }

//                                    ROCode1.add(arrayLstROCode.getJSONObject(i1).getString("ROCode"));
                                }
                                UserDataPrefrence.saveArrayList( ROlist1,getContext(),"ROLIst");
                                UserDataPrefrence.saveArrayList( ROCode1,getContext(),"ROCode");

                                try {
                                    try {
                                        UserID =  jsonObject1.getString("UserID");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        FullName =  jsonObject1.getString("FullName");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    UserName = mEditUserName.getText().toString().trim();
//                                     UserName =  jsonObject1.getString("UserName");

                                    try {
                                        UserRole =  jsonObject1.getString("UserRole");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Designation =  jsonObject1.getString("Designation");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Location =  jsonObject1.getString("Location");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Address =  jsonObject1.getString("Address");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        State =  jsonObject1.getString("State");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Gender =  jsonObject1.getString("Gender");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        EmailId =  jsonObject1.getString("EmailId");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Vendor =  jsonObject1.getString("Vendor");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        UserType =  jsonObject1.getString("UserType");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Active =  jsonObject1.getString("Active");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Token =  jsonObject1.getString("Token");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Area =  jsonObject1.getString("Area");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Zone =  jsonObject1.getString("Zone");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        Region =  jsonObject1.getString("Region");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        UserCustomRole =  jsonObject1.getString("UserCustomRole");
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        if(UserCustomRole.equalsIgnoreCase("DEALER")){
                                            try {
                                                UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                                        "ROKey", arrayLstROCode.getJSONObject(0).getString("Key"));
                                                UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                                        "ROValue", arrayLstROCode.getJSONObject(0).getString("Value"));
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }
                                        else{
                                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                    "FilteredRO", "");
                                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                    "FirstFilteredRO", "ALL");
                                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                    "FilteredROName", "");
                                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                    "Request_Selectd", "");
                                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                    "Request_Selectd_Name", "");
                                            UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                    "Request_Field", "");
                                            try {
                                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                        "Zone", Zone);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            try {
                                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                        "Region", Region);
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            UserDataPrefrence.saveArrayList( ROlist1,getContext(),"ROLIst");
                                            UserDataPrefrence.saveArrayList( ROCode1,getContext(),"ROCode");

                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    try {
                                        try {
                                           /* if(bundle.getString("fragment").equalsIgnoreCase("forgotmpin")){
                                                ISMPINSET =  "false";
                                            }else{
                                                ISMPINSET =  jsonObject1.getString("ISMPINSET");
                                            }*/
                                            ISMPINSET =  jsonObject1.getString("ISMPINSET");
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        try {
                                            ISMPINSET =  jsonObject1.getString("ISMPINSET");
                                        } catch (JSONException jsonException) {
                                            jsonException.printStackTrace();
                                        }

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                        "FullName", FullName);
                                UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                        "Area", Area);
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "UserName1", UserName);
                                UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                        "HPCLTOKEN", Token);
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "Password", mEditPassword.getText().toString().trim());
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "UserRole", UserRole);
                                UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                        "UserCustomRole", UserCustomRole);

                                if(ISMPINSET.equalsIgnoreCase("false")){
                                    Fragment fragment = new FragmentSetMPin();
                                    b.putString("LoginResponse",jsonObject1.toString());
                                    b.putString("Password",mEditPassword.getText().toString().trim());
                                    b.putString(ConstantDeclaration.USERROLEPREFKEY, "C");
                                    fragment.setArguments(b);
                                    replaceScreen(fragment);
//                                    ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
//                                    changeFrag.addFragment(fragment);
                                }else{
                                    Intent intent = new Intent(getActivity(), HOSDashboardActivity.class);
//                                    Intent intent = new Intent(getActivity(), HOSDashboardActivity_new.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    try {
                                        intent.putExtra(ConstantDeclaration.USERROLEPREFKEY, "C");
                                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                                "MPINSET", ISMPINSET);
                                        intent.putExtra("LoginResponse",jsonObject1.toString());
                                        intent.putExtra("Password",mEditPassword.getText().toString().trim());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                    startActivity(intent);
                                    getActivity().finish();
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            try {
                                if (ConstantDeclaration.gifProgressDialog.isShowing())
                                    ConstantDeclaration.gifProgressDialog.dismiss();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            error = str[1];
                            universalDialog = new UniversalDialog(getActivity(),
                                    alertDialogInterface, "", error, getString(R.string.got_it), "");
                            universalDialog.showAlert();
                        }
                    }

                }
                else {

                    try {
                        if (ConstantDeclaration.gifProgressDialog.isShowing())
                            ConstantDeclaration.gifProgressDialog.dismiss();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }                MESSAGE = "Request timed out \n please try again.";
                    universalDialog = new UniversalDialog(getActivity(),
                            alertDialogInterface, "", MESSAGE, getString(R.string.got_it), "");
                    universalDialog.showAlert();
                }
            }
        }
    }


    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, frag, "LOGIN_USERID");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("LOGIN_USERID");
        fragmentTransaction.commit();

    }
}

package com.agstransact.HP_SC.login.fragment;

import android.app.Activity;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.HOSDashboardActivity;
import com.agstransact.HP_SC.dashboard.fragment.Change_Mpin_new;
import com.agstransact.HP_SC.dashboard.fragment.Change_New_Mpin_new;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.splash.ActivityLaunchScreen;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ClsWeakDataEncrption;
import com.agstransact.HP_SC.utils.ClsWebService_hpcl;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.UniversalDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class FragmentLoginWithMPin extends Fragment  implements  View.OnFocusChangeListener,
        View.OnKeyListener, TextWatcher,View.OnClickListener, AlertDialogInterface {

    private View rootView;
    private EditText mEditMPin1,mEditMPin2,mEditMPin3,mEditMPin4;
    private TextView mTextLoginUserID,mTextForgotPin,mTextNeedHelp;
    private ImageView mIvShowPassword;
    private String mStrPin1, mStrPin2,mStrPin3,mStrPin4,mStrLoginPin;
    private AlertDialogInterface dialogInterface;
    private UniversalDialog universalDialog;
    private String ResponseCode="",ResponseMessage="",UserID="",FullName="",UserName="",UserRole="",Designation="",
            Location="",Address="",State="",Gender="",EmailId="",Vendor="",UserType="",Active="",Token="",Area="",
            Zone="",Region="",UserCustomRole="",ListPara="",ISMPINSET="";
    private int[] intMpin;
    LinearLayout llOtpView;
    EditText edtPinForTxn1, edtPinForTxn2, edtPinForTxn3, edtPinForTxn4, mPinHiddenEditText;
    CheckedTextView chkdViewMPIN;
    //    PinEntryEditText mPinEntry;
    Bundle bundle;
    JSONObject jsonObject;
    Button btn_send;
    LinearLayout rootLL;
    TextView tv_enter_mpin;
    private AppCompatCheckBox Cb_Password;
    String strMPIN="";



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_login_with_mpin,container,false);

        setUpViews();
        return rootView;
    }

    private void setUpViews() {
        mEditMPin1 = rootView.findViewById(R.id.edt_login_mpin1);
        mEditMPin2 = rootView.findViewById(R.id.edt_login_mpin2);
        mEditMPin3 = rootView.findViewById(R.id.edt_login_mpin3);
        mEditMPin4 = rootView.findViewById(R.id.edt_login_mpin4);

        edtPinForTxn1 = rootView.findViewById(R.id.et1);
        edtPinForTxn2 = rootView.findViewById(R.id.et2);
        edtPinForTxn3 = rootView.findViewById(R.id.et3);
        edtPinForTxn4 = rootView.findViewById(R.id.et4);

        edtPinForTxn1.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn2.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn3.setTransformationMethod(new MyPasswordTransformationMethod());
        edtPinForTxn4.setTransformationMethod(new MyPasswordTransformationMethod());
        mPinHiddenEditText = (EditText) rootView.findViewById(R.id.mPinHiddenEditText);
        setPINListeners();

        chkdViewMPIN =  rootView.findViewById(R.id.chkdViewMPIN);
        Cb_Password =  rootView.findViewById(R.id.Cb_Password);
        Cb_Password.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    // show password
                    edtPinForTxn1.setTransformationMethod(null);
                    edtPinForTxn2.setTransformationMethod(null);
                    edtPinForTxn3.setTransformationMethod(null);
                    edtPinForTxn4.setTransformationMethod(null);
                    Cb_Password.setBackground(getResources().getDrawable(R.drawable.eye_close));
                } else {
                    // hide password
                    edtPinForTxn1.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn2.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn3.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    edtPinForTxn4.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    Cb_Password.setBackground(getResources().getDrawable(R.drawable.eye));
                }
            }
        });
        chkdViewMPIN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (chkdViewMPIN.isChecked()) {


                }else{

                }
            }
        });
        mTextLoginUserID = rootView.findViewById(R.id.tv_login_with_userid);
        mTextForgotPin = rootView.findViewById(R.id.tv_forget_mpin);
        mTextNeedHelp = rootView.findViewById(R.id.tv_need_help);
        mTextLoginUserID.setOnClickListener(this);
        mTextForgotPin.setOnClickListener(this);
        mTextNeedHelp.setOnClickListener(this);
//        mEditMPin1.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (s.length() <= 0){
//                    requestFocus(mEditMPin1);
////                    mEditMPin1.setText("");
////                    mEditMPin1.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
//                }else {
//                    mEditMPin1.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape_solid));
//                    mStrPin1 = mEditMPin1.getText().toString();
//                    requestFocus(mEditMPin2);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        mEditMPin2.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (s.length() <= 0){
//                    requestFocus(mEditMPin2);
////                    mEditMPin2.setText("");
////                    mEditMPin2.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
//                }else {
//                    mEditMPin2.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape_solid));
//                    mStrPin2 = mEditMPin2.getText().toString();
//                    requestFocus(mEditMPin3);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        mEditMPin3.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (s.length() <= 0){
//                    requestFocus(mEditMPin3);
////                    mEditMPin3.setText("");
////                    mEditMPin3.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
//                }else {
//                    mEditMPin3.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape_solid));
//                    mStrPin3 = mEditMPin3.getText().toString();
//                    requestFocus(mEditMPin4);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
//
//        mEditMPin4.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//                if (s.length() <= 0){
//                    requestFocus(mEditMPin4);
////                    mEditMPin4.setText("");
////                    mEditMPin4.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
//                }else {
//                    mEditMPin4.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape_solid));
//                    mStrPin4 = mEditMPin4.getText().toString();
////                    requestFocus(mEditMPin4);
//                    mStrLoginPin = mStrPin1+mStrPin2+mStrPin3+mStrPin4;
//                    checkValidPIN(mStrLoginPin);
//                }
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//            }
//        });
    }

    private void checkValidPIN(String mStrLoginPin) {

        hideSoftKeyboard();

        JSONObject json = null;
        try {
            json = new JSONObject();
            json.put("userName", UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                    "UserName1",""));
            json.put("versionCode", "1");
            json.put("regidgcm", "3ndmdffkdkfdfm");
            json.put("guid", "dfdmfkdkmfkakfkd");
            json.put("tdtsm", "jdkjdk3kdmkd");
            json.put("machineIP", "12.23.234.22");
            json.put("latitude", "837389387383");
            json.put("longitude", "12933933343");
            json.put("deviceType", "Test");
            json.put("deviceMake", "kkmd");
            json.put("deviceModel", "Oppo");
            json.put("osVersion", "5");
//            json.put("mpin", ClsWeakDataEncrption.encryptData(getActivity(), mStrLoginPin));
            json.put("mpin", ClsWeakDataEncrption.encryptData(getActivity(), strMPIN));
        } catch (Exception e) {
            e.printStackTrace();
        }
        new AsyncCallVerifyPin().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }

    public void hideSoftKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
    }
    private void setPINListeners() {
        mPinHiddenEditText.addTextChangedListener(this);
        edtPinForTxn1.setOnFocusChangeListener(this);
        edtPinForTxn2.setOnFocusChangeListener(this);
        edtPinForTxn3.setOnFocusChangeListener(this);
        edtPinForTxn4.setOnFocusChangeListener(this);

        edtPinForTxn1.setOnKeyListener(this);
        edtPinForTxn2.setOnKeyListener(this);
        edtPinForTxn3.setOnKeyListener(this);
        edtPinForTxn4.setOnKeyListener(this);
        mPinHiddenEditText.setOnKeyListener(this);
    }
    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    public static void setFocus(EditText editText) {
        if (editText == null)
            return;

        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            final int id = v.getId();
            switch (id) {
                case R.id.mPinHiddenEditText:
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        if (mPinHiddenEditText.getText().length() == 4)
                            edtPinForTxn4.setText("");
                        else if (mPinHiddenEditText.getText().length() == 3)
                            edtPinForTxn3.setText("");
                        else if (mPinHiddenEditText.getText().length() == 2)
                            edtPinForTxn2.setText("");
                        else if (mPinHiddenEditText.getText().length() == 1)
                            edtPinForTxn1.setText("");

                        if (mPinHiddenEditText.length() > 0)
                            mPinHiddenEditText.setText(mPinHiddenEditText.getText().subSequence(0, mPinHiddenEditText.length() - 1));

                        return true;
                    }

                    break;

                default:
                    return false;
            }
        }

        return false;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


        if (s.length() == 0) {
            edtPinForTxn1.setText("");
        } else if (s.length() == 1) {
            edtPinForTxn1.setText(s.charAt(0) + "");
            edtPinForTxn2.setText("");
            edtPinForTxn3.setText("");
            edtPinForTxn4.setText("");


        } else if (s.length() == 2) {
            edtPinForTxn2.setText(s.charAt(1) + "");
            edtPinForTxn3.setText("");
            edtPinForTxn4.setText("");


        } else if (s.length() == 3) {
//            setFocusedPinBackground(mPinForthDigitEditText);
            edtPinForTxn3.setText(s.charAt(2) + "");
            edtPinForTxn4.setText("");

        } else if (s.length() == 4) {
//            setFocusedPinBackground(mPinFifthDigitEditText);
            edtPinForTxn4.setText(s.charAt(3) + "");

            hideSoftKeyboard(edtPinForTxn4);


        }

    }

    public void hideSoftKeyboard(EditText editText) {
        if (editText == null)
            return;

        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
    }

    @Override
    public void afterTextChanged(Editable s) {

        strMPIN = s.toString();
        if (strMPIN.length() == 4) {

//            checkValidPIN(strMPIN);
            if (strMPIN.length() == 4) {
                try{
                    if (ConstantDeclaration.gifProgressDialog != null) {
                        if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                            ConstantDeclaration.showGifProgressDialog(getActivity());
                        }
                    } else {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                    try {
//                            intMpin = universalDialog.getIntFromString(str.toString());

                        int[] intArray = new int[4];
                        for (int i = 0; i < 4; i++) {
                            intArray[i] = Integer.parseInt(String.valueOf(strMPIN.charAt(i)));
                        }
                        intMpin = intArray;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                            intMpin = universalDialog.getIntFromString(mPinEntry.getText().toString().trim());
                    if( checkPinSequence()){
                        try {
                            if (ConstantDeclaration.gifProgressDialog.isShowing())
                                ConstantDeclaration.gifProgressDialog.dismiss();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
//                        Bundle b = new Bundle();
//                        Change_New_Mpin_new frag = null;
//                        frag = new Change_New_Mpin_new();
//                        b.putString("OLDMPIN",strMPIN);
//                        frag.setArguments(b);
//                        clearPIN();
//                        ActionChangeFrag changeFrag = (ActionChangeFrag) getActivity();
//                        changeFrag.addFragment(frag);
                        checkValidPIN(strMPIN);

                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else{
                universalDialog = new UniversalDialog(getActivity(),
                        dialogInterface, "", "Please enter Valid MPIN", getString(R.string.got_it), "");
                universalDialog.showAlert();
            }
        }
//        if (strMPIN.length() == 4) {
//
//            btn_send.setEnabled(true);
//            btn_send.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.rounded_corner_button));
//        } else {
//            btn_send.setEnabled(false);
//            btn_send.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.disable_btn_bg));
//        }

    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        final int id = v.getId();
        switch (id) {
            case R.id.et1:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et2:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et3:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;

            case R.id.et4:
                if (hasFocus) {
                    setFocus(mPinHiddenEditText);
                    showSoftKeyboard(mPinHiddenEditText);
                }
                break;


            default:
                break;

        }
    }



    @Override
    public void methodDone() {

    }

    @Override
    public void methodCancel() {

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_login_with_userid:
                replaceScreen(new FragmentLoginScreen());
                break;

            case R.id.tv_forget_mpin:
                replaceScreen(new FragmentSetMPin());
                break;

            case R.id.tv_need_help:
                replaceScreen(new Fragment_help());
                break;

        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public void replaceScreen(Fragment frag) {

//        frag.setArguments(b);
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager
                .beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, frag, "LOGIN_USERID");
        fragmentTransaction
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragmentTransaction.addToBackStack("LOGIN_USERID");
        fragmentTransaction.commit();

    }

    public class AsyncCallVerifyPin extends AsyncTask<JSONObject, Void, String> {

        String strTimeStamp;

        public AsyncCallVerifyPin(String strTime) {
            strTimeStamp = strTime;
        }
        public AsyncCallVerifyPin() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {

//                mPinEntry.setText("");
                mEditMPin1.setText("");
                mEditMPin2.setText("");
                mEditMPin3.setText("");
                mEditMPin4.setText("");
                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(getActivity());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(getActivity());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Login/LoginWithMPIN", false, jsonObjects[0],"1");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (!isAdded()) {
                return;
            }
            if (isDetached()) {
                return;
            }

            /*if (s.contains("||")) {
                String[] str = (s.split("\\|\\|"));
                if (str[0].equalsIgnoreCase("00")
                        ||str[0].equalsIgnoreCase("0")) {
                    JSONObject jsonObject1  = null;
                    try {
                        JSONObject jsonObject = new JSONObject(str[2]);
                        jsonObject1 = new JSONObject(jsonObject.getString("Data"));
                        String ResponseCode =  jsonObject1.getString("ResponseCode");
                        String ResponseMessage =  jsonObject1.getString("ResponseMessage");
                        String roCode = jsonObject1.getString("ROCount");
                        FullName = jsonObject1.getString("FullName");
                        UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
                                "ROCount",roCode);


                        JSONArray arrayLstROCode = new JSONArray(jsonObject1.getString("Key"));
                        JSONArray arrayLstROName = new JSONArray(jsonObject1.getString("Value"));
                        List<String> ROlist1 = new ArrayList<String>();
                        List<String> ROCode1 = new ArrayList<String>();

                        for (int i1=0; i1<arrayLstROCode.length(); i1++) {
//                                ROlist1.add(arrayLstROCode.getString(i1));
                            ROlist1.add(arrayLstROCode.getJSONObject(i1).getString("ROName"));
                        }
                        for (int i1=0; i1<arrayLstROCode.length(); i1++) {
                            ROCode1.add(arrayLstROCode.getJSONObject(i1).getString("ROCode"));
                        }
                        UserDataPrefrence.saveArrayList( ROlist1,getContext(),"ROLIst");
                        UserDataPrefrence.saveArrayList( ROCode1,getContext(),"ROCode");

//                        UserDataPrefrence.saveArrayList( ROlist1,getContext(),"ROLIst");
                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRO", "");
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "ROLIst", "");
                        UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
                              "roCount",roCode);
                        UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
                                "roCount",FullName);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        try {
                            UserID =  jsonObject1.getString("UserID");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            FullName =  jsonObject1.getString("FullName");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            UserName =  jsonObject1.getString("UserName");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            UserRole =  jsonObject1.getString("UserRole");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Designation =  jsonObject1.getString("Designation");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Location =  jsonObject1.getString("Location");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Address =  jsonObject1.getString("Address");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            State =  jsonObject1.getString("State");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Gender =  jsonObject1.getString("Gender");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            EmailId =  jsonObject1.getString("EmailId");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Vendor =  jsonObject1.getString("Vendor");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            UserType =  jsonObject1.getString("UserType");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Active =  jsonObject1.getString("Active");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            Token =  jsonObject1.getString("Token");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            ISMPINSET =  jsonObject1.getString("ISMPINSET");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "UserName", UserName);
                        UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                "HPCLTOKEN", Token);
                        UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                "FullName", FullName);
                        Intent intent = new Intent(getActivity(), HOSDashboardActivity.class);
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "MPINSET", ISMPINSET);
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "UserRole", UserRole);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        try {
                            intent.putExtra(ConstantDeclaration.USERROLEPREFKEY, "C");
                            intent.putExtra("LoginResponse",jsonObject1.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(intent);
                        getActivity().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (str[0].equalsIgnoreCase("907")) {
                    mEditMPin1.setText("");
                    mEditMPin2.setText("");
                    mEditMPin3.setText("");
                    mEditMPin4.setText("");
                    universalDialog = new UniversalDialog(getActivity(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {
                            Intent intentLogOut = new Intent(getActivity(), ActivityLaunchScreen.class);
                            intentLogOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentLogOut);
                            getActivity().finish();
                        }

                        @Override
                        public void methodCancel() {

                        }
                    }, ""
                            , str[1], getString(R.string.dialog_ok), "");
                    universalDialog.setCancelable(false);
                    universalDialog.showAlert();
                    return;
                } else {
                    //18-1-19
                    UniversalDialog universalDialog = new UniversalDialog(getActivity(), null, ""
                            , str[1], getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
//                    mPinEntry.setText("");
                    mEditMPin1.setText("");
                    mEditMPin2.setText("");
                    mEditMPin3.setText("");
                    mEditMPin4.setText("");
                    mEditMPin1.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin2.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin3.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin4.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin1.requestFocus();
//                    Toast.makeText(getActivity(), str[1], Toast.LENGTH_SHORT).show();
                }
            }*/

            if (s.contains("||")) {
                String[] str = (s.split("\\|\\|"));
                if (str[0].equalsIgnoreCase("00")
                        ||str[0].equalsIgnoreCase("0")) {
                    JSONObject jsonObject1  = null;
                    try {
                        JSONObject jsonObject = new JSONObject(str[2]);
                        jsonObject1 = new JSONObject(jsonObject.getString("Data"));
                        String ResponseCode =  jsonObject1.getString("ResponseCode");
                        String ResponseMessage =  jsonObject1.getString("ResponseMessage");
                        JSONArray arrayLstROCode = new JSONArray(jsonObject1.getString("LstRoCode"));
                        try {
                            JSONArray arrayLstROName = new JSONArray(jsonObject1.getString("LstROCode"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        List<String> ROlist1 = new ArrayList<String>();
                        List<String> ROCode1 = new ArrayList<String>();

                        for (int i1=0; i1<arrayLstROCode.length(); i1++) {
                                ROlist1.add(arrayLstROCode.getString(i1));
                            ROlist1.add(arrayLstROCode.getJSONObject(i1).getString("Key"));
                        }
                        for (int i1=0; i1<arrayLstROCode.length(); i1++) {
                            ROCode1.add(arrayLstROCode.getJSONObject(i1).getString("Value"));
                        }
                        UserDataPrefrence.saveArrayList( ROlist1,getContext(),"ROLIst");
                        UserDataPrefrence.saveArrayList( ROCode1,getContext(),"ROCode");
//                        UserDataPrefrence.saveArrayList( ROlist1,getContext(),"ROLIst");
                        UserDataPrefrence.getPreference("HPCL_Preference", getContext(),
                                "SelectdRO", "");
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "SalectedROCount", "");
                        try {
                            String roCode = jsonObject1.getString("ROCount");
                            UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
                                    "ROCount",roCode);
                            UserDataPrefrence.savePreference("HPCL_Preference",getContext(),
                                    "SalectedROCount",roCode);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        try {
                            try {
                                UserID =  jsonObject1.getString("UserID");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                FullName =  jsonObject1.getString("FullName");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                UserName =  jsonObject1.getString("UserName");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                UserRole =  jsonObject1.getString("UserRole");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Designation =  jsonObject1.getString("Designation");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Location =  jsonObject1.getString("Location");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Address =  jsonObject1.getString("Address");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                State =  jsonObject1.getString("State");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Gender =  jsonObject1.getString("Gender");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                EmailId =  jsonObject1.getString("EmailId");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Vendor =  jsonObject1.getString("Vendor");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                UserType =  jsonObject1.getString("UserType");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Active =  jsonObject1.getString("Active");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Token =  jsonObject1.getString("Token");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Area =  jsonObject1.getString("Area");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                Zone =  jsonObject1.getString("Zone");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                Region =  jsonObject1.getString("Region");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                UserCustomRole =  jsonObject1.getString("UserCustomRole");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                if(UserCustomRole.equalsIgnoreCase("DEALER")){
                                    try {
                                        UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                                "ROKey", arrayLstROCode.getJSONObject(0).getString("Key"));
                                        UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                                "ROValue", arrayLstROCode.getJSONObject(0).getString("Value"));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }else{
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "FilteredRO", "");
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "FirstFilteredRO", "ALL");
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "FilteredROName", "");
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "Selectdzone","");
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "SelectdRegion","");
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "SelectdSalesArea","");
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "Request_Selectd", "");
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "Request_Selectd_Name", "");
                                    UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                            "Request_Field", "");
                                    UserDataPrefrence.saveArrayList( ROlist1,getContext(),"ROLIst");
                                    UserDataPrefrence.saveArrayList( ROCode1,getContext(),"ROCode");

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                ListPara =  jsonObject1.getString("ListPara");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            try {
                                ISMPINSET =  jsonObject1.getString("ISMPINSET");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
//                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
//                                "UserName", UserName);
                        UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                "HPCLTOKEN", Token);
                        UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                "FullName", FullName);
                        UserDataPrefrence.savePreference("HPCL_Preference", getActivity(),
                                "Area", Area);
                        Intent intent = new Intent(getActivity(), HOSDashboardActivity.class);
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "MPINSET", ISMPINSET);
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "UserRole", UserRole);
                        UserDataPrefrence.savePreference("HPCL_Preference", getContext(),
                                "UserCustomRole", UserCustomRole);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        try {
                            intent.putExtra(ConstantDeclaration.USERROLEPREFKEY, "C");
                            intent.putExtra("LoginResponse",jsonObject1.toString());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        startActivity(intent);
                        getActivity().finish();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else if (str[0].equalsIgnoreCase("907")) {
//                    mPinEntry.setText("");
                    mEditMPin1.setText("");
                    mEditMPin2.setText("");
                    mEditMPin3.setText("");
                    mEditMPin4.setText("");
                    mEditMPin1.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin2.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin3.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin4.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin1.requestFocus();
                    universalDialog = new UniversalDialog(getActivity(), new AlertDialogInterface() {
                        @Override
                        public void methodDone() {
                            Intent intentLogOut = new Intent(getActivity(), ActivityLaunchScreen.class);
                            intentLogOut.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intentLogOut);
                            getActivity().finish();
                        }

                        @Override
                        public void methodCancel() {

                        }
                    }, ""
                            , str[1], getString(R.string.dialog_ok), "");
                    universalDialog.setCancelable(false);
                    universalDialog.showAlert();
                    return;
                } else {
                    //18-1-19
                    UniversalDialog universalDialog = new UniversalDialog(getActivity(), null, ""
                            , str[1], getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
//                    mPinEntry.setText("");
                    mEditMPin1.setText("");
                    mEditMPin2.setText("");
                    mEditMPin3.setText("");
                    mEditMPin4.setText("");
                    mEditMPin1.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin2.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin3.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin4.setBackground(getResources().getDrawable(R.drawable.bg_oval_shape));
                    mEditMPin1.requestFocus();
//                    Toast.makeText(getActivity(), str[1], Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    public class MyPasswordTransformationMethod extends PasswordTransformationMethod {
        @Override
        public CharSequence getTransformation(CharSequence source, View view) {
            return new MyPasswordTransformationMethod.PasswordCharSequence(source);
        }

        private class PasswordCharSequence implements CharSequence {
            private CharSequence mSource;

            public PasswordCharSequence(CharSequence source) {
                mSource = source; // Store char sequence
            }

            public char charAt(int index) {
                return '*'; // This is the important part
            }

            public int length() {
                return mSource.length(); // Return default
            }

            public CharSequence subSequence(int start, int end) {
                return mSource.subSequence(start, end); // Return default
            }
        }
    }

    boolean checkPinSequence() {
        if (checkBackwardPinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else if (checkForwardPinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else if (checkSamePinSequence(intMpin)) {
            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }
            clearPIN();
            universalDialog = new UniversalDialog(getActivity(),
                    dialogInterface, "", "Invalid PIN. PIN cannot be in sequence or have repeated number.", getString(R.string.got_it), "");
            universalDialog.showAlert();
            return false;
        } else {
            return true;
        }

    }
    public void clearPIN() {
        edtPinForTxn1.setText("");
        edtPinForTxn2.setText("");
        edtPinForTxn3.setText("");
        edtPinForTxn4.setText("");
        strMPIN = "";
    }
    public boolean checkForwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] + 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("ForwardPin", "false");
                return false;
            }
        }
        Log.e("ForwardPin", "true");
        return isSequence;
    }
    public boolean checkBackwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] - 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("BackwardPin", "false");
                return false;
            }
        }
        Log.e("BackwardPin", "true");
        return isSequence;
    }
    public boolean checkSamePinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length; i++) {
            for (int j = i + 1; j < mPin.length; j++) {
                if (mPin[i] == mPin[j]) {
                    isSequence = true;
                } else {
                    Log.e("SamePin", "false");
                    return false;
                }
            }
        }
        Log.e("SamePin", "true");
        return isSequence;
    }

}

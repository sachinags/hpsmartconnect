package com.agstransact.HP_SC.login;

import android.app.Activity;
import android.app.Application;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.PersistableBundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.HOSDashboardActivity;
import com.agstransact.HP_SC.login.fragment.FragmentLoginScreen;
import com.agstransact.HP_SC.login.fragment.FragmentLoginWithMPin;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
import com.agstransact.HP_SC.utils.AlertDialogInterface;
import com.agstransact.HP_SC.utils.ConstantDeclaration;
import com.agstransact.HP_SC.utils.NetworkConnectivityReceiver;
import com.agstransact.HP_SC.utils.UniversalDialog;

/**
 * Created by Satish Patel on 25,March,2021
 */
public class LoginActivity extends AppCompatActivity implements Application.ActivityLifecycleCallbacks, NetworkConnectivityReceiver.ConnectivityReceiverListener,
        ActionChangeFrag {

    Toolbar toolbar;
    ActionBarDrawerToggle mDrawerToggle;
    LinearLayout mRootLL;
    NetworkConnectivityReceiver networkConnectivityReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        PrashantG
//        show full screen
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
            getSupportActionBar().hide(); // hide the title bar
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.login_activity);

        //riteshb 13-12-17
        //riteshb 24-11
        MainApplication.mDashBoardContext = null;
        MainApplication.mLoginContext = this;
        MainApplication.context = this;
        //riteshb 13-12-17

        NetworkConnectivityReceiver.connectivityReceiverListener = this;
        networkConnectivityReceiver = new NetworkConnectivityReceiver();

        intializeID();

        mDrawerToggle = new ActionBarDrawerToggle(this, null, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerToggle.setDrawerIndicatorEnabled(false);

        if (savedInstanceState == null) {
//            mRootLL.setBackground(ContextCompat.getDrawable(this, R.drawable.hpcl_background));
            /*default fragmnet when open the activity*/
            FragmentManager fragmentManager = getSupportFragmentManager();
            if(UserDataPrefrence.getPreference("HPCL_Preference",
                    getApplicationContext(), "MPINSET", "ISMPINSET").equalsIgnoreCase("true")) {
                ConstantDeclaration.replaceFragment_login(new FragmentLoginWithMPin(), fragmentManager);
            }else{
                ConstantDeclaration.replaceFragment_login(new FragmentLoginScreen(), fragmentManager);

            }
//            ConstantDeclaration.replaceFragment(new LoginFragment(), fragmentManager);
            mDrawerToggle.setDrawerIndicatorEnabled(false);

        }
        getSupportFragmentManager().addOnBackStackChangedListener(onBackStackChangedListener);



    }

    private void intializeID() {
        try {

            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            // Remove default title text
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            // Display icon in the toolbar
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            mRootLL = (LinearLayout) findViewById(R.id.rootLL);


        } catch (Exception e) {
        }
    }
    /*this is used for back arrow clicked of toolbar*/
    private FragmentManager.OnBackStackChangedListener onBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
        @Override
        public void onBackStackChanged() {
            Fragment frag = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
            String fragname = null;
            if (frag != null) {
                fragname = frag.getClass().getSimpleName();
            }

            if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                getSupportActionBar().hide();
            } else if ((fragname != null && fragname.equalsIgnoreCase("NeedHelpNavigatorFragment")) ||
                    (fragname != null && fragname.equalsIgnoreCase("NHelpContactUsFragment")) ||
                    (fragname != null && fragname.equalsIgnoreCase("NHelpFAQFragment")) ||
                    (fragname != null && fragname.equalsIgnoreCase("NHelpForgotPswrdFragment"))) {
                mRootLL.setBackground(ContextCompat.getDrawable(LoginActivity.this, R.drawable.gradient_bg));
            } else if ((fragname != null
                    && fragname.equalsIgnoreCase("RegisterSuccesfulFragment")) || (fragname != null
                    && fragname.equalsIgnoreCase("AgentRegisterSuccessFragment"))) {
                getSupportActionBar().hide();
//                mRootLL.setBackground(ContextCompat.getDrawable(LoginActivity.this, R.drawable.darapay_bg));

            } else {
                getSupportActionBar().show();
            }

        }
    };
    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        Fragment frag = getSupportFragmentManager().findFragmentById(R.id.dashboard_container);
//        String fragname = null;
//        try {
//            fragname = frag.getClass().getSimpleName();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        if (fragname.equalsIgnoreCase("Fragment_help")) {
//            getSupportFragmentManager().popBackStack();
//        }else{
            if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
                getSupportFragmentManager().popBackStack();
            } else {
                finish();

            }
//        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return mDrawerToggle.isDrawerIndicatorEnabled() && mDrawerToggle.onOptionsItemSelected(item) || item.getItemId() == android.R.id.home && getSupportFragmentManager().popBackStackImmediate() || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {

    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            ConstantDeclaration.hideKeyboard(this);
            try {
                Fragment frag = getSupportFragmentManager().findFragmentById(R.id.frameLayout);
                if(frag !=null){
                    frag.onActivityResult(requestCode,resultCode,data);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle bundle) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {

    }

    @Override
    public void addFragment(Fragment fragment) {

    }

    @Override
    public void ReplaceFrag(Fragment fragment) {

    }

    @Override
    public void updateDrawer(boolean isShown) {

    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }



}

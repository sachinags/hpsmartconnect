package com.agstransact.HP_SC.Notification;


import android.os.AsyncTask;
import android.os.Bundle;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.utils.ClsKeyboardUtil;
import com.agstransact.HP_SC.utils.RecyclerItemClickListner;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



/**
 * Created by amit verma.
 */
public class NotificationFragment extends Fragment implements RecyclerItemClickListner {

    private ArrayList<NotificationListModel> notificationList;
    TextView toolbarTV, tvNoDataFound;
    ImageView toolbarIV;
    RecyclerView rv_notification;
    NotificationAdapter notificationAdapter;
    String TAG = "NotificationFragment";
    RecyclerItemClickListner itemClickListner;
    RelativeLayout rootRL;
    String strRole;

    public NotificationFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment4
        View view = inflater.inflate(R.layout.fragment_notification, container, false);
        intializeID(view);

        itemClickListner = this;


        ClsKeyboardUtil objKeyBoardUtil;
        objKeyBoardUtil = new ClsKeyboardUtil(getActivity());
        objKeyBoardUtil.setupUI(rootRL);

        getNotificationList();
        return view;
    }

    private void getNotificationList() {
        JSONObject json = null;
        try {
            json = new JSONObject();
//            json.put("FROMID", UserDataPrefrence.getPreference(getActivity().getString(R.string.prefrence_name),
//                    getActivity(), ConstantDeclaration.MOBILENOPREFKEY, ""));
//            Long tsLing = System.currentTimeMillis() / 1000;
//            json.put("TIMESTAMP", tsLing.toString());


//        Toast.makeText(activity, ""+strMob, Toast.LENGTH_SHORT).show();

//            JSONObject jsondata = ConstantDeclaration.getJsonRequest("GETGCMNOTIFICATION"
//                    , ANDROID_SOURCEID
//                    , UserDataPrefrence.getPreference(getActivity().getString(R.string.prefrence_name),
//                            getActivity(), ConstantDeclaration.USER_MOBILE, "")
//                    , json);

//            new AsyncCallNotification(tsLing.toString()).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, jsondata);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void intializeID(View view) {
        rootRL = (RelativeLayout) view.findViewById(R.id.rootRL);
        rv_notification = (RecyclerView) view.findViewById(R.id.rv_notification);
        tvNoDataFound = (TextView) view.findViewById(R.id.tvNoDataFound);
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbarTV = (TextView) toolbar.findViewById(R.id.toolbarTV);
        toolbarIV = (ImageView) toolbar.findViewById(R.id.toolbarIV);
        toolbarTV.setVisibility(View.VISIBLE);
        toolbarIV.setVisibility(View.GONE);
        toolbarTV.setAllCaps(true);
        toolbarTV.setText(getResources().getString(R.string.notification));


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        rv_notification.setLayoutManager(mLayoutManager);
        rv_notification.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    public void onItemClick(String adapterType, int position) {


    }

    @Override
    public void onItemClick(String label, String value, int position) {

    }

    @Override
    public void onItemClick(int position) {

    }

    @Override
    public void onItemLongClick(int position) {

    }

}

package com.agstransact.HP_SC.Notification;

/**
 * Created by amit.verma on 31-10-2017.
 */

public class NotificationListModel {
    private String fromID;
    private String fromMobile;
    private String fromName;
    private String notifiactionRefID;
    private String notificationMsg;
    private String remark;
    private String createdOn;
    private String IsViewed;
    private String IsDeleted;
    private String status;
    private String toNumber;
    private String notificationID;
    private String customerIDNo;
    private String customerMobileNo;
    private String logo;
    private String telcoID;
    private String receiverID;
    private String CollectionCode;
    private String mobileNo;
    private String telcoPin;
    private String TotalAmount;
    private String TxnMode;
    private String strBillNo = "", strCustomerNo = "",strCustomerName = "";
    private String strReversed = "", strTransactionStatus="";

    private String strPinTranSubType;
    private String PinAmount;
    private String SerialNo;
    private String ExpiryDate;
    private String DecryptPIN;
    private String PINCount;

    public String getPINCount() {
        return PINCount;
    }

    public void setPINCount(String PINCount) {
        this.PINCount = PINCount;
    }

    public String getStrPinTranSubType() {
        return strPinTranSubType;
    }

    public void setStrPinTranSubType(String strPinTranSubType) {
        this.strPinTranSubType = strPinTranSubType;
    }

    public String getPinAmount() {
        return PinAmount;
    }

    public void setPinAmount(String pinAmount) {
        PinAmount = pinAmount;
    }

    public String getSerialNo() {
        return SerialNo;
    }

    public void setSerialNo(String serialNo) {
        SerialNo = serialNo;
    }

    public String getExpiryDate() {
        return ExpiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        ExpiryDate = expiryDate;
    }

    public String getDecryptPIN() {
        return DecryptPIN;
    }

    public void setDecryptPIN(String decryptPIN) {
        DecryptPIN = decryptPIN;
    }

    public String getStrCustomerName() {
        return strCustomerName;
    }

    public String getStrTransactionStatus() {
        return strTransactionStatus;
    }

    public void setStrTransactionStatus(String strTransactionStatus) {
        this.strTransactionStatus = strTransactionStatus;
    }

    public String getStrReversed() {
        return strReversed;
    }

    public void setStrReversed(String strReversed) {
        this.strReversed = strReversed;
    }

    public void setStrCustomerName(String strCustomerName) {
        this.strCustomerName = strCustomerName;
    }

    public String getStrBillNo() {
        return strBillNo;
    }

    public void setStrBillNo(String strBillNo) {
        this.strBillNo = strBillNo;
    }

    public String getStrCustomerNo() {
        return strCustomerNo;
    }

    public void setStrCustomerNo(String strCustomerNo) {
        this.strCustomerNo = strCustomerNo;
    }

    public String getTxnMode() {
        return TxnMode;
    }

    public void setTxnMode(String txnMode) {
        TxnMode = txnMode;
    }

    public String getTotalAmount() {
        return TotalAmount;
    }

    public void setTotalAmount(String totalAmount) {
        TotalAmount = totalAmount;
    }

    //10-4-18
    String StaffMobile, StaffName, PaymentAmount, strReceiptNo, MerchantMobile;
    String DiscountAmount, PerDiscount, MerchantName;

    public String getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        DiscountAmount = discountAmount;
    }

    public String getPerDiscount() {
        return PerDiscount;
    }

    public void setPerDiscount(String perDiscount) {
        PerDiscount = perDiscount;
    }

    public String getMerchantName() {
        return MerchantName;
    }

    public void setMerchantName(String merchantName) {
        MerchantName = merchantName;
    }

    public String getMerchantMobile() {
        return MerchantMobile;
    }

    public void setMerchantMobile(String merchantMobile) {
        MerchantMobile = merchantMobile;
    }

    public String getStrReceiptNo() {
        return strReceiptNo;
    }

    public void setStrReceiptNo(String strReceiptNo) {
        this.strReceiptNo = strReceiptNo;
    }

    public String getStaffMobile() {
        return StaffMobile;
    }

    public void setStaffMobile(String staffMobile) {
        StaffMobile = staffMobile;
    }

    public String getStaffName() {
        return StaffName;
    }

    public void setStaffName(String staffName) {
        StaffName = staffName;
    }

    public String getPaymentAmount() {
        return PaymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        PaymentAmount = paymentAmount;
    }

    public String getTelcoPin() {
        return telcoPin;
    }

    public void setTelcoPin(String telcoPin) {
        this.telcoPin = telcoPin;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getIsViewed() {
        return IsViewed;
    }

    public void setIsViewed(String isViewed) {
        IsViewed = isViewed;
    }

    public String getIsDeleted() {
        return IsDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        IsDeleted = isDeleted;
    }

    public String getCollectionCode() {
        return CollectionCode;
    }

    public void setCollectionCode(String collectionCode) {
        CollectionCode = collectionCode;
    }

    public String getTelcoID() {
        return telcoID;
    }

    public void setTelcoID(String telcoID) {
        this.telcoID = telcoID;
    }

    public String getReceiverID() {
        return receiverID;
    }

    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    public String getCustomerIDNo() {
        return customerIDNo;
    }

    public void setCustomerIDNo(String customerIDNo) {
        this.customerIDNo = customerIDNo;
    }

    public String getCustomerMobileNo() {
        return customerMobileNo;
    }

    public void setCustomerMobileNo(String customerMobileNo) {
        this.customerMobileNo = customerMobileNo;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(String notificationID) {
        this.notificationID = notificationID;
    }

    public String getToNumber() {
        return toNumber;
    }

    public void setToNumber(String toNumber) {
        this.toNumber = toNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDeleted() {
        return IsDeleted;
    }

    public void setDeleted(String deleted) {
        IsDeleted = deleted;
    }

    public String getViewed() {
        return IsViewed;
    }

    public void setViewed(String viewed) {
        IsViewed = viewed;
    }

    private String RequestAmt;
    private String IsActionRequired;
    private String currencyType;

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public String getTranType() {
        return tranType;
    }

    public void setTranType(String tranType) {
        this.tranType = tranType;
    }

    private String tranType;
    private String customeAccName;

    public String getCustomeAccName() {
        return customeAccName;
    }

    public void setCustomeAccName(String customeAccName) {
        this.customeAccName = customeAccName;
    }

    public String getCustomeAccNumber() {
        return customeAccNumber;
    }

    public void setCustomeAccNumber(String customeAccNumber) {
        this.customeAccNumber = customeAccNumber;
    }

    private String customeAccNumber;

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    private String merchantType;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    private String bankName;

    public String getFromID() {
        return fromID;
    }

    public void setFromID(String fromID) {
        this.fromID = fromID;
    }

    public String getFromMobile() {
        return fromMobile;
    }

    public void setFromMobile(String fromMobile) {
        this.fromMobile = fromMobile;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getNotifiactionRefID() {
        return notifiactionRefID;
    }

    public void setNotifiactionRefID(String notifiactionRefID) {
        this.notifiactionRefID = notifiactionRefID;
    }

    public String getNotificationMsg() {
        return notificationMsg;
    }

    public void setNotificationMsg(String notificationMsg) {
        this.notificationMsg = notificationMsg;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }


    public String getRequestAmt() {
        return RequestAmt;
    }

    public void setRequestAmt(String requestAmt) {
        RequestAmt = requestAmt;
    }

    public String getIsActionRequired() {
        return IsActionRequired;
    }

    public void setIsActionRequired(String isActionRequired) {
        IsActionRequired = isActionRequired;
    }
}

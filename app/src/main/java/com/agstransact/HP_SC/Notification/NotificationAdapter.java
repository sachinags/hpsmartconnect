package com.agstransact.HP_SC.Notification;

import android.content.Context;
import android.graphics.Typeface;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.utils.RecyclerItemClickListner;


import java.util.ArrayList;

/**
 * Created by amit.verma on 31-10-2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolderNotification> {

    ArrayList<NotificationListModel> mList;
    private Context context;
    private int lastPosition = -1;
    private static RecyclerItemClickListner itemClickListener;

    public NotificationAdapter() {

    }

    public NotificationAdapter(Context context, ArrayList<NotificationListModel> pList, RecyclerItemClickListner itemClickListener) {
        this.mList = pList;
        this.context = context;
        this.itemClickListener = itemClickListener;
    }


    @Override
    public void onViewDetachedFromWindow(ViewHolderNotification holder) {
        super.onViewDetachedFromWindow(holder);
//        ((ViewHolderNotification)holder).clearAnimation();
        holder.itemView.clearAnimation();
    }

    @Override
    public ViewHolderNotification onCreateViewHolder(ViewGroup parent, int viewType) {
        View txnHistoryItemView = LayoutInflater.from(context)
                .inflate(R.layout.noti_item_layout, parent, false);

        return new ViewHolderNotification(txnHistoryItemView);
    }

    @Override
    public void onBindViewHolder(ViewHolderNotification holder, int position) {
        NotificationListModel listModel = mList.get(position);
//        holder.txt_title.setText(listModel.getFromMobile() + " " + listModel.getNotificationMsg());
        holder.txt_title.setText(" " + listModel.getNotificationMsg());
        holder.txt_dateTime.setText(listModel.getCreatedOn());
        if (listModel.getViewed().equalsIgnoreCase("0")) {
            holder.txt_title.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.txt_title.setTypeface(Typeface.DEFAULT_BOLD);
            holder.txt_dateTime.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            holder.txt_title.setTextColor(ContextCompat.getColor(context, R.color.grey_bg));
            holder.txt_dateTime.setTextColor(ContextCompat.getColor(context, R.color.grey_bg));
        }

        if (!listModel.getStrReversed().equalsIgnoreCase("NA") &&
                !listModel.getStrReversed().equalsIgnoreCase(" ") &&
                !listModel.getStrReversed().isEmpty()) {
            holder.txt_reverse.setText(listModel.getStrTransactionStatus());
        }


        setAnimation(holder.itemView, position);
    }

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);// slide_in_left
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        } else {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);// slide_in_left
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    /*private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.fade_in);// slide_in_left
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }*/


    public class ViewHolderNotification extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView txt_title, txt_dateTime, txt_reverse;

        public ViewHolderNotification(View itemView) {
            super(itemView);
            txt_title = (TextView) itemView.findViewById(R.id.txt_title);
            txt_dateTime = (TextView) itemView.findViewById(R.id.txt_dateTime);
            txt_reverse = (TextView) itemView.findViewById(R.id.txt_reverse);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemClickListener.onItemClick("notification", getAdapterPosition());
        }
    }


    //    @Override
//    public void onViewDetachedFromWindow(AdapterServicesTxnHistory.ViewHolder_TxnHstry holder) {
//        super.onViewDetachedFromWindow(holder);
////        ((ViewHolder_TxnHstry)holder).clearAnimation();
//        holder.itemView.clearAnimation();
//    }
}

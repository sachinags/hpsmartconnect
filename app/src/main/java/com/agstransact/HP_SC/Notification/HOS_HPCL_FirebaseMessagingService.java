package com.agstransact.HP_SC.Notification;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.LauncherActivity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.agstransact.HP_SC.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by kalpesh.dhumal on 25-10-2017.
 */

public class HOS_HPCL_FirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = HOS_HPCL_FirebaseMessagingService.class.getSimpleName();

    private HOS_HPCL_FirebaseMessagingService daraPayNotificationUtils;


    @SuppressLint("NewApi")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
            handleNotification(remoteMessage.getNotification().getBody());
        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData().toString());

            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                handleDataMessage(json);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotification(String message) {

        try {
            String current_activity;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                ActivityManager am = (ActivityManager) getSystemService(ACTIVITY_SERVICE);

//                ComponentName componentInfo = taskInfo.get(0).topActivity;
//                Log.e("CURRENT Activity ::", "" + taskInfo.get(0).topActivity.getClassName() + "   Package Name :  " + componentInfo.getPackageName());
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    List<ActivityManager.AppTask> taskInfo = am.getAppTasks();
                    current_activity = taskInfo.get(0).getTaskInfo().topActivity.getClassName();
                } else {
                    List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
                    ComponentName componentInfo = taskInfo.get(0).topActivity;
                    Log.e("CURRENT Activity ::", "" + taskInfo.get(0).topActivity.getClassName() + "   Package Name :  " + componentInfo.getPackageName());
                    current_activity = taskInfo.get(0).topActivity.getClassName();
                }
            } else {
                ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
                List<ActivityManager.RunningTaskInfo> taskInfo = manager.getRunningTasks(1);
                ComponentName componentInfo = taskInfo.get(0).topActivity;
                Log.e("CURRENT Activity ::", "" + taskInfo.get(0).topActivity.getClassName() + "   Package Name :  " + componentInfo.getPackageName());
                current_activity = taskInfo.get(0).topActivity.getClassName();
            }


            Intent resultIntent = new Intent(getApplicationContext(), HOS_HPCL_FirebaseMessagingService.class);
            resultIntent.putExtra("message", message);
            resultIntent.putExtra("current_activity", current_activity);
            resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            startActivity(resultIntent);
        } catch (SecurityException e) {
            e.printStackTrace();
        }


    }

    private void handleDataMessage(JSONObject json) {
        Log.e(TAG, "push json: " + json.toString());

        try {

            if (!HOS_HPCL_NotificationUtils.isAppIsInBackground(getApplicationContext())) {
                notDial();
            } else {
                notDial();
            }
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
    }

    /**
     * Showing notification with text only
     */
//    private void showNotificationMessage(Context context, String title, String message, String timeStamp, Intent intent) {
//        daraPayNotificationUtils = new HOS_HPCL_NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        daraPayNotificationUtils.showNotificationMessage(title, message, timeStamp, intent);
//    }
//
//    /**
//     * Showing notification with text and image
//     */
//    private void showNotificationMessageWithBigImage(Context context, String title, String message, String timeStamp, Intent intent, String imageUrl) {
//        daraPayNotificationUtils = new HOS_HPCL_NotificationUtils(context);
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//        daraPayNotificationUtils.showNotificationMessage(title, message, timeStamp, intent, imageUrl);
//    }

    public void notDial() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.hp_smart_connect_logo)
                        .setContentTitle("My notification")
                        .setContentText("Hello World!");
// Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, LauncherActivity.class);

// The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(LauncherActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
    }

}

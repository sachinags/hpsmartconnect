package com.agstransact.HP_SC.Notification;

import android.app.Dialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.view.ContextThemeWrapper;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.agstransact.HP_SC.BuildConfig;
import com.agstransact.HP_SC.R;


import java.util.Locale;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class HOS_HPCL_NotificationDialog extends AppCompatActivity {

    AlertDialog alertDialog;

    Context context;

    String TAG = "NotDialog";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = HOS_HPCL_NotificationDialog.this;


        try {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
            r.play();
        } catch (Exception e) {
            if (BuildConfig.DEBUG) Log.e(TAG, Log.getStackTraceString(e));
        }
        //22-5
//        sendNotificationCount();
//        updateExpenseData();
        showDialog(getIntent().getExtras().getString("message"));



    }
    @Override
    protected void attachBaseContext(Context newBase) {
        Locale newLocale;
        // .. create or get your new Locale object here.
        try {

            super.attachBaseContext(CalligraphyContextWrapper.wrap(context));
        } catch (Exception e) {
            e.printStackTrace();
            super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        }
    }

    private void updateExpenseData() {
        Intent intent = new Intent("Update_Expense");
        LocalBroadcastManager.getInstance(HOS_HPCL_NotificationDialog.this).sendBroadcast(intent);
    }

    public void sendNotificationCount(){
        Intent intent = new Intent("Notication_Count");
        // You can also include some extra data.
        LocalBroadcastManager.getInstance(HOS_HPCL_NotificationDialog.this).sendBroadcast(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

    }

    public void showDialog(final String msg) {
        LayoutInflater inflater =
                (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.activity_dara_pay_notification_dialog,
                (ViewGroup) findViewById(R.id.layout_root));


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();


        // set the custom dialog components - text, image and button
        TextView txtMsg = (TextView) layout.findViewById(R.id.txtMsg);
        Button btnClose = (Button) layout.findViewById(R.id.btnClose);

        txtMsg.setText(msg);
        try {
//            txtMsg.setMovementMethod(new ScrollingMovementMethod());
        } catch (Exception e) {
            e.printStackTrace();
        }


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (alertDialog.isShowing())
                    alertDialog.dismiss();


                finish();
            }
        });

        alertDialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (alertDialog.isShowing())
                        alertDialog.dismiss();
                    finish();


                }
                return true;
            }
        });
        /*dialog = new Dialog(DaraPayNotificationDialog.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_dara_pay_notification_dialog);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);


        TextView txtMsg = (TextView) dialog.findViewById(R.id.txtMsg);
        Button btnClose = (Button) dialog.findViewById(R.id.btnClose);

        txtMsg.setText(msg);


        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();


                finish();
            }
        });
        dialog.show();


        dialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    finish();
                    dialog.dismiss();

                }
                return true;
            }
        });*/
    }


}

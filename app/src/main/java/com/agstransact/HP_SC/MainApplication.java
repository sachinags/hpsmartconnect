package com.agstransact.HP_SC;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import androidx.appcompat.widget.AppCompatTextView;

import com.agstransact.HP_SC.network.ApiManager;
import com.agstransact.HP_SC.utils.FontsOverride;
import com.agstransact.HP_SC.utils.UtilDate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.StringWriter;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by amit.verma on 23-08-2017.
 */

public class MainApplication extends MultiDexApplication {
    //    private static Context mInstance;

    public static Context mDashBoardContext;
    public static Context mLoginContext;
    public static Context mLauncherContext;
    public static MainApplication mInstance;
    public static ApiManager apiManager;
    public static Context context;


    public static Context getContext() {
        return context;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        apiManager = ApiManager.getInstance();
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread thread, Throwable e) {
                handleUncaughtException(thread, e);
            }
        });


//        mInstance = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            CalligraphyConfig.initDefault("fonts/Roboto-Light.ttf");
            CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                    .setDefaultFontPath("fonts/Roboto-Light.ttf")
                    .addCustomStyle(AppCompatTextView.class, android.R.attr.textViewStyle)
                    .setFontAttrId(R.attr.fontPath)
                    .build()
            );
        } else {
            FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/Roboto-Light.ttf");
        }
    }

//    public static synchronized Context getInstance() {
//        return mInstance;
//    }

//    public void setConnectivityListener(NetworkConnectivityReceiver.ConnectivityReceiverListener listener) {
//        NetworkConnectivityReceiver.connectivityReceiverListener = listener;
//    }


    public void handleUncaughtException(Thread thread, Throwable e) {
        try {
            e.printStackTrace(); // not all Android versions will print the stack trace automatically
            writeToFile(e);
        } catch (Exception e1) {
            e1.printStackTrace();
            System.exit(1);
        }

    }

    void writeToFile(Throwable e) {
        try {
            BufferedWriter out;
            FileWriter filewriter;
            String strTileText = "";

//            StackTraceElement[] temp = e.getCause().getStackTrace();
            StackTraceElement[] temp = e.getStackTrace();
//           String temp2 = Arrays.toString(e.getStackTrace());

            PackageManager manager = this.getPackageManager();
            PackageInfo info = null;
            try {
                info = manager.getPackageInfo(this.getPackageName(), 0);
            } catch (PackageManager.NameNotFoundException e2) {
            }
            String model = Build.MODEL;
            if (!model.startsWith(Build.MANUFACTURER))
                model = Build.MANUFACTURER + " " + model;
            String path = Environment.getExternalStorageDirectory() + "/" + "App Darapay/";

            File directory = new File(path);
            directory.mkdir();
            String fullName = path + "log.txt";
            File external = Environment.getExternalStorageDirectory();
            String sdcardPath = external.getPath();
            File file = new File(fullName);
            if (!file.exists()) {
                file.createNewFile();
            }
            filewriter = new FileWriter(file, true);
            out = new BufferedWriter(filewriter);

            strTileText += "Date: " + UtilDate.getCurrentSysDateDDMMYYY() + "\n";
            strTileText += "=================================================== " + "\n";
            strTileText += "Android version: " + Build.VERSION.SDK_INT + "\n";
            strTileText += "Device: " + model + "\n";
            strTileText += "App version: " + (info == null ? "(null)" : info.versionCode) + "\n";
            strTileText += "Line Number1: " + temp[0] + "\n";
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            strTileText += "Description : " + sw.toString() + "\n\n";
            strTileText += "=======================***==========================" + "\n";

//            strTileText =  AES.encrypt(strTileText, AES.KEY);

            out.write(strTileText);
            out.close();
            filewriter.close();
        } catch (Exception ex) {
            System.exit(1);
        }
        System.exit(1);
    }

}


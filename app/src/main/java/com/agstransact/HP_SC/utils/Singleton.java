package com.agstransact.HP_SC.utils;

import android.bluetooth.BluetoothDevice;

import com.agstransact.HP_SC.bluetooth_print.BluetoothService;
import com.agstransact.HP_SC.bluetooth_print.BluetoothService1;
import com.agstransact.HP_SC.bluetooth_print.P25Connector;

/**
 * Created by amit.verma on 02-08-2017.
 */

public class Singleton {
    private static Singleton mInstance;
    private String username;
    //no outer class can initialize this class's object
    public String fromPageForDone = "";
    /*if old pin is incorrect flag*/
    public Boolean isOldPinInCorrect = false;

    public BluetoothDevice mBTDevice = null;

    public boolean isNetworkAvailable;

//    String photolimit = jsonArray.getJSONObject(0).getString("PHOTOSIZELIMIT");
    public int photolimitSize = 200;

    //21-3-18
    // Member object for the services
    public BluetoothService mBTService = null;
    public BluetoothService1 mBTService1 = null;
//    public BluetoothService mBTService = null;
    //21-3-18

    public P25Connector mConnector = new P25Connector(new P25Connector.P25ConnectionListener() {

        @Override
        public void onStartConnecting() {
        }

        @Override
        public void onConnectionSuccess() {
        }

        @Override
        public void onConnectionFailed(String error) {
        }

        @Override
        public void onConnectionCancelled() {
        }

        @Override
        public void onDisconnected() {
        }
    });

    private Singleton() {
    }

    public static Singleton Instance() {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (mInstance == null) {
            mInstance = new Singleton();
        }
        return mInstance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

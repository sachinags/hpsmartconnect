package com.agstransact.HP_SC.utils;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.core.content.ContextCompat;

import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.agstransact.HP_SC.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by amit.verma on 05-08-2017.
 */
@SuppressLint("ValidFragment")
public class UniversalDatePickerDailog extends DialogFragment
        implements DatePickerDialog.OnDateSetListener,AlertDialogInterface {
    Context mContext;
    EditText txtDateOfBirth;
    TextView txtDateOfBirth1;
    TextView tv_date;
    Calendar c = null;
    String TAG = "UniversalDatePickerDailog";
    DateSelectedInterface dateSelectedInterface;
    UniversalDialog universalDialog;
    AlertDialogInterface alertDialogInterface;
    DatePickerDialog datePickerDialog;
    int year;
    int month;
    int day;
    String title = "";

    boolean isFromTxnSummary;

    public UniversalDatePickerDailog(Context mContext, EditText txtDateOfBirth,DateSelectedInterface dateSelectedInterface) {
        this.mContext = mContext;
        this.txtDateOfBirth = txtDateOfBirth;
        this.dateSelectedInterface = dateSelectedInterface;
        alertDialogInterface = this;
        this.year = 0;
        this.month = 0;
        this.day = 0;
    }
    public UniversalDatePickerDailog(Context mContext, TextView txtDateOfBirth,DateSelectedInterface dateSelectedInterface) {
        this.mContext = mContext;
        this.txtDateOfBirth1 = txtDateOfBirth;
        this.dateSelectedInterface = dateSelectedInterface;
        alertDialogInterface = this;
        this.year = 0;
        this.month = 0;
        this.day = 0;
    }
    public UniversalDatePickerDailog(Context mContext, EditText txtDateOfBirth
            ,DateSelectedInterface dateSelectedInterface
            , int year, int month, int day) {
        this.mContext = mContext;
        this.txtDateOfBirth = txtDateOfBirth;
        this.dateSelectedInterface = dateSelectedInterface;
        alertDialogInterface = this;
        this.year = year;
        this.month = month-1;
        this.day = day;
    }
    public UniversalDatePickerDailog(Context mContext, TextView txtDateOfBirth
            ,DateSelectedInterface dateSelectedInterface
            , int year, int month, int day) {
        this.mContext = mContext;
        this.txtDateOfBirth1 = txtDateOfBirth;
        this.dateSelectedInterface = dateSelectedInterface;
        alertDialogInterface = this;
        this.year = year;
        this.month = month-1;
        this.day = day;
    }

    public UniversalDatePickerDailog(Context mContext, TextView tv_date
            , DateSelectedInterface dateSelectedInterface, String from_date) {
        this.mContext = mContext;
        this.tv_date = tv_date;
        this.dateSelectedInterface = dateSelectedInterface;
        alertDialogInterface = this;
        isFromTxnSummary = true;
        this.year = 0;
        this.month = 0;
        this.day = 0;
        this.title = from_date;
    }

    public UniversalDatePickerDailog(Context mContext, TextView tv_date
            ,DateSelectedInterface dateSelectedInterface
            , int year, int month, int day, String from_date) {
        this.mContext = mContext;
        this.tv_date = tv_date;
        this.dateSelectedInterface = dateSelectedInterface;
        alertDialogInterface = this;
        isFromTxnSummary = true;
        this.year = year;
        this.month = month-1;
        this.day = day;
        this.title = from_date;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        c = Calendar.getInstance();
        if(year==0){
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
        }
        // Create a new instance of DatePickerDialog and return it
//        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_DEVICE_DEFAULT_DARK, this, year, month, day);
//        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_DARK, this, year, month, day);
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, year, month, day);
//        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_TRADITIONAL, this, year, month, day);
//        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), AlertDialog.THEME_TRADITIONAL, this, year, month, day);
//        try {
//            java.lang.reflect.Field[] datePickerDialogFields = datePickerDialog.getClass().getDeclaredFields();
//            for (java.lang.reflect.Field datePickerDialogField : datePickerDialogFields) {
//                if (datePickerDialogField.getName().equals("mDatePicker")) {
//                    datePickerDialogField.setAccessible(true);
//                    DatePicker datePicker = (DatePicker) datePickerDialogField.get(datePickerDialog);
//                    java.lang.reflect.Field[] datePickerFields = datePickerDialogField.getType().getDeclaredFields();
//                    for (java.lang.reflect.Field datePickerField : datePickerFields) {
//                        Log.i("test", datePickerField.getName());
//                        if ("mDelegate".equals(datePickerField.getName())) {
//                            datePickerField.setAccessible(true);
//                            Object dayPicker = datePickerField.get(datePicker);
//                            ((View) dayPicker).setVisibility(View.GONE);
//                        }
//                    }
//                }
//            }
//        }
//        catch (Exception ex) {
//        }
        if (isFromTxnSummary) {

            if(title.equalsIgnoreCase(mContext.getString(R.string.to_date))){
//            if(title.equalsIgnoreCase(mContext.getString(R.string.to_date))){

                try {
                    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                    Date date = sdf.parse(tv_from_date.getText().toString());
//                    long millis = date.getTime();

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
                    datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis()-10000);
                    datePickerDialog.setTitle(title);

                } catch (Exception e) {
                    e.printStackTrace();

                    try {
                        Calendar calendar = Calendar.getInstance();
//                        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
                        calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 1);
                        datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis() - 10000);
                        datePickerDialog.setTitle(title);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
//                Calendar calendar = Calendar.getInstance();
//                calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 7);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() );
            }else {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 3);
                datePickerDialog.getDatePicker().setMinDate(calendar.getTimeInMillis() - 10000);
                datePickerDialog.setTitle(title);
                Calendar calendar1 = Calendar.getInstance();
                calendar1.set(Calendar.DAY_OF_MONTH, calendar1.get(Calendar.DAY_OF_MONTH) - 7);
                datePickerDialog.getDatePicker().setMaxDate(calendar1.getTimeInMillis() - 10000);
            }
        }



//        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        if(isFromTxnSummary) {
//            datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel_dob), new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int which) {
//                    if (which == DialogInterface.BUTTON_NEGATIVE) {
//                        try {
//                            if (isFromTxnSummary) {
//                                tv_date.setText("");
//                                tv_date.setHint("Select Date");
//                            } else {
//                                txtDateOfBirth.setText("");
//                            }
//                        } catch (Exception e) {
//                            e.printStackTrace();
//                        }
//                    }
//                }
//            });
        }else{
            datePickerDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel_dob), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (which == DialogInterface.BUTTON_NEGATIVE) {
                        try {
                            if (isFromTxnSummary) {
                                tv_date.setText("");
                                tv_date.setHint(R.string.select_date_hint);
                                tv_date.setHintTextColor(ContextCompat.getColor(getContext(), R.color.white));
                            }else{
//                                txtDateOfBirth.setText("");
                                txtDateOfBirth1.setText("");
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
        return datePickerDialog;
    }

    public void onDateSet(DatePicker pDatePicker, int year, int month, int day) {
        // Do something with the date chosen by the user

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        try {
            /*validating abe should be 18*/
            Calendar userAge = new GregorianCalendar(year,month,day);
            Calendar minAdultAge = new GregorianCalendar();
//            minAdultAge.add(Calendar.YEAR, -18);
            if (isFromTxnSummary) {
                //28-11-18
                String strday = "0", strmonth ="0";
                if(day<10){
                    strday+=day;
                }else{
                    strday = String.valueOf(day);
                }
                if((month+1)<10){
                    strmonth+=(month+1);
                }else{
                    strmonth = String.valueOf(month+1);
                }
//                String strDate = strday + "-" + strmonth + "-" + year;
//                String strDate =     + "-" + strmonth + "-" + strday;
                String strDate = strday    + "|" + strmonth + "|" + year;
//                Date date1 = sdf.parse(strDate);
                //            ClsGlobale.selectedDate = year + "-" + (month + 1) + "-" + day;
                tv_date.setText(strDate);
                dateSelectedInterface.dateSelectSuccess();

            } else {
//                if (minAdultAge.before(userAge)) {
//                    /*Age is less than 18 year*/
//                    universalDialog = new UniversalDialog(getActivity(),alertDialogInterface,"",getString(R.string.age_validation_msg),
//                            getString(R.string.dialog_ok),"");
//                    universalDialog.showAlert();
//                }else {
                //28-11-18
                String strday = "0", strmonth ="0";
                if(day<10){
                    strday+=day;
                }else{
                    strday = String.valueOf(day);
                }
                if((month+1)<10){
                    strmonth+=(month+1);
                }else{
                    strmonth = String.valueOf(month+1);
                }
//                String strDate = day + "/" + (month + 1) + "/" + year;
//                    String strDate =  strmonth + "/" + year;


                if(strmonth.equalsIgnoreCase("01")){
                    strmonth ="Jan";
                }else if(strmonth.equalsIgnoreCase("02")){
                    strmonth ="Feb";
                }else if(strmonth.equalsIgnoreCase("03")){
                    strmonth ="Mar";
                }else if(strmonth.equalsIgnoreCase("04")){
                    strmonth ="Apr";
                }else if(strmonth.equalsIgnoreCase("05")){
                    strmonth ="May";
                }else if(strmonth.equalsIgnoreCase("06")){
                    strmonth ="Jun";
                }else if(strmonth.equalsIgnoreCase("07")){
                    strmonth ="Jul";
                }else if(strmonth.equalsIgnoreCase("08")){
                    strmonth ="Aug";
                }else if(strmonth.equalsIgnoreCase("09")){
                    strmonth ="Sep";
                }else if(strmonth.equalsIgnoreCase("10")){
                    strmonth ="Oct";
                }else if(strmonth.equalsIgnoreCase("11")){
                    strmonth ="Nov";
                }else if(strmonth.equalsIgnoreCase("12")){
                    strmonth ="Dec";
                }
                //28-11-18
//                    Date date1 = sdf.parse(strDate);
//                    Date date1 = sdf.parse(strmonth +" "+ year);
                //            ClsGlobale.selectedDate = year + "-" + (month + 1) + "-" + day;
//                    txtDateOfBirth.setText(strDate);
                String strDate =  strmonth + " " + year;

                txtDateOfBirth1.setText(strDate);
                dateSelectedInterface.dateSelectSuccess();
//                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void methodDone() {

    }

    @Override
    public void methodCancel() {

    }
}

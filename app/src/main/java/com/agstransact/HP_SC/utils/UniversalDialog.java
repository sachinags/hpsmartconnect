package com.agstransact.HP_SC.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agstransact.HP_SC.R;

/**
 * Created by amit.verma on 03-08-2017.
 */

public class UniversalDialog {
    private final Context mContext;
    private final AlertDialogInterface action;
    private final String message;
    private final String mPositiveBTName;
    private final String mNegativeBTName;
    private final String title;
    private boolean isCancelable;
    androidx.appcompat.app.AlertDialog dialog;


    public UniversalDialog(Context context, AlertDialogInterface action, String title, String message, String pPositiveBTName, String pNegativeBTName) {
        this.mContext = context;
        this.action = action;
        this.title = title;
        this.message = message;
        this.mPositiveBTName = pPositiveBTName;
        this.mNegativeBTName = pNegativeBTName;
        this.isCancelable = true;
    }

    public void setCancelable(boolean bool) {
        this.isCancelable = bool;
    }

    public boolean isShowing() {
        if (dialog != null) {
            return dialog.isShowing();
        } else {
            return false;
        }
    }

    public void closeDialog() {
        try {
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showAlert() {
        try {
            int valueInPixels_twenty = (int) mContext.getResources().getDimension(R.dimen.padding_20);
            int valueInPixels_ten = (int) mContext.getResources().getDimension(R.dimen.padding_10);
//        int valueMsgFont = (int) mContext.getResources().getDimension(R.dimen.fontsize_15);
            androidx.appcompat.app.AlertDialog.Builder builder =
                    new androidx.appcompat.app.AlertDialog.Builder(mContext, R.style.MyDialogTheme);

            TextView resultMessage = new TextView(mContext);
            resultMessage.setPadding(valueInPixels_twenty, valueInPixels_ten, valueInPixels_twenty, valueInPixels_ten);
            resultMessage.setTextSize(20);
            resultMessage.setTextColor(Color.BLACK);
            resultMessage.setText(message);
            resultMessage.setGravity(Gravity.CENTER);
            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            lp.setMargins(0, valueInPixels_twenty, 0, 0);
            resultMessage.setLayoutParams(lp);
            builder.setView(resultMessage);
            builder.setTitle(title);

            builder.setCancelable(isCancelable);
            builder.setPositiveButton(mPositiveBTName,
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // positive button logic
                            dialog.dismiss();
                            if (action != null) {
                                action.methodDone();

                            }

                        }
                    });

            builder.setNeutralButton(mNegativeBTName, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int i) {
                    if (action != null) {
                        action.methodCancel();
                    }
                    dialog.dismiss();
                }
            });

            dialog = builder.create();
            // display dialog


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean checkBackwardPinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length - 1; i++) {
            if (mPin[i] - 1 == mPin[i + 1]) {
                isSequence = true;
            } else {

                Log.e("BackwardPin", "false");
                return false;
            }
        }
        Log.e("BackwardPin", "true");
        return isSequence;
    }

    public boolean checkSamePinSequence(int[] mPin) {
        boolean isSequence = false;
        for (int i = 0; i < mPin.length; i++) {
            for (int j = i + 1; j < mPin.length; j++) {
                if (mPin[i] == mPin[j]) {
                    isSequence = true;
                } else {
                    Log.e("SamePin", "false");
                    return false;
                }
            }
        }
        Log.e("SamePin", "true");
        return isSequence;
    }
    public int[] getIntFromString(String s) {
        int[] intArray = new int[4];
        for (int i = 0; i < 4; i++) {
            intArray[i] = Integer.parseInt(String.valueOf(s.charAt(i)));
        }

        return intArray;
    }

}

package com.agstransact.HP_SC.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.agstransact.HP_SC.MainApplication;
//import com.agstransact.HP_SC.common.Log;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;


public class AsyncDownloadImage extends AsyncTask<Void, Void, Bitmap> {

    String strUrl;
    ImageView imageView;

    boolean isCacheUse;

    public AsyncDownloadImage(String strUrl, ImageView imageView) {
        this.strUrl = strUrl;
        this.imageView = imageView;
    }
    public AsyncDownloadImage(String strUrl, ImageView imageView, boolean isCacheUse) {
        this.strUrl = strUrl;
        this.imageView = imageView;
        this.isCacheUse = isCacheUse;
    }

    @Override
    protected Bitmap doInBackground(Void... voids) {
        Bitmap bm = null;
        try {
            URL url = new URL(strUrl);

            HttpsURLConnection connection = null;
            try {

                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                /*uat and sit*/
//            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open("uatsys.cer"));
                /*production*/
                InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(ClsWebService.CERTIFICATE));
                Certificate ca;
                try {
                    ca = cf.generateCertificate(caInput);
                    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
                } finally {
                    caInput.close();
                }

// Create a KeyStore containing our trusted CAs
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
                SSLContext context = SSLContext.getInstance("TLS");
                context.init(null, tmf.getTrustManagers(), null);

// Tell the URLConnection to use a SocketFactory from our SSLContext
                /*Amit 30.11.17 ssl pining */

                connection = (HttpsURLConnection) url.openConnection();
                /*Comment below line when url is development 06.12.17*/
                connection.setSSLSocketFactory(context.getSocketFactory());
                /*06.12.17*/


                if (isCacheUse) {
                    connection.setUseCaches(false);
                } else {
                   connection.setUseCaches(true);
                }
                connection.setAllowUserInteraction(false);
                connection.setConnectTimeout(45000);
                connection.setReadTimeout(45000);

                connection.connect();
                InputStream inputStream = connection.getInputStream();
                BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
                bm = BitmapFactory.decodeStream(bufferedInputStream);
                bufferedInputStream.close();
                inputStream.close();

                return bm;
            } catch (Exception e) {
                e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                Log.e("API Exception", e.toString());
            } finally {

                try {
                    connection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


        } catch (Exception e) {
            e.printStackTrace();

        }
        return bm;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (bitmap == null) {
//            imageView.setImageResource(R.drawable.airtel_logo);
        } else {
            imageView.setImageBitmap(bitmap);
        }

    }


    public  void setCacheUse(boolean isCacheUse){

        this.isCacheUse = isCacheUse;
    }



}




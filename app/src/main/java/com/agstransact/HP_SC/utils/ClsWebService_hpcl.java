package com.agstransact.HP_SC.utils;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

//import com.agstransact.HP_SC.prefrence.UserDataPrefrence;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

/**
 * Created by ritesh.bhavsar and amit verma  on 16-09-2017.
 */

public class ClsWebService_hpcl {

    //    private static final String URL = "http://52.172.43.210/HOSAPI/api/Login/";
//    private static final String URL = "https://uatsys.agsindia.com:7526/api/Login/";
//    private static final String URL = "https://dev.agsindia.com:7521/api/";
    // DEV AGS
//    private static final String URL = "http://10.90.37.33:8850/api/";
    //UAT AGS
//    private static final String URL = "https://uatsys.agsindia.com:7526/api/";
    //UAT HPCL
//    private static final String URL = "https://chosuat.hpcl.co.in/api/";
    //Prod HPCL
    private static final String URL = "https://chosm.hpcl.co.in/api/";
//    private static final String STR_DEV_CER = "atmgo_20_21.crt";
    private static final String STR_DEV_CER = "chos.hpcl.co.in.crt";
//    private static final String STR_DEV_CER = "agsindiacert20_22.cer";
    public static String CERTIFICATE = STR_DEV_CER;
    static String token;
    private static final int API_TIMEOUT = 60000;
    private static final int API_CONNECT_TIMEOUT = 20000;
    private static TrustManager[] trustManagers;


    public static String PostObject(String methodName, boolean isGet, JSONObject jsonObj) {
        WeakReference<Context> mmContext = null;
        java.net.URL murl = null;
        StringBuilder sb = new StringBuilder();
            HttpsURLConnection connection = null;
            try {
//                if (trustManagers == null) {
//                    trustManagers = new TrustManager[]{new SSLConection._FakeX509TrustManager()};
//                }
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
                Certificate ca;
                try {
                    ca = cf.generateCertificate(caInput);
                    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
                } finally {
                    caInput.close();
                }
                // Create a KeyStore containing our trusted CAs
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);
                SSLConection trustManager = new SSLConection(keyStore);

                // Create a TrustManager that trusts the CAs in our KeyStore
                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);
                // Create an SSLContext that uses our TrustManager
//                TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
//                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
//                        return null;
//                    }
//                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
//                    }
//                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
//                    }
//                }
//                };
                SSLContext context = SSLContext.getInstance("TLS");
//                SSLContext context = SSLContext.getInstance("SSL");
                context.init(null, tmf.getTrustManagers(), null);
//                context.init(null,new TrustManager[]{trustManager}, null);

                connection.setSSLSocketFactory(context.getSocketFactory());
//                connection.setDefaultSSLSocketFactory(context.getSocketFactory());
//                if (methodName.equalsIgnoreCase("delink")) {
//                    murl = new URL(URL);
//                } else {
                    murl = new URL(URL + methodName);
//                }

                connection = (HttpsURLConnection) murl.openConnection();
                /*jwt token has been implemented only for common  */
                try {
//                    if (methodName.equalsIgnoreCase("")){
                        token = UserDataPrefrence.getPreference("HPCL_Preference", MainApplication.getContext(), "TOKEN", "");
//                        UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),MainApplication.getContext(), ConstantDeclaration.TOKEN, token);
                        connection.setRequestProperty("authorization", "Bearer " + token);
//                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                connection.setRequestProperty("Content-type", "application/json");

                if (isGet) {
                    connection.setRequestMethod("GET");
                } else {
                    connection.setRequestMethod("POST");
                }

                connection.setUseCaches(false);
                connection.setAllowUserInteraction(false);
                connection.setConnectTimeout(API_CONNECT_TIMEOUT);
                connection.setReadTimeout(API_TIMEOUT);

                DataOutputStream obj = new DataOutputStream(connection.getOutputStream());
                obj.writeBytes(jsonObj.toString());
                obj.flush();


                int responsecode = connection.getResponseCode();

                if (responsecode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    String line = "";
                    Log.d("API Response",line.toString());
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    reader.close();
                    String str_response_msg = "";

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    String str_resCode = jsonObject.getString("ResponseCode");
                    str_response_msg = jsonObject.getString("ResponseMessage");
                    String str_resTime = "";
//                    Log.e("Response", str_resCode + "\n str_response_msg : " + str_response_msg);

                    try {
//                        if (str_resCode.equalsIgnoreCase("5555")) {
//                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    try {
////                        str_resTime = jsonObject.getString("TimeStamp");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
                    String res = str_resCode
                            + "||" + str_response_msg
                            + "||" + sb.toString();
                    return res.trim();
                } else if(responsecode == HttpsURLConnection.HTTP_UNAUTHORIZED){
                    return responsecode
                            + "||" + "Session Expired"
                            + "||" + "Please login again";

                } else {
                    return responsecode
                            + "||" + "Something went wrong"
                            + "||" + "Something went wrong";
                }
            } catch (java.net.SocketTimeoutException e) {
                Log.e("API Exception", e.toString());
                return "8086||Connection Timeout||Connection Timeout";
            } catch (IOException e) {
                Log.e("API Exception", e.toString());
                return "8086||Connection Timeout||Connection Timeout";
            } catch (Exception e) {
                e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                Log.e("API Exception", e.toString());
                if (methodName.equalsIgnoreCase("UpdateDeviceBuild")
                ) {
                    return sb.toString();
                }
                return "";
            } finally {
                try {
                    connection.disconnect();
                } catch (Exception e) {
                    Log.e("API Exception", e.toString());
                    e.printStackTrace();
                }
            }
        }
    public static String PostObjecttoken(String methodName, boolean isGet, JSONObject jsonObj, String token) {
        WeakReference<Context> mmContext = null;
        java.net.URL murl = null;
        StringBuilder sb = new StringBuilder();

        HttpsURLConnection connection = null;
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }
            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

//                if (methodName.equalsIgnoreCase("delink")) {
//                    murl = new URL(URL);
//                } else {
            murl = new URL(URL + methodName);
//                }

            connection = (HttpsURLConnection) murl.openConnection();
            /*jwt token has been implemented only for common  */
//            connection.setSSLSocketFactory(context.getSocketFactory());
            connection.setRequestProperty("Content-type", "application/json");
            try {
                    if (token.equalsIgnoreCase("")){
                        token = UserDataPrefrence.getPreference("HPCL_Preference", MainApplication.getContext(), "HPCLTOKEN", "");
                        connection.setRequestProperty("authorization", "Bearer " + token);
                    }
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (isGet) {
                connection.setRequestMethod("GET");
            } else {
                connection.setRequestMethod("POST");
            }

            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setConnectTimeout(API_CONNECT_TIMEOUT);
            connection.setReadTimeout(API_TIMEOUT);

            DataOutputStream obj = new DataOutputStream(connection.getOutputStream());
            obj.writeBytes(jsonObj.toString());
            obj.flush();


            int responsecode = connection.getResponseCode();

            if (responsecode == HttpsURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                Log.d("API Response",line.toString());
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                String str_response_msg = "";

                JSONObject jsonObject = new JSONObject(sb.toString());
                String str_resCode = jsonObject.getString("ResponseCode");
                str_response_msg = jsonObject.getString("ResponseMessage");
                String res = str_resCode
                        + "||" + str_response_msg
                        + "||" + sb.toString();
                Log.e("res",res);
                return res.trim();

            }
            else if(responsecode == HttpsURLConnection.HTTP_UNAUTHORIZED){
                return responsecode
                        + "||" + "Session Expired"
                        + "||" + "Please login again";

            } else {
                return responsecode
                        + "||" + "Something went wrong"
                        + "||" + "Something went wrong";
            }
        } catch (java.net.SocketTimeoutException e) {
            Log.e("API Exception", e.toString());
            return "8086||Connection Timeout||Connection Timeout";
        } catch (IOException e) {
            Log.e("API Exception", e.toString());
            return "8086||Connection Timeout||Connection Timeout";
        } catch (Exception e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Log.e("API Exception", e.toString());
            if (methodName.equalsIgnoreCase("UpdateDeviceBuild")
            ) {
                return sb.toString();
            }
            return "";
        } finally {
            try {
                connection.disconnect();
            } catch (Exception e) {
                Log.e("API Exception", e.toString());
                e.printStackTrace();
            }
        }
    }
    public static Void PostObjectDownload(String methodName, boolean isGet, File directory1, String token) {
        WeakReference<Context> mmContext = null;
        java.net.URL murl = null;
        StringBuilder sb = new StringBuilder();



        HttpsURLConnection connection = null;
        try {
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }
            // Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

            // Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

            // Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

//                if (methodName.equalsIgnoreCase("delink")) {
//                    murl = new URL(URL);
//                } else {
            murl = new URL("https://chosm.hpcl.co.in/" + methodName);
//                }

            connection = (HttpsURLConnection) murl.openConnection();
            /*jwt token has been implemented only for common  */
//            connection.setSSLSocketFactory(context.getSocketFactory());
            connection.setRequestProperty("Content-type", "application/json");
            try {
                if (token.equalsIgnoreCase("")){
                    token = UserDataPrefrence.getPreference("HPCL_Preference", MainApplication.getContext(), "HPCLTOKEN", "");
                    connection.setRequestProperty("authorization", "Bearer " + token);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (isGet) {
                connection.setRequestMethod("GET");
            } else {
                connection.setRequestMethod("POST");
            }

            connection.setUseCaches(false);
            connection.setAllowUserInteraction(false);
            connection.setConnectTimeout(API_CONNECT_TIMEOUT);
            connection.setReadTimeout(API_TIMEOUT);

            connection.connect();

            int responsecode = connection.getResponseCode();

            if (responsecode == HttpsURLConnection.HTTP_OK) {
                int contentLength = connection.getContentLength();
//                DataInputStream stream = new DataInputStream(murl.openStream());
                DataInputStream stream = new DataInputStream(connection.getInputStream());
                byte[] buffer = new byte[contentLength];
                stream.readFully(buffer);
                stream.close();
                String path = Environment.getExternalStorageDirectory() + "/" + "App HOS/";
                File directory = new File(path);
                directory.mkdir();
                String fullName = "";
                File file;
                File imagePath = new File(Environment.getExternalStorageDirectory(), "App HOS");

                String path1 = Environment.getExternalStorageDirectory() + "/" + "App HOS/";
                File directory2 = new File(path1);
                directory1.mkdir();

                File newFile = new File(imagePath, "HOSfile1.pdf");
                fullName = newFile.getPath();
                file = new File(fullName);
                if (!file.exists()) {
                    file.createNewFile();
                }
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line = "";
                Log.d("API Response",line.toString());
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                reader.close();
                String str_response_msg = "";

                JSONObject jsonObject = new JSONObject(sb.toString());
                String str_resCode = jsonObject.getString("ResponseCode");
                str_response_msg = jsonObject.getString("ResponseMessage");
//                String res = str_resCode
//                        + "||" + str_response_msg
//                        + "||" + sb.toString();
//                Log.e("res",res);
//                return res.trim();
//                return null;

            }
            else if(responsecode == HttpsURLConnection.HTTP_UNAUTHORIZED){
//                return responsecode
//                        + "||" + "Session Expired"
//                        + "||" + "Please login again";

            } else {
//                return responsecode
//                        + "||" + "Something went wrong"
//                        + "||" + "Something went wrong";
            }
        } catch (java.net.SocketTimeoutException e) {
            Log.e("API Exception", e.toString());
//            return "8086||Connection Timeout||Connection Timeout";
        } catch (IOException e) {
            Log.e("API Exception", e.toString());
//            return "8086||Connection Timeout||Connection Timeout";
        } catch (Exception e) {
            e.printStackTrace();
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Log.e("API Exception", e.toString());
            if (methodName.equalsIgnoreCase("UpdateDeviceBuild")
            ) {
//                return sb.toString();
            }
//            return "";
        } finally {
            try {
                connection.disconnect();
            } catch (Exception e) {
                Log.e("API Exception", e.toString());
                e.printStackTrace();
            }
        }
        return null;
    }

}

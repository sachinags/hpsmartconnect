package com.agstransact.HP_SC.utils;

public interface DrawerLocker {
    public void setDrawerLocked(boolean shouldLock);
}

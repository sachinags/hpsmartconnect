package com.agstransact.HP_SC.utils;


import android.util.Base64;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class AES {
    private static final String characterEncoding = "UTF-8";
    private static final String cipherTransformation = "AES/CBC/PKCS5Padding";
    private static final String aesEncryptionAlgorithm = "AES";
    /*this is using for fist time when launcher(Get Version APi)is going to call */
        public static String KEY = "AGSIndiaSwitch12";
        public static String KEY_HPCL = "AGSIndiaSwitch12";
        public static String KEY1 = "AGSIndiaSwitch12";
        public static final String DEFAULT_KEY = "AGSIndiaSwitch12";
        public static String OLD_PRE_KEY = "AGSIndiaSwitch12";


//    public static String KEY = "";
//    public static String ANDROID_SOURCEiD = "8ABB2315-0A67-4C3F-BDFB-64DDD198A2B3";


    public static String getkey(String keyValue, String tranType) {
        return keyValue.substring(0, 7) + tranType + keyValue.substring(keyValue.length() - 7);
    }

    public static void getSessionKey(String sessionId, String sourceId) {
        KEY = sourceId.substring(9, 13) + sessionId.substring(0, 5) + sourceId.substring(14, 17) + sessionId.substring(5, 9);
//        return sourceId.substring(9, 13) + sessionId.substring(0, 5) + sourceId.substring(14, 17) + sessionId.substring(5, 9);
    }
    public static void getSessionKeyDelink(String sessionId, String sourceId) {
        OLD_PRE_KEY = sourceId.substring(9, 13) + sessionId.substring(0, 5) + sourceId.substring(14, 17) + sessionId.substring(5, 9);
//        return sourceId.substring(9, 13) + sessionId.substring(0, 5) + sourceId.substring(14, 17) + sessionId.substring(5, 9);
    }


    public static byte[] decrypt(byte[] cipherText, byte[] key,
                                 byte[] initialVector) throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpecy = new SecretKeySpec(key,
                aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpecy, ivParameterSpec);
        cipherText = cipher.doFinal(cipherText);
        return cipherText;
    }


    public static byte[] encrypt(byte[] plainText, byte[] key, byte[] initialVector)
            throws NoSuchAlgorithmException,
            NoSuchPaddingException, InvalidKeyException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {

        Cipher cipher = Cipher.getInstance(cipherTransformation);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, aesEncryptionAlgorithm);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
        plainText = cipher.doFinal(plainText);
        return plainText;
    }

    private static byte[] getKeyBytes(String key)
            throws UnsupportedEncodingException {
        byte[] keyBytes = new byte[16];
        byte[] parameterKeyBytes = key.getBytes(characterEncoding);
        System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
        return keyBytes;
    }

    public static String encrypt(String plainText, String key)
            throws UnsupportedEncodingException, InvalidKeyException,
            NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        byte[] plainTextbytes = plainText.getBytes(characterEncoding);
        byte[] keyBytes = getKeyBytes(key);
//        return Base64.encodeBase64String(encrypt(plainTextbytes, keyBytes,keyBytes));
        String encodedString = new String(com.google.api.client.util.Base64.encodeBase64(encrypt(plainTextbytes, keyBytes, keyBytes)));
        String safeString = encodedString.replace('+', '-').replace('/', '_');
//        android.util.Base64.en
//        Base64.encodeB
//        com.google.api.client.util.Base64.encodeBase64()
        return safeString;
    }

    public static String decrypt(String encryptedText, String key)
            throws KeyException, GeneralSecurityException,
            GeneralSecurityException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException, IOException, NoSuchPaddingException, InvalidKeyException {
        byte[] cipheredBytes = com.google.api.client.util.Base64.decodeBase64(encryptedText.getBytes(characterEncoding));
        byte[] keyBytes = getKeyBytes(key);
        return new String(decrypt(cipheredBytes, keyBytes, keyBytes), characterEncoding);
    }
    public static String decryptDelink(String encryptedText, String key)
            throws KeyException, GeneralSecurityException,
            GeneralSecurityException, InvalidAlgorithmParameterException,
            IllegalBlockSizeException, BadPaddingException, IOException, NoSuchPaddingException, InvalidKeyException {
        byte[] cipheredBytes = com.google.api.client.util.Base64.decodeBase64(encryptedText.getBytes(characterEncoding));
        byte[] keyBytes = getKeyBytes(key);
        return new String(decrypt(cipheredBytes, keyBytes, keyBytes), characterEncoding);
    }

    public static String localEncrypt(String plainText, String key)
            throws UnsupportedEncodingException, InvalidKeyException,
            NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidAlgorithmParameterException, IllegalBlockSizeException,
            BadPaddingException {
        byte[] plainTextbytes = plainText.getBytes(characterEncoding);
        byte[] keyBytes = getKeyBytes(key);
//        return Base64.encodeBase64String(encrypt(plainTextbytes, keyBytes,keyBytes));
        String encodedString = new String(com.google.api.client.util.Base64.encodeBase64(encrypt(plainTextbytes, keyBytes, keyBytes)));
//        String safeString = encodedString.replace('+','-').replace('/','_');
        return encodedString;
    }
}

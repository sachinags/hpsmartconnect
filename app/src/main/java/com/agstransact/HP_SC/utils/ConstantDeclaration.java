package com.agstransact.HP_SC.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.format.Formatter;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.bluetooth_print.BluetoothService;
import com.agstransact.HP_SC.bluetooth_print.PrinterCommands;
import com.agstransact.HP_SC.bluetooth_print.Utils;
//import com.agstransact.HP_SC.merchantServices.model.TopUpListModel;
import com.agstransact.HP_SC.model.TopUpListModel;
//import com.agstransact.HP_SC.prefrence.UserDataPrefrence;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.sdk.Command;
import com.agstransact.HP_SC.sdk.PrintPicture;
import com.agstransact.HP_SC.sdk.PrinterCommand;
import com.agstransact.HP_SC.utils.ActionChangeFrag;
//import com.google.api.client.repackaged.com.google.common.base.Strings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import static android.content.Context.WIFI_SERVICE;


public class ConstantDeclaration {
    //  IS_NO_SSL = true will run No SSL URL on 29-02-20
    public static final boolean IS_NO_SSL = false ;
    //  IS_NO_SSL = true will run No SSL URL on 29-02-20

    //  IS_SSL_CONTEXT = false will run No SSL URL on 29-02-20
    public static final boolean IS_SSL_CONTEXT = true ;
    //  IS_SSL_CONTEXT = false will run No SSL URL on 29-02-20

    public static final boolean IS_WITHOUT_SIM_ACCESS = false ;


    public static final String INVALID_PIN =  "907";
//    PrashantG 19-03-2019
//    Acleda Details
    public static final String CASH_IN_USD = "00010869824511";
    public static final String CASH_IN_KHR = "00010869824521";
    public static final String CASH_OUT_USD = "00010869824611";
    public static final String CASH_OUT_KHR = "00010869824621";


    public static final String MerchantURL_Local =  "http://192.168.0.109:4040/MOC";
    public static final String MerchantURL =  "https://epaymentuat.acledabank.com.kh:8443/DARAPAY";
    public static final String MerchantName =  "DARAPAY";
    public static final String MerchantID =  "aqiqQ4XSHhKtrYq+m7UdH46dQ68=";
    public static final String UserName =  "darapayuser";
    public static final String Password =  "DaraPay$$123";
    public static final String Secret =  "48a07c5e0059";

//    Acleda Details

    public static final String DEVELOPER_KEY = "AIzaSyD8O2QmJZfRYh5neJ7CEODGlvPUnpnYiiQ";
    public static final String PROFILEIMG = "profileImage";

    public static String ANDROID_SOURCEID = "8ABB2315-0A67-4C3F-BDFB-64DDD198A2B3";
    public static String LANGUAGEPREFKEY = "LANGUAGECODE";
    public static String IMEIPREFKEY = "IMEI";
    public static String SIMSERIALNOPREFKEY = "SIMSERIALNO";
    public static String MOBILENOPREFKEY = "MobileNo";
    public static String CUSTOMERTYPEPREFKEY = "CUSTOMERTYPE";
    public static String ISDEVICEDELINKPREFKEY = "ISDEVICEDELINK";
    public static String TOKEN = "TOKEN";
    public static String ISSETMPINPREFKEY = "ISSETMPIN";
    public static String CHANGEPWDPREFKEY = "CHANGEPWD";
    public static String USERROLEPREFKEY = "USERROLE";
    public static String USERMERCH_ID = "user_merch_id";
    public static String USERMERCH_NAME = "user_merch_name";
    public static String SESSIONKEYPREFKEY = "SESSIONKEY";
    public static String FULLNAMEPREFKEY = "FULLNAME";
    public static String USERIDPREFKEY = "USERID";
    public static String USERDETAILIDPREFKEY = "USERDETAILID";
    public static String EMAILPREFKEY = "EMAIL";
    public static String ADDRESSPREFKEY = "ADDRESS";
    public static String LASTLOGINDATEPREFKEY = "LASTLOGINDATE";
    public static String TELCO_DETAILS_KEY = "TELCODETAILS";
    public static String TELCO_REVERSAL_STATUS = "TELCOREVERSALSTATUS";

    public static String MERCHANTTYPEMASTERLISTPREFKEY = "MERCHANTTYPEMASTERLIST";
    public static String ROLEMASTERLISTPREFKEY = "ROLEMASTERLIST";
    public static String IDTYPEMASTERLISTPREFKEY = "IDTYPEMASTERLIST";
    public static String CURRENCYMASTERLISTPREFKEY = "CURRENCYMASTERLIST";
    public static String CUSTOMERTYPEMASTERLISTPREFKEY = "CUSTOMERTYPEMASTERLIST";
    public static String MERCHANTCATEGORYMASTERLISTPREFKEY = "MERCHANTCATEGORYMASTER";
    public static String CURRENCY_FLAG = "CURRENCYFLAG";
    public static String EXCHANGE_RATE = "EXCHANGE_RATE";


    //    Sagar Input
    public static String SELECTED_CURRENCY = "CURRENCY_TAG";
    public static String CURRENCY_RATE = "CURRENCY_RATE_TAG";
    //    Sagar H Sompura 10-01-2018 making app bilingual
    public static String LANGUAGE_SELECTED = "LANG_SEL";

    //      Input for fingerprint checks saving
    public static String CHECK_ANDROID_SDK = "ANDROIDSDK";
    public static String FP_HARDWARE = "FP_HARDWARE";
    public static String FP_PERMISSION = "FP_PERMISSION";
    public static String FP_REGISTERED = "FP_REGISTERED";
    public static String FP_LOCKSCREEN = "FP_LOCKSCREEN";
    public static String ISDEVICEDELINK = "DEVICE_DELINK";
    public static String ISSETMPIN = "SET_MPIN";
    public static String USER_MOBILE = "USER_MOBILE";
    //31-1-19
    //get casa list according to role and enhancement
    public static int ENHANCEMENT_RES_CASA = 1;
    public static final String KEY_NAME = "DARAPAYWalletRDLSKN";

    public static String CALL_FROM_TXNSUCCESS = "CALL_FROM";
    public static String CALL_FOR_WALLET = "darapay";
    public static String CALL_FOR_NONWALLET = "nondarapay";
    public static String CUSTOMERFLAG = "4";
    public static String STAFFFLAG = "3";
    public static String MERCHANTFLAG = "8";
    public static String AGENTFLAG = "2";
// 2 for Agent
// 8 for Merchant


    public static String CALL_FROM = "callfrom";
    public static String CALL_TYPE = "calltype";
    public static String CALL_EWALLET = "darapay";
    public static String CALL_NON_EWALLET = "nondarapay";
    public static String TRAN_TYPE_CASHIN = "1";
    public static final String TRAN_TYPE_CASHOUT = "2";
    public static final String TRAN_TYPE_CASHOUT_SUBOPTIONS = "-2";
    public static String TRAN_TYPE_MOBILETOPUP = "3";
    public static String TRAN_TYPE_CASA_TO_EWALLET = "4";
    public static String TRAN_TYPE_EWALLET_TO_EWALLET = "5";
    public static String TRAN_TYPE_EWALLET_TO_NONWALLET = "6";
    public static String TRAN_TYPE_NONWALLET_TO_NONWALLET = "7";
    public static String TRAN_TYPE_BALANCE_INQUIRY = "8";
    public static String TRAN_TYPE_NONWALLET_TO_EWALLET = "9";
    public static String TRAN_TYPE_REVERSAL = "10";
    public static String TRAN_TYPE_COLLECT_CASH = "11";
    public static String TRAN_TYPE_EWALLET_TO_CASA = "12";
    public static String TRAN_TYPE_NONWALLET_TO_CASA = "13";
    public static String TRAN_TYPE_NWALLET_TO_MOBILETOPUP = "14";
    public static String TRAN_TYPE_MANUAL_REVERSAL = "15";
    public static String TRAN_TYPE_LOADING = "16";
    public static String TRAN_TYPE_UNLOADING = "19";
    public static String TRAN_TYPE_EWALLET_TO_CASA_CP = "20";
    public static String TRAN_TYPE_NONWALLET_TO_CASA_CP = "21";
    public static String TRAN_TYPE_MERCHANT_PAYMENT = "22";
    public static final String TRAN_TYPE_MERCHANT_PAYMENT_REFUND = "23";
    public static String TRAN_TYPE_CUST_UPGRD = "24";
    public static String TRAN_TYPE_WATER_SUPPLY_BILL_PAY_FOR_CUST = "25";
    public static String TRAN_TYPE_WATER_SUPPLY_BILL_PAY_FOR_AGENT= "26";
    public static final String TRAN_TYPE_Payroll = "27";
    public static String TRAN_TYPE_ACLEDA = "31";
    public static final String TRAN_TYPE_LOAN_DISBURSEMENT_AGENT = "32";
    public static String TRAN_TYPE_ACLEDA_AGENT = "34";
    public static final String TRAN_TYPE_Alipay_QR_PAYMENT = "35";
    public static final String TRAN_TYPE_Alipay_Refund = "36";
    public static final String TRAN_TYPE_ACLEDA_CashOut = "37";
    public static final String TRAN_TYPE_ACLEDA_BANK = "38";
    public static final String TRAN_TYPE_MPWT_AUCTION = "39";
    public static String TRAN_TYPE_ECOMMERCE_PAYMENT = "40";
    public static final String TRAN_TYPE_Ecommerce_Refund = "41";



    //4-4-18

    public static String STAN_KEY = "STAN";
    public static String STAN_KEY_ACLEDA = "STAN_ACLEDA";
    public static String SERVER_NOT_RESPONDING = "Server not responding";
    public static String SERVER_UNDER_MENTANANCE = "DaraPay is currently down for maintenance.We expect to be back soon. Thanks for your patience.";
    public static String FIREBASETOKENPREFKEY = "firebasetoken";
    public static String MEGTelconame = "MEG";
    public static String RazerTelconame = "Razer";


    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static String USER_FP_ENROLLED_KEY = "userfpenrolled";

    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 189;
    //    public static final int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 169;
    public static final int MY_PERMISSIONS_REQUEST_EXTERNAL_DATA = 179;
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 169;

    public static final String NOTIFICATIONCOUNTPREFKEY = "NotificationCount";
    //21-3-18
    //riteshb
    /******************************************************************************************************/
    // Message types sent from the BluetoothService Handler
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;
    public static final int MESSAGE_CONNECTION_LOST = 6;
    public static final int MESSAGE_UNABLE_CONNECT = 7;
    public static final int MESSAGE_CONN_LOST_1ST = 8;
    public static final int MESSAGE_CONN_LOST_2ND = 9;
    public static final int MESSAGE_CONN_LOST_3RD = 10;


    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    //22-5
    public static boolean shouldCallAPI = false;
    public static boolean goDown = true;

    public static int OTP_LIMIT_EXID_COUNT = 0;
    public static String TXN_LIST = "TXN_LIST";

    /*******************************************************************************************************/
    //21-3-18
    public static boolean isEmailValid(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        return pattern.matcher(email).matches();
    }

    public static Dialog gifProgressDialog;
    public static Dialog dialog;

    public static void showGifProgressDialog(Context mContext) {
//        if (gifProgressDialog == null) {
        gifProgressDialog = new Dialog(mContext);
        gifProgressDialog.setContentView(R.layout.gif_dialog_layout);
        gifProgressDialog.getWindow().setBackgroundDrawableResource(R.color.transperent100);
        gifProgressDialog.setCancelable(false);
        gifProgressDialog.show();
//        }
    }

    public static void dismissGifProgressDialog() {
        if (gifProgressDialog.isShowing()) {
            gifProgressDialog.dismiss();
//            gifProgressDialog = null;
        }
    }

//    public static void hideKeyboard(Activity activity) {
//        try {
//            InputMethodManager inputMethodManager =
//                    (InputMethodManager) activity.getSystemService(
//                            Context.INPUT_METHOD_SERVICE);
//            inputMethodManager.hideSoftInputFromWindow(
//                    activity.getCurrentFocus().getWindowToken(), 0);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        try {
            View v = ((Activity) ctx).getCurrentFocus();
            if (v == null)
                return;

            inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

    public static void hideKeyboard(Context mContext, View activity) {
        try {
            InputMethodManager inputMethodManager =
                    (InputMethodManager) mContext.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
            activity.clearFocus();
            inputMethodManager.hideSoftInputFromWindow(
                    activity.getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                return true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                return true;
            }
        } else {
            // not connected to the internet
            return false;
        }
        return false;
    }

    private static String GetDeviceipMobileData() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
                 en.hasMoreElements(); ) {
                NetworkInterface networkinterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkinterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (Exception ex) {
//            Log.e("Current IP", ex.toString());
        }
        return null;
    }

    private static String GetDeviceipWiFiData(Context context) {

        WifiManager wm = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);

        @SuppressWarnings("deprecation")

        String ip = Formatter.formatIpAddress(wm.getConnectionInfo().getIpAddress());

        return ip;

    }

    //Check the internet connection.
    public static String getDeviceIPAddress(Context context) {

        boolean WIFI = false;

        boolean MOBILE = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork != null) { // connected to the internet
            if (activeNetwork.getType() == ConnectivityManager.TYPE_WIFI) {
                // connected to wifi
                WIFI = true;
            } else if (activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE) {
                // connected to the mobile provider's data plan
                MOBILE = true;
            }
        }

        if (WIFI) {
            return GetDeviceipWiFiData(context);

        }

        if (MOBILE) {
            return GetDeviceipMobileData();

        }

        return "";
    }

    public static String getDeviceID(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static void replaceFragment(Fragment fragment, FragmentManager fragmentManager) {
        String backStateName = fragment.getClass().getName();


        Fragment currentFrag = fragmentManager.findFragmentById(R.id.dashboard_container);
        Log.e("Current Fragment", "" + currentFrag);

//        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        int countFrag = fragmentManager.getBackStackEntryCount();
        Log.e("Count", "" + countFrag);


        if (currentFrag != null && currentFrag.getClass().getName().equalsIgnoreCase(fragment.getClass().getName())) {
            return;
        }

//        fragment.setArguments(bundle);


        FragmentTransaction ft = fragmentManager.beginTransaction();
//        ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.replace(R.id.dashboard_container, fragment);
        ft.addToBackStack(backStateName);
        //28/12/17 riteshb
//        ft.commit();
        ft.commitAllowingStateLoss();
        //28/12/17 riteshb
        currentFrag = fragmentManager.findFragmentById(R.id.dashboard_container);
        Log.e("Current Fragment", "" + currentFrag);


    }

    public static void replaceFragment_login(Fragment fragment, FragmentManager fragmentManager) {
        String backStateName = fragment.getClass().getName();


        Fragment currentFrag = fragmentManager.findFragmentById(R.id.frameLayout);
        Log.e("Current Fragment", "" + currentFrag);

//        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        int countFrag = fragmentManager.getBackStackEntryCount();
        Log.e("Count", "" + countFrag);


        if (currentFrag != null && currentFrag.getClass().getName().equalsIgnoreCase(fragment.getClass().getName())) {
            return;
        }

//        fragment.setArguments(bundle);


        FragmentTransaction ft = fragmentManager.beginTransaction();
//        ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.replace(R.id.frameLayout, fragment);
        ft.addToBackStack(backStateName);
        //28/12/17 riteshb
//        ft.commit();
        ft.commitAllowingStateLoss();
        //28/12/17 riteshb
        currentFrag = fragmentManager.findFragmentById(R.id.frameLayout);
        Log.e("Current Fragment", "" + currentFrag);


    }


//    public static void addFragment(Fragment fragment, FragmentManager fragmentManager) {
//        String backStateName = fragment.getClass().getName();
//
//
//        Fragment currentFrag = fragmentManager.findFragmentById(R.id.frame_container);
//        Log.e("Current Fragment", "" + currentFrag);
//
////        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
//        int countFrag = fragmentManager.getBackStackEntryCount();
//        Log.e("Count", "" + countFrag);
//
//
//        if (currentFrag != null && currentFrag.getClass().getName().equalsIgnoreCase(fragment.getClass().getName())) {
//            return;
//        }
//
////        fragment.setArguments(bundle);
//
//
//        FragmentTransaction ft = fragmentManager.beginTransaction();
//        ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
//        ft.add(R.id.frame_container, fragment);
//        ft.addToBackStack(backStateName);
//        //28/12/17 riteshb
////        ft.commit();
//        ft.commitAllowingStateLoss();
//        //28/12/17 riteshb
//        currentFrag = fragmentManager.findFragmentById(R.id.frame_container);
//        Log.e("Current Fragment", "" + currentFrag);
//
//
//    }

    /*Amit Verma this method for getting focus to editText during validation error*/

    public static void requestFocus(View view, Activity context) {
        if (view.requestFocus()) {
            context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

// PrashantG 20-07-2018  For clear soft keypad focus(hide keypad)
    public static void clearFocus( Activity context) {
        try {
            context.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public static void AlertUserDialog(Context context, String message, String PositiveText) {
        final AlertDialog.Builder alertdialogbuilder = new AlertDialog.Builder(context);
        alertdialogbuilder.setMessage(message);
        alertdialogbuilder.setPositiveButton(PositiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertdialogbuilder.create();
        alertDialog.show();
    }

    public static void ShowToast(Context mContext, String s) {
        Toast.makeText(mContext, s, Toast.LENGTH_SHORT).show();
    }

    //Check the internet connection.
    public static String NetworkDetect(Context context) {
        boolean WIFI = false;
        boolean MOBILE = false;
        ConnectivityManager CM = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = CM.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            if (netInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (netInfo.isConnected()) WIFI = true;
            if (netInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (netInfo.isConnected()) MOBILE = true;
        }
        if (WIFI) {
            return GetDeviceipWiFiData(context);
        }
        if (MOBILE) {
            return GetDeviceipMobileData();
        }
        return "";
    }

    public static JSONObject getJsonRequest(String tranType, String source, String loginid, JSONObject json) {

        JSONObject jsondata = null;
        try {
            jsondata = new JSONObject();
            jsondata.put("TRANTYPE", tranType);
            jsondata.put("SOURCEID", source);
            jsondata.put("LOGINID", loginid);
            Encryption enc = new Encryption();

            String str4 = enc.encrypt(json.toString());
            jsondata.put("MESSAGE", str4.trim());
            String str1 = AES.decrypt(str4, AES.KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsondata;

    }
    public static JSONObject getJsonRequesthpcl(String Password) {

        JSONObject jsondata = null;
        try {
            jsondata = new JSONObject();
            jsondata.put("password", Password);
//            jsondata.put("SOURCEID", source);
//            jsondata.put("LOGINID", loginid);
            Encryption enc = new Encryption();

//            String str4 = enc.encrypt(jsondata.toString());
            String str4 = enc.encrypt_hpcl(jsondata.toString());
//            jsondata.put("MESSAGE", str4.trim());
            String str1 = AES.decrypt(str4, AES.KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsondata;

    }
    public static byte[] RSAEncrypt(final String plain) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        KeyPairGenerator  kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair kp = kpg.genKeyPair();
        PublicKey publicKey = kp.getPublic();
        PrivateKey privateKey = kp.getPrivate();

        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedBytes = cipher.doFinal(plain.getBytes());
//        System.out.println("EEncrypted?????" + new String(org.apache.commons.codec.binary.Hex.encodeHex(encryptedBytes)));
        return encryptedBytes;
    }

    public String RSADecrypt(final byte[] encryptedBytes) throws NoSuchAlgorithmException, NoSuchPaddingException,
            InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        Cipher cipher1 = Cipher.getInstance("RSA");
        KeyPairGenerator  kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(2048);
        KeyPair kp = kpg.genKeyPair();
        PrivateKey privateKey = kp.getPrivate();
        cipher1.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedBytes = cipher1.doFinal(encryptedBytes);
        String  decrypted = new String(decryptedBytes);
        System.out.println("DDecrypted?????" + decrypted);
        return decrypted;
    }
    //19-1-2018 riteshb
    public static JSONObject getJsonRequestDelink(String tranType, String source, String loginid, JSONObject json) {

        JSONObject jsondata = null;
        try {
            jsondata = new JSONObject();
            jsondata.put("TRANTYPE", tranType);
            jsondata.put("SOURCEID", source);
            jsondata.put("LOGINID", loginid);
            Encryption enc = new Encryption();

            String str4 = enc.encryptDelink(json.toString());
            jsondata.put("MESSAGE", str4.trim());
            String str1 = AES.decrypt(str4, AES.OLD_PRE_KEY);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsondata;

    }


    public static boolean containAlphanumeric(final String str) {
        byte counter = 0;
        boolean checkdigit = false, checkchar = false;
        for (int i = 0; i < str.length() && counter < 2; i++) {
            // If we find a non-digit character we return false.
            if (!checkdigit && Character.isDigit(str.charAt(i))) {
                checkdigit = true;
                counter++;
            }
            String a = String.valueOf(str.charAt(i));
            if (!checkchar && a.matches("[a-zA-Z]*")) {
                checkchar = true;
                counter++;
            }
        }
        if (checkdigit && checkchar) {
            return true;
        }
        return false;
    }


    public int checkFingerPrint(Context context) {
        if (UserDataPrefrence.getPreference(context.getString(R.string.prefrence_name),
                context, ConstantDeclaration.FP_HARDWARE, "").equalsIgnoreCase("1") &&
                UserDataPrefrence.getPreference(context.getString(R.string.prefrence_name),
                        context, ConstantDeclaration.FP_PERMISSION, "").equalsIgnoreCase("1")) {


            if (UserDataPrefrence.getPreference(context.getString(R.string.prefrence_name),
                    context, ConstantDeclaration.FP_REGISTERED, "").equalsIgnoreCase("1") &&
                    UserDataPrefrence.getPreference(context.getString(R.string.prefrence_name),
                            context, ConstantDeclaration.FP_LOCKSCREEN, "").equalsIgnoreCase("1")) {
                return 0;//zero for all parameters checked for fingerprint.
            } else {
                return 1;//machine has fingerprint but user hasn't enrolled the fingerprint.
            }
        } else {
            return 2;//no fingerprint status available
        }
    }

    public static void dialogInfo(Context context, String title, String description) {
        // custom dialog
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.dialog_info);
        dialog.setTitle("Fingerprint Setting");
        dialog.setCancelable(true);

        TextView tv_message_title = (TextView) dialog.findViewById(R.id.tv_message_title);
        tv_message_title.setText(title);

        TextView tv_message = (TextView) dialog.findViewById(R.id.tv_message);
        tv_message_title.setText(description);

        // set the custom dialog components - text, image and button
        TextView tvDone = (TextView) dialog.findViewById(R.id.tvDone);
        tvDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public static String randomSTAN() {
        String stan = "";

        SecureRandom secureRandom = new SecureRandom();
        int n = 100000 + secureRandom.nextInt(900000);
        Calendar c = Calendar.getInstance();

        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        String formattedDate = df.format(c.getTime());

        //riteshb 24-11 changed
        stan = formattedDate + n;

        return stan;
    }

    public static String strGetTranType(String strTranType) {
        String strWallet = "";

        if (strTranType.equalsIgnoreCase(TRAN_TYPE_EWALLET_TO_EWALLET)) {
            strWallet = "EWALTTOEWALT";
        } else if (strTranType.equalsIgnoreCase(TRAN_TYPE_EWALLET_TO_NONWALLET)) {
            strWallet = "EWALTTONONWALT";
        } else if (strTranType.equalsIgnoreCase(TRAN_TYPE_NONWALLET_TO_EWALLET)) {
            strWallet = "NWALTTOEWALT";
        } else if (strTranType.equalsIgnoreCase(TRAN_TYPE_NONWALLET_TO_NONWALLET)) {
            strWallet = "NWALTTONWALT";
        }
        return strWallet;
    }

    public static String amountFormatter(Double dblAmount) {
        String strformattedAmount = "0.00";

        //riteshb 17-11
//        9.1 Balance Enquiry shall show in '000 format.
        try {
            //riteshb 18-11
            //NOTE : use for localization
            //if locale is khmer then amount is 100000.45 = 100.000,45
            //which wrong
            //need intenational like 100,000.45
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat) nf;
            formatter.applyPattern("#,###.##");
//        NumberFormat formatter = new DecimalFormat("#,###.##", Locale.US);
//            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
//            formatter.setCurrency(Currency.getInstance("USD"));
            formatter.setMinimumFractionDigits(2);
            strformattedAmount = (formatter.format(dblAmount)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strformattedAmount;
    }
    public static String amountFormatter_pie(Double dblAmount) {
        String strformattedAmount = "0.00";

        //riteshb 17-11
//        9.1 Balance Enquiry shall show in '000 format.
        try {
            //riteshb 18-11
            //NOTE : use for localization
            //if locale is khmer then amount is 100000.45 = 100.000,45
            //which wrong
            //need intenational like 100,000.45
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat) nf;
            formatter.applyPattern("#,###,###,###,###");
//        NumberFormat formatter = new DecimalFormat("#,###.##", Locale.US);
//            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
//            formatter.setCurrency(Currency.getInstance("USD"));
//            formatter.setMinimumFractionDigits(2);
            strformattedAmount = (formatter.format(dblAmount)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strformattedAmount;
    }

    public static String amountFormatter_swma(Double dblAmount) {
        String strformattedAmount = "0.000";

        //riteshb 17-11
//        9.1 Balance Enquiry shall show in '000 format.
        try {
            //riteshb 18-11
            //NOTE : use for localization
            //if locale is khmer then amount is 100000.45 = 100.000,45
            //which wrong
            //need intenational like 100,000.45
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat) nf;
            formatter.applyPattern("#,###.###");
//        NumberFormat formatter = new DecimalFormat("#,###.##", Locale.US);
//            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
//            formatter.setCurrency(Currency.getInstance("USD"));
            formatter.setMinimumFractionDigits(3);
            strformattedAmount = (formatter.format(dblAmount)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strformattedAmount;
    }

    public static Double amountFormatter1(Double dblAmount) {
        Double strformattedAmount = 0.00;

        //riteshb 17-11
//        9.1 Balance Enquiry shall show in '000 format.
        try {
            //riteshb 18-11
            //NOTE : use for localization
            //if locale is khmer then amount is 100000.45 = 100.000,45
            //which wrong
            //need intenational like 100,000.45
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat) nf;
            formatter.applyPattern("#,###.##");
//        NumberFormat formatter = new DecimalFormat("#,###.##", Locale.US);
//            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
//            formatter.setCurrency(Currency.getInstance("USD"));
            formatter.setMinimumFractionDigits(2);
            strformattedAmount = Double.valueOf((formatter.format(dblAmount)));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strformattedAmount;
    }


    public static String amountFormatter_pay(Double dblAmount) {
        String strformattedAmount = "0.00";

        //riteshb 17-11
//        9.1 Balance Enquiry shall show in '000 format.
        try {
            //riteshb 18-11
            //NOTE : use for localization
            //if locale is khmer then amount is 100000.45 = 100.000,45
            //which wrong
            //need intenational like 100,000.45
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat formatter = (DecimalFormat) nf;
            formatter.applyPattern("#.##");
//        NumberFormat formatter = new DecimalFormat("#,###.##", Locale.US);
//            NumberFormat formatter = NumberFormat.getCurrencyInstance(Locale.US);
//            formatter.setCurrency(Currency.getInstance("USD"));
            formatter.setMinimumFractionDigits(2);
            strformattedAmount = (formatter.format(dblAmount)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return strformattedAmount;
    }

    public static void showAlertDialog(final String strTitle, final String msg, final Context mContext) {

        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(mContext, R.style.NotificationDialogTheme);


        LinearLayout layout = new LinearLayout(mContext);
        layout.setOrientation(LinearLayout.VERTICAL);

        TextView titleTV = new TextView(mContext);
        titleTV.setTextSize(17);
        titleTV.setText(strTitle);
        titleTV.setGravity(Gravity.CENTER);
        titleTV.setTypeface(Typeface.DEFAULT_BOLD);
        titleTV.setTextColor(mContext.getResources().getColor(R.color.black));
        LinearLayout.LayoutParams lp_new = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        int valueInPixels_twenty_one = (int) mContext.getResources().getDimension(R.dimen.padding_15);
        int valueInPixels_ten_one = (int) mContext.getResources().getDimension(R.dimen.padding_10);
        lp_new.setMargins(0, valueInPixels_ten_one, 0, 0);

//        titleTV.setPadding(0, valueInPixels_twenty_one, 0, valueInPixels_ten_one);
        titleTV.setLayoutParams(lp_new);

        builder.setView(titleTV);
        layout.addView(titleTV);


        TextView resultMessage = new TextView(mContext);
        resultMessage.setTextSize(15);

        resultMessage.setText(msg);
        resultMessage.setAllCaps(false);
        resultMessage.setGravity(Gravity.LEFT);
        resultMessage.setTextColor(mContext.getResources().getColor(R.color.grey_bg));
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        int valueInPixels_ten = (int) mContext.getResources().getDimension(R.dimen.padding_15);

        lp.setMargins(0, valueInPixels_ten, 0, valueInPixels_ten);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            resultMessage.setLetterSpacing(0.01F);
        }
        resultMessage.setPadding(valueInPixels_ten, 0, valueInPixels_ten, 0);
        resultMessage.setLayoutParams(lp);

        builder.setView(resultMessage);
        layout.addView(resultMessage);


        View view = new View(mContext);
        LinearLayout.LayoutParams lp1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                1);
        view.setBackgroundColor(mContext.getResources().getColor(R.color.black));
        lp1.setMargins(0, (int) mContext.getResources().getDimension(R.dimen.padding_5), 0,
                0);
        view.setLayoutParams(lp1);

        layout.addView(view);
//        layout.setAlpha(.4f);

        builder.setView(layout);


        //Customer

        builder.setPositiveButton(mContext.getResources().getString(R.string.dialog_ok),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                    }
                });

        androidx.appcompat.app.AlertDialog alert = builder.create();
        alert.show();

        final Button positiveButton = alert.getButton(androidx.appcompat.app.AlertDialog.BUTTON_POSITIVE);
        LinearLayout.LayoutParams positiveButtonLL = (LinearLayout.LayoutParams) positiveButton.getLayoutParams();
        positiveButton.setBackground(null);

        positiveButtonLL.width = ViewGroup.LayoutParams.MATCH_PARENT;
        positiveButton.setLayoutParams(positiveButtonLL);
    }

    public static String getTelcoDetails(String strRefDetail, String strJsonArray) {
        String strReturnObj = "";
        ArrayList<TopUpListModel> mList = new ArrayList<>();
        JSONArray jsonArray;

        try {
            jsonArray = new JSONArray(strJsonArray);
            for (int i = 0; i < jsonArray.length(); i++) {
//
                TopUpListModel objTopUpTelco = new TopUpListModel(
                        jsonArray.getJSONObject(i).getString("TELCONAME"),
                        jsonArray.getJSONObject(i).getString("TELCOCUSTID"),
                        jsonArray.getJSONObject(i).getString("TELCOID"),
                        jsonArray.getJSONObject(i).getString("LOGOPATH"),
                        jsonArray.getJSONObject(i).getString("DENOMINATIONS"),
                        jsonArray.getJSONObject(i).getString("TELCOTYPE"));
                mList.add(objTopUpTelco);
            }

            for (int i = 0; i < jsonArray.length(); i++) {
                if (strRefDetail.equalsIgnoreCase(mList.get(i).getStrTelcoID())) {
                    return jsonArray.getJSONObject(i).toString();
                }
            }
        } catch (Exception e) {
            return "fetch_again";
        }

        return strReturnObj;
    }

    private File createImageFile(Context mcontext) throws IOException {
        String timeStamp =
                new SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir =
                mcontext.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
//        imageFilePath = image.getAbsolutePath();
        return image;
    }

    public static void writePrintLogs(Exception e, String frompage) {
        try {
            String path = Environment.getExternalStorageDirectory() + "/" + "App Darapay/";

            File directory = new File(path);
            directory.mkdir();
            String fullName = path + "PrintLog.txt";
            File external = Environment.getExternalStorageDirectory();
            String sdcardPath = external.getPath();
            File file = new File(fullName);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter filewriter = new FileWriter(file, true);
            BufferedWriter out = new BufferedWriter(filewriter);

            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            String strTileText = frompage + "\n";
            strTileText += "Description : " + sw.toString() + "\n";
            out.write(strTileText);
            out.close();
            filewriter.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static void writePrintLogs(String strpage) {
        try {
            String path = Environment.getExternalStorageDirectory() + "/" + "App Darapay/";

            File directory = new File(path);
            directory.mkdir();
            String fullName = path + "PrintLog.txt";
            File external = Environment.getExternalStorageDirectory();
            String sdcardPath = external.getPath();
            File file = new File(fullName);
            if (!file.exists()) {
                file.createNewFile();
            }
            FileWriter filewriter = new FileWriter(file, true);
            BufferedWriter out = new BufferedWriter(filewriter);
            out.write(strpage + "\n");
            out.close();
            filewriter.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public static void SendDataString(String data) {
        if (Singleton.Instance().mBTService.getState() != BluetoothService.STATE_CONNECTED) {
            return;
        }
        if (data.length() > 0) {
            try {
                Singleton.Instance().mBTService.write(data.getBytes("GBK"));
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }

    /*
     * SendDataByte
     */
    public static void SendDataByte(byte[] data) {

        if (Singleton.Instance().mBTService.getState() != BluetoothService.STATE_CONNECTED) {
//            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }
        Singleton.Instance().mBTService.write(data);
    }

    //print photo
    public static void printPhoto(Bitmap img1) {
        try {
            Bitmap bmp = img1;
            if (bmp != null) {
                byte[] command = Utils.decodeBitmap(bmp);
                SendDataByte(PrinterCommands.ESC_ALIGN_CENTER);
                printText(command);
            } else {
                android.util.Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            android.util.Log.e("PrintTools", "the file isn't exists");
        }
    }
    public static void printPhoto1(Bitmap img1) {
        try {
            Bitmap bmp = img1;
            if (bmp != null) {
                byte[] command = Utils.decodeBitmap(bmp);
                SendDataByte(PrinterCommands.ESC_ALIGN_RIGHT);
                printText(command);
            } else {
                android.util.Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            android.util.Log.e("PrintTools", "the file isn't exists");
        }
    }

    //print byte[]
    public static void printText(byte[] msg) {
        try {
            // Print normal text
            SendDataByte(msg);
            SendDataByte(Command.LF);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void Print_BMP(Bitmap mBitmap, int width, int height) {

        // byte[] buffer = PrinterCommand.POS_Set_PrtInit();
        int nMode = 0;
        if (mBitmap != null) {
            /**
             * Parameters: mBitmap 要打印的图片 nWidth 打印宽度（58和80） nMode 打印模式 Returns: byte[]
             */
          /*  byte[] data = PrintPicture.POS_PrintBMP(mBitmap, width, height, nMode);
            // SendDataByte(buffer);
            SendDataByte(Command.ESC_Init);
            SendDataByte(Command.LF);
            SendDataByte(data);
            SendDataByte(PrinterCommand.POS_Set_PrtAndFeedPaper(30));
            SendDataByte(PrinterCommand.POS_Set_Cut(1));
            SendDataByte(PrinterCommand.POS_Set_PrtInit());*/
        }
    }

   /* public static void printReciept(Context mContext, Bundle bundle) {
        Log.e("PRINT:::","RUN");
        String strpage = "ClsBluetoothPrintFrag.java;printReciept();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
            Bitmap bmp = null;
            Bitmap result = null;
            //6-3-18
            //riteshb
            //added new image to non-dara to non-dara transaction
            try {

//                PrashantG 18-07-18
//                if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)
//                        || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_WATER_SUPPLY_BILL_PAY_FOR_AGENT)
//                        || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_Payroll)
//                        || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_AGENT)
//                        || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_CASHOUT)
//                        || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_LOAN_DISBURSEMENT_AGENT)
//                ) {
                    bmp = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_header_new);
//                            R.drawable.darapay_print_header_new);
                    result = Bitmap.createScaledBitmap(bmp, 261, 55, false);
                    if (bmp != null) {
//                        Print_BMP(result, 261,55);
                        printPhoto(result);
//                        ClsBTPrintHandler.mConnector.printPhoto(result);
//                        ClsBTPrintHandler.mConnector.printCustom("\n", 0, 0);
                    }
//                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            //6-3-18
            bmp = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.img_dara_print_logo);
//                    R.drawable.iv_print_header_logo);
//                    R.drawable.iv_print_header_new);
            result = Bitmap.createScaledBitmap(bmp, 378, 90, false);
            if (bmp != null) {
//                Print_BMP(result, 378, 90);
                printPhoto(result);
                sendData(mContext, bundle);
            } else {
                Log.e("Print Photo error", "the file isn't exists");
            }
        } catch (Exception e) {
            e.printStackTrace();
            strpage = "ClsBluetoothPrintFrag.java;printReceipt();";
            ConstantDeclaration.writePrintLogs(e, strpage);
        }
        strpage = "ClsBluetoothPrintFrag.java;printReceipt();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }*/

    // this will send text data to be printed by the bluetooth printer
    /*public static void sendData(Context mContext, Bundle bundle) throws IOException {
        String strpage = "ClsBluetoothPrintFrag.java;sendData();--Enter--";
        ConstantDeclaration.writePrintLogs(strpage);
        try {
//            try {
//                if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_MOBILETOPUP)) {
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                strpage = "ClsBluetoothPrintFrag.java;sendData();getarguments();";
//                ConstantDeclaration.writePrintLogs(e, strpage);
//                try {
//                    bundle.putString(CALL_FROM_TXNSUCCESS, bundle.getString("TranType"));
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                    strpage = "ClsBluetoothPrintFrag.java;sendData();getarguments();bundle.putString();";
//                    ConstantDeclaration.writePrintLogs(e, strpage);
//                }
//            }
            //riteshb 17-11
            try {
                if(bundle.getString(CALL_FROM_TXNSUCCESS)==null){
                   bundle.putString(CALL_FROM_TXNSUCCESS, "");
                }
            } catch (Exception e) {
                e.printStackTrace();
                bundle.putString(CALL_FROM_TXNSUCCESS, "");
            }
            if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_MOBILETOPUP)
                    || bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(TRAN_TYPE_NWALLET_TO_MOBILETOPUP))
            {
                String msg = "";
                msg += "--------------------------------\n";

               if (bundle.getString("TELCONAME").equalsIgnoreCase("CellCard")) {
                   Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_cell_card);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 300, 85, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 270, 62);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

                   String strCurrency = "";
                   if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                       strCurrency = "  $ ";
                   } else {
                       strCurrency = "  KHR ";
                   }
                   String strAmount = ConstantDeclaration.amountFormatter(
                           Double.parseDouble( bundle.getString("PINAMOUNT")))+ "\n";

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_value);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 45, false);

                   if (label_fund_success != null) {
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                       Command.ESC_Align[2] = 0x02;//right
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strCurrency.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                       Command.ESC_Align[2] = 0x02;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strAmount.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_code);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 50, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                           StringBuilder s;
                           s = new StringBuilder(bundle.getString("PIN"));

                           for (int i = 4; i < s.length(); i += 5) {
                               s.insert(i, " ");
                           }
                           String strpin = s.toString() + "\n";
//                            String strexpdate = " " + getActivity().getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE") + "\n";
                           Command.ESC_Align[2] = 0x01;//center
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x01;//medium font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strpin.getBytes("GBK"));
//                            ClsBTPrintHandler.mConnector.printCustom(strpin, 2, 0);
//                            ClsBTPrintHandler.mConnector.printCustom(strexpdate, 0, 0);
                       }

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_cellcard_info_1);
//                           R.drawable.img_cell_card_details);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 220, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 410,220);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   String strReceipt = "Receipt No.  : " + bundle.getString("ReceiptNoError", "") + "\n";
                   String strSerial = "Serial No.   : " + bundle.getString("SERIAL_NO", "") + "\n";
                   String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strReceipt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);


                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strSerial.getBytes("GBK"));

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strDate.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

//                    For reprint text
                   try {
                       if(bundle.getBoolean("IS_RE_PRINT")){

                           String strReprint = "Reprint \n";
                           Command.ESC_Align[2] = 0x01;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strReprint.getBytes("GBK"));

                           Command.ESC_Align[2] = 0x00;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(msg.getBytes("GBK"));
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                   }

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_cell_card_footer);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 165, false);
                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 390, 165);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }


               }else   if (bundle.getString("TELCONAME").equalsIgnoreCase("Smart")) {
                   Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_smart);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 270, 65, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 270, 62);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

                   String strCurrency = "";
                   if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                       strCurrency = "  $ ";
                   } else {
                       strCurrency = "  KHR ";
                   }
                   String strAmount = ConstantDeclaration.amountFormatter(
                           Double.parseDouble( bundle.getString("PINAMOUNT"))) + "\n";

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_value);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 45, false);

                   if (label_fund_success != null) {
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                       Command.ESC_Align[2] = 0x02;//right
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strCurrency.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                       Command.ESC_Align[2] = 0x02;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strAmount.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_code);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 50, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                           StringBuilder s;
                           s = new StringBuilder(bundle.getString("PIN"));

                           for (int i = 4; i < s.length(); i += 5) {
                               s.insert(i, " ");
                           }
                           String strpin = s.toString() + "\n";
//                            String strexpdate = " " + getActivity().getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE") + "\n";
                           Command.ESC_Align[2] = 0x01;//center
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x01;//medium font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strpin.getBytes("GBK"));
//                            ClsBTPrintHandler.mConnector.printCustom(strpin, 2, 0);
//                            ClsBTPrintHandler.mConnector.printCustom(strexpdate, 0, 0);
                       }

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_smart_detail);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 220, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 410,220);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   String strReceipt = "Receipt No.  : " + bundle.getString("ReceiptNoError", "") + "\n";
                   String strSerial = "Serial No.   : " + bundle.getString("SERIAL_NO", "") + "\n";
                   String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strReceipt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);


                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strSerial.getBytes("GBK"));

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strDate.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);


                   try {
                       if(bundle.getBoolean("IS_RE_PRINT")){

                           String strReprint = "Reprint \n";
                           Command.ESC_Align[2] = 0x01;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strReprint.getBytes("GBK"));

                           Command.ESC_Align[2] = 0x00;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(msg.getBytes("GBK"));
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                   }

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_smart_footer);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 165, false);
                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 390, 165);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

//               }else   if (bundle.getString("TELCONAME").equalsIgnoreCase("Razer")) {

               }
               else   if (bundle.getString("TELCONAME").equalsIgnoreCase(RazerTelconame))
               {
                   Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_razer_gold1);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 85, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 270, 62);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

                   String strCurrency = "";
                   if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                       strCurrency = "  $ ";
                   } else {
                       strCurrency = "  KHR ";
                   }

                   String Info = "";
                   Info = "HOW TO RELOAD RAZER GOLD" + "\n"
                           +"1.Access your RAZER GOLD account"  + "\n"+
                            "  at https://gold.razer.com with"+ "\n"
                           +"  your Razer ID and password."  + "\n" +
                            "2.In the top menu,click GOLD, " + "\n" +"" +
                            "  then click \"reload now\" and"+ "\n"
                           +"  choose Razer GOLD PIN."        + "\n"+
                            "3.Enter VOUCHER PIN number and " + "\n"+
                            "  click \"next\"." +"\n"+
                            "4.Enter the OTP sent to your  " +"\n"+
                            "  mobile and click \"CONFIRM\"." +"\n"+
                            "5.The receipt will be sent to" +"\n"+
                            "  your email which can be viewed"+"\n"+
                            "  at  https://gold.razer.com/acc"+"\n"+
                            "  ount-summary."+"\n";
                   String contactrazer="";
               contactrazer="    Contact Razer ID Support"+"\n"+
                            "   https://mysupport.razer.com"+"\n"+"\n";




                   String strAmount = ConstantDeclaration.amountFormatter(
                           Double.parseDouble( bundle.getString("PINAMOUNT"))) + "\n";

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_value);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 45, false);

                   if (label_fund_success != null) {
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                       Command.ESC_Align[2] = 0x02;//right
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strCurrency.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                       Command.ESC_Align[2] = 0x02;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strAmount.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_code);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 50, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                           StringBuilder s;
                           s = new StringBuilder(bundle.getString("PIN"));

                           for (int i = 4; i < s.length(); i += 5) {
                               s.insert(i, " ");
                           }
                           String strpin = s.toString() + "\n";
//                            String strexpdate = " " + getActivity().getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE") + "\n";
                           Command.ESC_Align[2] = 0x01;//center
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x01;//medium font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strpin.getBytes("GBK"));
//                            ClsBTPrintHandler.mConnector.printCustom(strpin, 2, 0);
//                            ClsBTPrintHandler.mConnector.printCustom(strexpdate, 0, 0);
                       }

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_razer_gold1);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 400, 220, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 410,220);
//                       printPhoto(label_fund_success);
                       if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 410,220);
//                           printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                           Command.ESC_Align[2] = 0x00;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(Info.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                       } else {
                           Log.e("Print Photo error", "the file isn't exists");
                       }

//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   String strReceipt = "Receipt No.  : " + bundle.getString("ReceiptNoError", "") + "\n";
                   String strSerial = "Serial No.   : " + bundle.getString("SERIAL_NO", "") + "\n";
                   String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strReceipt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);


                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strSerial.getBytes("GBK"));

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strDate.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);


                   try {
                       if(bundle.getBoolean("IS_RE_PRINT")){

                           String strReprint = "Reprint \n";
                           Command.ESC_Align[2] = 0x01;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strReprint.getBytes("GBK"));

                           Command.ESC_Align[2] = 0x00;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(msg.getBytes("GBK"));
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                   }

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_razer_gold1);
//                           R.drawable.img_meg_footer);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 450, 150, false);
                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 390, 165);
//                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);


//                        Print_BMP(label_fund_success, 410,220);
//                           printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                           Command.ESC_Align[2] = 0x00;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(contactrazer.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   Bitmap label_sender= BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_bottom);
                   label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                   if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                       printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

               }
               else   if (bundle.getString("TELCONAME").equalsIgnoreCase(MEGTelconame)) {
                   Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_meg_header);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 350, 120, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 270, 62);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

                   String strCurrency = "";
                   if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                       strCurrency = "  $ ";
                   } else {
                       strCurrency = "  KHR ";
                   }
                   String strAmount = ConstantDeclaration.amountFormatter(
                           Double.parseDouble( bundle.getString("PINAMOUNT"))) + "\n";

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_value);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 45, false);

                   if (label_fund_success != null) {
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                       Command.ESC_Align[2] = 0x02;//right
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strCurrency.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                       Command.ESC_Align[2] = 0x02;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strAmount.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_code);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 50, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                           StringBuilder s;
                           s = new StringBuilder(bundle.getString("PIN"));

                           for (int i = 4; i < s.length(); i += 5) {
                               s.insert(i, " ");
                           }
                           String strpin = s.toString() + "\n";
//                            String strexpdate = " " + getActivity().getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE") + "\n";
                           Command.ESC_Align[2] = 0x01;//center
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x01;//medium font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strpin.getBytes("GBK"));
//                            ClsBTPrintHandler.mConnector.printCustom(strpin, 2, 0);
//                            ClsBTPrintHandler.mConnector.printCustom(strexpdate, 0, 0);
                       }

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_meg_info);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 160, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 410,220);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   String strReceipt = "Receipt No.  : " + bundle.getString("ReceiptNoError", "") + "\n";
                   String strSerial = "Serial No.   : " + bundle.getString("SERIAL_NO", "") + "\n";
                   String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strReceipt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);


                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strSerial.getBytes("GBK"));

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strDate.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);


                   try {
                       if(bundle.getBoolean("IS_RE_PRINT")){

                           String strReprint = "Reprint \n";
                           Command.ESC_Align[2] = 0x01;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strReprint.getBytes("GBK"));

                           Command.ESC_Align[2] = 0x00;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(msg.getBytes("GBK"));
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                   }

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_meg_header);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 165, false);
                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 390, 165);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

               }

               else   if (bundle.getString("TELCONAME").equalsIgnoreCase("SEATEL")) {
                   Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_meg_header);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 270, 65, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 270, 62);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

                   String strCurrency = "";
                   if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                       strCurrency = "  $ ";
                   } else {
                       strCurrency = "  KHR ";
                   }
                   String strAmount = ConstantDeclaration.amountFormatter(
                           Double.parseDouble( bundle.getString("PINAMOUNT"))) + "\n";

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_value);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 45, false);

                   if (label_fund_success != null) {
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                       Command.ESC_Align[2] = 0x02;//right
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strCurrency.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                       Command.ESC_Align[2] = 0x02;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x10;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(strAmount.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.iv_print_topup_refill_code);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 50, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 250, 35);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                           StringBuilder s;
                           s = new StringBuilder(bundle.getString("PIN"));

                           for (int i = 4; i < s.length(); i += 5) {
                               s.insert(i, " ");
                           }
                           String strpin = s.toString() + "\n";
//                            String strexpdate = " " + getActivity().getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE") + "\n";
                           Command.ESC_Align[2] = 0x01;//center
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x01;//medium font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strpin.getBytes("GBK"));
//                            ClsBTPrintHandler.mConnector.printCustom(strpin, 2, 0);
//                            ClsBTPrintHandler.mConnector.printCustom(strexpdate, 0, 0);
                       }

                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_seatel_details);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 220, false);

                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 410,220);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                       Command.ESC_Align[2] = 0x00;//left
                       SendDataByte(Command.ESC_Align);
                       Command.GS_ExclamationMark[2] = 0x00;//small font
                       SendDataByte(Command.GS_ExclamationMark);
                       SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }
                   String strReceipt = "Receipt No.  : " + bundle.getString("ReceiptNoError", "") + "\n";
                   String strSerial = "Serial No.   : " + bundle.getString("SERIAL_NO", "") + "\n";
                   String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strReceipt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);


                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strSerial.getBytes("GBK"));

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(strDate.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                   Command.ESC_Align[2] = 0x00;//left
                   SendDataByte(Command.ESC_Align);
                   Command.GS_ExclamationMark[2] = 0x00;//small font
                   SendDataByte(Command.GS_ExclamationMark);
                   SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);


                   try {
                       if(bundle.getBoolean("IS_RE_PRINT")){

                           String strReprint = "Reprint \n";
                           Command.ESC_Align[2] = 0x01;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strReprint.getBytes("GBK"));

                           Command.ESC_Align[2] = 0x00;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(msg.getBytes("GBK"));
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                   }

                   label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                           R.drawable.img_seatel_footer);
                   label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 165, false);
                   if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 390, 165);
                       printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                   } else {
                       Log.e("Print Photo error", "the file isn't exists");
                   }

               } else {
                    //metfone
                    //print khmer header
                   String strAmount;
                    Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_metfone_logo);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 270, 62, false);

                    if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 270, 62);
                        printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    String strCurrency = "";
                    if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                        strCurrency = "  $ ";
                    } else {
                        strCurrency = "  KHR ";
                    }
                   if (UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
                           mContext, ConstantDeclaration.USERROLEPREFKEY,
                           "4").equalsIgnoreCase(CUSTOMERFLAG)) {
                       strAmount = bundle.getString("PaymentAmntReferenceNumber", "") + "\n";
                   }else{
                     strAmount = bundle.getString("PINAMOUNT", "") + "\n";
                   }

                    label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_topup_refill_value);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 45, false);

                    if (label_fund_success != null) {
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

//                        Print_BMP(label_fund_success, 250, 35);
                        printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                        Command.ESC_Align[2] = 0x02;//right
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(ConstantDeclaration.amountFormatter(
                                Double.parseDouble(strAmount.replace(" ", ""))).getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));

//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_topup_refill_code);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 280, 50, false);

                    if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 250, 35);
                        printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                        if (bundle.getBoolean("SHOW_PIN_EXP_FLAG")) {
                            StringBuilder s;
                            if(bundle.getString("CALL_FROM").equalsIgnoreCase("14")){
                                try {
                                    if (bundle.getString("from").equalsIgnoreCase("txnhistory")){
                                        s = new StringBuilder(bundle.getString("DecryptPIN"));
//                                        s = new StringBuilder(bundle.getString("DecryptPIN"));

                                    }else{
                                        s = new StringBuilder(bundle.getString("PIN"));
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    s = new StringBuilder(bundle.getString("PIN"));
                                }
                            }else{
                                s = new StringBuilder(bundle.getString("PIN"));
                            }


                            for (int i = 4; i < s.length(); i += 5) {
                                s.insert(i, " ");
                            }
                            String strpin = s.toString() + "\n";
//                            String strexpdate = " " + getActivity().getString(R.string.expiry_date) + "        : " + bundle.getString("EXPIRYDATE") + "\n";
                            Command.ESC_Align[2] = 0x01;//center
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x01;//medium font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strpin.getBytes("GBK"));
//                            ClsBTPrintHandler.mConnector.printCustom(strpin, 2, 0);
//                            ClsBTPrintHandler.mConnector.printCustom(strexpdate, 0, 0);
                        }

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_topup_details);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 410, 220, false);

                    if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 410,220);
                        printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    String strReceipt = "Receipt No.  : " + bundle.getString("ReceiptNoError", "") + "\n";
                    String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strReceipt.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strDate.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

//                    For reprint text
                   try {
                       if(bundle.getBoolean("IS_RE_PRINT")){

                           String strReprint = "Reprint \n";
                           Command.ESC_Align[2] = 0x01;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(strReprint.getBytes("GBK"));

                           Command.ESC_Align[2] = 0x00;//left
                           SendDataByte(Command.ESC_Align);
                           Command.GS_ExclamationMark[2] = 0x00;//small font
                           SendDataByte(Command.GS_ExclamationMark);
                           SendDataByte(msg.getBytes("GBK"));
                       }
                   } catch (Exception e) {
                       e.printStackTrace();
                   }

                    label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_metfone_call);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 390, 165, false);
                    if (label_fund_success != null) {
//                        Print_BMP(label_fund_success, 390, 165);
                        printPhoto(label_fund_success);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString(CALL_FROM_TXNSUCCESS).equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_CASHOUT)
            )
            {
                //Loan Disbustment

                String strAccountNo = "", strCustomerName = "", temp = "";
                String strCurrency = "", strAmount = "", strFee_value = "",strAmount1 = "", strFee_value1 = "";
                String strpayment_amount = "",strCustomerName1 = "",strCustomerMobNo ="";
                String strFIHeader = "";
//                JSONArray jsonArray = new JSONArray(bundle.getString("json"));

//                JSONObject jsonObject = jsonArray.getJSONObject(0);

                try {
                    try {


                        strAmount = bundle.getString("PaymentAmntReferenceNumber") + "\n";
                        strCustomerMobNo = bundle.getString("FromNumber") + "\n";
                        strCustomerName1 = bundle.getString("WALLETNAME") + "\n";
                        strFee_value = bundle.getString("fee");
                        strAmount1 = ConstantDeclaration.amountFormatter(
                                Double.parseDouble(strAmount.replace(" ", "")))+"\n";

                        strFIHeader = bundle.getString("BANK") + "\n";
//                        strAccountNo = jsonObject.getString("TRANSACTIONID");
//                        strCustomerName = jsonObject.getString("CUSTOMERNAME");
//                        strCurrency = jsonObject.getString("CURRENCYTYPE");
                        strCurrency = bundle.getString("CURRENCY");
                        if (strCurrency.equalsIgnoreCase("USD")) {
                            strCurrency = "  $ ";
                        } else {
                            strCurrency = "  KHR ";
                        }
//                        strAmount = jsonObject.getString("AMOUNT");
//                        strFee_value = jsonObject.getString("AGENTFEE");
//                        strFee_value = jsonObject.getString("CUSTOMERFEE");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String first3_letters = "", middle_str = "";
                    if(strAccountNo != null){
                        if(!strAccountNo.isEmpty()){
//                            first3_letters = strAccountNo.substring(0, 3);
//                            middle_str = strAccountNo.substring(3);
//                            temp =  Strings.repeat("X", middle_str.length()-2);
//                            strAccountNo = first3_letters + temp + strAccountNo.substring(strAccountNo.length()-2,strAccountNo.length() );
                            temp =  Strings.repeat("X", strAccountNo.length()-4);
                            strAccountNo = temp + strAccountNo.substring(strAccountNo.length()-4) ;



                        }
                    }

                    if(strCustomerName != null){
                        if(!strCustomerName.isEmpty()){

                            if(strCustomerName.contains(" ")){

                                strCustomerName= strCustomerName.trim();

                                temp = strCustomerName.substring(strCustomerName.lastIndexOf(' ')+2, strCustomerName.length());
                                temp =  Strings.repeat("X", temp.length());

                                strCustomerName = strCustomerName.substring(0, strCustomerName.lastIndexOf(' ')+2)+temp;

                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";



//                Command.ESC_Align[2] = 0x01;//left
//                SendDataByte(Command.ESC_Align);
//                Command.GS_ExclamationMark[2] = 0x01;//medium font
//                SendDataByte(Command.GS_ExclamationMark);
//                SendDataByte(strFIHeader.getBytes("GBK"));

//                Bitmap label_name1=drawText(mContext.getString(R.string.e_wallet_cashout_print), 378,45,  Color.BLACK);
//                label_name1 = Bitmap.createScaledBitmap(label_name1, 250, 40, false);
//                printPhoto(label_name1);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//
                //riteshb 21-11
                //String strsender = "";
                try {
                    strAccountNo += "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.tran_amt_print);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 42, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);

//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount1.getBytes("GBK"));

                    //                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                strCustomerMobNo +=  "\n";

                Bitmap label_cust_name1 = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.cust_wallet_no_print);
                label_cust_name1 = Bitmap.createScaledBitmap(label_cust_name1, 250, 40, false);
                if (label_cust_name1 != null) {
                    printPhoto(label_cust_name1);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomerMobNo.getBytes("GBK"));

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.acct_name_print);
                label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 153, 34, false);
                if (label_cust_name != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_cust_name);

//                    try{
//                        Bitmap label_name=drawText(strCustomerName, 378,45,  Color.BLACK);
//                        label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
//                        printPhoto(label_name);
//                    }catch(Exception e){
//                        e.getMessage();
//                    }

                    try {
                        char c = strCustomerName1.charAt(0);
                        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                            strCustomerName1 +=  "\n";
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strCustomerName1.getBytes("GBK"));

                        }else{
//                            Command.ESC_Align[2] = 0x00;//left
//                            SendDataByte(Command.ESC_Align);
//                            Command.GS_ExclamationMark[2] = 0x00;//small font
//                            SendDataByte(Command.GS_ExclamationMark);
//                            SendDataByte(strCustomerName.getBytes("GBK"));

                            Bitmap label_name=drawText(strCustomerName1, 378,45,  Color.BLACK);
                            label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                            printPhoto(label_name);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }


//                if (strCurrency.equalsIgnoreCase("USD")) {
//                    strCurrency = "  $ ";
//                } else {
//                    strCurrency = "  KHR ";
//                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount1.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount1, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                strFee_value1 = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(strFee_value.replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value1.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value1, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //13/11/18
                //added exchange rate if cross currency
//                try {
//                    if(!jsonObject.getString("PAYROLLCURRENCYTYPE")
//                            .equalsIgnoreCase(jsonObject.getString("CURRENCYTYPE"))){
//                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                                R.drawable.exchange_rate);
//                        label_sender = Bitmap.createScaledBitmap(label_sender, 168, 50, false);
//                        if (label_sender != null) {
////                    Print_BMP(label_sender, 250, 35);
//                            printPhoto(label_sender);
//
////                            NumberFormat formatter = new DecimalFormat("#00.00");
//                            String exchange_rate = (ConstantDeclaration.amountFormatter(Double.parseDouble(
//                                    jsonObject.getString("EXCHANGERATE")
//                                            .replace(",", "")))).toString() + "\n";
//
//                            Command.ESC_Align[2] = 0x02;//left
//                            SendDataByte(Command.ESC_Align);
//                            Command.GS_ExclamationMark[2] = 0x10;//small font
//                            SendDataByte(Command.GS_ExclamationMark);
//                            SendDataByte(exchange_rate.getBytes("GBK"));
//                        } else {
//                            Log.e("Print Photo error", "the file isn't exists");
//                        }
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }


//                Signature code
                String tempMsg = msg + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(tempMsg.getBytes("GBK"));
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.img_signature_lbl);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 300, 50, false);
//                if (label_sender != null) {
////                    Print_BMP(label_sender, 250,40);
//                    printPhoto(label_sender);
//                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + bundle.getString("ReceiptNoError") + "\n";
                String strDate = "Date/Time:" + bundle.getString("DateTime") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)
                    ) {
                //transfer

                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";

                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fund_trans_is_successful);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    //22-3-18
//                    Print_BMP(label_fund_success, 250, 35);
                    printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender = "";
                try {
                    strsender = "+855 " + bundle.getString("SenderNo") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//right
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//medium font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strsender.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strsender, 2, 0);
                    //size , align
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "+855 " + bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_receiver_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//medium font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomer.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)) {

                    String strcollection = " " + bundle.getString("collectionCode") + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_collection_code);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                        Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        Command.ESC_Align[2] = 0x01;//02-right, 01-center
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x11;//large font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strcollection.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strcollection, 3, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "  $ ";
                } else {
                    strCurrency = "  KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender,250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390, 170);
                    printPhoto(label_sender);
                    //                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_EWALLET))
            {
                //transfer

                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";

                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_dara_to_dara);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 260, 40, false);

                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 260,40);
                    printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender = "";
                try {
                    strsender = "+855 " + bundle.getString("SenderNo") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strsender.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strsender, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "+855 " + bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_dara_receiver);
                label_sender = Bitmap.createScaledBitmap(label_sender, 400, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 400, 40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomer.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

//                PrashantG 12-02-2019
                String strCustomerName = bundle.getString("WALLETNAME", "");
                strCustomerName +=  "\n";

                Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_cust_name);
                label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 250, 35, false);
                if (label_cust_name != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_cust_name);

                        Command.ESC_Align[2] = 0x01;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustomerName.getBytes("GBK"));
//                    try {
//                        Bitmap label_name = drawText(strCustomerName, 378, 45, Color.BLACK);
//                        label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
//                        printPhoto(label_name);
//                    } catch (Exception e) {
//                        e.getMessage();
//                    }
                }

//                PrashantG 12-02-2019


                    if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)) {

                    String strcollection = " " + bundle.getString("collectionCode") + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_collection_code);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                        Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x11;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strcollection.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strcollection, 3, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "  $ ";
                } else {
                    strCurrency = "  KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);
            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_WATER_SUPPLY_BILL_PAY_FOR_AGENT))
            {
                //bill payment

                if(bundle.getString("BILLERCODE").equals("4")){
                    String strBillNo = "", strCustomerName = "", strCustomerNo = "";
                    String strBillAmt = "", strUbillFee = "", strCustomerFee = "", strBillerName = "";


                    JSONObject billerDetails = new JSONObject(bundle.getString("BILLERDETAILS"));
                    JSONObject verifyDetails = new JSONObject(bundle.getString("BILLVERFIY"));
                    JSONObject successJson = new JSONObject(bundle.getString("json"));
                    String msg = "";
                    String strCurrencyType = "";

                    strBillAmt = verifyDetails.getString("AMOUNT");
                    strUbillFee = verifyDetails.getString("BILLERFEE");
                    strCustomerFee = verifyDetails.getString("CUSTOMERFEE");
                    strCurrencyType = verifyDetails.getString("CURRENCYTYPE");
                    try {
                        if(UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
                                mContext, ConstantDeclaration.LANGUAGE_SELECTED,
                                "EN").equalsIgnoreCase("EN")){
                            strBillerName = billerDetails.getString("BillerName").trim();
                        }else {
                            strBillerName = billerDetails.getString("BillerName").trim();
//                            strBillerName = billerDetails.getString("BillerName_KHR").trim();
                        }
                    } catch (Exception e12) {
                        e12.printStackTrace();
                        strBillerName = billerDetails.getString("BillerName").trim();
                    }

                    strBillNo = bundle.getString("BILLNUMBER");
                    strCustomerName = bundle.getString("CUSTOMERNAME");
                    strCustomerNo = "+855 "+ successJson.getString("CUSTOMERMOBILENO");


//                msg += "------------------------------------------\n";
                    msg += "--------------------------------\n";
                    //print khmer header
                    Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_mpwt_header_print);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 50, false);

                    if (label_fund_success != null) {
//                    Command.ESC_Align[2] = 0x00;//left
                        Command.ESC_Align[2] = 0x01;//center
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                        printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

//

                    //riteshb 21-11
                    //String strsender = "";
                    try {
                        strBillNo += "\n";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_mpwt_invoice_no);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strBillNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }


                    strCustomerName +=  "\n";

                    Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_cust_name);
                    label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 250, 35, false);
                    if (label_cust_name != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_cust_name);

//                        Command.ESC_Align[2] = 0x02;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x00;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strCustomerName.getBytes("GBK"));
                        try{
                            Bitmap label_name=drawText(strCustomerName, 378,45,  Color.BLACK);
                            label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                            printPhoto(label_name);
                        }catch(Exception e){
                            e.getMessage();
                        }



//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

//                    strCustomerNo +=  "\n";
//
//                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                            R.drawable.img_customer_no);
//                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 45, false);
//                    if (label_sender != null) {
////                    Print_BMP(label_sender, 250, 35);
//                        printPhoto(label_sender);
////                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//
//                        Command.ESC_Align[2] = 0x01;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x10;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strCustomerNo.getBytes("GBK"));
//
////                    ClsBTPrintHandler.mConnector.printCustom(strcollection, 4, 0);
//                    } else {
//                        Log.e("Print Photo error", "the file isn't exists");
//                    }


                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));

                    String strCurrency = "";
                    if (strCurrencyType.equalsIgnoreCase("USD")) {
                        strCurrency = "  $ ";
                    } else {
                        strCurrency = "  KHR ";
                    }
                    String strAmount = ConstantDeclaration.amountFormatter(
                            Double.parseDouble(strBillAmt))
                            + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_amount);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    String strFee_value = ConstantDeclaration.amountFormatter(
                            Double.parseDouble(strCustomerFee))
                            + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_fee);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
//                    String strUBillFee_value = ConstantDeclaration.amountFormatter(
//                            Double.parseDouble(strUbillFee))
//                            + "\n";
//                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                            R.drawable.ubill_fee);
////                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                    label_sender = Bitmap.createScaledBitmap(label_sender, 320, 60, false);
//                    if (label_sender != null) {
////                    Print_BMP(label_sender, 250,35);
//                        printPhoto(label_sender);
////                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//
//                        Command.ESC_Align[2] = 0x02;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x10;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strCurrency.getBytes("GBK"));
////                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
//                        Command.ESC_Align[2] = 0x02;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x10;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strUBillFee_value.getBytes("GBK"));
//                    } else {
//                        Log.e("Print Photo error", "the file isn't exists");
//                    }
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_total);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                                + Double.parseDouble(strFee_value.replace(",", ""))
                                + Double.parseDouble(strUbillFee.replace(",", ""))) + "\n\n";
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }


//                  Registration Reference number

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_reg_ref_no);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);


//                        String strRefNo = "2223335462875" + "\n";
                        String strRefNo = strBillNo + "\n";
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strRefNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strRefNo, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }



//                  Inspection Reference number

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_mpwt_inspection);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 260, 50, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

//                        String strInspectionRef =  "6548329864572"+ "\n";
                        String strInspectionRef =  successJson.getString("THIRDPARTYRRN")+ "\n";
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strInspectionRef.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strInspectionRef, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    try {
                        if (successJson.getString("TRANSFERTYPE").equalsIgnoreCase("0")) {
                            label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                    R.drawable.normal_plate_mpwt1);
                            label_sender = Bitmap.createScaledBitmap(label_sender, 260, 50, false);
                            if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                                printPhoto(label_sender);
                            }else{
                                Log.e("Print Photo error", "the file isn't exists");

                            }
                        } else if (successJson.getString("TRANSFERTYPE").equalsIgnoreCase("1")) {
                            label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                    R.drawable.personalized_plate_mpwt);
                            label_sender = Bitmap.createScaledBitmap(label_sender, 260, 50, false);
                            if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                                printPhoto(label_sender);
                            }else{
                                Log.e("Print Photo error", "the file isn't exists");

                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_agent_contact);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                                ConstantDeclaration.USER_MOBILE, "") + "\n";
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    String strReceipt = "Receipt No.: " + bundle.getString("RECEIPTNO", "") + "\n";
                    String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_bottom);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

                }  else if(bundle.getString("BILLERCODE").equals("3")){
                    String strBillNo = "", strCustomerName = "", strCustomerNo = "";
                    String strBillAmt = "", strUbillFee = "", strCustomerFee = "", strBillerName = "";


                    JSONObject billerDetails = new JSONObject(bundle.getString("BILLERDETAILS"));
                    JSONObject verifyDetails = new JSONObject(bundle.getString("BILLVERFIY"));
                    JSONObject successJson = new JSONObject(bundle.getString("json"));
                    String msg = "";
                    String strCurrencyType = "";

                    strBillAmt = verifyDetails.getString("AMOUNT");
                    strUbillFee = verifyDetails.getString("BILLERFEE");
                    strCustomerFee = verifyDetails.getString("CUSTOMERFEE");
                    strCurrencyType = verifyDetails.getString("CURRENCYTYPE");
                    try {
//                        PrashantG 21-02-2019
                        if (successJson.getString("SUPPLIER_NAME").isEmpty()) {
                            if(UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
                                    mContext, ConstantDeclaration.LANGUAGE_SELECTED,
                                    "EN").equalsIgnoreCase("EN")){
                                strBillerName = billerDetails.getString("BillerName").trim();
                            }else {
                                strBillerName = billerDetails.getString("BillerName").trim();
    //                            strBillerName = billerDetails.getString("BillerName_KHR").trim();
                            }
                        } else {
                            strBillerName = successJson.getString("SUPPLIER_NAME").trim();
                        }
//                        PrashantG 21-02-2019
                    } catch (Exception e12) {
                        e12.printStackTrace();
                        strBillerName = billerDetails.getString("BillerName").trim();
                    }

                    strBillNo = bundle.getString("BILLNUMBER");
                    strCustomerName = bundle.getString("CUSTOMERNAME");
                    strCustomerNo = "+855 "+ successJson.getString("CUSTOMERMOBILENO");


//                msg += "------------------------------------------\n";
                    msg += "--------------------------------\n";
                    //print khmer header
                    Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_successfull_bill_pay);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 45, false);

                    if (label_fund_success != null) {
//                    Command.ESC_Align[2] = 0x00;//left
                        Command.ESC_Align[2] = 0x01;//center
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                        printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    //print water supply bill payment khmer header
                    Bitmap label_header = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.ubill_biller_name);
                    label_header = Bitmap.createScaledBitmap(label_header, 250, 35, false);
                    if (label_header != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_header);
                        strBillerName+= "\n";
                        try {
//                            if(UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
//                                    mContext, ConstantDeclaration.LANGUAGE_SELECTED,
//                                    "EN").equalsIgnoreCase("EN")){
//                                Command.ESC_Align[2] = 0x02;//left
//                                SendDataByte(Command.ESC_Align);
//                                Command.GS_ExclamationMark[2] = 0x00;//small font
//                                SendDataByte(Command.GS_ExclamationMark);
//                                SendDataByte(strBillerName.getBytes("GBK"));
//                            }else{
//                                Command.ESC_Align[2] = 0x02;//left
//                                SendDataByte(Command.ESC_Align);
//                                Command.GS_ExclamationMark[2] = 0x00;//small font
//                                SendDataByte(Command.GS_ExclamationMark);
//                                SendDataByte(strBillerName.getBytes("GBK"));
////                                try{
////                                    Bitmap label_name=drawText(strBillerName, 378,45,  Color.BLACK);
////                                    label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
////                                    printPhoto(label_name);
////                                }catch(Exception e){
////                                    e.getMessage();
////                                }
//                            }

                                    Bitmap label_name=drawText(strBillerName, 390,45,  Color.BLACK);
                                    label_name = Bitmap.createScaledBitmap(label_name, 280, 48, false);
                                    printPhoto(label_name);


                        } catch (Exception e) {
                            e.printStackTrace();
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x00;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strBillerName.getBytes("GBK"));
                        }

//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    }else {
                        Log.e("Print Header Photo error", "the file isn't exists");
                    }


                    //print water supply bill payment khmer text
                     *//*   Bitmap label_header_title = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.img_invoice_text);
                        label_header_title = Bitmap.createScaledBitmap(label_header_title, 378, 40, false);
                        if (label_header_title != null) {
//                    Print_BMP(label_sender, 250,35);
                            printPhoto(label_header_title);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        }else {
                            Log.e("Print Header Photo error", "the file isn't exists");
                        }*//*




                    //riteshb 21-11
                    //String strsender = "";
                    try {
                        strBillNo += "\n";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_bill_no);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strBillNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }


                    strCustomerName +=  "\n";

                    Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_cust_name);
                    label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 250, 35, false);
                    if (label_cust_name != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_cust_name);

//                        Command.ESC_Align[2] = 0x02;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x00;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strCustomerName.getBytes("GBK"));
                            try{
                                Bitmap label_name=drawText(strCustomerName, 378,45,  Color.BLACK);
                                label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                                printPhoto(label_name);
                            }catch(Exception e){
                                e.getMessage();
                            }



//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    strCustomerNo +=  "\n";

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_customer_no);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 45, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x01;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustomerNo.getBytes("GBK"));

//                    ClsBTPrintHandler.mConnector.printCustom(strcollection, 4, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));

                    String strCurrency = "";
                    if (strCurrencyType.equalsIgnoreCase("USD")) {
                        strCurrency = "  $ ";
                    } else {
                        strCurrency = "  KHR ";
                    }
                    String strAmount = ConstantDeclaration.amountFormatter(
                            Double.parseDouble(strBillAmt))
                            + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_amount);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    String strFee_value = ConstantDeclaration.amountFormatter(
                            Double.parseDouble(strCustomerFee))
                            + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_fee);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }


//                    String strUBillFee_value = ConstantDeclaration.amountFormatter(
//                            Double.parseDouble(strUbillFee))
//                            + "\n";
//                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                            R.drawable.ubill_fee);
////                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                    label_sender = Bitmap.createScaledBitmap(label_sender, 320, 60, false);
//                    if (label_sender != null) {
////                    Print_BMP(label_sender, 250,35);
//                        printPhoto(label_sender);
////                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//
//                        Command.ESC_Align[2] = 0x02;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x10;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strCurrency.getBytes("GBK"));
////                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
//                        Command.ESC_Align[2] = 0x02;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x10;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strUBillFee_value.getBytes("GBK"));
//                    } else {
//                        Log.e("Print Photo error", "the file isn't exists");
//                    }




                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_total);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                                + Double.parseDouble(strFee_value.replace(",", ""))
                                + Double.parseDouble(strUbillFee.replace(",", ""))) + "\n";
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_agent_contact);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                                ConstantDeclaration.USER_MOBILE, "") + "\n";
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    String strReceipt = "Receipt No.: " + bundle.getString("RECEIPTNO", "") + "\n";
                    String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_bottom);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);



                }else if(bundle.getString("BILLERCODE").equals("2")||
                        bundle.getString("BILLERCODE").equals("5")){
                        String strBillNo = "", strBillNo1 = "",strCustomerName = "", strCustomerNo = "";
                        String strBillAmt = "", strUbillFee = "", strCustomerFee = "", strBillerName = "",strCustomerID = "";



                        JSONObject billerDetails = new JSONObject(bundle.getString("BILLERDETAILS"));
                        JSONObject verifyDetails = new JSONObject(bundle.getString("BILLVERFIY"));
                        JSONObject successJson = new JSONObject(bundle.getString("json"));
                        String msg = "";
                        String strCurrencyType = "";

                    strBillAmt = verifyDetails.getString("BILLPAYMENTAMOUNT");
                    strUbillFee = verifyDetails.getString("BILLERFEE");
                    strCustomerFee = verifyDetails.getString("CUSTOMERFEE");
                    strCurrencyType = verifyDetails.getString("CURRENCYTYPE");
                    strCustomerID = verifyDetails.getString("CUSTOMER_ID");
                    //change to supplier name
//                    strBillerName = billerDetails.getString("BillerName").trim();
                    try {
                        if(UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
                                mContext, ConstantDeclaration.LANGUAGE_SELECTED,
                                "EN").equalsIgnoreCase("EN")){
                            strBillerName = successJson.getString("SUPPLIER_NAME").trim();
                        }else {
                            strBillerName = successJson.getString("SUPPLIER_NAME").trim();
                        }
                    } catch (Exception e12) {
                        e12.printStackTrace();
                        strBillerName = successJson.getString("SUPPLIER_NAME").trim();
                    }
                    strBillNo = bundle.getString("BILLNUMBER");
                    strBillNo1 = bundle.getString("CUSTOMERNUMBER");
                    strCustomerName = bundle.getString("CUSTOMERNAME");
                    strCustomerNo = "+855 "+ successJson.getString("CUSTOMERMOBILENO");


//                msg += "------------------------------------------\n";
                        msg += "--------------------------------\n";
                        //print khmer header
                    if(bundle.getString("BILLERCODE").equals("5")) {
                        Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
//                                R.drawable.solid_waste_fi_name);
                                R.drawable.img_solid_waste_bill2);
                        label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 385, 40, false);

                        if (label_fund_success != null) {
//                    Command.ESC_Align[2] = 0x00;//left
                            Command.ESC_Align[2] = 0x01;//center
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x00;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                            printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                            Command.ESC_Align[2] = 0x00;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x00;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }
                    }else{
                        Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.img_successfull_bill_pay);
                        label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 45, false);

                        if (label_fund_success != null) {
//                    Command.ESC_Align[2] = 0x00;//left
                            Command.ESC_Align[2] = 0x01;//center
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x00;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                            printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                            Command.ESC_Align[2] = 0x00;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x00;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }
                    }
                        //print water supply bill payment khmer header
                        Bitmap label_header = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.ubill_biller_name);
                        label_header = Bitmap.createScaledBitmap(label_header, 250, 35, false);
                        if (label_header != null) {
//                    Print_BMP(label_sender, 250,35);
                            printPhoto(label_header);
                            strBillerName+= "\n";

                            try {
//                                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
//                                        R.drawable.solid_waste_fi_name);
//                                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 45, false);
//
//                                if (label_fund_success != null) {
//
//                                    printPhoto(label_fund_success);
////
//                                } else {
//                                    Log.e("Print Photo error", "the file isn't exists");
//                                }
                                if(bundle.getString("BILLERCODE").equals("5")) {
                                    Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
//                                R.drawable.solid_waste_fi_name);
                                            R.drawable.smwa);
                                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 380, 55, false);

                                    if (label_fund_success != null) {

//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                                        printPhoto(label_fund_success);

                                    } else {
                                        Log.e("Print Photo error", "the file isn't exists");
                                    }
                                }else{
                                    Bitmap label_name=drawText(strBillerName, 390,45,  Color.BLACK);
                                    label_name = Bitmap.createScaledBitmap(label_name, 280, 48, false);
                                    printPhoto(label_name);
                                }
//                                if(UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
//                                        mContext, ConstantDeclaration.LANGUAGE_SELECTED,
//                                        "EN").equalsIgnoreCase("EN")){
//                                    Command.ESC_Align[2] = 0x02;//left
//                                    SendDataByte(Command.ESC_Align);
//                                    Command.GS_ExclamationMark[2] = 0x00;//small font
//                                    SendDataByte(Command.GS_ExclamationMark);
//                                    SendDataByte(strBillerName.getBytes("GBK"));
//                                }else{
//                                    Command.ESC_Align[2] = 0x02;//left
//                                    SendDataByte(Command.ESC_Align);
//                                    Command.GS_ExclamationMark[2] = 0x00;//small font
//                                    SendDataByte(Command.GS_ExclamationMark);
//                                    SendDataByte(strBillerName.getBytes("GBK"));
//                                }
                            } catch (Exception e12) {
                                e12.printStackTrace();
                                Command.ESC_Align[2] = 0x02;//left
                                SendDataByte(Command.ESC_Align);
                                Command.GS_ExclamationMark[2] = 0x00;//small font
                                SendDataByte(Command.GS_ExclamationMark);
                                SendDataByte(strBillerName.getBytes("GBK"));
                            }

//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        }else {
                            Log.e("Print Header Photo error", "the file isn't exists");
                        }


                        //print water supply bill payment khmer text
                     *//*   Bitmap label_header_title = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.img_invoice_text);
                        label_header_title = Bitmap.createScaledBitmap(label_header_title, 378, 40, false);
                        if (label_header_title != null) {
//                    Print_BMP(label_sender, 250,35);
                            printPhoto(label_header_title);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        }else {
                            Log.e("Print Header Photo error", "the file isn't exists");
                        }*//*
                        //riteshb 21-11
                        //String strsender = "";
                        try {
                            strBillNo += "\n";
                            strBillNo1 += "\n";
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.img_bill_no);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                            printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                            if(bundle.getString("BILLERCODE").equals("5")) {
                                Command.ESC_Align[2] = 0x02;//left
                                SendDataByte(Command.ESC_Align);
                                Command.GS_ExclamationMark[2] = 0x10;//small font
                                SendDataByte(Command.GS_ExclamationMark);
                                SendDataByte(strBillNo1.getBytes("GBK"));
                            }else{
                                Command.ESC_Align[2] = 0x02;//left
                                SendDataByte(Command.ESC_Align);
                                Command.GS_ExclamationMark[2] = 0x10;//small font
                                SendDataByte(Command.GS_ExclamationMark);
                                SendDataByte(strBillNo.getBytes("GBK"));
                            }
//                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }


                        strCustomerName +=  "\n";
//                    if(bundle.getString("BILLERCODE").equals("5")) {
                     try{
                         Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                                 R.drawable.img_cust_name);
                         label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 250, 35, false);
                         if (label_cust_name != null) {
//                    Print_BMP(label_sender, 250, 35);
                             printPhoto(label_cust_name);
                         }

                                Bitmap label_name=drawText1(strCustomerName, 378,85,  Color.BLACK);
                                label_name = Bitmap.createScaledBitmap(label_name, 320, 80, false);
                                printPhoto(label_name);
                            }catch(Exception e){
                                e.getMessage();
                            }

//                    }

//                    else{
//                        Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
//                                R.drawable.img_cust_name);
//                        label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 250, 35, false);
//                        if (label_cust_name != null) {
////                    Print_BMP(label_sender, 250, 35);
//                            printPhoto(label_cust_name);
//
//                            Command.ESC_Align[2] = 0x02;//left
//                            SendDataByte(Command.ESC_Align);
//                            Command.GS_ExclamationMark[2] = 0x00;//small font
//                            SendDataByte(Command.GS_ExclamationMark);
//                            SendDataByte(strCustomerName.getBytes("GBK"));
////                            try{
////                                Bitmap label_name=drawText(strCustomerName, 378,45,  Color.BLACK);
////                                label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
////                                printPhoto(label_name);
////                            }catch(Exception e){
////                                e.getMessage();
////                            }
//
//
////                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
//                        } else {
//                            Log.e("Print Photo error", "the file isn't exists");
//                        }
//       }
                        strCustomerNo +=  "\n";

                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.img_customer_no);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 250, 45, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                            printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                            if(bundle.getString("BILLERCODE").equals("5")) {

                                Command.ESC_Align[2] = 0x01;//left
                                SendDataByte(Command.ESC_Align);
                                Command.GS_ExclamationMark[2] = 0x10;//small font
                                SendDataByte(Command.GS_ExclamationMark);
                                SendDataByte(strCustomerID.getBytes("GBK"));
                            }else{
                                Command.ESC_Align[2] = 0x01;//left
                                SendDataByte(Command.ESC_Align);
                                Command.GS_ExclamationMark[2] = 0x10;//small font
                                SendDataByte(Command.GS_ExclamationMark);
                                SendDataByte(strCustomerNo.getBytes("GBK"));
                            }

//                    ClsBTPrintHandler.mConnector.printCustom(strcollection, 4, 0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));

                        String strCurrency = "";
                        if (strCurrencyType.equalsIgnoreCase("USD")) {
                            strCurrency = "  $ ";
                        } else {
                            strCurrency = "  KHR ";
                        }
                    String strAmount = "";
                    if(bundle.getString("BILLERCODE").equals("5")){
                        strAmount = ConstantDeclaration.amountFormatter_swma(
                                Double.parseDouble(strBillAmt)) + "\n";

                    }else{
                        strAmount = ConstantDeclaration.amountFormatter(
                                Double.parseDouble(strBillAmt)) + "\n";
                    }

//                        String strAmount = ConstantDeclaration.amountFormatter(
//                                Double.parseDouble(strBillAmt))
//                                + "\n";
                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.iv_print_amount);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                            printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }
                    String strFee_value;
                    if(bundle.getString("BILLERCODE").equals("5")) {
                        strFee_value = ConstantDeclaration.amountFormatter_swma(
                                Double.parseDouble(successJson.getString("CUSTOMERFEE")))
//                                ConstantDeclaration.amountFormatter(
//                                Double.parseDouble(bundle.getString("ProcessingFee")))
                                + "\n";

                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.iv_print_fee);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                            printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }

                    }else {
                        strFee_value = ConstantDeclaration.amountFormatter(
                                Double.parseDouble(successJson.getString("CUSTOMERFEE")))
                                + "\n";

                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.iv_print_fee);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                            printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }
                    }
//    Removed Ubill fee as per DPS 158 mail on 14-01-19 by sachin Rathod

//                    String strUBillFee_value = ConstantDeclaration.amountFormatter(
//                            Double.parseDouble(strUbillFee))
//                            + "\n";
//                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                            R.drawable.ubill_fee);
////                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                    label_sender = Bitmap.createScaledBitmap(label_sender, 320, 60, false);
//                    if (label_sender != null) {
////                    Print_BMP(label_sender, 250,35);
//                        printPhoto(label_sender);
////                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//
//                        Command.ESC_Align[2] = 0x02;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x10;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strCurrency.getBytes("GBK"));
////                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
//                        Command.ESC_Align[2] = 0x02;//left
//                        SendDataByte(Command.ESC_Align);
//                        Command.GS_ExclamationMark[2] = 0x10;//small font
//                        SendDataByte(Command.GS_ExclamationMark);
//                        SendDataByte(strUBillFee_value.getBytes("GBK"));
//                    } else {
//                        Log.e("Print Photo error", "the file isn't exists");
//                    }

//    Removed Ubill fee as per DPS 158 mail on 14-01-19 by sachin Rathod



                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.iv_print_total);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                            printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                           if(bundle.getString("BILLERCODE").equals("5")){
                                String tot = ConstantDeclaration.amountFormatter_swma(Double.parseDouble(strAmount.replace(",", ""))
                                        + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                                Command.ESC_Align[2] = 0x02;//left
                                SendDataByte(Command.ESC_Align);
                                Command.GS_ExclamationMark[2] = 0x10;//small font
                                SendDataByte(Command.GS_ExclamationMark);
                                SendDataByte(tot.getBytes("GBK"));
                            }else{
                               String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                                       + Double.parseDouble(strFee_value.replace(",", ""))
//                                       + Double.parseDouble(strUbillFee.replace(",", ""))
                               ) + "\n";
                               Command.ESC_Align[2] = 0x02;//left
                               SendDataByte(Command.ESC_Align);
                               Command.GS_ExclamationMark[2] = 0x10;//small font
                               SendDataByte(Command.GS_ExclamationMark);
                               SendDataByte(tot.getBytes("GBK"));
                           }
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }

//         Remaining Balance
//                    try {
//                        if(bundle.getString("BILLERCODE").equals("5")&&
//                                bundle.getBoolean("ALLOW_EDIT_PAYMENT")
//                                || bundle.getBoolean("ALLOW_EXCEED_PAYMENT")
//                                || bundle.getBoolean("ALLOW_PARTIAL_PAYMENT")) {
//                            label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                                    R.drawable.remaining_bal);
//                            label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
//                            if (label_sender != null) {
//    //                    Print_BMP(label_sender, 250, 35);
//                                printPhoto(label_sender);
//    //                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//
//                                Command.ESC_Align[2] = 0x02;//left
//                                SendDataByte(Command.ESC_Align);
//                                Command.GS_ExclamationMark[2] = 0x10;//small font
//                                SendDataByte(Command.GS_ExclamationMark);
//                                SendDataByte(strCurrency.getBytes("GBK"));
//    //                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
//                               String  StrRemaining_Bal = ConstantDeclaration.amountFormatter_swma(
//                                        Double.parseDouble(successJson.getString("PAYROLLAMOUNT")))
//    //                                ConstantDeclaration.amountFormatter(
//    //                                Double.parseDouble(bundle.getString("ProcessingFee")))
//                                        + "\n";
//                                String tot = ConstantDeclaration.amountFormatter_swma(Double.parseDouble(strAmount.replace(",", ""))
//                                        + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
//                                Command.ESC_Align[2] = 0x02;//left
//                                SendDataByte(Command.ESC_Align);
//                                Command.GS_ExclamationMark[2] = 0x10;//small font
//                                SendDataByte(Command.GS_ExclamationMark);
//                                SendDataByte(StrRemaining_Bal.getBytes("GBK"));
//
//    //                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
//                            } else {
//                                Log.e("Print Photo error", "the file isn't exists");
//                            }
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    } catch (NumberFormatException e) {
//                        e.printStackTrace();
//                    }
//            Remaining Balance

                    Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.iv_print_agent_contact);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                            printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                            String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                                    ConstantDeclaration.USER_MOBILE, "") + "\n";
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }

                        String strReceipt = "Receipt No.: " + bundle.getString("RECEIPTNO", "") + "\n";
                        String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.iv_print_bottom);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                            printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);



                }else if(bundle.getString("BILLERCODE").equals("1")){
                    String strBillNo = "", strCustomerName = "", strCustomerNo = "";

                    strBillNo = bundle.getString("BILLNUMBER");
                    strCustomerName = bundle.getString("CUSTOMERNAME");
                    strCustomerNo = bundle.getString("CUSTOMERNUMBER");

                    String msg = "";
//                msg += "------------------------------------------\n";
                    msg += "--------------------------------\n";
                    //print khmer header
                    Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_successfull_bill_pay);
                    label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 45, false);

                    if (label_fund_success != null) {
//                    Command.ESC_Align[2] = 0x00;//left
                        Command.ESC_Align[2] = 0x01;//center
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                        printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                        Command.ESC_Align[2] = 0x00;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    //print water supply bill payment khmer header
                    Bitmap label_header = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_bill_pay_invoice_type);
                    label_header = Bitmap.createScaledBitmap(label_header, 250, 35, false);
                    if (label_header != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_header);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    }else {
                        Log.e("Print Header Photo error", "the file isn't exists");
                    }


                    //print water supply bill payment khmer text
                    Bitmap label_header_title = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_invoice_text);
                    label_header_title = Bitmap.createScaledBitmap(label_header_title, 378, 40, false);
                    if (label_header_title != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_header_title);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    }else {
                        Log.e("Print Header Photo error", "the file isn't exists");
                    }

                    //riteshb 21-11
                    //String strsender = "";
                    try {
                        strBillNo += "\n";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_bill_no);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strBillNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }


                    strCustomerName +=  "\n";

                    Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_cust_name);
                    label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 250, 35, false);
                    if (label_cust_name != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_cust_name);

                        try{
                            Bitmap label_name=drawText(strCustomerName, 378,45,  Color.BLACK);
                            label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                            printPhoto(label_name);
                        }catch(Exception e){
                            e.getMessage();
                        }



//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    strCustomerNo +=  "\n";

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.img_customer_no);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 45, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x01;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustomerNo.getBytes("GBK"));

//                    ClsBTPrintHandler.mConnector.printCustom(strcollection, 4, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));

                    String strCurrency = "";
                    if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                        strCurrency = "  $ ";
                    } else {
                        strCurrency = "  KHR ";
                    }
                    String strAmount = ConstantDeclaration.amountFormatter(
                            Double.parseDouble(bundle.getString("Amount", "0").replace(",", "")))
                            + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_amount);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    String strFee_value = ConstantDeclaration.amountFormatter(
                            Double.parseDouble(bundle.getString("ProcessingFee", "0").replace(",", "")))
                            + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_fee);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_total);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                        String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                                + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_agent_contact);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                        String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                                ConstantDeclaration.USER_MOBILE, "") + "\n";
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    String strReceipt = "Receipt No.: " + bundle.getString("RECEIPTNO", "") + "\n";
                    String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_bottom);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

                }

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_Payroll)
                    )
            {
                //bill payment

                String strAccountNo = "", strCustomerName = "", temp = "";
                String strCurrency = "", strAmount = "", strFee_value = "",strAmount1 = "", strFee_value1 = "";

               JSONArray jsonArray = new JSONArray(bundle.getString("json"));

               JSONObject jsonObject = jsonArray.getJSONObject(0);

                try {
                    try {
                        strAccountNo = jsonObject.getString("CUSTOMERACCOUNTNO");
                        strCustomerName = jsonObject.getString("CUSTOMERACCOUNTNAME");
                        strCurrency = jsonObject.getString("CURRENCYTYPE");
                        strAmount = jsonObject.getString("AMOUNT");
                        strFee_value = jsonObject.getString("FEE");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
String first3_letters = "", middle_str = "";
                    if(strAccountNo != null){
                        if(!strAccountNo.isEmpty()){
                            first3_letters = strAccountNo.substring(0, 3);
                            middle_str = strAccountNo.substring(3);
                            temp =  Strings.repeat("X", middle_str.length()-2);
                            strAccountNo = first3_letters + temp + strAccountNo.substring(strAccountNo.length()-2,strAccountNo.length() );
                        }
                    }

                    if(strCustomerName != null){
                        if(!strCustomerName.isEmpty()){

                            if(strCustomerName.contains(" ")){

                                strCustomerName= strCustomerName.trim();

                                temp = strCustomerName.substring(strCustomerName.lastIndexOf(' ')+2, strCustomerName.length());
                                temp =  Strings.repeat("X", temp.length());

                                strCustomerName = strCustomerName.substring(0, strCustomerName.lastIndexOf(' ')+2)+temp;


                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_payroll_header);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 45, false);

                if (label_fund_success != null) {
//                    Command.ESC_Align[2] = 0x00;//left
                    Command.ESC_Align[2] = 0x01;//center
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                    printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                //print water supply bill payment khmer header
//                Bitmap label_header = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.img_bill_pay_invoice_type);
//                label_header = Bitmap.createScaledBitmap(label_header, 250, 35, false);
//                if (label_header != null) {
////                    Print_BMP(label_sender, 250,35);
//                    printPhoto(label_header);
////                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                }else {
//                    Log.e("Print Header Photo error", "the file isn't exists");
//                }
//
//
//                //print water supply bill payment khmer text
//                Bitmap label_header_title = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.img_invoice_text);
//                label_header_title = Bitmap.createScaledBitmap(label_header_title, 378, 40, false);
//                if (label_header_title != null) {
////                    Print_BMP(label_sender, 250,35);
//                    printPhoto(label_header_title);
////                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                }else {
//                    Log.e("Print Header Photo error", "the file isn't exists");
//                }

                //riteshb 21-11
                //String strsender = "";
                try {
                    strAccountNo += "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_acc_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAccountNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }


                strCustomerName +=  "\n";

                Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_cust_name);
                label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 250, 35, false);
                if (label_cust_name != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_cust_name);

//                    try{
//                        Bitmap label_name=drawText(strCustomerName, 378,45,  Color.BLACK);
//                        label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
//                        printPhoto(label_name);
//                    }catch(Exception e){
//                        e.getMessage();
//                    }

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomerName.getBytes("GBK"));

//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

//                            strCustomerNo +=  "\n";
//
//                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.img_customer_no);
//                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 45, false);
//                if (label_sender != null) {
////                    Print_BMP(label_sender, 250, 35);
//                    printPhoto(label_sender);
////                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//
//                    Command.ESC_Align[2] = 0x01;//left
//                    SendDataByte(Command.ESC_Align);
//                    Command.GS_ExclamationMark[2] = 0x10;//small font
//                    SendDataByte(Command.GS_ExclamationMark);
//                    SendDataByte(strCustomerNo.getBytes("GBK"));
//
////                    ClsBTPrintHandler.mConnector.printCustom(strcollection, 4, 0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }
//                Command.ESC_Align[2] = 0x00;//left
//                SendDataByte(Command.ESC_Align);
//                Command.GS_ExclamationMark[2] = 0x00;//small font
//                SendDataByte(Command.GS_ExclamationMark);
//                SendDataByte(msg.getBytes("GBK"));
//

                if (strCurrency.equalsIgnoreCase("USD")) {
                    strCurrency = "  $ ";
                } else {
                    strCurrency = "  KHR ";
                }
                strAmount1 = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(strAmount.replace(" ", "")))+"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount1.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount1, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                 strFee_value1 = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(strFee_value.replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value1.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value1, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //13/11/18
                //added exchange rate if cross currency
                try {
                    if(!jsonObject.getString("PAYROLLCURRENCYTYPE")
                            .equalsIgnoreCase(jsonObject.getString("CURRENCYTYPE"))){
                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.exchange_rate);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 168, 50, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                            printPhoto(label_sender);

//                            NumberFormat formatter = new DecimalFormat("#00.00");
                            String exchange_rate = (ConstantDeclaration.amountFormatter(Double.parseDouble(
                                    jsonObject.getString("EXCHANGERATE")
                                            .replace(",", "")))).toString() + "\n";

                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(exchange_rate.getBytes("GBK"));
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + jsonObject.getString("RECEIPTNO") + "\n";
                String strDate = "Date/Time:" + jsonObject.getString("TRANSACTIONDATEFORMATED") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_LOAN_DISBURSEMENT_AGENT)
                    )
            {
                //Loan Disbustment

                String strAccountNo = "", strCustomerName = "", temp = "";
                String strCurrency = "", strAmount = "", strFee_value = "",strAmount1 = "", strFee_value1 = "";

                String strFIHeader = "";
               JSONArray jsonArray = new JSONArray(bundle.getString("json"));

               JSONObject jsonObject = jsonArray.getJSONObject(0);

                try {
                    try {
                        strFIHeader = jsonObject.getString("BANK") + "\n";
                        strAccountNo = jsonObject.getString("TRANSACTIONID");
                        strCustomerName = jsonObject.getString("CUSTOMERNAME");
                        strCurrency = jsonObject.getString("CURRENCYTYPE");
                        strAmount = jsonObject.getString("AMOUNT");
//                        strFee_value = jsonObject.getString("AGENTFEE");
                        strFee_value = jsonObject.getString("CUSTOMERFEE");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String first3_letters = "", middle_str = "";
                    if(strAccountNo != null){
                        if(!strAccountNo.isEmpty()){
//                            first3_letters = strAccountNo.substring(0, 3);
//                            middle_str = strAccountNo.substring(3);
//                            temp =  Strings.repeat("X", middle_str.length()-2);
//                            strAccountNo = first3_letters + temp + strAccountNo.substring(strAccountNo.length()-2,strAccountNo.length() );
                            temp =  Strings.repeat("X", strAccountNo.length()-4);
                            strAccountNo = temp + strAccountNo.substring(strAccountNo.length()-4) ;



                        }
                    }

                    if(strCustomerName != null){
                        if(!strCustomerName.isEmpty()){

                            if(strCustomerName.contains(" ")){

                                strCustomerName= strCustomerName.trim();

                                temp = strCustomerName.substring(strCustomerName.lastIndexOf(' ')+2, strCustomerName.length());
                                temp =  Strings.repeat("X", temp.length());

                                strCustomerName = strCustomerName.substring(0, strCustomerName.lastIndexOf(' ')+2)+temp;

                            }
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";
                Bitmap label_sender1 = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.loan_disbursement_title1);
                label_sender1 = Bitmap.createScaledBitmap(label_sender1, 250, 35, false);
                if (label_sender1 != null) {
                    printPhoto(label_sender1);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }


                    Command.ESC_Align[2] = 0x01;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x01;//medium font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFIHeader.getBytes("GBK"));

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//
                //riteshb 21-11
                //String strsender = "";
                try {
                    strAccountNo += "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_acc_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAccountNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_cust_name_loan_disb);
                label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 280, 45, false);
                if (label_cust_name != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_cust_name);

//                    try{
//                        Bitmap label_name=drawText(strCustomerName, 378,45,  Color.BLACK);
//                        label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
//                        printPhoto(label_name);
//                    }catch(Exception e){
//                        e.getMessage();
//                    }

                    try {
                        char c = strCustomerName.charAt(0);
                        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                            strCustomerName +=  "\n";
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strCustomerName.getBytes("GBK"));

                        }else{
//                            Command.ESC_Align[2] = 0x00;//left
//                            SendDataByte(Command.ESC_Align);
//                            Command.GS_ExclamationMark[2] = 0x00;//small font
//                            SendDataByte(Command.GS_ExclamationMark);
//                            SendDataByte(strCustomerName.getBytes("GBK"));

                        Bitmap label_name=drawText(strCustomerName, 378,45,  Color.BLACK);
                        label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                        printPhoto(label_name);

                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }


                if (strCurrency.equalsIgnoreCase("USD")) {
                    strCurrency = "  $ ";
                } else {
                    strCurrency = "  KHR ";
                }
                strAmount1 = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(strAmount.replace(" ", "")))+"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount1.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount1, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                 strFee_value1 = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(strFee_value.replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value1.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value1, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //13/11/18
                //added exchange rate if cross currency
                try {
                    if(!jsonObject.getString("PAYROLLCURRENCYTYPE")
                            .equalsIgnoreCase(jsonObject.getString("CURRENCYTYPE"))){
                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.exchange_rate);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 168, 50, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                            printPhoto(label_sender);

//                            NumberFormat formatter = new DecimalFormat("#00.00");
                            String exchange_rate = (ConstantDeclaration.amountFormatter(Double.parseDouble(
                                    jsonObject.getString("EXCHANGERATE")
                                            .replace(",", "")))).toString() + "\n";

                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(exchange_rate.getBytes("GBK"));
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


//                Signature code

                String tempMsg = msg + "\n\n\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(tempMsg.getBytes("GBK"));


                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_signature_lbl);
                label_sender = Bitmap.createScaledBitmap(label_sender, 300, 50, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
                }



                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + jsonObject.getString("RECEIPTNO") + "\n";
                String strDate = "Date/Time:" + jsonObject.getString("TRANSACTIONDATEFORMATED") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }

            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_NONWALLET)
                    )
            {

                //transfer

                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fund_trans_is_successful);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 250, 35, false);

                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                    printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strsender = "";
                try {
                    strsender = "+855 " + bundle.getString("FromNumber") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_sender_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strsender.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strsender, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strCustomer = "+855 " + bundle.getString("ToNumber", "") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_receiver_phone_no);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomer.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustomer, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strcollection = " " + bundle.getString("collectionCode") + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_collection_code);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x01;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x11;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strcollection.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strcollection, 4, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "  $ ";
                } else {
                    strCurrency = "  KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("fee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + bundle.getString("ReceiptNoError", "") + "\n";
                String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_AGENT)
            || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA)
            || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_BANK)
            || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_CashOut)
                    ) {
                //bill payment

//                nnkskcnks


                String  strCustomerNo = "", temp = "";
                String strCurrency = "", strAmount = "", strFee_value = "",strAmount1 = "", strFee_value1 = "";
                String strBankName = "";
//                strAccountNumber = "",replace = "",star;

                try {
//                    strBankName = bundle.getString("BankName", "");
                    strBankName = mContext.getString(R.string.acleda_bank1);
                } catch (Exception e) {
                    e.printStackTrace();
                }


                strBankName += "\n";
                JSONObject jsonObject1;


//                JSONArray jsonArray1 = new JSONArray(bundle.getString("JSONResponse"));
//                JSONObject jsonObject1 = new JSONObject(bundle.getString("JSONResponse"));
                  if(bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_AGENT)){
                      jsonObject1 = new JSONObject(bundle.getString("JSONResponse"));
                  }else{
                      jsonObject1 = new JSONObject(bundle.getString("VerifyResopnse"));
                  }
                String strAccountNumber = jsonObject1.getString("CUSTOMERACCOUNTNO");
//                String  replace1 = strAccountNumber.substring(4, 10);
                String  replace = strAccountNumber.substring(0, 3);
                String  replace2 = strAccountNumber.substring(11, 14);
                String star ="XXXXXXXX";
//                String star1 =replace1.replace(replace1,star);

//                strAccountNumber.substring(4, 10).replace(replace,star);
                String AccN = replace + star+ replace2 + "\n";
                JSONArray jsonArray = new JSONArray(bundle.getString("json"));

                JSONObject jsonObject = jsonArray.getJSONObject(0);

               if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA)){
                   strCurrency = jsonObject1.getString("CURRENCYTYPE");
                   strAmount = jsonObject1.getString("AMOUNT");
                   strFee_value = jsonObject1.getString("CUSTOMERFEE");
                   strCustomerNo = "+855 " + jsonObject1.getString("CUSTOMERMOBILENO");

                }else{
                    try {
                        try {
                            strCurrency = jsonObject1.getString("CURRENCYTYPE");
                            strAmount = jsonObject1.getString("AMOUNT");
                            strFee_value = jsonObject1.getString("CUSTOMERFEE");
//                            strAccountNumber = jsonObject.getString("ACCOUNTNO");
//                            replace = strAccountNumber.substring(4, 10);
//                            star ="********";
//                            replace.replaceAll(replace,star);
                            String strCustomerNo1= "+855 "+ jsonObject1.getString("CUSTOMERMOBILENO");
                            if(strCustomerNo1.length() == 5
                                    ||strCustomerNo1.length() <= 9){
                                strCustomerNo1 = "";
                            }
                            if(jsonObject1.getString("CUSTOMERMOBILENO") != null){
                                strCustomerNo = strCustomerNo1;
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }


                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";
                //print khmer header
                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_acleda_header);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 45, false);

                if (label_fund_success != null) {
//                    Command.ESC_Align[2] = 0x00;//left
                    Command.ESC_Align[2] = 0x01;//center
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 250,35);
                    printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }



                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_acleda_bank_name);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);

                Bitmap label_sender1 = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.acleda_bank_plc);
                label_sender1 = Bitmap.createScaledBitmap(label_sender1, 370, 48, false);

                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
                    printPhoto(label_sender1);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);


//                    try {
//                        char c = strBankName.charAt(0);
//                        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
//                            strBankName +=  "\n";
//                            Command.ESC_Align[2] = 0x02;//left
//                            SendDataByte(Command.ESC_Align);
//                            Command.GS_ExclamationMark[2] = 0x10;//small font
//                            SendDataByte(Command.GS_ExclamationMark);
//                            SendDataByte(strBankName.getBytes("GBK"));
//
//                        }else{
////                            Command.ESC_Align[2] = 0x00;//left
////                            SendDataByte(Command.ESC_Align);
////                            Command.GS_ExclamationMark[2] = 0x00;//small font
////                            SendDataByte(Command.GS_ExclamationMark);
////                            SendDataByte(strCustomerName.getBytes("GBK"));
//
//                            Bitmap label_name=drawText1("        "+strBankName, 378,45,  Color.BLACK);
//                            label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
//                            printPhoto1(label_name);
//
//                        }
//
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }

//                    Command.ESC_Align[2] = 0x02;//left
//                    SendDataByte(Command.ESC_Align);
//                    Command.GS_ExclamationMark[2] = 0x10;//small font
//                    SendDataByte(Command.GS_ExclamationMark);
//                    SendDataByte(strBankName.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
//                Bitmap label_sender1 = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.img_acleda_bank_name);
//                label_sender1 = Bitmap.createScaledBitmap(label_sender1, 250, 35, false);
//                if (label_sender1 != null) {
////                    Print_BMP(label_sender, 250,35);
//                    printPhoto(label_sender1);
////                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_acc_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(AccN.getBytes("GBK"));
////                    ClsBTPrintHandler.mConnector.printCustom(strBillNo, 2, 0);
//                } else {
//                    Log.e("Print Photo error", "the file isn't exists");
//                }

                }
                strCustomerNo +=  "\n";

                Bitmap label_cust_name = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_acleda_customer_no);
                label_cust_name = Bitmap.createScaledBitmap(label_cust_name, 250, 35, false);
                if (label_cust_name != null) {
                    printPhoto(label_cust_name);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustomerNo.getBytes("GBK"));

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                if (strCurrency.equalsIgnoreCase("USD")) {
                    strCurrency = "  $ ";
                } else {
                    strCurrency = "  KHR ";
                }
                strAmount1 = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(strAmount.replace(" ", "")))+"\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount1.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount1, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                strFee_value1 = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(strFee_value.replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value1.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value1, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //13/11/18
                //added exchange rate if cross currency
                try {
                    if(!jsonObject.getString("PAYROLLCURRENCYTYPE")
                            .equalsIgnoreCase(jsonObject.getString("CURRENCYTYPE"))){
                        label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                                R.drawable.exchange_rate);
                        label_sender = Bitmap.createScaledBitmap(label_sender, 168, 50, false);
                        if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                            printPhoto(label_sender);

//                            NumberFormat formatter = new DecimalFormat("#00.00");
                            String exchange_rate = (ConstantDeclaration.amountFormatter(Double.parseDouble(
                                    jsonObject.getString("EXCHANGERATE")
                                            .replace(",", "")))).toString() + "\n";

                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(exchange_rate.getBytes("GBK"));
                        } else {
                            Log.e("Print Photo error", "the file isn't exists");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);dp_to_ac_cashout
             if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_BANK)){
                 Bitmap label_sender20 = BitmapFactory.decodeResource(mContext.getResources(),
                         R.drawable.non_dara_to_cashout1);
                 label_sender20 = Bitmap.createScaledBitmap(label_sender20, 350, 40, false);
                 if (label_sender20 != null) {
//                    Print_BMP(label_sender, 250,40);
                     printPhoto(label_sender20);
                 }
                 Command.ESC_Align[2] = 0x00;//left
                 SendDataByte(Command.ESC_Align);
                 Command.GS_ExclamationMark[2] = 0x00;//small font
                 SendDataByte(Command.GS_ExclamationMark);
                 SendDataByte(msg.getBytes("GBK"));
                }
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);

                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + jsonObject.getString("RECEIPTNO") + "\n";
                String strDate = "Date/Time:" + jsonObject.getString("TRANSACTIONDATEFORMATED") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA)
                    || bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA_CP)
                    )
            {
//transfer

                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";
                //print khmer header
//                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_bank_header);
//                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 260, 40, false);

                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_acleda_header);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 45, false);


                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 260,40);
                    printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strBankName = "";
                try {
                    strBankName = bundle.getString("BankName") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bank_name);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strBankName.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBankName, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                JSONArray jsonArray = new JSONArray(bundle.getString("json"));
                JSONObject jsonObject = jsonArray.getJSONObject(0);

                String strCustAccName = "";
                if (jsonObject.getString("ACCOUNTNAME").length() <= 18) {
                    strCustAccName = jsonObject.getString("ACCOUNTNAME") + "\n";
                } else {
                    strCustAccName = jsonObject.getString("ACCOUNTNAME") + "\n";
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bank_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

//                    PrashantG 07-05-2019

//                    Command.ESC_Align[2] = 0x02;//left
//                    SendDataByte(Command.ESC_Align);
//                    Command.GS_ExclamationMark[2] = 0x00;//small font
//                    SendDataByte(Command.GS_ExclamationMark);
//                    SendDataByte(strCustAccName.getBytes("GBK"));

                    try {
                        char c = strCustAccName.charAt(0);
                        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                            strCustAccName +=  "\n";
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strCustAccName.getBytes("GBK"));

                        }else{
                            Bitmap label_name=drawText(strCustAccName, 378,45,  Color.BLACK);
                            label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                            printPhoto(label_name);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    PrashantG 07-05-2019

//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccName, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strCustAccNo = bundle.getString("AccountNumber") + "\n";
                if(bundle.getString("LabelTextKHR")==null){
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_acc_number);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustAccNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccNo, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }else if(bundle.getString("LabelTextKHR").isEmpty()){
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_acc_number);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustAccNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccNo, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }else{
                    Bitmap label_name=drawTextCASA(bundle.getString("LabelTextKHR"), 378,45,  Color.BLACK);
                    label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                    printPhoto(label_name);
                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustAccNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccNo, 2, 0);
                }

                if (!bundle.getString("CustomerMobile").isEmpty()) {
                    String strCustomerMob = "+855 " + bundle.getString("CustomerMobile", "") + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_cust_no_bank);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 50, false);
                    if (label_sender != null) {
//                        Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustomerMob.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCustomerMob, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("Currency").equalsIgnoreCase("USD")) {
                    strCurrency = "  $ ";
                } else {
                    strCurrency = "  KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("Amount", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("ProcessingFee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + jsonObject.getString("RECEIPTNO") + "\n";
                String strDate = "Date/Time:" + jsonObject.getString("TRANSACTIONDATEFORMATED") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }
            else if (bundle.getString("TranType").equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_BANK)
            )
            {
//transfer

                String msg = "";
//                msg += "------------------------------------------\n";
                msg += "--------------------------------\n";
                //print khmer header
//                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
//                        R.drawable.iv_print_bank_header);
//                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 260, 40, false);

                Bitmap label_fund_success = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.img_acleda_header);
                label_fund_success = Bitmap.createScaledBitmap(label_fund_success, 320, 45, false);


                if (label_fund_success != null) {
                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
//                    Print_BMP(label_fund_success, 260,40);
                    printPhoto(label_fund_success);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_fund_success);

                    Command.ESC_Align[2] = 0x00;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(msg.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                //riteshb 21-11
                String strBankName = "";
                try {
                    strBankName = bundle.getString("BankName") + "\n";
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Bitmap label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bank_name);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strBankName.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strBankName, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                JSONArray jsonArray = new JSONArray(bundle.getString("json"));
                JSONObject jsonObject = jsonArray.getJSONObject(0);

                String strCustAccName = "";
                if (jsonObject.getString("ACCOUNTNAME").length() <= 18) {
                    strCustAccName = jsonObject.getString("ACCOUNTNAME") + "\n";
                } else {
                    strCustAccName = jsonObject.getString("ACCOUNTNAME") + "\n";
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bank_number);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

//                    PrashantG 07-05-2019

//                    Command.ESC_Align[2] = 0x02;//left
//                    SendDataByte(Command.ESC_Align);
//                    Command.GS_ExclamationMark[2] = 0x00;//small font
//                    SendDataByte(Command.GS_ExclamationMark);
//                    SendDataByte(strCustAccName.getBytes("GBK"));

                    try {
                        char c = strCustAccName.charAt(0);
                        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
                            strCustAccName +=  "\n";
                            Command.ESC_Align[2] = 0x02;//left
                            SendDataByte(Command.ESC_Align);
                            Command.GS_ExclamationMark[2] = 0x10;//small font
                            SendDataByte(Command.GS_ExclamationMark);
                            SendDataByte(strCustAccName.getBytes("GBK"));

                        }else{
                            Bitmap label_name=drawText(strCustAccName, 378,45,  Color.BLACK);
                            label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                            printPhoto(label_name);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    PrashantG 07-05-2019

//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccName, 2, 0);
                } else {

                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strCustAccNo = bundle.getString("AccountNumber") + "\n";
                if(bundle.getString("LabelTextKHR")==null){
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_acc_number);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustAccNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccNo, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }else if(bundle.getString("LabelTextKHR").isEmpty()){
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_acc_number);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                        printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x00;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustAccNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccNo, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }else{
                    Bitmap label_name=drawTextCASA(bundle.getString("LabelTextKHR"), 378,45,  Color.BLACK);
                    label_name = Bitmap.createScaledBitmap(label_name, 250, 40, false);
                    printPhoto(label_name);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x00;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCustAccNo.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCustAccNo, 2, 0);
                }

                if (!bundle.getString("CustomerMobile").isEmpty()) {
                    String strCustomerMob = "+855 " + bundle.getString("CustomerMobile", "") + "\n";
                    label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.iv_print_cust_no_bank);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 250, 50, false);
                    if (label_sender != null) {
//                        Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
//                        ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                        Command.ESC_Align[2] = 0x02;//left
                        SendDataByte(Command.ESC_Align);
                        Command.GS_ExclamationMark[2] = 0x10;//small font
                        SendDataByte(Command.GS_ExclamationMark);
                        SendDataByte(strCustomerMob.getBytes("GBK"));
//                        ClsBTPrintHandler.mConnector.printCustom(strCustomerMob, 2, 0);
                    } else {
                        Log.e("Print Photo error", "the file isn't exists");
                    }
                }

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                String strCurrency = "";
                if (bundle.getString("Currency").equalsIgnoreCase("USD")) {
                    strCurrency = "  $ ";
                } else {
                    strCurrency = "  KHR ";
                }
                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("Amount", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_amount);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strAmount.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strAmount, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                String strFee_value = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("ProcessingFee", "0").replace(",", "")))
                        + "\n";
                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_fee);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strFee_value.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strFee_value, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_total);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 35, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(strCurrency.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(strCurrency, 2, 0);
                    String tot = ConstantDeclaration.amountFormatter(Double.parseDouble(strAmount.replace(",", ""))
                            + Double.parseDouble(strFee_value.replace(",", ""))) + "\n";

                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(tot.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(tot, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_agent_contact);
                label_sender = Bitmap.createScaledBitmap(label_sender, 250, 40, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 250,40);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
                    String agent = "+855 " + UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name), mContext,
                            ConstantDeclaration.USER_MOBILE, "") + "\n";
                    Command.ESC_Align[2] = 0x02;//left
                    SendDataByte(Command.ESC_Align);
                    Command.GS_ExclamationMark[2] = 0x10;//small font
                    SendDataByte(Command.GS_ExclamationMark);
                    SendDataByte(agent.getBytes("GBK"));
//                    ClsBTPrintHandler.mConnector.printCustom(agent, 2, 0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }

                String strReceipt = "Receipt No.: " + jsonObject.getString("RECEIPTNO") + "\n";
                String strDate = "Date/Time:" + jsonObject.getString("TRANSACTIONDATEFORMATED") + "\n";
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 1, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }

            else {
                //transfer

                String strTitle = "DaraPay";

                String msg = "";
//                msg += "******************************************\n\n";
                msg += "------------------------------\n";
                String strReceipt = "Receipt No.    : " + bundle.getString("ReceiptNoError", "") + "\n";
                String strCustomer = "Customer No.   : +855 " + bundle.getString("ToNumber", "") + "\n";
//            String strName = " Customer Name  : Kalpesh Dhumal\n";
                String strPaymentAmountTxt = "Payment Amount : ";
                String strCurrency = "";
                if (bundle.getString("CURRENCY", "").equalsIgnoreCase("USD")) {
                    strCurrency = "$ ";
                } else {
                    strCurrency = "KHR ";
                }

                String strAmount = ConstantDeclaration.amountFormatter(
                        Double.parseDouble(bundle.getString("PaymentAmntReferenceNumber", ""))) + "\n";
                String strFee = "Fee            : ";
                String strFee_value =bundle.getString("fee", "") + "\n";
                String strDate = "Date/Time:" + bundle.getString("DateTime", "") + "\n";
                String strFooter = "\n******************************************\n";
                strFooter += "\nThank You!!!\n\n";
                strFooter += "****** Please Visit Again. ******\n\n\n\n";
                Bitmap label_sender;
                if(bundle.getString("TranType").equalsIgnoreCase(TRAN_TYPE_COLLECT_CASH)){
                   label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.cash_collection_khr);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 320, 65, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
                    }
                }else if(bundle.getString("TranType").equalsIgnoreCase(TRAN_TYPE_CASHIN)){
                   label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.cash_in_khr);
                    label_sender = Bitmap.createScaledBitmap(label_sender, 320, 65, false);
                    if (label_sender != null) {
//                    Print_BMP(label_sender, 250, 35);
                        printPhoto(label_sender);
                    }
                }



                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strReceipt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strReceipt, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strCustomer.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strCustomer, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strPaymentAmountTxt.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strPaymentAmountTxt, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strCurrency.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strCurrency, 1, 0);
//            ClsBTPrintHandler.mConnector.printText(Ux17DB);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strAmount.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strAmount, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strFee.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strFee, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strCurrency.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strCurrency, 1, 0);
//            ClsBTPrintHandler.mConnector.printText(Ux17DB);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(ConstantDeclaration.amountFormatter(
                        Double.parseDouble(strFee_value.replace(" ", ""))).getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strFee_value, 0, 0);

                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(strDate.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(strDate, 0, 0);
                //2-5-18
//                Command.ESC_Align[2] = 0x00;//left
//                SendDataByte(Command.ESC_Align);
//                Command.GS_ExclamationMark[2] = 0x00;//small font
//                SendDataByte(Command.GS_ExclamationMark);
//                SendDataByte(strFooter.getBytes("GBK"));
////                ClsBTPrintHandler.mConnector.printCustom(strFooter, 0, 1);
//
//                Command.ESC_Align[2] = 0x00;//left
//                SendDataByte(Command.ESC_Align);
//                Command.GS_ExclamationMark[2] = 0x00;//small font
//                SendDataByte(Command.GS_ExclamationMark);
//                SendDataByte("\n\n".getBytes("GBK"));
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte(msg.getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom(msg, 0, 0);

                label_sender = BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.iv_print_bottom);
                label_sender = Bitmap.createScaledBitmap(label_sender, 390, 170, false);
                if (label_sender != null) {
//                    Print_BMP(label_sender, 390,170);
                    printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printPhoto(label_sender);
//                    ClsBTPrintHandler.mConnector.printCustom(tot,0,0);
                } else {
                    Log.e("Print Photo error", "the file isn't exists");
                }
                Command.ESC_Align[2] = 0x00;//left
                SendDataByte(Command.ESC_Align);
                Command.GS_ExclamationMark[2] = 0x00;//small font
                SendDataByte(Command.GS_ExclamationMark);
                SendDataByte("\n\n".getBytes("GBK"));
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

                //2-5-18
//                ClsBTPrintHandler.mConnector.printCustom("\n\n", 0, 0);

            }


        } catch (Exception e) {
            e.printStackTrace();
//            ClsBTPrintHandler.mConnector.printCustom("\n", 0, 0);
            strpage = "ClsBluetoothPrintFrag.java;sendData();";
            ConstantDeclaration.writePrintLogs(e, strpage);
        }
        strpage = "ClsBluetoothPrintFrag.java;sendData();--Exit--\n";
        ConstantDeclaration.writePrintLogs(strpage);
    }*/


    //9-4-18
    public static void fun_changeNav_withRole(Context mContext, ActionChangeFrag changeFrag, Fragment custFragment
            , Fragment agentFragment, Fragment merchantFragment, Fragment staffFragment) {
        if (UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
                mContext, ConstantDeclaration.USERROLEPREFKEY,
                "4").equalsIgnoreCase(CUSTOMERFLAG)) {
            if (custFragment != null) {
                changeFrag.addFragment(custFragment);
            }
        } else if (UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
                mContext, ConstantDeclaration.USERROLEPREFKEY,
                "4").equalsIgnoreCase(AGENTFLAG)) {
            if (agentFragment != null) {
                changeFrag.addFragment(agentFragment);
            }
        } else if (UserDataPrefrence.getPreference(mContext.getString(R.string.prefrence_name),
                mContext, ConstantDeclaration.USERROLEPREFKEY,
                "4").equalsIgnoreCase(MERCHANTFLAG)) {
            if (merchantFragment != null) {
                changeFrag.addFragment(merchantFragment);
            }
        } else {
            //staff
            if (staffFragment != null) {
                changeFrag.addFragment(staffFragment);
            }
        }

    }


    public static String callDefaultString(String strCallFrom) {
        String callDefaultString = "";
        try {
            if (strCallFrom.equalsIgnoreCase(TRAN_TYPE_CASHOUT)) {
                callDefaultString = "Cash-Out";
            } else if (strCallFrom.equalsIgnoreCase(TRAN_TYPE_CASHIN)) {
                callDefaultString = "Cash-In";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_MOBILETOPUP)) {
                callDefaultString = "Mobile topup";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_CASA_TO_EWALLET)) {
                callDefaultString = "CASA to DaraPay";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_EWALLET)) {
                callDefaultString = "DaraPay to DaraPay";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_NONWALLET)) {
                callDefaultString = "DaraPay to Non DaraPay";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_NONWALLET)) {
                callDefaultString = "Non DaraPay to Non DaraPay";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_BALANCE_INQUIRY)) {
                callDefaultString = "Balance enquiry";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_EWALLET)) {
                callDefaultString = "Non DaraPay to DaraPay";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_REVERSAL)) {
                callDefaultString = "Reversal";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_COLLECT_CASH)) {
                callDefaultString = "Collect Cash";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_CASA)) {
                callDefaultString = "DaraPay to CASA";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA)) {
                callDefaultString = "Non DaraPay to CASA";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NWALLET_TO_MOBILETOPUP)) {
                callDefaultString = "Non DaraPay mobile topup";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_MANUAL_REVERSAL)) {
                callDefaultString = "Manual reversal";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_LOADING)) {
                callDefaultString = "Loading";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_UNLOADING)) {
                callDefaultString = "Unloading";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_EWALLET_TO_CASA_CP)) {
                callDefaultString = "DaraPay to CASA CP";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_NONWALLET_TO_CASA_CP)) {
                callDefaultString = "Non DaraPay to CASA CP";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_MERCHANT_PAYMENT)) {
                callDefaultString = "Merchant payment";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_MERCHANT_PAYMENT_REFUND)) {
                callDefaultString = "Merchant Refund";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_CUST_UPGRD)) {
                callDefaultString = "Customer upgrade";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_WATER_SUPPLY_BILL_PAY_FOR_CUST)) {
                callDefaultString = "PPWSA Bill Payment";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_WATER_SUPPLY_BILL_PAY_FOR_AGENT)) {
                callDefaultString = "PPWSA Bill Payment";
            }else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_WATER_SUPPLY_BILL_PAY_FOR_AGENT)) {
                callDefaultString = "PPWSA Bill Payment";
            }else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_Payroll)) {
                callDefaultString = "CASA Cash-Out";
            }
//            else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA)
//                    || strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_AGENT)) {
//                callDefaultString = "NonDaraPay to ACLEDA";
//            }
            else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA)){
                callDefaultString = "DaraPay to ACLEDA Cash-In";
            }else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_AGENT)){
                callDefaultString = "Non DaraPay to ACLEDA Cash-In";
            } else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_CashOut)){
//                callDefaultString = "DaraPay to ACLEDA Cash-Out";
                callDefaultString = "DaraPay to ACLEDA Wallet Top Up";

            }else if (strCallFrom.equalsIgnoreCase(ConstantDeclaration.TRAN_TYPE_ACLEDA_BANK)){
                callDefaultString = "Non DaraPay to ACLEDA Cash-Out";
            }

            else {
                callDefaultString = "Enter your string for fav. txn";
            }
        } catch (Exception e) {
            callDefaultString = "Enter your string for fav. txn";
        }

        return  callDefaultString;
    }



    public static Bitmap drawText(String text, int textWidth,int textHeight,  int color) {

        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(50);

        StaticLayout mTextLayout = new StaticLayout(text, textPaint, textWidth, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to
        Bitmap b = Bitmap.createBitmap(mTextLayout.getWidth(), mTextLayout.getHeight(), Bitmap.Config.ARGB_4444);
        Canvas c = new Canvas(b);

        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();

        return b;
    }
    public static Bitmap drawText1(String text, int textWidth,int textHeight,  int color) {

        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(40);

        StaticLayout mTextLayout = new StaticLayout(text, textPaint, textWidth, Layout.Alignment.ALIGN_CENTER, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to

        Bitmap b = Bitmap.createBitmap(mTextLayout.getWidth(), mTextLayout.getHeight(), Bitmap.Config.ARGB_4444);
//        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(screenWidth, (int)newWidth);
//        params.addRule(RelativeLayout.ALIGN_PARENT_TOP, RelativeLayout.TRUE);
//        view.setLayoutParams(params);
//        view.setImageBitmap(scaledBitmap);

        Canvas c = new Canvas(b);
        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();

        return b;
    }

    public static Bitmap drawTextCASA(String text, int textWidth,int textHeight,  int color) {

        // Get text dimensions
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        textPaint.setStyle(Paint.Style.FILL);
        textPaint.setColor(Color.BLACK);
        textPaint.setTextSize(50);

        StaticLayout mTextLayout = new StaticLayout(text, textPaint, textWidth, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, false);

        // Create bitmap and canvas to draw to
        Bitmap b = Bitmap.createBitmap(mTextLayout.getWidth(), mTextLayout.getHeight(), Bitmap.Config.ARGB_4444);
        Canvas c = new Canvas(b);

        // Draw background
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.LINEAR_TEXT_FLAG);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        c.drawPaint(paint);

        // Draw text
        c.save();
        c.translate(0, 0);
        mTextLayout.draw(c);
        c.restore();

        return b;
    }

    public static void APPException(String Error){
        JSONObject json = null;
        try {
            json = new JSONObject();
            json.put("userName", UserDataPrefrence.getPreference("HPCL_Preference", MainApplication.getContext(),
                    "UserName1",""));
            json.put("SourceID","AA6CA277-F7C1-46BF-AA29-A59CCC9CEE9F");
            json.put("Error",Error);

        } catch (Exception e) {
            e.printStackTrace();
        }
        new AsyncAppException().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, json);
    }
    public static class AsyncAppException extends AsyncTask<JSONObject, Void, String> {

        String strTimeStamp;

        public AsyncAppException(String strTime) {
            strTimeStamp = strTime;
        }
        public AsyncAppException() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            try {

                if (ConstantDeclaration.gifProgressDialog != null) {
                    if (!ConstantDeclaration.gifProgressDialog.isShowing()) {
                        ConstantDeclaration.showGifProgressDialog(MainApplication.getContext());
                    }
                } else {
                    ConstantDeclaration.showGifProgressDialog(MainApplication.getContext());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected String doInBackground(JSONObject... jsonObjects) {
            return ClsWebService_hpcl.PostObjecttoken("Login/AppException", false, jsonObjects[0],"1");
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                if (ConstantDeclaration.gifProgressDialog.isShowing())
                    ConstantDeclaration.gifProgressDialog.dismiss();
            } catch (Exception e) {
                e.printStackTrace();
            }


            String str_response_msg = "", str_time = "";
            if (s.contains("||")) {
                String[] str = (s.split("\\|\\|"));
                if (str[0].equalsIgnoreCase("00")
                        ||str[0].equalsIgnoreCase("0")) {


                } else {

                }
            }
        }
    }
    public static void replaceFragment1(Fragment fragment, FragmentManager fragmentManager) {
        String backStateName = fragment.getClass().getName();


        Fragment currentFrag = fragmentManager.findFragmentById(R.id.container);
        Log.e("Current Fragment", "" + currentFrag);

//        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStateName, 0);
        int countFrag = fragmentManager.getBackStackEntryCount();
        Log.e("Count", "" + countFrag);


        if (currentFrag != null && currentFrag.getClass().getName().equalsIgnoreCase(fragment.getClass().getName())) {
            return;
        }

//        fragment.setArguments(bundle);


        FragmentTransaction ft = fragmentManager.beginTransaction();
//        ft.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        ft.replace(R.id.container, fragment);
        ft.addToBackStack(backStateName);
        //28/12/17 riteshb
//        ft.commit();
        ft.commitAllowingStateLoss();
        //28/12/17 riteshb
        currentFrag = fragmentManager.findFragmentById(R.id.container);
        Log.e("Current Fragment", "" + currentFrag);


    }

}

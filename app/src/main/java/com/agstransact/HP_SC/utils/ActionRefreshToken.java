package com.agstransact.HP_SC.utils;

/**
 * Created by Prashant Gadekar on 22,August,2019
 */
public interface ActionRefreshToken {


    public void refreshToken();
}

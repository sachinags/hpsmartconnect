package com.agstransact.HP_SC.utils;

import android.app.Application;

/**
 * Created by Satish Patel on 30,March,2021
 */
public class ClsApplication extends Application {
   private static ClsApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }


    public static synchronized ClsApplication getInstance(){
        return mInstance;
    }
}

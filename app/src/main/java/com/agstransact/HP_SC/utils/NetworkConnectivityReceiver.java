package com.agstransact.HP_SC.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import androidx.appcompat.app.AlertDialog;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.agstransact.HP_SC.R;
//import com.agstransact.HP_SC.common.Log;

import org.json.JSONObject;

import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by amit.verma on 21-09-2017.
 */

public class NetworkConnectivityReceiver extends BroadcastReceiver {
    public static ConnectivityReceiverListener connectivityReceiverListener;
    static AlertDialog alertDialog;
    static Context dialogContext;
    static int retrycount = 0;
    static  boolean isConnected;

    public NetworkConnectivityReceiver() {
        super();
    }

    @Override
    public void onReceive(final Context context, Intent intent) {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                new AsyncNetworkWorking(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                retrycount=0;
            }
        }, 500);

    }

    public interface ConnectivityReceiverListener {
        void onNetworkConnectionChanged(boolean isConnected);
    }

    public void addListner(ConnectivityReceiverListener listner, Context dialogContext) {
        connectivityReceiverListener = listner;
        this.dialogContext = dialogContext;
    }

    public void removeListener() {
        connectivityReceiverListener = null;

    }

    public static class AsyncNetworkWorking extends AsyncTask<JSONObject, Void, Boolean> {
        Context context;

        public AsyncNetworkWorking(Context context) {
            this.context = context;
        }


        @Override
        protected Boolean doInBackground(JSONObject... jsonObjects) {
            if (isNetworkAvailable(context)) {
                try {
//                    HttpsURLConnection urlc = null;
//                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
//                    InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
//                    Certificate ca;
//                    try {
//                        ca = cf.generateCertificate(caInput);
//                        System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//                    } finally {
//                        caInput.close();
//                    }
//                    String keyStoreType = KeyStore.getDefaultType();
//                    KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//                    keyStore.load(null, null);
//                    keyStore.setCertificateEntry("ca", ca);
//                    String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//                    TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//                    tmf.init(keyStore);
//                    SSLContext context = SSLContext.getInstance("TLS");
//                    context.init(null, tmf.getTrustManagers(), null);
//
//                    urlc = (HttpsURLConnection) (new URL(BaseUrl).openConnection());
//                    urlc.setSSLSocketFactory(context.getSocketFactory());
//                    urlc.setRequestProperty("User-Agent", "Test");
//                    urlc.setRequestProperty("Connection", "close");
//                    urlc.setRequestMethod("POST");
//                    urlc.setConnectTimeout(1500);
//                    urlc.connect();
//                    return (urlc.getResponseCode() == 200);
                    HttpsURLConnection urlc = null;
                    urlc = (HttpsURLConnection) (new URL("https://www.google.com").openConnection());
                    urlc.setConnectTimeout(3500);
                    urlc.connect();
                    int code = urlc.getResponseCode();
                    if (code == 200 || code == 401) {
                        return true;
                    }
                } catch (Exception e) {
                    Log.e("Network Available::", e.toString());
                }
            } else {
                Log.d("Network Available::", "No network available!");
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);

            try {
           /* ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            boolean isConnected = activeNetwork != null
                    && activeNetwork.isConnectedOrConnecting();*/

                isConnected = aBoolean;

                if (!isConnected) {
                    if(retrycount > 2) {
                        if (alertDialog != null && alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                        showDialog(context);
                        retrycount = 0;
                    }else{
                        //check again
                        retrycount++;
                        new AsyncNetworkWorking(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    }
                } else {
                    retrycount = 0;
                    if (alertDialog != null && alertDialog.isShowing()) {
                        alertDialog.dismiss();
                    }
                }

                if (connectivityReceiverListener != null) {
                    connectivityReceiverListener.onNetworkConnectionChanged(isConnected);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    public static boolean isNetworkAvailable(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
            return activeNetwork != null
                    && activeNetwork.isConnectedOrConnecting();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static void showDialog(final Context context) {
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.network_error_layout, null
                , false);


        AlertDialog.Builder builder = new AlertDialog.Builder(dialogContext);
        builder.setView(layout);
        alertDialog = builder.create();
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        alertDialog.setCancelable(false);
        alertDialog.show();

        final Button btnClose = (Button) layout.findViewById(R.id.btnClose);
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnClose.setEnabled(false);
                new AsyncNetworkWorking(context).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            btnClose.setEnabled(true);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },3000);
            }
        });
        // set the custom dialog components - text, image and button
       /* TextView txtMsg = (TextView) layout.findViewById(R.id.txtMsg);
        TextView title_txt = (TextView) layout.findViewById(R.id.title_txt);
        Button btnClose = (Button) layout.findViewById(R.id.btnClose);

        title_txt.setText("SORRY!");
        txtMsg.setText("Network not Available");
*/

       /* btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (alertDialog.isShowing())
                    alertDialog.dismiss();


                ((AppCompatActivity) context).finish();
            }
        });*/

       /* alertDialog.setOnKeyListener(new Dialog.OnKeyListener() {

            @Override
            public boolean onKey(DialogInterface arg0, int keyCode,
                                 KeyEvent event) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    if (alertDialog.isShowing())
                        alertDialog.dismiss();
//                    finish();


                }
                return true;
            }
        });*/

    }
}

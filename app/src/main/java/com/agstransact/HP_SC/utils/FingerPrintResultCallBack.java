package com.agstransact.HP_SC.utils;

/**
 * Created by amit.verma on 11-08-2017.
 */

public interface FingerPrintResultCallBack {
    // you can define any parameter as per your requirement
//        this will give boolean true if finger print authentication will be success
    void fpResultCallBack(String message, Boolean isSuccess);
}

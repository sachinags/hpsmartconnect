package com.agstransact.HP_SC.utils;

/**
 * Created by sagar.sompura on 9/15/2017.
 */

public interface FavouriteItemClickListener {
    void onItemClick(int position);
}

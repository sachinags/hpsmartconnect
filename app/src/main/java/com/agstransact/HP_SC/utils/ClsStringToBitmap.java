package com.agstransact.HP_SC.utils;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/**
 * Created by prashant.gadekar on 08-09-2017.
 */

public class ClsStringToBitmap {

    public static Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = android.util.Base64.decode(encodedString, android.util.Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0,
                    encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }
}

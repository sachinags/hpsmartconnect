package com.agstransact.HP_SC.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.core.content.ContextCompat;
import android.view.View;

import com.agstransact.HP_SC.R;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Date;

/**
 * Created by prashant.gadekar on 24-10-2017.
 */

public class ClsCaptureScreenShot {

    static Context mContext;
    static FragmentActivity activity_main;

    public static void takeScreenshot(FragmentActivity activity, String str_name) {

        mContext = activity;
        activity_main = activity;
        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        if (!checkStoragePermission()){
            return;
        }
            try {
                // image naming and path  to include sd card  appending name you choose for file
//                String mPath = Environment.getExternalStorageDirectory().toString() + "/" +activity.getString(R.string.foldar_name)+ str_name + ".jpg";

                // create bitmap screen capture
                View v1 = activity.getWindow().getDecorView().getRootView();
                v1.setDrawingCacheEnabled(true);
                Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
                v1.setDrawingCacheEnabled(false);




                String path = Environment.getExternalStorageDirectory() + "/" + "Darapay";

                File directory = new File(path);
                directory.mkdir();
                String fullName = path +  "/"+now + ".jpg";
                File imageFile = new File(fullName);
                if (!imageFile.exists()) {
                    FileOutputStream outputStream = new FileOutputStream(imageFile);
                    int quality = 100;
                    bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
                    outputStream.flush();
                    outputStream.close();
                    showSuccessfulDialoig();
                }else{
                    UniversalDialog universalDialog = new UniversalDialog(mContext, null,
                            "",
                            activity.getString(R.string.already_capture_screen)
                            , mContext.getString(R.string.dialog_ok), "");
                    universalDialog.showAlert();
                }



//            openScreenshot(imageFile);
            } catch (Throwable e) {
                // Several error may come out with file handling or DOM
                e.printStackTrace();
            }
    }

    private static boolean checkStoragePermission() {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(activity_main,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                UniversalDialog universalDialog = new UniversalDialog(mContext, new AlertDialogInterface() {
                    @Override
                    public void methodDone() {
                        ActivityCompat.requestPermissions(activity_main,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                101);
                    }

                    @Override
                    public void methodCancel() {

                    }
                },
                        mContext.getString(R.string.storage_permission_title),
                        mContext.getString(R.string.storage_permission_msg)
                        , mContext.getString(R.string.dialog_ok), "");
                universalDialog.showAlert();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(activity_main,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        101);
            }
            return false;


        }else {
            return true;
        }
    }
    private static void showSuccessfulDialoig(){
        UniversalDialog universalDialog = new UniversalDialog(mContext, null,
                "",
                mContext.getString(R.string.screen_shot_msg)
                , mContext.getString(R.string.dialog_ok), "");
        universalDialog.showAlert();

    }
}

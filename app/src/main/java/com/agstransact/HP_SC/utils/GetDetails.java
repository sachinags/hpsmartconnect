package com.agstransact.HP_SC.utils;

import android.content.Context;

import com.agstransact.HP_SC.R;
//import com.agstransact.HP_SC.customertransfer.adapter.MyFundOptionPojo;

public class GetDetails {

   /* public static MyFundOptionPojo usingTranType(int tranType, Context mContext){

        MyFundOptionPojo data = null;

            if(tranType == 11){
                data = new MyFundOptionPojo();
                data.setLabel(mContext.getResources().getString(R.string.cash_collection).toUpperCase());
                data.setLogo_id(R.drawable.cash_loan);
                data.setInfo_id(R.mipmap.ic_info);
                data.setTranType(tranType);
            }else if(tranType == 14){
                data = new MyFundOptionPojo();
                data.setLabel(mContext.getResources().getString(R.string.mobile_top_up).toUpperCase());
                data.setLogo_id(R.drawable.mobile_topup_refs);
                data.setInfo_id(R.mipmap.ic_info);
                data.setTranType(tranType);
            }else if(tranType == 23|| tranType == 41){
                data = new MyFundOptionPojo();
                data.setLabel(mContext.getResources().getString(R.string.pay_refund).toUpperCase());
                data.setLogo_id(R.drawable.alipay_refund_logo);
                data.setInfo_id(R.mipmap.ic_info);
                data.setTranType(tranType);
            }else if(tranType == 27){
                data = new MyFundOptionPojo();
                data.setLabel(mContext.getResources().getString(R.string.cnb_payroll));
                data.setLogo_id(R.drawable.img_cnb_payroll);
                data.setInfo_id(R.mipmap.ic_info);
                data.setTranType(tranType);
            }

        return data;
    }

    public static com.agstransact.HP_SC.merchantServices.MyFundOptionPojo usingTranTypeService(int tranType, Context mContext){

        com.agstransact.HP_SC.merchantServices.MyFundOptionPojo data = null;

            if(tranType == 3){
                data = new com.agstransact.HP_SC.merchantServices.MyFundOptionPojo();
                data.setLabel(mContext.getResources().getString(R.string.cash_collection).toUpperCase());
                data.setLogo_id(R.drawable.cash_loan);
                data.setInfo_id(R.mipmap.ic_info);
            }

            if(tranType == 14){
                data = new com.agstransact.HP_SC.merchantServices.MyFundOptionPojo();
                data.setLabel(mContext.getResources().getString(R.string.mobile_top_up).toUpperCase());
                data.setLogo_id(R.drawable.mobile_topup_refs);
                data.setInfo_id(R.mipmap.ic_info);
            }

        return data;
    }


    public static int getDefaultLogo(int tranType){

        com.agstransact.HP_SC.merchantServices.MyFundOptionPojo data = null;

        if(tranType == 1){
           return R.drawable.cash_in_refs;
        }else if(tranType == 2
                || tranType == 27){
           return R.drawable.cash_out_refs;
        }else if(tranType == 3){
            return R.drawable.mobile_topup_refs;
        }else if(tranType == 5 || tranType == 9 ){
            return R.drawable.e_wallet;
        }else if(tranType == 6 || tranType == 7 ){
            return R.drawable.non_e_wallet;
        }else if(tranType == 12 || tranType == 13
                || tranType == 20 || tranType == 21 ){
            return R.drawable.casa_loan;
        }else if(tranType == 14){
           return R.drawable.mobile_topup_refs;
        }else if(tranType == 24){
           return R.drawable.customer_type_upgrade_refs;
        }else if(tranType == 25 || tranType == 26){
           return R.drawable.img_bill_payment;
        }else if(tranType == 32){
           return R.drawable.img_loan_disbursement;
        }
        return R.drawable.default_dot;
    }


    public static int  getTranName(int tranType){

        com.agstransact.HP_SC.merchantServices.MyFundOptionPojo data = null;

        if(tranType == 1){
            return R.string.cash_in;
        }else if(tranType == 2
                || tranType == 27){
            return R.string.cash_out;
        }else if(tranType == 3 || tranType == 14){
            return R.string.mobile_top_up;
        }else if(tranType == 5 || tranType == 9 ){
            return R.string.e_wallet;
        }else if(tranType == 6 || tranType == 7 ){
            return R.string.non_e_wallet;
        }else if(tranType == 12 || tranType == 13
                || tranType == 20 || tranType == 21){
            return R.string.to_casa_loan;
        }else if(tranType == 24){
            return R.string.customer_type;
        }else if(tranType == 25 || tranType == 26){
            return R.string.bill_payment;
        }else if(tranType == 32){
            return R.string.loan_disbursement;
        }
        return  R.string.not_found;
    }*/



}

package com.agstransact.HP_SC.utils;

import android.content.Context;
import android.util.Base64;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;

public class ClsWeakDataEncrption {
    public static String encryptData(Context mcontext, String txt) {
        String encoded = "";
        byte[] encrypted = null;
        try {
            byte[] publicBytes = Base64.decode(readAssestFileData(mcontext), Base64.NO_WRAP);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey pubKey = keyFactory.generatePublic(keySpec);
//            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");  //or try with "RSA"
            Cipher cipher = Cipher.getInstance("RSA/ECB/OAEPWITHSHA-256ANDMGF1PADDING");  //or try with "RSA"
//            Cipher cipher = Cipher.getInstance("RSA");  //or try with "RSA"
            cipher.init(Cipher.ENCRYPT_MODE, pubKey);
            encrypted = cipher.doFinal(txt.getBytes());
            encoded = Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encoded;
    }
    public static String DecryptData(Context mcontext, final byte[] encryptedBytes) {
        String encoded = "";
        byte[] encrypted = null;
        try {
            byte[] publicBytes = Base64.decode(readAssestFiledecryptData(mcontext), Base64.NO_WRAP);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            PublicKey pubKey = keyFactory.generatePublic(keySpec);
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1PADDING");  //or try with "RSA"
            cipher.init(Cipher.DECRYPT_MODE, pubKey);
            encrypted = cipher.doFinal(encryptedBytes);
            encoded = Base64.encodeToString(encrypted, Base64.NO_WRAP);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return encoded;
    }


    private static String readAssestFileData(Context mcontext) {
        BufferedReader reader = null;
        String key = "";
        try {
            reader = new BufferedReader(new InputStreamReader(mcontext.getAssets().open("encrypt_hpcl.txt"), "UTF-8"));
//            reader = new BufferedReader(new InputStreamReader(mcontext.getAssets().open(ENCRYPT_FILE), "UTF-8"));
            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                key = key + mLine;
                //                System.out.println("test.txt" + mLine);
                //                ConstantDeclaration.showMessageDialog(this, mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {                    //log the exception
                }
            }
        }
        return key;
    }
    private static String readAssestFiledecryptData(Context mcontext) {
        BufferedReader reader = null;
        String key = "";
        try {
            reader = new BufferedReader(new InputStreamReader(mcontext.getAssets().open("encrypt_hpcl.txt"), "UTF-8"));
//            reader = new BufferedReader(new InputStreamReader(mcontext.getAssets().open(ENCRYPT_FILE), "UTF-8"));
            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                key = key + mLine;
                //                System.out.println("test.txt" + mLine);
                //                ConstantDeclaration.showMessageDialog(this, mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {                    //log the exception
                }
            }
        }
        return key;
    }


}

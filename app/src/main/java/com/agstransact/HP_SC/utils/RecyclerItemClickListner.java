package com.agstransact.HP_SC.utils;

/**
 * Created by amit.verma on 05-09-2017.
 */

public interface RecyclerItemClickListner {
    void onItemClick(String adapterType, int position);
    void onItemClick(String label,String value,int position);
    void onItemClick(int position);
    void onItemLongClick(int position);
}

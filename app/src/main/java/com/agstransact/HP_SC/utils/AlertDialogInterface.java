package com.agstransact.HP_SC.utils;

/**
 * Created by amit.verma on 03-08-2017.
 * making interface class for dialog, show we can get the action ok and cancel button
 */

public interface AlertDialogInterface {
    /*ok method calling */
    void methodDone();

    /*cancel method calling*/
    void methodCancel();
}

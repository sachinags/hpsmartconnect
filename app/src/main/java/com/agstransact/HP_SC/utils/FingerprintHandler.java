package com.agstransact.HP_SC.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;

import static android.hardware.fingerprint.FingerprintManager.*;

/**
 * Created by amit.verma on 10-08-2017.
 */

@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerprintHandler extends AuthenticationCallback {


    private Context context;
    private FingerPrintResultCallBack fingerPrintResult;
    CancellationSignal cancellationSignal;


    // Constructor
    public FingerprintHandler(Context mContext,FingerPrintResultCallBack fpCallBack) {
        this.context = mContext;
        this.fingerPrintResult =fpCallBack;
    }


    public void startAuth(FingerprintManager manager, CryptoObject cryptoObject) {
         cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }


    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("Fingerprint Authentication error\n" + errString, false);
    }


    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
//        this.update("Fingerprint Authentication help\n" + helpString, false);
        this.update("Fingerprint Authentication error\n" + helpString, false);
    }


    @Override
    public void onAuthenticationFailed() {
        this.update("Fingerprint Authentication failed.", false);
    }


    @Override
    public void onAuthenticationSucceeded(AuthenticationResult result) {
        this.update("Fingerprint Authentication succeeded.", true);
    }


    public void update(String error, Boolean success) {
       /* TextView textView = (TextView) ((Activity)context).findViewById(R.id.errorText);
        textView.setText(e);
        if(success){
            textView.setTextColor(ContextCompat.getColor(context,R.color.colorPrimaryDark));
        }*/
        try {
            if (fingerPrintResult != null) {
                fingerPrintResult.fpResultCallBack(error,success);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public void cancel() {
        if (cancellationSignal != null) {
            cancellationSignal.cancel();
            cancellationSignal = null;
//            this.update("Fingerprint Authentication cancelled.", false);
        }
    }
}
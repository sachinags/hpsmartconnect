package com.agstransact.HP_SC.utils;

/**
 * Created by ritesh.bhavsar on 27-10-2017.
 */

public interface RecyclerSeekBarListener {
    void seekBarOnSeekListener(int position, String Value);
}

package com.agstransact.HP_SC.utils;

import android.content.Intent;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.dashboard.HOSDashboardActivity;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.splash.ActivityLaunchScreen;
//import com.agstransact.HP_SC.dashboard.DashBoardActivity;
//import com.agstransact.HP_SC.launcher.ActivityLauncher;
//import com.agstransact.HP_SC.login.HOSDashboardActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import static com.agstransact.HP_SC.utils.ClsWebService.CERTIFICATE;

/**
 * Created by ritesh.bhavsar on 31-01-2018.
 */

public class MultipartFileUploader {
    private final String boundary;
    private static final String LINE_FEED = "\r\n";
    private HttpsURLConnection conn;
    private String charset;
    private OutputStream outputStream;
    private PrintWriter writer;
    public MultipartFileUploader(String requestURL, String charset)
            throws IOException {
        this.charset = charset;

        // creates a unique boundary based on time stamp
        boundary = "===" + System.currentTimeMillis() + "===";

        try {
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }

// Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);


            URL url = new URL(requestURL);

            // open a URL connection to the Servlet
//            FileInputStream fileInputStreamFront = new FileInputStream(sourceFileFront);
//                FileInputStream fileInputStreamBack = new FileInputStream(sourceFileBack);
//            URL url = new URL("https://dev.agsindia.com:7522/api/CoreEngine/MediaUpload");
            // Open a HTTP  connection to  the URL
            conn = (HttpsURLConnection) url.openConnection();
            conn.setSSLSocketFactory(context.getSocketFactory());
//            fileNameFront = fileNameFront.substring(fileNameFront.lastIndexOf("/") + 1);
//            fileNameBack = fileNameBack.substring(fileNameBack.lastIndexOf("/") + 1);
            conn.setDoInput(true); // Allow Inputs
            conn.setDoOutput(true); // Allow Outputs
            conn.setUseCaches(false); // Don't use a Cached Copy
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");
            conn.setRequestProperty("ENCTYPE", "multipart/form-data");
            conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
            conn.setRequestMethod("POST");
//            conn.setRequestProperty("FRONTPHOTO", fileNameFront);
//            conn.setRequestProperty("BACKPHOTO", fileNameBack);

        }catch (Exception e){

        }
    }
    public void setrequest(String name, String value){
        conn.setRequestProperty(name, value);
    }
    public void setconnection(){
        try {
            outputStream = conn.getOutputStream();
            writer = new PrintWriter(new OutputStreamWriter(outputStream, charset),
                    true);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Adds a form field to the request
     * @param name field name
     * @param value field value
     */
    public void addFormField(String name, String value) {
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append("Content-Disposition: form-data; name=\"" + name + "\"")
                .append(LINE_FEED);
        writer.append("Content-Type: text/plain; charset=" + charset).append(
                LINE_FEED);
        writer.append(LINE_FEED);
        writer.append(value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Adds a upload file section to the request
     * @param fieldName name attribute in <input type="file" name="..." />
     * @param uploadFile a File to be uploaded
     * @throws IOException
     */
    public void addFilePart(String fieldName, File uploadFile)
            throws IOException {
        String fileName = uploadFile.getName();
        writer.append("--" + boundary).append(LINE_FEED);
        writer.append(
                "Content-Disposition: form-data; name=\"" + fieldName.trim()
                        + "\"; filename=\"" + fileName + "\"")
                .append(LINE_FEED);
        writer.append(
                "Content-Type: "
                        + URLConnection.guessContentTypeFromName(fileName))
                .append(LINE_FEED);
        writer.append("Content-Transfer-Encoding: binary").append(LINE_FEED);
        writer.append(LINE_FEED);
        writer.flush();

        FileInputStream inputStream = new FileInputStream(uploadFile);
        int bytesAvailable = inputStream.available();
        int maxBufferSize = 1 * 1024 * 1024;
        int bufferSize = Math.min(bytesAvailable, maxBufferSize);
        byte[] buffer = new byte[bufferSize];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }
        outputStream.flush();
        inputStream.close();

        writer.append(LINE_FEED);
        writer.flush();
    }
    /**
     * Adds a header field to the request.
     * @param name - name of the header field
     * @param value - value of the header field
     */
    public void addHeaderField(String name, String value) {
        writer.append(name + ": " + value).append(LINE_FEED);
        writer.flush();
    }

    /**
     * Completes the request and receives response from the server.
     * @return a list of Strings as response in case the server returned
     * status OK, otherwise an exception is thrown.
     * @throws IOException
     */
    public String finish() throws IOException {
//        List<String> response = new ArrayList<String>();
        String response = "";
        StringBuilder sb = new StringBuilder();

        writer.append(LINE_FEED).flush();
        writer.append("--" + boundary + "--").append(LINE_FEED);
        writer.close();

        // checks server's status code first
        int status = conn.getResponseCode();
        if (status == HttpURLConnection.HTTP_OK) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            reader.close();
            conn.disconnect();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(sb.toString());

            String str_resCode = jsonObject.getString("ResponseCode");
            String str_resMsg = jsonObject.getString("ResponseMsg");
            String str_resTime = "";
            try {
                if (str_resCode.equalsIgnoreCase("5555")) {
                    //riteshb 13-12-17
                    if(MainApplication.mDashBoardContext !=null) {
                        Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
                        ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
                        ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
                    }else if(MainApplication.mLoginContext !=null) {
                        Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
                        ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
                        ((LoginActivity) MainApplication.mLoginContext).finish();
                    }
                    //riteshb 13-12-17
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                str_resTime = jsonObject.getString("TimeStamp");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String str_response_msg = "";
            if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
                str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
                str_resTime = AES.decrypt(str_resTime, AES.KEY);
            } else {
                str_response_msg = str_resMsg;
            }
            String res = str_resCode
                    + "||" + str_response_msg
                    + "||" + str_resTime;
            return res.trim();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }


        return "";
    }
}

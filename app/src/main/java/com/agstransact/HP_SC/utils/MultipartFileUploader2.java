package com.agstransact.HP_SC.utils;


import android.content.Intent;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.dashboard.HOSDashboardActivity;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.splash.ActivityLaunchScreen;
//import com.agstransact.HP_SC.common.ConstantDeclaration;
//import com.agstransact.HP_SC.dashboard.DashBoardActivity;
//import com.agstransact.HP_SC.launcher.ActivityLauncher;
//import com.agstransact.HP_SC.login.LoginNRegisterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import static com.agstransact.HP_SC.utils.ClsWebService.CERTIFICATE;

/**
 * Created by ritesh.bhavsar on 01-02-2018.
 */

public class MultipartFileUploader2 {

    private HttpURLConnection httpConn;
    private HttpsURLConnection httpConnSSL;
    private DataOutputStream request;
    private final String boundary = "*****";
    private final String crlf = "\r\n";
    private final String twoHyphens = "--";

    /**
     * This constructor initializes a new HTTP POST request with content type
     * is set to multipart/form-data
     *
     * @param requestURL
     * @throws IOException
     */
    public MultipartFileUploader2(String requestURL, String filekey1, String fileval1
            , String filekey2, String fileval2)
            throws IOException {

        try {
//            HostnameVerifier allHostsValid = new HostnameVerifier() {
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            };
//
//            CertificateFactory cf = CertificateFactory.getInstance("X.509");
//            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
//            Certificate ca;
//            try {
//                ca = cf.generateCertificate(caInput);
//                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//            } finally {
//                caInput.close();
//            }
//
//// Create a KeyStore containing our trusted CAs
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//
//// Create a TrustManager that trusts the CAs in our KeyStore
//            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);
//
//// Create an SSLContext that uses our TrustManager
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, tmf.getTrustManagers(), null);


            // creates a unique boundary based on time stamp

            URL url = new URL(requestURL);
            httpConn = (HttpURLConnection) url.openConnection();
//            httpConn.setSSLSocketFactory(context.getSocketFactory());
            httpConn.setUseCaches(false);
            httpConn.setDoOutput(true); // indicates POST method
            httpConn.setDoInput(true);

            httpConn.setRequestMethod("POST");
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty("Cache-Control", "no-cache");
            httpConn.setRequestProperty(
                    "Content-Type", "multipart/form-data;boundary=" + this.boundary);
            httpConn.setRequestProperty("Connection", "Keep-Alive");
            httpConn.setRequestProperty(filekey1, fileval1);
            httpConn.setRequestProperty(filekey2, fileval2);
            request = new DataOutputStream(httpConn.getOutputStream());
        } catch (Exception e) {

        }
    }

    /**
     * This constructor initializes a new HTTP POST request with content type
     * is set to multipart/form-data
     *
     * @param requestURL
     * @throws IOException
     */
    public MultipartFileUploader2(String requestURL, String filekey1, String fileval1
            )
            throws IOException {

        try {
//            HostnameVerifier allHostsValid = new HostnameVerifier() {
//                public boolean verify(String hostname, SSLSession session) {
//                    return true;
//                }
//            };

//            CertificateFactory cf = CertificateFactory.getInstance("X.509");
//            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
//            Certificate ca;
//            try {
//                ca = cf.generateCertificate(caInput);
//                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//            } finally {
//                caInput.close();
//            }
//
//// Create a KeyStore containing our trusted CAs
//            String keyStoreType = KeyStore.getDefaultType();
//            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//            keyStore.load(null, null);
//            keyStore.setCertificateEntry("ca", ca);
//
//// Create a TrustManager that trusts the CAs in our KeyStore
//            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//            tmf.init(keyStore);
//
//// Create an SSLContext that uses our TrustManager
//            SSLContext context = SSLContext.getInstance("TLS");
//            context.init(null, tmf.getTrustManagers(), null);


            // creates a unique boundary based on time stamp

            if (ConstantDeclaration.IS_NO_SSL) {
                URL url = new URL(requestURL);
                httpConn = (HttpURLConnection) url.openConnection();
//            httpConn.setSSLSocketFactory(context.getSocketFactory());
                httpConn.setUseCaches(false);
                httpConn.setDoOutput(true); // indicates POST method
                httpConn.setDoInput(true);

                httpConn.setRequestMethod("POST");
                httpConn.setRequestProperty("Connection", "Keep-Alive");
                httpConn.setRequestProperty("Cache-Control", "no-cache");
                httpConn.setRequestProperty(
                        "Content-Type", "multipart/form-data;boundary=" + this.boundary);
                httpConn.setRequestProperty("Connection", "Keep-Alive");
                httpConn.setRequestProperty(filekey1, fileval1);
                request = new DataOutputStream(httpConn.getOutputStream());
            } else {

                HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
            Certificate ca;
            try {
                ca = cf.generateCertificate(caInput);
                System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
            } finally {
                caInput.close();
            }

// Create a KeyStore containing our trusted CAs
            String keyStoreType = KeyStore.getDefaultType();
            KeyStore keyStore = KeyStore.getInstance(keyStoreType);
            keyStore.load(null, null);
            keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
            String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
            TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
            tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
            SSLContext context = SSLContext.getInstance("TLS");
            context.init(null, tmf.getTrustManagers(), null);

                URL url = new URL(requestURL);
                httpConnSSL = (HttpsURLConnection) url.openConnection();
                httpConnSSL.setSSLSocketFactory(context.getSocketFactory());
                httpConnSSL.setUseCaches(false);
                httpConnSSL.setDoOutput(true); // indicates POST method
                httpConnSSL.setDoInput(true);

                httpConnSSL.setRequestMethod("POST");
                httpConnSSL.setRequestProperty("Connection", "Keep-Alive");
                httpConnSSL.setRequestProperty("Cache-Control", "no-cache");
                httpConnSSL.setRequestProperty(
                        "Content-Type", "multipart/form-data;boundary=" + this.boundary);
                httpConnSSL.setRequestProperty("Connection", "Keep-Alive");
                httpConnSSL.setRequestProperty(filekey1, fileval1);
                request = new DataOutputStream(httpConnSSL.getOutputStream());

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a form field to the request
     *
     * @param name  field name
     * @param value field value
     */
    public void addFormField(String name, String value) throws IOException {
        request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
        request.writeBytes("Content-Disposition: form-data; name=\"" + name + "\"" + this.crlf);
        request.writeBytes("Content-Type: text/plain; charset=UTF-8" + this.crlf);
        request.writeBytes(this.crlf);
        request.writeBytes(value + this.crlf);
        request.flush();
    }

    /**
     * Adds a upload file section to the request
     *
     * @param fieldName  name attribute in <input type="file" name="..." />
     * @param uploadFile a File to be uploaded
     * @throws IOException
     */
    public void addFilePart(String fieldName, File uploadFile)
            throws IOException {
        String fileName = uploadFile.getName();
        request.writeBytes(this.twoHyphens + this.boundary + this.crlf);
        request.writeBytes("Content-Disposition: form-data; name=\"" +
                fieldName + "\";filename=\"" +
                fileName + "\"" + this.crlf);
        request.writeBytes(this.crlf);

//        byte[] bytes = Files.readAllBytes(uploadFile.toPath());
//        byte[] bytes =   FileUtils.readFileToByteArray(uploadFile);
        int size = (int) uploadFile.length();
        byte[] bytes = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(new FileInputStream(uploadFile));
            buf.read(bytes, 0, bytes.length);
            buf.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        request.write(bytes);
    }

    /**
     * Completes the request and receives response from the server.
     *
     * @return a list of Strings as response in case the server returned
     * status OK, otherwise an exception is thrown.
     * @throws IOException
     */
    public String finish() throws IOException {
        String response = "";

        request.writeBytes(this.crlf);
        request.writeBytes(this.twoHyphens + this.boundary +
                this.twoHyphens + this.crlf);

        request.flush();
        request.close();

        // checks server's status code first
        int status = 0;
        if (ConstantDeclaration.IS_NO_SSL) {
            status = httpConn.getResponseCode();
        } else {
            status = httpConnSSL.getResponseCode();
        }
        if (status == HttpURLConnection.HTTP_OK) {
            InputStream responseStream;
            if (ConstantDeclaration.IS_NO_SSL) {
                 responseStream = new
                        BufferedInputStream(httpConn.getInputStream());
            }else{
                responseStream = new
                        BufferedInputStream(httpConnSSL.getInputStream());
            }

            BufferedReader responseStreamReader =
                    new BufferedReader(new InputStreamReader(responseStream));

            String line = "";
            StringBuilder stringBuilder = new StringBuilder();

            while ((line = responseStreamReader.readLine()) != null) {
                stringBuilder.append(line).append("\n");
            }
            responseStreamReader.close();

            response = stringBuilder.toString();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);

                String str_resCode = jsonObject.getString("ResponseCode");
                String str_resMsg = jsonObject.getString("ResponseMsg");
                String str_resTime = "";
                try {
                    if (str_resCode.equalsIgnoreCase("5555")) {
                        //riteshb 13-12-17
                        if(MainApplication.mDashBoardContext !=null) {
                            Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
                            ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
                            ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
                        }else if(MainApplication.mLoginContext !=null) {
                            Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
                            ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
                            ((LoginActivity) MainApplication.mLoginContext).finish();
                        }
                        //riteshb 13-12-17
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    str_resTime = jsonObject.getString("TimeStamp");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                String str_response_msg = "";
                if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
                    str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
                    str_resTime = AES.decrypt(str_resTime, AES.KEY);
                } else {
                    str_response_msg = str_resMsg;
                }
                String res = str_resCode
                        + "||" + str_response_msg
                        + "||" + str_resTime;
                return res.trim();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e){
                e.printStackTrace();
            }

            if (ConstantDeclaration.IS_NO_SSL) {
                httpConn.disconnect();
            } else {
                httpConnSSL.disconnect();
            }
        } else {
            throw new IOException("Server returned non-OK status: " + status);
        }

        return response;
    }

}

package com.agstransact.HP_SC.utils;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.agstransact.HP_SC.R;

/**
 * Created by amit.verma on 03-08-2017.
 */

public class UniversalAddToFavDialog {
    private final Context mContext;
    private final AlertDialogInterface action;
    private final String message;
    private final String mPositiveBTName;
    private final String mNegativeBTName;
    private final String title;
    private boolean isCancelable;
    androidx.appcompat.app.AlertDialog dialog;



    public UniversalAddToFavDialog(Context context, AlertDialogInterface action, String title, String message, String pPositiveBTName, String pNegativeBTName) {
        this.mContext = context;
        this.action = action;
        this.title = title;
        this.message = message;
        this.mPositiveBTName = pPositiveBTName;
        this.mNegativeBTName = pNegativeBTName;
        this.isCancelable = true;
    }
    public void setCancelable(boolean bool){
        this.isCancelable = bool;
    }

    public boolean isShowing(){
        if(dialog!=null) {
            return dialog.isShowing();
        }else{
            return false;
        }
    }
    public void showAlert() {
        androidx.appcompat.app.AlertDialog.Builder builder =
                new androidx.appcompat.app.AlertDialog.Builder(mContext, R.style.MyDialogTheme);



        TextView resultMessage = new TextView(mContext);
        resultMessage.setPadding(20,10,20,10);
        resultMessage.setTextSize(20);
        resultMessage.setText(message);
        resultMessage.setGravity(Gravity.CENTER);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 20, 0, 0);
        resultMessage.setLayoutParams(lp);
        builder.setView(resultMessage);
        builder.setTitle(title);

        builder.setCancelable(isCancelable);

        builder.setPositiveButton(mPositiveBTName,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // positive button logic
                        dialog.dismiss();
                        if(action!=null){
                            action.methodDone();

                        }

                    }
                });

        builder.setNeutralButton(mNegativeBTName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if(action!=null){
                    action.methodCancel();
                }
                dialog.dismiss();
            }
        });

        dialog = builder.create();
        // display dialog


        dialog.show();

    }

    public void showAddToFavDialog(String strDefaultName, Context context, final AlertDialogInterface action){

        final Dialog dialogAddFav = new Dialog(context);
        dialogAddFav.setContentView(R.layout.dialog_add_to_fav_name);
        dialogAddFav.setCancelable(false);

        // set the custom dialog components - text, image and button
//        TextView tv_SpilloverTitle = (TextView) dialogAddFav.findViewById(R.id.tv_SpilloverTitle);
//        tv_SpilloverTitle.setText("");
        EditText et_fav_name = dialogAddFav.findViewById(R.id.et_fav_name);
        et_fav_name.setText(strDefaultName);
//        TextView tvCancelSpillOver = (TextView) dialogAddFav.findViewById(R.id.tvCancelSpover);
//        TextView tvContinueSpOver = (TextView) dialogAddFav.findViewById(R.id.tvContinueSpOver);
       /* tvCancelSpillOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogAddFav.dismiss();
                if(action != null){
                    action.methodCancel();
                }
            }
        });
        tvContinueSpOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Setup spillover view and its details.
                dialogAddFav.dismiss();
                if(action != null){
                    action.methodDone();
                }
            }
        });*/
        dialogAddFav.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialogAddFav.show();
    }


}

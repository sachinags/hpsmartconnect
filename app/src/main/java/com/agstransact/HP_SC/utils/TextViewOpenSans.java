package com.agstransact.HP_SC.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.agstransact.HP_SC.R;

import androidx.appcompat.widget.AppCompatTextView;

/**
 * Created by Satish Patel on 30,March,2021
 */
public class TextViewOpenSans extends AppCompatTextView {

    public TextViewOpenSans(Context context) {
        this(context, null);
    }

    public TextViewOpenSans(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextViewOpenSans(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (isInEditMode())
            return;

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.FontText);

        if (ta != null) {
            String fontAsset = ta.getString(R.styleable.FontText_typefaceAsset);

            if (!TextUtils.isEmpty(fontAsset)) {
                Typeface tf = FontManager.getInstance().getFont(fontAsset);
                int style = Typeface.NORMAL;
                float size = getTextSize();

                if (getTypeface() != null)
                    style = getTypeface().getStyle();

                if (tf != null)
                    setTypeface(tf, style);
                else
                    Log.d("FontText", String.format("Could not create a font from asset: %s", fontAsset));
            }
        }
    }

}

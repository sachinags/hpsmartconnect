package com.agstransact.HP_SC.utils;


import androidx.fragment.app.Fragment;

/**
 * Created by amit.verma on 22-09-2017.
 */

public interface ActionChangeFrag {
    void addFragment(Fragment fragment);
    void ReplaceFrag(Fragment fragment);
    void updateDrawer(boolean isShown);
}

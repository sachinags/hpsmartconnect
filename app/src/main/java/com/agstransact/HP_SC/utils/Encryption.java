package com.agstransact.HP_SC.utils;

import android.util.Base64;

import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by ritesh.bhavsar on 20-09-2017.
 */

public class Encryption {

//    String KEY_AES = "AGSIndiaSwitch12";

    public String encrypt(String value) {
        try {
//            byte[] key = KEY_AES.getBytes("UTF-8");
            byte[] key = AES.KEY.getBytes("UTF-8");
//            byte[] ivs = KEY_AES.getBytes("UTF-8");
            byte[] ivs = AES.KEY.getBytes("UTF-8");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(ivs);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, paramSpec);
            return Base64.encodeToString(cipher.doFinal(value.getBytes("UTF-8")), Base64.NO_WRAP);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public String encrypt_hpcl(String value) {
        try {
//            byte[] key = KEY_AES.getBytes("UTF-8");
            byte[] key = AES.KEY.getBytes("UTF-8");
//            byte[] ivs = KEY_AES.getBytes("UTF-8");
            byte[] ivs = AES.KEY.getBytes("UTF-8");
            Cipher cipher = Cipher.getInstance("RSA");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "RSA");
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(ivs);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, paramSpec);
            return Base64.encodeToString(cipher.doFinal(value.getBytes("UTF-8")), Base64.NO_WRAP);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String encryptDelink(String value) {
        try {
//            byte[] key = KEY_AES.getBytes("UTF-8");
            byte[] key = AES.OLD_PRE_KEY.getBytes("UTF-8");
//            byte[] ivs = KEY_AES.getBytes("UTF-8");
            byte[] ivs = AES.OLD_PRE_KEY.getBytes("UTF-8");
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
            SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
            AlgorithmParameterSpec paramSpec = new IvParameterSpec(ivs);
            cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, paramSpec);
            return Base64.encodeToString(cipher.doFinal(value.getBytes("UTF-8")), Base64.NO_WRAP);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}

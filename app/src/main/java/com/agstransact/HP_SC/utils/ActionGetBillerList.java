package com.agstransact.HP_SC.utils;


/**
 * Created by amit.verma on 22-09-2017.
 */

public interface ActionGetBillerList {
    String getBillerList();

    String getTransactionList(int type);
}

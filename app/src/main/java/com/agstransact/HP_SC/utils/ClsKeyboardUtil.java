package com.agstransact.HP_SC.utils;

import android.app.Activity;
import androidx.fragment.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.PopupWindow;
import android.widget.SearchView;

public class ClsKeyboardUtil {

	Activity mActivity;
	
	public ClsKeyboardUtil(Activity mactivity) {
		// TODO Auto-generated constructor stub
		mActivity = mactivity;
	}
	
	public void setupUI(View view) {
	    try {
			if(!(view instanceof EditText) &&  !(view instanceof Button) ) {
			    view.setOnTouchListener(new OnTouchListener() {
			        public boolean onTouch(View v, MotionEvent event) {
			            hideSoftKeyboard(mActivity);
			            return false;
			        }
			    });
			}
			if (view instanceof ViewGroup) {
			    for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
			        View innerView = ((ViewGroup) view).getChildAt(i);
			    	setupUI(innerView);
			    }
			}
		} catch (Exception e) {
			
		}
	}
	
	public void setupUIChat(View view, final PopupWindow pop) {
	    try {
			if(!(view instanceof EditText) &&  !(view instanceof Button) ) {
			    view.setOnTouchListener(new OnTouchListener() {
			        public boolean onTouch(View v, MotionEvent event) {
			        	hideSoftKeyboardChat(mActivity,pop);
			            return false;
			        }
			    });
			}
			if (view instanceof ViewGroup) {
			    for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
			        View innerView = ((ViewGroup) view).getChildAt(i);
			    	setupUI(innerView);
			    }
			}
		} catch (Exception e) {
			
		}
	}

	public void hideSoftKeyboardChat(Activity activity,PopupWindow pop) {
		try {
			InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
			pop.dismiss();
			
			
		} catch (Exception e) {
			
		}
	}
	public void hideSoftKeyboard(Activity activity) {
		try {
			InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
			
		} catch (Exception e) {
			
		}
	}
	
	public void hideSoftKeyboard(FragmentActivity activity, SearchView sview) {
		try {
			InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(sview.getWindowToken(), 0);
			
		} catch (Exception e) {
			
		}
	}
	
	public void hideSoftKeyboard(FragmentActivity activity, EditText sview) {
		try {
			InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(sview.getWindowToken(), 0);
			
		} catch (Exception e) {
			
		}
	}
}

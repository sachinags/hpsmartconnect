package com.agstransact.HP_SC.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by prashant.gadekar on 11-07-2017.
 */

public class UtilDate {


    /*public  static String getCurrentSysDate(){

       Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String strDate = year + "-" + (month + 1) + "-" + day;
        return  strDate;
    }*/

    public  static String getCurrentSysDateDDMMYYY(){

        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String strDate =  day+ "-" + (month + 1) + "-" + year;
        return  strDate;
    }

}

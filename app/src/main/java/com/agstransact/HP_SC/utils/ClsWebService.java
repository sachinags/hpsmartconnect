package com.agstransact.HP_SC.utils;

import android.content.Context;
import android.content.Intent;

import com.agstransact.HP_SC.MainApplication;
import com.agstransact.HP_SC.R;
import com.agstransact.HP_SC.dashboard.HOSDashboardActivity;
import com.agstransact.HP_SC.login.LoginActivity;
import com.agstransact.HP_SC.preferences.UserDataPrefrence;
import com.agstransact.HP_SC.splash.ActivityLaunchScreen;
//import com.agstransact.HP_SC.dashboard.DashBoardActivity;
//import com.agstransact.HP_SC.launcher.ActivityLauncher;
//import com.agstransact.HP_SC.login.LoginActivity;
//import com.agstransact.HP_SC.prefrence.UserDataPrefrence;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Iterator;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

import static com.agstransact.HP_SC.utils.ConstantDeclaration.IS_SSL_CONTEXT;
import static com.agstransact.HP_SC.utils.ConstantDeclaration.SERVER_NOT_RESPONDING;
import static com.agstransact.HP_SC.utils.ConstantDeclaration.SERVER_UNDER_MENTANANCE;

//import static com.agstransact.HP_SC.common.ConstantDeclaration.IS_SSL_CONTEXT;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.SERVER_NOT_RESPONDING;
//import static com.agstransact.HP_SC.common.ConstantDeclaration.SERVER_UNDER_MENTANANCE;

/**
 * Created by ritesh.bhavsar and amit verma  on 16-09-2017.
 */

public class ClsWebService {

//    private static String BaseUrl = "https:/\/13.71.112.68:7501/api/CoreEngine";
//  private static final String BaseUrl_DEV = "https://13.71.112.68:7522/api/CoreEngine";  //dev
//    private static final String BaseUrl_DEV_local = "https://104.211.202.8:7522/api/CoreEngine";  //dev old
    private static final String BaseUrl_DEV = "https://dev.agsindia.com:7522/api/CoreEngine";  //dev
    private static final String BaseUrl_DEV_Login_For_DEV = "https://dev.agsindia.com:7522/api/Darapay/";  //dev for login forever
//    private static final String BaseUrl_DEV_Login_For_SIT = "https://uatsys.agsindia.com:7526/api/Darapay/";  //SIT for login forever
//    private static final String BaseUrl_DEV_Login_For_UAT = "https://uatsys.agsindia.com:7523/api/Darapay/";  //UAT for login forever
//    private static final String BaseUrl_DEV_Login_For_PROD = "https://app.darapay.com.kh:8000/api/Darapay/";  //PROD for login forever
//    private static final String BaseUrl_UAT = "https://uatsys.agsindia.com:7523/api/CoreEngine"; //UAT
//    private static final String BaseUrl_SIT = "https://uatsys.agsindia.com:7526/api/CoreEngine"; //SIT testing
////    private static String BaseUrl `= "https://app.darapay.com.kh:8000/api/CoreEngine"; //Combodia production
//    private static final String BaseUrl_PROD = "https://app.darapay.com.kh:8000/api/CoreEngine"; //Combodia production
//    private static final String BaseUrl_Demo =  "http://52.172.50.133:7889/api/CoreEngine";  //dev
//    private static final String BaseUrl_Demo1 = "http://13.71.112.68:7570/api/CoreEngine";  //dev


//    private static final String STR_UAT_CER = "uatsys.cer";
//    private static final String STR_UAT_CER = "uat_certificate_220719.cer";
////    private static final String STR_UAT_CER = "star_agsindia_co.crt";
//    private static final String STR_PROD_CER = "app_darapay_com_kh.cer";
//    private static final String STR_DEV_CER = "local_agsindia_com.cer";
    private static final String STR_DEV_CER = "androidCer.cer";
//    private static final String STR_DEV_CER = "dev_thawani_cert.cer";
    public static String CERTIFICATE = STR_DEV_CER;
    public static String BaseUrl = BaseUrl_DEV;
    //   as per sadhana changed used old baseurl for login 23-12-19
//    public statidc String BaseUrl_DEV_Login_For = BaseUrl_DEV;
    //as per sadhana changed used old baseurl for login 23-12-19

    public static String BaseUrl_DEV_Login_For = BaseUrl_DEV_Login_For_DEV;

    static WeakReference<Context> mmContext = null;
    static String token;


    //    With SdaSL Pinning
    public static String PostObject(String methodName, boolean isGet, JSONObject jsonObj) {

        if (!ConstantDeclaration.IS_NO_SSL) {
//            WeakReference<Context> mmContext = null;
                mmContext = null;
            if(MainApplication.mDashBoardContext!=null){
                mmContext = new WeakReference<Context>(MainApplication.mDashBoardContext);
            }else if(MainApplication.mLoginContext!=null){
                mmContext = new WeakReference<Context>(MainApplication.mLoginContext);
            }else if(MainApplication.mLauncherContext!=null){
                mmContext = new WeakReference<Context>(MainApplication.mLauncherContext);
            }

            URL murl = null;
//        HostnameVerifier allHostsValid = new HostnameVerifier() {
//            public boolean verify(String hostname, SSLSession session) {
//                return true;
//            }
//        };

            HttpsURLConnection connection = null;
            try {
                /*Amit 30.11.17 ssl pining */
                // Load CAs from an InpuctStream
    // (could be from a resource or ByteArrayInputStream or ...)
                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                /*dev*/
    //            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open("indiatransact_64.cer"));
                 /*uat and sit*/
                InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
                /*production*/
    //            InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open("app_darapay_com_kh.cer"));
                Certificate ca;
                try {
                    ca = cf.generateCertificate(caInput);
                    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
                } finally {
                    caInput.close();
                }

    // Create a KeyStore containing our trusted CAs
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);

    // Create a TrustManager that trusts the CAs in our KeyStore
                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);

    // Create an SSLContext that uses our TrustManager
                SSLContext context = SSLContext.getInstance("TLS");
                context.init(null, tmf.getTrustManagers(), null);

    // Tell the URLConnection to use a SocketFactory from our SSLContext
                /*Amit 30.11.17 ssl pining */

//                if(methodName.equalsIgnoreCase("delink")){
//                    murl = new URL(BaseUrl);
//                }else{
//                    if (methodName.equalsIgnoreCase("2")) {
//                        murl = new URL(BaseUrl );
//                    }else if (methodName.equalsIgnoreCase("1")) {
////                        murl = new URL(BaseUrl_DEV_Login_For + "Login/" );
//                        murl = new URL(BaseUrl_DEV_Login_For);
//                    }else if (methodName.equalsIgnoreCase("")) {
//                        murl = new URL(BaseUrl_DEV_Login_For );
//
//                    }
//                }

                if(methodName.equalsIgnoreCase("delink")){
                    murl = new URL(BaseUrl);
                }else{
                    if (methodName.equalsIgnoreCase("2")) {
                        murl = new URL(BaseUrl );
                    }else if (methodName.equalsIgnoreCase("1")) {
                        murl = new URL(BaseUrl_DEV_Login_For + "Login/" );
                    }else if (methodName.equalsIgnoreCase("")) {
                        murl = new URL(BaseUrl_DEV_Login_For + "Post/");

                    }
                }

                connection = (HttpsURLConnection) murl.openConnection();
                /*Comment below line when url is development 06.12.17*/
                if(IS_SSL_CONTEXT) {
                    connection.setSSLSocketFactory(context.getSocketFactory());
                }
    /*06.12.17*/
    //            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

                /*try {
                    connection.setDefaultHostnameVerifier(new HostnameVerifier() {
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    });
                    SSLContext context = SSLContext.getInstance("TLS");
                    context.init(null, new X509TrustManager[]{new X509TrustManager() {
                        public void checkClientTrusted(X509Certificate[] chain,
                                                       String authType) {
                        }

                        public void checkServerTrusted(X509Certificate[] chain,
                                                       String authType) {
                        }

                        public X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                    }}, new SecureRandom());
                    connection.setDefaultSSLSocketFactory(
                            context.getSocketFactory());
                } catch (Exception e) { // should never happen
                    e.printStackTrace();
                }
                connection.setHostnameVerifier(allHostsValid);*/

    //            connection.setRequestProperty("Content-length", "0");
                connection.setRequestProperty("Content-type", "application/json");
    //            connection.setRequestProperty("Content-type", "application/json; charset=utf-8");

                try {
                    if (methodName.equalsIgnoreCase("")){
                        token = UserDataPrefrence.getPreference(MainApplication.getContext().getString(R.string.prefrence_name), MainApplication.getContext(), ConstantDeclaration.TOKEN, "");
//                        UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),MainApplication.getContext(), ConstantDeclaration.TOKEN, token);
                        connection.setRequestProperty("authorization", "Bearer " + token);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (isGet) {
                    connection.setRequestMethod("GET");
                } else {
                    connection.setRequestMethod("POST");
                }

                connection.setUseCaches(false);
                connection.setAllowUserInteraction(false);
                connection.setConnectTimeout(45000);
//                connection.setReadTimeout(45000);
                connection.setReadTimeout(150000);

                DataOutputStream obj = new DataOutputStream(connection.getOutputStream());
                obj.writeBytes(jsonObj.toString());
                obj.flush();

    //            connection.connect();

                int responsecode = connection.getResponseCode();

                if (responsecode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    reader.close();

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    String str_resCode = jsonObject.getString("ResponseCode");
                    String str_resMsg = jsonObject.getString("ResponseMsg");
                    String str_resTime = "";
                    try {
                        if (str_resCode.equalsIgnoreCase("5555")) {

                            try {
                                UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
                                        MainApplication.getContext(), ConstantDeclaration.TOKEN, "");
                            } catch (Exception e) {
                                Log.e("EX", e.toString());
                            }


                            //riteshb 13-12-17
                            if(MainApplication.mDashBoardContext !=null) {
                                if(mmContext.get() == MainApplication.mDashBoardContext){
                                    Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
                                    ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
                                    ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
                                    MainApplication.mDashBoardContext = null;
                                }else{
                                    return "";
                                }
                            }
                            else if(MainApplication.mLoginContext !=null) {
                                if(mmContext.get() == MainApplication.mLoginContext) {
                                    Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
                                    ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
                                    ((LoginActivity) MainApplication.mLoginContext).finish();
                                    MainApplication.mLoginContext = null;
                                }else{
                                    return "";
                                }
                            }else if(MainApplication.mLauncherContext !=null) {
//                                if(mmContext.get() == MainApplication.mLauncherContext) {
//                                    new UniversalDialog(MainApplication.mLauncherContext, new AlertDialogInterface() {
//                                        @Override
//                                        public void methodDone() {
//
//                                            Intent intent = new Intent(MainApplication.mLauncherContext, ActivityLaunchScreen.class);
//                                            ((ActivityLaunchScreen) MainApplication.mLauncherContext).startActivity(intent);
//                                            ((ActivityLaunchScreen) MainApplication.mLauncherContext).finish();
//                                            MainApplication.mLauncherContext = null;
//
//                                        }
//
//                                        @Override
//                                        public void methodCancel() {
//
//                                        }
//                                    }, "",
//                                            str_resMsg, "OK", "").showAlert();
//                                }else{
//                                    return "";
//                                }
                                if(mmContext.get() == MainApplication.mLauncherContext) {
                                    Intent intent = new Intent(MainApplication.mLauncherContext, ActivityLaunchScreen.class);
                                    ((ActivityLaunchScreen) MainApplication.mLauncherContext).startActivity(intent);
                                    ((ActivityLaunchScreen) MainApplication.mLauncherContext).finish();
                                    MainApplication.mLauncherContext = null;
                                }else{
                                    return "";
                                }
                            }
    //                        return "";
                            //riteshb 13-12-17
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        str_resTime = jsonObject.getString("TimeStamp");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String str_response_msg = "";
                    if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
                        if (methodName.equalsIgnoreCase("delink")) {
                            str_response_msg = AES.decrypt(str_resMsg, AES.OLD_PRE_KEY);
                            str_resTime = AES.decrypt(str_resTime, AES.OLD_PRE_KEY);
                        } else {
                            str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
                        str_resTime = AES.decrypt(str_resTime, AES.KEY);
                    }
                    }
                    else  if (jsonObj.getString("TRANTYPE").equalsIgnoreCase("NEARME")&&
                            str_resCode.equalsIgnoreCase("907") ) {
                        try {
                            UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
                                    MainApplication.getContext(), ConstantDeclaration.TOKEN, token);
                            Log.e("Token", UserDataPrefrence.getPreference(MainApplication.getContext().getString(R.string.prefrence_name),
                                    MainApplication.getContext(), ConstantDeclaration.TOKEN, ""));
                        } catch (Exception e) {
                            Log.e("EX", e.toString());
                        }
                        str_response_msg = str_resMsg;
                    }
                    else {
                        str_response_msg = str_resMsg;
                    }




                    String res = str_resCode
                            + "||" + str_response_msg
                            + "||" + str_resTime;

//                    TODO PrashantG 18-10-2019 for logout when user enter 3 invalid pin
                    try {
                        if (!jsonObj.getString("TRANTYPE").equalsIgnoreCase("NEARME") &&
                                str_resCode.equalsIgnoreCase("907")) {
                            try{
                            if (str_resCode.equalsIgnoreCase(ConstantDeclaration.INVALID_PIN)) {

                                try {
                                    UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
                                            MainApplication.getContext(), ConstantDeclaration.TOKEN, "");
                                } catch (Exception e) {
                                    Log.e("EX", e.toString());
                                }
                            }
                        } catch(Exception e){
                            e.printStackTrace();
                        }
                    }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
//                    TODO PrashantG 18-10-2019 for logout when user enter 3 invalid pin

                    try {
                        UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
                                MainApplication.getContext(), ConstantDeclaration.STAN_KEY, "");
                    } catch (Exception e) {
                        Log.e("EX", e.toString());
                    }

                    return res.trim();
                }else{
                    if (responsecode == 401) {
                        String res = "401"
                                + "||" + "Your session expired!"
                                + "||" + "";
                        return res.trim();
                    } else if(responsecode == 500){
                        String res = "954"
                                + "||" + SERVER_UNDER_MENTANANCE
                                + "||" + "";

                        return  res;
                    }
                }
            } catch (java.net.SocketTimeoutException e) {
                return SERVER_NOT_RESPONDING;
            } catch (java.io.IOException e) {
                String res = "954"
                        + "||" + SERVER_UNDER_MENTANANCE
                        + "||" + "";

                return  res;
//                return SERVER_NOT_RESPONDING;
            } catch (Exception e) {
                e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                Log.e("API Exception", e.toString());
    //            String s = e.printStackTrace();
                UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
                        MainApplication.getContext(), ConstantDeclaration.STAN_KEY, "");
                return SERVER_NOT_RESPONDING;
            } finally {

                try {
                    connection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            return "";
        } else {

            WeakReference<Context> mmContext = null;
            if(MainApplication.mDashBoardContext!=null){
                mmContext = new WeakReference<Context>(MainApplication.mDashBoardContext);
            }else if(MainApplication.mLoginContext!=null){
                mmContext = new WeakReference<Context>(MainApplication.mLoginContext);
            }

            URL murl = null;

            HttpURLConnection connection = null;
            try {

                if(methodName.equalsIgnoreCase("delink")){
                    murl = new URL(BaseUrl);
                }else{
                    murl = new URL(BaseUrl + methodName);
                }

                connection = (HttpURLConnection) murl.openConnection();

                connection.setRequestProperty("Content-type", "application/json");

                if (isGet) {
                    connection.setRequestMethod("GET");
                } else {
                    connection.setRequestMethod("POST");
                }

                connection.setUseCaches(false);
                connection.setAllowUserInteraction(false);
                connection.setConnectTimeout(45000);
                connection.setReadTimeout(45000);

                DataOutputStream obj = new DataOutputStream(connection.getOutputStream());
                obj.writeBytes(jsonObj.toString());
                obj.flush();


                int responsecode = connection.getResponseCode();

                if (responsecode == HttpsURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line = "";
                    while ((line = reader.readLine()) != null) {
                        sb.append(line + "\n");
                    }
                    reader.close();

                    JSONObject jsonObject = new JSONObject(sb.toString());
                    String str_resCode = jsonObject.getString("ResponseCode");
                    String str_resMsg = jsonObject.getString("ResponseMsg");
                    String str_resTime = "";
                    try {
                        if (str_resCode.equalsIgnoreCase("5555")) {
                            //riteshb 13-12-17
                            if(MainApplication.mDashBoardContext !=null) {
                                if(mmContext.get() == MainApplication.mDashBoardContext){
                                    Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
                                    ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
                                    ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
                                    MainApplication.mDashBoardContext = null;
                                }else{
                                    return "";
                                }
                            }else if(MainApplication.mLoginContext !=null) {
                                if(mmContext.get() == MainApplication.mLoginContext) {
                                    Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
                                    ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
                                    ((LoginActivity) MainApplication.mLoginContext).finish();
                                    MainApplication.mLoginContext = null;
                                }else{
                                    return "";
                                }
                            }
//                        return "";
                            //riteshb 13-12-17
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        str_resTime = jsonObject.getString("TimeStamp");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String str_response_msg = "";
                    if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
                        if (methodName.equalsIgnoreCase("delink")) {
                            str_response_msg = AES.decrypt(str_resMsg, AES.OLD_PRE_KEY);
                            str_resTime = AES.decrypt(str_resTime, AES.OLD_PRE_KEY);
                        } else {
                            str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
                            str_resTime = AES.decrypt(str_resTime, AES.KEY);
                        }
                    } else {
                        str_response_msg = str_resMsg;
                    }

                    String res = str_resCode
                            + "||" + str_response_msg
                            + "||" + str_resTime;
                    try {
                        UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
                                MainApplication.getContext(), ConstantDeclaration.STAN_KEY, "");
                    } catch (Exception e) {
                        Log.e("EX", e.toString());
                    }

                    return res.trim();
                }else{
                    if (responsecode == 401) {
                        String res = "401"
                                + "||" + "Session expired"
                                + "||" + "";
                        return res.trim();
                    }
                }
            } catch (java.net.SocketTimeoutException e) {
                return SERVER_NOT_RESPONDING;
            } catch (java.io.IOException e) {
                return SERVER_NOT_RESPONDING;
            } catch (Exception e) {
                e.printStackTrace();
                StringWriter sw = new StringWriter();
                PrintWriter pw = new PrintWriter(sw);
                e.printStackTrace(pw);
                Log.e("API Exception", e.toString());
//            String s = e.printStackTrace();
                UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
                        MainApplication.getContext(), ConstantDeclaration.STAN_KEY, "");
                return SERVER_NOT_RESPONDING;
            } finally {

                try {
                    connection.disconnect();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }


            return "";

        }
    }



//    Without SSL Pinning

//    public static String PostObject(String methodName, boolean isGet, JSONObject jsonObj) {
//
//        WeakReference<Context> mmContext = null;
//        if(MainApplication.mDashBoardContext!=null){
//            mmContext = new WeakReference<Context>(MainApplication.mDashBoardContext);
//        }else if(MainApplication.mLoginContext!=null){
//            mmContext = new WeakReference<Context>(MainApplication.mLoginContext);
//        }
//
//        URL murl = null;
//
//        HttpURLConnection connection = null;
//        try {
//
//            if(methodName.equalsIgnoreCase("delink")){
//                murl = new URL(BaseUrl);
//            }else{
//                murl = new URL(BaseUrl + methodName);
//            }
//
//            connection = (HttpURLConnection) murl.openConnection();
//
//            connection.setRequestProperty("Content-type", "application/json");
//
//            if (isGet) {
//                connection.setRequestMethod("GET");
//            } else {
//                connection.setRequestMethod("POST");
//            }
//
//            connection.setUseCaches(false);
//            connection.setAllowUserInteraction(false);
//            connection.setConnectTimeout(45000);
//            connection.setReadTimeout(45000);
//
//            DataOutputStream obj = new DataOutputStream(connection.getOutputStream());
//            obj.writeBytes(jsonObj.toString());
//            obj.flush();
//
//
//            int responsecode = connection.getResponseCode();
//
//            if (responsecode == HttpsURLConnection.HTTP_OK) {
//                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
//                StringBuilder sb = new StringBuilder();
//                String line = "";
//                while ((line = reader.readLine()) != null) {
//                    sb.append(line + "\n");
//                }
//                reader.close();
//
//                JSONObject jsonObject = new JSONObject(sb.toString());
//                String str_resCode = jsonObject.getString("ResponseCode");
//                String str_resMsg = jsonObject.getString("ResponseMsg");
//                String str_resTime = "";
//                try {
//                    if (str_resCode.equalsIgnoreCase("5555")) {
//                        //riteshb 13-12-17
//                        if(MainApplication.mDashBoardContext !=null) {
//                            if(mmContext.get() == MainApplication.mDashBoardContext){
//                                Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
//                                ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
//                                ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
//                                MainApplication.mDashBoardContext = null;
//                            }else{
//                                return "";
//                            }
//                        }else if(MainApplication.mLoginContext !=null) {
//                            if(mmContext.get() == MainApplication.mLoginContext) {
//                                Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
//                                ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
//                                ((LoginActivity) MainApplication.mLoginContext).finish();
//                                MainApplication.mLoginContext = null;
//                            }else{
//                                return "";
//                            }
//                        }
////                        return "";
//                        //riteshb 13-12-17
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                try {
//                    str_resTime = jsonObject.getString("TimeStamp");
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//                String str_response_msg = "";
//                if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
//                    if (methodName.equalsIgnoreCase("delink")) {
//                        str_response_msg = AES.decrypt(str_resMsg, AES.OLD_PRE_KEY);
//                        str_resTime = AES.decrypt(str_resTime, AES.OLD_PRE_KEY);
//                    } else {
//                        str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
//                        str_resTime = AES.decrypt(str_resTime, AES.KEY);
//                    }
//                } else {
//                    str_response_msg = str_resMsg;
//                }
//
//                String res = str_resCode
//                        + "||" + str_response_msg
//                        + "||" + str_resTime;
//                try {
//                    UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
//                            MainApplication.getContext(), ConstantDeclaration.STAN_KEY, "");
//                } catch (Exception e) {
//                    Log.e("EX", e.toString());
//                }
//
//                return res.trim();
//            }
//        } catch (java.net.SocketTimeoutException e) {
//            return SERVER_NOT_RESPONDING;
//        } catch (java.io.IOException e) {
//            return SERVER_NOT_RESPONDING;
//        } catch (Exception e) {
//            e.printStackTrace();
//            StringWriter sw = new StringWriter();
//            PrintWriter pw = new PrintWriter(sw);
//            e.printStackTrace(pw);
//            Log.e("API Exception", e.toString());
////            String s = e.printStackTrace();
//            UserDataPrefrence.savePreference(MainApplication.getContext().getString(R.string.prefrence_name),
//                    MainApplication.getContext(), ConstantDeclaration.STAN_KEY, "");
//            return SERVER_NOT_RESPONDING;
//        } finally {
//
//            try {
//                connection.disconnect();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//
//
//        return "";
//    }


//    Image Upload without SSL Pinning

    public static String UplaodImg(String sourceFileUri, boolean isGet, JSONObject jsonObj, String flagName) {


        if (ConstantDeclaration.IS_NO_SSL) {
            String fileName = sourceFileUri;

            HttpURLConnection conn = null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            String attachmentName = "bitmap";
            String attachmentFileName = "bitmap.bmp";
            File sourceFile = new File(sourceFileUri);

            if (!sourceFile.isFile()) {
                return "";
            } else {
                try {

                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(BaseUrl+"/MediaUpload");
                    // Open a HTTP  connection to  the URL
                    conn = (HttpURLConnection) url.openConnection();

                    fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Cache-Control", "no-cache");
                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
    //                conn.setRequestProperty("uploaded_file", fileName);
                    conn.setRequestProperty(flagName, fileName);

                    dos = new DataOutputStream(conn.getOutputStream());
    //                String uid = "12345", flag = "F";
    //                String field1 = "USERID";
    //                String field2 = "FLAG";
                    Iterator<String> iter = jsonObj.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        try {
                            Object value = jsonObj.get(key);
                            dos.writeBytes(twoHyphens + boundary + lineEnd);
                            dos.writeBytes("Content-Disposition: form-data; name=" +
                                    key.trim() + "" + lineEnd);
                            dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "" + lineEnd);
                            dos.writeBytes(lineEnd);
                            dos.writeBytes(value.toString().trim() + lineEnd);
                            dos.writeBytes(lineEnd);
                        } catch (JSONException e) {
                            // Something went wrong!
                        }
                    }
    //                dos.writeBytes(twoHyphens + boundary + lineEnd);
    ////                dos.writeBytes("Content-Disposition: form-data; name='uploaded_file';filename="
    ////                                + fileName + "" + lineEnd);
    //                dos.writeBytes("Content-Disposition: form-data; name=\"" +
    //                        field1 + "\"" + lineEnd);
    ////                filename=\"" +
    ////                        fileName + "\"" + lineEnd);
    //                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "\"" + lineEnd);
    //                dos.writeBytes(lineEnd);
    //                dos.writeBytes(uid + lineEnd);
    //                dos.writeBytes(lineEnd);
    ////

    //                dos.writeBytes(twoHyphens + boundary + lineEnd);
    ////                dos.writeBytes("Content-Disposition: form-data; name='uploaded_file';filename="
    ////                                + fileName + "" + lineEnd);
    //                dos.writeBytes("Content-Disposition: form-data; name=\"" +
    //                        field2 + "\"" + lineEnd);
    //                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "\"" + lineEnd);
    //
    //                dos.writeBytes(lineEnd);
    //                dos.writeBytes(flag + lineEnd);
    //                dos.writeBytes(lineEnd);


                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\""+flagName+"\";filename="
                            + fileName + "" + lineEnd);
    //                dos.writeBytes("Content-Type: image/png;" + "\"" + lineEnd);

    //                dos.writeBytes("Content-Disposition: form-data; name=\"" +
    //                        attachmentName + "\";filename=\"" +
    //                        fileName + "\";USERID=\"" +
    //                        uid + "\";FLAG=\"" +
    //                        flag + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);


                    // create a buffer of  maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {

                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    }

                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    // Responses from the server (code and message)
                    int serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();
    //                Toast.makeText(MainActivity.this, serverResponseMessage + ": " + serverResponseCode,
    //                        Toast.LENGTH_SHORT).show();
                    android.util.Log.i("uploadFile", "HTTP Response is : "
                            + serverResponseMessage + ": " + serverResponseCode);

                    if (serverResponseCode == 200) {
                        InputStream responseStream = new
                                BufferedInputStream(conn.getInputStream());

                        BufferedReader responseStreamReader =
                                new BufferedReader(new InputStreamReader(responseStream));

                        String line = "";
                        StringBuilder sb = new StringBuilder();

                        while ((line = responseStreamReader.readLine()) != null) {
                            sb.append(line).append("\n");
                        }
                        responseStreamReader.close();
    //                    String response = sb.toString();
                        responseStream.close();
                        JSONObject jsonObject = new JSONObject(sb.toString());
                        String str_resCode = jsonObject.getString("ResponseCode");
                        String str_resMsg = jsonObject.getString("ResponseMsg");
                        String str_resTime = "";
                        try {
                            if (str_resCode.equalsIgnoreCase("5555")) {
                                //riteshb 13-12-17
                                if(MainApplication.mDashBoardContext !=null) {
                                    Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
                                    ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
                                    ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
                                }else if(MainApplication.mLoginContext !=null) {
                                    Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
                                    ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
                                    ((LoginActivity) MainApplication.mLoginContext).finish();
                                }
                                //riteshb 13-12-17
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            str_resTime = jsonObject.getString("TimeStamp");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String str_response_msg = "";
                        if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
                            str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
                            str_resTime = AES.decrypt(str_resTime, AES.KEY);
                        } else {
                            str_response_msg = str_resMsg;
                        }
                        String res = str_resCode
                                + "||" + str_response_msg
                                + "||" + str_resTime;
                        return res.trim();
                    }
                    //close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();
                } catch (MalformedURLException ex) {
    //                dialog.dismiss();
                    ex.printStackTrace();
                    android.util.Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
                } catch (final Exception e) {

    //                dialog.dismiss();
                    e.printStackTrace();

                }
    //            dialog.dismiss();
                return "";
            }
        } else {

            String fileName = sourceFileUri;

            HttpsURLConnection conn = null;
            DataOutputStream dos = null;
            String lineEnd = "\r\n";
            String twoHyphens = "--";
            String boundary = "*****";
            int bytesRead, bytesAvailable, bufferSize;
            byte[] buffer;
            int maxBufferSize = 1 * 1024 * 1024;
            String attachmentName = "bitmap";
            String attachmentFileName = "bitmap.bmp";
            File sourceFile = new File(sourceFileUri);

            if (!sourceFile.isFile()) {
                return "";
            } else {
                try {
                    HostnameVerifier allHostsValid = new HostnameVerifier() {
                        public boolean verify(String hostname, SSLSession session) {
                            return true;
                        }
                    };

                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
                    InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
                    Certificate ca;
                    try {
                        ca = cf.generateCertificate(caInput);
                        System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
                    } finally {
                        caInput.close();
                    }

// Create a KeyStore containing our trusted CAs
                    String keyStoreType = KeyStore.getDefaultType();
                    KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                    keyStore.load(null, null);
                    keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
                    String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                    TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                    tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
                    SSLContext context = SSLContext.getInstance("TLS");
                    context.init(null, tmf.getTrustManagers(), null);

                    // open a URL connection to the Servlet
                    FileInputStream fileInputStream = new FileInputStream(sourceFile);
                    URL url = new URL(BaseUrl+"/MediaUpload");
                    // Open a HTTP  connection to  the URL
                    conn = (HttpsURLConnection) url.openConnection();
                    if(IS_SSL_CONTEXT) {
                        conn.setSSLSocketFactory(context.getSocketFactory());
                    }

//                try {
//                    conn.setDefaultHostnameVerifier(new HostnameVerifier() {
//                        public boolean verify(String hostname, SSLSession session) {
//                            return true;
//                        }
//                    });
//                    SSLContext context = SSLContext.getInstance("TLS");
//                    context.init(null, new X509TrustManager[]{new X509TrustManager() {
//                        public void checkClientTrusted(X509Certificate[] chain,
//                                                       String authType) {
//                        }
//
//                        public void checkServerTrusted(X509Certificate[] chain,
//                                                       String authType) {
//                        }
//
//                        public X509Certificate[] getAcceptedIssuers() {
//                            return new X509Certificate[0];
//                        }
//                    }}, new SecureRandom());
//                    conn.setDefaultSSLSocketFactory(
//                            context.getSocketFactory());
//                } catch (Exception e) { // should never happen
//                    e.printStackTrace();
//                }
//                conn.setHostnameVerifier(allHostsValid);


//                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
                    /*dev*/
//            InputStream caInput = new BufferedInputStream(MainActivity.this.getAssets().open("indiatransact_64.cer"));
//            InputStream caInput = new BufferedInputStream(MainActivity.this.getAssets().open("indiatransact_binary.cer"));
//             /*uat and sit*/
////                    InputStream caInput = new BufferedInputStream(MainActivity.this.getAssets().open("uatsys.cer"));
//            /*production*/
////            InputStream caInput = new BufferedInputStream(MainActivity.this.getAssets().open("app_darapay_com_kh.cer"));
//                    Certificate ca;
//                    try {
//                        ca = cf.generateCertificate(caInput);
//                        System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//                    } finally {
//                        caInput.close();
//                    }
//
//// Create a KeyStore containing our trusted CAs
//                    String keyStoreType = KeyStore.getDefaultType();
//                    KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//                    keyStore.load(null, null);
//                    keyStore.setCertificateEntry("ca", ca);
//
//// Create a TrustManager that trusts the CAs in our KeyStore
//                    String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//                    TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//                    tmf.init(keyStore);
//
//// Create an SSLContext that uses our TrustManager
//                    SSLContext context = SSLContext.getInstance("TLS");
//                    context.init(null, tmf.getTrustManagers(), null);

// Tell the URLConnection to use a SocketFactory from our SSLContext
                    /*Amit 30.11.17 ssl pining */
                    /*Comment below line when url is development 06.12.17*/
//                    conn.setSSLSocketFactory(context.getSocketFactory());
                    fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
                    conn.setDoInput(true); // Allow Inputs
                    conn.setDoOutput(true); // Allow Outputs
                    conn.setUseCaches(false); // Don't use a Cached Copy
                    conn.setRequestMethod("POST");
                    conn.setRequestProperty("Connection", "Keep-Alive");
                    conn.setRequestProperty("Cache-Control", "no-cache");
                    conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                    conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
//                conn.setRequestProperty("uploaded_file", fileName);
                    conn.setRequestProperty(flagName, fileName);

                    dos = new DataOutputStream(conn.getOutputStream());
//                String uid = "12345", flag = "F";
//                String field1 = "USERID";
//                String field2 = "FLAG";
                    Iterator<String> iter = jsonObj.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        try {
                            Object value = jsonObj.get(key);
                            dos.writeBytes(twoHyphens + boundary + lineEnd);
                            dos.writeBytes("Content-Disposition: form-data; name=" +
                                    key.trim() + "" + lineEnd);
                            dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "" + lineEnd);
                            dos.writeBytes(lineEnd);
                            dos.writeBytes(value.toString().trim() + lineEnd);
                            dos.writeBytes(lineEnd);
                        } catch (JSONException e) {
                            // Something went wrong!
                        }
                    }
//                dos.writeBytes(twoHyphens + boundary + lineEnd);
////                dos.writeBytes("Content-Disposition: form-data; name='uploaded_file';filename="
////                                + fileName + "" + lineEnd);
//                dos.writeBytes("Content-Disposition: form-data; name=\"" +
//                        field1 + "\"" + lineEnd);
////                filename=\"" +
////                        fileName + "\"" + lineEnd);
//                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "\"" + lineEnd);
//                dos.writeBytes(lineEnd);
//                dos.writeBytes(uid + lineEnd);
//                dos.writeBytes(lineEnd);
////

//                dos.writeBytes(twoHyphens + boundary + lineEnd);
////                dos.writeBytes("Content-Disposition: form-data; name='uploaded_file';filename="
////                                + fileName + "" + lineEnd);
//                dos.writeBytes("Content-Disposition: form-data; name=\"" +
//                        field2 + "\"" + lineEnd);
//                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "\"" + lineEnd);
//
//                dos.writeBytes(lineEnd);
//                dos.writeBytes(flag + lineEnd);
//                dos.writeBytes(lineEnd);


                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\""+flagName+"\";filename="
                            + fileName + "" + lineEnd);
//                dos.writeBytes("Content-Type: image/png;" + "\"" + lineEnd);

//                dos.writeBytes("Content-Disposition: form-data; name=\"" +
//                        attachmentName + "\";filename=\"" +
//                        fileName + "\";USERID=\"" +
//                        uid + "\";FLAG=\"" +
//                        flag + "\"" + lineEnd);
                    dos.writeBytes(lineEnd);


                    // create a buffer of  maximum size
                    bytesAvailable = fileInputStream.available();

                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];

                    // read file and write it into form...
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {

                        dos.write(buffer, 0, bufferSize);
                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    }

                    // send multipart form data necesssary after file data...
                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                    // Responses from the server (code and message)
                    int serverResponseCode = conn.getResponseCode();
                    String serverResponseMessage = conn.getResponseMessage();
//                Toast.makeText(MainActivity.this, serverResponseMessage + ": " + serverResponseCode,
//                        Toast.LENGTH_SHORT).show();
                    android.util.Log.i("uploadFile", "HTTP Response is : "
                            + serverResponseMessage + ": " + serverResponseCode);

                    if (serverResponseCode == 200) {
                        InputStream responseStream = new
                                BufferedInputStream(conn.getInputStream());

                        BufferedReader responseStreamReader =
                                new BufferedReader(new InputStreamReader(responseStream));

                        String line = "";
                        StringBuilder sb = new StringBuilder();

                        while ((line = responseStreamReader.readLine()) != null) {
                            sb.append(line).append("\n");
                        }
                        responseStreamReader.close();
//                    String response = sb.toString();
                        responseStream.close();
                        JSONObject jsonObject = new JSONObject(sb.toString());
                        String str_resCode = jsonObject.getString("ResponseCode");
                        String str_resMsg = jsonObject.getString("ResponseMsg");
                        String str_resTime = "";
                        try {
                            if (str_resCode.equalsIgnoreCase("5555")) {
                                //riteshb 13-12-17
                                if(MainApplication.mDashBoardContext !=null) {
                                    Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
                                    ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
                                    ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
                                }else if(MainApplication.mLoginContext !=null) {
                                    Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
                                    ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
                                    ((LoginActivity) MainApplication.mLoginContext).finish();
                                }
                                //riteshb 13-12-17
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            str_resTime = jsonObject.getString("TimeStamp");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        String str_response_msg = "";
                        if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
                            str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
                            str_resTime = AES.decrypt(str_resTime, AES.KEY);
                        } else {
                            str_response_msg = str_resMsg;
                        }
                        String res = str_resCode
                                + "||" + str_response_msg
                                + "||" + str_resTime;
                        return res.trim();
                    }
                    //close the streams //
                    fileInputStream.close();
                    dos.flush();
                    dos.close();
                } catch (MalformedURLException ex) {
//                dialog.dismiss();
                    ex.printStackTrace();
                    android.util.Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
                } catch (final Exception e) {

//                dialog.dismiss();
                    e.printStackTrace();

                }
//            dialog.dismiss();
                return "";
            }


        }
    }

//    Image Upload with SSL Pinning

//    public static String UplaodImg(String sourceFileUri, boolean isGet, JSONObject jsonObj, String flagName) {
//        String fileName = sourceFileUri;
//
//        HttpsURLConnection conn = null;
//        DataOutputStream dos = null;
//        String lineEnd = "\r\n";
//        String twoHyphens = "--";
//        String boundary = "*****";
//        int bytesRead, bytesAvailable, bufferSize;
//        byte[] buffer;
//        int maxBufferSize = 1 * 1024 * 1024;
//        String attachmentName = "bitmap";
//        String attachmentFileName = "bitmap.bmp";
//        File sourceFile = new File(sourceFileUri);
//
//        if (!sourceFile.isFile()) {
//            return "";
//        } else {
//            try {
//                HostnameVerifier allHostsValid = new HostnameVerifier() {
//                    public boolean verify(String hostname, SSLSession session) {
//                        return true;
//                    }
//                };
//
//                CertificateFactory cf = CertificateFactory.getInstance("X.509");
//                InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
//                Certificate ca;
//                try {
//                    ca = cf.generateCertificate(caInput);
//                    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
//                } finally {
//                    caInput.close();
//                }
//
//// Create a KeyStore containing our trusted CAs
//                String keyStoreType = KeyStore.getDefaultType();
//                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
//                keyStore.load(null, null);
//                keyStore.setCertificateEntry("ca", ca);
//
//// Create a TrustManager that trusts the CAs in our KeyStore
//                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
//                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
//                tmf.init(keyStore);
//
//// Create an SSLContext that uses our TrustManager
//                SSLContext context = SSLContext.getInstance("TLS");
//                context.init(null, tmf.getTrustManagers(), null);
//
//                // open a URL connection to the Servlet
//                FileInputStream fileInputStream = new FileInputStream(sourceFile);
//                URL url = new URL(BaseUrl+"/MediaUpload");
//                // Open a HTTP  connection to  the URL
//                conn = (HttpsURLConnection) url.openConnection();
//                conn.setSSLSocketFactory(context.getSocketFactory());
//
//
////                try {
////                    conn.setDefaultHostnameVerifier(new HostnameVerifier() {
////                        public boolean verify(String hostname, SSLSession session) {
////                            return true;
////                        }
////                    });
////                    SSLContext context = SSLContext.getInstance("TLS");
////                    context.init(null, new X509TrustManager[]{new X509TrustManager() {
////                        public void checkClientTrusted(X509Certificate[] chain,
////                                                       String authType) {
////                        }
////
////                        public void checkServerTrusted(X509Certificate[] chain,
////                                                       String authType) {
////                        }
////
////                        public X509Certificate[] getAcceptedIssuers() {
////                            return new X509Certificate[0];
////                        }
////                    }}, new SecureRandom());
////                    conn.setDefaultSSLSocketFactory(
////                            context.getSocketFactory());
////                } catch (Exception e) { // should never happen
////                    e.printStackTrace();
////                }
////                conn.setHostnameVerifier(allHostsValid);
//
//
////                    CertificateFactory cf = CertificateFactory.getInstance("X.509");
//            /*dev*/
////            InputStream caInput = new BufferedInputStream(MainActivity.this.getAssets().open("indiatransact_64.cer"));
////            InputStream caInput = new BufferedInputStream(MainActivity.this.getAssets().open("indiatransact_binary.cer"));
////             /*uat and sit*/
//////                    InputStream caInput = new BufferedInputStream(MainActivity.this.getAssets().open("uatsys.cer"));
////            /*production*/
//////            InputStream caInput = new BufferedInputStream(MainActivity.this.getAssets().open("app_darapay_com_kh.cer"));
////                    Certificate ca;
////                    try {
////                        ca = cf.generateCertificate(caInput);
////                        System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
////                    } finally {
////                        caInput.close();
////                    }
////
////// Create a KeyStore containing our trusted CAs
////                    String keyStoreType = KeyStore.getDefaultType();
////                    KeyStore keyStore = KeyStore.getInstance(keyStoreType);
////                    keyStore.load(null, null);
////                    keyStore.setCertificateEntry("ca", ca);
////
////// Create a TrustManager that trusts the CAs in our KeyStore
////                    String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
////                    TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
////                    tmf.init(keyStore);
////
////// Create an SSLContext that uses our TrustManager
////                    SSLContext context = SSLContext.getInstance("TLS");
////                    context.init(null, tmf.getTrustManagers(), null);
//
//// Tell the URLConnection to use a SocketFactory from our SSLContext
//            /*Amit 30.11.17 ssl pining */
//            /*Comment below line when url is development 06.12.17*/
////                    conn.setSSLSocketFactory(context.getSocketFactory());
//                fileName = fileName.substring(fileName.lastIndexOf("/") + 1);
//                conn.setDoInput(true); // Allow Inputs
//                conn.setDoOutput(true); // Allow Outputs
//                conn.setUseCaches(false); // Don't use a Cached Copy
//                conn.setRequestMethod("POST");
//                conn.setRequestProperty("Connection", "Keep-Alive");
//                conn.setRequestProperty("Cache-Control", "no-cache");
//                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
//                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
////                conn.setRequestProperty("uploaded_file", fileName);
//                conn.setRequestProperty(flagName, fileName);
//
//                dos = new DataOutputStream(conn.getOutputStream());
////                String uid = "12345", flag = "F";
////                String field1 = "USERID";
////                String field2 = "FLAG";
//                Iterator<String> iter = jsonObj.keys();
//                while (iter.hasNext()) {
//                    String key = iter.next();
//                    try {
//                        Object value = jsonObj.get(key);
//                        dos.writeBytes(twoHyphens + boundary + lineEnd);
//                        dos.writeBytes("Content-Disposition: form-data; name=" +
//                                key.trim() + "" + lineEnd);
//                        dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "" + lineEnd);
//                        dos.writeBytes(lineEnd);
//                        dos.writeBytes(value.toString().trim() + lineEnd);
//                        dos.writeBytes(lineEnd);
//                    } catch (JSONException e) {
//                        // Something went wrong!
//                    }
//                }
////                dos.writeBytes(twoHyphens + boundary + lineEnd);
//////                dos.writeBytes("Content-Disposition: form-data; name='uploaded_file';filename="
//////                                + fileName + "" + lineEnd);
////                dos.writeBytes("Content-Disposition: form-data; name=\"" +
////                        field1 + "\"" + lineEnd);
//////                filename=\"" +
//////                        fileName + "\"" + lineEnd);
////                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "\"" + lineEnd);
////                dos.writeBytes(lineEnd);
////                dos.writeBytes(uid + lineEnd);
////                dos.writeBytes(lineEnd);
//////
//
////                dos.writeBytes(twoHyphens + boundary + lineEnd);
//////                dos.writeBytes("Content-Disposition: form-data; name='uploaded_file';filename="
//////                                + fileName + "" + lineEnd);
////                dos.writeBytes("Content-Disposition: form-data; name=\"" +
////                        field2 + "\"" + lineEnd);
////                dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "\"" + lineEnd);
////
////                dos.writeBytes(lineEnd);
////                dos.writeBytes(flag + lineEnd);
////                dos.writeBytes(lineEnd);
//
//
//                dos.writeBytes(twoHyphens + boundary + lineEnd);
//                dos.writeBytes("Content-Disposition: form-data; name=\""+flagName+"\";filename="
//                                + fileName + "" + lineEnd);
////                dos.writeBytes("Content-Type: image/png;" + "\"" + lineEnd);
//
////                dos.writeBytes("Content-Disposition: form-data; name=\"" +
////                        attachmentName + "\";filename=\"" +
////                        fileName + "\";USERID=\"" +
////                        uid + "\";FLAG=\"" +
////                        flag + "\"" + lineEnd);
//                dos.writeBytes(lineEnd);
//
//
//                // create a buffer of  maximum size
//                bytesAvailable = fileInputStream.available();
//
//                bufferSize = Math.min(bytesAvailable, maxBufferSize);
//                buffer = new byte[bufferSize];
//
//                // read file and write it into form...
//                bytesRead = fileInputStream.read(buffer, 0, bufferSize);
//
//                while (bytesRead > 0) {
//
//                    dos.write(buffer, 0, bufferSize);
//                    bytesAvailable = fileInputStream.available();
//                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
//                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);
//
//                }
//
//                // send multipart form data necesssary after file data...
//                dos.writeBytes(lineEnd);
//                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
//
//                // Responses from the server (code and message)
//                int serverResponseCode = conn.getResponseCode();
//                String serverResponseMessage = conn.getResponseMessage();
////                Toast.makeText(MainActivity.this, serverResponseMessage + ": " + serverResponseCode,
////                        Toast.LENGTH_SHORT).show();
//                android.util.Log.i("uploadFile", "HTTP Response is : "
//                        + serverResponseMessage + ": " + serverResponseCode);
//
//                if (serverResponseCode == 200) {
//                    InputStream responseStream = new
//                            BufferedInputStream(conn.getInputStream());
//
//                    BufferedReader responseStreamReader =
//                            new BufferedReader(new InputStreamReader(responseStream));
//
//                    String line = "";
//                    StringBuilder sb = new StringBuilder();
//
//                    while ((line = responseStreamReader.readLine()) != null) {
//                        sb.append(line).append("\n");
//                    }
//                    responseStreamReader.close();
////                    String response = sb.toString();
//                    responseStream.close();
//                    JSONObject jsonObject = new JSONObject(sb.toString());
//                    String str_resCode = jsonObject.getString("ResponseCode");
//                    String str_resMsg = jsonObject.getString("ResponseMsg");
//                    String str_resTime = "";
//                    try {
//                        if (str_resCode.equalsIgnoreCase("5555")) {
//                            //riteshb 13-12-17
//                            if(MainApplication.mDashBoardContext !=null) {
//                                Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
//                                ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
//                                ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
//                            }else if(MainApplication.mLoginContext !=null) {
//                                Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
//                                ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
//                                ((LoginActivity) MainApplication.mLoginContext).finish();
//                            }
//                            //riteshb 13-12-17
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    try {
//                        str_resTime = jsonObject.getString("TimeStamp");
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//
//                    String str_response_msg = "";
//                    if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
//                            str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
//                            str_resTime = AES.decrypt(str_resTime, AES.KEY);
//                    } else {
//                        str_response_msg = str_resMsg;
//                    }
//                    String res = str_resCode
//                            + "||" + str_response_msg
//                            + "||" + str_resTime;
//                    return res.trim();
//                }
//                //close the streams //
//                fileInputStream.close();
//                dos.flush();
//                dos.close();
//            } catch (MalformedURLException ex) {
////                dialog.dismiss();
//                ex.printStackTrace();
//                android.util.Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
//            } catch (final Exception e) {
//
////                dialog.dismiss();
//                e.printStackTrace();
//
//            }
////            dialog.dismiss();
//            return "";
//        }
//    }
    public static String UplaodImg(String sourceFileUriFront, String sourceFileUriBack, boolean isGet, JSONObject jsonObj) {
        String fileNameFront = sourceFileUriFront;
        String fileNameBack = sourceFileUriBack;

        HttpsURLConnection conn = null;
        DataOutputStream dos = null;
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "*****";
        int bytesRead, bytesAvailable, bufferSize;
        byte[] buffer;
        int maxBufferSize = 1 * 1024 * 1024;
        String attachmentName = "bitmap";
        String attachmentFileName = "bitmap.bmp";
        File sourceFileFront = new File(sourceFileUriFront);
        File sourceFileBack = new File(sourceFileUriBack);

        if (!sourceFileFront.isFile() && !sourceFileBack.isFile()) {
            return "";
        } else {
            try {
                HostnameVerifier allHostsValid = new HostnameVerifier() {
                    public boolean verify(String hostname, SSLSession session) {
                        return true;
                    }
                };

                CertificateFactory cf = CertificateFactory.getInstance("X.509");
                InputStream caInput = new BufferedInputStream(MainApplication.getContext().getAssets().open(CERTIFICATE));
                Certificate ca;
                try {
                    ca = cf.generateCertificate(caInput);
                    System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
                } finally {
                    caInput.close();
                }

// Create a KeyStore containing our trusted CAs
                String keyStoreType = KeyStore.getDefaultType();
                KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                keyStore.load(null, null);
                keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
                String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
                tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
                SSLContext context = SSLContext.getInstance("TLS");
                context.init(null, tmf.getTrustManagers(), null);

                // open a URL connection to the Servlet
                FileInputStream fileInputStreamFront = new FileInputStream(sourceFileFront);
                FileInputStream fileInputStreamBack = new FileInputStream(sourceFileBack);
                URL url = new URL(BaseUrl+"/MediaUpload");
                // Open a HTTP  connection to  the URL
                conn = (HttpsURLConnection) url.openConnection();
                if(IS_SSL_CONTEXT) {
                    conn.setSSLSocketFactory(context.getSocketFactory());
                }                fileNameFront = fileNameFront.substring(fileNameFront.lastIndexOf("/") + 1);
                fileNameBack = fileNameBack.substring(fileNameBack.lastIndexOf("/") + 1);
                conn.setDoInput(true); // Allow Inputs
                conn.setDoOutput(true); // Allow Outputs
                conn.setUseCaches(false); // Don't use a Cached Copy
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Connection", "Keep-Alive");
                conn.setRequestProperty("Cache-Control", "no-cache");
                conn.setRequestProperty("ENCTYPE", "multipart/form-data");
                conn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
//                conn.setRequestProperty("FRONTPHOTO", fileNameFront);
//                conn.setRequestProperty("BACKPHOTO", fileNameBack);

                dos = new DataOutputStream(conn.getOutputStream());
//                String uid = "12345", flag = "F";
//                String field1 = "USERID";
//                String field2 = "FLAG";
                Iterator<String> iter = jsonObj.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        Object value = jsonObj.get(key);
                        dos.writeBytes(twoHyphens + boundary + lineEnd);
                        dos.writeBytes("Content-Disposition: form-data; name=" +
                                key.trim() + "" + lineEnd);
                        dos.writeBytes("Content-Type: text/plain; charset=UTF-8" + "" + lineEnd);
                        dos.writeBytes(lineEnd);
                        dos.writeBytes(value.toString().trim() + lineEnd);
                        dos.writeBytes(lineEnd);
                    } catch (JSONException e) {
                        // Something went wrong!
                    }
                }
                String keyFront = "FRONTPHOTO";
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name="
                        + keyFront.trim() + ";filename="
                        + fileNameFront.trim() + "" + lineEnd);
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                bytesAvailable = fileInputStreamFront.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStreamFront.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStreamFront.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStreamFront.read(buffer, 0, bufferSize);

                }
                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
//                fileInputStreamFront.close();

                String keyBack = "BACKPHOTO";
                dos.writeBytes(twoHyphens + boundary + lineEnd);
                dos.writeBytes("Content-Disposition: form-data; name="+
                        keyBack.trim()+";filename="
                        + fileNameBack.trim() + "" + lineEnd);
                dos.writeBytes(lineEnd);

                // create a buffer of  maximum size
                fileInputStreamBack = new FileInputStream(sourceFileBack);
                bytesAvailable = fileInputStreamBack.available();

                bufferSize = Math.min(bytesAvailable, maxBufferSize);
                buffer = new byte[bufferSize];

                // read file and write it into form...
                bytesRead = fileInputStreamBack.read(buffer, 0, bufferSize);

                while (bytesRead > 0) {

                    dos.write(buffer, 0, bufferSize);
                    bytesAvailable = fileInputStreamBack.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    bytesRead = fileInputStreamBack.read(buffer, 0, bufferSize);

                }

                // send multipart form data necesssary after file data...
                dos.writeBytes(lineEnd);
                dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

                // Responses from the server (code and message)
                int serverResponseCode = conn.getResponseCode();
                String serverResponseMessage = conn.getResponseMessage();
//                Toast.makeText(MainActivity.this, serverResponseMessage + ": " + serverResponseCode,
//                        Toast.LENGTH_SHORT).show();
                android.util.Log.i("uploadFile", "HTTP Response is : "
                        + serverResponseMessage + ": " + serverResponseCode);



                if (serverResponseCode == 200) {
                    InputStream responseStream = new
                            BufferedInputStream(conn.getInputStream());

                    BufferedReader responseStreamReader =
                            new BufferedReader(new InputStreamReader(responseStream));

                    String line = "";
                    StringBuilder sb = new StringBuilder();

                    while ((line = responseStreamReader.readLine()) != null) {
                        sb.append(line).append("\n");
                    }
                    responseStreamReader.close();
//                    String response = sb.toString();
                    responseStream.close();
                    JSONObject jsonObject = new JSONObject(sb.toString());
                    String str_resCode = jsonObject.getString("ResponseCode");
                    String str_resMsg = jsonObject.getString("ResponseMsg");
                    String str_resTime = "";
                    try {
                        if (str_resCode.equalsIgnoreCase("5555")) {
                            //riteshb 13-12-17
                            if(MainApplication.mDashBoardContext !=null) {
                                Intent intent = new Intent(MainApplication.mDashBoardContext, ActivityLaunchScreen.class);
                                ((HOSDashboardActivity) MainApplication.mDashBoardContext).startActivity(intent);
                                ((HOSDashboardActivity) MainApplication.mDashBoardContext).finish();
                            }else if(MainApplication.mLoginContext !=null) {
                                Intent intent = new Intent(MainApplication.mLoginContext, ActivityLaunchScreen.class);
                                ((LoginActivity) MainApplication.mLoginContext).startActivity(intent);
                                ((LoginActivity) MainApplication.mLoginContext).finish();
                            }
                            //riteshb 13-12-17
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        str_resTime = jsonObject.getString("TimeStamp");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String str_response_msg = "";
                    if (str_resCode.equalsIgnoreCase("0") || str_resCode.equalsIgnoreCase("00")) {
                        str_response_msg = AES.decrypt(str_resMsg, AES.KEY);
                        str_resTime = AES.decrypt(str_resTime, AES.KEY);
                    } else {
                        str_response_msg = str_resMsg;
                    }
                    //close the streams //
                    fileInputStreamFront.close();
                fileInputStreamBack.close();
                    dos.flush();
                    dos.close();
                    String res = str_resCode
                            + "||" + str_response_msg
                            + "||" + str_resTime;
                    return res.trim();
                }
            } catch (MalformedURLException ex) {
//                dialog.dismiss();
                ex.printStackTrace();
                android.util.Log.e("Upload file to server", "error: " + ex.getMessage(), ex);
            } catch (final Exception e) {

//                dialog.dismiss();
                e.printStackTrace();

            }
//            dialog.dismiss();
            return "";
        }
    }
}

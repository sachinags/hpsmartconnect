package com.agstransact.HP_SC.utils;


public class Log {
    public static String debugMode = "true";
    static final String LOG = debugMode;

    public static void i(String tag, String string) {
        if (LOG.equals("true")) android.util.Log.i(tag, string);
    }

    public static void e(String tag, String string) {
        if (LOG.equals("true")) android.util.Log.e(tag, string);
    }

    public static void d(String tag, String string) {
        if (LOG.equals("true")) android.util.Log.d(tag, string);
    }

    public static void v(String tag, String string) {
        if (LOG.equals("true")) android.util.Log.v(tag, string);
    }

    public static void w(String tag, String string) {
        if (LOG.equals("true")) android.util.Log.w(tag, string);
    }

    public static void isLoggable(String tag, int string) {
        if (LOG.equals("true")) android.util.Log.isLoggable(tag, string);
    }

    public static String getStackTraceString(Throwable tr) {
        if (LOG.equals("true")) {
            return android.util.Log.getStackTraceString(tr);
        } else {
            return "";
        }
    }

    public static void e1(String tag, String string) {
        if (LOG.equals("true")) android.util.Log.e(tag, string);
    }
}
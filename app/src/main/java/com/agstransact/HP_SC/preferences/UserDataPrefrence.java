package com.agstransact.HP_SC.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;

//import com.agstransact.HP_SC.common.Log;
//import com.google.common.reflect.TypeToken;
//import com.google.gson.Gson;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by amit.verma on 23-08-2017.
 */

public class UserDataPrefrence {


    /*public static void saveLangCodePreference(String PREFERENCE_FILENAME, Context context, String tag, String stringValue) {

        String key = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);

        Log.e("Device id", key);


        SecurePreferences preferences = new SecurePreferences(context, PREFERENCE_FILENAME, key, true);
        preferences.put(tag, stringValue);
    }

    public static String getLangCodePreference(String PREFERENCE_FILENAME, Context context, String tag, String defaultString) {

        String key = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);

        SecurePreferences preferences = new SecurePreferences(context, PREFERENCE_FILENAME, key, true);
        return preferences.getString(tag, defaultString);

    }*/


    public static void savePreference(String PREFERENCE_FILENAME, Context context, String tag, String stringValue) {

        String key = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("Device id",key);
        SecurePreferences preferences = new SecurePreferences(context, PREFERENCE_FILENAME, tag, true);
        preferences.put(tag, stringValue);
    }

    public static void savePreference1(String PREFERENCE_FILENAME, Context context, String tag, String stringValue) {

        String key = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("Device id",key);
        SecurePreferences preferences = new SecurePreferences(context, PREFERENCE_FILENAME, tag, true);
        preferences.put(tag, stringValue);
    }
    public static void savePreference1(String PREFERENCE_FILENAME, Context context, String tag, ArrayList<String> list) {

        String key = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("Device id",key);
        SecurePreferences preferences = new SecurePreferences(context, PREFERENCE_FILENAME, tag, true);
//       for(int i =0;list.clear();i++){
//
//       }
//        preferences.put(tag, String.valueOf(stringValue.get(0)));
    }

    public static String getPreference(String PREFERENCE_FILENAME, Context context, String tag, String defaultString) {

        String key = Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID);

        SecurePreferences preferences = new SecurePreferences(context, PREFERENCE_FILENAME, tag, true);
        return preferences.getString(tag, defaultString);

    }
    public static String getPreference1(String PREFERENCE_FILENAME, Context context, String tag, String defaultString) {

        String key = Settings.Secure.getString(context.getContentResolver(),Settings.Secure.ANDROID_ID);

        SecurePreferences preferences = new SecurePreferences(context, PREFERENCE_FILENAME, tag, true);
        return preferences.getString(tag, defaultString);

    }
    public static void saveArrayList(List<String> list, Context context, String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public static List<String> getArrayList(String key, Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        return gson.fromJson(json, type);
    }
}

package com.agstransact.HP_SC.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

public class ModelResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("d3")
    @Expose
    private JSONArray d3;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMsg() {
        return message;
    }

    public void setMsg(String message) {
        this.message = message;
    }

    public JSONArray getD3() {
        return d3;
    }

    public void setD3(JSONArray d3) {
        this.d3 = d3;
    }

    /*public String getD3() {
        return d3;
    }

    public void setD3(String d3) {
        this.d3 = d3;
    }*/

}

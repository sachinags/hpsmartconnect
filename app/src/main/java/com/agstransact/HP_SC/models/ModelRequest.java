package com.agstransact.HP_SC.models;

public class ModelRequest {
    private String d1;
    private String d2;

    public ModelRequest(String d1, String d2) {
        this.d1 = d1;
        this.d2 = d2;
    }

    public String getD1() {
        return d1;
    }

    public void setD1(String d1) {
        this.d1 = d1;
    }

    public String getD2() {
        return d2;
    }

    public void setD2(String d2) {
        this.d2 = d2;
    }
}

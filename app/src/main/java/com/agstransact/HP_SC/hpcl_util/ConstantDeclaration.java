package com.agstransact.HP_SC.hpcl_util;

public class ConstantDeclaration {

    //UAT AGS
//    public static final String BASE_URL = "https://uatsys.agsindia.com:7526/api/";

    //UAT HPCL
    public static final String BASE_URL = "https://chosuat.hpcl.co.in/api/";

    //Prod HPCL
//    public static final String BASE_URL = "https://chosm.hpcl.co.in/api/";

    private static final String STR_DEV_CER = "agsindiacert20_22.cer";
    public static String CERTIFICATE = STR_DEV_CER;
}

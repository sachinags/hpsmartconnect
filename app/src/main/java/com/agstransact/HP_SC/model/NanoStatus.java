package com.agstransact.HP_SC.model;

public class NanoStatus {
    private float ptValue;
    private String ptStatus;

    public float getPtValue() {
        return ptValue;
    }

    public void setPtValue(float ptValue) {
        this.ptValue = ptValue;
    }

    public String getPtStatus() {
        return ptStatus;
    }

    public void setPtStatus(String ptStatus) {
        this.ptStatus = ptStatus;
    }
}

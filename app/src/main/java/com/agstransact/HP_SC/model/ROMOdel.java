package com.agstransact.HP_SC.model;

public class ROMOdel {
    private String rovalue;
    private String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getRovalue() {
        return rovalue;
    }

    public void setRovalue(String rovalue) {
        this.rovalue = rovalue;
    }
}

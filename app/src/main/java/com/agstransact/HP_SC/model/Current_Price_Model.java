package com.agstransact.HP_SC.model;

public class Current_Price_Model {
    String ProductName;
    String Status;
    String EffectiveDate;
    public Current_Price_Model(String productName, String status, String effectiveDate) {
        ProductName = productName;
        Status = status;
        EffectiveDate = effectiveDate;
    }

    public String getEffectiveDate() {
        return EffectiveDate;
    }

    public void setEffectiveDate(String effectiveDate) {
        EffectiveDate = effectiveDate;
    }


    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

}

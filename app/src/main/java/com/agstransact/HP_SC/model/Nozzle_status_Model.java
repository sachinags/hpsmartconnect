package com.agstransact.HP_SC.model;

public class Nozzle_status_Model {
    public Nozzle_status_Model(String product, String nozzlesConfigured, String nozzlesDisabled, String nozzlesinUse) {
        Product = product;
        NozzlesConfigured = nozzlesConfigured;
        NozzlesDisabled = nozzlesDisabled;
        NozzlesinUse = nozzlesinUse;
    }

    String Product,NozzlesConfigured,NozzlesDisabled,NozzlesinUse;

    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getNozzlesConfigured() {
        return NozzlesConfigured;
    }

    public void setNozzlesConfigured(String nozzlesConfigured) {
        NozzlesConfigured = nozzlesConfigured;
    }

    public String getNozzlesDisabled() {
        return NozzlesDisabled;
    }

    public void setNozzlesDisabled(String nozzlesDisabled) {
        NozzlesDisabled = nozzlesDisabled;
    }

    public String getNozzlesinUse() {
        return NozzlesinUse;
    }

    public void setNozzlesinUse(String nozzlesinUse) {
        NozzlesinUse = nozzlesinUse;
    }


}

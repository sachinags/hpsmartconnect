package com.agstransact.HP_SC.model;

public class Dupump_Details_Model {
    String DUMake;
    String DuNo;
    String PumpNo;
    String Nozzleno;
    String TankNo;
    String ProductName;

    public Dupump_Details_Model(String DUMake, String duNo, String pumpNo, String nozzleno, String tankNo, String productName) {
        this.DUMake = DUMake;
        DuNo = duNo;
        PumpNo = pumpNo;
        Nozzleno = nozzleno;
        TankNo = tankNo;
        ProductName = productName;
    }



    public String getDUMake() {
        return DUMake;
    }

    public void setDUMake(String DUMake) {
        this.DUMake = DUMake;
    }

    public String getDuNo() {
        return DuNo;
    }

    public void setDuNo(String duNo) {
        DuNo = duNo;
    }

    public String getPumpNo() {
        return PumpNo;
    }

    public void setPumpNo(String pumpNo) {
        PumpNo = pumpNo;
    }

    public String getNozzleno() {
        return Nozzleno;
    }

    public void setNozzleno(String nozzleno) {
        Nozzleno = nozzleno;
    }

    public String getTankNo() {
        return TankNo;
    }

    public void setTankNo(String tankNo) {
        TankNo = tankNo;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }



}

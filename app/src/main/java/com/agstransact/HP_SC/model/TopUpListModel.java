package com.agstransact.HP_SC.model;

/**
 * Created by Sagar.Sompura on 28-10-2017.
 */

public class TopUpListModel {
    private int topUpLogo;
    private String strTelcoName, strTelcoCustId, strTelcoID, strImagePath, strDenominations, strTelcoStatus;

    public TopUpListModel() {
    }

    public TopUpListModel(String strTelcoName, String strTelcoCustId, String strTelcoID,
                          String strImagePath, String strDenominations, String strTelcoStatus) {
//        , String strTelcoStatus
        this.strTelcoName = strTelcoName;
        this.strTelcoCustId = strTelcoCustId;
        this.strTelcoID = strTelcoID;
        this.strImagePath = strImagePath;
        this.strDenominations = strDenominations;
        this.strTelcoStatus = strTelcoStatus;

    }

    public String getStrTelcoName() {
        return strTelcoName;
    }

    public void setStrTelcoName(String strTelcoName) {
        this.strTelcoName = strTelcoName;
    }

    public String getStrTelcoCustId() {
        return strTelcoCustId;
    }

    public void setStrTelcoCustId(String strTelcoCustId) {
        this.strTelcoCustId = strTelcoCustId;
    }

    public String getStrTelcoID() {
        return strTelcoID;
    }

    public void setStrTelcoID(String strTelcoID) {
        this.strTelcoID = strTelcoID;
    }

    public int getTopUpLogo() {
        return topUpLogo;
    }

    public void setTopUpLogo(int topUpLogo) {
        this.topUpLogo = topUpLogo;
    }

    public String getStrImagePath() {
        return strImagePath;
    }

    public void setStrImagePath(String strImagePath) {
        this.strImagePath = strImagePath;
    }

    public String getStrDenominations() {
        return strDenominations;
    }

    public void setStrDenominations(String strDenominations) {
        this.strDenominations = strDenominations;
    }

    public String getStrTelcoStatus() {
        return strTelcoStatus;
    }

    public void setStrTelcoStatus(String strTelcoStatus) {
        this.strTelcoStatus = strTelcoStatus;
    }
}

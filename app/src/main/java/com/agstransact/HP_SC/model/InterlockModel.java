package com.agstransact.HP_SC.model;

public class InterlockModel {

    String interlockLbl;
    String interlockValue;

    public InterlockModel(String interlockLbl, String interlockValue) {
        this.interlockLbl = interlockLbl;
        this.interlockValue = interlockValue;
    }

    public String getInterlockLbl() {
        return interlockLbl;
    }

    public void setInterlockLbl(String interlockLbl) {
        this.interlockLbl = interlockLbl;
    }

    public String getInterlockValue() {
        return interlockValue;
    }

    public void setInterlockValue(String interlockValue) {
        this.interlockValue = interlockValue;
    }
}

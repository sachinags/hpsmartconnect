package com.agstransact.HP_SC.model;

/**
 * Created by kalpesh.dhumal on 26-06-2017.
 */

public class DrawerMenuModel {


    String menuName;
    int menuIcon;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public int getMenuIcon() {
        return menuIcon;
    }

    public void setMenuIcon(int menuIcon) {
        this.menuIcon = menuIcon;
    }


}

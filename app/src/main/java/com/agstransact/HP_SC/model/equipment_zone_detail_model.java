package com.agstransact.HP_SC.model;

public class equipment_zone_detail_model {
    String Product;
    String Tank;
    String Du;
    String Nozzle;

    public equipment_zone_detail_model(String product, String tank, String du, String nozzle) {
        Product = product;
        Tank = tank;
        Du = du;
        Nozzle = nozzle;
    }
    public String getProduct() {
        return Product;
    }

    public void setProduct(String product) {
        Product = product;
    }

    public String getTank() {
        return Tank;
    }

    public void setTank(String tank) {
        Tank = tank;
    }

    public String getDu() {
        return Du;
    }

    public void setDu(String du) {
        Du = du;
    }

    public String getNozzle() {
        return Nozzle;
    }

    public void setNozzle(String nozzle) {
        Nozzle = nozzle;
    }

}

package com.agstransact.HP_SC.model;

public class CriticalStock {
    private String product;
    private int stock;

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}

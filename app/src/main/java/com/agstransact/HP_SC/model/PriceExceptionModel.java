package com.agstransact.HP_SC.model;

import java.util.ArrayList;
import java.util.List;

public class PriceExceptionModel {

    private ArrayList<CategoryModel> categoryData;
    private ArrayList<ValueModel> valueData;

    public ArrayList<CategoryModel> getCategoryData() {
        return categoryData;
    }

    public void setCategoryData(ArrayList<CategoryModel> categoryData) {
        this.categoryData = categoryData;
    }

    public ArrayList<ValueModel> getValueData() {
        return valueData;
    }

    public void setValueData(ArrayList<ValueModel> valueData) {
        this.valueData = valueData;
    }
}

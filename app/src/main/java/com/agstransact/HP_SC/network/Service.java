package com.agstransact.HP_SC.network;

import com.agstransact.HP_SC.models.ModelRequest;
import com.agstransact.HP_SC.models.ModelResponse;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface Service {


    @POST("Login/GetVersion")
    Call<String> getVersionDetails(@Body JSONObject versionObject);
}
